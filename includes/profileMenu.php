	<h1 class="noUnderline">
<?php 
		if($userToProfile->data()->picture != ''){
			if($userToProfile->isPremium()){
				echo '<img class="circle-image premium" src="' . $userToProfile->data()->picture . '"/>';
				echo '<img class="circle-image-premium-icon" src="/images/crown.svg?v=14"/>';
			} else {
				echo '<img class="circle-image" src="' . $userToProfile->data()->picture . '"/>';
			}
		}
		
		
		$icon = '';
		if($userToProfile->isTeacher()){
			$icon = ' (Teacher)';
		} else if($userToProfile->isStudent()){
			//$icon = '<i class="fas fa-book-reader"></i>';
			if(isset($isTeachersStudent) && $isTeachersStudent){
				$icon = ' (Your Student)';
			} else {
				$icon = ' (Student)';
			}
		}
		echo escape($userToProfile->data()->username) . $icon;
?>
	</h1>
	
	
<?php
	//if($user->isLoggedIn() && $onPersonalProfile){ 
?>
	<div id="profileMenu">
		<ul>
<?php
	if($onPersonalProfile){
?>
			<li class="<?php if($currPage == "updateProfile"){echo ' active';}?>"><a href="/user/<?php echo $userToProfile->data()->username; ?>/profile">Profile</a></li>
<?php
		if($user->isTeacher()){?>
			<li class="<?php if($currPage == "classes"){echo ' active';}?>"><a href="/user/<?php echo $userToProfile->data()->username; ?>/classes">Classes</a></li>
			<li class="<?php if($currPage == "assignments"){echo ' active';}?>"><a href="/user/<?php echo $userToProfile->data()->username; ?>/assignments">Assignments</a></li>
<?php
		} else if($user->isStudent()){?>
			<li class="<?php if($currPage == "assignments"){echo ' active';}?>"><a href="/user/<?php echo $userToProfile->data()->username; ?>/assignments">Assignments</a></li>
<?php
		}
	}
?>
			<li class="<?php if($currPage == "testStats"){echo ' active';}?>"><a href="/user/<?php echo $userToProfile->data()->username; ?>/test-stats">Test Stats</a></li>
			<li class="<?php if($currPage == "typingLessons" || $currPage == 'anotherUsersTypingLessons'){echo ' active';}?>"><a href="/user/<?php echo $userToProfile->data()->username; ?>/lessons">Typing Lessons</a></li>
		</ul>
	</div>
<?php
	//}
?>