<script>
	var assignments = <?php echo json_encode($assignments); ?>;
	var userTargets = <?php echo '{tl_wpm_target: ' . $userData->tl_wpm_target . ', tl_accuracy_target: ' . $userData->tl_accuracy_target . '}'; ?>;
	
	var keyboardArr = <?php echo json_encode($keyboardArr);?>;
	var lessonsArr = <?php echo json_encode($lessonsArr);?>;
	var textTypesArr = <?php echo json_encode($textTypes);?>;
	var assignmentTypesArr = <?php echo json_encode($assignmentTypesArr);?>;
	var studentUserData = <?php echo json_encode($studentUserData);?>;
	var studentHistory = <?php echo json_encode($studentHistoryArr);?>;
	var studentDataComplete = new Map();
	var studentClassMap = new Map();
	var startDate;
	var endDate;
	var start = <?php echo ($userData->ol_duedate_filter == null) ? GetJSMoment(8)[0] : GetJSMoment($userData->ol_duedate_filter)[0]; ?>;
	var end = <?php echo ($userData->ol_duedate_filter == null) ? GetJSMoment(8)[1] : GetJSMoment($userData->ol_duedate_filter)[1]; ?>;
	var typeFilterBitwise = <?php echo ($userData->ol_type_filter == null) ? 'null' : $userData->ol_type_filter; ?>;
	var assignSort = <?php echo $userData->ol_assign_sort; ?>;
	
	var studentList;
	var studentsGroupedByClass = new Array();
	var studentsGroupedByAssignment = new Array();
	var studentList = new Array();
	var selectedStudents;
	var dueDate;
	var updateUS = false;
	
	var myTransfer;

    var settings = {
        "inputId": "languageInput",
        "data": studentList,
        "groupData": studentsGroupedByClass,
        "itemName": "username",
        "groupItemName": "groupName",
        "groupListName" : "groupData",
        "container": "transfer",
        "valueName": "user_id",
        "callable" : function (data, names) {
			selectedStudents = data;
			if(selectedStudents.length == 0 || defaultDueDate == undefined)
				$(".ui-dialog-buttonset button:contains('Create')").button("disable")
			else
				$(".ui-dialog-buttonset button:contains('Create')").button("enable")
            //$("#selectedItemSpan").text(names);
			
        }
    };

	//var myTransfer = $(".transfer").transfer(settings);
	//myTransfer.getSelectedItems();
	//Transfer.transfer(settings);

	$(document).ready(function(){
<?php
	if($user->isTeacher()){
?>
		assignments.forEach(mergeAssignmentsByStudent);
		
		// setup a array of individual assignments with an array of students attached
		function mergeAssignmentsByStudent(item, index){
			
			studentEntry = {"stud_id": item.stud_id, "username" : item.username, "email" : item.email, "firstname" : item.firstname, "lastname" : item.lastname, "progress" : item.progress, "completed_date" : item.completed_date, "tl_wpm_target": item.tl_wpm_target, "tl_accuracy_target": item.tl_accuracy_target};
			
			if(studentsGroupedByAssignment.length == 0 || item.assign_id != studentsGroupedByAssignment[studentsGroupedByAssignment.length - 1].assign_id){
				studentsGroupedByAssignment.push({"assign_id":item.assign_id, "assign_date":item.assign_date,"activity":item.activity, "at_name":item.at_name, "classroom_id":item.classroom_id, "due_date":item.due_date, "goal":item.goal, "metric_type":item.metric_type, "notes":item.notes, "selection":item.selection, "selection_readable":item.selection_readable, "title":item.title, "type":item.type, "selection_ttdl": item.selection_ttdl, "is_assigned": item.is_assigned, "studentData": new Array(studentEntry)});		// create new entry for assignment
			} else {
				studentsGroupedByAssignment[studentsGroupedByAssignment.length - 1].studentData.push(studentEntry);			// add student to recently added assignment
			}
		}
<?php
	}
?>
		
		function cb(start, end) {
			var myLabel = $('#reportrange').data('daterangepicker').chosenLabel;
			if(myLabel != undefined){
				labelHTML = HTMLelemStr(myLabel, 'div', '', 'dateRangeLabel');
			} else {
				myLabel = GetPickerLabelFromDates(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
				labelHTML = HTMLelemStr(myLabel, 'div', '', 'dateRangeLabel');
			}
			
			calendarIconHTML = HTMLelemStr('<i class="fa fa-filter" aria-hidden="true"></i>', 'div', '', 'iconContainer')
			insideTChtml = labelHTML + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY') + '<i class="fa fa-caret-down"></i>';
			if(myLabel == 'All Time'){
				insideTChtml = labelHTML + '<i class="fa fa-caret-down"></i>';
			}
			textContainerHTML = HTMLelemStr(insideTChtml, 'div', '', 'textContainer')
						
			$('#reportrange').html(calendarIconHTML + textContainerHTML);
			startDate = start.format('YYYY-MM-DD');
			endDate = end.format('YYYY-MM-DD');
		}

		$('#reportrange').daterangepicker({
			startDate: start,
			endDate: end,
			opens: 'left',
			applyButtonClasses: "stoUI stoUIClickable solidColor whiteTxt",
			cancelButtonClasses: "stoUI stoUIClickable ",
			ranges: {
			   'Tomorrow': [moment().add(1, 'days'), moment().add(1, 'days')],
			   'Today': [moment(), moment()],
			   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			   'Next 7 Days': [moment(), moment().add(6, 'days')],
			   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			   'Next 30 Days': [moment(), moment().add(29, 'days')],
			   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			   'Next Week': [moment().add(1, 'week').startOf('week'), moment().add(1, 'week').endOf('week')],
			   'This Week': [moment().startOf('week'), moment().endOf('week')],
			   'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
			   'Next Month': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')],
			   'This Month': [moment().startOf('month'), moment().endOf('month')],
			   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
			   'All Time': [moment('2020-01-01'), moment().add(10, 'year')]
			}
		}, cb);

		cb(start, end);
		
		
		startDate = start.format('YYYY-MM-DD');
		endDate = end.format('YYYY-MM-DD');
		
		
		studentUserData.forEach(SetupStudentClassroomMaps);
		
		SetupClassrooms();
<?php if($user->isTeacher()){?>
		AssignmentsToHTML(studentsGroupedByAssignment, true);
<?php } else if($user->isStudent()){?>
		AssignmentsToHTML(assignments, false);
<?php } ?>
		
		
		function setDD(date) {
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			startDate = start.format('YYYY-MM-DD');
			endDate = end.format('YYYY-MM-DD');
		}
<?php
	if($user->isTeacher()){
?>
		$('#ADDpicker').daterangepicker({
			"singleDatePicker": true,
			"timePicker": true,
			"locale": {
				"format": "M/DD h:mm A",
			},
			"startDate": "07/25/2020",
			"endDate": "07/31/2020",
			"minDate": moment().subtract(6, 'month').startOf('year'),
			"maxDate": moment().add(5, 'years'),
			//"minYear": parseInt(moment().format('YYYY')),
			//"maxYear": parseInt(moment().add(4, 'year').format('YYYY'),10),
			"opens": "center",
			"drops": "up"
		  }, function(start, end, label) {
			dueDate = start.toISOString().slice(0,19).replace('T',' ');
			$('#ADDpicker span').html(start.format('ddd MMMM D, YYYY @ h:mm A'));
			
			if(selectedStudents != undefined && selectedStudents.length != 0)
				$(".ui-dialog-buttonset button:contains('Create')").button("enable");
		});
		
		$('#ADDpicker').on('apply.daterangepicker', function(start, label){
			dueDate = label.startDate.toISOString().slice(0,19).replace('T',' ');
			$('#ADDpicker span').html(label.startDate.format('ddd MMMM D, YYYY @ h:mm A'));
			
			if(selectedStudents != undefined && selectedStudents.length != 0)
				$(".ui-dialog-buttonset button:contains('Create')").button("enable");
		});
		
		defaultDueDate = moment().endOf('week').subtract(1, 'days');
		
		if(dueDate == undefined)
			dueDate = defaultDueDate.toISOString().slice(0,19).replace('T',' ');
		
		$('#ADDpicker').data('daterangepicker').setStartDate(defaultDueDate);
		$('#ADDpicker').data('daterangepicker').setEndDate(defaultDueDate);
		
		$('#ATPselect').trigger("change");
		$('#ASPselect option:selected').prop('selected', false);
		$('#ASPselect option.specLesson[value="1"]').prop('selected', true);
		
		$('#ASPselect').trigger("change");
		<?php
	}
?>
		// Filter By Type
		$("#showUnassignedAssignmentsBtn").button();
		$("#showCurrentAssignmentsBtn").button();
		$("#showPastAssignmentsBtn").button();
		$("#showPastDueAssignmentsBtn").button();
		$("#showAllAssignmentsBtn").button();
		$("#showStudentProgressBtn").button();
		
		$("#uaLbl").data('assignClass', 'notAssigned');
		$("#caLbl").data('assignClass', 'currentAssignment');
		$("#paLbl").data('assignClass', 'completedAssignment');
		$("#pdaLbl").data('assignClass', 'pastDue');
		
		$('.individualTypeFilter').click(function() { 
			var jqElem = $(this);
			var isNowSelected = !jqElem.hasClass('ui-state-active');
			var assignClass = jqElem.data('assignClass');
			var assignClassNotClause = ':not(.dueDateFiltered)';
			
			assignmentClassSelector = "." + assignClass + assignClassNotClause;
			
			bitMask = GetBitMaskByTypeClassName(assignClass);
			
			typeFilterBitwise = SetTypeFilterBit(typeFilterBitwise, bitMask, isNowSelected);
			
			if(isNowSelected){
				$(assignmentClassSelector).show();
			} else {
				$(assignmentClassSelector).hide();
				CheckIfAllAssignmentsHidden();
			}
			updateUS = true;
			CheckIfAllAssignmentsHidden();
		});
		
		$('#aaLbl').click(function() { 
			var jqElem = $(this);
			var isNowSelected = !jqElem.hasClass('ui-state-active');
			
			if(isNowSelected){
				jqElem.html('<span class="ui-button-text">Hide All</span>');
				$("#uaLbl:not(.ui-state-active) span").click();
				$("#caLbl:not(.ui-state-active) span").click();
				$("#paLbl:not(.ui-state-active) span").click();
				$("#pdaLbl:not(.ui-state-active) span").click();
			} else {
				jqElem.html('<span class="ui-button-text">Show All</span>');
				$("#uaLbl.ui-state-active span").click();
				$("#caLbl.ui-state-active span").click();
				$("#paLbl.ui-state-active span").click();
				$("#pdaLbl.ui-state-active span").click();
			}
		});
		
		$('#showStudentProgress').click(function() { 
			var jqElem = $(this);
			var isNowSelected = !jqElem.hasClass('ui-state-active');
			
			if(isNowSelected){
				$(".assignStudentData").show();
			} else {
				$(".assignStudentData").hide();
			}
		});

		$('.hudContainer').mouseleave(function(){
			if(updateUS){
				updateUS = false;
				
				$.post('/setData.php', { type: "us", col: ['ol_type_filter', 'ol_assign_sort'], val: [typeFilterBitwise, parseInt($('#assignSortSelect').val())]},
					function(output){
				});
			}
		});
		
		$('.usfContainer').each(SetupUSFcontData);
		
		$('#assignSortSelect').selectmenu({
			//height:150,
			"width":"100%"
		});
		
		$('#assignSortSelect').on("selectmenuchange", function(event, data){
			if(data == undefined){
				theVal = assignSort;
			} else {
				theVal = data.item.value;
			}
			
			if(theVal == 0){
				paramName = "dueDate";
				ifLessThan = -1;
				ifGreaterThan = 1;
			} else if(theVal == 1){
				paramName = "dueDate";
				ifLessThan = 1;
				ifGreaterThan = -1;
			} else if(theVal == 2){
				paramName = "assignDate";
				ifLessThan = -1;
				ifGreaterThan = 1;
			} else if(theVal == 3){
				paramName = "assignDate";
				ifLessThan = 1;
				ifGreaterThan = -1;
			}
			
			domElems = $('.usfContainer');
			newUSFs = domElems.sort(function(a,b){
				if($('#' + a.id).data(paramName) < $('#' + b.id).data(paramName)){
					return ifLessThan;
				} else {
					return ifGreaterThan;
				}
			}).appendTo('#allAssignments');
			updateUS = true;
		});
		
		InitializeTypeFilters();
		$('#reportrange').trigger('apply.daterangepicker');
		
		$('#assignSortSelect').val(assignSort).selectmenu("refresh").trigger("selectmenuchange");
	});
	
	// put data to help sort usf containers
	function SetupUSFcontData(item, index){
		elemID = $(this)[0].id;
<?php
	if($user->isTeacher()){
?>
		$("#" + elemID).data("dueDate", new Date(studentsGroupedByAssignment[item].due_date));
		$("#" + elemID).data("assignDate", new Date(studentsGroupedByAssignment[item].assign_date));
<?php
	} else {
?>
		$("#" + elemID).data("dueDate", new Date(assignments[item].due_date));
		$("#" + elemID).data("assignDate", new Date(assignments[item].assign_date));
<?php
	} 
?>
	}
	
	// find student by class
	function SetupClassrooms(){
		
		studentClassMap.forEach(function(val, key){
			SetupClasses(key);
		});
		
		// only initialize dual listbox once
		if($('#transfer_double_languageInput').length == 0){
			Transfer.transfer(settings);
			//myTransfer = $(".transfer").transfer();
		}
	}
	
	function SetupClasses(classID){
		classTableHTML = '<tr><th>Student</th><th>Email</th><th>Lesson Time</th><th>Num Lessons Completed</th><th>Total Test Time</th></tr>';
		var studentsInClass = studentClassMap.get(classID);
		
		if(studentsInClass.length > 0){
			studentsGroupedByClass.push({"groupName":studentsInClass[0].class_name, "groupData": new Array()});
		}
		
		studentsInClass.forEach(AddStudentWithDataToClass);
		$('#class_' + classID).html(classTableHTML);
		
	}
	
	
	// lookup both student data and student history to get html string to display to teacher
	function AddStudentWithDataToClass(item, index, arr){
		var itemExtended = {...item, 
			"firstLastname": item.firstname + " " + item.lastname, 
			"lastFirstname": item.lastname + ", " + item.firstname,
			"firstLastnameEmail": item.firstname + " " + item.lastname + " (" + item.email + ")",
			"lastFirstnameEmail": item.lastname + ", " + item.firstname + " (" + item.email + ")"};
		
		
		// always add students to most recently added class
		studentsGroupedByClass[studentsGroupedByClass.length-1].groupData.push(itemExtended);	
		
		studentList.push(itemExtended);
		
		var allStudentHistory = studentHistory.filter(function(student){
			var effectiveEndDate = end.add(1, 'days').format('YYYY-MM-DD');
			return student.stud_id == item.user_id && student.timestamp > startDate && student.timestamp <= effectiveEndDate;
		});
		
		var totalLessonTime = 0;
		var totalNumLessons = 0;
		var totalTestTime = 0;
		
		for(var ii = 0; ii < allStudentHistory.length; ii++){
			if(typeof allStudentHistory[ii].lesson_module !== 'undefined'){
				totalLessonTime += parseInt(allStudentHistory[ii].lesson_timespent);
				totalNumLessons++;
			} else if (typeof allStudentHistory[ii].total_time_secs !== 'undefined'){
				totalTestTime += parseInt(allStudentHistory[ii].total_time_secs);
			}
		}
		
		classTableHTML += '<tr><td><a href="/user/' + item.username + '/lessons">' + item.username + '</a></td><td>' + item.email + "</td><td>" + makeSecondsReadable(totalLessonTime) + "</td><td>" + totalNumLessons + "</td><td>" + makeSecondsReadable(totalTestTime) + "</td></tr>";
	}
	
	// forEach --studentUserData-- create --studentClassMap--
	function SetupStudentClassroomMaps(item, index){
		var studentByUserID = studentHistory.filter(function(student){
			return student.stud_id == item.user_id;
		});
		studentDataComplete.set(item.user_id, studentByUserID);
		
		var studentByClassroom = studentUserData.filter(function(student){
			return student.class_id == item.class_id;
		});
		
		studentClassMap.set(item.class_id, studentByClassroom);
	}
	
	$('#ATPselect').change(function(){
		var selection = parseInt($(this).val());
		switch(selection){
			case 0:	// Lesson Practice - Time
				var ASPselection = parseInt($('#ASPselect').val());
				if(ASPselection == 83 || ASPselection == 84){
						$('#ASPselect').val(1);
				}
				$('#ASVdiv').show();
				$('#ASVdiv label.beforeInput').html('<h4><i class="fa fa-clock-o" aria-hidden="true"></i> Time:</h4>');
				$('#ASVdiv label.afterInput').html("<h4>minutes</h4>");
				
				$('option.specLesson').show();
				$('option.textTypes').hide();
				if($('#ASPselect option.specLesson:selected').length == 0){
					$('#ASPselect option:selected').prop('selected', false);
					$('#ASPselect option.specLesson[value="1"]').prop('selected', true);
				}
				//$('#ASPselect').find('option.specLesson[value="1"]').attr('selected', 'selected');
			break;
			case 1:	// Lesson Practice - Count
				$('#ASVdiv').show();
				$('#ASVdiv label.beforeInput').html('<h4><i class="fa fa-list-ol" aria-hidden="true"></i> Count:</h4>');
				$('#ASVdiv label.afterInput').html("<h4>times</h4>");
				
				$('option.specLesson').show();
				$('option.textTypes').hide();
				if($('#ASPselect option.specLesson:selected').length == 0){
					$('#ASPselect option:selected').prop('selected', false);
					$('#ASPselect option.specLesson[value="1"]').prop('selected', true);
				}
			break;
			case 2:	// Lesson Achievement
				$('#ASVdiv').hide();
				
				$('option.specLesson').show();
				$('option.textTypes').hide();
				if($('#ASPselect option.specLesson:selected').length == 0){
					$('#ASPselect option:selected').prop('selected', false);
					$('#ASPselect option.specLesson[value="1"]').prop('selected', true);
				}
			break;
			case 3: // Typing Test - time
				$('#ASVdiv').show();
				$('#ASVdiv label.beforeInput').html('<h4><i class="fa fa-clock-o" aria-hidden="true"></i> Time:</h4>');
				$('#ASVdiv label.afterInput').html("<h4>minutes</h4>");
				
				$('option.specLesson').hide();
				$('option.textTypes').show();
				if($('#ASPselect option.textTypes:selected').length == 0){
					$('#ASPselect option:selected').prop('selected', false);
					$('#ASPselect option.textTypes[value="1"]').prop('selected', true);
				}
			break;
			case 4: // Typing Test - Achievement
				$('#ASVdiv').show();
				$('#ASVdiv label.beforeInput').html('<h4><i class="fa fa-clock-o" aria-hidden="true"></i> Time:</h4>');
				$('#ASVdiv label.afterInput').html("<h4>minutes</h4>");
				
				$('option.specLesson').hide();
				$('option.textTypes').show();
				if($('#ASPselect option.textTypes:selected').length == 0){
					$('#ASPselect option:selected').prop('selected', false);
					$('#ASPselect option.textTypes[value="1"]').prop('selected', true);
				}
			break;
		}
	});
	
	$('#ASPselect').change(function(){
		var selection = parseInt($(this).val());
		if(selection == 83 || selection == 84){
			if(parseInt($('#ATPselect').val()) == 0){
				$('#ATPselect').val(1);
				$('#ATPselect').trigger("change");
			}
		}
	});
	
	$( "#ASVselect").spinner({
		mouseWheel: true,
		numberFormat: "n0",
	  spin: function( event, ui ) {
		if ( ui.value > 65000 ) {
		  $( this ).spinner( "value", 65000 );
		  return false;
		} else if ( ui.value < 1 ) {
		  $( this ).spinner( "value", 1 );
		  return false;
		}
	  },
		change: function(event, ui){
			if($(this).spinner("value") == null){
				$( this ).spinner( "value", 1 );
				return false;
			}
		}
	});

	$("#ASVselect").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false;}
	});
	
	$('#reportrange').on('apply.daterangepicker', function(ev, picker){
		
		if(picker == undefined)
			picker = $(this).data('daterangepicker');
		
		if(picker.chosenLabel == undefined){
			picker.chosenLabel = $('.dateRangeLabel').html();
		}
		
		startDate = picker.startDate.format('YYYY-MM-DD');
		endDate = picker.endDate.format('YYYY-MM-DD');
		$.post('/setData.php', { type: "us", col: 'ol_duedate_filter', val: GetTLdateFilterEnum(picker.chosenLabel)},
			function(output){
		});
		//SetupClassrooms();
		
		classesToHide = GetAssignmentTypesToHide();
		
		
		//var filteredAssignments = assignments.filter(function(assignment){
		$('.usfContainer').show();
		if(picker.chosenLabel != 'All Time'){
			var ddFilterEnd = moment(picker.endDate).add(1, 'days').format('YYYY-MM-DD');
				
			var filteredAssignments = assignments.filter(function(assignment){
				//return assignment.due_date > ddFilterStart && assignment.due_date <= ddFilterEnd;
				if(assignment.due_date > startDate && assignment.due_date <= ddFilterEnd){
					$("#assignId_" + assignment.assign_id).removeClass("dueDateFiltered");
					return;
				} else {
					$("#assignId_" + assignment.assign_id).addClass("dueDateFiltered");
				}
			});
		} else {
			$('.dueDateFiltered').removeClass("dueDateFiltered");
		}
		endDate = picker.endDate;
		$('.dueDateFiltered ' + classesToHide).hide();
		CheckIfAllAssignmentsHidden();
	});
	
	function AssignmentsToHTML(assignmentArr, isTeacher){
		assignmentHTML = '';
		currentTime = new Date();
		
		assignmentArr.forEach(function(val, key){
			retArr = GetAssignmentExtraParams(val, key, assignmentArr);
			assignmentArr[key].selection_readable = retArr[0];
			assignmentArr[key].selection_ttdl = retArr[1];
			
			AssignmentToHTML(val, key);
		});
		
		if(assignmentArr.length == 0){
			if(isTeacher){
				assignmentHTML = HTMLelemStr('Create an assignment', 'div', '', 'assignmentsNotify');
			} else {
				assignmentHTML = HTMLelemStr('You don\'t have any assignments.', 'div', '', 'assignmentsNotify');
			}
		}
		
		$('#allAssignments').html(assignmentHTML);
	}
	
	function GetAssignmentExtraParams(val, key, aArr){
		if(parseInt(val.type) <= 2){	// is lesson assignment type
			for(ii = 0; ii < lessonsArr.length; ii++){
				if(lessonsArr[ii].id == aArr[key].selection)
					break;
			}
			return [UCFirst(lessonsArr[ii].type) + ' ' + lessonsArr[ii].lesson_type + ' ' + lessonsArr[ii].lesson_num, null];
		} else {						// is test assignment type
			for(ii = 0; ii < textTypesArr.length; ii++){
				if(textTypesArr[ii].tid == aArr[key].selection)
					break;
			}
			return [textTypesArr[ii].display_text, textTypesArr[ii].type]
		}
	}
	
	
	function AssignmentToHTML(val, key){
		
		// setup theProgress and theGoal
		// input: val.progress,  val.goal, val.type
		// output: progressBarHTML, theGoalWithTxtUnits
		retArr = SetupProgressAndGoal(val.type, val.goal, val.progress);
		progressBarHTML = retArr[0];
		theGoalWithTxtUnits = retArr[1];
		
		
		// setup labels
		// input: val.type, val.selection, val.selection_ttdl, val.assign_id, userTargets[], 
		// output: assignTypeLblHTML, assignSelectionLblHTML, assignGoalLblHTML, fourthParam, assignLink
		retArr = SetupLabels(val.type, val.selection, val.selection_ttdl, val.assign_id);
		assignTypeLblHTML = retArr[0];
		assignSelectionLblHTML = retArr[1];
		assignGoalLblHTML = retArr[2];
		fourthParam = retArr[3];
		assignLink = retArr[4];
		
		
		// setup notes
		assignNotesHTML = '';
		if(val.notes != ''){
			assignNotesLblHTML = HTMLelemStr('Notes: ', 'span', '', 'assignNotesLbl');
			assignNotesHTML = HTMLelemStr(assignNotesLblHTML + val.notes, 'div', '', 'assignNotes');
		}
		
		
		// setup due date and completed date
		//		input:  
		//		output: theDDclass, theDDtoPrint, assignStatus, assignIconHTML, ddLabelHTML
		theDDclass = "assignDueDate";
		retArr = GetDateFormats(val.due_date, currentTime, true);
		theDDdate = retArr[0];
		theDDtoPrint = retArr[1];
		theDDtitle = retArr[2];
		
		retArr = GetDateFormats(val.completed_date, currentTime, false);
		theCDdate = retArr[0];
		theCDtoPrint = retArr[1];
		theCDtitle = retArr[2];
			
		assignStatus = ' currentAssignment';
		assignIconHTML = HTMLelemStr('<i class="fa fa-calendar" aria-hidden="true"></i>', 'div', '', 'assignIcon');
		
		// if negative date and a due date exists
		if(theDDtoPrint.slice(0,1) == '-' && theDDdate != null){
			theDDtoPrint = readableTimeDifference(theDDdate, currentTime) + ' ago';
			theDDclass += " dd_pastDue";
<?php if($user->isTeacher()){?>
			assignStatus = " completedAssignment";
			assignIconHTML = HTMLelemStr('<i class="fa fa-calendar-check-o" aria-hidden="true"></i>', 'div', '', 'assignIcon');
			ddLabelHTML = HTMLelemStr('Past Due', 'div', '', 'dd_label');
<?php } else { ?>
			assignStatus = " pastDue";
			assignIconHTML = HTMLelemStr('<i class="fa fa-times" aria-hidden="true"></i>', 'div', '', 'assignIcon');
			ddLabelHTML = HTMLelemStr('Due', 'div', '', 'dd_label');
<?php } ?>
		} else {
			ddLabelHTML = HTMLelemStr('Due in', 'div', '', 'dd_label');
			
			if((theDDtoPrint.includes('hr') && parseInt(theDDtoPrint.slice(0,2)) < 31) || theDDtoPrint.includes('min') || theDDtoPrint.includes('sec')){
				assignIconHTML = HTMLelemStr('<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>', 'div', '', 'assignIcon');	
				assignStatus += " almostDue";
			}
		}
		ddVerboseHTML = HTMLelemStr(theDDtitle, 'div', '', 'dd_verbose');
		
		// mark if assignment is not assigned
		if(val.is_assigned == 0){
			assignStatus = ' notAssigned';
			assignIconHTML = HTMLelemStr('<i class="fa fa-sticky-note-o" aria-hidden="true"></i>', 'div', '', 'assignIcon');
			ddLabelHTML = HTMLelemStr('Unassigned', 'div', '', 'dd_label');
			assignOnClickHTML = 'onclick="EditAssignmentSetup(\'Update and Assign\', \'Save Changes\', \'Edit Assignment\', ' + val.assign_id + ')"';
		} else {
			assignOnClickHTML = 'onclick="EditAssignmentSetup(\'Update\', \'Unassign and Save\', \'Edit Assignment\', ' + val.assign_id + ')"';
		}
		
<?php if($user->isTeacher()){?>

		tinyWPMhtml = HTMLelemStr('WPM', 'div', '', 'note');
		tinyAccHTML = HTMLelemStr('%', 'div', '', 'note');
		// setup student list
		assignStudentTableHTML = '';
		assignStudentTableTH = '<th>Progress</th><th>Last, First</th><th>Username</th><th>Email</th><th>Targets</th><th>Completed Date</th>';
		for(var ii = 0; ii < val.studentData.length; ii++){
			if(val.studentData[ii].stud_id != null){
				retArr = SetupProgressAndGoal(val.type, val.goal, val.studentData[ii].progress)
				progressBarVal = retArr[3];
				progressPercentAndVal = retArr[4];
				progressBarHTML = HTMLelemStr('', 'span', '', 'studentProgressBar', '', 'width: ' + progressBarVal + '%;');
				progressBarExtraClasses = '';
				if(progressBarVal == 100){
					progressBarExtraClasses += ' completed';
				} else if(progressBarVal > 75){
					progressBarExtraClasses += ' almostComplete';
				}
				
				retArr = GetDateFormats(val.studentData[ii].completed_date, currentTime, false);
				theJSdate = retArr[0];
				theCDtoPrint = retArr[1];
				theCDtitle = retArr[2];
				
				studentFirstName = (val.studentData[ii].firstname == null) ? ' -' : val.studentData[ii].firstname;
				studentLastName = (val.studentData[ii].lastname == null) ? '- ' : val.studentData[ii].lastname;
				
				assignStudentTableTR = HTMLelemStr(progressBarHTML + progressPercentAndVal, 'td', '', 'studentProgress' + progressBarExtraClasses);
				assignStudentTableTR += HTMLelemStr(studentLastName + ', ' + studentFirstName, 'td');
				assignStudentTableTR += HTMLelemStr('<a href="/user/' + val.studentData[ii].username + '/lessons">' + val.studentData[ii].username + '</a>', 'td','','studentUsername', val.studentData[ii].username);
				assignStudentTableTR += HTMLelemStr(val.studentData[ii].email, 'td', '', 'studentEmail', val.studentData[ii].email);
				assignStudentTableTR += HTMLelemStr(val.studentData[ii].tl_wpm_target + tinyWPMhtml + ', ' + val.studentData[ii].tl_accuracy_target + tinyAccHTML, 'td');
				assignStudentTableTR += HTMLelemStr(theCDtoPrint, 'td','','',theCDtitle);
				
				assignStudentTableHTML += HTMLelemStr(assignStudentTableTR, 'tr', '', 'studentRow' + progressBarExtraClasses);
			} else {
				assignStudentTableHTML += HTMLelemStr('<td colspan="6">No students are selected</td>', 'tr', '', 'studentRow');
			}
		}
		assignStudentTableHTML = HTMLelemStr('<div class="ASDtableLabel"><h5>Student Progress</h5></div><table><tr>' + assignStudentTableTH + '</tr>' + assignStudentTableHTML + '</table>', 'div', '', 'assignStudentData')
		
		ctaInsideDeleteHTML = HTMLelemStr('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', 'div', '', 'assignCTAtxt deleteCTAtxt','','','DeleteAssignment(' + val.assign_id + ',\'' + htmlEntities(val.title) + '\');');
		ctaInsideHTML = HTMLelemStr('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit', 'div', '', 'assignCTAtxt editCTAtxt');
		ctaInsideDuplicateHTML = HTMLelemStr('<i class="fa fa-clone" aria-hidden="true"></i> Duplicate', 'div', '', 'assignCTAtxt duplicateCTAtxt','Duplicated assignment will always be unassigned','','DuplicateAssignment(' + val.assign_id + ')');
				
		containerStartHTML = '<div id="assignId_' + val.assign_id + '" class="usfContainer stoInfoBox' + assignStatus + '"' + assignOnClickHTML + '>';
		
		theTitle = (val.title == "") ? '&lt;Untitled&gt;' : val.title;
			
		assignmentHTML += containerStartHTML + HTMLelemStr(theTitle, 'h4', '','') 
									+ assignIconHTML  
									+ '<div class="assignmentBody">' 
										+ HTMLelemStr(assignTypeLblHTML + 'Typing ' + val.activity, 'div','','assignActivity') 
										+ HTMLelemStr(assignSelectionLblHTML + val.selection_readable, 'div', '', 'selectionReadable') 
										+ HTMLelemStr(assignGoalLblHTML + theGoalWithTxtUnits, 'div') 
										+ fourthParam
										+ assignNotesHTML
									+ '</div>' 
									+ HTMLelemStr(ddLabelHTML + theDDtoPrint + ddVerboseHTML, 'div', '', theDDclass, theDDtitle) 
									+ assignStudentTableHTML
									+ HTMLelemStr(ctaInsideDeleteHTML + ctaInsideHTML + ctaInsideDuplicateHTML, 'div', '', 'assignCTA')
								+ '</div>';
<?php } else if($user->isStudent()){?>
		
		
		if(val.completed_date == "0000-00-00 00:00:00" || (theCDdate > theDDdate)){
			
			// setup CTA
			ctaInsideHTML = HTMLelemStr('Go Do Assignment <i class="fa fa-arrow-right" aria-hidden="true"></i>', 'div', '', 'assignCTAtxt');

			
			
			containerStartHTML = '<div id="assignId_' + val.assign_id + '" class="usfContainer stoInfoBox' + assignStatus + '"><a href="' + assignLink + '">';
			
			assignmentHTML += containerStartHTML + HTMLelemStr(val.title, 'h4', '','') 
										+ assignIconHTML  
										+ '<div class="assignmentBody">' 
											+ HTMLelemStr(assignTypeLblHTML + 'Typing ' + val.activity, 'div','','assignActivity') 
											+ HTMLelemStr(assignSelectionLblHTML + val.selection_readable, 'div', '', 'selectionReadable') 
											+ HTMLelemStr(assignGoalLblHTML + theGoalWithTxtUnits, 'div') 
											+ fourthParam
											+ assignNotesHTML
										+ '</div>' 
										+ HTMLelemStr(ddLabelHTML + theDDtoPrint + ddVerboseHTML, 'div', '', theDDclass, theDDtitle) 
										+ HTMLelemStr(ctaInsideHTML, 'div', '', 'assignCTA')
										+ progressBarHTML
									+ '</a></div>';
		} else {
			// setup due date for completed assignments
			if(theDDtoPrint.indexOf('ago') == -1)
				theDDtoPrint = 'in ' + theDDtoPrint;
			
			cdLabelHTML = HTMLelemStr('Completed', 'div', '', 'cd_label');
			containerStartHTML = '<div id="assignId_' + val.assign_id + '" class="usfContainer stoInfoBox completedAssignment">';
			assignIconHTML = HTMLelemStr('<i class="fa fa-check" aria-hidden="true"></i>', 'div', '', 'assignIcon');
			completedDDlblHTML = HTMLelemStr('Due', 'span');
			completedDDhtml = HTMLelemStr(completedDDlblHTML + theDDtoPrint + ddVerboseHTML, 'div', '', 'assignMainBodyDD', theDDtitle)
			
			// setup completed date
			cdVerboseHTML = HTMLelemStr(theCDtitle, 'div', '', 'cd_verbose');
			
			
			assignmentHTML += containerStartHTML + HTMLelemStr(val.title, 'h4', '','') 
										+ assignIconHTML 
										+ '<div class="assignmentBody">' 
											+ HTMLelemStr(assignTypeLblHTML + 'Typing ' + val.activity, 'div','','assignActivity') 
											+ HTMLelemStr(assignSelectionLblHTML + val.selection_readable, 'div', '', 'selectionReadable') 
											+ HTMLelemStr(assignGoalLblHTML + theGoalWithTxtUnits, 'div') 
											+ fourthParam
											+ completedDDhtml 
											+ assignNotesHTML
										+ '</div>'
										+ HTMLelemStr(cdLabelHTML + theCDtoPrint + cdVerboseHTML, 'div', '', theDDclass, theCDtitle)
									+ '</div>';
			
		}
<?php } ?>		
	}
	
	// ===================================================================================================
	// DATE SETUP
	//		input:  valDate, theCurrentTime, isDueDate
	//		output: jsDate, dateToPrint, dateTitle
	function GetDateFormats(valDate, theCurrentTime, isDueDate=false){
		if(valDate == undefined){
			return [null, '-', 'No due date'];
		}
		jsDate = new Date(valDate + ' UTC');
		dateTitle = moment(jsDate).format('ddd MMMM D, YYYY @ h:mm A');
		if(isDueDate){
			dateToPrint = (valDate == "0000-00-00 00:00:00") ? "-" : readableTimeDifference(theCurrentTime, jsDate);
		} else {
			dateToPrint = (valDate == "0000-00-00 00:00:00") ? "-" : readableTimeDifference(jsDate, theCurrentTime) + ' ago';
		}
		
		return[jsDate, dateToPrint, dateTitle];
	}
	
	// ===================================================================================================
	// SETUP theProgress and theGoal
	//		notes: this function is shared by teacher & student
	// 		input: val.progress,  val.goal, val.type
	// 		output: progressBarHTML, theGoalWithTxtUnits, theProgress, progressBarVal
	function SetupProgressAndGoal(valType, valGoal, valProgress){
		if(valType == 0 || valType == 3 || valType == 4){
			progressConverted = Math.round((parseInt(valProgress) / 60)*100)/100;
			theProgress = progressConverted + " mins";
			goalConverted = Math.round(valGoal / 60);
			theGoalWithTxtUnits = goalConverted + " mins";
		} else {
			progressConverted = valProgress;
			theProgress = valProgress + " times";
			goalConverted = valGoal;
			theGoalWithTxtUnits = goalConverted + " times";
		}
		
		if(valProgress == null){
			theProgress = "Not Started";
			progressBarVal = 0;
			progressPercentAndVal = theProgress;
			progressBarValueHTML = HTMLelemStr(theProgress, 'div', '', 'PBval', '', '');
			progressBarBarHTML = HTMLelemStr('', 'div', '', 'PBbar', '', 'width: 0%;');
			progressBarHTML = HTMLelemStr(progressBarBarHTML + progressBarValueHTML, 'div', '', 'assignProgress notStarted');
		} else {
			progressBarVal = Math.floor((progressConverted/goalConverted) * 100);
			progressBarVal = (progressBarVal > 100) ? 100 : progressBarVal;
			progressPercentAndVal = progressBarVal + '% (' + theProgress + ')';
			progressBarValPosition = (progressBarVal >= 24) ? progressBarVal - 22 : 5;
			progressBarValPosition = (progressBarValPosition < 5) ? 5 : progressBarValPosition;
			progressBarValPosition = (progressBarValPosition >= 73) ? 73 : progressBarValPosition;
			progressBarValueHTML = HTMLelemStr(progressBarVal + '% (' + progressConverted + ' / ' + theGoalWithTxtUnits + ')', 'div', '', 'PBval', '', 'left: ' + progressBarValPosition + '%;');
			progressBarBarHTML = HTMLelemStr('', 'div', '', 'PBbar', '', 'width: ' + progressBarVal + '%;');
			progressBarHTML = HTMLelemStr(progressBarBarHTML + progressBarValueHTML, 'div', '', 'assignProgress started');
		}
		
		return [progressBarHTML, theGoalWithTxtUnits, theProgress, progressBarVal, progressPercentAndVal];
	}
	
	// ===================================================================================================
	// SETUP LABELS
	// 		input: val.type, val.selection, val.selection_ttdl, val.assign_id, userTargets[], 
	// 		output: assignTypeLblHTML, assignSelectionLblHTML, assignGoalLblHTML, fourthParam, assignLink
	function SetupLabels(valType, valSelection, valSelection_ttdl, valAssign_id){
		tinyWPMhtml = HTMLelemStr('WPM', 'div', '', 'note');
		tinyAccHTML = HTMLelemStr('%', 'div', '', 'note');
		if(valSelection == '83' || valSelection == '84'){
			pageLink = 'keyboard-basics';
		} else {
			pageLink = 'typing-tutor';
		}
		// setup labels
		switch(parseInt(valType)){
			case 0:
				assignTypeLblHTML = HTMLelemStr('Do', 'span');
				assignSelectionLblHTML = HTMLelemStr('On', 'span');
				assignGoalLblHTML = HTMLelemStr('For at least', 'span');
				fourthParam = '';
				assignLink = '/' + pageLink + '?mod=' + valSelection + '&sa_id=' + valAssign_id;
			break;
			case 1:
				assignTypeLblHTML = HTMLelemStr('Complete', 'span');
				assignSelectionLblHTML = HTMLelemStr('On', 'span');
				assignGoalLblHTML = HTMLelemStr('At least', 'span');
				fourthParam = '';
				assignLink = '/' + pageLink + '?mod=' + valSelection + '&sa_id=' + valAssign_id;
			break;
			case 2:
				assignTypeLblHTML = HTMLelemStr('Complete', 'span');
				assignSelectionLblHTML = HTMLelemStr('On', 'span');
				assignGoalLblHTML = HTMLelemStr('At least', 'span');
				fourthParamLblHTML = HTMLelemStr('With at least', 'span');
<?php if($user->isTeacher()){?>
				bothUserTargets = 'Use Students\' Targets';
<?php } else { ?>
				bothUserTargets = userTargets["tl_wpm_target"] + tinyWPMhtml + ' ' + userTargets["tl_accuracy_target"] + tinyAccHTML;
<?php } ?>
				fourthParam = HTMLelemStr(fourthParamLblHTML + bothUserTargets, 'div');
				assignLink = '/' + pageLink + '?mod=' + valSelection + '&sa_id=' + valAssign_id;
			break;
			case 3:
				assignTypeLblHTML = HTMLelemStr('Do', 'span');
				assignSelectionLblHTML = HTMLelemStr('Text type', 'span');
				assignGoalLblHTML = HTMLelemStr('For at least', 'span');
				fourthParam = '';
				assignLink = '/typing-test?ttdl=' + valSelection_ttdl + '&sa_id=' + valAssign_id;
			break;
			case 4:
				assignTypeLblHTML = HTMLelemStr('Do', 'span');
				assignSelectionLblHTML = HTMLelemStr('Text type', 'span');
				assignGoalLblHTML = HTMLelemStr('At least', 'span');
				fourthParamLblHTML = HTMLelemStr('With at least', 'span');
				bothUserTargets = userTargets["tl_wpm_target"] + tinyWPMhtml + ' ' + userTargets["tl_accuracy_target"] + tinyAccHTML;
				fourthParam = HTMLelemStr(fourthParamLblHTML + bothUserTargets, 'div');
				assignLink = '/typing-test?ttdl=' + valSelection_ttdl + '&sa_id=' + valAssign_id;
			break;
			default:
				assignTypeLblHTML = HTMLelemStr('Default', 'span');
				assignSelectionLblHTML = HTMLelemStr('Default', 'span');
				assignGoalLblHTML = HTMLelemStr('With at least', 'span');
				assignLink = '/typing-tutor';
			break;
		}
		
		return [assignTypeLblHTML, assignSelectionLblHTML, assignGoalLblHTML, fourthParam, assignLink];
	}
	
	function EditAssignmentSetup(actionBtnTxt, saveBtnTxt, dialogTitle, theAssignId){
		assignmentFound = false;
		for(ii = 0; ii < studentsGroupedByAssignment.length; ii++){
			if(studentsGroupedByAssignment[ii].assign_id == theAssignId){
				assignmentFound = true;
				break;
			}
		}
		
		if(assignmentFound){
			CreateOrEditAssignment(actionBtnTxt, saveBtnTxt, dialogTitle, studentsGroupedByAssignment[ii]);
		} else {
			return 'Assignment not found';
		}
	}
	
	
	function CreateOrEditAssignment(actionBtnTxt, saveBtnTxt, dialogTitle, assignmentData){
		$( "#dialog-create-assignment" ).dialog({
		  resizable: true,
		  height:650,
		  width:1050,
		  modal: true,
		  draggable: true,
		  buttons: [
			{
				text: actionBtnTxt,
				class: "mainAction",
				click: function() {
					// remove duplicates
					var ssarr = selectedStudents.slice().sort(function(a,b){return a > b}).reduce(function(a,b){if (a.slice(-1)[0] !== b) a.push(b);return a;},[]);
					var assign_id = (this.className.length > 9) ? this.className.substr(9) : '';
					
					var ddWformating = new Date(dueDate + ' UTC');
					ddWformating = moment(ddWformating);
					ddWformating = ddWformating.format('dddd MMMM D, YYYY @ h:mm A');
					
					$.post('/setData.php', { type: "ol_sa", ssa: ssarr, tit: $('#assignmentTitle').val(), note: $('#assignmentNotes').val(), atp: $('#ATPselect').val(), asp: $('#ASPselect').val(), asv: $('#ASVselect').val(), add: dueDate, aid: assign_id, ddwf: ddWformating},
						function(output){
							var temp = "test";
							temp += "ing";
							window.location.reload();
					});
					$( this ).dialog( "close" );
				}			
			}, {
				text: saveBtnTxt,
				click: function() {// remove duplicates
					if(selectedStudents != undefined){
						var ssarr = selectedStudents.slice().sort(function(a,b){return a > b}).reduce(function(a,b){if (a.slice(-1)[0] !== b) a.push(b);return a;},[]);
					}
					var assign_id = (this.className.length > 9) ? this.className.substr(9) : '';
					
					var ddWformating = new Date(dueDate + ' UTC');
					ddWformating = moment(ddWformating);
					ddWformating = ddWformating.format('dddd MMMM D, YYYY @ h:mm A');
					
					$.post('/setData.php', { type: "ol_sa", ssa: ssarr, tit: $('#assignmentTitle').val(), note: $('#assignmentNotes').val(), atp: $('#ATPselect').val(), asp: $('#ASPselect').val(), asv: $('#ASVselect').val(), add: dueDate, aid: assign_id, ddwf: ddWformating, mode: 'save'},
						function(output){
							var temp = "test";
							temp += "ing";
							window.location.reload();
					});
					  $( this ).dialog( "close" );
					}
			}, {
				text: "Cancel",
				click: function() {
					  $( this ).dialog( "close" );
					}
			}
		  ],
		  open: function() {
			  $('.ui-dialog-title').html(dialogTitle);
			  
				if(assignmentData != null){
					$('#dialog-create-assignment').removeClass();
					$('#dialog-create-assignment').addClass('assignId_' + assignmentData.assign_id);
					$('#assignmentTitle').val($('<textarea/>').html(assignmentData.title).text());
					$('#assignmentNotes').val(assignmentData.notes);
					$('#ATPselect').val(assignmentData.type);
					$('#ATPselect').trigger("change");
					
					if(assignmentData.activity == "Test"){
						$('#ASPselect .textTypes[value="' + assignmentData.selection + '"]').prop('selected', true)
					} else {
						$('#ASPselect .specLesson[value="' + assignmentData.selection + '"]').prop('selected', true);
					}
					$('#ASPselect').trigger("change");
					
					if(assignmentData.due_date != null){
						var jsDD = new Date(assignmentData.due_date + ' UTC');
						dueDate = jsDD.toISOString().slice(0,19).replace('T',' ');
						assignMoment = moment(jsDD);
						$('#ADDpicker span').html(assignMoment.format('ddd MMMM D, YYYY @ h:mm A'));
						$('#ADDpicker').data('daterangepicker').setStartDate(assignMoment);
						$('#ADDpicker').data('daterangepicker').setEndDate(assignMoment);
					}
					
					var goalToDisplay = assignmentData.goal;
					if(assignmentData.type != 1 && assignmentData.type != 2)
						goalToDisplay /= 60;
					
					$('#ASVselect').val(goalToDisplay);
					
					$('[id^=delete_selected_]').click();
					
					var checkboxList = $('[class^=group-checkbox]');
					for(var ii = 0; ii < assignmentData.studentData.length; ii++){
						$('input[type=checkbox][value=' + assignmentData.studentData[ii].stud_id + ']').prop('checked',true)
					}
					$('[id^=add_selected_]').click();
					
				} else {
					$('#dialog-create-assignment').removeClass();
					$('#assignmentTitle').val('Assignment');
					$('#assignmentNotes').val('');
					$('#ATPselect').val(0);
					$('#ATPselect').trigger("change");
					
					$('#ASPselect .specLesson[value="1"]').prop('selected', true);
					$('#ASPselect').trigger("change");
					
					$('#ASVselect').val(2);
					
					assignMoment = moment().endOf('week').subtract(1, 'days');
					$('#ADDpicker span').html(assignMoment.format('ddd MMMM D, YYYY @ h:mm A'));
					$('#ADDpicker').data('daterangepicker').setStartDate(assignMoment);
					$('#ADDpicker').data('daterangepicker').setEndDate(assignMoment);
					
					$('[id^=delete_selected_]').click();
				}
			  
			  
			  $(this).parent().find(":contains('Cancel')").focus();
			  $(this).parent().find(":contains('Create')").blur();
			  if(dueDate == undefined)
				$(".ui-dialog-buttonset button:contains('Create')").button("disable")
		  }
		});
	}
	
	function DeleteAssignmentDialog(assign_id, assignTitle, delButtonText){
		$('#extraInfo').html("You are deleting assignment: <div id=\"assignTitle\"><textarea style=\"border:none; width:100%; text-align:center; font-weight: 800; font-family:'Roboto'; outline:none; resize:none;\">" + assignTitle + "</textarea></div>");
		$( "#dialog-confirm" ).dialog({
		  resizable: false,
		  height:370,
		  width:590,
		  modal: true,
		  draggable: true,
		  buttons: [
			{
				text: delButtonText,
				click: function() {
					$( this ).dialog( "close" );
					$.post('/setData.php', { type: 'ol_da', aid: assign_id},
						function(output){
							window.location.reload();
					});
				}			
			}, {
				text: "Cancel",
				click: function() {
					  $( this ).dialog( "close" );
					}
			}
		  ],
		  open: function() {
			  $(this).parent().find(":contains('Cancel')").focus();
			  $(this).parent().find(":contains('Delete')").blur();
		  }
		});
	}

	function DeleteAssignment(assign_id, assignTitle){
		DeleteAssignmentDialog(assign_id, assignTitle, 'Delete Assignment');
		
		event.cancelBubble = true;
		return false;
	}

	function DuplicateAssignment(assign_id){

		foundAssignment = false;
		for(var ii = 0; ii < studentsGroupedByAssignment.length; ii++){
			if(studentsGroupedByAssignment[ii].assign_id == assign_id){
				foundAssignment = true;
				assign_index = ii;
				break;
			}
		}
		
		if(foundAssignment){
			aData = studentsGroupedByAssignment[assign_index];
			ssarr = new Array();
			for(var ii = 0; ii < aData.studentData.length; ii++){
				ssarr.push(aData.studentData[ii].stud_id);
			}
			
			theMode = 'save';		// always make duplicate unassigned

			if(aData.type != 1 && aData.type != 2){
				goalInBaseUnits = aData.goal / 60;
			} else {
				goalInBaseUnits = aData.goal;
			}
			
			$.post('/setData.php', { type: "ol_sa", ssa: ssarr, tit: aData.title, note: aData.notes, atp: aData.type, asp: aData.selection, asv: goalInBaseUnits, add: aData.due_date, aid: '', mode: theMode},
				function(output){
					window.location.reload();
			});
		}
		
		event.cancelBubble = true;
		return false;
	}
	
	function RefreshAssignments(){
		$.post('/getData.php', { type: "ol_ad"},
			function(output){
				myJSONdata = $.parseJSON(output);
				assignments = myJSONdata["assignments"];
		});
	}
	
	function GetAssignmentTypesToHide(){
		classesToHideStr = '';
		
		if(!$('#caLbl').hasClass('ui-state-active')){
			classesToHideStr += '.currentAssignment';
		}
		
		if(!$('#uaLbl').hasClass('ui-state-active')){
			classesToHideStr += ', .notAssigned';
		}

		if(!$('#paLbl').hasClass('ui-state-active')){
			classesToHideStr += ', .completedAssignment';
		}
		
		if(!$('#pdaLbl').hasClass('ui-state-active')){
			classesToHideStr += ', .pastDue';
		}
		return classesToHideStr;
	}
	
	
	function SetTypeFilterBit(valBitwise, theMask, unsetBit){
		if(unsetBit){
			theNotMask = ~theMask;
			valBitwise = valBitwise & theNotMask;
		} else {
			valBitwise = valBitwise | theMask;
		}
		return valBitwise;
	}
	
	function GetBitMaskByTypeClassName(typeClassName){
		if(typeClassName == 'currentAssignment'){
			return 0b0001;
		} else if(typeClassName == 'completedAssignment'){
			return 0b0010;
		} else if(typeClassName == 'pastDue'){
			return 0b0100;
		} else if(typeClassName == 'notAssigned'){
			return 0b1000;
		} else {
			return 0;
		}
	}
	
	function InitializeTypeFilters(){
		typeFilterBitwiseInitVal = typeFilterBitwise;
		
		if((GetBitMaskByTypeClassName($('#uaLbl').data('assignClass')) & typeFilterBitwise) != 0){
			$('#uaLbl').click();
		}
		
		if((GetBitMaskByTypeClassName($('#caLbl').data('assignClass')) & typeFilterBitwise) != 0){
			$('#caLbl').click();
		}
		
		if((GetBitMaskByTypeClassName($('#paLbl').data('assignClass')) & typeFilterBitwise) != 0){
			$('#paLbl').click();
		}
		
		if(((GetBitMaskByTypeClassName($('#pdaLbl').data('assignClass')) & typeFilterBitwise) != 0) || typeFilterBitwiseInitVal == null){
			$('#pdaLbl').click();
		}
		
	}
	
	function CheckIfAllAssignmentsHidden(){
		if($('#allAssignments').children(':visible').length == 0){
			$('#allAssignmentsHiddenWarning').show();
		} else {
			$('#allAssignmentsHiddenWarning').hide();
		}
	}
	
</script>