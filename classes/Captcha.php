<?php
class Captcha{
	private $_db,
			$_timeFrameMins = 10,
			$_captchaAt = 200;
	
	public function __construct(){
		$this->_db = DB::getInstance();
	}
	
	public function addFailedLoginAttempt(){
		$timestamp = date('Y-m-d H:i:s');
		
		// cleanup db table about 1/10 calls
		if(rand(1, 10) == 5){
			$this->cleanupDB();
		}
		
		return $this->_db->insert('faild_logns', array('attempted_at' => $timestamp, 'unix_attempted_at' => strtotime($timestamp)));
	}
	
	public function failedAttemptCount(){
		return $this->_db->get('faild_logns', array('unix_attempted_at', '>', strtotime('-' . $this->_timeFrameMins . ' minutes')))->count();
	}
	
	public function getCaptcha(){
		$url = 'http://api.textcaptcha.com/stotest_tk.json';
		$captcha = json_decode(file_get_contents($url),true); 
		if (!$captcha) {
		 $captcha = array( // fallback challenge
		  'q'=>'Is ice hot or cold?',
		  'a'=>array(md5('cold'))
		 );
		 // + capture error info & log
		}
		return $captcha;
	}
	
	public function doCaptcha(){
		if($this->failedAttemptCount() > $this->_captchaAt){
			return true;
		} else{
			return false;
		}
	}
	
	public function passedCaptcha($userAns, $capSessionVarName){
		$capAns = '';
		$captchaPass = true;
		if(Session::exists($capSessionVarName)){
			$capAns = Session::get($capSessionVarName);
		}
		
		if($capAns != ''){
			if(!in_array(md5(strtolower(trim($userAns))), $capAns)){
				$captchaPass = false;
			}
		} 
		return $captchaPass;
	}
	
	
	public function cleanupDB(){
		$this->_db->delete('faild_logns', array('unix_attempted_at', '<', strtotime('-' . $this->_timeFrameMins . ' minutes')));
	}
	
	
}