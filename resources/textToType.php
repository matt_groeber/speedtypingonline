<?php echo header('Content-Type: text/xml'); ?>
<?php echo '<?xml version="1.0" encoding="ISO-8859-1"?>'; ?>
<!--
    Document   : textToType.xml
    Created on : June 8, 2011, 10:41 AMF
    Author     : Matt
    Description:
        Purpose of the document is to store text to type
        
        
Notepad++ Replace:
    -extended search: use "\r\n" to find CR and LF
    -extended search: find ".\r\n" and replace with ". "
    -extended search: find ",\r\n" and replace with ", "
    -extended search: find "\r\n" and replace with ". "
    -regex: put periods at the end of each newline by finding "[a-zA-Z]$" and replacing with ".$"
    
    
    -TO CLEANUP TEXT in Notepad++:
    (rids text of Carriage Returns and Line feeds and put periods at end of each new line)
        1)find ".\r\n" and replace with ". "
        2)find ",\r\n" and replace with ", "
        3)find "?\r\n" and replace with "? "
        4)find "!\r\n" and replace with "! "
        5)find ")\r\n" and replace with ") "
        6)find "\r\n\r\n" and replace with ". "
        7)find "\r\n" and replace with ". "
        8)make sure there is a period at the end
        9)find "&" and replace with "&amp;"
       10)find "<" and replace with "&lt;"
       11)find ">" and replace with "&gt;"
       
 Basic Cleanup:
     - ampersand "&" replace with &amp;
     - "<" replace with &lt;
     - ">" replace with &gt;
     - ".[A-Z]" replace with ". [A-Z]"
     - "  " (double-space) replace with " " (single-space)
     - "-" replace with hyphen "-"
     - "�" replace with "e"
	 - "�" replace with "e"
     - "�" replace with "\'"
     - "�" replace with "a"
     
-->

<root>
    <ENTRY>
        <BOOK>
            <TITLE>A Tale of Two Cities</TITLE>
            <AUTHOR>Charles Dickens</AUTHOR>
            <TYPE>Book</TYPE>
            <TEXT>It was the best of times, it was the worst of times, it was the age of wisdom, it was the age of foolishness, it was the epoch of belief, it was the epoch of incredulity, it was the season of light, it was the season of darkness, it was the spring of hope, it was the winter of despair, we had everything before us, we had nothing before us, we were all going direct to heaven, we were all going direct the other way. In short, the period was so far like the present period, that some of its noisiest authorities insisted on its being received, for good or for evil, in the superlative degree of comparison only. There were a king with a large jaw and a queen with a plain face, on the throne of England; there were a king with a large jaw and a queen with a fair face, on the throne of France. In both countries it was clearer than crystal to the lords of the State preserves of loaves and fishes, that things in general were settled for ever. It was the year of Our Lord one thousand seven hundred and seventy-five. Spiritual revelations were conceded to England at that favoured period, as at this. Mrs. Southcott had recently attained her five and twentieth blessed birthday, of whom a prophetic private in the life guards had heralded the sublime appearance by announcing that arrangements were made for the swallowing up of London and Westminster. Even the Cock-lane ghost had been laid only a round dozen of years, after rapping out its messages, as the spirits of this very year last past (supernaturally deficient in originality) rapped out theirs. Mere messages in the earthly order of events had lately come to the English Crown and People, from a congress of British subjects in America: which, strange to relate, have proved more important to the human race than any communications yet received through any of the chickens of the Cock-lane brood.</TEXT>
        </BOOK>
    </ENTRY>
    <ENTRY>
        <BOOK>
            <TITLE>The Iliad</TITLE>
            <AUTHOR>Homer</AUTHOR>
            <TYPE>Book</TYPE>
            <TEXT>Sing, O goddess, the anger of Achilles son of Peleus, that brought countless ills upon the Achaeans. Many a brave soul did it send hurrying down to Hades, and many a hero did it yield a prey to dogs and vultures, for so were the counsels of Jove fulfilled from the day on which the son of Atreus, king of men, and great Achilles, first fell out with one another. And which of the gods was it that set them on to quarrel? It was the son of Jove and Leto; for he was angry with the king and sent a pestilence upon the host to plague the people, because the son of Atreus had dishonoured Chryses his priest. Now Chryses had come to the ships of the Achaeans to free his daughter, and had brought with him a great ransom: moreover he bore in his hand the sceptre of Apollo wreathed with a suppliant's wreath, and he besought the Achaeans, but most of all the two sons of Atreus, who were their chiefs. "Sons of Atreus," he cried, "and all other Achaeans, may the gods who dwell in Olympus grant you to sack the city of Priam, and to reach your homes in safety; but free my daughter, and accept a ransom for her, in reverence to Apollo, son of Jove." On this the rest of the Achaeans with one voice were for respecting the priest and taking the ransom that he offered; but not so Agamemnon, who spoke fiercely to him and sent him roughly away. "Old man," said he, "let me not find you tarrying about our ships, nor yet coming hereafter. Your sceptre of the god and your wreath shall profit you nothing. I will not free her. She shall grow old in my house at Argos far from her own home, busying herself with her loom and visiting my couch; so go, and do not provoke me or it shall be the worse for you." The old man feared him and obeyed. Not a word he spoke, but went by the shore of the sounding sea and prayed apart to King Apollo whom lovely Leto had borne. "Hear me," he cried, "O god of the silver bow, that protectest Chryse and holy Cilla and rulest Tenedos with thy might, hear me oh thou of Sminthe. If I have ever decked your temple with garlands, or burned your thigh-bones in fat of bulls or goats, grant my prayer, and let your arrows avenge these my tears upon the Danaans."</TEXT>
        </BOOK>
    </ENTRY>
    <ENTRY>
        <SHORT_STORY>
            <TITLE>The Old Man with the Two Black Dogs - Arabian Nights</TITLE>
            <AUTHOR>Anonymous</AUTHOR>
            <TYPE>Short Story</TYPE>
            <TEXT>Long time back, ours was a happy and prosperous family. We were three brothers who loved each other very much. In due time, our old father became very ill. He left for heaven and willed each of us a thousand gold dinars. We were clever so we invested in various shops and soon became rich merchants. One day my eldest brother had the idea of expanding his trade connections in other kingdoms. He decided to travel to other lands across the sea for this purpose. He then sold all his shops, luxury items and the house to get some money. For buying a variety of goods, he set sail in a beautiful trade ship. Nearly a year went by, but we heard nothing from him. One afternoon, as I sat fanning myself at the shop, a beggar approached me. He was looking very weak. He was barely covered in tattered clothes. I picked a silver coin from my pocket and gave it to him. Seeing this, the poor beggar burst out in tears. "Oh! What fate!" he cried bitterly. "A brother is giving another alms in pity." I gave him a second look. Suddenly I recognised him to be my eldest brother. I consoled him and took him home. After a warm bath and some delicious hot lunch, my brother told me his sad tale. He said that he could not earn as much as he had invested in his new ventures. The heavy losses had made him poor and he had reached back home with great difficulty. I had by then earned two thousand gold dinars in my business so I gave one thousand gold dinars to my eldest brother. I encouraged him to start a new business. Some months later, my second brother decided to seek foreign lands to expand his business. I narrated our eldest brother's example to him but he insisted on going for trading overseas. He soon joined a caravan that was ready to leave for a foreign land. He, too, went with many hopes and a lot of goods. For a year, I heard no news of his business ventures. When a year went by, one fine morning he arrived at my doorstep much in the same state as my elder brother. He told me that his caravan had been looted by bandits and he lost everything. Once again I lent my thousand dinars to my other brother too. He was happy to begin his business again. Soon both my brothers did well in their ventures and prospered. We lived happily and together again. One fine morning, both my brothers came to me and said, "Brother, all three of us must go on a long journey to expand our business. We'll trade together and amass wealth." I refused because I had seen my brothers becoming penniless after such adventurous business trips. But they persisted. After refusing their request for nearly five years, I gave in. After making necessary arrangements, the three of us bought grand goods to sell. My brothers spent all their money to buy the goods. I, thus, took six thousand dinars that I had and gave them a thousand dinars each. I kept one thousand dinars for my use. Then I dug a safe hole in my house and buried the three thousand dinars that I was left with. Then we loaded the goods on a large ship and set sail. Nearly two months after sailing, we anchored at a port. We made a lot of money by trading there. When we got ready to leave, a beautiful but poor woman approached me. She bowed to me and kissed my hand. Then she said, "Sir, please be kind enough to accept me as your wife. I have nobody to care for me and nowhere to stay." I was taken aback. I said, "Dear woman, I don't even know you. How can you expect me to marry a stranger?" The woman pleaded tearfully and persuaded me to take her as my wife. She promised to be loyal and loving and soon we were married after the required arrangements were made. As we set sail, she took on the role of a caring wife. She was soft-spoken, hard working and always ready to serve me or help my brothers. I was very happy to have her as a wife. My happiness was not favoured by my brothers who grew jealous day by day. Their resentment took shape of a plot to kill me and my wife. Thus, one night, as my wife and I were in deep sleep, my two brothers threw us aboard. My wife who was a fairy used her powers to save both of us. Soon I found ourselves on an island. Then my wife said, "Dear, I am a fairy. I married you for I saw a kind man who would be a fit husband for me. You have taken good care of me but I am very hurt and angry at the way your ungrateful brothers have treated you. I'll punish them by sinking their ship." I was horrified. "Please don't do that. After all they are my brothers. Let's forgive and forget." But nothing could stop my angry fairy wife. She declared that her fury would end only after she avenged herself. Then she chanted some magic words! I stood before my house in my hometown. My fairy wife was by my side. I opened the door to welcome her into her new home. I saw two sinister black dogs just inside the door. I was surprised. "Dear, I don't know where these black dogs came from. I never had any pets either." I explained. "I know, dear," my fairy wife said. "These black dogs are your own ungrateful brothers. I changed them into the black dogs to punish them. Now you can treat them anyway you wish. I must take your leave now. The spell cast by me will last for ten years. You can contact me after that time." My fairy wife told me where her home was and vanished into thin air. Now ten years have passed. I am leading the black dogs in search of my fairy wife. "Now, Oh! Genie, you must not have ever heard of such a wonderous, unbelievable tale. I ask you to grant a third of the merchant's life in return of this tale." The genie agreed again. Then the third old man said, "I'll tell you a stranger and fantastic story of all." He then told such a tale full of unbelievable events and wonderous magic that the genie declared to spare a third of the merchant's punishment. Thus, the genie departed. The merchant and the three men then went on their journeys. The merchant reached home and was received happily by his family. Then he told the strange tale of how he was freed.</TEXT>
        </SHORT_STORY>
    </ENTRY>
    <ENTRY>
        <SHORT_STORY>
            <TITLE>The Donkey, the Ox, and the Labourer - Arabian Nights</TITLE>
            <AUTHOR>Anonymous</AUTHOR>
            <TYPE>Short Story</TYPE>
            <TEXT>A very wealthy merchant possessed several country houses, where he kept a large number of cattle of every kind. He retired with his wife and family to one of these estates, in order to improve it under his own direction. He had the gift of understanding the language of beasts, but with this condition, that he should not, on pain of death, interpret it to any one else. And this hindered him from communicating to others what he learned by means of this faculty. He kept in the same stall an ox and a donkey. One day as he sat near them, and was amusing himself in looking at his children who were playing about him, he heard the ox say to the donkey, "Sprightly, O! how happy do I think you, when I consider the ease you enjoy, and the little labour that is required of you. You are carefully rubbed down and washed, you have well-dressed corn, and fresh clean water. Your greatest business is to carry the merchant, our master, when he has any little journey to make, and were it not for that you would be perfectly idle. I am treated in a very different manner, and my condition is as deplorable as yours is fortunate. Daylight no sooner appears than I am fastened to a plough, and made to work till night, which so fatigues me, that sometimes my strength entirely fails. Besides, the labourer, who is always behind me, beats me continually. By drawing the plough, my tail is all flayed; and in short, after having laboured from morning to night, when I am brought in they give me nothing to eat but sorry dry beans, not so much as cleansed from dirt, or other food equally bad; and to heighten my misery, when I have filled my belly with such ordinary stuff, I am forced to lie all night in my own dung: so that you see I have reason to envy your lot. "The donkey did not interrupt the ox; but when he had concluded, answered, "They that called you a foolish beast did not lie. You are too simple; you suffer them to conduct you whither they please, and shew no manner of resolution. In the mean time, what advantage do you reap from all the indignities you suffer." You kill yourself for the ease, pleasure, and profit of those who give you no thanks for your service. But they would not treat you so, if you had as much courage as strength. When they come to fasten you to the stall, why do you not resist? why do you not gore them with your horns, and shew that you arc angry, by striking your foot against the ground? And, in short, why do not you frighten them by bellowing aloud? Nature has furnished you with means to command respect; but you do not use them. They bring you sorry beans and bad straw; eat none of them, only smell and then leave them. If you follow my advice, you will soon experience a change, for which you will thank me."The ox took the donkey's advice in very good part, and owned he was much obliged to him. "Dear Sprightly," added he, "I will not fail to do as you direct, and you shall see how I will acquit myself." Here ended their conversation, of which the merchant lost not a word. Early the next morning the labourer went for the ox. He fastened him to the plough and conducted him to his usual work. The ox, who had not forgotten the donkey's counsel, was very troublesome and untowardly all that day, and in the evening, when the labourer brought him back to the stall, and began to fasten him, the malicious beast instead of presenting his head willingly as he used to do, was restive, and drew back bellowing; and then made at the labourer, as if he would have gored him with his horns. In a word, he did all that the donkey had advised him. The day following, the labourer came as usual, to take the ox to his labour; but finding the stall full of beans, the straw that he had put in the night before not touched, and the ox lying on the ground with his legs stretched out, and panting in a strange manner, he believed him to be unwell, pitied him, and thinking that it was not proper to take him to work, went immediately and acquainted his master with his condition. The merchant perceiving that the ox had followed all the mischievous advice of the donkey, determined to punish the latter, and accordingly ordered the labourer to go and put him in the ox's place, and to he sure to work him hard. The labourer did as he was desired. The donkey was forced to draw the plough all that day, which fatigued him so much the more, as he was not accustomed to that kind of labour; besides he had been so soundly beaten, that he could scarcely stand when he came back. Meanwhile, the ox was mightily pleased; he ate up all that was in his stall, and rested himself the whole day. He rejoiced that he had followed the donkey's advice, blessed him a thousand times for the kindness he had done him, and did not fail to express his obligations when the donkey had returned. The donkey made no reply, so vexed was he at the ill treatment he had received; but he said within himself, "It is by my own imprudence I have brought this misfortune upon myself. I lived happily, every thing smiled upon me; I had all that I could wish; it is my own fault that I am brought to this miserable condition; and if I cannot contrive some way to get out of it, I am certainly undone." As he spoke, his strength was so much exhausted that he fell down in his stall, as if he had been half dead. Here the grand vizier, himself to Scheherazade, and said, "Daughter, you act just like this donkey; you will expose yourself to destruction by your erroneous policy. Take my advice, remain quiet, and do not seek to hasten your death." "Father," replied Scheherazade, "the example you have set before me will not induce me to change my resolution. I will never cease importuning you until you present me to the sultan as his bride." The vizier, perceiving that she persisted in her demand, replied, "Alas! then, since you will continue obstinate, I shall be obliged to treat you in the same manner as the merchant whom I before referred to treated his wife a short time after."The merchant understanding that the donkey was in a lamentable condition, was desirous of knowing what passed between him and the ox, therefore after supper he went out by moonlight, and sat down by them, his wife bearing him company. After his arrival, he heard the donkey say to the ox "Comrade, tell me, I pray you, what you intend to do tomorrow, when the labourer brings you meat?" "What will I do?" replied the ox, "I will continue to act as you taught me. I will draw back from him and threaten him with my horns, as I did yesterday: I will feign myself ill, and at the point of death." "Beware of that," replied the donkey, "it will ruin you; for as I came home this evening, I heard the merchant, our master, say something that makes me tremble for you." "Alas! what did you hear?" demanded the ox; "as you love me, withhold nothing from me, my dear Sprightly." "Our master," replied the donkey, "addressed himself thus to the labourer: "Since the ox does not eat, and is not able to work, I would have him killed tomorrow, and we will give his flesh as an alms to the poor for God's sake, as for the skin, that will be of use to us, and I would have you give it the currier to dress; therefore be sure to send for the butcher." This is what I had to tell you," said the donkey. "The interest I feel in your preservation, and my friendship for you, obliged me to make it known to you, and to give you new advice. As soon as they bring you your bran and straw, rise up and eat heartily. Our master will by this think that you are recovered, and no doubt will recall his orders for killing you; but, if you act otherwise, you will certainly be slaughtered."This discourse had the effect which the donkey designed. The ox was greatly alarmed, and bellowed for fear. The merchant, who heard the conversation very attentively, fell into a loud fit of laughter. His wife was greatly surprised, and asked, "Pray, husband, tell me what you laugh at so heartily, that I may laugh with you." "Wife," replied he, "you must content yourself with hearing me laugh." "No," returned she, "I will know the reason." "I cannot afford you that satisfaction," he, "and can only inform you that I laugh at what our donkey just now said to the ox. The rest is a secret, which I am not allowed to reveal." "What," demanded she "hinders you from revealing the secret?" "If I tell it you," replied he, "I shall forfeit my life." "You only jeer me," cried his wife, "what you would have me believe cannot be true. If you do not directly satisfy me as to what you laugh at, and tell me what the ox and the donkey said to one another, I swear by heaven that you and I shall never bed together again."Having spoken thus, she went into the house, and seating herself in a corner, cried there all night. Her husband lay alone, and finding next morning that she continued in the same humour, told her, she was very foolish to afflict herself in that manner; that the thing was not worth so much; that it concerned her very little to know while it was of the utmost consequence to him to keep the secret: "therefore," continued he, "I conjure you to think no more of it." "I shall still think so much of it," replied she, "as never to forbear weeping till you have satisfied my curiosity." "But I tell you very seriously," answered he, "that it will cost me my life if I yield to your indiscreet solicitations." "Let what will happen," said she, "I do insist upon it." "I perceive," resumed the merchant, "that it is impossible to bring you to reason, and since I foresee that you will occasion your own death by your obstinacy, I will call in your children, that they may see you before you die." Accordingly he called for them, and sent for her father and mother, and other relations. When they were come and had heard the reason of their being summoned, they did all they could to convince her that she was in the wrong, but to no purpose: she told them she would rather die than yield that point to her husband. Her father and mother spoke to her by herself, and told her that what she desired to know was of no importance to her; but they could produce no effect upon her, either by their authority or intreaties. When her children saw that nothing would prevail to draw her out of that sullen temper, they wept bitterly. The merchant himself was half frantic, and almost ready to risk his own life to save that of his wife, whom he sincerely loved. The merchant had fifty hens and one cock, with a dog that gave good heed to all that passed. While the merchant was considering what he had best do, he saw his dog run towards the cock as he was treading a hen, and heard him say to him: "Cock, I am sure heaven will not let you live long; are you not ashamed to ad thus today?" The cock standing up on tiptoe, answered fiercely: "And why not today as well as other days?" "If you do not know," replied the dog, "then I will tell you, that this day our master is in great perplexity. His wife would have him reveal a secret which is of such a nature, that the disclosure would cost him his life. Things are come to that pass, that it is to be feared he will scarcely have resolution enough to resist his wife's obstinacy; for he loves her, and is affected by the tears she continually sheds. We are all alarmed at his situation, while you only insult our melancholy, and have the impudence to divert yourself with your hens."The cock answered the dog's reproof thus: "What, has our master so little sense? he has but one wife, and cannot govern her, and though I have fifty, I make them all do what I please. Let him use his reason, he will soon find a way to rid himself of his trouble." "How?" demanded the dog; "what would you have him do?" "Let him go into the room where his wife is," resumed the cock, "lock the door, and take a stick and thrash her well; and I will answer for it, that will bring her to her senses, and make her forbear to importune him to discover what he ought not to reveal." The merchant had no sooner heard what the cock said, than he took up a stick, went to his wife, whom he found still crying, and shutting the door, belaboured her so soundly, that she cried out, "Enough, husband, enough, forbear, and I will never ask the question more." Upon this, perceiving that she repented of her impertinent curiosity, he desisted; and opening the door, her friends came in, were glad to find her cured of her obstinacy, and complimented her husband upon this happy expedient to bring his wife to reason."Daughter," added the grand vizier, "you deserve to be treated as the merchant treated his wife.""Father," replied Scheherazade, "I beg you would not take it ill that I persist in my opinion. I am nothing moved by the story of this woman. I could relate many, to persuade you that you ought not to oppose my design. Besides, pardon me for declaring, that your opposition is vain; for if your paternal affection should hinder you from granting my request, I will go and offer myself to the sultan." In short, the father, being overcome by the resolution of his daughter, yielded to her importunity, and though he was much grieved that he could not divert her from so fatal a resolution, he went instantly to acquaint the sultan, that next night he would bring him Scheherazade. The sultan was much surprized at the sacrifice which the grand vizier proposed to make. "How could you", said he, "resolve to bring me your own daughter?" "Sir," answered the vizier, "it is her own offer. The sad destiny that awaits her could not intimidate her; she prefers the honour of being your majesty's wile for one night, to her life." "But do not act under a mistake, vizier," said the sultan; "tomorrow. when I place Scheherazade in your hands, I expect you will put her to death; and if you fail, I swear that your own life shall answer." "Sir," rejoined the vizier "my heart without doubt will be full of grief to execute your commands; but it is to no purpose for nature to murmur. Though I am her father, I will answer for the fidelity of my hand to obey your order." Shier-ear accepted his minister's offer, and told him he might bring his daughter when he pleased. The grand vizier went with the intelligence to Scheherazade, who received it with as much joy as if it had been the most agreeable information she could have received. She thanked her father for having so greatly obliged her; and perceiving that he was overwhelmed with grief, told him for his consolation, that she hoped he would never repent of having married her to the sultan; and that, on the contrary, he should have reason to rejoice at his compliance all his days. Her business now was to adorn herself to appear before the sultan; but before she went, she took her sister Dinarzade apart, and said to her, "My dear sister, I have need of your assistance in a matter of great importance, and must pray you not to deny it me. My father is going to conduct me to the sultan; do not let this alarm you, but hear me with patience. As soon as I am in his presence, I will pray him to allow you to lie in the bride chamber, that I may enjoy your company this one night more. If I obtain that favour, as I hope to do, remember to awake me tomorrow an hour before day, and to address me in these or some such words: �My sister, if you be not asleep, I pray you that till daybreak, which will be very shortly, you will relate to me one of the entertaining stories of which you have read so many.' I will immediately tell you one; and I hope by this means to deliver the city from the consternation it is under at present." Dinarzade answered that she would with pleasure act as she required her. The grand vizier conducted Scheherazade to the palace, and retired, after having introduced her into the sultan's apartment. As soon as the sultan was left alone with her, he ordered her to uncover her face: he found her so beautiful that he was perfectly charmed; but perceiving her to be in tears, demanded the reason. "Sir," answered Scheherazade, "I have a sister who loves me tenderly, and I could wish that she might be allowed to pass the night in this chamber, that I might see her, and once more bid her adieu. Will you be pleased to allow me the consolation of giving her this last testimony of my affection?" Shier-ear having consented, Dinarzade was sent for, who came with all possible expedition. An hour before day, Dinarzade failed not to do as her sister had ordered. "My dear sister," cried she, "if you be not asleep, I pray that until daybreak, which will be very shortly, you will tell me one of those pleasant stories you have read. Alas! this may perhaps be the last time that I shall enjoy that pleasure."Scheherazade, instead of answering her sister, addressed herself to the sultan: "Sir, will your majesty be pleased to allow me to afford my sister this satisfaction?" "With all my heart," replied the sultan. Scheherazade then bade her sister attend, and afterwards, addressing herself to Shier-ear, proceeded as follows.</TEXT>
        </SHORT_STORY>
    </ENTRY>
    <ENTRY>
        <SHORT_STORY>
            <TITLE>The Emperor's New Clothes - Andersen's Fairy Tales</TITLE>
            <AUTHOR>Hans Christian Andersen</AUTHOR>
            <TYPE>Short Story</TYPE>
            <TEXT>Many years ago, there was an Emperor, who was so excessively fond of new clothes, that he spent all his money in dress. He did not trouble himself in the least about his soldiers; nor did he care to go either to the theatre or the chase, except for the opportunities then afforded him for displaying his new clothes. He had a different suit for each hour of the day; and as of any other king or emperor, one is accustomed to say, "he is sitting in council," it was always said of him, "The Emperor is sitting in his wardrobe."Time passed merrily in the large town which was his capital; strangers arrived every day at the court. One day, two rogues, calling themselves weavers, made their appearance. They gave out that they knew how to weave stuffs of the most beautiful colors and elaborate patterns, the clothes manufactured from which should have the wonderful property of remaining invisible to everyone who was unfit for the office he held, or who was extraordinarily simple in character."These must, indeed, be splendid clothes!" thought the Emperor. "Had I such a suit, I might at once find out what men in my realms are unfit for their office, and also be able to distinguish the wise from the foolish! This stuff must be woven for me immediately." And he caused large sums of money to be given to both the weavers in order that they might begin their work directly. So the two pretended weavers set up two looms, and affected to work very busily, though in reality they did nothing at all. They asked for the most delicate silk and the purest gold thread; put both into their own knapsacks; and then continued their pretended work at the empty looms until late at night."I should like to know how the weavers are getting on with my cloth," said the Emperor to himself, after some little time had elapsed; he was, however, rather embarrassed, when he remembered that a simpleton, or one unfit for his office, would be unable to see the manufacture. To be sure, he thought he had nothing to risk in his own person; but yet, he would prefer sending somebody else, to bring him intelligence about the weavers, and their work, before he troubled himself in the affair. All the people throughout the city had heard of the wonderful property the cloth was to possess; and all were anxious to learn how wise, or how ignorant, their neighbors might prove to be."I will send my faithful old minister to the weavers," said the Emperor at last, after some deliberation, "he will be best able to see how the cloth looks; for he is a man of sense, and no one can be more suitable for his office than be is."So the faithful old minister went into the hall, where the knaves were working with all their might, at their empty looms. "What can be the meaning of this?" thought the old man, opening his eyes very wide. "I cannot discover the least bit of thread on the looms." However, he did not express his thoughts aloud. The impostors requested him very courteously to be so good as to come nearer their looms; and then asked him whether the design pleased him, and whether the colors were not very beautiful; at the same time pointing to the empty frames. The poor old minister looked and looked, he could not discover anything on the looms, for a very good reason, viz: there was nothing there. "What!" thought he again. "Is it possible that I am a simpleton? I have never thought so myself; and no one must know it now if I am so. Can it be, that I am unfit for my office? No, that must not be said either. I will never confess that I could not see the stuff.""Well, Sir Minister!" said one of the knaves, still pretending to work. "You do not say whether the stuff pleases you.""Oh, it is excellent!" replied the old minister, looking at the loom through his spectacles. "This pattern, and the colors, yes, I will tell the Emperor without delay, how very beautiful I think them.""We shall be much obliged to you," said the impostors, and then they named the different colors and described the pattern of the pretended stuff. The old minister listened attentively to their words, in order that he might repeat them to the Emperor; and then the knaves asked for more silk and gold, saying that it was necessary to complete what they had begun. However, they put all that was given them into their knapsacks; and continued to work with as much apparent diligence as before at their empty looms. The Emperor now sent another officer of his court to see how the men were getting on, and to ascertain whether the cloth would soon be ready. It was just the same with this gentleman as with the minister; he surveyed the looms on all sides, but could see nothing at all but the empty frames."Does not the stuff appear as beautiful to you, as it did to my lord the minister?" asked the impostors of the Emperor's second ambassador; at the same time making the same gestures as before, and talking of the design and colors which were not there."I certainly am not stupid!" thought the messenger. "It must be, that I am not fit for my good, profitable office! That is very odd; however, no one shall know anything about it." And accordingly he praised the stuff he could not see, and declared that he was delighted with both colors and patterns. "Indeed, please your Imperial Majesty," said he to his sovereign when he returned, "the cloth which the weavers are preparing is extraordinarily magnificent."The whole city was talking of the splendid cloth which the Emperor had ordered to be woven at his own expense. And now the Emperor himself wished to see the costly manufacture, while it was still in the loom. Accompanied by a select number of officers of the court, among whom were the two honest men who had already admired the cloth, he went to the crafty impostors, who, as soon as they were aware of the Emperor's approach, went on working more diligently than ever; although they still did not pass a single thread through the looms."Is not the work absolutely magnificent?" said the two officers of the crown, already mentioned. "If your Majesty will only be pleased to look at it! What a splendid design! What glorious colors!" and at the same time they pointed to the empty frames; for they imagined that everyone else could see this exquisite piece of workmanship."How is this?" said the Emperor to himself. "I can see nothing! This is indeed a terrible affair! Am I a simpleton, or am I unfit to be an Emperor? That would be the worst thing that could happen. Oh! the cloth is charming," said he, aloud. "It has my complete approbation." And he smiled most graciously, and looked closely at the empty looms; for on no account would he say that he could not see what two of the officers of his court had praised so much. All his retinue now strained their eyes, hoping to discover something on the looms, but they could see no more than the others; nevertheless, they all exclaimed, "Oh, how beautiful!" and advised his majesty to have some new clothes made from this splendid material, for the approaching procession. "Magnificent! Charming! Excellent!" resounded on all sides; and everyone was uncommonly gay. The Emperor shared in the general satisfaction; and presented the impostors with the riband of an order of knighthood, to be worn in their button-holes, and the title of "Gentlemen Weavers."The rogues sat up the whole of the night before the day on which the procession was to take place, and had sixteen lights burning, so that everyone might see how anxious they were to finish the Emperor's new suit. They pretended to roll the cloth off the looms; cut the air with their scissors; and sewed with needles without any thread in them. "See!" cried they, at last. "The Emperor's new clothes are ready!"And now the Emperor, with all the grandees of his court, came to the weavers; and the rogues raised their arms, as if in the act of holding something up, saying, "Here are your Majesty's trousers! Here is the scarf! Here is the mantle! The whole suit is as light as a cobweb; one might fancy one has nothing at all on, when dressed in it; that, however, is the great virtue of this delicate cloth.""Yes indeed!" said all the courtiers, although not one of them could see anything of this exquisite manufacture."If your Imperial Majesty will be graciously pleased to take off your clothes, we will fit on the new suit, in front of the looking glass."The Emperor was accordingly undressed, and the rogues pretended to array him in his new suit; the Emperor turning round, from side to side, before the looking glass."How splendid his Majesty looks in his new clothes, and how well they fit!" everyone cried out. "What a design! What colors! These are indeed royal robes!""The canopy which is to be borne over your Majesty, in the procession, is waiting," announced the chief master of the ceremonies."I am quite ready," answered the Emperor. "Do my new clothes fit well?" asked he, turning himself round again before the looking glass, in order that he might appear to be examining his handsome suit. The lords of the bedchamber, who were to carry his Majesty's train felt about on the ground, as if they were lifting up the ends of the mantle; and pretended to be carrying something; for they would by no means betray anything like simplicity, or unfitness for their office. So now the Emperor walked under his high canopy in the midst of the procession, through the streets of his capital; and all the people standing by, and those at the windows, cried out, "Oh! How beautiful are our Emperor's new clothes! What a magnificent train there is to the mantle; and how gracefully the scarf hangs!" in short, no one would allow that he could not see these much admired clothes; because, in doing so, he would have declared himself either a simpleton or unfit for his office. Certainly, none of the Emperor's various suits, had ever made so great an impression, as these invisible ones."But the Emperor has nothing at all on!" said a little child."Listen to the voice of innocence!" exclaimed his father; and what the child had said was whispered from one to another."But he has nothing at all on!" at last cried out all the people. The Emperor was vexed, for he knew that the people were right; but he thought the procession must go on now! And the lords of the bedchamber took greater pains than ever, to appear holding up a train, although, in reality, there was no train to hold.</TEXT>
        </SHORT_STORY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>Much Ado About Nothing</TITLE>
            <AUTHOR>William Shakespeare</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>Don Pedro, the prince of Aragon, is an old friend of Leonato, the governor of Messina. Don Pedro has been invited to stay with Leonato after quelling the rebellion started by his illegitimate brother, Don John. On the visit, two companions accompany Don Pedro: the valiant fighter Claudio and the witty bachelor Benedick. Claudio is instantly attracted to Leonato's daughter Hero. He wants to marry her, but is hesitant in wooing her. Therefore, Don Pedro promises to woo Hero for Claudio at the masquerade ball. Leonato's niece, Beatrice, is a clever match for Benedick. Like him, she vows never to marry. The two of them spar constantly, claiming to dislike one another. Don Pedro and Leonato think up a plan to bring Benedick and Beatrice together, since they know the bickering couple is attracted to one another but do not want to admit it. Meanwhile, Don John, Don Pedro's bastard brother, dislikes Claudio intensely, since Claudio defeated him in the recent battle. Don John plots with his henchman Borachio to frustrate Claudio's marriage plans. Borachio convinces Margaret to stand at Hero's chamber window and talk to him late at night. This way, the easily duped Claudio will think his fiance has betrayed himDon Pedro puts his plan into action, dropping hints to Beatrice that Benedick is madly in love with her, and dropping hints to Benedick that Beatrice pines for him. Their egos are stroked, their passions aroused. The plan succeeds and they emerge as a confirmed pair of lovers. Don John and Borachio also succeed in their conspiracy. Claudio waits for the marriage day to denounce his fiance. Shocked and humiliated, not to mention confused, Hero swoons and is presumed dead. Claudio and Don Pedro leave, and the Friar of the church comes to Hero's defense. The Friar keeps her in hiding till Claudio is repentant of his unforgivable act. Beatrice asks her newfound lover Benedick to avenge Hero's insult by challenging his dear friend Claudio to a duel. Benedick, a slave to love, reluctantly agrees. A couple of night watchmen of the palace overhear Borachio reveal his part in the heinous plot and arrest him. The deception is brought to light and Claudio repents of his lack of faith. Leonato takes Claudio to Hero's "grave" to show that he is truly sorry. He then brings out his niece (Hero in disguise) and offers her to Claudio as a bride. A rejoicing Claudio sees that Hero is alive. The two couples, Hero and Claudio and Beatrice and Benedick, marry. The news reaches them all that Don John has been arrested for his evil deeds. The group dances and the curtain falls. </TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>To Kill a Mockingbird</TITLE>
            <AUTHOR>Harper Lee</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>To Kill a Mockingbird is primarily a novel about growing up under extraordinary circumstances in the 1930s in the southern United States. The story covers a span of three years, during which the main characters undergo significant changes. Scout Finch lives with her brother Jem and their father Atticus in the fictitious town of Maycomb, Alabama. Maycomb is a small, personal town, and every family has its social station depending on where they live, who their parents are, and how long their ancestors have lived in Maycomb. A widower, Atticus raises his children by himself, with the help of kindly neighbors and a black housekeeper named Calpurnia. Scout and Jem almost instinctively understand the complexities and machinations of their neighborhood and town. The only neighbor who puzzles them is the mysterious Arthur Radley, nicknamed Boo, who never comes outside. When Dill, another neighbor's nephew, starts spending summers in Maycomb, the three children begin an obsessive (sometimes perilous) quest to lure Boo outside. Scout is a tomboy who prefers the company of boys and generally solves her differences with her fists. She tries to make sense of a world that demands that she act like a lady, a brother who criticizes her for acting like a girl, and a father who accepts her just as she is. Scout hates school, gaining her most valuable education on her own street and from her father. Not quite midway through the story, Scout and Jem discover that their father is going to represent a black man named Tom Robinson, who is accused of raping and beating a white woman. Suddenly, Scout and Jem have to tolerate a barrage of racial slurs and insults because of Atticus' role in the trial. During this time, Scout has a very difficult time restraining from physically fighting with other children, a tendency that gets her in trouble with her Aunt Alexandra and Uncle Jack. Even Jem, the older and more levelheaded of the two, loses his temper a time or two. After responding to a neighbor's (Mrs. Dubose) verbal attack by destroying her plants, Jem is sentenced to read to her every day after school for one month. Ultimately, Scout and Jem learn a powerful lesson about bravery from this woman. As the trial draws nearer, Aunt Alexandra comes to live with them under the guise of providing a feminine influence for Scout. During the novel's last summer, Tom is tried and convicted even though Atticus proves that Tom could not have possibly committed the crime of which he is accused. In the process of presenting Tom's case, Atticus inadvertently insults and offends Bob Ewell, a nasty, lazy drunkard whose daughter is Tom's accuser. In spite of Tom's conviction, Ewell vows revenge on Atticus and the judge for besmirching his already tarnished name. All three children are bewildered by the jury's decision to convict; Atticus tries to explain why the jury's decision was in many ways a foregone conclusion. Shortly after the trial, Scout attends one of her aunt's Missionary Society meetings. Atticus interrupts the meeting to report that Tom Robinson had been killed in an escape attempt. Scout learns valuable lessons about achieving the ideal of womanhood and carrying on in the face of adversity that day. Things slowly return to normal in Maycomb, and Scout and Jem realize that Boo Radley is no longer an all-consuming curiosity. The story appears to be winding down, but then Bob Ewell starts making good on his threats of revenge. Scout is in the Halloween pageant at school, playing the part of a ham. With Atticus and Aunt Alexandra both too tired to attend, Jem agrees to take Scout to the school. After embarrassing herself onstage, Scout elects to leave her ham costume on for the walk home with Jem. On the way home, the children hear odd noises, but convince themselves that the noises are coming from another friend who scared them on their way to school that evening. Suddenly, a scuffle occurs. Scout really can't see outside of her costume, but she hears Jem being pushed away, and she feels powerful arms squeezing her costume's chicken wire against her skin. During this attack, Jem badly breaks his arm. Scout gets just enough of a glimpse out of her costume to see a stranger carrying Jem back to their house. The sheriff arrives at the Finch house to announce that Bob Ewell has been found dead under the tree where the children were attacked, having fallen on his own knife. By this time, Scout realizes that the stranger is none other than Boo Radley, and that Boo is actually responsible for killing Ewell, thus saving her and Jem's lives. In spite of Atticus' insistence to the contrary, the sheriff refuses to press charges against Boo. Scout agrees with this decision and explains her understanding to her father. Boo sees Jem one more time and then asks Scout to take him home, but rather than escort him home as though he were a child, she has Boo escort her to his house as a gentleman would. With Boo safely home, Scout returns to Jem's room where Atticus is waiting. He reads her to sleep and then waits by Jem's bedside for his son to wake up.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>Lord of the Flies</TITLE>
            <AUTHOR>William Golding</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>Lord of the Flies explores the dark side of humanity, the savagery that underlies even the most civilized human beings. Golding intended this novel as a tragic parody of children's adventure tales, illustrating humankind's intrinsic evil nature. He presents the reader with a chronology of events leading a group of young boys from hope to disaster as they attempt to survive their uncivilized, unsupervised, isolated environment until rescued. In the midst of a nuclear war, a group of British boys find themselves stranded without adult supervision on a tropical island. The group is roughly divided into the "littluns," boys around the age of six, and the "biguns," who are between the ages of ten and twelve. Initially, the boys attempt to form a culture similar to the one they left behind. They elect a leader, Ralph, who, with the advice and support of Piggy (the intellectual of the group), strives to establish rules for housing and sanitation. Ralph also makes a signal fire the group's first priority, hoping that a passing ship will see the smoke signal and rescue them. A major challenge to Ralph's leadership is Jack, who also wants to lead. Jack commands a group of choirboys turned hunters who sacrifice the duty of tending the fire so that they can participate in the hunts. Jack draws the other boys slowly away from Ralph's influence because of their natural attraction to and inclination toward the adventurous hunting activities symbolizing violence and evil. The conflict between Jack and Ralph and the forces of savagery and civilization that they represent is exacerbated by the boys' literal fear of a mythical beast roaming the island. One night, an aerial battle occurs above the island, and a casualty of the battle floats down with his opened parachute, ultimately coming to rest on the mountaintop. Breezes occasionally inflate the parachute, making the body appear to sit up and then sink forward again. This sight panics the boys as they mistake the dead body for the beast they fear. In a reaction to this panic, Jack forms a splinter group that is eventually joined by all but a few of the boys. The boys who join Jack are enticed by the protection Jack's ferocity seems to provide, as well as by the prospect of playing the role of savages: putting on camouflaging face paint, hunting, and performing ritualistic tribal dances. Eventually, Jack's group actually slaughters a sow and, as an offering to the beast, puts the sow's head on a stick. Of all the boys, only the mystic Simon has the courage to discover the true identity of the beast sighted on the mountain. After witnessing the death of the sow and the gift made of her head to the beast, Simon begins to hallucinate, and the staked sow's head becomes the Lord of the Flies, imparting to Simon what he has already suspected: The beast is not an animal on the loose but is hidden in each boy's psyche. Weakened by his horrific vision, Simon loses consciousness. Recovering later that evening, he struggles to the mountaintop and finds that the beast is only a dead pilot/soldier. Attempting to bring the news to the other boys, he stumbles into the tribal frenzy of their dance. Perceiving him as the beast, the boys beat him to death. Soon only three of the older boys, including Piggy, are still in Ralph's camp. Jack's group steals Piggy's glasses to start its cooking fires, leaving Ralph unable to maintain his signal fire. When Ralph and his small group approach Jack's tribe to request the return of the glasses, one of Jack's hunters releases a huge boulder on Piggy, killing him. The tribe captures the other two biguns prisoners, leaving Ralph on his own. The tribe undertakes a manhunt to track down and kill Ralph, and they start a fire to smoke him out of one of his hiding places, creating an island-wide forest fire. A passing ship sees the smoke from the fire, and a British naval officer arrives on the beach just in time to save Ralph from certain death at the hands of the schoolboys turned savages.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>The Great Gatsby</TITLE>
            <AUTHOR>F. Scott Fitzgerald</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>The Great Gatsby is a story told by Nick Carraway, who was once Gatsby's neighbor, and he tells the story sometime after 1922, when the incidents that fill the book take place. As the story opens, Nick has just moved from the Midwest to West Egg, Long Island, seeking his fortune as a bond salesman. Shortly after his arrival, Nick travels across the Sound to the more fashionable East Egg to visit his cousin Daisy Buchannan and her husband, Tom, a hulking, imposing man whom Nick had known in college. There he meets professional golfer Jordan Baker. The Buchannans and Jordan Baker live privileged lives, contrasting sharply in sensibility and luxury with Nick's more modest and grounded lifestyle. When Nick returns home that evening, he notices his neighbor, Gatsby, mysteriously standing in the dark and stretching his arms toward the water, and a solitary green light across the Sound. One day, Nick is invited to accompany Tom, a blatant adulterer, to meet his mistress, Myrtle Wilson, a middle class woman whose husband runs a modest garage and gas station in the valley of ashes, a desolate and rundown section of town that marks the convergence of the city and the suburbs. After the group meets and journeys into the city, Myrtle phones friends to come over and they all spend the afternoon drinking at Myrtle and Tom's apartment. The afternoon is filled with drunken behavior and ends ominously with Myrtle and Tom fighting over Daisy, his wife. Drunkenness turns to rage and Tom, in one deft movement, breaks Myrtle's nose. Following the description of this incident, Nick turns his attention to his mysterious neighbor, who hosts weekly parties for the rich and fashionable. Upon Gatsby's invitation (which is noteworthy because rarely is anyone ever invited to Gatsby's parties - they just show up, knowing they will not be turned away), Nick attends one of the extravagant gatherings. There, he bumps into Jordan Baker, as well as Gatsby himself. Gatsby, it turns out, is a gracious host, but yet remains apart from his guest, an observer more than a participant, as if he is seeking something. As the party winds down, Gatsby takes Jordan aside to speak privately. Although the reader isn't specifically told what they discuss, Jordan is greatly amazed by what she's learned. As the summer unfolds, Gatsby and Nick become friends and Jordan and Nick begin to see each other on a regular basis, despite Nick's conviction that she is notoriously dishonest (which offends his sensibilities because he is "one of the few honest people" he has ever met). Nick and Gatsby journey into the city one day and there Nick meets Meyer Wolfshiem, one of Gatsby's associates and Gatsby's link to organized crime. On that same day, while having tea with Jordan Baker, Nick learns the amazing story that Gatsby told her the night of his party. Gatsby, it appears, is in love with Daisy Buchannan. They met years earlier when he was in the army but could not be together because he did not yet have the means to support her. In the intervening years, Gatsby made his fortune, all with the goal of winning Daisy back. He bought his house so that he would be across the Sound from her and hosted the elaborate parties in the hopes that she would notice. It has come time for Gatsby to meet Daisy again, face-to-face, and so, through the intermediary of Jordan Baker, Gatsby asks Nick to invite Daisy to his little house where Gatsby will show up unannounced. The day of the meeting arrives. Nick's house is perfectly prepared, due largely to the generosity of the hopeless romantic Gatsby, who wants every detail to be perfect for his reunion with his lost love. When the former lovers meet, their reunion is slightly nervous, but shortly, the two are once again comfortable with each other, leaving Nick to feel an outsider in the warmth the two people radiate. As the afternoon progresses, the three move the party from Nick's house to Gatsby's, where he takes special delight in showing Daisy his meticulously decorated house and his impressive array of belongings, as if demonstrating in a very tangible way just how far out of poverty he has traveled. At this point, Nick again lapses into memory, relating the story of Jay Gatsby. Born James Gatz to "shiftless and unsuccessful farm people," Gatsby changed his name at seventeen, about the same time he met Dan Cody. Cody would become Gatsby's mentor, taking him on in "a vague personal capacity" for five years as he went three times around the Continent. By the time of Cody's death, Gatsby had grown into manhood and had defined the man he would become. Never again would he acknowledge his meager past; from that point on, armed with a fabricated family history, he was Jay Gatsby, entrepreneur. Moving back to the present, we discover that Daisy and Tom will attend one of Gatsby's parties. Tom, of course, spends his time chasing women, while Daisy and Gatsby sneak over to Nick's yard for a moment's privacy while Nick, accomplice in the affair, keeps guard. After the Buchannans leave, Gatsby tells Nick of his secret desire: to recapture the past. Gatsby, the idealistic dreamer, firmly believes the past can be recaptured in its entirety. Gatsby then goes on to tell what it is about his past with Daisy that has made such an impact on him. As the summer unfolds, Gatsby and Daisy's affair begins to grow and they see each other regularly. On one fateful day, the hottest and most unbearable of the summer, Gatsby and Nick journey to East Egg to have lunch with the Buchannans and Jordan Baker. Oppressed by the heat, Daisy suggests they take solace in a trip to the city. No longer hiding her love for Gatsby, Daisy pays him special attention and Tom deftly picks up on what's going on. As the party prepares to leave for the city, Tom fetches a bottle of whisky. Tom, Nick, and Jordan drive in Gatsby's car, while Gatsby and Daisy drive Tom's coupe. Low on gas, Tom stops Gatsby's car at Wilson's gas station, where he sees that Wilson is not well. Like Tom, who has just learned of Daisy's affair, Wilson has just learned of Myrtle's secret life, although he does not know who the man is, and it has made him physically sick. Wilson announces his plans to take Myrtle out West, much to Tom's dismay. Tom has lost a wife and a mistress all in a matter of an hour. Absorbed in his own fears, Tom hastily drives into the city. The group ends up at the Plaza hotel, where they continue drinking, moving the day closer and closer to its tragic end. Tom, always a hot-head, begins to badger Gatsby, questioning him as to his intentions with Daisy. Decidedly tactless and confrontational, Tom keeps harping on Gatsby until the truth comes out: Gatsby wants Daisy to admit she's never loved Tom but that, instead, she has always loved him. When Daisy is unable to do this, Gatsby declares that Daisy is going to leave Tom. Tom, though, understands Daisy far better than Gatsby does and knows she won't leave him: His wealth and power, matured through generations of privilege, will triumph over Gatsby's newly found wealth. In a gesture of authority, Tom orders Daisy and Gatsby to head home in Gatsby's car. Tom, Nick, and Jordan follow. As Tom's car nears Wilson's garage, they can all see that some sort of accident has occurred. Pulling over to investigate, they learn that Myrtle Wilson, Tom's mistress, has been hit and killed by a passing car that never bothered to stop, and it appears to have been Gatsby's car. Tom, Jordan, and Nick continue home to East Egg. Nick, now disgusted by the morality and behavior of the people with whom he has been on friendly terms, meets Gatsby outside of the Buchannans house where he is keeping watch for Daisy. With a few well chosen questions, Nick learns that Daisy, not Gatsby, was driving the car, although Gatsby confesses he will take all the blame. Nick, greatly agitated by all that he has experienced during the day, continues home, but an overarching feeling of dread haunts him. Nearing dawn the next morning, Nick goes to Gatsby's house. While the two men turn the house upside down looking for cigarettes, Gatsby tells Nick more about how he became the man he is and how Daisy figured into his life. Later that morning, while at work, Nick is unable to concentrate. He receives a phone call from Jordan Baker, but is quick to end the discussion, and thereby the friendship. He plans to take an early train home and check on Gatsby. The action then switches back to Wilson who, distraught over his wife's death, sneaks out and goes looking for the driver who killed Myrtle. Nick retraces Wilson's journey, which placed him, by early afternoon, at Gatsby's house. Wilson murders Gatsby and then turns the gun on himself. After Gatsby's death, Nick is left to help make arrangements for his burial. What is most perplexing, though, is that no one seems overly concerned with Gatsby's death. Daisy and Tom mysteriously leave on a trip and all the people who so eagerly attended his parties, drinking his liquor and eating his food, refuse to become involved. Even Meyer Wolfshiem, Gatsby's business partner, refuses to publicly mourn his friend's death. A telegram from Henry C. Gatz, Gatsby's father, indicates he will be coming from Minnesota to bury his son. Gatsby's funeral boasts only Nick, Henry Gatz, a few servants, the postman, and the minister at the graveside. Despite all his popularity during his lifetime, in his death, Gatsby is completely forgotten. Nick, completely disillusioned with what he has experienced in the East, prepares to head back to the Midwest. Before leaving, he sees Tom Buchannan one last time. When Tom notices him and questions him as to why he didn't want to shake hands, Nick curtly offers "You know what I think of you." Their discussion reveals that Tom was the impetus behind Gatsby's death. When Wilson came to his house, he told Wilson that Gatsby owned the car that killed Myrtle. In Tom's mind, he had helped justice along. Nick, disgusted by the carelessness and cruel nature of Tom, Daisy, and those like them, leaves Tom, proud of his own integrity. On the last night before leaving, Nick goes to Gatsby's mansion, then to the shore where Gatsby once stood, arms outstretched toward the green light. The novel ends prophetically, with Nick noting how we are all a little like Gatsby, boats moving up a river, going forward but continually feeling the pull of the past.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>The Kite Runner</TITLE>
            <AUTHOR>Khaled Hosseini</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>The Kite Runner is the story of Amir, a Sunni Muslim, who struggles to find his place in the world because of the aftereffects and fallout from a series of traumatic childhood events. An adult Amir opens the novel in the present day United States with a vague reference to one of these events, and then the novel flashes back to Amir's childhood in Afghanistan. In addition to typical childhood experiences, Amir struggles with forging a closer relationship with his father, Baba; with determining the exact nature of his relationship with Hassan, his Shi'a Muslim servant; and eventually with finding a way to atone for pre-adolescent decisions that have lasting repercussions. Along the way, readers are able to experience growing up in Afghanistan in a single parent home, a situation that bears remarkable similarities to many contemporary households. One of the biggest struggles for Amir is learning to navigate the complex socioeconomic culture he faces, growing up in Afghanistan as a member of the privileged class yet not feeling like a privileged member of his own family. Hassan and his father, Ali, are servants, yet at times, Amir's relationship with them is more like that of family members. And Amir's father, Baba, who does not consistently adhere to the tenets of his culture, confuses rather than clarifies things for young Amir. Many of the ruling class elite in Afghanistan view the world as black and white, yet Amir identifies many shades of gray. In addition to the issues affecting his personal life, Amir must also contend with the instability of the Afghan political system in the 1970s. During a crucial episode, which takes place during an important kite flying tournament, Amir decides not to act. He decides not to confront bullies and aggressors when he has the chance, and this conscious choice of inaction sets off a chain reaction that leads to guilt, lies, and betrayals. Eventually, because of the changing political climate, Amir and his father are forced to flee Afghanistan. Amir views coming to America as an opportunity to leave his past behind. Although Amir and Baba toil to create a new life for themselves in the United States, the past is unable to stay buried. When it rears its ugly head, Amir is forced to return to his homeland to face the demons and decisions of his youth, with only a slim hope to make amends. Ultimately, The Kite Runner is a novel about relationships, specifically the relationships between Amir and Hassan, Baba, Rahim Khan, Soraya, and Sohrab, and how the complex relationships in our lives overlap and connect to make us the people we are.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>Jane Eyre</TITLE>
            <AUTHOR>Charlotte Bronte</AUTHOR>
			<TYPE>Summary</TYPE>
            <TEXT>Orphaned as an infant, Jane Eyre lives with her aunt Sarah Reed at Gateshead as the novel opens. Jane is ten years old, an outsider in the Reed family. Her female cousins, Georgiana and Eliza, tolerate, but don't love her. Their brother, John, is more blatantly hostile to Jane, reminding her that she is a poor dependent of his mother who shouldn't even be associating with the children of a gentleman. One day he is angered to find Jane reading one of his books, so he takes the book away and throws it at her. Finding this treatment intolerable, Jane fights back. She is blamed for the conflagration and sent to the red room, the place where her kind Uncle Reed died. In this frightening room, Jane thinks she sees her uncle's ghost and begs to be set free. Her Aunt Reed refuses, insisting Jane remain in her prison until she learns complete submissiveness. When the door to the red room is locked once again, Jane passes out. She wakes back in her own room, with the kind physician, Mr. Lloyd, standing over her bed. He advises Aunt Reed to send Jane away to school, because she is obviously unhappy at Gateshead. Jane is sent to Lowood School, a charity institution for orphan girls, run by Mr. Brocklehurst. A stingy and mean minister, Brocklehurst provides the girls with starvation levels of food, freezing rooms, and poorly made clothing and shoes. He justifies his poor treatment of them by saying that they need to learn humility and by comparing them to the Christian martyrs, who also endured great hardships. Despite the difficult conditions at Lowood, Jane prefers school to life with the Reeds. Here she makes two new friends: Miss Temple and Helen Burns. From Miss Temple, Jane learns proper ladylike behavior and compassion; from Helen she gains a more spiritual focus. The school's damp conditions, combined with the girls' near starvation diet, produces a typhus epidemic, in which nearly half the students die, including Helen Burns, who dies in Jane's arms. Following this tragedy, Brocklehurst is deposed from his position as manager of Lowood, and conditions become more acceptable. Jane quickly becomes a star student, and after six years of hard work, an effective teacher. Following two years of teaching at Lowood, Jane is ready for new challenges. Miss Temple marries, and Lowood seems different without her. Jane places at advertisement for a governess position in the local newspaper. She receives only one reply, from a Mrs. Fairfax of Thornfield, near Millcote, who seeks a governess for a ten year old girl. Jane accepts the job. At Thornfield, a comfortable three story country estate, Jane is warmly welcomed. She likes both her new pupil, Adele Varens, and Mrs. Fairfax, the housekeeper at Thornfield, but is soon restless. One January afternoon, while walking to Millcote to mail a letter, Jane helps a horseman whose horse has slipped on a patch of ice and fallen. Returning to Thornfield, Jane discovers that this man is Edward Fairfax Rochester, the owner of Thornfield and her employer. He is a dark haired, moody man in his late thirties. Although he is often taciturn, Jane grows fond of his mysterious, passionate nature. He tells Jane about Adele's mother, Celine, a Parisian opera singer who was once his mistress. Adele, he claims, is not his daughter, but he rescued the poor girl after her mother abandoned her. Jane also discovers that Thornfield harbors a secret. From time to time, she hears strange, maniacal laughter coming from the third story. Mrs. Fairfax claims this is just Grace Poole, an eccentric servant with a drinking problem. But Jane wonders if this is true. One night, Jane smells smoke in the hallway, and realizes it is coming from Rochester's room. Jane races down to his room, discovering his curtains and bed are on fire. Unable to wake Rochester, she douses both him and his bedding with cold water. He asks her not to tell anyone about this incident and blames the arson on Grace Poole. Why doesn't he press charges on Grace, or at least evict her from the house, Jane wonders. Following this incident, Rochester leaves suddenly for a house party at a local estate. Jane is miserable during his absence and realizes she is falling in love with him. After a weeklong absence, he returns with a party of guests, including the beautiful Blanche Ingram. Jane jealously believes Rochester is pursing this accomplished, majestic, dark haired beauty. An old friend of Rochester's, Richard Mason, joins the party one day. From him, Jane learns that Rochester once lived in Spanish Town, Jamaica. One night, Mason is mysteriously attacked, supposedly by the crazy Grace Poole. Jane leaves Thornfield for a month to attend her aunt, who is on her deathbed following her son John's excessive debauchery and apparent suicide. Jane tries to create a reconciliation with her aunt, but the woman refuses all Jane's attempts at appeasement. Before dying, she gives Jane a letter from her uncle, John Eyre, who had hoped to adopt Jane and make her his heir. The letter was sent three years ago, but Aunt Reed had vindictively kept it from Jane. Sarah Reed dies, unloved by her daughters. When Jane returns to Thornfield, the houseguests have left. Rochester tells Jane he will soon marry Blanche, so she and Adele will need to leave Thornfield. In the middle of this charade, Jane reveals her love for him, and the two end up engaged. Jane is happy to be marrying the man she loves, but during the month before the wedding she is plagued by strange dreams of a destroyed Thornfield and a wailing infant. Two nights before the wedding, a frightening, dark haired woman enters her room and rips her wedding veil in two. Although Jane is certain this woman didn't look like Grace Poole, Rochester assures her it must have been the bizarre servant. The morning of the wedding finally arrives. Jane and Rochester stand at the altar, taking their vows, when suddenly a strange man announces there's an impediment to the marriage: Rochester is already married to a woman named Bertha Antoinetta Mason. Rochester rushes the wedding party back to Thornfield, where they find his insane and repulsive wife locked in a room on the third story. Grace Poole is the woman's keeper, but Bertha was responsible for the strange laughter and violence at Thornfield. Rochester tries to convince Jane to become his mistress and move with him to a pleasure villa in the south of France. Instead, Jane sneaks away in the middle of the night, with little money and no extra clothing. With twenty shillings, the only money she has, she catches a coach that takes her to faraway Whitcross. There, she spends three days roaming the woods, looking for work and, finally, begging for food. On the third night, she follows a light that leads her across the moors to Marsh End (also called Moor House), owned by the Rivers family. Hannah, the housekeeper, wants to send her away, but St. John Rivers, the clergyman who owns the house, offers her shelter. Jane soon becomes close friends with St. John's sisters, Diana and Mary, and he offers Jane a humble job as the schoolmistress for the poor girls in his parish at Morton. Because their father lost most of his money before he died, Diana and Mary have been forced to earn a living by working as governesses. One day, St. John learns that, unbeknownst to her, Jane has inherited 20,000 pounds from her uncle, John Eyre. Furthermore, she discovers that St. John's real name is St. John Eyre Rivers, so he, his sisters, and Jane are cousins. The Rivers were cut out of John Eyre's will because of an argument between John and their father. Thrilled to discover that she has a family, Jane insists on splitting the inheritance four ways, and then remodels Moor House for her cousins, who will no longer need to work as governesses. Not content with his life as a smalltime clergyman, St. John plans to become a missionary in India. He tries to convince Jane to accompany him, as his wife. Realizing that St. John doesn't love her but just wants to use her to accomplish his goals, Jane refuses his request, but suggests a compromise by agreeing to follow him to India as a comrade, but not as a wife. St. John tries to coerce her into the marriage, and has almost succeeded, when, one night Jane suddenly hears Rochester's disembodied voice calling out to her. Jane immediately leaves Moor House to search for her true love, Rochester. Arriving at Millcote, she discovers Thornfield a burned wreck, just as predicted in her dreams. From a local innkeeper, she learns that Bertha Mason burned the house down one night and that Rochester lost an eye and a hand while trying to save her and the servants. He now lives in seclusion at Ferndean. Jane immediately drives to Ferndean. There she discovers a powerless, unhappy Rochester. Jane carries a tray to him and reveals her identity. The two lovers are joyfully reunited and soon marry. Ten years later, Jane writes this narrative. Her married life is still blissful; Adele has grown to be a helpful companion for Jane; Diana and Mary Rivers are happily married; St. John still works as a missionary, but is nearing death; and Rochester has regained partial vision, enough to see their first born son.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>The Age of Innocence</TITLE>
            <AUTHOR>Edith Wharton</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>It is a January evening in 1870s New York City and the fashionable are attending the opera. As young Newland Archer, lawyer and man about town, gazes up at his future fiance, May Welland, in the Mingott family opera box, he is disconcerted by the arrival of May's cousin, the Countess Ellen Olenska, who has left her profligate but wealthy Polish husband. To discourage gossip, Newland decides to announce his and May's engagement at the Beaufort's ball that night. All of old New York is at the ball, gossiping about the Countess. Later, when the family plans a dinner to introduce her to society, no one accepts. Without delay, the Mingott family enlist the help of ancient social sages, Henry and Louisa van der Luyden, to shore up support by inviting old New York to a dinner it cannot refuse. In this way they introduce the exotic Countess, and she finds New York society charmingly narrow and provincial compared to Paris. The next day Newland visits the Countess' small house in a Bohemian section of town. He finds her drawing room exotic and her friendship with shady financier Julius Beaufort unsettling. But he senses her loneliness and, despite some misgivings, sends her yellow roses. The Mingotts enlist Newland's boss, Mr. Letterblair, to ask Newland to dissuade the Countess from seeking a divorce. When Newland speaks with Ellen, a passionate and exotic woman, unlike his quiet, innocent May, he finds himself falling in love with her, despite his engagement. Worried by temptation, Newland flees to Florida where May's family is vacationing and asks May to move the wedding date up. Startled, May tells him that if there is "someone else," he may have his freedom. Touched by her selflessness, Newland returns to New York. As he confesses his love to Ellen, a telegram arrives from May, saying that they can be married in a month. Newland knows his duty. Book II of The Age of Innocence begins with May marrying Newland as New York society watches. By August, a year later, Newland and May have settled into a fashionable if boring life in New York, living in a wealthy part of town and spending summers with the rest of the rich in Newport. Ellen has moved to Washington D.C.; she returns to stay with her grandmother briefly, but later leaves to visit Boston. Still under her spell, Newland lies to his wife and follows Ellen there. Ellen promises to stay in America only if they do not hurt May with a clandestine affair. She returns to Washington. Meanwhile, Julius Beaufort's shady financial dealings catch up with him, and his wife, Regina, appeals to Ellen's grandmother for help. Mrs. Mingott suffers a stroke and sends for Ellen to nurse her; during the two hour carriage ride with Ellen from the train station, Newland suggests they have an affair. Ellen refuses, knowing that will hurt May. He abruptly leaves the carriage and walks home. Seeing May in the library, he realizes he will dutifully stay married to her forever. Undaunted, the next day Newland meets Ellen at the Metropolitan Museum, where she finally agrees to a future one time affair. Elated but guilty, Newland decides to confess all to May, but she interrupts to tell him that Ellen is leaving for Europe and the Archers will give a farewell dinner for her. Shocked, Newland intends to later follow Ellen. At the dinner, however, he suddenly realizes that the entire family, including May, thinks that he and Ellen are already having an affair; giving Ellen the funds to live in Europe is the family's way of dealing with the situation. That night as he and May retire, she announces that she thought she was pregnant and told Ellen earlier, before she was really sure. But now she is sure, sealing Newland's fate forever. The years pass. Newland is 57 and he and May have two grown children: Dallas and Mary. May has recently died of pneumonia, nursing a third child to health. Newland accompanies Dallas to Paris on a business trip, where Dallas tells Newland the Countess Ellen Olenska has invited them to dine. Newland has not seen her in 26 years. Dallas confides to his father May's deathbed confession that Newland sacrificed the one thing he loved because of duty and honor. That evening outside the Countess' apartment, Newland encourages Dallas to go up without him. In Newland's memory, their love stays forever young, perfect and unchanging over time.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>Animal Farm</TITLE>
            <AUTHOR>George Orwell</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>One night, all the animals at Mr. Jones' Manor Farm assemble in a barn to hear old Major, a pig, describe a dream he had about a world where all animals live free from the tyranny of their human masters. Old Major dies soon after the meeting, but the animals, now inspired by his philosophy of Animalism, plot a rebellion against Jones. Two pigs, Snowball and Napoleon, prove themselves important figures and planners of this dangerous enterprise. When Jones forgets to feed the animals, the revolution occurs, and Jones and his men are chased off the farm. Manor Farm is renamed Animal Farm, and the Seven Commandments of Animalism are painted on the barn wall. Initially, the rebellion is a success: The animals complete the harvest and meet every Sunday to debate farm policy. The pigs, because of their intelligence, become the supervisors of the farm. Napoleon, however, proves to be a power hungry leader who steals the cows' milk and a number of apples to feed himself and the other pigs. He also enlists the services of Squealer, a pig with the ability to persuade the other animals that the pigs are always moral and correct in their decisions. Later that fall, Jones and his men return to Animal Farm and attempt to retake it. Thanks to the tactics of Snowball, the animals defeat Jones in what thereafter becomes known as The Battle of the Cowshed. Winter arrives, and Mollie, a vain horse concerned only with ribbons and sugar, is lured off the farm by another human. Snowball begins drawing plans for a windmill, which will provide electricity and thereby give the animals more leisure time, but Napoleon vehemently opposes such a plan on the grounds that building the windmill will allow them less time for producing food. On the Sunday that the pigs offer the windmill to the animals for a vote, Napoleon summons a pack of ferocious dogs, who chase Snowball off the farm forever. Napoleon announces that there will be no further debates; he also tells them that the windmill will be built after all and lies that it was his own idea, stolen by Snowball. For the rest of the novel, Napoleon uses Snowball as a scapegoat on whom he blames all of the animals' hardships. Much of the next year is spent building the windmill. Boxer, an incredibly strong horse, proves himself to be the most valuable animal in this endeavor. Jones, meanwhile, forsakes the farm and moves to another part of the county. Contrary to the principles of Animalism, Napoleon hires a solicitor and begins trading with neighboring farms. When a storm topples the half finished windmill, Napoleon predictably blames Snowball and orders the animals to begin rebuilding it. Napoleon's lust for power increases to the point where he becomes a totalitarian dictator, forcing "confessions" from innocent animals and having the dogs kill them in front of the entire farm. He and the pigs move into Jones' house and begin sleeping in beds (which Squealer excuses with his brand of twisted logic). The animals receive less and less food, while the pigs grow fatter. After the windmill is completed in August, Napoleon sells a pile of timber to Frederick, a neighboring farmer who pays for it with forged banknotes. Frederick and his men attack the farm and explode the windmill but are eventually defeated. As more of the Seven Commandments of Animalism are broken by the pigs, the language of the Commandments is revised: For example, after the pigs become drunk one night, the Commandment, "No animals shall drink alcohol" is changed to, "No animal shall drink alcohol to excess."Boxer again offers his strength to help build a new windmill, but when he collapses, exhausted, Napoleon sells the devoted horse to a knacker (a glue boiler). Squealer tells the indignant animals that Boxer was actually taken to a veterinarian and died a peaceful death in a hospital - a tale the animals believe. Years pass and Animal Farm expands its boundaries after Napoleon purchases two fields from another neighboring farmer, Pilkington. Life for all the animals (except the pigs) is harsh. Eventually, the pigs begin walking on their hind legs and take on many other qualities of their former human oppressors. The Seven Commandments are reduced to a single law: "All Animals Are Equal / But Some Are More Equal Than Others." The novel ends with Pilkington sharing drinks with the pigs in Jones' house. Napoleon changes the name of the farm back to Manor Farm and quarrels with Pilkington during a card game in which both of them try to play the ace of spades. As other animals watch the scene from outside the window, they cannot tell the pigs from the humans.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>Beloved</TITLE>
            <AUTHOR>Toni Morrison</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>Sethe, a thirteen year old child of unnamed slave parents, arrives at Sweet Home, an idyllic plantation in Kentucky operated by Garner, an unusually humane master, and his wife, Lillian. Within a year, Sethe selects Halle Suggs to be her mate and, by the time she is 18, bears him three children. After Garner dies, his wife turns control of the plantation over to her brother-in-law, the schoolteacher, who proves to be a brutal overseer. Schoolteacher's cruelty drives the Sweet Home slave men, Paul D, Halle, Paul A, and Sixo, to plot their escape. In August, fearful that her sons will be sold, a very pregnant Sethe packs her children Howard, Buglar, and Beloved in a wagon and sends them to safety with their grandmother in Cincinnati. Schoolteacher discovers what she has done, and as Halle watches from the loft of a barn, schoolteacher takes notes as his nephews, the "two boys with mossy teeth," suck the milk from Sethe's breasts. She reports the assault to the ailing Mrs. Garner. The nephews retaliate by beating Sethe with cowhide until her back is split open with wounds. Unknown to Sethe, schoolteacher roasts Sixo alive and hangs Paul A for trying to escape the plantation. Before she leaves Sweet Home, Sethe confronts Paul D, who is shackled in an iron collar for his part in the escape attempt. Sethe then makes her own escape. Sethe flees through the woods and, with the help of Amy Denver, a runaway white indentured servant, gives birth to her fourth child. Then, with the help of Stamp Paid, a black ferryman, she crosses the Ohio river into freedom. Safely reunited with her mother-in-law, Baby Suggs, and her babies in Cincinnati, Sethe enjoys 28 days of contentment. Then one day as Stamp Paid replenishes the woodpile and Baby Suggs and Sethe work in the yard, schoolteacher, the sheriff, a slave catcher, and one of schoolteacher's nephews arrive to recapture Sethe and her children. To spare her children a return to bondage, Sethe slices the throat of the eldest girl, tries to kill her two boys, and threatens to dash out the brains of her infant daughter, Denver. The sheriff takes Sethe and Denver to jail, and Sethe is condemned to hang. She leaves her cell long enough to attend her daughter's funeral. Three months later, pressure from the Quaker abolitionist Edward Bodwin and the Colored Ladies of Delaware, Ohio produces Sethe's freedom. She barters sex for a gravestone inscribed "Beloved" to mark her daughter's burial site. Immediately, Beloved's ghost makes itself known in Baby Suggs's house at 124 Bluestone Road. Sethe is granted a release from her death sentence, but after leaving jail she finds the black community closed to her. With the aid of Mr. Bodwin, she locates work and manages to build a stable, though solitary, life. Her mother-in-law withdraws completely from the community and dies several years later. Shortly after Baby Suggs's death, Sethe's sons leave home, unnerved by the presence of Beloved's ghost. Left with only Denver, Sethe lives in uneasy solitude. Years later, after escaping a cruel Georgia prison and wandering North, Paul D arrives in Cincinnati and reunites with Sethe. He immediately banishes the disruptive ghost from the house. The two former slaves attempt to form a family, although Denver is uncomfortable with Paul D's presence. Sethe and Paul D's relationship is interrupted by the appearance of a mysterious young woman who calls herself Beloved, the same name that is on the headstone of Sethe's murdered daughter. Beloved quickly becomes a dominant force in Sethe's house. She drives Paul D out of Sethe's bed and seduces him. She becomes the sole focus of Sethe's life after Sethe realizes that this young woman is the reincarnation of her dead child. Drawing Sethe into an unhealthy, obsessive relationship, Beloved grows stronger while Sethe's body and mind weaken. Sethe quits her job and withdraws completely into the house. With the aid of Denver and some female neighbors, Sethe escapes Beloved's control through a violent scene in which she mistakes Bodwin for a slave catcher and tries to stab him with an ice pick. Beloved vanishes, and Paul D returns, helping Sethe rediscover the value of life and her own self-worth.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>Beowulf</TITLE>
            <AUTHOR>Anonymous</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>Beowulf, a young warrior in Geatland (southwestern Sweden), comes to the Scyldings' aid, bringing with him 14 of his finest men. Hrothgar once sheltered Beowulf's father during a deadly feud, and the mighty Geat hopes to return the favor while enhancing his own reputation and gaining treasure for his king, Hygelac. At a feast before nightfall of the first day of the visit, an obnoxious, drunken Scylding named Unferth insults Beowulf and claims that the Geat visitor once embarrassingly lost a swimming contest to a boyhood acquaintance named Breca and is no match for Grendel. Beowulf responds with dignity while putting Unferth in his place. In fact, the two swimmers were separated by a storm on the fifth night of the contest, and Beowulf had slain nine sea monsters before finally returning to shore. While the Danes retire to safer sleeping quarters, Beowulf and the Geats bed down in Heorot, fully aware that Grendel will visit them. He does. Angered by the joy of the men in the mead hall, the ogre furiously bursts in on the Geats, killing one and then reaching for Beowulf. With the strength of 30 men in his hand grip, Beowulf seizes the ogre's claw and does not let go. The ensuing battle nearly destroys the great hall, but Beowulf emerges victorious as he rips Grendel's claw from its shoulder socket, sending the mortally wounded beast fleeing to his mere (pool). The claw trophy hangs high under the roof of Heorot. The Danes celebrate the next day with a huge feast featuring entertainment by Hrothgar's scop (pronounced "shop"), a professional bard who accompanies himself on a harp and sings or chants traditional lays such as an account of the Danes' victory at Finnsburh. This bard also improvises a song about Beowulf's victory. Hrothgar's wife, Queen Wealhtheow, proves to be a perfect hostess, offering Beowulf a gold collar and her gratitude. Filled with mead, wine, and great food, the entire party retires for what they expect to be the first peaceful night in years. But Grendel's mother, not quite as powerful as her son but highly motivated, climbs to Heorot that night, retrieves her son's claw, and murderously abducts one of the Scyldings (Aeschere) while Beowulf sleeps elsewhere. The next morning, Hrothgar, Beowulf, and a retinue of Scyldings and Geats follow the mother's tracks into a dark, forbidding swamp and to the edge of her mere. The slaughtered Aeschere's head sits on a cliff by the lake, which hides the ogres' underground cave. Carrying a sword called Hrunting, a gift from the chastised Unferth, Beowulf dives into the mere to seek the mother. Near the bottom of the lake, Grendel's mother attacks and hauls the Geat warrior to her dimly lit cave. Beowulf fights back once inside the dry cavern, but the gift sword, Hrunting, strong as it is, fails to penetrate the ogre's hide. The mother moves to kill Beowulf with her knife, but his armor, made by the legendary blacksmith Weland, protects him. Suddenly Beowulf spots a magical, giant sword and uses it to cut through the mother's spine at the neck, killing her. A blessed light unexplainably illuminates the cavern, disclosing Grendel's corpse and a great deal of treasure. Beowulf decapitates the corpse. The magic sword melts to its hilt. Beowulf returns to the lake's surface carrying the head and hilt but leaving the treasure. After more celebration and gifts and a sermon by Hrothgar warning of the dangers of pride and the mutability of time, Beowulf and his men return to Geatland. There he serves his king well until Hygelac is killed in battle and his son dies in a feud. Beowulf is then named king and rules successfully for 50 years. Like Hrothgar, however, his peace is shattered in his declining years. Beowulf must battle one more demon. A fiery dragon has become enraged because a lone fugitive has inadvertently discovered the dragon's treasure-trove and stolen a valuable cup. The dragon terrorizes the countryside at night, burning several homes, including Beowulf's. Led by the fugitive, Beowulf and eleven of his men seek out the dragon's barrow. Beowulf insists on taking on the dragon alone, but his own sword, Naegling, is no match for the monster. Seeing his king in trouble, one thane, Wiglaf, goes to his assistance. The others flee to the woods. Together, Wiglaf and Beowulf kill the dragon, but the mighty king is mortally wounded. Dying, Beowulf leaves his kingdom to Wiglaf and requests that his body be cremated in a funeral pyre and buried high on a seaside cliff where passing sailors might see the barrow. The dragon's treasure hoard is buried with him. It is said that they lie there still.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>Brave New World</TITLE>
            <AUTHOR>Aldous Huxley</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>The first scene, offering a tour of a lab where human beings are created and conditioned according to the society's strict caste system, establishes the antiseptic tone and the theme of dehumanized life. The natural processes of birth, aging, and death represent horrors in this world. Bernard Marx, an Alpha Plus high caste psychologist, emerges as the single discontented person in a world where material comfort and physical pleasure are the only concerns. Scorned by women, Bernard nevertheless manages to engage the attention of Lenina Crowne, a "pneumatic" beauty who agrees to spend a vacation week with him at the remote Savage Reservation in New Mexico, a place far from the controlled, technological world of London. Before Bernard leaves, his superior, the D.H.C., spontaneously reveals that long ago he, too, visited the Savage Reservation, and he confesses in sorrow that he lost the woman who accompanied him there. Embarrassed by the disclosure of his socially unacceptable emotion, the D.H.C. turns on Bernard, threatening him with banishment for his own social sins, not engaging enthusiastically enough in intercourse and soma. In the Savage Reservation with Lenina, Bernard meets a woman from London who gave birth to a son about 20 years before. Seeing his opportunity to gain power over the D.H.C., the father of the child, Bernard brings Linda and John back to London and presents them publicly to the D.H.C., who is about to banish Bernard. Shocked and humiliated by the proof of his horrifying connection with natural birth, the D.H.C. flees in terror. Once a social outcast, Bernard now enjoys great success, because of his association with the new celebrity, John, called "the Savage."Reared on the traditional ways of the Reservation and an old volume of the poetry of Shakespeare, John finds London strange, confusing, and finally repellent. His quotation of Miranda's line from The Tempest, "O brave new world/That has such people in it," at first expresses his awe of the "Other Place" his mother told him of as a child. But the quotation becomes ironic as John becomes more and more disgusted by the recreational sex, soma, and identical human beings of London. Lenina's attempted seduction provokes John's anger and violence, and, later, the death of Linda further arouses his fury. At last, John's attempt to keep a crowd of Deltas from their ration of soma results in a riot and his arrest, along with Bernard and Helmholtz Watson, an "emotional engineer" who wishes to be a poet. The three face the judgment of World Controller Mustapha Mond, who acknowledges the flaws of this brave new world, but pronounces the loss of freedom and individuality a small price to pay for stability. Mond banishes Bernard and Helmholtz to the Falkland Islands and rules that John must stay in London. When his two friends leave for their exile, John determines to make a retreat for himself in a remote, secluded lighthouse outside the city. There he tries to purify himself of civilization with ritual whippings and vomiting. Drawn by the spectacle of his wild penances, reporters and crowds press in on John, who becomes a public curiosity, a kind of human animal in a zoo. When Lenina appears in the crowd, John furiously attacks her with the whip. John's frenzy inflames the crowd, and, in accordance with their social training, the violence turns into a sexual orgy, with John drawn in more or less unwillingly. The next day, when John awakes from the effects of the soma, he realizes in horror what he has done. The novel closes on an image of John's body, hanging lifeless from a wooden beam in his lighthouse retreat.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>The Catcher in the Rye</TITLE>
            <AUTHOR>J.D. Salinger</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>Holden Caulfield, the seventeen year old narrator and protagonist of the novel, addresses the reader directly from a mental hospital or sanitarium in southern California. He wants to tell us about events that took place over a two day period the previous December. Typically, he first digresses to mention his older brother, D.B., who was once a "terrific" short story writer but now has sold out and writes scripts in nearby Hollywood. The body of the novel follows. It is a frame story, or long flashback, constructed through Holden's memory. Holden begins at Pencey Prep, an exclusive private school in Pennsylvania, on the Saturday afternoon of the traditional football game with school rival, Saxon Hall. Holden misses the game. Manager of the fencing team, he managed to lose the team's equipment on the subway that morning, resulting in the cancellation of a match in New York. He is on his way to the home of his history teacher, Mr. Spencer, to say goodbye. Holden has been expelled and is not to return after Christmas break, which begins Wednesday. Spencer is a well meaning but long winded old man, and Holden gladly escapes to the quiet of an almost deserted dorm. Wearing his new red hunting cap, he begins to read. His reverie is temporary. First, a dorm neighbor named Ackley disturbs him. Later, Holden argues with his roommate, Stradlater, who fails to appreciate a theme that Holden has written for him about Holden's deceased brother Allie's baseball glove. A womanizer, Stradlater has just returned from a date with Holden's old friend Jane Gallagher. The two roommates fight, Stradlater winning easily. Holden has had enough of Pencey Prep and catches a train to New York City where he plans to stay in a hotel until Wednesday, when his parents expect him to return home for Christmas vacation. En route to New York, Holden meets the mother of a Pencey classmate and severely distorts the truth by telling her what a popular boy her "rat" son is. Holden's Manhattan hotel room faces windows of another wing of the hotel, and he observes assorted behavior by "perverts." Holden struggles with his own sexuality. He meets three women in their thirties, tourists from Seattle, in the hotel lounge and enjoys dancing with one but ends up with only the check. Following a disappointing visit to Ernie's Nightclub in Greenwich Village, Holden agrees to have a prostitute, Sunny, visit his room. Holden has second thoughts, makes up an excuse, and pays the girl to leave. To his surprise, Maurice, her pimp, soon returns with her and beats up Holden for more money. He has lost two fights in one night. It is near dawn Sunday morning. After a short sleep, Holden telephones Sally Hayes, a familiar date, and agrees to meet her that afternoon to go to a play. Meanwhile, Holden leaves the hotel, checks his luggage at Grand Central Station, and has a late breakfast. He meets two nuns, one an English teacher, with whom he discusses Romeo and Juliet. Holden looks for a special record for his ten year old sister, Phoebe, called "Little Shirley Beans." He spots a small boy singing "If a body catch a body coming through the rye," which somehow makes Holden feel less depressed. Sally is snobbish and "phony," but the two watch a play featuring married Broadway stars Alfred Lunt and Lynn Fontanne. Sally and Holden skate at Radio City but fight when Holden tries to discuss things that really matter to him and suddenly suggests that they run off together. Holden leaves, sees the Christmas show at Radio City Music Hall, endures a movie, and gets very drunk. Throughout the novel, Holden has been worried about the ducks in the lagoon at Central Park. He tries to find them but only manages to break Phoebe's recording in the process. Exhausted physically and mentally, he heads home to see his sister. Holden and Phoebe are close friends as well as siblings. He tells her that the one thing he'd like to be is "the catcher in the rye." He would stand near the edge of a cliff, by a field of rye, and catch any of the playing children who, in their abandon, come close to falling off. When his parents return from a late night out, Holden, undetected, leaves the apartment and visits the home of Mr. Antolini, a favorite teacher, where he hopes to stay a few days. Startled, Holden awakes in the predawn hours to find Antolini patting Holden's head. He quickly leaves. Monday morning, Holden arranges to meet Phoebe for lunch. He plans to say goodbye and head west where he hopes to live as a deaf mute. She insists on leaving with him, and he finally agrees to stay. Holden's story ends with Phoebe riding a carrousel in the rain as Holden watches. In the final chapter, Holden is at the sanitarium in California. He doesn't want to tell us any more. In fact, the whole story has only made him miss people, even the jerks.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>The Canterbury Tales</TITLE>
            <AUTHOR>Geoffrey Chaucer</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>In April, with the beginning of spring, people of varying social classes come from all over England to gather at the Tabard Inn in preparation for a pilgrimage to Canterbury to receive the blessings of St. Thomas a Becket, the English martyr. Chaucer himself is one of the pilgrims. That evening, the Host of the Tabard Inn suggests that each member of the group tell tales on the way to and from Canterbury in order to make the time pass more pleasantly. The person who tells the best story will be awarded an elegant dinner at the end of the trip. The Host decides to accompany the party on its pilgrimage and appoints himself as the judge of the best tale. Shortly after their departure the day, the pilgrims draw straws. The Knight, who draws the shortest straw, agrees to tell the first story, a noble story about knights and honor and love. When the Knight finishes his story, the Host calls upon the Monk. The drunken Miller, however, insists that it is his turn, and he proceeds to tell a story about a stupid carpenter. At the end of his story, everyone roars with laughter, except the Reeve, who had once been a carpenter. To get back at the Miller, the Reeve tells a lowbrow story about a cheating miller. At the end of The Reeve's Tale, the Cook, Roger, promises to tell a true story, but he doesn't complete his tale. By now, the first day is rapidly passing, and the Host hurries the pilgrims to get on with their tales. Using the best legalese that he knows, he calls upon the Man of Law for the next tale. The Man of Law proceeds to tell the tale of Constancy. The Host is very pleased with the tale and asks the Parson to relate another one just as good. The Parson declines, however, and rebukes the Host for swearing and ridiculing him (the Parson). The Shipman breaks in and tells a lively story to make up for so much moralizing. The Wife of Bath is the next to tell a story, and she begins by claiming that happy marriages occur only when a wife has sovereignty over her husband. When the Wife of Bath finishes her story, the Friar offers his own tale about a summoner. The Host, however, always the peacekeeper, admonishes the Friar to let the Summoner alone. The Summoner interrupts and says the Friar can do as he likes and will be repaid with a tale about a friar. Nevertheless, the Friar's tale about a summoner makes the Summoner so angry that he tells an obscene story about the fate of all friars and then continues with an obscene tale about one friar in particular. After the Friar and Summoner finish their insulting stories about each other, the Host turns to the Clerk and asks for a lively tale. The Clerk tells a story about Griselda and her patience, a story that depicts the exact opposite of The Wife of Bath's Tale. The Merchant comments that he has no wife as patient and sweet as Griselda and tells of tale of a young wife who cheats on her old husband. After the Merchant's tale, the Host requests another tale about love and turns to the Squire, who begins a tale of supernatural events. He does not finish, however, because the Franklin interrupts him to compliment the Squire on his eloquence and gentility. The Host, interested only get in getting the next story told, commands the Franklin to begin his tale, which he does. The Franklin tells of a happy marriage. Then the Physician offers his tale of the tragic woe of a father and daughter, a story that upsets the Host so much that he requests a merry tale from the Pardoner. The Pardoner tells a tale in which he proves that, even though he is not a moral man, he can tell a moral tale. At the end of the tale, the Pardoner invites the pilgrims to buy relics and pardons from him and suggests that the Host should begin because he is the most sinful. This comment infuriates the Host; the Knight intercedes between the Host and the Pardoner and restores peace. The pilgrims then hear a story by the Prioress about a young martyr. After the seriousness of this tale, the Host turns to Chaucer and asks him for something to liven up the group. Chaucer begins a story about Sir Topas but is soon interrupted by the Host, who exclaims that he is tired of the jingling rhymes and wants Chaucer to tell a little something in prose. Chaucer complies with the boring story of Melibee. After the tale of Melibee, the Host turns to the merry Monk and demands a story that he confidently expects to be a jovial and happy tale. Instead, the Monk relates a series of tales in which tragedy befalls everyone. The Knight joins in with the Host in proclaiming that the Monk's tales are too much to bear and requests a merry tale. But the Monk refuses, and the Host turns to the Nun's Priest and calls for a tale. Thus the Nun's Priest relates the tale of the barnyard rooster, Chaunticleer, his lady, and a fox. The Second Nun then offers a tale that befits her station, a retelling of the events in the life of St. Cecilia. Suddenly, two men approach the pilgrims. One is a canon; the other his yeoman (servant). The Host welcomes them and asks whether either has a tale to tell. The Canon's Yeoman answers that his master has many strange tales filled with mirth and laughter, yet when he begins to tell of their life and actions, the Canon slips away embarrassed and frightened. As the party nears Canterbury, the Host demands a story from the Manciple, who tells of a white crow that can sing and talk. Finally, the Host turns to the last of the group, the Parson, and bids him to tell his tale. The Parson agrees and proceeds with a sermon. The Tales end with Chaucer's retraction.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>The Count of Monte Cristo</TITLE>
            <AUTHOR>Alexandre Dumas</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>Edmond Dantes, a handsome, promising young sailor, skillfully docks the three masted ship, the Pharaon, in Marseilles after its captain died en route home. As a reward, Dantes is promised a captainship, but before he can claim his new post and be married to his fiancee, Mercedes', a conspiracy of four jealous and unsavory men arrange for him to be seized and secretly imprisoned in solitary confinement in the infamous Chateau d'If, a prison from which no one has ever escaped. The four men responsible are: Fernand Mondego, who is jealous of Mercedes' love for Dantes; Danglars, the purser of the Pharaon, who covets Dantes' promised captainship; Caderousse, an unprincipled neighbor; and Villefort, a prosecutor who knows that Dantes is carrying a letter addressed to Villefort's father; the old man is a Bonapartist who would probably be imprisoned by the present royalist regime were it not for his son's, Villefort's, influence. Villefort fears, however, that this letter might damage his own position, and so he makes sure, he thinks, that no one ever hears about either Dantes or the letter again. For many years, Dantes barely exists in his tiny, isolated cell; he almost loses his mind and his will to live until one day he hears a fellow prisoner burrowing nearby. He too begins digging, and soon he meets an old Abbe who knows the whereabouts of an immense fortune, one that used to belong to an immensely wealthy Italian family. Dantes and the Abbe continue digging for several years, and from the Abbe, Dantes learns history, literature, science, and languages, but when at last they are almost free, the Abbe dies. Dantes hides his body, then sews himself in the Abbe's burial sack. The guards arrive, carry the sack outside, and heave the body far out to sea. Dantes manages to escape and is picked up by a shipful of smugglers, whom he joins until he can locate the island where the treasure is hidden. When he finally discovers it, he is staggered by the immensity of its wealth. And when he emerges into society again, he is the very rich and very handsome Count of Monte Cristo. Monte Cristo has two goals, to reward those who were kind to him and his aging father, and to punish those responsible for his imprisonment. For the latter, he plans slow and painful punishment. To have spent fourteen years barely subsisting in a dungeon demands cruel and prolonged punishment. As Monte Cristo, Dantes ingeniously manages to be introduced to the cream of Parisian society, among whom he goes unrecognized. But Monte Cristo, in contrast, recognizes all of his enemies, all now wealthy and influential men. Fernand has married Mercedes and is now known as Count de Morcerf. Monte Cristo releases information to the press that proves that Morcerf is a traitor, and Morcerf is ruined socially. Then Monte Cristo destroys Morcerf's relationship with his family, whom he adores. When they leave him, he is so distraught that he shoots himself. To revenge himself on Danglars, who loves money more than anything else, Monte Cristo ruins him financially. To revenge himself on Caderousse, Monte Cristo easily traps Caderousse because of his insatiable greed, then watches as one of Caderousse's cohorts murders him. To revenge himself on Villefort, Monte Cristo slowly reveals to Villefort that he knows about a love affair that Villefort had long ago with the present Madame Danglars. He also reveals to him, by hints, that he knows about an illegitimate child whom he fathered, a child whom Villefort believed that he buried alive. The child lived, however, and is now engaged to Danglars' daughter, who is the illegitimate young man's half-sister. Ironically, Villefort's wife proves to be even more villainous than her husband, for she poisons the parents of Villefort's first wife; then she believes that she has successfully poisoned her husband's daughter by his first marriage. With those people dead, her own son is in line for an enormous inheritance. Villefort, however, discovers his wife's plottings and threatens her, and so she poisons herself and their son. At this point, Dantes is half fearful that his revenge has been too thorough, but because he is able to unite two young people who are very much in love and unite them on the Isle of Monte Cristo, he sails away, happy and satisfied, never to be seen again.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>Beauty and the Beast</TITLE>
            <AUTHOR>Gabrielle-Suzanne Barbot de Villeneuve</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>There was once a merchant who had three daughters, the youngest of whom was so beautiful that everybody called her Beauty. This made the two eldest very jealous; and, as they were spiteful and bad-tempered by nature, instead of loving their younger sister they felt nothing but envy and hatred towards her. After some years there came a terrible storm at sea, and most of the merchant's ships were sunk, and he became very poor. He and his family were obliged to live in a very small house and do without the servants and fine clothes to which they had been used. The two eldest sisters did nothing but weep and lament for their lost fortune, but Beauty did her best to keep the house bright and cheerful, so that her father might not miss too much all the comfort and luxury to which he was used. One day the merchant told his daughters that he was going to take a journey into foreign lands in the hope of recovering some of his property. Then he asked them what they would like him to bring them home in case he should be successful. The eldest daughter asked for fine gowns and beautiful clothing; the second for jewels and gold and silver trinkets."And Beauty. What would Beauty like?" asked the father. Beauty was so happy and contented always that there was scarcely anything for which she longed. She thought for a moment, then she said:"I should like best of all a red rose!" The other sisters burst out laughing and scoffed at Beauty's simple request; but her father promised to bring her what she wanted. Then he said goodbye to his children and set out on his travels. He was away for nearly a year, and was so fortunate as to win back a great part of his lost wealth. When the time came for his return, he was easily able to buy the things his eldest daughters wished for; but nowhere could he find a red rose to take home to Beauty, and at last he was obliged to set off without one. When he was within a few miles journey of his home, he lost himself in a thick wood. Darkness came on, and he began to be afraid that he would have to pass the night under a tree, when suddenly he saw a bright light shining in the distance. He went towards it, and on his approach found it came from a great castle that was set right in the heart of the forest. The merchant made up his mind to ask if he might spend the night there; but to his surprise, when he reached the door he found it set wide open, and nobody about. After awhile, finding that no one came in answer to his repeated knocking, he walked inside. There he found a table laid with every delicacy, and, being very hungry, he sat down and made a good repast. After he had finished his supper he laid himself down on a luxurious couch, and in a few minutes was fast asleep. In the morning, after eating a hearty breakfast, which he found prepared for him, he left the mysterious castle, without having set eyes on a single person. As he was passing through the garden he found himself in an avenue of rose trees, all covered with beautiful red roses."Here are such thousands of flowers," he said to himself, "that, surely, one bud will not be missed;" and, thinking of Beauty, he broke off a rose from one of the bushes. Scarcely had he done so when he heard a terrible noise, and, turning round, he saw coming towards him a hideous Beast, who exclaimed in an awful tone:"Ungrateful wretch! You have partaken of my hospitality, have eaten of my food, have slept in my house, and in return you try to rob me of my roses. For this theft you shall die!"The merchant fell on his knees and begged for pardon, but the Beast would not listen to him."Either you must die now, or else you must swear to send me in your stead the first living thing that meets you on your return home," he said; and the merchant, overcome with terror, and thinking that one of his dogs would be sure to be the first creature to greet him, gave his promise. But to his horror and dismay, it was his youngest daughter, Beauty, who first ran out to greet him on his return. She had seen him coming from afar, and hastened to welcome him home. She did not at first understand her father's grief at seeing her; but when he told her the story of the Beast and his promise she did her best to comfort him."Do not fear, dear father," she said, "perhaps the Beast will not prove so terrible as he looks. He spared your life; he may spare mine, since I have done him no harm."Her father shook his head mournfully; but there was no help for it. He had promised to send the Beast the first living creature that met him on his return, so he was obliged to send Beauty herself in his place. When he left Beauty at the palace of the Beast she found everything prepared for her comfort and convenience. A beautiful bedchamber was ready for her use; the rooms were filled with everything that she could possibly want, and in the great hall of the castle a table was set with every delicacy. And everywhere there were bowls full of red roses. No servants were visible; but there was no lack of service, for invisible hands waited upon her and attended to her every want. She had but to wish, and whatever she wanted was at once placed before her. Beauty was filled with astonishment at all this luxury and magnificence."Surely the Beast does not wish to harm me," she thought, "or he would never have so ordered everything for my comfort." And she waited with a good courage for the coming of the Lord of the Castle. In the evening the beast appeared. He was certainly very terrible to look at, and Beauty trembled at the sight of the hideous monster. But she forced herself to appear brave, and, indeed, there was no cause for her alarm. The Beast was kindness itself, and so gentle and respectful in his attentions to her that Beauty soon lost all fear. She soon became very fond of him, and would have been quite happy had it not been for the thought of her father and sisters, and the grief which she knew her father would be suffering on her account. The thought of his sorrow made her sorrowful too; and one night, when the Beast came to visit her at his usual hour, she was so sad that he asked her what was the matter. Then Beauty begged him to let her go and visit her father. The Beast was very unwilling to grant her request."If I let you go, I am afraid you will never come back to me," he said, "and then I shall die of grief."Beauty promised most earnestly to come back to him if he would only allow her to spend a few days with her family; and at last the Beast yielded to her entreaties. He gave her a ring, saying:"Put this on your little finger when you go to bed tonight, and wish; and in the morning you will find yourself at home in your father's house. But if you do not return to me at the end of a week, I shall die of sorrow."Beauty's father was almost overcome with joy at seeing his daughter again, and he was delighted to hear of her happiness and good fortune. But her two sisters, who in the meantime had married, were more jealous than ever of their beautiful sister. They were not very happy with their husbands, who were poor and not over-lovable; and they were very envious of Beauty's clothes and of all the luxuries with which she told them she was surrounded. They tried to think of a plan by which they could prevent their sister from enjoying her good fortune."Let us keep her beyond the week that the Beast has allowed her," they said; "then, doubtless, he will be so angry that he will kill her."So they pretended to be very fond of Beauty, and when the time came for her return, they overwhelmed her with tears and caresses, begging her not to leave them, and to stay at least one more day with them. Beauty was distressed at their grief, and at last she consented to stay just one more day; though her heart misgave her sorely when she thought of the poor Beast. That night, as she lay in bed, she had a dream. She dreamt that she saw the Beast dying of sorrow at her forgetfulness; and so real did it seem that she woke up in an agony of dismay."How could I have been so cruel and ungrateful," she cried. "I promised faithfully that I would return at the end of the week. What will he think of me for breaking my promise!"Hastily rising from bed, she searched for the ring the Beast had given her. Then putting it on her little finger she wished to be at the Palace of the Beast again. In a moment she found herself there; and quickly putting on her clothes she hurried out to look for the Beast. She searched through room after room; but nowhere could she find him. At last she ran out into the garden; and there, on a plot of grass, where he and she had often sat together, she found him lying as if dead upon the ground. With a bitter cry she sank on her knees beside the poor Beast."Oh, Beast; my dear, dear Beast!" she cried. "How could I have been so cruel and wicked and unkind? He has died of sorrow as he said he would!" And the tears fell down from her eyes as she spoke. Overcome with grief and remorse, she stooped down and tenderly kissed the ugly Beast. In a moment there was a sudden noise, and Beauty was startled to find that the ugly Beast had vanished. The Beast was a beast no longer, but a handsome Prince, who knelt at her feet, thanking her for having broken his enchantment."A wicked fairy," he said, "condemned me to keep the form of a beast until a beautiful maiden should forget my ugliness and kiss me. You, by your love and tenderness, have broken the spell and released me from my horrible disguise. Now, thanks to you, I can take my proper form again." And then he begged Beauty to become his bride. So Beauty married the Prince who had been a Beast, and they lived together in the castle and ruled over the Prince's country, and were happy ever after.</TEXT>
        </SUMMARY>
    </ENTRY>
    <ENTRY>
        <SUMMARY>
            <TITLE>Aladdin and the Wonderful Lamp</TITLE>
            <AUTHOR>Unknown</AUTHOR>
            <TYPE>Summary</TYPE>
            <TEXT>Aladdin was the only son of a poor widow who lived in China; but instead of helping his mother to earn their living, he let her do all the hard work, while he himself only thought of idling and amusement. One day, as he was playing in the streets, a stranger came up to him, saying that he was his father's brother, and claiming him as his long lost nephew. Aladdin had never heard that his father had had a brother; but as the stranger gave him money and promised to buy him fine clothes and set him up in business, he was quite ready to believe all that he told him. The man was a magician, who wanted to use Aladdin for his own purposes. The next day the stranger came again, brought Aladdin a beautiful suit of clothes, gave him many good things to eat, and took him for a long walk, telling him stories all the while to amuse him. After they had walked a long way, they came to a narrow valley, bounded on either side by tall, gloomy looking mountains. Aladdin was beginning to feel tired, and he did not like the look of this place at all. He wanted to turn back; but the stranger would not let him. He made Aladdin follow him still farther, until at length they reached the place where he intended to carry out his evil design. Then he made Aladdin gather sticks to make a fire, and when they were in a blaze he threw into them some powder, at the same time saying some mystical words, which Aladdin could not understand. Immediately they were surrounded with a thick cloud of smoke. The earth trembled, and burst open at their feet, disclosing a large flat stone with a brass ring fixed in it. Aladdin was so terribly frightened that he was about to run away; but the Magician gave him such a blow on the ear that he fell to the ground. Poor Aladdin rose to his feet with eyes full of tears, and said reproachfully, "Uncle, what have I done that you should treat me so?""You should not have tried to run away from me," said the Magician, "when I have brought you here only for your own advantage. Under this stone there is hidden a treasure which will make you richer than the richest monarch in the world. You alone may touch it. If I assist you in any way the spell will be broken, but if you obey me faithfully, we shall both be rich for the rest of our lives. Come, take hold of the brass ring and lift the stone."Aladdin forgot his fears in the hope of gaining this wonderful treasure, and took hold of the brass ring. It yielded at once to his touch, and he was able to lift the great stone quite easily and move it away, which disclosed a flight of steps, leading down into the ground."Go down these steps," commanded the Magician, "and at the bottom you will find a great cavern, divided into three halls, full of vessels of gold and silver; but take care you do not meddle with these. If you touch anything in the halls you will meet with instant death. The third hall will bring you into a garden, planted with fine fruit trees. When you have crossed the garden, you will come to a terrace, where you will find a niche, and in the niche a lighted lamp. Take the lamp down, and when you have put out the light and poured away the oil, bring it to me. If you would like to gather any of the fruit of the garden you may do so, provided you do not linger."Then the Magician put a ring on Aladdin's finger, which he told him was to preserve him from evil, and sent him down into the cavern. Aladdin found everything just as the Magician had said. He passed through the three halls, crossed the garden, took down the lamp from the niche, poured out the oil, put the lamp into his bosom, and turned to go back. As he came down from the terrace, he stopped to look at the trees of the garden, which were laden with wonderful fruits. To Aladdin's eyes it appeared as if these fruits were only bits of colored glass, but in reality they were jewels of the rarest quality. Aladdin filled his pockets full of the dazzling things, for though he had no idea of their real value, yet he was attracted by their dazzling brilliance. He had so loaded himself with these treasures that when at last he came to the steps he was unable to climb them without assistance."Pray, Uncle," he said, "give me your hand to help me out.""Give me the lamp first," replied the Magician."Really, Uncle, I cannot do so until I am out of this place," answered Aladdin, whose hands were, indeed, so full that he could not get at the lamp. But the Magician refused to help Aladdin up the steps until he had handed over the lamp. Aladdin was equally determined not to give it up until he was out of the cavern, and, at last, the Magician fell into a furious rage. Throwing some more of the powder into the fire, he again said the magic words. No sooner had he done so than there was a tremendous thunderclap, the stone rolled back into its place, and Aladdin was a prisoner in the cavern. The poor boy cried aloud to his supposed uncle to help him; but it was all in vain, his cries could not be heard. The doors in the garden were closed by the same enchantment, and Aladdin sat down on the steps in despair, knowing that there was little hope of his ever seeing his Mother again. For two terrible days he lay in the cavern waiting for death. On the third day, realizing that it could not now be far off, he clasped his hands in anguish, thinking of his Mother's sorrow; and in so doing he accidently rubbed the ring which the Magician had put upon his finger. Immediately a genie of enormous size rose out of the earth, and, as Aladdin started back in fright and horror, said to him:"What wouldst thou have of me?""Who are you?" gasped Aladdin."I am the slave of the ring. I am ready to obey thy commands," came the answer. Aladdin was still trembling; but the danger he was in already made him answer without hesitation:"Then, if you are able, deliver me, I beseech you, from this place."Scarcely had he spoken, when he found himself lying on the ground at the place to which the Magician had first brought him. He hastened home to his Mother, who had mourned him as dead. As soon as he had told her all his adventures, he begged her to get him some food, for he had now been three days without eating."Alas, child!" replied his Mother, "I have not a bit of bread to give you.""Never mind, Mother," said Aladdin, "I will go and sell the old lamp which I brought home with me. Doubtless I shall get a little money for it."His Mother reached down the lamp; but seeing how dirty it was, she thought it would sell better if she cleaned it. But no sooner had she begun to rub it than a hideous genie appeared before her, and said in a voice like thunder:"What wouldst thou have of me? I am ready to obey thy commands, I and all the other slaves of the lamp."Aladdin's Mother fainted away at the sight of this creature; but Aladdin, having seen the genie of the ring, was not so frightened, and said boldly:"I am hungry, bring me something to eat."The genie disappeared, but returned in an instant with twelve silver dishes, filled with different kinds of savory meats, six large white loaves, two bottles of wine, and two silver drinking cups. He placed these things on the table and then vanished. Aladdin fetched water, and sprinkling some on his Mother's face soon brought her back to life again. When she opened her eyes and saw all the good things the genie had provided, she was overcome with astonishment."To whom are we indebted for this feast?" she cried. "Has the Sultan heard of our poverty and sent us these fine things from his own table?""Never mind now how they came here," said Aladdin. "Let us first eat, then I will tell you."Mother and son made a hearty meal, and then Aladdin told his Mother that it was the genie of the lamp who had brought them the food. His Mother was greatly alarmed, and begged him to have nothing further to do with genies, advising him to sell the lamp at once. But Aladdin would not part with such a wonderful possession, and resolved to keep both the ring and the lamp safely, in case he should ever need them again. He showed his Mother the fruits which he had gathered in the garden, and his Mother admired their bright colors and dazzling radiance, though she had no idea of their real value. Not many days after this, Aladdin was walking in the streets of the city, when he heard a fanfare of trumpets announcing the passing of the Princess Badroulboudour, the Sultan's only daughter. Aladdin stopped to see her go by, and was so struck by her great beauty that he fell in love with her on the spot and made up his mind to win her for his bride."Mother," he said, "I cannot live without the Princess Badroulboudour. You must go to the Sultan and demand her hand in marriage for me."Aladdin's Mother burst out laughing at the idea of her son wishing to be the son-in-law of the Sultan, and told him to put such thoughts out of his head at once. But Aladdin was not to be laughed out of his fancy. He knew by this time that the fruits which he had gathered from the magic garden were jewels of great value, and he insisted upon his Mother taking them to the Sultan for a present, and asking the hand of the Princess in marriage for her son. The poor woman was terribly frightened, fearing lest the Sultan should punish her for her impudence; but Aladdin would hear of no excuses, and at last she set forth in fear and trembling, bearing the jewels on a china dish covered with a napkin. When she came before the Sultan, she told him, with many apologies and pleas for forgiveness, of her son's mad love for the Princess Badroulboudour. The Sultan smiled at the idea of the son of a poor old woman asking for the hand of his daughter, and asked her what she had under the napkin. But when the woman uncovered the jewels, he started up from his throne in amazement, for he had never before seen so many large and magnificent jewels collected together. He thought Aladdin must be a very unusual and extraordinary person to be able to make him such a valuable present, and he began to wonder whether it might not be worth while to bestow the Princess's hand upon him. However, he thought he would ask for some further proof of his wealth and power; so, turning to the woman, he said:"Good Mother, tell your son he shall have the Princess Badroulboudour for his wife as soon as he sends me forty basins of gold, filled with jewels as valuable as these, and borne by forty slaves. Hasten now and carry him my message. I will await your return."Aladdin's Mother was dismayed at this request."Where can Aladdin get such basins and jewels and slaves?" she thought, as she hurried home to him. But Aladdin only smiled when his Mother gave him the Sultan's message. He rubbed the lamp, and at once the genie stood before him, asking him what was his pleasure."Go," said Aladdin, "fetch me forty basins all of massive gold, full of jewels, borne by forty slaves."The genie brought these things at once, and Aladdin then sent his Mother with them to the Sultan. The Sultan was amazed at this wonderful show of wealth and at the quickness with which it had been brought, and he sent for Aladdin to come to the Court. Aladdin first summoned the genie to bring him fine clothes and a splendid horse, and a retinue fit for the future son-in-law of the Sultan; and then, with a train of slaves bearing magnificent presents for the Princess, he set out for the Palace. The Sultan would have married him to his daughter at once; but Aladdin asked him to wait until the next morning, when he hoped to have a Palace worthy to receive his wife. Once again he summoned the genie to his aid, and commanded him to build a Palace that in beauty and magnificence should surpass any that had ever been built on the earth before. The next morning when the Sultan awoke and looked out of his window, he saw, opposite to his own, the most wonderful Palace he had ever seen. The walls were built of gold and silver, and encrusted with diamonds, rubies and emeralds, and other rare and precious stones. The stables were filled with the finest horses; beautiful gardens surrounded the building, and everywhere were hundreds of slaves and servants to wait on the Princess. The Sultan was so overcome with all this magnificence, that he insisted upon marrying his daughter to Aladdin that very day, and the young couple took up their residence in the Palace the genie had built. For a time they lived very happily, but the Magician, who had gone to Africa after he had left Aladdin to perish in the cavern, at length happened to hear of Aladdin's fame and riches; and guessing at once the source of all this wealth, he returned once more to China, determined to gain possession of the magic lamp. He bought a number of new and beautiful lamps, disguised himself as an old beggar, and then, waiting until Aladdin was out hunting, he came to the windows of the Palace, crying out:"New lamps for old; new lamps for old."When the Princess heard this strange cry she was very much amused."Let us see," she said to her ladies, "whether this foolish fellow means what he says; there is an ugly old lamp in Aladdin's room," and taking the precious lamp, which Aladdin always kept by his bedside, she sent it out to the old man by one of the slaves, saying, "Give me a new lamp for this!" The Magician was overjoyed. He saw at once that it was the very lamp he wanted, and giving the Princess the best of the new ones in exchange, he hurried away with his treasure. As soon as he found himself alone, he summoned the slave of the lamp, and told him to carry himself, the Palace, and the Princess Badroulboudour to the farthest corner of Africa. This order the genie at once obeyed. When Aladdin returned from hunting and found that his wife and his Palace had vanished, he was overcome with anguish, guessing that his enemy, the Magician, had by some means got possession of the lamp. The Sultan, whose grief and anger at the loss of his daughter were terrible, ordered him to leave the Court at once, and told him that unless he returned in forty days with the Princess safe and well, he would have him beheaded. Aladdin went out from the Sultan's presence, not knowing what to do or where to turn. But after he had wandered about for some time in despair, he remembered the ring which he still wore on his finger. He rubbed it, and in a moment the genie stood before him. But when Aladdin commanded him to bring back the Palace and the Princess, the genie answered, "What you command is not in my power. You must ask the slave of the lamp. I am only the slave of the ring.""Then," said Aladdin, "if you cannot bring my Palace to me, I command you to take me to my Palace." No sooner were the words out of his mouth than he found himself standing in Africa, close to the missing Palace. The Princess Badroulboudour, who, since the moment when the Magician had had her in his power, had not ceased to weep and lament for her foolishness in exchanging the lamp, happened to be looking out of the window; and when she saw Aladdin she nearly fainted with joy, and sent a slave to bring him secretly into the Palace. Then she and Aladdin made a plan to get the better of the Magician and to recover the lost lamp. Aladdin summoned the genie of the ring, who procured for him a very powerful sleeping powder, which he gave to the Princess. Then Aladdin hid himself behind some curtains in the room, and the Princess sent a message to the Magician asking him to take supper with her. The Magician was delighted at the Princess's invitation, and accepted it joyfully, never dreaming that Aladdin had found his way to Africa. As they were eating and drinking together, the Princess put the sleeping powder into the Magician's cup of wine, and no sooner had he tasted it than he fell down in a deep sleep as if dead. This was Aladdin's chance. Hastily coming out from behind the curtains, he snatched the lamp from the Magician's bosom, and called the genie to come to his assistance. The genie, having first thrown out the Magician, then carried the Palace with the Princess and Aladdin back to the spot from which it had been taken. Great was the Sultan's joy at receiving back his daughter. The whole city was given over to rejoicings, and for ten days nothing was heard but the sound of drums and trumpets and cymbals, and nothing was seen but illuminations and gorgeous entertainments in honor of Aladdin's safe return. Aladdin and the Princess ascended the throne after the Sultan died and they lived long and happily and had many beautiful children.</TEXT>
        </SUMMARY>
	</ENTRY>
	<ENTRY>
		<FABLES>
			<TITLE>The Lion and the Mouse</TITLE>
			<AUTHOR>Aesop</AUTHOR>
			<TYPE>Fables</TYPE>
			<TEXT>Long ago in a dense jungle far away there lived a mighty lion whom all the other creatures feared very much. King of the jungle as he was, the terrible beast knew no fear and loved the respect he received from all the animals in the forest. He used to spend half his day in hunting and the other half in sleeping. No creature dared to come near his den at any time of the day, especially when he was sleeping because he got terribly angry if his sleep was disturbed in any way. But one day a little mouse got curious to see how the lion's den looked like. So he set out for the cave where the lion rested. When he got near, he could not see the lion. Thinking the lion would be gone for awhile, the little mouse ran and sneaked into the cave. It was a dark, desolate place but big enough for the lion to live. As soon as he thought he should head back he heard the sound of the footsteps of the lion. The little mouse began to tremble with fear. The mouse hid himself in the dark inside of the cave and saw the huge shadow of the lion falling on the floors. The lion sat near the entrance of the cave and rested his head on his huge paws. Soon he was fast asleep. The whole cave seemed to tremble with the loud snoring of the jungle king. The mouse tried to creep out as stealthily as he could. Soon he was near the entrance. But as he tried to cross the lion, his little tail grazed against the left paw of the beast and the lord of the jungle woke up with a start. Imagine his anger and the roar he gave when he saw the puny mouse in his den. The lion placed his huge paw upon the mouse's tail and opened his big jaws to swallow the mouse when the mouse started to plead for his life. He offered to help the lion at any time in the future in return for his mercy. The lion was amused at this thought. How can the little mouse help him? But he let him go and roared with laughter. The mouse ran for his life, thanking the stars. A few days later, as the lion was prowling majestically through the jungle, it was suddenly caught in a hunter's snare. He struggled furiously to break free. But for all his efforts, he only found himself getting even more entangled in the net of ropes. He roared out of anger and helplessness. The whole jungle began to shake due to the terrible sound and every animal heard the cries of the beast. The mouse heard it too. Realizing the lion was in trouble, the mouse ran as fast as he could to the place where the sounds were coming from. Soon he found the lion trapped in the hunter's snare. Without wasting a second, he began nibbling through the ropes with his sharp little teeth. Very soon the lion was free. The lion humbly thanked the mouse and confessed how wrong he was in thinking that the mouse could never be able to help him. The two creatures became the best of friends from that day on. No matter how weak and small a creature is, he may be of help if the time comes.</TEXT>
		</FABLES>
	</ENTRY>
	<ENTRY>
		<FABLES>
			<TITLE>The Tortoise and the Hare</TITLE>
			<AUTHOR>Aesop</AUTHOR>
			<TYPE>Fables</TYPE>
			<TEXT>The hare was once boasting of his speed before the other animals. He started bragging about never losing a race and challenged any animal in the forest to race against him. A tortoise standing nearby calmly accepted the challenge. The hare started laughing until the tortoise asked where they would race to. The pair agreed to a course and made a starting line. The hare darted out of sight at once, but after running for a while decided to lay down and have a nap since he was so far ahead. While he was sleeping the tortoise plodded on and on, slowly but surely. By the time the hare awoke from his nap he noticed the tortoise seconds away from the finish line. Even running as fast as he could the hare could not catch up to the tortoise to save the race and the tortoise won. Slow and steady wins the race.</TEXT>
		</FABLES>
	</ENTRY>
	<ENTRY>
		<FABLES>
			<TITLE>The Horse and the Stag</TITLE>
			<AUTHOR>Aesop</AUTHOR>
			<TYPE>Fables</TYPE>
			<TEXT>The horse had the plain entirely to himself. A stag intruded into his domain and shared his pasture. The horse wanted to drive off the stranger from his land. He asked a man nearby to help him in punishing the stag. The man replied that if the horse would receive a bit in his mouth and agree to carry him the man would forever hunt the stag. The horse consented and allowed the man to mount him. From that hour on he found that instead of obtaining revenge on the stag he had enslaved himself to the service of man. He who seeks to injure others often injures only himself.</TEXT>
		</FABLES>
	</ENTRY>
	<ENTRY>
		<FABLES>
			<TITLE>The Boy Who Cried Wolf</TITLE>
			<AUTHOR>Aesop</AUTHOR>
			<TYPE>Fables</TYPE>
			<TEXT>There was once a shepherd boy who was bored as he sat watching his sheep. To amuse himself he loudly cried out that a wolf was after his sheep. All the villagers came running to help the boy. When they arrived they found no wolf. The boy laughed at the sight of their angry faces. The villagers asked that the boy only call for help if he needed it, and walked away a little agitated. Later the boy cried out about a wolf again. He was even more delighted when he saw the villagers running to his aid yet again. The villagers sternly told the boy not to cry out falsely again. The next day a real wolf approached and began hunting the boy's sheep. No matter how loud the boy yelled no body came to help him. The wolf drove nearly all of his sheep away. Nobody believes a liar even when he is telling the truth.</TEXT>
		</FABLES>
	</ENTRY>
	<ENTRY>
		<FABLES>
			<TITLE>The Fox and the Crow</TITLE>
			<AUTHOR>Aesop</AUTHOR>
			<TYPE>Fables</TYPE>
			<TEXT>A fox once saw a crow fly off with a piece of cheese in its beak and settle on a branch of a tree. The fox walked up to the foot of the tree. He greeted the crow and began to compliment the crow. He commented on her pretty feathers, bright eyes, and her amazing voice. He asked that he might hear her beautiful song. The crow lifted up her head and began to caw her best. The moment she opened her mouth the piece of cheese fell to the ground. The fox quickly ate the cheese and in exchange for the meal offered some advice for the future. Do not trust flatterers.</TEXT>
		</FABLES>
	</ENTRY>
	<ENTRY>
		<FABLES>
			<TITLE>The Fox and the Goat</TITLE>
			<AUTHOR>Aesop</AUTHOR>
			<TYPE>Fables</TYPE>
			<TEXT>Once upon a time a fox fell down a well. He was stuck there for quite a while. Finally, a goat wandered by. The goat asked the fox what he was doing down in the well. The fox snarled at the goat and demanded he leave his water alone. The goat did not think it was fair for the fox to keep all the water to himself and jumped into the well. Quick as a flash, the fox leaped on the goat's back and out of the well. He ran happily off, leaving the goat stuck in the well. Do not always believe what you hear from someone who is in trouble.</TEXT>
		</FABLES>
	</ENTRY>
	<ENTRY>
		<FABLES>
			<TITLE>The Ugly Duckling</TITLE>
			<AUTHOR>Hans Christian Anderson</AUTHOR>
			<TYPE>Fables</TYPE>
			<TEXT>One day a mother duck noticed her eggs were hatching. One by one they hatched until the largest one was left. Finally it hatched and out came an ugly gray baby duck. As the baby ducks grew older the ugly duckling became unhappy. Everyone made fun of him for being different and ugly. Finally one day he decided to run away. He saw some beautiful white birds and longed to be with them. Winter came and the ugly duckling was very cold and unhappy. Spring came and the ugly duckling saw the pretty swans again. He wanted to join them but was too afraid of the rejection. Running away into the water he saw his reflection and realized he is no more an ugly duckling but is now a beautiful swan. Do not doubt yourself nor put down others who have yet to blossom into a beautiful swan.</TEXT>
		</FABLES>
	</ENTRY>
	<ENTRY>
		<FABLES>
			<TITLE>The Three Little Pigs</TITLE>
			<AUTHOR>Unknown</AUTHOR>
			<TYPE>Fables</TYPE>
			<TEXT>Once upon a time there were three little pigs. The first pig wanted to relax and play all day. He chose to build his house out of straw since it would be the easiest and the quickest. The second pig wanted to play too. He chose to build his house out of sticks since it would be easy yet better than straw. The third pig wanted to play but also wanted to build a sturdy house that would last forever. He decided to build his house out of brick since it would be the strongest even though it would take the most amount of work. The first two pigs were finished in no time and started playing. The third pig wanted to play too, but instead worked hard to finish his house. Later a wolf was looking for food and found the house of the first pig. Huffing and puffing he blew down the house. The first pig fled to the house of the second pig. The wolf followed the pig and blew down the stick house as well. The first two pigs then fled to the house of the third pig. The wolf huffed and puffed but could not blow down the brick house. The first two pigs learned that hard work and dedication pays off.</TEXT>
		</FABLES>
	</ENTRY>
    <ENTRY>
        <LYRICS>
            <TITLE>Yesterday</TITLE>
            <AUTHOR>The Beatles</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>Yesterday, all my troubles seemed so far away
now it looks as though they're here to stay
oh, I believe in yesterday
suddenly, I'm not half the man I used to be
there's a shadow hanging over me.
Oh, yesterday came suddenly
why she had to go I don't know she wouldn't say
I said something wrong, now I long for yesterday
yesterday, love was such an easy game to play
now I need a place to hide away
oh, I believe in yesterday
why she had to go I don't know she wouldn't say
I said something wrong, now I long for yesterday
yesterday, love was such an easy game to play
now I need a place to hide away
oh, I believe in yesterday</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>Hey Jude</TITLE>
            <AUTHOR>The Beatles</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>Hey Jude, don't make it bad
take a sad song and make it better
remember to let her into your heart
then you can start to make it better
hey Jude, don't be afraid
you were made to go out and get her
the minute you let her under your skin
then you begin to make it better
and anytime you feel the pain, hey Jude, refrain
don't carry the world upon your shoulders
for well you know that it's a fool who plays it cool
by making his world a little colder
hey Jude, don't let me down
you have found her, now go and get her
remember to let her into your heart
then you can start to make it better
so let it out and let it in, hey Jude, begin
you're waiting for someone to perform with
and don't you know that it's just you, hey Jude, you'll do
the movement you need is on your shoulder
hey Jude, don't make it bad
take a sad song and make it better
remember to let her under your skin
then you'll begin to make it
Better better better oh</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>With A Little Help From My Friends</TITLE>
            <AUTHOR>The Beatles</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>What would you think if I sang out of tune
would you stand up and walk out on me?
Lend me your ears and I'll sing you a song
and I'll try not to sing out of key
oh I get by with a little help from my friends
I get high with a little help from my friends
going to try with a little help from my friends
what do I do when my love is away?
Does it worry you to be alone?
How do I feel by the end of the day?
Are you sad because you're on your own?
No I get by with a little help from my friends
I get high with a little help from my friends
going to try with a little help from my friends
do you need anybody?
I need somebody to love
could it be anybody?
I want somebody to love
would you believe in a love at first sight?
Yes I'm certain that it happens all the time
what do you see when you turn out the light?
I can't tell you, but I know it's mine
oh I get by with a little help from my friends
I get high with a little help from my friends
I'm going to try with a little help from my friends
do you need anybody?
I just need somebody to love
could it be anybody?
I want somebody to love
oh I get by with a little help from my friends
going to try with a little help from my friends
oh I get high with a little help from my friends
yes I get by with a little help from my friends
with a little help from my friends</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>Livin' On A Prayer</TITLE>
            <AUTHOR>Bon Jovi</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>working for her man, she brings home her pay 
for love, for love 
she says we've got to hold on to what we've got 
'cause it doesn't make a difference 
if we make it or not 
we've got each other and that's a lot 
for love, we'll give it a shot 
whoa! we're half way there 
whoa! livin' on a prayer 
take my hand and we'll make it, I swear 
whoa! livin' on a prayer 
Tommy's got his six string in hock 
now he's holding in what he used 
to make it talk, so tough, it's tough 
Gina dreams of running away 
when she cries in the night 
Tommy whispers baby it's okay, someday 
we've got to hold on to what we've got 
'cause it doesn't make a difference 
if we make it or not 
we've got each other and that's a lot 
for love, we'll give it a shot 
whoa! we're half way there 
whoa! livin' on a prayer 
take my hand and we'll make it, I swear 
whoa! livin' on a prayer 
We've got to hold on ready or not 
you live for the fight when it's all that you've got</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>Wanted Dead or Alive</TITLE>
            <AUTHOR>Bon Jovi</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>It's all the same, only the names will change
everyday it seems we're wasting away
another place where the faces are so cold
I'd drive all night just to get back home
I'm a cowboy, on a steel horse I ride
I'm wanted dead or alive
wanted dead or alive
sometimes I sleep, sometimes it's not for days
and the people I meet always go their separate ways
sometimes you tell the day
by the bottle that you drink
and times when you're alone all you do is think
I walk these streets, a loaded six string on my back
I play for keeps, 'cause I might not make it back
I been everywhere, and I'm standing tall
I've seen a million faces and I've rocked them all
I'm a cowboy, on a steel horse I ride
I'm wanted dead or alive
I'm a cowboy, I got the night on my side
I'm wanted dead or alive
and I ride, dead or alive
I still drive, dead or alive</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>Eye of the Tiger</TITLE>
            <AUTHOR>Survivor</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>Rising up, back on the street
did my time, took my chances
went the distance, now I'm back on my feet
just a man and his will to survive
so many times, it happens too fast
you change your passion for glory
don't lose your grip on the dreams of the past
you must fight just to keep them alive
it's the eye of the tiger, it's the thrill of the fight
rising up to the challenge of our rival
and the last known survivor stalks his prey in the night
and he's watching us all in the eye of the tiger
face to face, out in the heat
hanging tough, staying hungry
they stack the odds 'til we take to the street
for we kill with the skill to survive
it's the eye of the tiger, it's the thrill of the fight
rising up to the challenge of our rival
and the last known survivor stalks his prey in the night
and he's watching us all in the eye of the tiger
rising up, straight to the top
have the guts, got the glory 
went the distance, now I'm not gonna stop
just a man and his will to survive
it's the eye of the tiger, it's the thrill of the fight
rising up to the challenge of our rival
and the last known survivor stalks his prey in the night
and he's watching us all in the eye of the tiger</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>American Pie</TITLE>
            <AUTHOR>Don McLean</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>A long long time ago
I can still remember how
that music used to make me smile
and I knew if I had my chance
that I could make those people dance
and maybe they'd be happy for a while
but February made me shiver
with every paper I'd deliver
bad news on the doorstep
I couldn't take one more step
I can't remember if I cried
when I read about his widowed bride
but something touched me deep inside
the day the music died
so bye, bye miss american pie
drove my Chevy to the levee but the levee was dry
them good ole boys were drinking whiskey in rye
singing this'll be the day that I die
this'll be the day that I die
did you write the book of love
and do you have faith in God above
if the nible tells you so?
Now do you believe in rock and roll?
Can music save your mortal soul?
And can you teach me how to dance real slow?
Well, I know that you're in love with him
cause I saw you dancing in the gym
you both kicked off your shoes
man, I dig those rhythm and blues
I was a lonely teenage broncin' buck
with a pink carnation and a pickup truck
but I knew I was out of luck
the day the music died
I started singing
bye, bye miss american pie
drove my Chevy to the levee but the levee was dry
them good ole boys were drinking whiskey in rye
singing this'll be the day that I die
this'll be the day that I die
now, for ten years we've been on our own
and moss grows fat on a rolling stone
but, that's not how it used to be
when the jester sang for the king and queen
in a coat he borrowed from James Dean
and a voice that came from you and me
oh and while the king was looking down
the jester stole his thorny crown
the courtroom was adjourned
no verdict was returned
and while Lenin read a book on Marx
the quartet practiced in the park
and we sang dirges in the dark
the day the music died
we were singing
bye, bye miss american pie
drove my Chevy to the levee but the levee was dry
them good ole boys were drinking whiskey in rye
singing this'll be the day that I die
this'll be the day that I die
helter skelter in a summer swelter
the birds flew off with a fallout shelter
eight miles high and falling fast
it landed foul on the grass
the players tried for a forward pass
with the jester on the sidelines in a cast
now the half time air was sweet perfume
while sergeants played a marching tune
we all got up to dance
oh, but we never got the chance
cause the players tried to take the field
the marching band refused to yield
do you recall what was revealed
the day the music died?
We started singing
bye, bye miss american pie
drove my Chevy to the levee but the levee was dry
them good ole boys were drinking whiskey in rye
singing this'll be the day that I die
this'll be the day that I die
oh, and there we were all in one place
a generation lost in space
with no time left to start again
so come on Jack be nimble, Jack be quick
Jack Flash sat on a candlestick
cause fire is the devil's only friend
and as I watched him on the stage
my hands were clenched in fists of rage
no angel born in Hell
could break that Satan's spell
and as the flames climbed high into the night
to light the sacrificial rite
I saw Satan laughing with delight
the day the music died
he was singing
bye, bye miss american pie
drove my Chevy to the levee but the levee was dry
them good ole boys were drinking whiskey in rye
singing this'll be the day that I die
this'll be the day that I die
I met a girl who sang the blues
and I asked her for some happy news
but she just smiled and turned away
I went down to the sacred store
where I'd heard the music years before
but the man there said the music wouldn't play
and in the streets the children screamed
the lovers cried, and the poets dreamed
but not a word was spoken
the church bells all were broken
and the three men I admire most
the Father, Son, and the Holy Ghost
they caught the last train for the coast
the day the music died
and they were singing
bye, bye miss american pie
drove my Chevy to the levee but the levee was dry
them good ole boys were drinking whiskey in rye
singing this'll be the day that I die</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>The Saga Begins (Parody of American Pie)</TITLE>
            <AUTHOR>&quot;Weird Al&quot; Yankovic</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>A long, long time ago
in a galaxy far away
Naboo was under an attack
and I thought me and Qui-Gon Jinn
could talk the federation into
maybe cutting them a little slack
but their response, it didn't thrill us
they locked the doors and tried to kill us
we escaped from that gas
then met Jar Jar and Boss Nass
we took a bongo from the scene
and we went to Theed to see the queen
we all wound up on Tatooine
that's where we found this boy
oh my my this here Anakin guy
may be Vader someday later now he's just a small fry
and he left his home and kissed his mommy goodbye
saying soon I'm gonna be a jedi
soon I'm gonna be a jedi
did you know this junkyard slave
isn't even old enough to shave
but he can use the Force, they say
ah, do you see him hitting on the queen
though he's just nine and she's fourteen
yeah, he's probably gonna marry her someday
well, I know he built C3PO
and I've heard how fast his pod can go
and we were broke, it's true
so we made a wager or two
he was a prepubescent flying ace
and the minute Jabba started off that race
well, I knew who would win first place
oh yes, it was our boy
we started singing
my, my this here Anakin guy
may be Vader someday later now he's just a small fry
and he left his home and kissed his mommy goodbye
saying soon I'm gonna be a jedi
soon I'm gonna be a jedi
now we finally got to Coruscant
the Jedi Council we knew would want
to see how good the boy could be
so we took him there and we told the tale
how his midi chlorians were off the scale
and he might fulfill that prophecy
oh, the Council was impressed, of course
could he bring balance to the Force?
They interviewed the kid
oh, training they forbid
because Yoda sensed in him much fear
and Qui-Gon said now listen here
just stick it in your pointy ear
I still will teach this boy
he was singing
my, my this here Anakin guy
may be Vader someday later now he's just a small fry
and he left his home and kissed his mommy goodbye
saying soon I'm gonna be a jedi
soon I'm gonna be a jedi
we caught a ride back to Naboo
'cause Queen Amidala wanted to
I frankly would've liked to stay
we all fought in that epic war
and it wasn't long at all before
little hotshot flew his plane and saved the day
and in the end some Gunguns died
some ships blew up and some pilots fried
a lot of folks were croakin'
the battle droids were broken
and the Jedi I admire most
met up with Darth Maul and now he's toast
well, I'm still here and he's a ghost
I guess I'll train this boy
and I was singing
my, my this here Anakin guy
may be Vader someday later now he's just a small fry
and he left his home and kissed his mommy goodbye
saying soon I'm gonna be a jedi
soon I'm gonna be a jedi</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>Before He Cheats</TITLE>
            <AUTHOR>Carrie Underwood</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>Right now he's probably slow dancing with a bleached blond tramp,
and she's probably getting frisky
right now, he's probably buying her some fruity little drink
'cause she can't shoot whiskey
right now, he's probably up behind her with a poolstick,
showing her how to shoot a combo
and he don't know
that I dug my key into the side of his
pretty little souped up four wheel drive,
carved my name into his leather seats
I took a Louisville slugger to both headlights,
slashed a hole in all four tires
maybe next time he'll think before he cheats.
Right now, she's probably up singing some
white-trash version of Shania karaoke
right now, she's probably saying "I'm drunk,"
and he's a thinking that he's gonna get lucky
right now, he's probably dabbing on
three dollars worth of that bathroom polo
and he don't know
that I dug my key into the side of his
pretty little souped up four wheel drive,
carved my name into his leather seats,
I took a Louisville slugger to both headlights,
slashed a hole in all four tires
maybe next time he'll think before he cheats.
I might've saved a little trouble for the next girl,
'cause the next time that he cheats
oh, you know it won't be on me!
No, not on me
'cause I dug my key into the side of his
pretty little souped up four wheel drive,
carved my name into his leather seats
I took a Louisville slugger to both headlights,
slashed a hole in all four tires
maybe next time he'll think before he cheats.
Oh, maybe next time he'll think before he cheats</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>Summer Nights</TITLE>
            <AUTHOR>Grease</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>Summer lovin', had me a blast
summer lovin', happened so fast
met a girl crazy for me
met a boy cute as can be
summer days drifting away
to, uh oh, those summer nights
well a well a well a huh!
Tell me more, tell me more
did you get very far?
Tell me more, tell me more
like does he have a car?
She swam by me, she got a cramp
he ran by me, got my suit damp
saved her life, she nearly drowned
he showed off splashing around
summer sun, something's begun
but, uh oh, those summer nights
well a well a well a huh!
Tell me more, tell me more
was it love at first sight?
Tell me more, tell me more
did she put up a fight?
Took her bowling in the arcade
we went strolling; drank lemonade
we made out under the dock
we stayed out till ten o'clock
summer fling don't mean a thing
but, uh oh, those summer nights
tell me more, tell me more
but you don't gotta brag
tell me more, tell me more
'cause he sounds like a drag
he got friendly holding my hand
well, she got friendly down in the sand
he was sweet, just turned eighteen
well, she was good, you know what I mean
summer heat, boy and girl meet
but, uh oh, those summer nights
tell me more, tell me more
how much dough did he spend?
Tell me more, tell me more
could she get me a friend?
it turned colder; that's where it ends
so I told her we'd still be friends
then we made our true love vow
wonder what she's doin'gnow
summer dreams ripped at the seams
but, oh, those summer nights</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>Sweet Caroline</TITLE>
            <AUTHOR>Neil Diamond</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>Where it began,
I can't begin to knowing
but then I know it's growing strong
was in the spring
and spring became the summer
who'd have believed you'd come along
hands, touching hands
reaching out, touching me touching you
sweet Caroline
good times never seemed so good
I've been inclined
to believe they never would
but now I, look at the night
and it don't seem so lonely
we fill it up with only two
and when I hurt,
hurting runs off my shoulders
how can I hurt when I'm with you
warm, touching warm
reaching out, touching me touching you
sweet Caroline
good times never seemed so good
I've been inclined,
to believe they never would
oh no, no
sweet Caroline
good times never seemed so good
sweet Caroline,
I believe they never could
sweet Caroline</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>Creep</TITLE>
            <AUTHOR>Radiohead</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>When you were here before
couldn't look you in the eye
you're just like an angel
your skin makes me cry
you float like a feather
in a beautiful world
I wish I was special
you're so fruiting special
but I'm a creep
I'm a weirdo
what the heck am I doing here?
I don't belong here
I don't care if it hurts
I want to have control
I want a perfect body
I want a perfect soul
I want you to notice when I'm not around
you're so fruiting special
I wish I was special
but I'm a creep
I'm a weirdo
what the heck I'm doing here?
I don't belong here
she's running out the door
she's running out
she runs runs runs
whatever makes you happy
whatever you want
you're so fruiting special
I wish I was special
but I'm a creep
I'm a weirdo
what the heck am I doing here?
I don't belong here
I don't belong here</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>I Will Survive</TITLE>
            <AUTHOR>Gloria Gaynor</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>First I was afraid
I was petrified
kept thinking I could never live
without you by my side
but I spent so many nights
thinking how you did me wrong
I grew strong
I learned how to carry on
and so you're back
from outer space
I just walked in to find you here
with that sad look upon your face
I should have changed my stupid lock
I should have made you leave your key
if I had known for just one second
you'd be back to bother me
go on now go walk out the door
just turn around now
'cause you're not welcome anymore
weren't you the one who tried to hurt me with goodbye
you think I'd crumble
you think I'd lay down and die
oh no, not I
I will survive
as long as i know how to love
I know I will stay alive
I've got all my life to live
I've got all my love to give
and I'll survive
I will survive
It took all the strength I had
not to fall apart
kept trying hard to mend
the pieces of my broken heart
and I spent oh so many nights
just feeling sorry for myself
I used to cry
now I hold my head up high
and you see me
somebody new
I'm not that chained up little person
still in love with you
and so you felt like dropping in
and just expect me to be free
now I'm saving all my loving
for someone who's loving me</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>Don't Stop Believin'</TITLE>
            <AUTHOR>Journey</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>Just a small town girl
livin' in a lonely world
she took the midnight train
going anywhere
just a city boy
born and raised in South Detroit
he took the midnight train
going anywhere
a singer in a smokey room
smell of wine and cheap perfume
for a smile they can share the night
it goes on and on and on and on
strangers waiting
up and down the boulevard
their shadows searching
in the night
streetlights, people
living just to find emotion
hiding, somewhere in the night
working hard to get my fill
everybody wants a thrill
paying anything to roll the dice
just one more time
some will win
some will lose
some were born to sing the blues
oh, the movie never ends
it goes on and on and on and on
strangers waiting
up and down the boulevard
their shadows searching
in the night
streetlights, people
living just to find emotion
hiding, somewhere in the night
don't stop believin'
hold on to the feelin'
streetlights, people woah oh
don't stop believin'
hold on
streetlights, people</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>My Girl</TITLE>
            <AUTHOR>The Temptations</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>I've got sunshine on a cloudy day
when it's cold outside I've got the month of May
I guess you'd say
what can make me feel this way?
My girl, my girl, my girl
talking 'bout my girl, my girl
I've got so much honey the bees envy me
I've got a sweeter song than the birds in the trees
I guess you'd say
what can make me feel this way?
My girl, my girl, my girl
talking 'bout my girl, my girl
I don't need no money, fortune, or fame
I've got all the riches baby one man can claim
I guess you'd say
what can make me feel this way?
My girl, my girl, my girl
talking 'bout my girl, my girl
I've got sunshine on a cloudy day
with my girl.
I've even got the month of May
with my girl</TEXT>
        </LYRICS>
    </ENTRY>
	<ENTRY>
        <LYRICS>
            <TITLE>Total Eclipse of the Heart</TITLE>
            <AUTHOR>Bonnie Tyler</AUTHOR>
            <TYPE>Song Lyrics</TYPE>
            <TEXT>Turnaround, every now and then I get a little bit lonely and you're never coming round
turnaround, every now and then I get a little bit tired of listening to the sound of my tears
turnaround, every now and then I get a little bit nervous that the best of all the years have gone by
turnaround, every now and then I get a little bit terrified and then I see the look in your eyes
turnaround bright eyes, every now and then I fall apart
turnaround bright eyes, every now and then I fall apart
and I need you now tonight
and I need you more than ever
and if you'll only hold me tight
we'll be holding on forever
and we'll only be making it right
cause we'll never be wrong together
we can take it to the end of the line
your love is like a shadow on me all of the time
I don't know what to do and I'm always in the dark
we're living in a powder keg and giving off sparks
I really need you tonight
forever's gonna start tonight
forever's gonna start tonight
once upon a time I was falling in love
but now I'm only falling apart
there's nothing I can do
a total eclipse of the heart
once upon a time there was light in my life
but now there's only love in the dark
nothing I can do
a total eclipse of the heart
turnaround, every now and then I get a little bit restless and I dream of something wild
turnaround, every now and then I get a little bit helpless and I'm lying like a child in your arms
turnaround, every now and then I get a little bit angry and I know I've got to get out and cry
turnaround, every now and then I get a little bit terrified but then I see the look in you eyes
turnaround bright eyes, every now and then I fall apart
turnaround bright eyes, every now and then I fall apart
and I need you now tonight
and I need you more than ever
and if you'll only hold me tight
we'll be holding on forever
and we'll only be making it right
cause we'll never be wrong together
we can take it to the end of the line
your love is like a shadow on me all of the time
I don't know what to do and I'm always in the dark
we're living in a powder keg and giving off sparks
I really need you tonight
forever's gonna start tonight
forever's gonna start tonight
once upon a time I was falling in love
but now I'm only falling apart
there's nothing I can do
a total eclipse of the heart</TEXT>
        </LYRICS>
    </ENTRY>
    <ENTRY>
        <COMMON_WORDS>
            <TITLE>1000+ Most Common English Words</TITLE>
            <AUTHOR>&lt;a href=&quot;http://www.paulnoll.com/Books/Clear-English&quot;&gt;www.paulnoll.com/Books/Clear-English&lt;a&gt;</AUTHOR>
            <TYPE>Common Words</TYPE>
            <TEXT>about. after. again. air. all. along. also. an. and. another. any. are. around. as. at. away. back. be. because. been. before. below. between. both. but. by. came. can. come. could. day. did. different. do. does. don't. down. each. end. even. every. few. find. first. for. found. from. get. give. go. good. great. had. has. have. he. help. her. here. him. his. home. house. how. I. if. in. into. is. it. its. just. know. large. last. left. like. line. little. long. look. made. make. man. many. may. me. men. might. more. most. must. my. name. never. new. next. no. not. now. number. of. off. old. on. one. only. or. other. our. out. over. own. part. people. place. put. read. right. said. same. saw. say. see. she. should. show. small. so. some. something. sound. still. such. take. tell. than. that. the. them. then. there. these. they. thing. think. this. those. thought. three. through. time. to. together. too. two. under. up. us. use. very. want. water. way. we. well. went. were. what. when. where. which. while. who. why. will. with. word. work. world. would. write. year. you. your. was. able. above. across. add. against. ago. almost. among. animal. answer. became. become. began. behind. being. better. black. best. body. book. boy. brought. call. cannot. car. certain. change. children. city. close. cold. country. course. cut. didn't. dog. done. door. draw. during. early. earth. eat. enough. ever. example. eye. face. family. far. father. feel. feet. fire. fish. five. food. form. four. front. gave. given. got. green. ground. group. grow. half. hand. hard. heard. high. himself. however. I'll. I'm. idea. important. inside. John. keep. kind. knew. known. land. later. learn. let. letter. life. light. live. living. making. mean. means. money. morning. mother. move. near. night. nothing. once. open. order. page. paper. parts. perhaps. picture. play. point. ready. red. remember. rest. room. run. school. sea. second. seen. sentence. several. short. shown. since. six. slide. sometime. soon. space. States. story. sun. sure. table. though. today. told. took. top. toward. tree. try. turn. United. until. upon. using. usually. white. whole. wind. without. yes. yet. young. alone. already. although. am. America. anything. area. ball. beautiful. beginning. Bill. birds. blue. boat. bottom. box. bring. build. building. built. can't. care. carefully. carried. carry. center. check. class. coming. common. complete. dark. deep. distance. doing. dry. easy. either. else. everyone. everything. fact. fall. fast. felt. field. finally. fine. floor. follow. foot. friend. full. game. getting. girl. glass. goes. gold. gone. happened. having. heart. heavy. held. hold. horse. hot. hour. hundred. ice. Indian. instead. itself. job. kept. language. lay. least. leave. let's. list. longer. low. main. map. matter. mind. Miss. moon. mountain. moving. music. needed. notice. outside. past. pattern. person. piece. plant. poor. possible. power. probably. problem. question. quickly. quite. rain. ran. real. river. road. rock. round. sat. scientist. shall. ship. simple. size. sky. slowly. snow. someone. special. stand. start. state. stay. stood. stop. stopped. strong. suddenly. summer. surface. system. taken. talk. tall. ten. that's. themselves. third. tiny. town. tried. voice. walk. warm. watch. weather. whether. wide. wild. winter. within. writing. written. age. ask. baby. base. beside. bright. business. buy. case. catch. caught. child. choose. circle. clear. color. copy. correct. couldn't. difference. direction. dried. easily. edge. egg. eight. energy. England. especially. Europe. exactly. except. explain. famous. farm. fell. figure. flat. fly. forest. free. French. fun. George. government. grass. grew. hair. happy. he's. heat. history. human. I've. inch. information. iron. Jim. Joe. King. larger. late. leg. length. listen. lost. lot. lower. machine. mark. maybe. measure. meet. middle. milk. minute. modern. moment. month. mouth. natural. nearly. necessary. New York. north. object. ocean. oil. pay. per. plan. plane. present. product. rather. reach. reason. record. running. seems. sent. seven. shape. sides. single. skin. sleep. smaller. soft. soil. south. speak. speed. spring. square. star. step. store. straight. strange. street. subject. suppose. teacher. thousand. thus. Tom. travel. trip. trouble. unit. village. wall. war. wasn't. week. whose. window. wish. women. won't. wood. wrote. yellow. you're. yourself. action. addition. afraid. afternoon. ahead. amount. ancient. anyone. arm. bad. bear. beyond. bit. blood. board. Bob. born. break. British. broken. brother. brown. busy. capital. cat. cattle. cause. century. chance. clean. clothes. coast. control. cool. corn. corner. cover. cross. Dan. dead. deal. death. decide. difficult. doesn't. drive. engine. evening. farmer. faster. fight. fill. finger. force. forward. France. fresh. garden. general. glad. greater. greatest. guess. happen. Henry. higher. hit. hole. hope. huge. interest. island. isn't. jack. lady. largest. lead. led. level. love. Mary. material. meant. meat. method. missing. needs. nor. nose. note. opposite. pair. party. pass. period. please. position. pound. practice. pretty. produce. pull. quiet. race. radio. region. result. return. rich. ride. ring. rule. sand. science. section. seed. send. sense. sets. sharp. sight. sign. silver. similar. sit. son. song. spent. spread. stick. stone. tail. team. teeth. temperature. test. there's. therefore. thick. thin. train. various. wait. Washington. wave. we'll. weight. west. wife. wouldn't. wrong. you'll. according. act. actually. Africa. alike. apart. ate. attention. bank. basic. beat. blow. bone. bread. careful. chair. chief. Christmas. church. cloth. cloud. column. compare. contain. continued. cost. cotton. count. dance. describe. desert. dinner. doctor. dollar. drop. dropped. ear. east. electric. element. enjoy. equal. exercise. experiment. familiar. farther. fear. forth. gas. giving. gray. grown. hardly. hat. hill. hurt. I'd. imagine. include. indeed. Johnny. joined. key. kitchen. knowledge. law. lie. major. met. metal. movement. nation. nature. nine. none. office. older. onto. original. paragraph. parent. particular. path. Paul. Peter. pick. president. pressure. process. public. quick. report. rope. rose. row. safe. salt. Sam. scale. sell. separate. sheep. shoe. shore. simply. sing. sister. sitting. sold. soldier. solve. speech. spend. steel. string. student. studied. sugar. television. term. throughout. tired. total. touch. trade. truck. twice. type. uncle. unless. useful. value. verb. visit. wear. what's. wheel. William. wing. wire. won. wonder. worker. yard. alive. angry. army. average. bag. band. Billy. branch. breakfast. breath. broke. bus. cabin. California. camp. captain. cell. cent. certainly. changing. closer. coal. coat. community. company. completely. compound. condition. consider. correctly. crop. crowd. current. danger. dear. degree. develop. die. directly. discover. divide. double. dress. drink. drove. dust. easier. effect. electricity. empty. entire. everybody. exciting. expect. experience. express. fair. feed. final. finish. flew. fruit. further. future. Greek. guide. gun. herself. hungry. instrument. Jane. join. jump. laid. liquid. loud. market. member. Mexico. Mike. mine. motion. myself. neck. news. nice. noise. noun. oxygen. paid. phrase. plain. poem. population. proper. proud. provide. purpose. putting. quietly. raise. range. rate. regular. related. replied. represent. rise. scientific. season. seat. share. shot. shoulder. slow. smile. solid. solution. sort. southern. stage. statement. station. steam. stream. strength. supply. surprise. symbol. till. tomorrow. tube. twelve. twenty. usual. valley. variety. vowel. we're. wet. wooden. worth</TEXT>
        </COMMON_WORDS>
    </ENTRY>
    <ENTRY>
        <EASY_WORDS>
            <TITLE>Easy Typing - Common English Words</TITLE>
            <AUTHOR>&lt;a href=&quot;http://www.world-english.org/english500.htm&quot;&gt;www.world-english.org&lt;a&gt;</AUTHOR>
            <TYPE>Common Words</TYPE>
            <TEXT>the. of. to. and. a. in. is. it. you. that. he. was. for. on. are. with. as. I. his. they. be. at. one. have. this. from. or. had. by. hot. but. some. what. there. we. can. out. other. were. all. your. when. up. use. word. how. said. an. each. she. which. do. their. time. if. will. way. about. many. then. them. would. write. like. so. these. her. long. make. thing. see. him. two. has. look. more. day. could. go. come. did. my. sound. no. most. number. who. over. know. water. than. call. first. people. may. down. side. been. now. find. any. new. work. part. take. get. place. made. live. where. after. back. little. only. round. man. year. came. show. every. good. me. give. our. under. name. very. through. just. form. much. great. think. say. help. low. line. before. turn. cause. same. mean. differ. move. right. boy. old. too. does. tell. set. three. want. air. well. also. play. small. end. put. home. read. hand. port. large. spell. add. land. here. must. big. high. such. act. why. ask. men. change. went. light. kind. off. need. house. picture. try. us. again. animal. point. mother. world. near. build. self. earth. father. head. stand. own. page. should. found. answer. grow. study. learn. plant. cover. food. sun. four. thought. let. keep. eye. never. last. door. between. city. since. hard. start. might. story. saw. far. sea. draw. left. late. run. don't. while. press. close. night. real. life. few. stop. open. seem. next. white. begin. got. walk. ease. paper. often. those. both. mark. book. until. mile. river. car. feet. care. second. group. rain. eat. friend. began. idea. fish. north. once. base. hear. horse. cut. sure. watch. color. face. main. enough. plain. girl. usual. young. ready. above. ever. red. list. through. feel. talk. bird. soon. body. dog. family. direct. pose. leave. song. measure. state. product. wind. ship. area. half. rock. order. fire. south. problem. piece. told. knew. pass. farm. top. king. size. heard. best. hour. true. during. am. step. early. hold. west. ground. reach. fast. sing. listen. table. travel. sit. snow. heat. hot. yes. shape. game. check. ran. ago. laugh. wonder. dry. age. plane. gold. boat. test. busy. yet. blue. force. full. stay. lot. inch. tail. clear. behind. mind. strong. minute. free. warm. final. front. drive. done. able. box. plan. wait. note. dark. cry. lead. unit. fly. fine. town. power. ten. war. lay. slow. love. person. money. road. map. rule. cold</TEXT>
        </EASY_WORDS>
    </ENTRY>
	<ENTRY>
        <SIGHT_WORDS>
            <TITLE>Easy Typing - Sight Words</TITLE>
            <AUTHOR>&lt;a href=&quot;http://bogglesworldesl.com/dolch/lists.htm&quot;&gt;bogglesworldesl.com&lt;a&gt;</AUTHOR>
            <TYPE>Sight Words</TYPE>
            <TEXT>a. and. away. big. blue. can. come. down. find. for. funny. go. help. here. I. in. is. it. jump. little. look. make. me. my. not. one. play. red. run. said. see. the. three. to. two. up. we. where. yellow. you. all. am. are. at. ate. be. black. brown. but. came. did. do. eat. four. get. good. have. he. into. like. must. new. no. now. on. our. out. please. pretty. ran. ride. saw. say. she. so. soon. that. there. they. this. too. under. want. was. well. went. what. white. who. will. with. yes. a. and. away. big. blue. can. come. down. find. for. funny. go. help. here. I. in. is. it. jump. little. look. make. me. my. not. one. play. red. run. said. see. the. three. to. two. up. we. where. yellow. you. all. am. are. at. ate. be. black. brown. but. came. did. do. eat. four. get. good. have. he. into. like. must. new. no. now. on. our. out. please. pretty. ran. ride. saw. say. she. so. soon. that. there. they. this. too. under. want. was. well. went. what. white. who. will. with. yes. after. again. an. any. ask. as. by. could. every. fly. from. give. going. had. has. her. him. his. how. just. know. let. live. may. of. old. once. open. over. put. round. some. stop. take. thank. them. then. think. walk. were. when</TEXT>
        </SIGHT_WORDS>
    </ENTRY>
    <ENTRY>
        <FACTS>
            <TITLE>Interesting and Fun Facts</TITLE>
            <AUTHOR>Various</AUTHOR>
            <TYPE>Facts</TYPE>
            <TEXT>Right handed people live, on average, nine years longer than left handed people. Tablecloths were originally meant to be served as towels with which dinner guests could wipe their hands and faces after eating. Your home is ten times more likely to have a fire than be burglarized. Your statistical chance of being murdered is one in twenty thousand. Cherophobia is a fear of fun. You are about one centimeter taller in the morning than in the evening. The chances of you dying on the way to get your lottery tickets is greater than your chances of winning. Jumbo jets use four thousand gallons of fuel to take off. The first alarm clock could only ring at four in the morning. The first hard drive available for the Apple II had a capacity of only five megabytes. To have your picture taken by the very first camera you would have had to sit still for eight hours. The cigarette lighter was invented before the match. MySpace reports over one hundred and ten million registered users - were it a country, it would be the tenth largest, just behind Mexico. Domain names are being registered at a rate of more than one million names every month. The Dvorak keyboard is more efficient than Qwerty - twenty times faster, in fact. On an average work day, a typist's fingers travel 12.6 miles. "Stewardesses" is the longest word that is typed with only the left hand. "Typewriter" is the longest word that can be made using the letters only on one row of the keyboard. Bill Gates' house was designed using a Macintosh computer. The praying mantis is the only insect that can turn its head. Chewing gum while peeling onions will keep you from crying. A giraffe can clean its ears with its twenty one inch tongue. Babe Ruth wore a cabbage leaf under his cap to keep him cool - he changed it every two innings. The most common name in the world is Mohammed. The elephant is the only mammal that can't jump. A jellyfish is ninety five percent water. Honeybees have hair on their eyes. It's against the law to slam your car door in Switzerland. Owls are the only birds who can see the color blue. A sneeze travels out your mouth at over one hundred miles per hour. It's against the law to have a pet dog in Iceland. Dolphins sleep with one eye open. Ants stretch when they wake up in the morning. They have square watermelons in Japan - they stack better. Proportional to their weight, men are stronger than horses. Legislation passed during world war one making it illegal to say "gesundheit" to a sneezer was never repealed. In the weightlessness of space a frozen pea will explode if it comes in contact with Pepsi. More than ten people a year are killed by vending machines. Hippo milk is pink. Women have a better sense of smell than men. Ice Cream is chinese food. The human brain has the capacity to store everything that you experience. Donkeys kill more people annually than plane crashes. The Mona Lisa has no eyebrows - it was the fashion in Renaissance Florence to shave them off. The average person spends three years of his or her life on a toilet. Scientists aren't sure what color dinosaurs were. A goldfish has a memory span of three seconds. Giraffes and rats can last longer without water than camels. Your stomach produces a new layer of mucus every two weeks so that it doesn't digest itself. A baby octopus is about the size of a flea at birth. A blue whale's heart is the size of a Volkswagen Beetle. Ninety percent of women who walk into a department store immediately turn to the right. A cockroach can live several weeks with its head cut off when it finally dies from starvation. A group of frogs is called an army. A man once sued his doctor because he survived his cancer longer than the doctor predicted. A mole can dig a tunnel three hundred feet long in just one night. A peanut is not a nut, it is a legume. A pound of potato chips costs two hundred times more than a pound of potatoes. A rat can fall from a five story building without injury. A real estate agents rule of thumb: To estimate what a house will sell for, ask the owner what it's worth and subtract ten percent. A shrimp's heart is in its head. According to United States law, a beer commercial can never show a person actually drinking beer. Actor Mark Wahlberg has a third nipple. An albatross can sleep while it flies. Farmers in England are required by law to provide their pigs with toys. Tablecloths were originally meant to be served as towels with which dinner guests could wipe their hands and faces after eating. Your home is ten times more likely to have a fire than be burglarized. Your statistical chance of being murdered is one in twenty thousand. Cherophobia is a fear of fun. You are about one centimeter taller in the morning than in the evening. The chances of you dying on the way to get your lottery tickets is greater than your chances of winning. Jumbo jets use four thousand gallons of fuel to take off. The first alarm clock could only ring at four in the morning. The first hard drive available for the Apple II had a capacity of only five megabytes. To have your picture taken by the very first camera you would have had to sit still for eight hours. The cigarette lighter was invented before the match. MySpace reports over one hundred and ten million registered users - were it a country, it would be the tenth largest, just behind Mexico. Domain names are being registered at a rate of more than one million names every month. The Dvorak keyboard is more efficient than Qwerty - twenty times faster, in fact. On an average work day, a typist's fingers travel 12.6 miles. "Stewardesses" is the longest word that is typed with only the left hand. "Typewriter" is the longest word that can be made using the letters only on one row of the keyboard. Bill Gates' house was designed using a Macintosh computer. The praying mantis is the only insect that can turn its head. Chewing gum while peeling onions will keep you from crying. A giraffe can clean its ears with its twenty one inch tongue. The most common name in the world is Mohammed. The elephant is the only mammal that can't jump. A jellyfish is ninety five percent water. Honeybees have hair on their eyes. It's against the law to slam your car door in Switzerland. Owls are the only birds that can see the color blue. A sneeze travels out your mouth at over one hundred miles per hour. It's against the law to have a pet dog in Iceland. Dolphins sleep with one eye open. Ants stretch when they wake up in the morning. Proportional to their weight, men are stronger than horses. Legislation passed during world war one making it illegal to say "gesundheit" to a sneezer was never repealed. In the weightlessness of space a frozen pea will explode if it comes in contact with Pepsi. More than ten people a year are killed by vending machines. Hippo milk is pink. Women have a better sense of smell than men. Ice Cream is Chinese food. The human brain has the capacity to store everything that you experience. Donkeys kill more people annually than plane crashes. The average person spends three years of his or her life on a toilet. Scientists aren't sure what color dinosaurs were. A goldfish has a memory span of three seconds. Giraffes and rats can last longer without water than camels. Your stomach produces a new layer of mucus every two weeks so that it doesn't digest itself. A baby octopus is about the size of a flea at birth. A blue whale's heart is the size of a Volkswagen Beetle. Ninety percent of women who walk into a department store immediately turn to the right. A group of frogs is called an army. A man once sued his doctor because he survived his cancer longer than the doctor predicted. A mole can dig a tunnel three hundred feet long in just one night. A peanut is not a nut, it is a legume. A pound of potato chips costs two hundred times more than a pound of potatoes. A rat can fall from a five story building without injury. A real estate agents rule of thumb: To estimate what a house will sell for, ask the owner what it's worth and subtract ten percent. A shrimp's heart is in its head. According to United States law, a beer commercial can never show a person actually drinking beer. Actor Mark Wahlberg has a third nipple. An albatross can sleep while it flies. Farmers in England are required by law to provide their pigs with toys</TEXT> <!-- LEAVE PERIOD OFF OF LAST FACT!!! also no space between last letter and "</TEXT>" -->
        </FACTS>
    </ENTRY>
    <ENTRY>
        <PROVERBS>
            <TITLE>Proverbs - Poor Richards Almanac</TITLE>
            <AUTHOR>Ben Franklin</AUTHOR>
            <TYPE>Proverbs</TYPE>
            <TEXT>For want of a nail a shoe was lost, for want of a shoe a horse was lost, for want of a horse a rider was lost, for want of a rider an army was lost, for want of an army a battle was lost, for want of a battle the war was lost, for want of the war the kingdom was lost, and all for the want of a little horseshoe nail. Early to bed, early to rise, makes a man healthy, wealthy and wise. Wars are not paid for in wartime, the bill comes later. The only thing more expensive than education is ignorance. A ploughman on his legs is higher than a gentleman on his knees. If you would be loved, love and be lovable. If a man empties his purse into his head, no man can take it away from him - an investment in knowledge always pays the best interest. By failing to prepare, you are preparing to fail. Do not anticipate trouble or worry about what may never happen. Look before, or you'll find yourself behind. Approve not of him who commends all you say. Clean your fingers before you point at my spots. Many have been ruined by buying good pennyworths. Would you live with ease, do what you ought and not what you please. Better slip with foot than tongue. You cannot pluck roses without fear of thorns, nor enjoy a fair wife without danger of horns. Without justice, courage is weak. Fools multiply folly. Of learned fools I have seen ten times ten; of unlearned wise men I have seen a hundred. Poverty wants some things, luxury many things, avarice all things. There is small revenge in words, but words may be greatly revenged. A man is never so ridiculous by those qualities that are his own as by those that he affects to have. It is better to take many injuries than to give one. An old young man will be a young old man. He is no clown that drives the plow, but he that does clownish things. Fish and visitors stink in three days. Diligence is the mother of good luck. Wealth is not his that has it, but his that enjoys it. He that can have patience can have what he will. God helps them that help themselves. Creditors have better memories than debtors. He that scatters thorns, let him not go barefoot. If you desire many things, many things will seem but a few. There are no gains without pains. At the working man's house hunger looks in but dares not enter. Industry pays debts while despair increases them. God gives all things to industry. Trouble springs from idleness and grievous toil from needless ease. Having been poor is no shame, but being ashamed of it is. 'Tis hard but glorious to be poor and honest. Anger is never without a reason but seldom with a good one. Love your neighbor yet don't pull down your hedge. He that does what he should not shall feel what he would not. The honest man takes pains and then enjoys pleasures; the knave takes pleasures and then suffers pains. What you would seem to be, be really. Necessity never made a good bargain. He that lies down with dogs shall rise up with fleas. Being ignorant is not so much a shame as being unwilling to learn. Be civil to all, serviceable to many, familiar with few, friend to one, enemy to none. Love your enemies, for they tell you your faults. Be always ashamed to catch thyself idle. Fear to do evil and you need fear nothing else. Wish not so much to live long as to live well. Beware of little expenses - a small leak will sink a great ship. Keep your eyes wide open before marriage, half shut afterwards. You can bear your own faults and why not a fault in your wife. The way to be safe is never to be secure. He that best understands the world, least likes it. Blessed is he who expects nothing for he shall never be disappointed. Success has ruined many a man. Ill customs and bad advice are seldom forgotten. If your head is wax, don't walk in the sun. There are no fools so troublesome as those who have wit. Quarrels never could last long if on one side only lay the wrong. Visit your aunt, but not every day, and call at your brothers, but not every night</TEXT> <!-- LEAVE PERIOD OFF OF LAST FACT!!! also no space between last letter and "</TEXT>" -->
        </PROVERBS>
    </ENTRY>
    <ENTRY>
        <PROVERBS>
            <TITLE>Proverbs - Various</TITLE>
            <AUTHOR>Various</AUTHOR>
            <TYPE>Proverbs</TYPE>
            <TEXT>Live every day like it's your last. Be courageous, it is the only place left uncrowded. Don't chase your dreams, catch them. Today is the first day of the rest of your life. It's not about how hard you hit, its how hard you get hit and keep moving forward. It's not the size of the dog in the fight but the size of the fight in the dog. The only difference between try and triumph is a little umph. Today I shall behave as though this day is the one by which I shall be remembered. Try and fail, but don't fail to try. Many say I am just one to try, but I say I am one less to quit. Seventy percent of success in life is showing up. A society grows great when old men plant trees whose shade they know they shall never sit in. No matter what you do there will be critics. People can live one hundred years without really living a minute. Live your life in the manner that you would like your kids to live theirs. Think of the beauty still left around you and be happy. If you spend your whole life waiting for the storm, you'll never enjoy the sunshine. You can never cross the ocean unless you have the courage to lose sight of the shore. Happiness is the best face lift. Nobody ever drowned in his own sweat. Fall seven times, stand up eight. The difference between ordinary and extraordinary is that little extra. It is better to light a candle than curse the darkness. If you only do what you know you can do you never do very much. Whatever you are be a good one. Find something you love to do and you'll never have to work a day in your life. Happy are those who dream dreams and are ready to pay the price to make them come true. Every day is a gift, that's why they call it the present. Whether you think you can or think you can't, you're right. You are what you eat. You are never too old to learn. You can lead a horse to water but you can't make it drink. You can't teach an old dog new tricks. You never know what you can do until you try. When the cat's away, the mice will play. Who makes himself a sheep will be eaten by the wolves. Wonders will never cease. Two wrongs don't make a right. Walls have ears. Waste not, want not. Variety is the spice of life. There is a trick in every trade. To err is human, to forgive divine. Too many cooks spoil the broth. The early bird catches the worm. The first step is the hardest. The way to a man's heart is through his stomach. Rome was not built in a day. Saying is one thing, doing is another. Tall oaks grow from little acorns. Practice what you preach. Practice makes perfect. One man's trash is another man's treasure. People who live in glass houses should not throw stones. Once bitten, twice shy. No man is an island. No news is good news. No smoke without fire. Nothing ventured, nothing gained. Nobody is perfect. No man can serve two masters. Money is the root of all evil. Necessity is the mother of invention. Never put off till tomorrow what can be done today. Man is the head of the family; woman is the neck that turns the head. Look before you leap. Laughter is the best medicine. Learn to walk before you run. Let not the sun go down on your wrath. Let the chips fall where they may. Lightning never strikes in the same place twice. It's no use crying over spilt milk. When it rains it pours. Knowledge in youth is wisdom in age. Knowledge is power. If two ride a horse, one must ride behind. If you want a friend, be a friend. In the land of the blind, the one eyed man is king. Honesty is the best policy. However long the night, the dawn will break. If you don't stand for something, you'll fall for anything. Give someone an inch and they will take a yard. Give someone enough rope and they will hang themselves. Half a loaf is better than none. Every rose has its thorn. Fool me once, shame on you; fool me twice, shame on me. Fools rush in where angels fear to tread. Don't count your chickens before they're hatched. Easier said than done. Elbow grease is the best polish. Every cloud has a silver lining. Birds of a feather flock together. Constant occupation prevents temptation. Clothes don't make the man. A rolling stone gathers no moss. A smooth sea never made a skilled mariner. A rotten apple spoils the barrel. A stumble may prevent a fall. A watched pot never boils. Beauty is in the eye of the beholder. Better to be alone than in bad company. Better late than never. A friend in need is a friend indeed. A friend to all is a friend to none. A leopard cannot change its spots. A man is known by the company he keeps. An ounce of prevention is worth a pound of cure. A bird in hand is worth two in a bush. A burden of one's own choice is not felt. A chain is no stronger than its weakest link. An apple a day keeps the doctor away. All that glitters is not gold. All good things come to those who wait. Actions speak louder than words. Early to bed, early to rise, makes a man healthy, wealthy and wise. Wars are not paid for in wartime, the bill comes later. The only thing more expensive than education is ignorance. A ploughman on his legs is higher than a gentleman on his knees. If you would be loved, love and be lovable. If a man empties his purse into his head, no man can take it away from him - an investment in knowledge always pays the best interest. By failing to prepare, you are preparing to fail. Do not anticipate trouble or worry about what may never happen. Look before, or you'll find yourself behind. Approve not of him who commends all you say. Clean your fingers before you point at my spots. Many have been ruined by buying good pennyworths. Would you live with ease, do what you ought and not what you please. Better slip with foot than tongue. You cannot pluck roses without fear of thorns, nor enjoy a fair wife without danger of horns. Without justice, courage is weak. Fools multiply folly. Of learned fools I have seen ten times ten; of unlearned wise men I have seen a hundred. Poverty wants some things, luxury many things, avarice all things. There is small revenge in words, but words may be greatly revenged. A man is never so ridiculous by those qualities that are his own as by those that he affects to have. It is better to take many injuries than to give one. An old young man will be a young old man. He is no clown that drives the plow, but he that does clownish things. Fish and visitors stink in three days. Diligence is the mother of good luck. Wealth is not his that has it, but his that enjoys it. He that can have patience can have what he will. God helps them that help themselves. Creditors have better memories than debtors. He that scatters thorns, let him not go barefoot. If you desire many things, many things will seem but a few. There are no gains without pains. At the working man's house hunger looks in but dares not enter. Industry pays debts while despair increases them. God gives all things to industry. Trouble springs from idleness and grievous toil from needless ease. Having been poor is no shame, but being ashamed of it is. �Tis hard but glorious to be poor and honest. Anger is never without a reason but seldom with a good one. Love your neighbor yet don't pull down your hedge. He that does what he should not shall feel what he would not. The honest man takes pains and then enjoys pleasures; the knave takes pleasures and then suffers pains. What you would seem to be, be really. Necessity never made a good bargain. He that lies down with dogs shall rise up with fleas. Being ignorant is not so much a shame as being unwilling to learn. Be civil to all, serviceable to many, familiar with few, friend to one, enemy to none. Love your enemies, for they tell you your faults. Be always ashamed to catch thyself idle. Fear to do evil and you need fear nothing else. Wish not so much to live long as to live well. Beware of little expenses - a small leak will sink a great ship. Keep your eyes wide open before marriage, half shut afterwards. You can bear your own faults and why not a fault in your wife. The way to be safe is never to be secure. He that best understands the world, least likes it. Blessed is he who expects nothing for he shall never be disappointed. Success has ruined many a man. Ill customs and bad advice are seldom forgotten. If your head is wax, don't walk in the sun. There are no fools so troublesome as those who have wit. Quarrels never could last long if on one side only lay the wrong. Visit your aunt, but not every day, and call at your brothers, but not every night</TEXT> <!-- LEAVE PERIOD OFF OF LAST FACT!!! also no space between last letter and "</TEXT>" -->
        </PROVERBS>
    </ENTRY>
    <ENTRY>
        <CUSTOM>
            <TITLE>Custom</TITLE>
            <AUTHOR>User</AUTHOR>
            <TYPE>Custom</TYPE>
            <TEXT>This is an example to show you how to type custom text. To type whatever words, letters, or text you wish, select "Custom" from the text type selection dropdown. Enter the text you wish to type in the textbox that appears, click Reset, and you're ready to go! Works great to practice typing some words that you commonly mess up on such as "teh" instead of "the". Typing these problem words over and over will eventually rewire your brain to type them correctly again! This will help you increase your typing speed and hopefully avoid lots of frustration. You can also use the custom text mode to set the text to your skill level. If you're just learning how to type, choose smaller, common words that are easier to type so you can get the hang of it without feeling overwhelmed. We do provide a random, easy to type words text type that you can select as well. On the other hand, more advanced typists may choose to type longer and more complex words, such as medical dictionary terms. This has the benefit of increasing conscious typing speed versus increasing the subconscious muscle memory typing skill that is learned by typing common words over and over again. Go ahead and try it! Anything longer than one line will simply be repeated so no need to copy and paste short custom text over and over.</TEXT>
        </CUSTOM>
    </ENTRY>
</root>
