<?php
function escape($string){
	return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

function printFormError($string){
	echo '<p class="phpError">' . implode('<br />', $string) . '</p>';
}

function trimTrailingZeroes($input){
	return $input + 0;
}

function inMaintenanceMode(){
	return config::get('mysql/maint_mode');
}

function fullSiteURL(){
	return config::get('constants/rootUrl');
}

function exitPHPwithFooter(){
	include $_SERVER['DOCUMENT_ROOT'] . '/includes/overall/footer.php';
	exit();
}

function getTutorData($userData, $startDate = null, $endDate = null){
	
	// get typing lessons
	if(inMaintenanceMode()){
		include $_SERVER['DOCUMENT_ROOT'] . '/resources/STO_tl.php';	// $tl_data
		$lessons = $tl_data;
	} else if ($userData != null) {
		$joinFilter = array('user_id', '=', $userData->id);
		if($startDate != null && $startDate != ''){
			//$dataFilter = array_merge($dataFilter, array(array('timestamp','timestamp'), array('>','='), array($startDate,'')));
			//$dataFilter = array_merge($dataFilter, array('(','timestamp', '>', $startDate,')'));
			//$dataFilter = array_merge($dataFilter, array('(','timestamp', '>', $startDate, 'OR', 'timestamp', 'IS NULL', '',')'));
			//$dataFilter = array_merge($dataFilter, array('timestamp', '>', $startDate));
			$joinFilter = array_merge($joinFilter, array('timestamp', '>', $startDate));
		}
		if($endDate != null){
			$joinFilter = array_merge($joinFilter, array('timestamp', '<=', $endDate));
		}
		
		$db_obj = DB::getInstance()->get('typing_lessons', array('type', '!=', 'backup'),'typing_lessons.id, timestamp, type, lesson_type, lesson_num, lesson_order, lesson_count, lesson_timeSpent, description, purpose, hint, display_text, letters, text_type, text_to_type, wpm, accuracy', array('lesson_order', 'ASC'), array('lesson_history', 'id', 'lesson_module', 'LEFT', $joinFilter),  array(99999,0));
		$lessons = $db_obj->results();
		
		$wpmTarget = $userData->tl_wpm_target;
		$accTarget = $userData->tl_accuracy_target;
		
		if($wpmTarget <= 0){
			$wpmTarget = 1;
		}
		if($accTarget <= 0){
			$accTarget = 0.1;
		}
		
	} else {
		$db_obj = DB::getInstance()->get('typing_lessons', array('type', '!=', 'backup'),'typing_lessons.id, type, lesson_type, lesson_num, lesson_order, lesson_count, lesson_timeSpent, description, purpose, hint, display_text, letters, text_type, text_to_type', array('lesson_order', 'ASC'), null,  array(9999999,0));
		$lessons = $db_obj->results();
		
		$wpmTarget = 10;
		$accTarget = 90;
	}
	
	// create array of typing lessons with only most recent lesson history for every particular lesson
	$lessonsWithoutFullHistory = array();
	$lwhSafeCount = count($lessons) - 1;
	$lwhCounter = 0;
	
	for($ii = 0; $ii < $lwhSafeCount; $ii++){
		if($lessons[$ii]->lesson_order == $lessons[$ii+1]->lesson_order){
			$lessonsWithoutFullHistory[$lwhCounter] = $lessons[$ii+1];
		} else {
			$lessonsWithoutFullHistory[$lwhCounter] = $lessons[$ii];
			$lwhCounter++;
		}
	}
	$lessonsWithoutFullHistory[$lwhCounter] = $lessons[$ii];
	
	$mostRecentDate = '';
	$mostRecentLessonIndex = -1;
	$worstLessonIndex = -1;
	$classicPassedCount = 0;
	$advancedPassedCount = 0;
	$classicLessonCount = 0;
	$advancedLessonCount = 0;
	
	if($wpmTarget <= 0){
		$wpmTarget = 1;
	}
	if($accTarget <= 0){
		$accTarget = 0.1;
	}
	
	$retArr = getLessonHTMLfromArray($lessons, $wpmTarget, $accTarget, $mostRecentLessonIndex, $worstLessonIndex);
	$classicLessonsHTML = $retArr[0];
	$advancedLessonsHTML = $retArr[1];
	$classicLessonCount = $retArr[2];
	$classicPassedCount = $retArr[3];
	$advancedLessonCount = $retArr[4];
	$advancedPassedCount = $retArr[5];
	$keyboardBasicsHTML = $retArr[6];
	$keyboardBasicsCount = $retArr[7];
	$nextLesson = 0;
	
	
	// see if passed most recent lesson before going for next lesson
	if($mostRecentLessonIndex >= 0){
		$MRLscore = min(($lessons[$mostRecentLessonIndex]->wpm / $wpmTarget), ($lessons[$mostRecentLessonIndex]->accuracy / $accTarget));
		if($lessons[$mostRecentLessonIndex]->type == "keyboard")
			$MRLscore = 1;
		if($MRLscore > 0.8){
			$numLessons = sizeof($lessons);
			$nextLesson = ($mostRecentLessonIndex + 1) % $numLessons;
			$nextLessonScore = min(($lessons[$nextLesson]->wpm / $wpmTarget), ($lessons[$nextLesson]->accuracy / $accTarget));
			$lcv = 0;
			while($nextLessonScore > 0.8 && $lcv < $numLessons){
				$nextLesson = ($nextLesson + 1) % $numLessons;
				$nextLessonScore = $lessons[$nextLesson]->wpm / $wpmTarget;
				$lcv++;
			}
		} else {
			if($mostRecentLessonIndex < 0){
				$nextLesson = 0;
			} else {
				$nextLesson = $mostRecentLessonIndex;
			}
		}
	}
	
	if($nextLesson <= 0){
		if($wpmTarget >= Config::get('constants/touchTypingAchieved')){/* don't recommend keyboard basics lessons if touch typing skill is achieved */
			$nextLesson = 2;
		} else {
			$nextLesson = 0;	// always suggest a lesson, so suggest lesson 1 if user is just starting
		}
		
	}
	
	$nextLessonHTML = '';
	if(isset($nextLesson) && $nextLesson >= 0){
		$retArr = getLessonHTMLfromArray(array($lessons[$nextLesson]), $wpmTarget, $accTarget, $param1, $param2, 'micro');
		 
		if($lessons[$nextLesson]->type == "advanced"){
			$nextLessonHTML = $retArr[1];
		} else if($lessons[$nextLesson]->type == "keyboard"){
			$nextLessonHTML = $retArr[6];
		} else {
			$nextLessonHTML = $retArr[0];
		}
	}
	
	$mostRecentLessonHTML = '';
	if(isset($mostRecentLessonIndex) && $mostRecentLessonIndex >= 0){
		$retArr = getLessonHTMLfromArray(array($lessons[$mostRecentLessonIndex]), $wpmTarget, $accTarget, $param1, $param2, 'micro');
	 
		if($lessons[$mostRecentLessonIndex]->type == "advanced"){
			$mostRecentLessonHTML = $retArr[1];
		} else if($lessons[$mostRecentLessonIndex]->type == "keyboard"){
			$mostRecentLessonHTML = $retArr[6];
		} else {
			$mostRecentLessonHTML = $retArr[0];
		}
	}
	
	$worstLessonHTML = '';
	if(isset($worstLessonIndex) && $worstLessonIndex >= 0){
		$retArr = getLessonHTMLfromArray(array($lessons[$worstLessonIndex]), $wpmTarget, $accTarget, $param1, $param2, 'micro');
		 
		if($lessons[$worstLessonIndex]->type == "advanced"){
			$worstLessonHTML = $retArr[1];
		} else {
			$worstLessonHTML = $retArr[0];
		}	
	}
	
	return array(
		'lessons' => $lessons,
		'classicLessonHTML' => $classicLessonsHTML,
		'advancedLessonHTML' => $advancedLessonsHTML,
		'lastLesson' => $mostRecentLessonIndex,
		'lastLessonHTML' => $mostRecentLessonHTML,
		'worstLesson' => $worstLessonIndex,
		'worstLessonHTML' => $worstLessonHTML,
		'nextLessonHTML' => $nextLessonHTML,
		'wpmTarget' => $wpmTarget,
		'accTarget' => $accTarget,
		'classicPassedCount' => $classicPassedCount,
		'advancedPassedCount' => $advancedPassedCount,
		'classicLessonCount' => $classicLessonCount,
		'advancedLessonCount' => $advancedLessonCount,
		'lessonsWithoutFullHistory' => $lessonsWithoutFullHistory,
		'keyboardBasicsHTML' => $keyboardBasicsHTML,
		'keyboardBasicsCount' => $keyboardBasicsCount
	);
}

function getLessonHTMLfromArray($lessonArr, $wpmTarget = 'default', $accTarget = 'default', &$mostRecentLessonIndex = 0, &$worstLessonIndex = 0, $idPrefix = ''){
	$containerStart = '';
	$containerEnd = '</div>';
	$containerStartHist = '';
	$containerEndHist = '';
	$stoTT_HTML = '';
	$stoTT_HTML_history = '';
	$wpmTarget = ($wpmTarget == 'default') ? Config::get('constants/wpmLessonTarget') : $wpmTarget;
	$accTarget = ($accTarget == 'default') ? Config::get('constants/accLessonTarget') : $accTarget;
	$classicPassedCount = 0;
	$advancedPassedCount = 0;
	$classicLessonHTML = '';
	$advancedLessonHTML = '';
	$keyboardBasicsHTML = '';
	$classicLessonCount = 0;
	$advancedLessonCount = 0;
	$keyboardBasicsCount = 0;
	$lessonHistCount = 0;
	$lessonIsStacked = false;
	$hasDisplayText = false;
	
	$letterSymbolMatch = array("Q W E R T Y U I O P", "A S D F G H J K L ;", "Z X C V B N M , .", "R T Y U F G H J V B N M", "E I D K C ,", "W O S L X .", "Q P A ; ' \" Z ? ! ^", "^", "*");
	$letterSymbolSwap = array("Top Row", "Home Row", "Bottom Row", "Index Finger", "Middle Finger", "Ring Finger", "Pinky Finger", "Shift", "All");
	$contCount = 1;
	
	if(!isset($worstLessonIndex)){
		$worstLessonIndex = -1;
	}
	if(!isset($worstLessonScore)){
		$worstLessonScore = -1;
	}
	
	for($ii = 0; $ii < sizeof($lessonArr); $ii++){
		
		$lessonFullTitle = $lessonArr[$ii]->lesson_type . ' ' . $lessonArr[$ii]->lesson_num;
		$lessonMiniTitle = $lessonArr[$ii]->lesson_type[0] . ' ' . $lessonArr[$ii]->lesson_num;
		
		$theLetters = $lessonArr[$ii]->display_text;
		
		if($theLetters == ''){
			$theLetters = implode(' ', str_split(strtoupper($lessonArr[$ii]->letters)));
			$hasDisplayText = false;
		} else {
			$hasDisplayText = true;
		}
		
		$letterClass = 'lessonText';
		
		if(!$hasDisplayText){
			$letterClass .= ' largeLessonText';
		} else {
			if(strlen($theLetters) > 5){
				if(strpos($theLetters, " ") > 0){
					$letterClass .= ' doubleLineLessonText';
				} else {
					$letterClass .= ' smallLessonText';
				}
				
			} else { 
				$letterClass .= ' normalLessonText';
			}
		}
						
		if($lessonArr[$ii]->lesson_type == "Review"){
			if($containerEnd == ''){
				$containerStart = '';
			} else {
				$containerStart = '<div class="loneReview">';
				$contCount++;
			}
			
			$containerEnd = '</div>';
			
			if($contCount == 2 || $contCount == 5 || $contCount == 8 || $contCount == 11 || $contCount == 12 || $contCount == 17 
							   || $contCount == 18 || $contCount == 20 || $contCount == 21 || $contCount == 22){
				$containerEnd .= '<div class="lessonGroupTitle">' . $theLetters . '</div><div class="groupIconImg"></div></div>';
			}
			
			if($contCount == 13){
				$containerStart = '<div class="lessonGroup ' . $lessonArr[$ii]->type . '">' . $containerStart;
			}
			
			
			$passedSymbol = '<div class="star"><i class="fa fa-star" aria-hidden="true"></i></div>';
		} else {
			if($containerEnd == ''){
				$containerStart = '';
				$containerEnd = '';
			} else {
				$containerStart = '<div class="lessonContainer cont' . $contCount . '">';
				if($contCount == 1 || $contCount == 2 || $contCount == 5 || $contCount == 8 || $contCount == 11 || $contCount == 12 
								   || $contCount == 17 || $contCount == 18 || $contCount == 20 || $contCount == 21){
					$containerStart = '<div class="lessonGroup ' . $lessonArr[$ii]->type . ' cID_' . $contCount . '">' . $containerStart;
				}
				$contCount++;
				$containerEnd = '';
			}
			//$passedSymbol = '<div class="checkmark">&#x2714;</div>';
			$passedSymbol = '<div class="checkmark"><i class="fa fa-check" aria-hidden="true"></i></div>';
		}
		
		if(sizeof($lessonArr) == 1){
			$containerStart = '';
			$containerEnd = '';
		}
		
		$lessonStatus = '';
		$wpmStatus = ' class="warn"';
		$accStatus = ' class="warn"';
		$stoTT_HTML = '';
		$stackedImages = '';
		$lessonMod = $lessonArr[$ii]->lesson_order;
		
		if(isset($lessonArr[$ii]->wpm) && $lessonArr[$ii]->wpm != null){
			
			$lessonHistCount = 0;
			$lessonIsStacked = false;
			$stoTT_HTML_history = '';
			$lessonWPMave = $lessonArr[$ii]->wpm;
			$lessonAccAve = $lessonArr[$ii]->accuracy;
			
			// handle multiple copies of same lesson
			while(sizeof($lessonArr) > $ii+1 && $lessonMod == $lessonArr[$ii+1]->lesson_order){
				
				if($lessonArr[$ii]->wpm >= $wpmTarget){$wpmStatus = ' class="success"';}
				if($lessonArr[$ii]->accuracy >= $accTarget){$accStatus = ' class="success"';}

				if ($lessonArr[$ii]->type == "keyboard"){
					$stoTT_HTML_history = '<div class="mStat"><div class="success">Completed</div><div class="makeDateReadable">' . $lessonArr[$ii]->timestamp . '</div></div>' . $stoTT_HTML_history;
				} else {
					$stoTT_HTML_history = '<div class="mStat"><div' . $wpmStatus . '>' . $lessonArr[$ii]->wpm . '<span class="noteSmall"> WPM</span></div><div' . $accStatus . '>' . $lessonArr[$ii]->accuracy . '<span class="noteSmall">%</span></div><div class="makeDateReadable">' . $lessonArr[$ii]->timestamp . '</div></div>' . $stoTT_HTML_history;
				}
				$lessonIsStacked = true;
				$lessonHistCount++;
				$ii++;
				
				$wpmStatus = ' class="warn"';
				$accStatus = ' class="warn"';
				
				if($lessonHistCount == 1){
					$stackedImages = '<div class="lessonButton classic ' . strtolower($lessonArr[$ii]->lesson_type) . ' bottomStack"><a><div class="lessonTitle">Lesson</div></a></div>';
				} else if($lessonHistCount == 4){
					$stackedImages .= '<div class="lessonButton classic ' . strtolower($lessonArr[$ii]->lesson_type) . ' middleStack"><a><div class="lessonTitle">Lesson</div></a></div>';
				}
				$lessonWPMave += $lessonArr[$ii]->wpm;
				$lessonAccAve += $lessonArr[$ii]->accuracy;
			}
			
			$lessonWPMave = $lessonWPMave / ($lessonHistCount + 1);
			$lessonAccAve = $lessonAccAve / ($lessonHistCount + 1);
			
			if($lessonIsStacked == true){
				$containerStart = $containerStart . '<div class="stackedLesson">';
				$stoTT_HTML_history =  "<div class=\"histTitle\">History</div><div class=\"lessonHist\">" .$stoTT_HTML_history . "</div>";
			} else {
				$stoTT_HTML_history = '';
			}
		
			// see if most recent lesson
			if(!isset($mostRecentDate) || $mostRecentDate == '' || $lessonArr[$ii]->timestamp > $mostRecentDate){
				$mostRecentDate = $lessonArr[$ii]->timestamp;
				$mostRecentLessonIndex = $ii;
			}
			
			$wpmScore = $lessonArr[$ii]->wpm / $wpmTarget;
			$accScore = $lessonArr[$ii]->accuracy / $accTarget;
			
			$lowestScore = min($wpmScore, $accScore);
			if($lowestScore < 1){
				$lessonScore = $lowestScore;
			} else {
				$lessonScore = $wpmScore + $accScore;
			}
			
			if($lessonArr[$ii]->wpm >= $wpmTarget){$wpmStatus = ' class="success"';}
			if($lessonArr[$ii]->accuracy >= $accTarget){$accStatus = ' class="success"';}
			
			if($lessonScore >= 0){
				if($lessonScore >= 1 || $lessonArr[$ii]->type == "keyboard"){ /* keyboard basics are always 100% pass */
					$lessonStatus = 'p100 passed ';
					if ($lessonArr[$ii]->type == "classic"){
						$classicPassedCount++;
					} else if($lessonArr[$ii]->type == "advanced"){
						$advancedPassedCount++;
					}
				} else {
					if ($lessonScore >= 0.5){
						if($lessonScore >= 0.95){
							$lessonStatus = 'p95 progress ';
						} else if($lessonScore >= 0.85){
							$lessonStatus = 'p85 progress ';
						} else if($lessonScore >= 0.75){
							$lessonStatus = 'p75 progress ';
						} else {
							$lessonStatus = 'p50 progress ';
						} 
					} else {
						$lessonStatus = 'p0 progress fail ';
					} 
					
					// see if worse lesson score
					if($worstLessonIndex < 0 || $lessonScore < $worstLessonScore){
						$worstLessonScore = $lessonScore;
						$worstLessonIndex = $ii;
					} 
				}
			}
			
			if($lessonIsStacked){
				$lessonStatus .= 'topStack ';
			}
		
			$stoTT_HTML_new = '<div class="topTitle">' . ucfirst($lessonArr[$ii]->type) . ' ' . $lessonFullTitle . '</div><div class="large_mStat">' . $theLetters . '</div>';
			
			if($lessonArr[$ii]->type != "keyboard"){ /* keyboard basics don't need this data */
				$stoTT_HTML_new .= '<div class="mStat"><span' . $wpmStatus . '>' . $lessonArr[$ii]->wpm . '<span class="noteSmall"> WPM</span></span><span' . $accStatus . '>' . $lessonArr[$ii]->accuracy . '<span class="noteSmall">%</span></span></div>' . 
				'<div class="splitCol doubleCol targets"><div class="label">Targets:</div><div>' . $wpmTarget . ' <span class="noteSmall">WPM</span></div><div>' . $accTarget . ' <span class="noteSmall"> %</span></div></div>' . 
				'<div class="splitCol doubleCol averages"><div class="label">Average:</div><div>' . round($lessonWPMave) . ' <span class="noteSmall">WPM</span></div><div>' . round($lessonAccAve, 1) . ' <span class="noteSmall"> %</span></div></div>' . 
				'<div class="splitCol singleCol count" title="Total number of times this lesson has been completed"><div class="label">Count:</div><div>' . $lessonArr[$ii]->lesson_count . '</div></div>' . 
				'<div class="splitCol singleCol timeSpent" title="Time spent on this specific lesson"><div class="label">Time Spent:</div><div class="makeTimeSpentReadable">' . makeSecondsReadable($lessonArr[$ii]->lesson_timeSpent) . '</div></div>';
			} else {
				$stoTT_HTML_new .= '<div class="mStat"><div class="success">Complete</div></div>';
			}
			
			$stoTT_HTML .= $stoTT_HTML_new . '<div class="splitCol singleCol date"><div class="label">Completed:</div><div><div class="makeDateReadable">' . $lessonArr[$ii]->timestamp . '</div></div></div>' . $stoTT_HTML_history . 
				'<div class="attention"><span>Score: </span>' . round($lessonScore * 100) . ' pts</div>' . $stoTT_HTML;
			
			
			/*
			ini_set('xdebug.var_display_max_data', '8024');
			var_dump($stoTT_HTML);
			exit();*/
			
			if($stoTT_HTML_history != ''){
				$stoTT_HTML .= '</div>';
			}
			
			
		}
		
		if($lessonArr[$ii]->type == "advanced"){
			$titleDecoration = '<div class="titDec"><i class="fa fa-caret-right" aria-hidden="true"></i><i class="fa fa-caret-left" aria-hidden="true"></i></div>';
			$pageLink = "typing-tutor.php";
		} else if($lessonArr[$ii]->type == "keyboard"){
			$titleDecoration = '';
			$pageLink = "keyboard-basics.php";
		} else {
			$titleDecoration = '';
			$pageLink = "typing-tutor.php";
		}
		
		$tempLessonHTML = $containerStart . $stackedImages . '<div id="' . $idPrefix . 'lessonMod_' . $lessonArr[$ii]->id .  '" class="' . $lessonStatus . 'lessonButton ' . $lessonArr[$ii]->type . ' ' . strtolower($lessonArr[$ii]->lesson_type) . '"><a href="/' . $pageLink . '?mod=' . $lessonArr[$ii]->id . '">' . $titleDecoration . '<div class="lessonTitle">' . $lessonFullTitle . '</div><div class="lessonTitle miniTitle">' . $lessonMiniTitle . '</div><div class="meter"><span></span>' . $passedSymbol . '</div><div class="' . $letterClass . '"><span class="letters">' . $theLetters . '</span></div></a><div class="stoTT">' . $stoTT_HTML . '</div></div>' . $containerEnd;
		
		
		/*ini_set('xdebug.var_display_max_data', '8024');
		var_dump($tempLessonHTML);
		exit();*/
		
		if ($lessonArr[$ii]->type == "classic"){
			$classicLessonHTML .= $tempLessonHTML;
			$classicLessonCount++;
		} else if($lessonArr[$ii]->type == "advanced"){
			$advancedLessonHTML .= $tempLessonHTML;
			$advancedLessonCount++;
		} else if($lessonArr[$ii]->type == "keyboard"){
			$keyboardBasicsHTML .= $tempLessonHTML;
			$keyboardBasicsCount++;
		}
		
	}
	
	return array($classicLessonHTML, $advancedLessonHTML, $classicLessonCount, $classicPassedCount, $advancedLessonCount, $advancedPassedCount, $keyboardBasicsHTML, $keyboardBasicsCount);
}

function getLessonIdIndexFromMod($lessonArr, $theMod){
	$retObj = null;
	$prevIndex = null;
	foreach($lessonArr as $key=>$struct) {
		if ($theMod == $struct->id) {
			$retObj = (object)['index' => $key, 'id' => $struct->id, 'prevIndex' => $prevIndex, 'prevId' => $lessonArr[$prevIndex]->id];
			break;
		}
		$prevIndex = $key;
	}
	return $retObj;
}

function getTestLength($userData, $debugMode){
	$testLength = array('', '', '', '', '', '', '', '');
	if($userData->tt_length == 30){
		$testLength[0] = ' selected="selected"';
	} else if($userData->tt_length == 120){
		$testLength[2] = ' selected="selected"';
	} else if($userData->tt_length == 180){
		$testLength[3] = ' selected="selected"';
	} else if($userData->tt_length == 300){
		$testLength[4] = ' selected="selected"';
	} else if($userData->tt_length == 600){
		$testLength[5] = ' selected="selected"';
	} else if($userData->tt_length == 900){
		$testLength[6] = ' selected="selected"';
	}else if($userData->tt_length == 1200){
		$testLength[7] = ' selected="selected"';
	} else if($userData->tt_length == 3 && $debugMode){
		$testLength[8] = ' selected="selected"';
	} else {
		// default normal length = 60 secs
		$testLength[1] = ' selected="selected"';
	}
	return $testLength;
}

function getAd($adType = "lead", $pageName = null, $debugMode = false, $userPremium = false, $restrictResponsiveCss=null){
	$startComments = "";
	$endComments = "";
	$backgroundColor = "";
	$useSovrn = true;
	
	// debug mode check first override, then if user is premium
	if($debugMode || Config::get('constants/debugMode')){
		$startComments = "<!--";
		$endComments = "-->";
		if($userPremium){
			$backgroundColor = " style=\"background: orange;\"";
		} else {
			$backgroundColor = " style=\"background: gray;\"";
		}
	} else if($userPremium){
		return '';
	}
	
	if($pageName == "typingTutor"){
		if($adType == "lead"){
		
			return '<div id="leadAd"' . $backgroundColor . '>' . 
						$startComments . '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>' . $endComments .
						'<!-- STO_TutorLead -->' .
						$startComments . '<ins class="adsbygoogle"
							 style="display:inline-block;width:728px;height:90px"
							 data-ad-client="ca-pub-2169602241028960"
							 data-ad-slot="0241532016"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . $endComments .
					'</div>';
		} else if($adType = "rightSky"){
			return '<div id="rightAd"' . $backgroundColor . '>' .
						'<!-- tutor_rightSky_responsive -->' . $startComments . '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-2169602241028960"
							 data-ad-slot="7118120206"
							 data-ad-format="auto"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . $endComments . 
					'</div>';
		}
	} else if($pageName == "typingTest"){
		if($adType == "lead"){
			return '<div id="leadAd"' . $backgroundColor . '>' .	
						'<!-- Test -->' . $startComments . 
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-2169602241028960"Fl
							 data-ad-slot="6615279405"
							 data-ad-format="auto"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . $endComments . 
					'</div>';
		} else if($adType == "rightSky"){
			return '<div id="rightSkyAd"' . $backgroundColor . '><!-- test_rightSky_responsive -->' . $startComments . 
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-2169602241028960"
							 data-ad-slot="4164653804"
							 data-ad-format="auto"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . 
						'<div class="field longLabel stoUI" style="margin-top:25px;">Want no more ads?</div>
						<div class="mediumBtn insideUpdateBtn flatGoldBtn" style="margin-top:5px; max-width:253px; color:white; ">
							<a href="/goPremium.php" style="text-decoration:none;">
								<div style="position:relative; padding: 7px 0px;">
									<img src="/images/crown.svg" style="height:50px; display:inline-block; position:absolute; left:-11px; top:0px;"></img>
									<span style="color:#583100; margin-left: 50px; font-family: \'roboto\';">Go Premium!</span>
								</div>
								<div class="goPremiumFreeTag">Free 7-day Trial</div>
							</a>
						</div>' .
						$endComments .
					'</div>';
		} else if($adType == "thirdBottom"){
			if($useSovrn){
				return '<div id="rectAd"' . $backgroundColor . '><!-- Sovrn 300x250_Test_BTF -->' . $startComments . 
						'<script type="text/javascript" src="//ap.lijit.com/www/delivery/fpi.js?z=402269&u=SpeedTyping&width=300&height=250"></script>' . $endComments .
					'</div>';
			} else {
				return '<div id="rectAd"' . $backgroundColor . '><!-- STO_TestBelowResponsive -->' . $startComments .
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-2169602241028960"
							 data-ad-slot="8355861386"
							 data-ad-format="auto"
							 data-full-width-responsive="true"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . $endComments .
					'</div>';
			}
		}
	} else if($pageName == "typeTheAlphabet"){
		if($adType == "lead"){
			return '<div id="topLeadPlug" class="leadAd" ' . $backgroundColor . '>' .	
						'<!-- STO_TTA_ResponsiveTop -->' . $startComments . 
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-2169602241028960"
							 data-ad-slot="1474493309"
							 data-ad-format="auto"
							 data-full-width-responsive="true"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . $endComments . 
					'</div>';
		} else if($adType == "rightSky"){
			return '<div id="rightSky"' . $backgroundColor . '><!-- STO_TTA_ResponsiveSkySwitch -->' . $startComments . 
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-2169602241028960"
							 data-ad-slot="4666674999"
							 data-ad-format="auto"
							 data-full-width-responsive="true"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . $endComments .
					'</div>';
		} else if($adType == "thirdBottom"){
			return '<div id="bottomLeadPlug"' . $backgroundColor . '><!-- STO_TTA_ResponsiveBelow -->' . $startComments .
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-2169602241028960"
							 data-ad-slot="4979280582"
							 data-ad-format="auto"
							 data-full-width-responsive="true"></ins>
						<script>
							(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . $endComments .
					'</div>';
		}
	} else if($pageName == "fastFireTyper"){
		if($adType == "lead_ATF"){
			if($useSovrn){
				return '<div id="topleadPlug" class="sensePlug" ' . $backgroundColor . '><!-- Sovrn 728x90_Fire_ATF -->' . $startComments . 
						'<script type="text/javascript" src="//ap.lijit.com/www/delivery/fpi.js?z=402265&u=SpeedTyping&width=728&height=90"></script>' . $endComments .
					'</div>';
			} else { 
				return '<div id="topleadPlug" class="sensePlug" ' . $backgroundColor . '>' .	
						'<!-- STO_FFT_ResponsiveTop -->' . $startComments . 
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<ins class="adsbygoogle"
								 style="display:block"
								 data-ad-client="ca-pub-2169602241028960"
								 data-ad-slot="5176970562"
								 data-ad-format="auto"
								 data-full-width-responsive="true"></ins>
							<script>' . $endComments . 
					'</div>';
			}
		} else if($adType == "lead_BTF"){
			if($useSovrn){
				return '<div id="topsecondLeadPlug" class="sensePlug" ' . $backgroundColor . '><!-- Sovrn 728x90_Fire_BTF -->' . $startComments . 
						'<script type="text/javascript" src="//ap.lijit.com/www/delivery/fpi.js?z=402266&u=SpeedTyping&width=728&height=90"></script>' . $endComments .
					'</div>';
			} else {
				return '<div id="topsecondLeadPlug" class="sensePlug" ' . $backgroundColor . '>' .	
						'<!-- STO_FFT_ResponsiveBelow -->' . $startComments . 
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<ins class="adsbygoogle"
								 style="display:block"
								 data-ad-client="ca-pub-2169602241028960"
								 data-ad-slot="1047574741"
								 data-ad-format="auto"
								 data-full-width-responsive="true"></ins>
							<script>' . $endComments . 
					'</div>';
			}
		} else if($adType == "largeRect"){
			if($useSovrn){
				return '<div id="largeRectPlug" class="sensePlug" ' . $backgroundColor . '><!-- Sovrn 300x250_Fire_BTF -->' . $startComments . 
						'<script type="text/javascript" src="//ap.lijit.com/www/delivery/fpi.js?z=402267&u=SpeedTyping&width=300&height=250"></script>' . $endComments .
					'</div>';
			} else {
				return '<div id="largeRectPlug" class="sensePlug" ' . $backgroundColor . '><!-- STO_FFT_ResponsiveBottom -->' . $startComments . 
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-2169602241028960"
							 data-ad-slot="7393491242"
							 data-ad-format="auto"
							 data-full-width-responsive="true"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . $endComments .
					'</div>';
			}
		} 
	} else if($pageName == "typingLessons"){
		if($adType == "lead_ATF"){
			if($useSovrn){
				return '<div id="topleadPlug" class="sensePlug" ' . $backgroundColor . '><!-- Sovrn Lessons_728x90_ATF -->' . $startComments . 
						'<script type="text/javascript" src="//ap.lijit.com/www/delivery/fpi.js?z=404615&u=SpeedTyping&width=728&height=90"></script>' . $endComments .
					'</div>';
			} else { 
				return '<div id="' . $adType . '"' . $backgroundColor . '><!-- STO_Lessons_ATF -->' . $startComments .
					'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						 style="display:block"
						 data-ad-client="ca-pub-2169602241028960"
						 data-ad-slot="5426630200"
						 data-ad-format="auto"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>' . $endComments .
					'</div>';
			}
		} else if($adType == "lead_BTF"){
			if($useSovrn){
				return '<div id="topsecondLeadPlug" class="sensePlug" ' . $backgroundColor . '><!-- Sovrn Lessons_728x90_BTF -->' . $startComments . 
						'<script type="text/javascript" src="//ap.lijit.com/www/delivery/fpi.js?z=404616&u=SpeedTyping&width=728&height=90"></script>' . $endComments .
					'</div>';
			} else {
				return '<div id="' . $adType . '"' . $backgroundColor . '><!-- STO_Lessons_BTF -->' . $startComments .
					'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						 style="display:block"
						 data-ad-client="ca-pub-2169602241028960"
						 data-ad-slot="3949897002"
						 data-ad-format="auto"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>' . $endComments .
					'</div>';
			}
		} else if($adType == "generalAsideAd"){
			if($useSovrn){
				return '<div id="generalAsideAd" class="sensePlug" ' . $backgroundColor . '><!-- Sovrn Lessons_300x250 -->' . $startComments . 
						'<script type="text/javascript" src="//ap.lijit.com/www/delivery/fpi.js?z=405558&u=SpeedTyping&width=300&height=250"></script>' . $endComments .
					'</div>';
			} else {
				return '<div id="generalAsideAd" class="sensePlug" ' . $backgroundColor . '><!-- STO_Lessons_generalAsideAd -->' . $startComments . 
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 data-ad-client="ca-pub-2169602241028960"
							 data-ad-slot="3949897002"
						 data-ad-format="auto"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . $endComments .
						'</div>';
			}
		} 
	} else if($pageName == "typingGames"){
		if($adType == "lead"){
			return '<div id="topLeadAd" class="leadAd" ' . $backgroundColor . '>' .	
						'<!-- STO_TypingGames_Lead -->' . $startComments . 
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
							<ins class="adsbygoogle"
								 style="display:inline-block;width:728px;height:90px"
								 data-ad-client="ca-pub-2169602241028960"
								 data-ad-slot="1403194606"></ins>
							<script>
							(adsbygoogle = window.adsbygoogle || []).push({});
							</script>' . $endComments . 
					'</div>';
		} else if($adType == "rightSky"){
			return '<div id="sideSky"' . $backgroundColor . '><!-- test_rightSky_responsive -->' . $startComments . 
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-2169602241028960"
							 data-ad-slot="4164653804"
							 data-ad-format="auto"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . $endComments .
					'</div>';
		} else if($adType == "thirdBottom"){
			return '<div id="rectAd"' . $backgroundColor . '><!-- general_all_responsive -->' . $startComments .
						'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
						<ins class="adsbygoogle"
							 style="display:block"
							 data-ad-client="ca-pub-2169602241028960"
							 data-ad-slot="1439369800"
							 data-ad-format="auto"
							 data-full-width-responsive="true"></ins>
						<script>
						(adsbygoogle = window.adsbygoogle || []).push({});
						</script>' . $endComments .
					'</div>';
		}
	} else if($pageName == null){
		if($restrictResponsiveCss == null){
			return '<div id="' . $adType . '"' . $backgroundColor . '><!-- general_all_responsive -->' . $startComments .
					'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						 style="display:block;"
						 data-ad-client="ca-pub-2169602241028960"
						 data-ad-slot="1439369800"
						data-ad-format="auto"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>' . $endComments .
				'</div>';
		} else {
			return '<div id="' . $adType . '"' . $backgroundColor . '><!-- general_all_responsive -->' . $startComments .
					'<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<ins class="adsbygoogle"
						 style="display:block;' . $restrictResponsiveCss . '"
						 data-ad-client="ca-pub-2169602241028960"
						 data-ad-slot="1439369800"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>' . $endComments .
				'</div>';
		}
		
	}
	
}

function makeSecondsReadable($seconds){
	if($seconds > 3600){
		return floor($seconds / 3600) . gmdate("\h i\m s\s", $seconds % 3600);
	} else {
		return gmdate("i\m s\s", $seconds % 3600);
	}
}

function getReadableTimeDiff($inputTime){	// don't ever try to do this - do datetime client side

	$date_a = date('m/d/Y h:i:s a', time());
	$date_b = new DateTime($inputTime);
	$date_b = $date_b->format('m/d/Y h:i:s a');
	
	
	$date1 = date_create(date('m/d/Y h:i:s a', time()));
	$date2 = date_create(date('m/d/Y h:i:s a', time()-5000000));

	$interval = date_diff($date2,$date1);
	
	if($interval == false){
		return 'false';
	}
	
	return $retStr;
}

//'tlDateFilterEnum' => array("TODAY" => 0, "YESTERDAY" => 1, "LAST7DAYS" => 2, "LAST30DAYS" => 3, "THIS_WEEK" => 4, "LAST_WEEK" => 5, "THIS_MONTH" => 6, "LAST_MONTH" => 7, "ALL_TIME" => 8, "CUSTOM_RANGE" => 9)

function GetJSMoment($theSetting){
	$startDateStr = "moment()";
	$endDateStr = "moment()";
	
	switch($theSetting){
		case 0:	//	today
		break;
		case 1:	// yesterday
			$startDateStr .= ".subtract(1, 'days')";
			$endDateStr .= ".subtract(1, 'days')";
		break;
		case 2:	// Last 7 days
			$startDateStr .= ".subtract(6, 'days')";
		break;
		case 3: // Last 30 days
			$startDateStr .= ".subtract(29, 'days')";
		break;
		case 4: // This week
			$startDateStr .= ".startOf('week')";
			$endDateStr .= ".endOf('week')";
		break;
		case 5: // Last week
			$startDateStr .= ".subtract(1, 'week').startOf('week')";
			$endDateStr .= ".subtract(1, 'week').endOf('week')";
		break;
		case 6:	// This month
			$startDateStr .= ".startOf('month')";
			$endDateStr .= ".endOf('month')";
		break;
		case 7:	// Last month
			$startDateStr .= ".subtract(1, 'month').startOf('month')";
			$endDateStr .= ".subtract(1, 'month').endOf('month')";
		break;
		case 8: // All time
			$startDateStr = "moment('2020-01-01')";
			$endDateStr = "moment().add(10, 'year')";
		break;
		case 9:	//	tomorrow
			$startDateStr .= ".add(1, 'days')";
			$endDateStr .= ".add(1, 'days')";
		break;
		case 10:	// Next 7 days
			$endDateStr .= ".add(6, 'days')";
		break;
		case 11: // Next 30 days
			$endDateStr .= ".add(29, 'days')";
		break;
		case 12: // Next week
			$startDateStr .= ".add(1, 'week').startOf('week')";
			$endDateStr .= ".add(1, 'week').endOf('week')";
		break;
		case 13:	// Next month
			$startDateStr .= ".add(1, 'month').startOf('month')";
			$endDateStr .= ".add(1, 'month').endOf('month')";
		break;
		case 14: // Custom Range
			//$startDateStr .= ".subtract(1, 'days')";
		break;
		
	}
	return array($startDateStr, $endDateStr);
}

/**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string $email The email address
 * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
 * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
 * @param boole $img True to return a complete IMG tag False for just the URL
 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
 * @return String containing either just a URL or a complete image tag
 * @source https://gravatar.com/site/implement/images/php/
 */
function get_gravatar( $email, $s = 80, $d = 'retro', $r = 'g', $img = false, $atts = array() ) {
    $url = 'https://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";
    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}


function getCurrentTime(){
	return date('Y-m-d\\TH:i:s\\Z', time()+18060);		// need to add 5 hours
}

?>
