<?php
//$gateway = new Braintree_Gateway(['environment' => 'sandbox', 'merchantId' => 'z2tjx669rxwbzmgr', 'publicKey' => 'wjscqct5wmnk45d7', 'privateKey' => '654b2628235fc1fbd8e8d16b7fed0791']);
/*$clientToken = $gateway->clientToken()->generate([
    "customerId" => $aCustomerId
]);*/

//echo($clientToken = $gateway->clientToken()->generate());


$currPage = "goPremium";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$currentUser = new User();

if(!$currentUser->isLoggedIn()){
	Redirect::to('login.php?loginfirst=true');
}

$htmlTitle = "Go Premium";
$htmlDescription = "Become a Premium Member of SpeedTypingOnline.com";
$cssFiles = "beh";

$noWidgets = true;
$addContainer = true;
$jQuery = true;
$fontPacks = 'ajkl';
require_once 'includes/overall/header.php';


if(inMaintenanceMode()){
	echo '<h2>Site is currently under Maintenance</h2>';
	echo '<h4>User registration is currently disabled as a major site-wide upgrade is currently underway.</h4>';
	echo '<h4>Please feel free to continue using the site and come back here soon to register your account.</h4>';
	echo '<h4>Sorry for any inconvenience and thank you for your patience!</h4>';
	exitPHPwithFooter();
}

?>

<!-- <h2>Go Premium!</h2> -->
</div> <!-- <div id="heading"> -->


<div id="pageMain">
	<div class="responsiveCols">
		<div class="cardContainer">
			<?php if(!$currentUser->isLoggedIn()){
					// should redirect before getting here
				?>
				<div id="card2" class="card">
					<h3 class="card-title">
						<div><a href="/register.php">Create an account</a></div>
					</h3>
					<div class="package_price">or</div> 
					<h3 class="card-title">
						<div><a href="/login.php">Login to your account first</a></div>
					</h3>
					
				</div>
			
			
			<?php } else if($currentUser->isPremium()){?>
				<div id="card2" class="card">
					<h1 class="card-title">Congratulations!</h1>
					<div>You are already a premium member! Nice job - and Thanks!</div>
					<div style="visibility: hidden;">
						<div id="paypal-button-container"></div>
					</div>
				</div>
			<?php } else if ($currentUser->isPendingPremium()){ ?>
				<div id="card2" class="card">
					<h1 class="card-title">Thanks!</h1>
					<div>Sit tight - your premium account activation is in process!</div>
						
					<div style="visibility: hidden;">
						<div id="paypal-button-container"></div>
					</div>
				</div>
			<?php } else { ?>
				<div id="card2" class="card">
					<h1 class="card-title">Premium Account</h1>
					<ul class="card-features">
						<li><h5>No Ads!</h5></li>
						<li><h5>Unlimited Test History!</h5></li>
						<li><h5>Cancel Anytime</h5></li>
						<!--<li><h5>Longer High Score Duration!</h5></li>-->
						<!-- <li><h5>Support Future Updates</h5></li> -->
					</ul>
					<p id="freetrial" class="ribbon"><span class="text"><strong class="bold">FREE</strong> 7-day Trial</span></p>
					
					<p id="monthprice" class="package_price lead"><!--<span>$</span><span class="price">5</span><span>/ Month</span>--><span>then $<b>5</b>/month billed monthly</span></p>
					<p id="yearprice" class="package_price lead" style="display:none;"><!--<span>$</span><span class="price">30</span><span>/ Year</span>--><span>then $<b>30</b>/year billed annually</span></p>
					<div class="stoUI stoUIradio">
						<form id="LTradio">
							<div>Pay by the</div>
							<label for="PayByMonth" class="radioLeft">
								<input type="radio" id="PayByMonth" name="PayByMonth" <?php echo ($lessonModule <= 83) ? 'checked' : ''; ?> value="Month" onclick="PayMonthClicked()">
								<span><h3>Month</h3></span>
							</label>
							<label for="PayByYear" class="radioRight">
								<input type="radio" id="PayByYear" name="PayByYear" <?php echo ($lessonModule >= 84) ? 'checked' : ''; ?> value="Year" onclick="PayYearClicked()">
								<span><h3>Year</h3></span>
							</label>
						</form>
					</div>
					
					<div id="paypal-button-container"></div>
				</div>
				
			<?php } ?>
		</div>
	</div>
</div>

<style>
	#lineWrapper{position: relative; width: 40px; height: 300px; margin: 0px 10px 10px;}
	.stoUICheckbox{margin-top:25px;}
	form div.alignInput label{width:135px;}
	
	@media only screen and (max-width: 1248px){
		aside{display:none;}
		#heading, #pageMain{margin-right:0px;}
	}
	
	@media only screen and (max-width: 870px){
		.socialLogin{display:block !important; margin-bottom:20px;}
		#lineWrapper{height:60px; display: block; margin: 0 auto 20px auto;}
	}
	
	div.card{    box-shadow: 0px 0px 8px -3px #191919; border-radius: 22px; padding: 10px; background: #e1f7ff; border: 12px solid #c4efff; min-width: 754px;}
	
	input[type="radio"]:checked+label div.card{ background: #ffd78d; border: 12px solid orange; }
	input[type="radio"]:checked+label div.card .card-title{ color:#6d4700; }
	input[type="radio"]:checked+label div.card li{ color:#5a3a06; }
	input[type="radio"]:checked+label div.card .package_price .price{ color: #563901; filter: drop-shadow(4px 3px 1px #c3a261);}
	input[type="radio"]:checked+label div.card .card-features{ border-color: #bf8e32; }
	.cardContainer > li {width:46%;}
	/*div.card:hover{background: #c5f0ff; border: 12px solid #93e3ff; cursor: pointer;}*/
	div.card .card-title, h2{font-family: 'roboto';  font-size: 3em; font-weight: 700; text-transform: uppercase; color:#00465d;}
	div.card .card-title{font-size: 2em; font-weight: 700; text-transform: uppercase; color:#00465d; margin:10px 0 -4px 0;}
	div.card .card-features{border-bottom: 1px solid #a2d3e2; width: 62%; padding: 16px 0px 12px 0px; margin-bottom:34px;}
	div.card .card-features > li {text-indent: -5px; white-space: nowrap;}
	div.card .card-features > li:before {content: "-"; font-weight: 300; font-size: 1em; font-family: 'roboto';}
	div.card .card-features > li h5 {display:inline-block; text-indent:10px;}
	div.card li{width:100%; display:inline-block; text-align:center; font-size:1.7em;line-height:1.1em; font-family: 'varela round', 'comfortaa'; color:#003d50;}
	div.card .package_price{ padding:0px 0px 5px 0px; /*display:inline-block;*/ font-size:1.3em; color: #2d495f;}
	div.card .price{/*font-size:4em;*/ font-size:3em; font-weight:600; line-height:.9em; margin:0 20px;}
	div.card .priceSecondary{/*font-size:4em;*/ font-size:2.4em; font-weight:400; line-height:.9em;}
	div.card .package_price > span:first-child{vertical-align:top; }
	div.card .package_price .price{font-family:'roboto'; font-weight:700; color:#00374a; filter: drop-shadow(4px 3px 1px #a7c3cc);}
	div.card .crossout{text-decoration:line-through; color:#aaa;}
	ul.card-features {
		list-style: none;
	}
	.card-features li {
		display: inline-block;
		margin-right: 15px;
	}
	ul.cardContainer input {
		display:none;
	}
	label {
		cursor: pointer;
	}
	input:checked + label {
		background: red;
	}
	
	.stoUIradio label{width:170px; margin:-24px 0px 34px 0px;}
	
	.stoUIradio label span {background:transparent; border:none; color:#00374a; text-align:center; text-size:2em;}
	.stoUIradio input:checked + span {background:transparent; border:none; color:#00374a;}
	
	.stoUIradio label span h3 {/*background:#c4efff;*/ border:none; color:#00374a; display:inline-block; border-radius:15px; padding:5px 21px; font-size:1.6em;}
	.stoUIradio input:checked + span h3 {background:#74c6e4; border:none; color:#00374a;}
	
	input[type="radio"]:checked+label .stoUIradio label span h3 {background:transparent; border:none; color:#4A2C02;}
	input[type="radio"]:checked+label .stoUIradio input:checked + span h3 {background:#754c00; border:none; color:white;}
	
	.stoUIradio input:hover + span{background:transparent; border:none;}
	.stoUIradio input:hover + span h3{background:#045571; color: white;}
	
	input[type="radio"]:checked+label .stoUIradio input:hover + span h3 {background:#f59f00; border:none; color: #5f3d00;}
	input[type="radio"]:checked+label #LTradio div {color: #563901;}
	
	
	/* Paypal style */ 
	.button { cursor: pointer; font-weight: 500; left: 3px; line-height: inherit; position: relative; text-decoration: none; text-align: center; border-style: solid; border-width: 1px; border-radius: 3px; -webkit-appearance: none; -moz-appearance: none; display: inline-block; }
	.button--small { padding: 10px 20px; font-size: 0.875rem; }
	.button--green { outline: none; background-color: #64d18a; border-color: #64d18a; color: white; transition: all 200ms ease; }
	.button--green:hover { background-color: #8bdda8; color: white; }
	
	
	/* Free trial ribbon */
	.ribbon{
	  font-size:20px;
	  position:relative;
	  display:inline-block;
	  margin:0 1.3em 1.3em 1.3em;
	  text-align:center;
	  z-index:0;
	  word-spacing:7px;
	}
	.text{
	  display:inline-block;
	  padding:0.6em 1.1em 0.6em 1.1em;
	  min-width:12.4em;
	  line-height:0.7em;
	  background: #FFD72A;
	  position:relative;
	  font-size:2.2em;
	  font-weight:500;
	  color: #2d495f;
	  font-family:'Montserrat';
	}
	.ribbon:after,.ribbon:before,
	.text:before,.text:after,
	.text .bold:before{
	  content:'';
	  position:absolute;
	  border-style:solid;
	}
	.ribbon:before{
	  top:0.3em; left:0.2em;
	  width:100%; height:100%;
	  border:none;
	  background:#c5e2ec;
	  z-index:-2;
	}
	.text:before{
	  bottom:100%; left:0;
	  border-width: .5em .7em 0 0;
	  border-color: transparent #FC9544 transparent transparent;
	}
	.text:after{
	  top:100%; right:0;
	  border-width: .5em 2em 0 0;
	  border-color: #FC9544 transparent transparent transparent;
	}
	.ribbon:after, .text .bold:before{
	  top:0.95em;right:-1.4em;
	  border-width: 1.1em 1em 1.1em 3em;
	  border-color: #FECC30 transparent #FECC30 #FECC30;
	  z-index:-1;
	  font-size:1.7em;
	}
	.text .bold{font-family:'Montserrat'; font-weight:700; color:#00374a;}
	.text .bold:before{
	  border-color: #c5e2ec transparent #c5e2ec #c5e2ec;
	  top:1em;
	  right:-1.6em;
	  font-size:0.8em;
	}
	
	#paypal-button-container{max-width:580px;}
	
</style>
  

<script src="https://www.paypal.com/sdk/js?client-id=AZIhM2sBODN4Gek5hETFxyqW1cfdN6Ns5bAfbikCbVVR8hC2C74nyBu39SMyvLzxuh2-WQbnhzKEIdss&vault=true&intent=subscription" data-sdk-integration-source="button-factory"></script>
<script>
	var plan_id = 'P-9A038109HH3870947MB4IQVY';
  paypal.Buttons({
      style: {
          shape: 'pill',
          color: 'gold',
          layout: 'vertical'/*,
          label: 'subscribe'*/
      },
      createSubscription: function(data, actions) {
			
        return actions.subscription.create({
          'plan_id': plan_id,
		  /*"application_context": { "brand_name": "Speed Typing Online", "locale": "en-US", "shipping_preference": "NO_SHIPPING"},*/
		  "application_context": {
			"brand_name": "Speed Typing Online", "locale": "en-US", "shipping_preference": "NO_SHIPPING",
			/*"user_action": "SUBSCRIBE_NOW",
			"payment_method": {
			  "payer_selected": "PAYPAL",
			  "payee_preferred": "IMMEDIATE_PAYMENT_REQUIRED"
			},
			"return_url": "https://example.com/returnUrl",
			"cancel_url": "https://example.com/cancelUrl"*/
		  },
		  'custom_id': <?php echo '"' . $currentUser->data()->id .  '"'?>
        });
      },
      onApprove: function(data, actions) {
        /*alert(data);
        alert(actions);
        alert(data.subscriptionID);*/
		////$.post('setData.php', { subid: data.subscriptionID/*type: "tt", keyboard: kl*/},
		
		$.post('setData.php', { type: 'spp'},
			function(output){
				location.reload();
			});
      }
  }).render('#paypal-button-container');
</script>


<script type="text/javascript">  

	function PayMonthClicked(){
		plan_id = 'P-9A038109HH3870947MB4IQVY'; // monthly
		$('#radio1').prop("checked", false);
		$('#radio2').prop("checked", true);
		$('#monthprice').show();
		$('#yearprice').hide();
		$('#PayByYear').prop("checked", false);
	}
	
	function PayYearClicked(){
		plan_id = 'P-44942726EK110974BMCEER2A';	//yearly plan
		$('#radio1').prop("checked", false);
		$('#radio2').prop("checked", true);
		$('#monthprice').hide();
		$('#yearprice').show();
		$('#PayByMonth').prop("checked", false);
	}

</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<?php
require_once 'includes/overall/footer.php';
?>