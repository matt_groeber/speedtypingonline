function HTMLelemStr(theText, theElem='div', theId='', theClass='', theTitle='', theStyle='', theOnclick=''){
	idHTML = (theId == '') ? '' : ' id="' + theId + '"';
	classHTML = (theClass == '') ? '' : ' class="' + theClass + '"';
	titleHTML = (theTitle == '') ? '' : ' title="' + theTitle + '"';
	styleHTML = (theStyle == '') ? '' : ' style="' + theStyle + '"';
	onclickHTML = (theOnclick == '') ? '' : ' onclick="' + theOnclick + '"';
	
	return '<' + theElem + idHTML + classHTML + titleHTML + styleHTML + onclickHTML + '>' + theText + '</' + theElem + '>';
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function UCFirst(input){
	if(input.length > 1)
		return input.slice(0,1).toUpperCase() + input.slice(1, input.length);
	else if(input.length == 1)
		return input.toUpperCase();
	else
		return false;
}

function CopyElemHTMLtoClipboard(elemID){
  /* Get the text field */
  var copyText = document.getElementById(elemID);

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied the text: \"" + copyText.value + '"');
}

function formatAMPM(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

function readableTimeDifference(earlier, later){
	var seconds = Math.floor((later - earlier)/1000);
	
	var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hrs";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " mins";
    }
    return Math.floor(seconds) + " secs";
}

function doReadableTimeDifference(currDateTime, className, printLiteralToo, addToolTip){
	var literalDateStr = '';
	var finalHtml = '';
	$('.' + className).each(function(){
		earlyDate = new Date($(this).html().replace(/-/g,'/'));
		timeDiffString = readableTimeDifference(earlyDate, currDateTime);
		if(printLiteralToo == true || addToolTip == true){
			literalDateStr = formatAMPM(earlyDate) + ", " + earlyDate.toDateString();
		}
		if(addToolTip){
			finalHtml = '<span title="' + literalDateStr + '">' + timeDiffString + " ago</span>"
		} else {
			finalHtml = timeDiffString + " ago" + " (" + literalDateStr + ")";
		}
		
		$(this).html(finalHtml);
		if(timeDiffString.indexOf("mins") > 0 || timeDiffString.indexOf("secs") > 0)
			$(this).addClass("borderRad_4 dkBlueBkgd boldFont centerTxt");
	});
}

function msecsToTimeStr(milliSecs) {
	var timeStr = '';
    var Seconds = Math.floor(milliSecs / 1000);
    milliSecs -= Seconds * 1000;
	milliSecs = Math.floor(milliSecs);
	var Minutes = Math.floor(Seconds / 60);
    Seconds -= Minutes * 60;
	Seconds = Math.floor(Seconds);
	
	if(Minutes != 0){
		timeStr = LeadingZero(Minutes) + ":" + LeadingZero(Seconds) + "." + DoubleLeadZero(milliSecs);
	} else {
		timeStr = Seconds + "." + DoubleLeadZero(milliSecs);
	}
    return(timeStr);
}

function LeadingZero(Time) {
    return (Time < 10) ? "0" + Time : + Time;
}

function DoubleLeadZero(Time) {
	if(Time < 10) {return "00" + Time;}
	else if(Time < 100) {return "0" + Time;}
	else {return Time;}
}

function makeSecondsReadable(seconds){
	if(seconds == 0){
		return 0;
	}
	
	var hrs = 0;
	var hrStr = "";
	if(seconds > 3600){
		hrs = Math.floor(seconds / 3600);
		hrStr = hrs + '<span class="sub">h</span> ';
	} 
	var secs = seconds - (hrs * 3600);
	var mins = 0;
	var minStr = "";
	if(secs > 60){
		mins = Math.floor(secs/60);
		minStr = mins + '<span class="sub">m</span> ';
	} 
	secs = secs - (mins * 60);
	if(secs < 10 && mins > 0){
		secs = "0" + secs;
	}
	
	return hrStr + minStr + secs + '<span class="sub">s</span>';
}

function GetTLdateFilterEnum(inputStr){
	var index = 4;
	switch(inputStr){
		case "Today":
			index = 0;
		break;
		case "Yesterday":
			index = 1;
		break;
		case "Last 7 Days":
			index = 2;
		break;
		case "Last 30 Days":
			index = 3;
		break;
		case "This Week":
			index = 4;
		break;
		case "Last Week":
			index = 5;
		break;
		case "This Month":
			index = 6;
		break;
		case "Last Month":
			index = 7;
		break;
		case "All Time":
			index = 8;
		break;
		case "Tomorrow":
			index = 9;
		break;
		case "Next 7 Days":
			index = 10;
		break;
		case "Next 30 Days":
			index = 11;
		break;
		case "Next Week":
			index = 12;
		break;
		case "Next Month":
			index = 13;
		break;
		case "Custom Range":
			index = 14;
		break;
	}
	
	return index;
}

function GetPickerLabelFromDates(theStartDay, theEndDay){
	tomorrow = moment().add(1, 'days').format('YYYY-MM-DD');
	if(theStartDay == tomorrow && theEndDay == tomorrow){
		return 'Tomorrow';
	}
	
	today = moment().format('YYYY-MM-DD');
	if(theStartDay == today && theEndDay == today){
		return 'Today';
	}
	
	yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
	if(theStartDay == yesterday && theEndDay == yesterday){
		return 'Yesterday';
	}
	
	sevenDaysFromNow = moment().add(6, 'days').format('YYYY-MM-DD');
	if(theStartDay == today && theEndDay == sevenDaysFromNow){
		return 'Next 7 days';
	}
	
	sevenDaysAgo = moment().subtract(6, 'days').format('YYYY-MM-DD');
	if(theStartDay == sevenDaysAgo && theEndDay == today){
		return 'Last 7 days';
	}
	
	thirtyDaysFromNow = moment().add(29, 'days').format('YYYY-MM-DD');
	if(theStartDay == today && theEndDay == thirtyDaysFromNow){
		return 'Next 30 days';
	}
	
	thirtyDaysAgo = moment().subtract(29, 'days').format('YYYY-MM-DD');
	if(theStartDay == thirtyDaysAgo && theEndDay == today){
		return 'Last 30 days';
	}
	
	nextWeekStart = moment().add(1, 'week').startOf('week').format('YYYY-MM-DD');
	nextWeekEnd = moment().add(1, 'week').endOf('week').format('YYYY-MM-DD');
	if(theStartDay == nextWeekStart && theEndDay == nextWeekEnd){
		return 'Next Week';
	}
	
	thisWeekStart = moment().startOf('week').format('YYYY-MM-DD');
	thisWeekEnd = moment().endOf('week').format('YYYY-MM-DD');
	if(theStartDay == thisWeekStart && theEndDay == thisWeekEnd){
		return 'This Week';
	}
	
	lastWeekStart = moment().subtract(1, 'week').startOf('week').format('YYYY-MM-DD');
	lastWeekEnd = moment().subtract(1, 'week').endOf('week').format('YYYY-MM-DD');
	if(theStartDay == lastWeekStart && theEndDay == lastWeekEnd){
		return 'Last Week';
	}
	
	nextMonthStart = moment().add(1, 'month').startOf('month').format('YYYY-MM-DD');
	nextMonthEnd = moment().add(1, 'month').endOf('month').format('YYYY-MM-DD');
	if(theStartDay == nextMonthStart && theEndDay == nextMonthEnd){
		return 'Next Month';
	}
	
	thisMonthStart = moment().startOf('month').format('YYYY-MM-DD');
	thisMonthEnd = moment().endOf('month').format('YYYY-MM-DD');
	if(theStartDay == thisMonthStart && theEndDay == thisMonthEnd){
		return 'This Month';
	}
	
	lastMonthStart = moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD');
	lastMonthEnd = moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD');
	if(theStartDay == lastMonthStart && theEndDay == lastMonthEnd){
		return 'Last Month';
	}
	
	allTimeStart = moment('2020-01-01').format('YYYY-MM-DD');
	allTimeEnd = moment().add(10, 'year').format('YYYY-MM-DD');
	if(theStartDay == allTimeStart && theEndDay == allTimeEnd){
		return 'All Time';
	}
	
	return 'Custom Range';
}

function getQueryParams(qs) {
    qs = qs.split("+").join(" ");
    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}

function cleanData(strType, delButtonText){
	
    $( "#dialog-confirm" ).dialog({
      resizable: false,
      height:280,
	  width:590,
      modal: true,
	  draggable: false,
      buttons: [
		{
			text: delButtonText,
			click: function() {
				$( this ).dialog( "close" );
				$.post('/cleanData.php', { type: strType },
					function(output){
						window.location.reload();
				});
			}			
        }, {
			text: "Cancel",
			click: function() {
				  $( this ).dialog( "close" );
				}
		}
      ],
	  open: function() {
		  $(this).parent().find(":contains('Cancel')").focus();
		  $(this).parent().find(":contains('Delete')").blur();
	  }
    });
}

function ResetClassCode(classID, operativeID=null){
	$.post('/setData.php', { type: "ol_rcc", cid: classID},
		function(output){
			if(operativeID != null){
				operativeID = '#' + operativeID;
				$(operativeID + ' .ccToDisplay').html(output);
				$(operativeID + ' .ccToCopy').val(output);
				$(operativeID + ' .ccResetBtn').css("visibility", "hidden");
			}
			
	});
}

/**
 * http://www.openjs.com/scripts/events/keyboard_shortcuts/
 * Version : 2.01.B
 * By Binny V A
 * License : BSD
 */
shortcut = {
	'all_shortcuts':{},//All the shortcuts are stored in this array
	'add': function(shortcut_combination,callback,opt) {
		//Provide a set of default options
		

		var default_options = {
			'type':'keydown',
			'propagate':false,
			'disable_in_input':false,
			'target':document,
			'keycode':false
		}
		if(!opt) opt = default_options;
		else {
			for(var dfo in default_options) {
				if(typeof opt[dfo] == 'undefined') opt[dfo] = default_options[dfo];
			}
		}

		var ele = opt.target;
		if(typeof opt.target == 'string') ele = document.getElementById(opt.target);
		var ths = this;
		shortcut_combination = shortcut_combination.toLowerCase();

		//The function to be called at keypress
		var func = function(e) {
		
			e = e || window.event;
			
			if(opt['disable_in_input']) { //Don't enable shortcut keys in Input, Textarea fields
				var element;
				if(e.target) element=e.target;
				else if(e.srcElement) element=e.srcElement;
				if(element.nodeType==3) element=element.parentNode;

				if(element.tagName == 'INPUT' || element.tagName == 'TEXTAREA') return;
			}
	
			//Find Which key is pressed
			if (e.keyCode) code = e.keyCode;
			else if (e.which) code = e.which;
			var character = String.fromCharCode(code).toLowerCase();
			
			if(code == 188) character=","; //If the user presses , when the type is onkeydown
			if(code == 190) character="."; //If the user presses , when the type is onkeydown

			var keys = shortcut_combination.split("+");
			//Key Pressed - counts the number of valid keypresses - if it is same as the number of keys, the shortcut function is invoked
			var kp = 0;
			
			//Work around for stupid Shift key bug created by using lowercase - as a result the shift+num combination was broken
			var shift_nums = {
				"`":"~",
				"1":"!",
				"2":"@",
				"3":"#",
				"4":"$",
				"5":"%",
				"6":"^",
				"7":"&",
				"8":"*",
				"9":"(",
				"0":")",
				"-":"_",
				"=":"+",
				";":":",
				"'":"\"",
				",":"<",
				".":">",
				"/":"?",
				"\\":"|"
			}
			//Special Keys - and their codes
			var special_keys = {
				'esc':27,
				'escape':27,
				'tab':9,
				'space':32,
				'return':13,
				'enter':13,
				'backspace':8,
	
				'scrolllock':145,
				'scroll_lock':145,
				'scroll':145,
				'capslock':20,
				'caps_lock':20,
				'caps':20,
				'numlock':144,
				'num_lock':144,
				'num':144,
				
				'pause':19,
				'break':19,
				
				'insert':45,
				'home':36,
				'delete':46,
				'end':35,
				
				'pageup':33,
				'page_up':33,
				'pu':33,
	
				'pagedown':34,
				'page_down':34,
				'pd':34,
	
				'left':37,
				'up':38,
				'right':39,
				'down':40,
	
				'f1':112,
				'f2':113,
				'f3':114,
				'f4':115,
				'f5':116,
				'f6':117,
				'f7':118,
				'f8':119,
				'f9':120,
				'f10':121,
				'f11':122,
				'f12':123
			}
	
			var modifiers = { 
				shift: { wanted:false, pressed:false},
				ctrl : { wanted:false, pressed:false},
				alt  : { wanted:false, pressed:false},
				meta : { wanted:false, pressed:false}	//Meta is Mac specific
			};
                        
			if(e.ctrlKey)	modifiers.ctrl.pressed = true;
			if(e.shiftKey)	modifiers.shift.pressed = true;
			if(e.altKey)	modifiers.alt.pressed = true;
			if(e.metaKey)   modifiers.meta.pressed = true;
                        
			for(var i=0; k=keys[i],i<keys.length; i++) {
				//Modifiers
				if(k == 'ctrl' || k == 'control') {
					kp++;
					modifiers.ctrl.wanted = true;

				} else if(k == 'shift') {
					kp++;
					modifiers.shift.wanted = true;

				} else if(k == 'alt') {
					kp++;
					modifiers.alt.wanted = true;
				} else if(k == 'meta') {
					kp++;
					modifiers.meta.wanted = true;
				} else if(k.length > 1) { //If it is a special key
					if(special_keys[k] == code) kp++;
					
				} else if(opt['keycode']) {
					if(opt['keycode'] == code) kp++;

				} else { //The special keys did not match
					if(character == k) kp++;
					else {
						if(shift_nums[character] && e.shiftKey) { //Stupid Shift key bug created by using lowercase
							character = shift_nums[character]; 
							if(character == k) kp++;
						}
					}
				}
			}
			
			if(kp == keys.length && 
						modifiers.ctrl.pressed == modifiers.ctrl.wanted &&
						modifiers.shift.pressed == modifiers.shift.wanted &&
						modifiers.alt.pressed == modifiers.alt.wanted &&
						modifiers.meta.pressed == modifiers.meta.wanted) {
				callback(e);
	
				if(!opt['propagate']) { //Stop the event
					//e.cancelBubble is supported by IE - this will kill the bubbling process.
					e.cancelBubble = true;
					e.returnValue = false;
	
					//e.stopPropagation works in Firefox.
					if (e.stopPropagation) {
						e.stopPropagation();
						e.preventDefault();
					}
					
					return false;
				}
			}
			
		}
		this.all_shortcuts[shortcut_combination] = {
			'callback':func, 
			'target':ele, 
			'event': opt['type']
		};
		//Attach the function with the event
		if(ele.addEventListener) ele.addEventListener(opt['type'], func, false);
		else if(ele.attachEvent) ele.attachEvent('on'+opt['type'], func);
		else ele['on'+opt['type']] = func;
	},

	//Remove the shortcut - just specify the shortcut and I will remove the binding
	'remove':function(shortcut_combination) {
		shortcut_combination = shortcut_combination.toLowerCase();
		var binding = this.all_shortcuts[shortcut_combination];
		delete(this.all_shortcuts[shortcut_combination])
		if(!binding) return;
		var type = binding['event'];
		var ele = binding['target'];
		var callback = binding['callback'];

		if(ele.detachEvent) ele.detachEvent('on'+type, callback);
		else if(ele.removeEventListener) ele.removeEventListener(type, callback, false);
		else ele['on'+type] = false;
	}
}

