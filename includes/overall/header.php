<?php ob_start(); 
$htmlTitle = isset($htmlTitle) ? $htmlTitle . " - SpeedTypingOnline" : "SpeedTypingOnline";
$htmlDescription = isset($htmlDescription) ? $htmlDescription : "";
$pageURL = isset($pageURL) ? $pageURL : fullSiteURL();?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<title><?php echo escape($htmlTitle);?></title>
<meta name="description" content="<?php echo escape($htmlDescription);?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes, minimal-ui" />
<meta name="mobile-web-app-capable" content="yes" />
<meta property="og:url"           content="<?php echo escape($pageURL);?>" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="<?php echo escape($htmlTitle);?>" />
<meta property="og:description"   content="<?php echo escape($htmlDescription);?>" />
<meta property="og:image"         content="/images/STO_logoPNG.png" />
<meta property="fb:page_id" content="268106593233443" />
<meta property="fb:app_id" content="210810555621824>" id="fbapp_id" />
<?php echo (isset($metaNoIndex) && $metaNoIndex) ? '<meta name="robots" content="noindex"></meta>' : ''; ?>
<link rel="shortcut icon" href="/STO_favicon.ico" />
<link rel="image_src" type="image/png" href="/STO_logoPNG.png" />
<?php

	// css file selection
	$str = $cssFiles;
	$strlen = strlen( $str );
	for( $ii = 0; $ii < $strlen; $ii++ ) {
		$char = $str[$ii];
		if($char == 'a'){
			echo '<link rel="stylesheet" href="/css/_a_main2.css?v=57" type="text/css">';
		} else if($char == 'b'){
			echo '<link rel="stylesheet" href="/css/_b_menuGeneral.css?v=64" type="text/css">';
		} else if($char == 'c'){
			echo '<link rel="stylesheet" href="/css/_c_menu.css?v=2" type="text/css">';
		} else if($char == 'd'){
			echo '<link rel="stylesheet" href="/css/_d_jquery-ui-1.8.16.custom.css?v=2" type="text/css">';
		} else if($char == 'e'){
			echo '<link rel="stylesheet" href="/css/_e_footer.css?v=13" type="text/css">';
		} else if($char == 'f'){
			echo '<link rel="stylesheet" href="/css/_f_ltt.css?v=25" type="text/css">';
		} else if($char == 'g'){
			echo '<link rel="stylesheet" href="/css/_g_typingLessons.css?v=56" type="text/css">';
		} else if($char == 'h'){
			echo '<link rel="stylesheet" href="/css/_h_STO.css?v=37" type="text/css">';
		} else if($char == 'i'){
			echo '<link rel="stylesheet" href="/css/_i_STOjUI.css?v=16" type="text/css">';
		} else if($char == 'j'){
			echo '<link rel="stylesheet" href="/css/_j_testStats.css?v=2" type="text/css">';
		} else if($char == 'k'){
			echo '<link rel="stylesheet" href="/css/_k_update.css?v=22" type="text/css">';
		} else if($char == 'm'){
			echo '<link rel="stylesheet" href="/css/_m_tCal.css?v=5" type="text/css">';
		} else if($char == 'n'){
			echo '<link rel="stylesheet" href="/css/_n_about.css?v=2" type="text/css">';
		} else if($char == 'o'){
			echo '<link rel="stylesheet" href="/css/_o_home.css?v=2" type="text/css">';
		} else if($char == 'p'){
			echo '<link rel="stylesheet" href="/css/_p_home_mobile.css?v=2" type="text/css" media="screen">';
		} else if($char == 'q'){
			echo '<link rel="stylesheet" href="/css/_q_typingGames.css?v=2" type="text/css">';
		} else if($char == 'r'){
			echo '<link rel="stylesheet" href="/css/_r_wbk.css?v=7" type="text/css">';
		} else if($char == 's'){
			echo '<link rel="stylesheet" href="/css/_s_tta.css?v=10" type="text/css">';
		} else if($char == 't'){
			echo '<link rel="stylesheet" href="/css/_t_myResults.css?v=2" type="text/css">';
		} else if($char == 'u'){
			echo '<link rel="stylesheet" href="/css/_u_trg.css?v=3" type="text/css">';
		} else if($char == 'v'){
			echo '<link rel="stylesheet" href="/css/_v_assignments.css?v=2" type="text/css">';
		} else if($char == 'w'){
			echo '<link rel="stylesheet" href="/css/_w_tablesorter.css?v=9" type="text/css">';
		} else {
			echo '<link rel="stylesheet" href="/css/_' . $char . '_NoStylesheetFound.css" type="text/css">';
		}
	}
	
	// css file selection
	if(isset($fontPacks)){
		$str = $fontPacks;
		$strlen = strlen( $str );
		if($strlen > 0){
			$fontLink = '<link href="https://fonts.googleapis.com/css2?';
			for( $ii = 0; $ii < $strlen; $ii++ ) {
				$fontLink .= 'family=';
				$char = $str[$ii];
				if($char == 'a'){
					$fontLink .= 'Comfortaa';
				} else if($char == 'b'){
					$fontLink .= 'Concert+One';
				} else if($char == 'c'){
					$fontLink .= 'Kavoon';
				} else if($char == 'd'){
					$fontLink .= 'Suez+One';
				} else if($char == 'e'){
					$fontLink .= 'Concert+One';
				} else if($char == 'f'){
					$fontLink .= 'Lobster';
				} else if($char == 'g'){
					$fontLink .= 'Lobster+Two';
				} else if($char == 'h'){
					$fontLink .= 'Righteous';
				} else if($char == 'i'){
					$fontLink .= 'Russo+One';
				} else if($char == 'j'){
					$fontLink .= 'Montserrat:wght@400;700';
				} else if($char == 'k'){
					$fontLink .= 'Overpass';
				} else if($char == 'l'){
					$fontLink .= 'Varela+Round';
				} else if($char == 'r'){
					$fontLink .= 'Roboto:wght@400;700';
				} else {
					$fontLink .= 'unknown:' . $char;
				}
				$fontLink .= '&';
			}
			
			$fontLink .= 'display=swap" rel="stylesheet">';
			echo $fontLink;
		}
	}
	
	if(isset($fontAwesomeCDN) && $fontAwesomeCDN){
		//echo '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/fontawesome.min.css">';
		echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">';
		//echo '<script src="https://kit.fontawesome.com/fb1639f816.js" crossorigin="anonymous"></script>';
	}
	
	if(isset($jQuery) && $jQuery){?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tools/1.2.7/jquery.tools.min.js" type="text/javascript"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="/js/jquery.tools.min.js?v=3" type="text/javascript"><\x3C/script>');
			}
		</script>
<?php	
}
	if(isset($jQueryUI) && $jQueryUI){?>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="/js/jquery.js" type="text/javascript"><\/script>');
				document.write('<script src="/js/jquery-ui-1.11.4.js" type="text/javascript"><\/script>');
			}
		</script>
<?php
	}
?>
<!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body class="Frame">
	<div id="cookieConsentPopup">
		<a id="cookieButton">OK</a>
		<div>We use cookies to personalize content and ads, offer social media features and analyze users' behavior on this site. <a href="/cookie-policy">Learn More</a></div>
	</div>
<?php 
	if($currPage === ""){
		$currPage = "typingGames";
	} 
	include $_SERVER['DOCUMENT_ROOT'] . '/includes/topMenu.php'; 
	
	if(isset($addContainer) && $addContainer){
		if(!isset($noWidgets)){$noWidgets = true;}
?>
	<div id="container" <?php if($noWidgets){echo 'class="noWidgets"';}?>>
		<?php if(!$noWidgets){include $_SERVER['DOCUMENT_ROOT'] . '/includes/aside.php';}?>
		<div id="heading">

<?php 
	}
	
ob_end_flush();
?>

