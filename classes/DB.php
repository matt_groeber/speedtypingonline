<?php
class DB{
	private static $_instance = null;
	private $_pdo, 
			$_query, 
			$_error = false, 
			$_results, 
			$_lastInsertId = -1,
			$_defaultLimit = 30,
			$_count = 0,
			$_maintMode = false;
			
	private function __construct() {
		try {
			$this->_pdo = new PDO('mysql:host=' . Config::get('mysql/host') . ';dbname=' . Config::get('mysql/db'), Config::get('mysql/username'), Config::get('mysql/password'), array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		} catch(PDOException $e){
			die($e->getMessage());
		}
	}
	
	public static function getInstance(){
	
		if(!isset(self::$_instance)) {
			self::$_instance = new DB();
		}
		return self::$_instance;
	}
	
	public function query($sql, $params = array()){
		$this->_error = false;
		if($this->_query = $this->_pdo->prepare($sql)){
			$x = 1;
			if(count($params)){
				foreach($params as $param){
					$this->_query->bindValue($x, $param);
					$x++;
				}
			}
			
			if($this->_query->execute()){
				$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
				$this->_count = $this->_query->rowCount();
				$this->_lastInsertId = $this->_pdo->lastInsertId();
			} else {
				$this->_error = true;
			}
		}
		return $this;
	}
	
	// Optional '$cols' argument provides columns to SELECT by (default will be '*')
	// Optional '$orderBy' argument provides ability to order results by a column
	//				if not array will default to DESC, if array put 'ASC' in second parameter
	//              also if $orderBy[1] is array will sort by list in $orderBy[1] by field name $orderBy[0]
	// PARAMETERS:
	//   $joinArr  (6th parameter) = array(1,2,3) where [1 => table to join], [2 => first table field], [3 => second table field], 
	//									[4 => (optional, default = "INNER") type of join], 
	//									[5 => (optional array) [1] -> col, [2] -> operator, [3] -> value]
	//									*** $joinArr can also be array(array(1), array(2), array(3)...) to join multiple tables
	//   $limitArr (7th paremter)) = array(1,2) where [1 => limit], [2 => offset]
	//   $groupBy (8th paremter)) = (optional)
	public function action($action, $table, $where = array(), $cols = null, $orderBy = null, $joinArr = null, $limitArr = null, $groupBy = null) {
		$wherePerm = $where;
		// first see if site is in maintenance mode (cutoff all database traffic)
		if(config::get('mysql/maint_mode')){
			$this->_query = 'Maintenance Mode Enabled';
			$this->_maintMode = true;
			return $this;
		}
		
		if($cols === null){
			if($action != 'DELETE')
				$cols = '*';
		}
		
		if(count($where) > 0 /*&& (count($where) % 3) == 0*/){
			
			$sql = "{$action} {$cols} FROM {$table}";
			$conjunction = ' ';
			$retValues = array();
			
			// optional 6th parameter "$joinArr" (each element can be array to join more tables)
			if(is_array($joinArr)){
				if(is_array($joinArr[0])){
					$e0 = $joinArr[0];
					$e1 = $joinArr[1];
					$e2 = $joinArr[2];
					if(isset($joinArr[3])){
						$e3 = $joinArr[3];
					}
					if(isset($joinArr[4])){
						$e4 = $joinArr[4];
					}
					for($ii=0; $ii < count($joinArr[0]); $ii++){
						$subJoinArr = array($e0[$ii], $e1[$ii], $e2[$ii]);
						if(isset($e3[$ii])){
							$subJoinArr = array_merge($subJoinArr, array($e3[$ii]));
						}
						if(isset($e4[$ii])){
							$subJoinArr = array_merge($subJoinArr, array($e4[$ii]));
						}
						$sql = $this->generateJoinPhrase($table, $subJoinArr, $sql, $retValues);
					}
				} else {
					$sql = $this->generateJoinPhrase($table, $joinArr, $sql, $retValues);
				}
			}
			
			$sql .= " WHERE ";
			$atStart = true;
			
			do{
				$openParenthesis = '';
				if($where[0] == '('){
					$openParenthesis = '( ';
					$where = array_slice($where, 1);
				}
				
				if(!$atStart){
					if($where[0] == 'OR'){
						$conjunction = ' OR ';
					} else {
						$conjunction = ' AND ';
					}
				}
				
				if($where[0] == 'OR' || $where[0] == 'AND'){
					$where = array_slice($where, 1);
				}
				
				$field		= $where[0];
				$operator	= $where[1];
				$value		= ($operator == 'IN' && !is_array($where[2])) ? array($where[2]) : $where[2];
				
				$sql .= "{$conjunction}{$openParenthesis}{$field}";
				
				$sql = $this->addOperatorAction($operator, $value, $sql);
				$where = array_slice($where, 3);
				$retValues = array_merge($retValues, $value);
				
				if(sizeof($where) > 0 && $where[0] == ')'){
					$sql .= ' ) ';
					$where = array_slice($where, 1);
				}
				$atStart = false;
			} while(count($where) > 0);
			
			//optional 8th parameter "$groupBy"
			if($groupBy !== null){
				$sql .= " GROUP BY {$groupBy}";
			}
				
			// optional 5th parameter "$orderBy"
			if($orderBy !== null){
				$orderCol = is_array($orderBy) ? $orderBy[0] : $orderBy;
				if(is_array($orderBy[1])){
					$orderCol = "FIELD(" . $orderCol . ",";
					$order = implode(',', $orderBy[1]);
					$order .= ')';
				} else {
					$order = (is_array($orderBy) && isset($orderBy[1])) ? $orderBy[1] : 'DESC';
				}
			
				$sql .= " ORDER BY {$orderCol} {$order}";						
			}
			
			// optional 7th parameter "$limitArr"
			if(is_array($limitArr)){
				$limitPage = $limitArr[0];
				$limitOffset = (sizeof($limitArr) > 1) ? $limitArr[1] : 0;
				$sql .= " LIMIT {$limitOffset} , {$limitPage}";
			} else if ($action != 'DELETE') {
				$sql .= " LIMIT 0 , {$this->_defaultLimit}";
			}
			
			if(!$this->query($sql, $retValues)->error()){
				/*if($table == "OL_students"){
					error_log('---------------------------------------------');
					error_log($sql);
					error_log(implode("; ", $retValues));
					error_log('---------------------------------------------');
				}*/
				/*if($joinArr !== null && $table != "`users`" && $cols != "typing_tests.id, wpm, accuracy, timestamp, keyboard_used, total_time_secs, title"){*//*
					echo 'in DB.php';
					var_dump($table);
					var_dump($wherePerm);
					var_dump($cols);
					var_dump($sql);
					var_dump($retValues);
					var_dump($this);
				}*/
				return $this;
			} else {						// USEFUL FOR DEBUGGING MySQL QUERIES
				echo 'DB error';
				error_log('DB ERROR -- $sql -----> ');
				error_log($sql);
				error_log('DB ERROR -- $retValues -----> ');
				error_log(implode("; ", $retValues));
				/*var_dump($sql);
				var_dump($retValues);
				exit();*/
			}
		}
		return false;
	}
	
	/*private function generateWhereClause($whereSubArr, $sql){
		$field		= $where[0];
		$operator	= $where[1];
		$value		= ($operator == 'IN' && !is_array($where[2])) ? array($where[2]) : $where[2];
		
		$sql .= "{$conjunction}{$field}";
		
		$sql = $this->addOperatorAction($operator, $value, $sql);
		$where = array_slice($where, 3);
		$retValues = array_merge($retValues, $value);
		$conjunction = ' AND ';
		
		return $sql;
	}*/
	
	private function generateJoinPhrase($table, $joinArr, $sql, &$retValues){

		$tableToJoin = $joinArr[0];
		
		// add table name to specify field to join on unless already provided
		if(strpos($joinArr[1], '.') === false)
			$firstTableField = $table . '.' . $joinArr[1];
		else
			$firstTableField = $joinArr[1];
		
		// add table name to specify field to join on unless already provided
		if(strpos($joinArr[2], '.') === false)
			$secondTableField = $tableToJoin . '.' . $joinArr[2];
		else
			$secondTableField = $joinArr[2];
		
		if(isset($joinArr[3]) && $joinArr != null){
			$joinTypes = array('LEFT', 'RIGHT', 'INNER', 'FULL OUTER');
			if(in_array($joinArr[3], $joinTypes)){
				$joinType = $joinArr[3];
				//var_dump($joinType);
			}
		} else {
			$joinType = 'INNER';
		}
		
		$sql .= " {$joinType} JOIN `{$tableToJoin}` on {$firstTableField} = {$secondTableField}"; 
		//var_dump($sql);
		
		if(isset($joinArr[4]) && is_array($joinArr[4])){
			//echo '<br /><br />JOIN ARRAY IS A GO!!! <br /><br />';
			do{
				$joinCondition = $joinArr[4];
				$jcValue = $joinCondition[2];
				$jcCol = $joinCondition[0];
				$sql .= " AND {$jcCol}";
				$sql = $this->addOperatorAction($joinCondition[1], $joinCondition[2], $sql);
				array_push($retValues, $jcValue);
				//var_dump($joinArr[4]);
				$joinArr[4] = array_slice($joinArr[4], 3);
				//var_dump($joinArr[4]);
			} while (count($joinArr[4]) > 0);
		}
				
		return $sql;
	}
	
	private function addOperatorAction($operator, &$value, $sql){
		
		$operators = array('=', '>', '<', '>=', '<=', '!=', 'IN', 'IS NULL');
		
		if(in_array($operator, $operators)){
			$sql .= " {$operator}";
			
			if($operator != 'IS NULL'){
				if(is_array($value)){
					$sql .= " (";

					for($ii = 1; $ii < sizeof($value); $ii++)
						$sql .="?,";
					
					$sql .= "?)";
				} else {
					$sql .= " ?";
					$value = array($value);
				}
			} else {
				$value = array();
			}
		} else {
			throw new Exception("Invalid operator");
		}
		return $sql;
	}
	
	public function get($table, $where, $cols = null, $orderBy = null, $joinArr = null, $limitArr = null, $groupBy = null) {
		return $this->action('SELECT', $table, $where, $cols, $orderBy, $joinArr, $limitArr, $groupBy);
	}
	
	public function getDistinct($table, $where, $cols = null, $orderBy = null, $joinArr = null, $limitArr = null, $groupBy = null) {
		return $this->action('SELECT DISTINCT', $table, $where, $cols, $orderBy, $joinArr, $limitArr, $groupBy);
	}
		
	public function delete($table, $where) {
		return $this->action('DELETE', $table, $where);
	}
	
	// Single INSERT PARAMETERS:
	//   $fields = array('field_1' => val_1, 'field_2' => val_2, 'field_3' => val_3...) 
	// 
	// Multi INSERT PARAMETERS
	//   $fields = array(  array('field_1', 'field_2', ...)
	//                     array(val_1_of_1, val_1_of_2, ...)
	//                     array(val_2_of_1, val_2_of_2, ...)
	//                     ... )
	public function insert($table, $fields = array()) {
		$numFields = count($fields);
		if($numFields) {
			$sql = "INSERT INTO {$table} ";
			
			if(@!is_array($fields[0])){
				$keys = array_keys($fields);
				$values = null;
				$x = 1;
				
				foreach($fields as $field) {
					$values .= '?';
					if($x < $numFields){
						$values .= ', ';
					}
					$x++;
				}
				
				$sql .= "(`" . implode('`, `', $keys) . "`) VALUES ({$values})";
				
			} else if($numFields > 1) {
				$numRows = sizeof($fields[1]);
				$numParams = $numFields - 1;
				$rowQmarks = '(';
				$qMarks = '';
				$newFields = array();
				
				for($ii = 1; $ii < $numParams; $ii++){
					$rowQmarks .= '?,';
				}
				$rowQmarks .= '?)';
				
				for($ii = 1; $ii < $numRows; $ii++){
					$qMarks .= $rowQmarks . ', ';
				}
				$qMarks .= $rowQmarks;
				
				
				for($ii = 0; $ii < $numRows; $ii++){
					for($jj = 1; $jj <= $numParams; $jj++){
						$newFields[] = $fields[$jj][$ii];
					}
				}
				
				
				$sql .= "(`" . implode('`, `', $fields[0]) . "`) VALUES {$qMarks}";
				$fields = $newFields;
			}
			
			if(!$this->query($sql, $fields)->error()) {
				return true; 
			}/*else {	// USEFUL FOR DEBUGGING MySQL QUERIES
				echo 'was an error';
				var_dump($sql);
				var_dump($fields);
				exit();
			}*/
		}
		return false;
	}
	
	public function update($table, $id, $fields, $id_field = '') {
		$set = '';
		$x = 1;
		
		if($id_field === ''){
			$id_field = 'id';
		}
		
		foreach($fields as $name => $value) {
			$set .= "`{$name}` = ?";
			if($x < count($fields)) {
				$set .= ', ';
			}
			$x++;
		}
		
		if(is_array($id)){
			if(count($id) != count($id_field))
				return false;
			
			$conj = '';
			$whereClause = '';
			for($ii = 0; $ii < count($id); $ii++){
				$whereClause .= "{$conj}{$id_field[$ii]} = '{$id[$ii]}'";
				$conj = ' AND ';
			}
		} else {
			$whereClause = "{$id_field} = '{$id}'";
		}
		
		$sql = "UPDATE {$table} SET {$set} WHERE {$whereClause}";
		/*if($table == 'OL_assignees'){
			var_dump($sql);
			exit();
		}*/
		
		if(!$this->query($sql, $fields)->error()) {
			return true;
		}/*else {	// USEFUL FOR DEBUGGING MySQL QUERIES
				echo 'was an error';
				var_dump($sql);
				var_dump($fields);
				exit();
			}*/
		
		return false;
	}
	
	public function results() {
		return $this->_results;
	}
	
	public function first() {
		return $this->results()[0];
	}
	
	public function error() {
		return $this->_error;
	}
	
	public function inMaintenanceMode() {
		return $this->_maintMode;
	}
	
	public function count() {
		return $this->_count;
	}
	
	public function getLastInsertID(){
		if($this->_lastInsertId > 0){
			return $this->_lastInsertId;
		} else {
			return false;
		}
	}
		
}

?>