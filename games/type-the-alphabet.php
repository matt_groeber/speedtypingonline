<?php 
$currPage = "typeTheAlphabet";
require_once '../core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$htmlTitle = "Type the Alphabet";
$htmlDescription = "How fast can you type alphabet? Learn every letter on the keyboard while trying to beat your fastest time at typing A to Z.";
$cssFiles = "bsehi";
$fontPacks = "de";
$noWidgets = true;
$fontAwesomeCDN = true;
$jQueryUI = true;
require_once '../includes/overall/header.php';

$db = DB::getInstance();

$user_obj = new User();
$loggedInID = ($user_obj->isLoggedIn()) ? $user_obj->data()->id : null;
$hs = new HighScore($loggedInID);
//$hs = new HighScore();
$userData = $user_obj->data();
$debugMode = $user_obj->hasPermission('debug') || Config::get('constants/debugMode');

$slowestTime = 99999999;
?>
<script>
<?php
echo "var alphaTypeEnum = " . json_encode(Config::get('constants/alphaTypeEnum')) . ";";
echo "var alphaTypeDecode = " . json_encode(Config::get('constants/alphaTypeDecode')) . ";";
echo 'var slowestTime = ' . $slowestTime;
?>
</script>

<div id="fbLikeDiv">
	<div class="fb-like" data-href="http://www.speedtypingonline.com/games/type-the-alphabet.php" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true" data-font="arial"></div>
</div>

<div class="grid grid-pad responsiveCols">
   <div id="ttaInterfaceContainer" class="leftCol">
<?php
		echo getAd("lead", $currPage, $debugMode, $user->isPremium());
?>
		<div id="centerEntryModule" class="module">
			<div class="topLeftLink"><a href="#howTo"><i class="fa fa-question-circle" aria-hidden="true"></i></a></div>
			<div id="ttaLogo">abc</div>
			<h1>Type the Alphabet</h1>
			<div id="wrapStats">
				<div id="timerDiv">   
					<div id="timerText">Time</div>
					<div id="sWatch" class="statsText">00:00</div>
				</div>
				<div id='wpmDiv'>                
                    <div id='wpmText'>Speed (WPM)</div>
                    <div id='wpmValue' class="statsText">0</div>
                </div>
				<div id="scoreDiv"> 
					<div id="scoreText">Fastest Time</div>
					<div id="wbkScore" class="statsText">-</div>
					<span id="clearDataBtn" style="display:none;"><input class="stoUI stoUIClickable" type="button" value="Clear Scores" onclick="cleanData('tta', 'Delete all alphabet game scores');"></span>
				</div>
			</div>
			
			<div id="centerContent">
					<div id="divCover" style="z-index: 0;"></div>
					<div id="blockDivContainer" class="mainDivInputs" tabindex="0" onmouseover="this.focus()" onmouseout="/*this.blur()*/" ></div>
				<div id="buttonWrapper">
					<div id="resetBtnWrap">
						<div id="resetBtnDiv">
							<input type="button" name="ResetBtn" value="Reset" id="resetBtn" class="orangeButton" onclick="Reset(0,'sWatch')">
						</div>
					</div>
				</div>
				
			</div>	<!-- end centerContent -->
			
			<div id="textSelectDropDown">
				<div class="boldFont">Mode: </div>
				<select id="textTypeSelected">
					<option value="NORMAL">A-Z (Normal)</option>
					<option value="SPACES">A-Z (with Spaces)</option>
					<option value="BACKWARDS">Z-A (Backwards)</option>
					<option value="BACKWARDS_SPACES">Z-A (with Spaces)</option>
					<option value="RANDOM">Random</option>
				</select>
			</div>
			
			<div id="filterYourHScontainer">
				<div class="boldFont">Filter High Scores: </div>
				<select id="filterYourHS">
					<option <?php if(Input::get('type') == null || Input::get('type') == "ALL"){ echo 'selected="selected"';}?> value="ALL">All</option>
					<option <?php if(Input::get('type') == "NORMAL"){ echo 'selected="selected"';}?> value="NORMAL">A-Z (Normal)</option>
					<option <?php if(Input::get('type') == "SPACES"){ echo 'selected="selected"';}?> value="SPACES">A-Z (Spaces)</option>
					<option <?php if(Input::get('type') == "BACKWARDS"){ echo 'selected="selected"';}?> value="BACKWARDS">Z-A (Backwards)</option>
					<option <?php if(Input::get('type') == "BACKWARDS_SPACES"){ echo 'selected="selected"';}?> value="BACKWARDS_SPACES">Z-A (with Spaces)</option>
					<option <?php if(Input::get('type') == "RANDOM"){ echo 'selected="selected"';}?> value="RANDOM">Random</option>
				</select>
			</div>
		</div>	
		<?php echo getAd("thirdBottom", $currPage, $debugMode, $user->isPremium()); ?>
    </div>
	<div class="rightCol">

<?php
		echo getAd("rightSky", null, $debugMode, $user->isPremium());
?>
	</div>
</div>
<div id="highScoreContainer" class="grid grid-pad">
   <div class="col-2-5 instructions" style="height: auto;">
     <div class="module instructWrap">
		<div class="instructDiv" style="height: auto;">
			<h3>Your Scores</h3>
			<div id="personalAlphaHS">
				<table>
					<tr>
						<th>#</th>
						<th>Seconds</th>
						<th>Type</th>
						<th>Time Ago</th>
					</tr>
				</table>
			</div>
		</div>
	 </div>
	</div>
	<div class="col-3-5 instructions" style="height: auto;">
		<div class="module instructWrap">
			<div class="instructDiv" style="height: auto;">
				<h3>Recent High Score Board</h3>
				<div id="alphaHS">
					<table>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Seconds</th>
							<th>Type</th>
							<th>Time Ago</th>
						</tr>
					</table>
				</div>
			</div>
		</div>
	 </div>
	 
	<div id="dialog-confirm" title="Permanently delete all your alphabet game scores?" style="display: none;">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Your high scores for the alphabet typing game will be permanently deleted and cannot be recovered.</p>
		<p>Are you sure?</p>
	</div>
</div>
<div id="howTo"></div>
<div class="stoFancyList">
			<h2>How To</h2>
			<div class="grid grid-pad">
				<div class="col-1-2">
					<ul class="arrowList">
						<li>
							<h3>Start Game</h3>
							<span>
								<p>Click on the text box with the orange border; the timer starts as soon as you begin typing!</p>
								<p>Click 'Reset' or press <span class="boldFont">'Shift-Return'</span> to restart the game.</p>
							</span>
						</li>
						<li>
							<h3>Change Mode</h3>
							<span>
								<p>Click the <span class="boldFont">'Mode'</span> dropdown menu located directly below the input box.</p>
							</span>
						</li>
					</ul>
				</div>
				<div class="col-1-2">
					<ul class="arrowList">
						<li>
							<h3>Play</h3>
							<span>
								<p>Type the alphabet in order (or according to the mode you select) as fast as you can without any mistakes!</p>
								<p>This game might seem simple but ends up being tons of fun and a great way to learn all the letters on the keyboard and improve your typing skills.</p>
								<p>How fast can you type the alphabet?</p>
							</span>
						</li>
					</ul>
				</div>
			</div>
			
			<div><h2 id="featuresDest">Game Features</h2></div>
			<div class="grid grid-pad">
				<div class="col-1-2">
					<ul class="checkList">
						<li><h3>Restart Game Hotkey</h3>
						<p>Keep your hands on the keyboard - use keyboard shortcut <span class="boldFont">"Shift-Return"</span> to restart game.</p></li>
						<li><h3>Multiple Alphabet Modes</h3>
						<p>Four different modes (normal a-z, alphabet with spaces, backwards, and random) for an extra challenge.</p></li>
					</ul>
				</div>
				<div class="col-1-2">
					<ul class="checkList">
						<li><h3>Live Statistics</h3>
						<p>Shows your typing speed as you play.</p></li>
						<li><h3>High Scores</h3>
						<p>Compete against yourself and other players with a high score board!</p>
						<p>Scores only last for 12 hours to keep the competition fresh and to discourage cheating.</p></li>
					</ul>
				</div>
			</div>
<script src="../js/general.js" type="text/javascript"></script>
<script src="../js/tta.js?v=7" type="text/javascript"></script>
<script type="text/javascript">
 var currDateTime = new Date(<?php echo '"' . date('Y-m-d H:i:s') . '"';?>.replace(/-/g,'/'));

 $(function() {
	currentTextTypeEnum = alphaTypeEnum[$("#textTypeSelected").val()] - 1;
	 
	$('#textTypeSelected').selectmenu({
		width:190,
		select: function(event, data){
			if(!HScoresWereFiltered){
				$('#filterYourHS option[value=' +  data.item.value + ']').prop('selected', true);
				$('#filterYourHS').selectmenu('refresh');
			}
			Reset(0,'sWatch');
		}
	});
	
	$('#filterYourHS').selectmenu({
		width:150,
		change: function(){
			Reset(0,'sWatch');
			HScoresWereFiltered = true;
			/*var qps = getQueryParams(window.location.search);
			var queryStr = '';
			
			if($.isEmptyObject(qps)){
				queryStr = "?type=" + $('#filterYourHS').val();
			} else if(qps.type == null){
				queryStr = window.location.search + "&type=" + $('#filterYourHS').val();
			} else {
				var queryStr = '?';
				$.each(qps, function(key, val){
					if(key == "type"){
						val = $('#filterYourHS').val();
					}
					queryStr += key + '=' + val + '&';
				});
				queryStr = queryStr.substring(0, queryStr.length - 1);
			}
			window.location.href = window.location.origin + window.location.pathname + queryStr;*/
		}
	});

		// ADD HOTKEY
		shortcut.add("Shift+return",function() {
			Reset(0, 'sWatch');
			},{
			"type":"keyup",
			"propagate":false,
			"disable_in_input":false,
			"target":document
			});

        $('#blockDivContainer').on("keydown", function(event){
            if($('#blockDivContainer').get()[0].innerHTML != ""){
                return CreateTimer_block(event, 'sWatch', 0/* Round Time */);
            }
        });
    
        // call reset function every time text type drop down menu changes
        $("#textTypeSelected").change(function () {
            Reset(0,'sWatch');			
        });
		
        Reset(0,'sWatch');
	});
</script>
<?php 
include '../includes/overall/footer.php'; 
?>