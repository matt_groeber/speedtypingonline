<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<?php

    $xWPM = round(htmlspecialchars($_GET["r"]) / 492368);
?>


<html>
    <head>
        <title>I type <?php echo $xWPM;?> words per minute! How fast are you?</title>
        
        <link rel="image_src" type="image/png" href="http://www.speedtypingonline.com/images/STO_TypingInterfaceTilted.png"></link>
        <link rel="shortcut icon" href="http://www.speedtypingonline.com/STO_favicon.ico"></link>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <meta name="description" content="Free online typing test at www.speedTypingOnline.com"></meta>
        <meta name="keywords" content="speed test result"></meta>
		<meta name="robots" content="noindex"></meta>
    </head>
    <body>
        <div style="margin: auto; width: 500px; margin-top: 120px; border: 1px solid black; padding:50px 150px 50px 150px;">
            <a href="http://www.speedtypingonline.com/typing-test" 
               style="display: block; width: 550px; height: 100px; 
               background: url('/images/STO_logoPNG.png') no-repeat; 
               padding-top: 20px; padding-bottom: 10px; padding-left: 90px; color: black; 
               font-weight: bold; text-decoration: none; font-family: Arial, serif; 
               font-size: 40px;">Speed Typing Online</a>
               
            <h1 style="font-size: 36px; text-align:center;">
                <a href="http://www.speedtypingonline.com/typing-test">See how fast you type! <br /><br /> Take our free Typing Test</a>
            </h1>
               
        </div>
        
    </body>
</html>
        



<?php

/*htmlspecialchars($_GET["r"]) / 4968;
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
