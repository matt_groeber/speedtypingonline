<?php
class Classroom{
	private $_db,
			$_user;
	
	public function __construct($user = null){
		$this->_db = DB::getInstance();
		if($user){
			if($user instanceof User){
				$this->_user = $user;
			} else {
				$this->_user = new User($user);
			}
		}
	}
	
	public function getClasses($withStudentInfo = null){
		if($this->_user->isStudent()){
			return $this->_db->get('OL_classrooms', array('stud_id', '=', $this->_user->data()->id), 'class_name, req_confirm, is_verified, firstname, lastname', null, array(array('OL_students', 'users'), array('id','teach_id'), array('class_id', 'id')))->results();
		} else if($this->_user->isTeacher()){
			if($withStudentInfo){
				// 9-7-20 - User id 447557 was not seeing students unless third table joined was a LEFT join
				$db_obj = $this->_db->get('OL_students', array('teach_id', '=', $this->_user->data()->id), 'user_id, class_id, class_name, req_confirm, ccode, username, firstname, lastname, email, num_tests, num_lessons, is_verified', null, array(array('OL_classrooms', 'users', 'users_achieve'), array('class_id','stud_id','stud_id'), array('id', 'id','user_id'), array('INNER','INNER','LEFT')), array(2000));
				
				//var_dump($db_obj);
				
				return $db_obj->results();
				
			} else {
				return $this->_db->get('OL_classrooms', array('teach_id', '=', $this->_user->data()->id), 'id, class_name, req_confirm, ccode', null)->results();
			}
		}
	}
	
	public function CreateNewClassroom($classroomName=null){
		if($classroomName == null){
			$classroomName = "My Class";
		}
		
		if($this->_user->isTeacher()){
			$db_obj = $this->_db->get('OL_classrooms', array('teach_id', '=', $this->_user->data()->id), 'id');
			$assignTeachID = $db_obj->results()[0]->teach_id;
			if(count($db_obj->results()) < 5){
				
				$newCCode = $this->GenerateCCode_Private();
				
				$result = $this->_db->insert('OL_classrooms', array('teach_id' => $this->_user->data()->id, 'class_name' => $classroomName, 'ccode' => $newCCode, 'creation_dt' => gmdate('Y-m-d H:i:s')));
				
				if($result != true){
					$result = 'An error occurred';
				}
				
				return $result;
			} else {
				return 'You can\'t have more than 5 classes';
			}
			
		} else {
			return 'You must be a teacher to do that';
		}
		
	}
	
	public function GetAssignments($completenessFlag=null, $whereArr=null, $sort=null){
		$where = array();
		
		if($completenessFlag == 'incompleted')
			$where = array_merge($where, array('completed_date', '=', '0000-00-00 00:00:00'));
		else if($completenessFlag == 'completed')
			$where = array_merge($where, array('completed_date', '!=', '0000-00-00 00:00:00'));
		
		if($whereArr != null)
			$where = array_merge($where, $whereArr);
		
		if($sort == null)
			$sort = array('assign_date','ASC');
		
		if($this->_user->isStudent()){
			$where = array_merge($where, array('stud_id', '=', $this->_user->data()->id, 'is_assigned', '=', 1));
			
			return $this->_db->get('OL_assignments', $where, 'assign_id, title, notes, type, at_name, activity, metric_type, min_speed, selection, goal, progress, assign_date, due_date, completed_date, classroom_id, teach_id', $sort, array(array('OL_assignees','OL_assignTypes'),array('id','type'), array('assign_id','id')), array(997))->results();
		} else if($this->_user->isTeacher()){
			$where = array_merge($where, array('teach_id', '=', $this->_user->data()->id));
			
			$db_obj = $this->_db->get('OL_assignments', $where, 'OL_assignments.id, assign_id, title, notes, type, at_name, activity, metric_type, min_speed, selection, goal, assign_date, due_date, completed_date, is_assigned, progress, classroom_id, stud_id, username, firstname, lastname, email,tl_wpm_target,tl_accuracy_target', $sort, array(array('OL_assignees','OL_assignTypes','users','user_settings'),array('id','type','OL_assignees.stud_id','OL_assignees.stud_id'), array('assign_id','id','id','user_id'), array('LEFT', 'LEFT', 'LEFT', 'LEFT')), array(9999));
			
			$assignments = $db_obj->results();
			
			foreach($assignments as &$val){
				$val->assign_id = $val->id;
			}
			
			return $assignments;
		}
	}
	
	public function UpdateAssignmentProgress($selection, $wpm, $acc, $time, $activity=null, $specAssignId=null){
		if($this->_user->isStudent()){
			
			// need to convert selection from type (eg PANGRAMS) to tid (eg 10) for typing test activity type assignments
			if($activity == 'Test'){
				$textTypes = $this->_db->get('`text_types`', array('tid', '>', '0'), 'tid, type, display_text')->results();
				$selectionAsTextType = $selection;
				$selection = -1;
				foreach($textTypes as &$tt){
					if($tt->type == $selectionAsTextType){
						$selection = $tt->tid;
						break;
					}
				}
			}
			
			$whereArr = array();
			
			if($activity != null){
				$whereArr = array_merge($whereArr, array('activity', '=', $activity));
			}
			
			if($specAssignId != null){
				$whereArr = array_merge($whereArr, array('assign_id', '=', $specAssignId));
			}
			
			//var_dump($whereArr);
			$assignmentArr = $this->GetAssignments('incompleted', $whereArr, array('due_date', 'ASC')); // sort earliest due date first
			
			if($specAssignId != null){
				for($ii = 0; $ii < count($assignmentArr); $ii++){
					if($specAssignId == $assignmentArr[$ii]->assign_id){
						$fAss = $assignmentArr[$ii];
						break;
					}
				}
			} else {
				$fAss = $assignmentArr[0];
			}
			
			// this is working - now we need to setup so each assignment type is updated properly according to parameters
			$updateArr = array();
			$currentDT = gmdate('Y-m-d H:i:s');
			if($wpm > $fAss->min_speed){
				if($fAss->metric_type == 'Time'){
					$progressVal = $fAss->progress + $time;
					if($progressVal >= (int)$fAss->goal){
						$updateArr += ['completed_date' => $currentDT];
						$progressVal = $fAss->goal;
					}
				} else if ($fAss->metric_type == 'Achievement'){
					$metWPMtarget = $wpm >= $this->_user->data()->tl_wpm_target;
					$metAccTarget = $acc >= $this->_user->data()->tl_accuracy_target;
					
					if($metWPMtarget && $metAccTarget){
						if($fAss->activity == 'Lesson'){
							$updateArr += ['completed_date' => $currentDT];
							$progressVal = 1;
						} else if($fAss->activity == 'Test'){
							$progressVal = $fAss->progress + $time;
							if($progressVal >= (int)$fAss->goal){
								$updateArr += ['completed_date' => $currentDT];
							}
						} else {
							return "Assignment activity doesn't exist";
						}
					} else {
						if(!$metWPMtarget && !$metAccTarget){
							return 'did not meet target speed or target accuracy';
						} else if(!$metWPMtarget){
							return 'did not meet target speed';
						} else {
							return 'did not meet target accuracy';
						}
					}
				} else if ($fAss->metric_type == 'Count'){
					$progressVal = $fAss->progress + 1;
					if($progressVal >= (int)$fAss->goal){
						$updateArr += ['completed_date' => $currentDT];
					}
				}
			} else {
				return 'did not meet minimum speed requirement';
			}
			
			$updateArr += ['progress' => $progressVal];

			if(!empty($assignmentArr)){
				return $this->_db->update('OL_assignees', array($fAss->assign_id, $this->_user->data()->id), $updateArr, array('assign_id', 'stud_id'));
			} else {
				return 'no assignments to update!';
			}
		}
	}
	
	public function GetAssignmentTypes(){
		return $this->_db->get('OL_assignTypes', array('display_order', '!=', 0), 'id, display_order, at_name, at_descrip', null)->results();
	}
	
	public function getStudentHistory($classID = null){
		$lessonHist = $this->_db->getDistinct('OL_students', array('teach_id', '=', $this->_user->data()->id), 'OL_students.stud_id, lesson_module, lesson_timespent, timestamp', array('OL_students.stud_id','ASC'), array(array('OL_classrooms', 'lesson_history'), array('class_id','stud_id'), array('id', 'user_id')), array(20000))->results();
		$testHist = $this->_db->getDistinct('OL_students', array('teach_id', '=', $this->_user->data()->id), 'OL_students.stud_id, timestamp, total_time_secs', array('OL_students.stud_id','ASC'), array(array('OL_classrooms', 'typing_tests'), array('class_id', 'stud_id'), array('id', 'user_id')), array(20000))->results();

		return array($lessonHist, $testHist);
	}
	
	public function getStudentEmailList($studIdArr){
		
		$db_obj = $this->_db->get('OL_students', array('teach_id', '=', $this->_user->data()->id, 'stud_id', 'IN', $studIdArr), 'email', null, array(array('OL_classrooms', 'users'),array('class_id', 'stud_id'), array('id', 'id')))->results();
		
		return $db_obj;
	}
	
	public function isUserTeacherOfStudent($studentID){

		return !empty($this->_db->get('OL_students', array('teach_id', '=', $this->_user->data()->id, 'stud_id', '=', $studentID), 'stud_id', null, array('OL_classrooms','class_id', 'id'))->results());
	}
	
	public function addStudentToClass($studentID, $classCode){
		
		$classResults = $this->_db->get('OL_classrooms', array('ccode', '=', $classCode), 'OL_classrooms.id, stud_id, teach_id, email, users.username, class_name, ccode, req_confirm', null, array(array('OL_students','users'), array('id','teach_id'),array('class_id','id'),array('LEFT','INNER')));
		
		$classResults = $classResults->results();
		
		if(!empty($classResults)){
			$studentResults = $this->_db->get('OL_students', array('class_id', '=', $classResults[0]->id, 'stud_id', '=', $studentID),'is_verified')->results();
			
			if(empty($studentResults)){
				if($classResults[0]->req_confirm == '1'){
					$result = $this->_db->insert('OL_students', array('stud_id' => $studentID, 'class_id' => $classResults[0]->id, 'is_verified' => 0, 'date_added' => gmdate('Y-m-d H:i:s')));
					$emailResult = $this->EmailTeacherStudentAddedToClass(array($classResults[0]->email), $classResults[0]->class_name, 'applied to join', $classResults[0]->username);

					return 'You have requested to join the class and are waiting for your teacher\'s approval';
				} else {
					$result = $this->_db->insert('OL_students', array('stud_id' => $studentID, 'class_id' => $classResults[0]->id, 'is_verified' => 1, 'date_added' => gmdate('Y-m-d H:i:s')));
					$emailResult = $this->EmailTeacherStudentAddedToClass(array($classResults[0]->email),$classResults[0]->class_name, 'added to', $classResults[0]->username);

					return 'You have been added to the class';
				}
			} else {
				if($studentResults[0]->is_verified == -1){
					return 'You were denied entry into this class';
				} else {
					return 'You already belong to that class';
				}
			}
		} else {
			return 'Invalid class code';
		}
		
	}
	
	public function setClassroomJoinMethod($ccode, $reqConfirm){
		$retArr = $this->_db->get('OL_classrooms', array('teach_id', '=', $this->_user->data()->id, 'ccode', '=', $ccode), 'id')->results();
		
		if(!empty($retArr)){
			$dbResult = $this->_db->update('OL_classrooms', $ccode, array('req_confirm' => $reqConfirm), 'ccode');
			/*var_dump($dbResult);
			exit();*/
			return $dbResult;
		} else {
			return 'Did not work...';
		}
	}
	
	private function getClassroomCode($croomID){
		return $this->_db->get('OL_classrooms', array('id', '=', $croomID), 'ccode')->results();
	}
	
	private function GenerateCCode_Private(){
		do{
			$hash = substr(Hash::unique(), 0, 6); 
			$db_obj = $this->_db->get('OL_classrooms', array('ccode', '=', $hash));
			$hashCheck = $db_obj->results();
		} while (!empty($hashCheck));
		
		return $hash;
	}
	
	public function generateCCode($croomID){
		
		$hash = $this->GenerateCCode_Private();
		
		$dbResult = $this->_db->update('OL_classrooms', $croomID, array('ccode' => $hash), 'id');
		return $hash;
	}
	
	public function GetUserData(){
		return $this->_user->data();
	}
	
	public function EmailTeacherStudentAddedToClass($emailArr, $className, $state, $teachUsername){
		
		$englishPhraseHelp = '';
		$extraPhrase = '';
		if($state == "added to"){
			$englishPhraseHelp = " was ";
		} else {
		$extraPhrase = '<div style="margin-top:15px;"><h4>Click the button below to go to your class and either accept or deny this user entry into your class.</h4></div>';
		}
		
		$firstName = ($this->_user->data()->firstname == null) ? '-' : $this->_user->data()->firstname;
		$lastName = ($this->_user->data()->lastname == null) ? '-' : $this->_user->data()->lastname;
		
		//<a href="/user/  $studentValue->username; /lessons"><?php echo $studentValue->username;</a>
		
		$firstNameHTML = '<td style="text-align: center;">' . $firstName . '</td>';
		$lastNameHTML = '<td style="text-align: center;">' . $lastName . '</td>';
		$usernameHTML = '<td style="text-align: center;"><a href="' . Config::get('constants/rootUrl') . '/user/' . $this->_user->data()->username . '/test-stats">' . $this->_user->data()->username . '</a></td>';
		$emailHTML = '<td style="text-align: center;">' . $this->_user->data()->email . '</td>';
		
		$emailSubj = 'Student ' . $state . ' your class: "' . $className . '"';
		
		// top message
		$emailBodyHTML = '<h3 style="color: #00668C">A new student ' . $englishPhraseHelp . $state . ' your class "' . $className . '":</h3>';
		
		// student info box
		$emailBodyHTML .= '<div style="border: 1px solid #FFD1AC; background: #FFF8EA; max-width: 420px; margin-top: 5px; border-radius: 8px; text-align:center; font-size:1.2em; padding:0 20px 20px 20px;">' . 
		'<table><tr><th style="padding:0px 15px">Last</th><th style="padding:0px 15px">First</th><th style="padding:0px 15px">Username</th><th style="padding:0px 15px">Email</th></tr>' . 
		'<tr>' . $lastNameHTML . $firstNameHTML . $usernameHTML . $emailHTML . '</tr></table>' . 
		'</div>';
		
		// extra phrase if class is locked
		$emailBodyHTML .= $extraPhrase;
		
		// go to class link
		$emailBodyHTML .= '<div style="border: 1px solid #739497; background: #93c3cd; max-width: 300px; margin-top: 25px; margin-left: 75px; border-radius: 8px; text-align:center; font-size:1.2em; font-weight:bold;">' . 
		'<a href="' . Config::get('constants/rootUrl') . '/user/' . $teachUsername . '/classes" style="height: 100%; width:100%; display: inline-block; padding: 10px; text-decoration:none; font-size:20px; color:#2d495f;"><div>Go To Class</div></a>' .
		'</div>';

		$emailRet = $this->_user->email($emailArr, $emailSubj, $emailBodyHTML);

		return $emailRet;
	}
	
	public function EmailNewAssignment($studentsArr, $assignmentId, $assignTitle, $assignType, $dueDateWformatting, $assignSelection, $assignNotes, $assignMode){
		
		if($this->_user->isTeacher()){
			$assignEmailResults = $this->getStudentEmailList($studentsArr);
			$assignEmailArr = array();
			for($ii = 0; $ii < count($assignEmailResults); $ii++){
				$assignEmailArr[] = $assignEmailResults[$ii]->email;
			}
			
			if($assignSelection == '83' || $assignSelection == '84'){
				$assignLinkPage = 'keyboard-basics';
			} else {
				$assignLinkPage = 'typing-tutor';
			}
			
			if($assignType != 3 && $assignType != 4){
				$assignLink = '/' . $assignLinkPage . '?mod=' . $assignSelection . '&sa_id=' . $assignmentId;
			} else {
				$textTypes = $this->_db->get('`text_types`', array('tid', '>', '0'), 'tid, type, display_text')->results();
				for($ii = 0; $ii < count($textTypes); $ii++){
					if($textTypes[$ii]->tid == $assignSelection)
						break;
				}
				$assignLink = '/typing-test?ttdl=' . $textTypes[$ii]->type . '&sa_id=' . $assignmentId;
			}
			
			if($assignMode == 'new'){
				$subjBlurb = 'New Assignment!';
				$teacherBlurb = 'gave you a new assignment!';
				$doAssignmentBtn = '<div id="goDoAssignment" style="border: 2px solid #AD5E0C; background: #E0821F; max-width: 240px; margin: 18px auto 0px auto; border-radius: 8px; text-align:center; font-size:1.2em; font-weight:bold;"><a href="' . Config::get('constants/rootUrl') . $assignLink . '" style="height: 100%; display: inline-block; padding: 10px; text-decoration:none; font-size:20px; color:#FFF;"><div>Go Do Assigment</div></a></div>';
			} else if($assignMode == 'delete'){
				$subjBlurb = 'Assignment Deleted';
				$teacherBlurb = 'removed this assignment for you:';
				$doAssignmentBtn = '';
			} else {
				$subjBlurb = 'Edit to Assignment';
				$teacherBlurb = 'made a change to your assignment.';
				$doAssignmentBtn = '<div id="goDoAssignment" style="border: 2px solid #AD5E0C; background: #E0821F; max-width: 240px; margin: 18px auto 0px auto; border-radius: 8px; text-align:center; font-size:1.2em; font-weight:bold;"><a href="' . Config::get('constants/rootUrl') . $assignLink . '" style="height: 100%; display: inline-block; padding: 10px; text-decoration:none; font-size:20px; color:#FFF;"><div>Go Do Assigment</div></a></div>';
			}
			
			$assignmentsPageLink = Config::get('constants/rootUrl') . '/assignments.php';
			$emailSubj = $subjBlurb . ' - "' . $assignTitle . '" (Due: ' . $dueDateWformatting . ')';
			$emailRet = $this->_user->email($assignEmailArr, $emailSubj, '<h3 style="color: #00668C">Your teacher (' . $this->_user->data()->firstname . ' ' . $this->_user->data()->lastname . ') ' . $teacherBlurb . '</h3>' . 
				'<div style="border: 1px solid #FFD1AC; background: #FFF8EA; max-width: 420px; margin-top: 5px; border-radius: 8px; text-align:center; font-size:1.2em; padding:0 20px 20px 20px;">' . 
					'<p><div style="font-size:22px; font-weight:bold;"><b>' . $assignTitle . '<b></div></p>' . 
					'<div style="width: 90%; text-align: left; margin: 0 auto;"><span style="display: block; font-size: 14px; font-weight: 700; text-decoration: underline;">Due</span> ' . $dueDateWformatting . '</div>' . 
					'<div style="width: 90%; text-align: left; margin: 20px auto 0px auto;"><span style="display: block; font-size: 14px; font-weight: 700; text-decoration: underline;">Notes</span> ' . $_POST['note'] . '</div>' . $doAssignmentBtn .
				'</div>' .
				'<div style="border: 1px solid #739497; background: #93c3cd; max-width: 300px; margin-top: 25px; margin-left: 75px; border-radius: 8px; text-align:center; font-size:1.2em; font-weight:bold;"><a href="' . Config::get('constants/rootUrl') . '/assignments.php" style="height: 100%; display: inline-block; padding: 10px; text-decoration:none; font-size:20px; color:#2d495f;"><div>See All Assignments</div></a></div>');

			return $emailRet;
		}

	}
}

?>