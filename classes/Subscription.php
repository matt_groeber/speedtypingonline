<?php
class Subscription{
	private $_db;
	
	public function __construct(){
		$this->_db = DB::getInstance();
	}
	
	/*public function GetSubId($userId){
		return $this->_db->get('users_subs', array('user_id', '=', $userId), 'user_id, subscription_id, created, status, plan_period_end', array('created', 'desc'))->results()[0]->subscription_id;
	}*/
	public function GetPlanLength($planId){
		$retVal = null;
		
		switch($planId){
			case 'P-9A038109HH3870947MB4IQVY':
				$retVal = 31;
				break;
			case 'P-44942726EK110974BMCEER2A':
				$retVal = 366;
				break;
			case 'P-5FY57216HH153244RMBTDRUA':
				$retVal = 31;
				break;
			case 'P-1C3520401G8395604MBTDPVQ':
				$retVal = 366;
				break;
		}
		
		return $retVal;
	}
	
	public function GetSubscriptionStatus($userId){
		$subResult = $this->_db->get('users_subs', array('user_id', '=', $userId), 'user_id, subscription_id, created, status, plan_id, plan_period_end, is_trial', array('created', 'desc'))->results()[0];
		
		return array($subResult->status, $subResult->plan_period_end, $subResult->is_trial, $subResult->plan_id);
	}
	
	public function GetAccessToken(){
	$ch = curl_init();
	$clientId = "AZIhM2sBODN4Gek5hETFxyqW1cfdN6Ns5bAfbikCbVVR8hC2C74nyBu39SMyvLzxuh2-WQbnhzKEIdss";
	$secret = "EN1EVIcGqGT4txeqIXQ0JtlQ48fz8Rvg_uCzoMg_-1So0iYc0c0s6XQisesqcU2PV7pP3vN2XSfu1qb3";

	curl_setopt($ch, CURLOPT_URL, "https://api-m.paypal.com/v1/oauth2/token");
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

	$result = curl_exec($ch);
	curl_close($ch);

	if(empty($result)){
		return false;
	} else {
		$json = json_decode($result);
		return $json->access_token;
	}

}
	
	
	
	
	public function GetSubscriptionDetails($subId){
		$accessToken = $this->GetAccessToken();
		echo $accessToken;
		echo '<br /><br />';
		$ch = curl_init();
		
		curl_setopt_array($ch, array(
			CURLOPT_URL => "https://api-m.paypal.com/v1/billing/subscriptions/" . $subId,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
			"authorization: Bearer " . $accessToken,
			"content-type: application/json"
			),
		));

		$result = curl_exec($ch);
		//$err = curl_error($ch);
		//var_dump($err);
		curl_close($ch);

		if(empty($result)){
			return false;
		} else {
			$json = json_decode($result);
			//var_dump($json);
			return $json;
		}
	}
	
	public function SuspendOrCancelSubscriptionByUserId($userId, $cancel = false){
		$subObjArr = $this->_db->get('users_subs', array('user_id', '=', $userId, 'status', '!=', 'CANCELLED'), 'user_id, subscription_id, created, status, plan_period_start, plan_period_end', array('created', 'desc'))->results();
		error_log('--------------------------------------------------');
		error_log("     CALLED CancelSubscriptionByUserId");
		error_log('--------------------------------------------------');
		//var_dump($subObjArr);
		foreach($subObjArr as $subObj){
			
			$startDate = new DateTime($endDateInDB);
			
			$db_obj2 = $this->_db->update('users_subs', $subObj->subscription_id, array('cancel_hook_failed' => 1), 'subscription_id');
			//$this->CancelSubscription($subObj->subscription_id);
			if($cancel)
				$this->CancelSubscription($subObj->subscription_id);
			else
				$this->SuspendSubscription($subObj->subscription_id);
		}
	}

	public function CancelSubscription($subId){
		$accessToken = $this->GetAccessToken();
		$ch = curl_init();
		error_log('--------------------------------------------------');
		error_log("      CALLED CancelSubscription");
		error_log("subID = " . $subId);
		error_log('--------------------------------------------------');
		curl_setopt_array($ch, array(
			CURLOPT_URL => "https://api-m.paypal.com/v1/billing/subscriptions/" . $subId . "/cancel",
			//CURLOPT_URL => "https://api.sandbox.paypal.com/v1/billing/subscriptions/I-9XLJEDMAXH35/cancel",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => array("reason" => "Not satisfied with the service"),
			CURLOPT_POST => true,
			CURLOPT_HTTPHEADER => array(
			"authorization: Bearer " . $accessToken,
			"content-type: application/json",
			),
			//CURLOPT_POSTFIELDS => "user wishes to cancel subscription",
		));
		
		var_dump($ch);
		$result = curl_exec($ch);
		var_dump($result);
		$err = curl_error($ch);
		error_log("curl error:");
		error_log($err);
		var_dump($err);
		curl_close($ch);

		if(empty($result)){
			error_log('--------------------------------------------------');
			error_log("        RESULT WAS EMPTY!");
			error_log('--------------------------------------------------');
			return false;
		} else {
			$json = json_decode($result);
			
			error_log('--------------------------------------------------');
			error_log("        RESULT WAS GOOD!");
			error_log('--------------------------------------------------');
			//var_dump($json);
			return $json;
		}
	}
	
	public function SuspendSubscription($subId){
		$accessToken = $this->GetAccessToken();
		$ch = curl_init();
		error_log('--------------------------------------------------');
		error_log("      CALLED SuspendSubscription");
		error_log("subID = " . $subId);
		error_log('--------------------------------------------------');
		curl_setopt_array($ch, array(
			CURLOPT_URL => "https://api-m.paypal.com/v1/billing/subscriptions/" . $subId . "/suspend",
			//CURLOPT_URL => "https://api.sandbox.paypal.com/v1/billing/subscriptions/I-9XLJEDMAXH35/cancel",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => array("reason" => "Not satisfied with the service"),
			CURLOPT_POST => true,
			CURLOPT_HTTPHEADER => array(
			"authorization: Bearer " . $accessToken,
			"content-type: application/json",
			),
			//CURLOPT_POSTFIELDS => "user wishes to cancel subscription",
		));
		
		var_dump($ch);
		$result = curl_exec($ch);
		var_dump($result);
		$err = curl_error($ch);
		var_dump($err);
		curl_close($ch);

		if(empty($result)){
			error_log('--------------------------------------------------');
			error_log("        RESULT WAS EMPTY!");
			error_log('--------------------------------------------------');
			return false;
		} else {
			$json = json_decode($result);
			
			error_log('--------------------------------------------------');
			error_log("        RESULT WAS GOOD!");
			error_log('--------------------------------------------------');
			//var_dump($json);
			return $json;
		}
	}

	public function CreateSubscription($purchaseEmail, $accountEmail){
		
		$planid = 'P-26E75358584637235L6S65WY';	// Daily Plan
		$planid = 'P-0H6618474J512313AL6HPW2I';	// Monthly Plan
		$accessToken = $this->GetAccessToken();
		//echo 'access token = "' . $accessToken . '"';
		
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://api-m.paypal.com/v1/billing/subscriptions");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		/*echo '<br /><br />';
		echo 'test2';*/
		$time = getCurrentTime();
		/*echo '<br /><br />';
		echo 'test3';
		echo '<br /><br />';
		echo '{ "plan_id": "' . $planid . '", "start_time": "' . $time . '", "subscriber": { "name": {"given_name": "John", "surname": "Doe"}, "email_address": "' . $purchaseEmail . '"}, "application_context": { "brand_name": "STO sandbox", "locale": "en-US", "shipping_preference": "NO_SHIPPING", "user_action": "SUBSCRIBE_NOW", "payment_method": { "payer_selected": "PAYPAL", "payee_preferred": "IMMEDIATE_PAYMENT_REQUIRED"}, "return_url": "https://searchmarkit.com/typing-tutor", "cancel_url": "https://searchmarkit.com/typing-test"} }';
		
		echo '<br /><br />';
		echo '<br /><br />';*/
		
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, '{ "plan_id": "' . $planid . '", "start_time": "' . $time . '", "subscriber": { "name": {"given_name": "John", "surname": "Doe"}, "email_address": "sb-jq3gp3482832@personal.example.com"}, "application_context": { "brand_name": "Speed Typing", "locale": "en-US", "shipping_preference": "NO_SHIPPING", "user_action": "SUBSCRIBE_NOW", "payment_method": { "payer_selected": "PAYPAL", "payee_preferred": "IMMEDIATE_PAYMENT_REQUIRED"}, "return_url": "https://searchmarkit.com/typing-tutor", "cancel_url": "https://searchmarkit.com/typing-test"} }');

		$headers = array();
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Authorization: Bearer ' . $accessToken;
		$headers[] = 'Content-Type: application/json';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		//var_dump($ch);
		error_log('--------------------------------------------------');
		error_log('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$> Calling curl_exec');
		error_log('--------------------------------------------------');
		$result = curl_exec($ch);
		//return $result;

		curl_close($ch);
		
		if(empty($result)){
			return false;
		} else {
			$json = json_decode($result);
		return $json->links[0]->href;
			//return $json;
			//var_dump($json);
		}
		
		/*echo '<br /><br />';
		
		echo 'Status = ' . $json->status;
		echo '<br /><br />';
		echo 'id = ' . $json->id;
		echo '<br /><br />';
		echo 'time = ' . $json->create_time;
		echo '<br /><br />';
		echo 'approval link = ' . $json->links[0]->href;*/
		
		$db = DB::getInstance();
		$result = $db->insert('users_subs', array('purchase_email' => $purchaseEmail, 'account_email' => $accountEmail, 'subscription_id' => $json->id, 'plan_id' => $planid, 'status' => $json->status, 'created' => $json->create_time, 'plan_period_start' => $time));
		//var_dump($result);
		
		return $json->links[0]->href;

	}
	
	public function ReactivateSubscription($subId){
		$accessToken = $this->GetAccessToken();
		$ch = curl_init();

		curl_setopt_array($ch, array(
			CURLOPT_URL => "https://api-m.paypal.com/v1/billing/subscriptions/" . $subId . "/activate",
			//CURLOPT_URL => "https://api.sandbox.paypal.com/v1/billing/subscriptions/I-9XLJEDMAXH35/cancel",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS => array("reason" => "Reactivate subscription"),
			CURLOPT_POST => true,
			CURLOPT_HTTPHEADER => array(
			"authorization: Bearer " . $accessToken,
			"content-type: application/json",
			),
			//CURLOPT_POSTFIELDS => "user wishes to cancel subscription",
		));
		
		var_dump($ch);
		$result = curl_exec($ch);
		var_dump($result);
		$err = curl_error($ch);
		var_dump($err);
		curl_close($ch);

		if(empty($result)){
			return false;
		} else {
			$json = json_decode($result);
			//var_dump($json);
			return $json;
		}
	}
}

?>
