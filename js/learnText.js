
var lastRandNum;
var theEntry;
var tutStep = 0;


function GetText(){
	
	if(textSelection == "CUSTOM_LESSON"){
		// if text type is random, a "getElementsByTagName("textTypeYouAreLookingFor").length == 1"
		// indicates that the type chosen is the textTypeYouAreLookingFor
		var tempString = "";
		var lastRN = -1;			// initialize to a known impossible value for currRN
		var currRN;
		var lessonLength = $('#lessonLengthSelected').val();
				
		var sOdds = new Array(0.05, 0.17, 0.42, 0.67, 0.9, 0.96, 0.99, 1);
		textString = $('#customLessonTA').val();
		var lettersToLearn = textString;
		tempString = textString.split("");
		
		// clean up input (remove all spaces)
		for( var lcv = 0; lcv < tempString.length; lcv++){
			if(tempString[lcv] == " "){
				tempString.splice(lcv, 1);	// remove the space at tempString[lcv]
				lcv--;		// backup one
			}
		}
		
		currRN = Math.floor(Math.random()*(tempString.length - 1));
		textString = tempString[currRN];
		
		while(textString.length < lessonLength){
		
			var i = 0;
			var length = 1;
			var rand = Math.random();
			
			while(rand > sOdds[length - 1]){
				length++;
			}
			
			while( i < length){
				textString += tempString[Math.floor(Math.random()*tempString.length)];
				i++;
			}
			
			textString += " ";
		}
		
		$("#prevLessonLink").hide();
		$("#nextLessonLink").hide();
		$('#lessonLettersToLearn').removeClass("doubleLine");
		$('#lessonDivContainer').removeClass("review");
		$('#lessonDivContainer').removeClass("lesson");
		$('#lessonDivContainer').addClass("custom");
		$('.current').removeClass("current");
		$('#LL_title').html("Custom");
		
		resultsTitle = "Custom Lesson (\"" + lettersToLearn.split("").join(", ") + "\") Results";
		setupLessonInfoPanel("Custom Lesson", resultsTitle, "", '', lettersToLearn);
		setupLessonText(textString);
	} else {
		
		$.post('/getData.php', { type: "tl"},
			function(output){
				myJSONdata = $.parseJSON(output);
				lessonData = myJSONdata["lessonsWithoutFullHistory"];
				loggedInResults = myJSONdata["loggedInResults"];
				
				setupLessonProfileBlurb(myJSONdata);
				
				var textType = "CL";
				textString = "d f j k dd ff jj kk ddf ddj ddk ffd ffj ffk jjd jjf jjk kkd kkf kkj dfd djd dkd fdf fjf fkf jdj jfj jkj ddff ddjj ddkk ffdd ffjj ffkk jjdd jjff jjkk kkdd kkff kkjj dfj dfk";
				var titlePrefix = '';
				var theTitle = "Default Lesson";
				var thePurpose = "Lesson Placeholder";
				var theHint = "Browser had issues loading lesson data"
				var theT_Type = "default"
				var lettersToLearn = "dfjk";
				
				if(lessonData && (lessonIndex < lessonData.length && lessonIndex >= 0) && lessonData[lessonIndex].text_to_type != ''){
					textType = "CL";
					textString = lessonData[lessonIndex].text_to_type;
					titlePrefix = (lessonData[lessonIndex].type == "advanced") ? "Advanced " : '';
					theTitle = titlePrefix + lessonData[lessonIndex].lesson_type + ' ' + lessonData[lessonIndex].lesson_num;
					thePurpose = lessonData[lessonIndex].purpose;
					theHint = lessonData[lessonIndex].hint;
					theT_Type = lessonData[lessonIndex].text_type;
					
					if(lessonData[lessonIndex].display_text != ''){
						lettersToLearn = lessonData[lessonIndex].display_text;
						$('#lessonLettersToLearn').addClass("doubleLine");
					} else {
						lettersToLearn = lessonData[lessonIndex].letters.split("").join(" ");
						$('#lessonLettersToLearn').removeClass("doubleLine");
					}				
					
					//$('#nextLessonDiv').html(myJSONdata["loggedInResults"].nextLessonHTML);

					if(lessonIndex < lessonData.length && lessonIndex > 0){
						$("#prevLessonLink").show();
						var PLIndex = currentIndex-1;
						var prevLessonPage = (lessonData[PLIndex].type == "keyboard") ? "keyboard-basics.php" : "typing-tutor.php";
						$("#prevLessonLink").attr("href", "https://www.speedtypingonline.com/" + prevLessonPage + ".php?mod=" + lessonData[PLIndex].id);
						$("#prevLessonLink").attr("title", lessonData[PLIndex].type.charAt(0).toUpperCase() + lessonData[PLIndex].type.slice(1) + ' ' + lessonData[PLIndex].lesson_type + ' ' + lessonData[PLIndex].lesson_num + ':\n' + lessonData[PLIndex].purpose);
					} else {
						$("#prevLessonLink").hide();
					}
					
					var nextLessonMod = lessonIndex + 2;
					if(nextLessonMod <= lessonData.length && nextLessonMod > 0){
						$("#nextLessonLink").show();
						var NLIndex = currentIndex+1;
						$("#nextLessonLink").attr("title", lessonData[NLIndex].type.charAt(0).toUpperCase() + lessonData[NLIndex].type.slice(1) + ' ' + lessonData[NLIndex].lesson_type + ' ' + lessonData[NLIndex].lesson_num + ':\n' + lessonData[NLIndex].purpose);
					} else {
						$("#nextLessonLink").hide();
					}
					
					
					$('#lessonMod_' + lessonData[currentIndex].id).addClass("current");
				} else {
					$("#prevLessonLink").hide();
					$("#nextLessonLink").hide();
				}
				
				if(theTitle.toLowerCase().indexOf("review") >= 0){
					$('#lessonDivContainer').removeClass("custom");
					$('#lessonDivContainer').removeClass("lesson");
					$('#lessonDivContainer').addClass("review");
					$('#LL_title').html("Review");
				} else {
					$('#lessonDivContainer').removeClass("custom");
					$('#lessonDivContainer').removeClass("review");
					$('#lessonDivContainer').addClass("lesson");
					$('#LL_title').html("Learn");
				}
				
				// post-processing of textString
				textString = textString.replace(/&amp;/g, "&");
				textString = textString.replace(/&lt;/g, "<");
				textString = textString.replace(/&gt;/g, "<");
				textString = textString.replace(/  /g, " ");    // eliminate double spacing
				
				// if text type is random, a "getElementsByTagName("textTypeYouAreLookingFor").length == 1"
				// indicates that the type chosen is the textTypeYouAreLookingFor
				var tempString = "";
				var lastRN = -1;			// initialize to a known impossible value for currRN
				var currRN;
				var lessonLength = $('#lessonLengthSelected').val();
				var RTE = " Results";
				resultsTitle = theTitle + RTE;
				var lessonTitle = theTitle + ":";
				var sel_ID = "#" + textType;
				
				$(".RL").removeClass("Sel_Lesson");
				$(".CL").removeClass("Sel_Lesson");
				$(".AL").removeClass("Sel_Lesson");
				$(sel_ID).addClass("Sel_Lesson");
				
				
				setupLessonInfoPanel(lessonTitle, resultsTitle, thePurpose, theHint, lettersToLearn);
				
				
				$('#centerContent').show();
				$('#centerConsoleWrap').removeClass("projectorScreen");
				if(theT_Type == "letters"){
					tempString = textString.split("").sort(function() { return 0.5 - Math.random() });
					textString = "";
					
					var spaceOdds = [0, .2, .25, .5, .75, 1];
					var wordString = "";

					
					for(var ii = 0; ii < tempString.length; ii++){
						textString += tempString[ii] + ' ';
					}
					
					var adjustedLessonLength = parseInt(lessonLength) + tempString.length;
					
					while(textString.length < adjustedLessonLength){
						wordString = "";
						while(Math.random() > spaceOdds[wordString.length]){
							wordString += tempString[Math.floor(Math.random()*tempString.length)];
						}
						
						textString += wordString + ' ';
					}
				} else if(theT_Type == "sentences"){
					tempString = textString.split("$");
					textString = "";
					lessonLength *= 2;
					while(textString.length < lessonLength){
						currRN = Math.floor(Math.random()*tempString.length);
						if(currRN != lastRN){
							textString += tempString[currRN] + ' ';
							lastRN = currRN;
						}
					}
					//textString = textString.substr(0, textString.length - 1);
				} else{
					tempString = textString.split(" ");
					textString = "";
					while(textString.length < lessonLength){
						currRN = Math.floor(Math.random()*tempString.length);
						if(currRN != lastRN){
							textString += tempString[currRN] + " ";
							lastRN = currRN;
						}
					}
				}
				
				setupLessonText(textString);
		});
    }
	
}


function ResetLessonTutorial(){
	$("#BigHandsImg").css("top", "-80px");
}

function setupCurrentLesson(id, val, defaultVal, target){
	var elem = $(id);
	elem.html(val);
	if(val == defaultVal){
		elem.parent().removeClass("success");
		elem.parent().removeClass("warn");
		return;
	} 
	
	val = parseFloat(val);
	target = parseFloat(target);
	
	if(val < target){
		elem.parent().removeClass("success");
		elem.parent().addClass("warn");
	} else {
		elem.parent().addClass("success");
		elem.parent().removeClass("warn");
	}
}

function setupLessonInfoPanel(theLessonTitle, theResultsTitle, thePurpose, theHint, lettersToLearn){
	var theLessonTitle = theLessonTitle===undefined ? "Default Purpose" : theLessonTitle;
	var theResultsTitle = theResultsTitle===undefined ? "Default Purpose" : theResultsTitle;
	var thePurpose = thePurpose===undefined ? "Default Purpose" : thePurpose;
	var theHint = theHint || "Focus on proper technique before speed.";
	var lettersToLearn = lettersToLearn===undefined ? "-" : lettersToLearn;
	var pn = $('#personName').val();
	var personalizedName = (pn == '' || pn == '<Name>') ? "" : pn + "'s ";
	
	$("#textPurposeDiv").html(theLessonTitle);
	$("#resultsTitle").html(personalizedName + theResultsTitle);
	$("#lessonPurposeDisplay").html(thePurpose);
	$("#lessonHintDisplay").html("<ul class=\"dashed\"><li>" + theHint.split("; ").join("</li><li>") + "</li></ul>");
	$("#lessonLettersToLearn").text(lettersToLearn);
}

function setupLessonProfileBlurb(JSONdata){
	var lessonData = JSONdata["lessonData"];
	var loggedInResults = JSONdata["loggedInResults"];
	var lessonsWithoutHistory = JSONdata["lessonsWithoutFullHistory"];
	
	if(!JSONdata["loggedInUsername"]){
		$('#loginOrRegister').html('<div id="askToLogin" class="stoInfoBox" onclick="location.href=\'/login.php\';"><span><a href="/login.php">Login</a> or <a href="/register.php">Register</a></span><span> to track progress and set targets.</span></div>');
	} else{
		var totalLessonsPassed = loggedInResults.classicPassedCount + loggedInResults.advancedPassedCount;
		var totalLessons = loggedInResults.classicLessonCount + loggedInResults.advancedLessonCount;
		
		$('#numLessonsPassed').html(totalLessonsPassed + ' <span class="noteSmall">(' + Math.round((totalLessonsPassed/totalLessons)*1000)/10 + '%)</span>');
		$('#classicLessons').html(loggedInResults.classicLessonsHTML);
		
		var lessonWPM;
		var lessonAcc;
		
		if(textSelection == "CUSTOM_LESSON"){
			lessonWPM = tutorGlobals['customLessonWPM'];
			lessonAcc = tutorGlobals['customLessonAccuracy'];
		} else {
			lessonWPM = (lessonsWithoutHistory[lessonIndex].wpm == null) ? tutorGlobals["defaultWPM"] : lessonsWithoutHistory[lessonIndex].wpm;
			lessonAcc = (lessonsWithoutHistory[lessonIndex].accuracy == null) ? tutorGlobals["defaultAccuracy"] : lessonsWithoutHistory[lessonIndex].accuracy;
			if(lessonAcc.slice(-2) == '.0'){ lessonAcc = lessonAcc.slice(0,-2);}
		}
				
		tutorGlobals["targetWPM"] = loggedInResults.wpmTarget;
		tutorGlobals["targetAccuracy"] = loggedInResults.accTarget;
		
		setupCurrentLesson('#currentLessonWPM', lessonWPM, tutorGlobals["defaultWPM"], loggedInResults.wpmTarget);
		setupCurrentLesson('#currentLessonAcc', lessonAcc, tutorGlobals["defaultAccuracy"], loggedInResults.accTarget);
		
		if(lessonWPM == tutorGlobals["defaultWPM"] || lessonAcc == tutorGlobals["defaultAccuracy"]){
			$('#lessonsPassedDiv').show();
			$('#lessonScoreDiv').hide();
		} else {
			var lessonScore = 0;
			var scoreHTML = '';
			var wpmScore = lessonWPM / loggedInResults.wpmTarget;
			var accScore = lessonAcc / loggedInResults.accTarget;
			
			var lowestScore = Math.min(wpmScore, accScore);
			if(lowestScore < 1){
				lessonScore = lowestScore;
			} else {
				lessonScore = wpmScore + accScore;
				
				if(lessonsWithoutHistory[lessonIndex].lesson_type.toLowerCase().indexOf("review") >= 0){
					scoreHTML = ' <i class="fa fa-star" aria-hidden="true"></i>';
				} else {
					scoreHTML = ' <i class="fa fa-check" aria-hidden="true"></i>';
				}
				
			}
			
			$('#lessonsPassedDiv').hide();
			$('#lessonScoreDiv').show();
		}
		
		
		scoreHTML = Math.floor(lessonScore * 100) + scoreHTML;
		$('#currentLessonScore').html(scoreHTML);
		
		
		$('#recommendedNextLesson').html(loggedInResults.nextLessonHTML);
		var temp = $('#recommendedNextLesson a').attr("href");
		tutorGlobals["nextLessonMod"] = parseInt(temp.substr(temp.indexOf("=") + 1)) - 1;
		//$('#recommendedNextLesson a').attr("href", "#");
	}
}

function setupLessonText(textString){
	var tempStr = "";
	var tempBlockDivStr = new Array("","","","");
	var localCutoff = 0;
	minCursorReplaceStart = 0;
	
	for(i=0; i < 4; i++){
		tempStr = textString.substring(cutoff, (42*(i+1)) - ((42*i) - cutoff));   // get next 42 characters from text string
		localCutoff = tempStr.search(/(\r\n|\n|\r)/);
		if (localCutoff < 1){      // if next 42 characters do not contain a carriage return (\n) (or solely a newline)
			
		
			localCutoff = tempStr.lastIndexOf(" ") + 1;                 // find last space in 42 chars

			if(localCutoff == 0){
				localCutoff = 42;   // if no space found within next 42 chars, cut word at full block line length
			}

			for(j=0; j<localCutoff; j++){

				blockDivText[i] += '<span class="plainText">' + tempStr.substring(j, j+1) + '</span>';
			}
			//blockDivText[i] = tempStr2.substring(0,localCutoff);     USE WITHOUT SECOND INSIDE FOR LOOP
			cutoff += localCutoff;
		}
		else{       // next 42 characters contained a carriage return
			var tempStr2 = tempStr.substring(0, localCutoff+1);
			tempStr2 = tempStr2.replace(/(\r\n|\n|\r|\s\r\n|\s\n|\s\r)/," ");

			for(j=0; j<tempStr2.length; j++){

				blockDivText[i] += '<span class="plainText">' + tempStr2.substring(j, j+1) + '</span>';
			}
			cutoff += localCutoff + 1;
			
		}
		if(i <= 1){ minCursorReplaceStart += localCutoff + 2;}      // minCursorReplaceStart is the length of the very first two lines...
		
	}
	// place cursor on first letter
	blockDivText[0] = blockDivText[0].replace(/plainText/i,"nxtLetter");
	tempBlockDivStr[0] = blockDivText[0];
	tempBlockDivStr[1] = blockDivText[1];
	tempBlockDivStr[2] = blockDivText[2];
	tempBlockDivStr[3] = blockDivText[3];
	
	cleanTextString = textString.replace(/\s\n|\n\s|\n/gm," ");    // remove all newline characters leaving spaces if needed
	tempStr = "";   // initialize for line div
	var spaceFound = textString.indexOf(" ", 41);
	cleanTextString = cleanTextString + "                                 ";				// add some blank spaces to the end so cursor stays at center at end of string
	
	tempStr += '<span class="nxtLetter">' + cleanTextString.substring(0, 1) + '</span>';
	keyIndicator(cleanTextString.substring(0, 1));
	
	for(i=1; i<=spaceFound; i++){
		
		tempStr += '<span class="plainText">' + cleanTextString.substring(i, i+1) + '</span>';
		
	}
					
	lineDivText = '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ tempStr.substring(0,(32*22));
	
	
	blockDiv = $('#blockDivContainer').get();
	blockDiv = blockDiv[0];
	
	lineDiv = $('#lineDivContainer').get();
	lineDiv = lineDiv[0];
	
	blockDiv.innerHTML = formatForInnerHTML_block(tempBlockDivStr);
	lineDiv.innerHTML = formatForInnerHTML(lineDivText);
		
	//Target IE6 and below
	/*if ($.browser.msie && $.browser.version <= 6 ) {
		$("div.blockLines").css("font-size", "25px" );
		$("#line_input").css("font-size", "25px" );
	}*/
	
	$('#lessonDivContainer').show();
	disableKeyboard = false;
}

function getLessonData(callback){
	$.post('getData.php', { type: "tl" },callback);
}

 
 function keyIndicator(nL){

var numRowHeight = -249;
var topRowHeight = -205;		// was 77px
var homeRowHeight = -161;		// was 121px
var botRowHeight = -116;		// was 166px

	switch(nL){
		case "a": doKeyPressShading(true, 109, homeRowHeight, false, false, false, 1); break;
		case "A": doKeyPressShading(true, 109, homeRowHeight, false, false, true, 18); break;
		case "b": doKeyPressShading(true, 306, botRowHeight, false, false, false, 4); break;
		case "B": doKeyPressShading(true, 306, botRowHeight, false, false, true, 48); break;
		case "c": doKeyPressShading(true, 218, botRowHeight, false, false, false, 3); break;
		case "C": doKeyPressShading(true, 218, botRowHeight, false, false, true, 38); break;
		case "d": doKeyPressShading(true, 197, homeRowHeight, false, false, false, 3); break;
		case "D": doKeyPressShading(true, 197, homeRowHeight, false, false, true, 38); break;
		case "e": doKeyPressShading(true, 185, topRowHeight, false, false, false, 3); break;
		case "E": doKeyPressShading(true, 185, topRowHeight, false, false, true, 38); break;
		case "f": doKeyPressShading(true, 241, homeRowHeight, false, false, false, 4); break;
		case "F": doKeyPressShading(true, 241, homeRowHeight, false, false, true, 48); break;
		case "g": doKeyPressShading(true, 285, homeRowHeight, false, false, false, 4); break;
		case "G": doKeyPressShading(true, 285, homeRowHeight, false, false, true, 48); break;
		case "h": doKeyPressShading(true, 329, homeRowHeight, false, false, false, 5); break;
		case "H": doKeyPressShading(true, 329, homeRowHeight, false, true, false, 15); break;
		case "i": doKeyPressShading(true, 406, topRowHeight, false, false, false, 6); break;
		case "I": doKeyPressShading(true, 406, topRowHeight, false, true, false, 16); break;
		case "j": doKeyPressShading(true, 373, homeRowHeight, false, false, false, 5); break;
		case "J": doKeyPressShading(true, 373, homeRowHeight, false, true, false, 15); break;
		case "k": doKeyPressShading(true, 418, homeRowHeight, false, false, false, 6); break;
		case "K": doKeyPressShading(true, 418, homeRowHeight, false, true, false, 16); break;
		case "l": doKeyPressShading(true, 463, homeRowHeight, false, false, false, 7); break;
		case "L": doKeyPressShading(true, 463, homeRowHeight, false, true, false, 17); break;
		case "m": doKeyPressShading(true, 394, botRowHeight, false, false, false, 5); break;
		case "M": doKeyPressShading(true, 394, botRowHeight, false, true, false, 15); break;
		case "n": doKeyPressShading(true, 350, botRowHeight, false, false, false, 5); break;
		case "N": doKeyPressShading(true, 350, botRowHeight, false, true, false, 15); break;
		case "o": doKeyPressShading(true, 451, topRowHeight, false, false, false, 7); break;
		case "O": doKeyPressShading(true, 451, topRowHeight, false, true, false, 17); break;
		case "p": doKeyPressShading(true, 496, topRowHeight, false, false, false, 8); break;
		case "P": doKeyPressShading(true, 496, topRowHeight, false, true, false, 18); break;
		case "q": doKeyPressShading(true, 97, topRowHeight, false, false, false, 1); break;
		case "Q": doKeyPressShading(true, 97, topRowHeight, false, false, true, 18); break;
		case "r": doKeyPressShading(true, 229, topRowHeight, false, false, false, 4); break;
		case "R": doKeyPressShading(true, 229, topRowHeight, false, false, true, 48); break;
		case "s": doKeyPressShading(true, 153, homeRowHeight, false, false, false, 2); break;
		case "S": doKeyPressShading(true, 153, homeRowHeight, false, false, true, 28); break;
		case "t": doKeyPressShading(true, 273, topRowHeight, false, false, false, 4); break;
		case "T": doKeyPressShading(true, 273, topRowHeight, false, false, true, 48); break;
		case "u": doKeyPressShading(true, 361, topRowHeight, false, false, false, 5); break;
		case "U": doKeyPressShading(true, 361, topRowHeight, false, true, false, 15); break;
		case "v": doKeyPressShading(true, 262, botRowHeight, false, false, false, 4); break;
		case "V": doKeyPressShading(true, 262, botRowHeight, false, false, true, 48); break;
		case "w": doKeyPressShading(true, 141, topRowHeight, false, false, false, 2); break;
		case "W": doKeyPressShading(true, 141, topRowHeight, false, false, true, 28); break;
		case "x": doKeyPressShading(true, 174, botRowHeight, false, false, false, 2); break;
		case "X": doKeyPressShading(true, 174, botRowHeight, false, false, true, 28); break;
		case "y": doKeyPressShading(true, 317, topRowHeight, false, false, false, 5); break;
		case "Y": doKeyPressShading(true, 317, topRowHeight, false, true, false, 15); break;
		case "z": doKeyPressShading(true, 130, botRowHeight, false, false, false, 1); break;
		case "Z": doKeyPressShading(true, 130, botRowHeight, false, false, true, 18); break;
		case ";": doKeyPressShading(true, 508, homeRowHeight, false, false, false, 8); break;
		case ",": doKeyPressShading(true, 439, botRowHeight, false, false, false, 6); break;
		case ".": doKeyPressShading(true, 484, botRowHeight, false, false, false, 7); break;
		case "\"": doKeyPressShading(true, 553, homeRowHeight, false, true, false, 18); break;
		case "'": doKeyPressShading(true, 553, homeRowHeight, false, false, false, 8); break;
		case " ": doKeyPressShading(false, 0, 0, true, false, false, 9); break;
		case "!": doKeyPressShading(true, 75, numRowHeight, false, false, true, 18); break;
		case "?": doKeyPressShading(true, 529, botRowHeight, false, true, false, 18); break;
		default: doKeyPressShading(false, 0, 0, false, false, false, 0);
	}
 }
 
 // This function is called to position and show/hide certain key shading divs
 function doKeyPressShading(kPBool, kpLeft, kpTop, spaceBool, L_ShiftBool, R_ShiftBool, fingersToUse){
	if(kPBool){		
		$("#keyToPress").show(); 
	}
	else{
		$("#keyToPress").hide();
	}
	
	if(spaceBool){		
		$("#spaceToPress").show(); 
	}
	else{
		$("#spaceToPress").hide();
	}
	$("#keyToPress").css("margin-left", kpLeft);
	$("#keyToPress").css("margin-top", kpTop);
	
	if(L_ShiftBool){		
		$("#L_ShiftToPress").show(); 
		$("#R_ShiftToPress").hide();
	}
	else if(R_ShiftBool){
		$("#R_ShiftToPress").show();
		$("#L_ShiftToPress").hide();
	}
	else{
		$("#L_ShiftToPress").hide();
		$("#R_ShiftToPress").hide();
	}
	
	var L_Pink = "rect(42px 40px 100px 0px)";
	var L_Ring = "rect(0px 62px 77px 36px)";
	var L_Mid = "rect(0px 81px 100px 62px)";
	var L_Index = "rect(0px 120px 100px 80px)";
	var R_Index = "rect(0px 73px 80px 35px)";
	var R_Mid = "rect(0px 95px 76px 69px)";
	var R_Ring = "rect(0px 118px 76px 92px)";
	var R_Pink = "rect(40px 150px 100px 115px)";
	var L_Thumb = "rect(80px 162px 134px 95px)";
	var R_Thumb = "rect(80px 55px 150px 0px)";
	var noFinger = "rect(0px 0px 0px 0px)";
	
	switch(fingersToUse){
		case 1:	$("#L_FingerToPress").css("clip", L_Pink); $("#R_FingerToPress").css("clip", noFinger);	break;
		case 2:	$("#L_FingerToPress").css("clip", L_Ring); $("#R_FingerToPress").css("clip", noFinger); break;
		case 3:	$("#L_FingerToPress").css("clip", L_Mid); $("#R_FingerToPress").css("clip", noFinger); break;
		case 4:	$("#L_FingerToPress").css("clip", L_Index); $("#R_FingerToPress").css("clip", noFinger); break;
		case 5:	$("#L_FingerToPress").css("clip", noFinger); $("#R_FingerToPress").css("clip", R_Index); break;
		case 6:	$("#L_FingerToPress").css("clip", noFinger); $("#R_FingerToPress").css("clip", R_Mid); break;
		case 7:	$("#L_FingerToPress").css("clip", noFinger); $("#R_FingerToPress").css("clip", R_Ring); break;
		case 8:	$("#L_FingerToPress").css("clip", noFinger); $("#R_FingerToPress").css("clip", R_Pink);	break;
		case 9:	$("#L_FingerToPress").css("clip", L_Thumb);	$("#R_FingerToPress").css("clip", R_Thumb);	break;
		case 15: $("#L_FingerToPress").css("clip", L_Pink); $("#R_FingerToPress").css("clip", R_Index); break;
		case 16: $("#L_FingerToPress").css("clip", L_Pink); $("#R_FingerToPress").css("clip", R_Mid); break;
		case 17: $("#L_FingerToPress").css("clip", L_Pink); $("#R_FingerToPress").css("clip", R_Ring); break;
		case 18: // 18 = 1 + 8 = left pinkie + right pinkie
			$("#L_FingerToPress").css("clip", L_Pink); $("#R_FingerToPress").css("clip", R_Pink); break;
		case 28: $("#L_FingerToPress").css("clip", L_Ring); $("#R_FingerToPress").css("clip", R_Pink); break;
		case 38: $("#L_FingerToPress").css("clip", L_Mid); $("#R_FingerToPress").css("clip", R_Pink); break;
		case 48: $("#L_FingerToPress").css("clip", L_Index); $("#R_FingerToPress").css("clip", R_Pink); break;
		default: $("#L_FingerToPress").css("clip", noFinger); $("#R_FingerToPress").css("clip", noFinger);
		}
 }

function ApplyColorsBlurred(){
    var e = new Array;
    var i = 0;
    var fs = false;
    while ((i < document.styleSheets.length) && !fs) {
        if (document.styleSheets[i].cssRules && (findCSSRule("span.goodEntry", document.styleSheets[i].cssRules) >= 0)) {
            e = document.styleSheets[i].cssRules;
            fs = true;
        } else if (document.styleSheets[i].rules && (findCSSRule("span.goodEntry", document.styleSheets[i].rules) >= 0)) {
            e = document.styleSheets[i].rules;
            fs = true;
        }
        i++
    }
    if (fs) {
        e[findCSSRule('span.nxtLetter',e)].style.color = $("#PTFC").val(); 
		e[findCSSRule('span.nxtLetter',e)].style.backgroundColor = $("#PTBack").val();
    }
}

function ApplyColorsFocused(){
    var e = new Array;
    var i = 0;
    var fs = false;
    while ((i < document.styleSheets.length) && !fs) {
        if (document.styleSheets[i].cssRules && (findCSSRule("span.goodEntry", document.styleSheets[i].cssRules) >= 0)) {
            e = document.styleSheets[i].cssRules;
            fs = true;
        } else if (document.styleSheets[i].rules && (findCSSRule("span.goodEntry", document.styleSheets[i].rules) >= 0)) {
            e = document.styleSheets[i].rules;
            fs = true;
        }
        i++
    }
    if (fs) {
        e[findCSSRule('span.nxtLetter',e)].style.color = $("#CFC").val(); 
		e[findCSSRule('span.nxtLetter',e)].style.backgroundColor = $("#CBack").val();
    }
}
