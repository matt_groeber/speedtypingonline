<?php
class User{
	private $_db,
			$_data,
			$_sessionName,
			$_cookieName,
			$_isLoggedIn,
			$_isPremium,
			$_isPendingPremium,
			$_isPendingCancelPremium,
			$_isPendingReactivatePremium;
	
	public function __construct($user = null){
		$this->_db = DB::getInstance();
		
		$this->_sessionName = Config::get('session/session_name');
		$this->_cookieName = Config::get('remember/cookie_name');

		if(!$user) {
			if(Session::exists($this->_sessionName)){
				$user = Session::get($this->_sessionName);

				if($this->find($user)){
					$this->_isLoggedIn = true;
					//var_dump($this->_data);
					//exit();
					if(isset($this->_data->sub_status))
						$this->updatePremiumStatus();
				} else {
					// MPG LATER
				}
				
				if($this->_data->premium == 1){
					$this->_isPremium = true;
				} else if($this->_data->premium == 2){
					$this->_isPendingPremium = true;
				} else if($this->_data->premium == 3){
					$this->_isPendingCancelPremium = true;
				} else if($this->_data->premium == 4){
					$this->_isPendingReactivatePremium = true;
				}
			} else {
				if(!isset($this->_data)){
					$this->_data = new \stdClass();
				}
				
				$this->_data->id = null;
				$this->_data->username = null;
				$this->_data->password = null;
				$this->_data->firstname = '';
				$this->_data->lastname = '';
				$this->_data->email = null;
				$this->_data->email_code = null;
				$this->_data->allow_email = null;
				$this->_data->premium = '0';
				$this->_data->picture = '';
				$this->_data->active = null;
				$this->_data->social_login = '0';
				$this->_data->pw_recover = null;
				$this->_data->joined = null;
				$this->_data->last_login = null;
				$this->_data->group = '5';
				$this->_data->flagged = '0';		// bitwise 
													// 0x0001 - auto flag inappropriate words in username
													// 0x0010 - manual flag inappropriate username
													// 0x0100 - auto flag game cheater
													// 0x1000 - manual flag game cheater
				$this->_data->user_id = null;
				$this->_data->input_mode = '';
				$this->_data->tt_keyboard_layout = '0';
				$this->_data->tt_text_type = '';
				$this->_data->tt_text_title = '0';
				$this->_data->tt_length = '60';
				$this->_data->tt_double_spacing = '0';
				$this->_data->tt_color_text = '1';
				$this->_data->tt_phase_shift = '1';
				$this->_data->tl_wpm_target = '10';
				$this->_data->tl_accuracy_target = '90.0';
				$this->_data->tl_length = '140';
				$this->_data->tl_auto_track_targets = '1';
				$this->_data->tl_date_filter = '4';
				$this->_data->auto_email = null;
				$this->_isLoggedIn = false;
				$this->_isPremium = false;
				$this->_isPendingPremium = false;
				$this->_isPendingCancelPremium = false;
				$this->_isPendingReactivatePremium = false;
			}
		} else {
			$this->find($user);
			
			if($this->_data->premium == 1){
				$this->_isPremium = true;
			} else if($this->_data->premium == 2){
				$this->_isPendingPremium = true;
			} else if($this->_data->premium == 3){
				$this->_isPendingCancelPremium = true;
			} else if($this->_data->premium == 4){
				$this->_isPendingReactivatePremium = true;
			}
		}
	}
	
	public function update($fields = array(), $id = null, $table = 'users', $idColName = 'id'){
		
		if(!$id && $this->isLoggedIn()){
			$id = $this->data()->id;
		}
	
		if(!$this->_db->update($table, $id, $fields, $idColName)){
			throw new Exception('There was a problem updating.');
		}
	}
	
	public function updateSettings($fields = array(), $id = null, $table = 'user_settings', $idColName = 'user_id'){
		$this->update($fields, $id, $table, $idColName);
	}
	
	public function flag_user($id = null){
		$this->update(array('flagged', 1), $id);
	}
	
	public function create($fields = array()){
		if(!$this->_db->insert('users', $fields)){
			throw new Exception('There was a problem creating your account.');
		} else {
			$newUserID = $this->_db->getLastInsertID();
			if(!$this->_db->insert('user_settings', array('user_id' => $newUserID))){
				throw new Exception('There was a problem creating your account settings.');
			} 
			
			if(!$this->_db->insert('users_achieve', array('user_id' => $newUserID))){
				throw new Exception('There was a problem creating your account achievements.');
			}
		}
	}
	
	public function find($user = null){
		if($user) {
			//$field = (is_numeric($user))? 'id' : 'username';		// we might need to validate usernames to being alphanumeric
			if(is_numeric($user)){
				$field = 'id';
			} else if (filter_var($user, FILTER_VALIDATE_EMAIL)){
				$field = 'email';
			} else {
				$field = 'username';
			}
			$data = $this->_db->get('`users`', array('users.' . $field, '=', $user), null, null, array('user_settings', 'id', 'user_id','LEFT'));

			if($data->count()){
				$userdata = $data->first();

				if($userdata->premium != 0){
					$sub_obj = new Subscription();
					$sub_arr = $sub_obj->GetSubscriptionStatus($userdata->id);

					$userdata->sub_status = $sub_arr[0];
					$userdata->sub_end_date = $sub_arr[1];
					
					if($userdata->premium == 4 && $sub_arr[0] == 'CANCELLED'){
						error_log("----------------------------------------");
						error_log("User is premium but cancelled sub! - Maybe add code here");
						error_log("----------------------------------------");
					}
				}
				
				$this->_data = $userdata;
				return true;
			}
		}
		return false;
	}
	
	public function get_user($field, $value, $cols = null, $orderBy = null){
		//$data = $this->_db->get('users', array($field, '=', $value), $cols, $orderBy, array('user_settings', 'id', 'user_id'));
		$data = $this->_db->get('users', array($field, '=', $value), $cols, $orderBy);
		
		if($data->count()){
			return $data->results();
		} else {
			return '';
		}
	}
	
	public function get_users($field, $value, $cols = null, $orderBy = null){
		//$data = $this->_db->get('users', array($field, 'IN', $value), $cols, $orderBy, array('user_settings', 'id', 'user_id'));
		$data = $this->_db->get('users', array($field, 'IN', $value), $cols, $orderBy);
		
		if($data->count()){
			return $data->results();
		} else {
			return '';
		}
	}
	
	
	public function get_user_settings(){
		$data = $this->_db->get('user_settings', array($field, 'IN', $value), $cols, $orderBy);
		
		if($data->count()){
			return $data->results();
		} else {
			return '';
		}
	}
	
	private function do_login($remember){
		Session::put($this->_sessionName, $this->data()->id);
		$this->update(array('last_login' => gmdate('Y-m-d H:i:s')), $this->data()->id);
		
		if($remember) {
			$hash = Hash::unique();
			$hashCheck = $this->_db->get('users_session', array('user_id', '=', $this->data()->id));
			
			if(!$hashCheck->count()) {
				$this->_db->insert('users_session', array(
					'user_id' => $this->data()->id,
					'hash' => $hash
				));
			} else {
				$hash = $hashCheck->first()->hash;
			}
			
			Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));
		}
	}
	
	public function login($username = null, $password  = null, $remember = false){
	
		if(!$username && !$password && $this->exists()){
			Session::put($this->_sessionName, $this->data()->id);
		} else {
			$user = $this->find($username);
			$userActive = $this->data()->active;
			
			if($userActive === '0' || $userActive === 0){
				return 'Inactive User';
			} else if($user){
				if(password_verify($password, $this->data()->password)){
					Session::put($this->_sessionName, $this->data()->id);
					$this->update(array('last_login' => gmdate('Y-m-d H:i:s')), $this->data()->id);
					
					if($remember) {
						$hash = Hash::unique();
						$hashCheck = $this->_db->get('users_session', array('user_id', '=', $this->data()->id));
						
						if(!$hashCheck->count()) {
							$this->_db->insert('users_session', array(
								'user_id' => $this->data()->id,
								'hash' => $hash
							));
						} else {
							$hash = $hashCheck->first()->hash;
						}
						
						Cookie::put($this->_cookieName, $hash, Config::get('remember/cookie_expiry'));
					}
					
					return true;
				}
			}
		}
		return false;
	}
	
	public function google_login(){
		$this->do_login(true);
	}
	
	// User::email
	// Inputs: (1) array of email strings, (2) email subject string, (3) email body string
	// Sends email with subject (2) and body (3) to first email in (1) array, rest of emails
	// in (1) array get added as BCC
	public function email($emails, $subject = "", $body = ""/*, $altBody*/){

		require_once '/home/matt/www/classes/PHPMailer/PHPMailerAutoload.php';
		
		$mail = new PHPMailer;

		$mail->IsSMTP();                                      				// Set mailer to use SMTP
		//$mail->SMTPDebug = 2;												// set this for debugging results
		$mail->Host = 'in-v3.mailjet.com'; //'smtp.gmail.com';										// Specify main and backup server	//'smtp.mandrillapp.com';                 
		$mail->SMTPSecure = 'ssl';
		$mail->Port = 465;  //   587                              			 // Set the SMTP port
		$mail->SMTPAuth = true;                               				// Enable SMTP authentication
		$mail->Username = '5e29d6d6b642af331e159c08aac5916a';//'speedtypingonline@gmail.com';           			// SMTP username
		$mail->Password = 'a0b5c04607c59baf04f41e3addb42a81';//'johnfradel';//'MSxRoRoBRGRKZSrcdPtxLQ';           // SMTP password
		//$mail->SMTPSecure = 'tls';                            			// Enable encryption, 'ssl' also accepted

		// MPG LATER - update from
		$mail->From = 'admin@speedtypingonline.com';//'speedtypingonline@gmail.com';
		$mail->FromName = 'Speed Typing Online';
		
		// get first email address
		if(is_array($emails)){
			$firstEmail = array_shift($emails);
		} else {
			$firstEmail = $emails;
		}
		
		// TO:
		$mail->AddAddress($firstEmail);			// MPG LATER - add 'name' as second parameter

		// BBC:
		if(!empty($emails)){
			foreach($emails as $email){
				$mail->AddBCC($email);
			}
		}
		
			
		$mail->IsHTML(true);                                  // Set email format to HTML
		$mail->Subject = $subject;
		$mail->Body    = $body;
		
		// MPG LATER - $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
		// add unsubscribe link? wasn't working... - $mail->addCustomHeader("List-Unsubscribe",'<matt@speedtypingonline.com>, <https://learnspeedtyping.com/recover?email='.$address.'>');
		
		if(!$mail->Send()) {
		   return array(false, $mail->ErrorInfo);
		}
		

		return array(true, 'Message has been sent to "' . $firstEmail . '"');
	}
	
	public function hasPermission($key){
		$group = $this->_db->get('`groups`', array('id', '=', $this->data()->group));
		
		if($group->count()){
			$permissions = json_decode($group->first()->permissions, true);
			
			if(is_array($permissions)){
				if($permissions[$key] == true){
					return true;
				}
			}
		}
		return false;
	}
	
	public function fullName(){
		$fullName = '';
		$spacer = '';
		
		if($this->_data->firstname != ''){
			$fullName = $this->_data->firstname;
			$spacer = ' ';
		}
		
		if($this->_data->lastname != ''){
			$fullName .= $spacer . $this->_data->lastname;
		}
		
		return $fullName;
	}
	
	public function groupDisplayName($groupID = null){
		
		if(!$groupID){
			$groupID = $this->data()->group;
		}
		
		return $this->_db->get('`groups`', array('id', '=', $groupID))->first()->display_name;
	}
	
	public function exists(){
		return (!empty($this->_data)) ? true : false;
	}
	
	public function logout() {
		$this->_db->delete('users_session', array('user_id', '=', $this->data()->id));
		
		unset($_SESSION['access_token']);
		
		Session::delete($this->_sessionName);
		Cookie::delete($this->_cookieName);
	}
	
	public function data(){
		return $this->_data;
	}
	
	public function isLoggedIn(){
		return $this->_isLoggedIn;
	}
	
	public function updatePremiumStatus(){
		$servertime = new DateTime();
		$subenddate = new DateTime($this->_data->sub_end_date);
		$subenddate->add(new DateInterval('P1D'));	// add small buffer (max 24h) to allow users with trial time for payment to go next day at 10am
		
		// use sub_end_date as the ultimate sub cancellation premium or not flag
		if($servertime > $subenddate && $subenddate->format('Y-m-d H:i:s') != '-0001-12-01 00:00:00'){
			error_log('**************       servertime is > subenddate       ********************');
			$sub = new Subscription();
			$sub->SuspendOrCancelSubscriptionByUserId($this->data()->id, true);
			return $this->update(array('premium' => 0));
		} else {
			if($this->_data->premium == 0)
				return $this->update(array('premium' => 1));
			else
				return false;
		}
		
	}
	
	public function isPremium(){
		return $this->_isPremium || $this->_isPendingCancelPremium || $this->_isPendingReactivatePremium;
	}
	
	public function isPendingPremium(){
		return $this->_isPendingPremium;
	}
	
	public function isPendingCancelPremium(){
		return $this->_isPendingCancelPremium;
	}
	
	public function isPendingReactivatePremium(){
		return $this->_isPendingReactivatePremium;
	}
	
	public function isTeacher(){
		return $this->_data->group == 3;
	}
	
	public function isStudent(){
		return $this->_data->group == 4;
	}
	
	public function isCheater_auto(){
		return ($this->_data->flagged & 0b00100) != 0;
	}
	
	public function isCheater(){
		$autoCheater = ($this->_data->flagged & 0b00100) != 0;
		$manualCheater = ($this->_data->flagged & 0b01000) != 0;
		return $autoCheater || $manualCheater;
	}
	
	public function Cheated(){
		if($this->isLoggedIn()){
			
			// update user to auto cheater if not already
			if(!$this->isCheater_auto()){
				$newFlagVal = $this->_data->flagged | 0b00100;
				$this->_db->update('users', $this->data()->id, array('flagged' => $newFlagVal), 'id');
			}
			//error_log('USER HAS CHEATED!!!');
		}
	}
	
	public function activeUserCount(){
		return $this->_db->count($this->_db->get('users', array('active', '=', 1)));
	}
}

?>