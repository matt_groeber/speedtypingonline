<?php 
$currPage = "typingTutor";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$htmlTitle = "Free Online Typing Tutor";
$htmlDescription = "Free online typing tutor! Learn touch typing fast using these free typing lessons. Multiple learning methods, and custom lesson. Set target speed and accuracy.";
$cssFiles = "aghf";
$noWidgets = true;
$jQueryUI = true;
$fontAwesomeCDN = true;
require_once 'includes/overall/header.php';

$user = new User();
$userData = $user->data();
$debugMode = $user->hasPermission('debug') || Config::get('constants/debugMode');

$retArr = getTutorData($userData);
$classicLessonsHTML = $retArr['classicLessonHTML'];
$advancedLessonsHTML = $retArr['advancedLessonHTML'];
$lessons = $retArr['lessons'];
$lessonsWithoutHistory = $retArr['lessonsWithoutFullHistory'];

$lessonWPMtarget = ($userData->tl_wpm_target == '') ? Config::get('constants/wpmLessonTarget') : $userData->tl_wpm_target;
$lessonAccTarget = ($userData->tl_accuracy_target == '') ? Config::get('constants/accLessonTarget') : trimTrailingZeroes($userData->tl_accuracy_target);

//$lessonIdIndex = getLessonIdIndexFromMod($lessons, Input::get('mod'));
//var_dump($lessons[$lessonIdIndex->index]);
//var_dump($lessonIdIndex->id);
//var_dump($retArr['lessons'][51]);
//exit();

if(!$lessonModule = Input::get('mod')){
	$lessonModule = 2;
} 
$lessonIdIndex = getLessonIdIndexFromMod($lessonsWithoutHistory, $lessonModule);
$lessonId = $lessonIdIndex->id;
$lessonIndex = $lessonIdIndex->index;
$prevLessonId = $lessonIdIndex->prevId;
$prevLessonIndex = $lessonIdIndex->prevIndex;

if($lessonIdIndex == null){
	if($lessonModule > count($lessonsWithoutHistory)){
		// set to last lesson(review) module
		$lessonId = 65;
		$lessonIndex = 49;
		$prevLessonId = 45;
		$prevLessonIndex = 48;
	} else{
		// set to first lesson module
		$lessonId = 1;
		$lessonIndex = 2;
		$prevLessonId = 84;
		$prevLessonIndex = 1;
	}
}

//echo '<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />';
//var_dump($lessons);
//var_dump($lessonIdIndex);
//exit();

if($lessonIdIndex->index < sizeof($lessons)){
	$lessonType = $lessons[$lessonIdIndex->index]->type;
}

if ($userData->input_mode == "INLINE"){
	$inputMode['BLOCK'] = ' style="display:none;"';
	$inputMode['INLINE'] = '';
	$divCoverStyle = ' style="z-index: 0; height:60px; margin-top:50px;"';
	$divGlowStyle = ' style="height:60px; width:668px; top:48px; left:0px;"';
	
} else {
	$inputMode['BLOCK'] = '';
	$inputMode['INLINE'] = ' style="display:none;"';
	$divCoverStyle = ' style="z-index: 0; height:160px; margin-top:-1px;"';
	$divGlowStyle = ' style="height:162px; width:668px; top:-2px; left:0px;"';
}

$lessonLength = array('', '', '', '', '');

if($userData->tl_length == 70){
	$lessonLength[0] = ' selected="selected"';
} else if($userData->tl_length == 280){
	$lessonLength[2] = ' selected="selected"';
} else if($userData->tl_length == 560){
	$lessonLength[3] = ' selected="selected"';
} else if($userData->tl_length == 1 && $debugMode){
	$lessonLength[4] = ' selected="selected"';
} else {
	// default normal length = 140
	$lessonLength[1] = ' selected="selected"';
}

?>
<div id="widthDiv"></div>
<div id="container">
	<div id="rightBar">
		<?php echo getAd("rightSky", $currPage, $debugMode, $user->isPremium());?>
	</div>

	<div id="mainBody">
		<div id="mainBodyContent">
			<div id="main">
				<div id="wrapHeading">
					<div id="heading">
						<h1>Free Online Typing Tutor</h1>
						<span>
							<h3><a href="#how_to">Learning »</a></h3>
							<h3><a href="#feature_jump">Features »</a></h3>
						</span>
					</div>
					
				</div>
				<div id="lessonDivContainer">
					<div id="lessonDiv">
						<div id="lessonDivLead">
							<span id="textPurposeDiv"></span>
							<span id="lessonPurposeDisplay" class="textInfoDisplay"></span>
						</div>
						<span id="lessonLettersWrap" class="lessonDivCols">
						   <div id="LL_title" class="LI_title">Learn</div>
						   <div id="lessonLettersToLearn" class="textInfoDisplay" ></div>
						</span>
						<span class="lessonDivCols">
							<div class="LI_title">Hint(s)</div>
							<div id="lessonHintDisplay" class="textInfoDisplay"></div>
						</span>
						<span id="lessonTargetsWrap" class="lessonDivCols" <?php echo ($user->isLoggedIn()) ? '' : 'style="display:none;"';?>>
							<span class="LI_title">Targets <i class="fa fa-pencil" aria-hidden="true"></i></span>
							<div class="textInfoDisplay">
								<div>
									<span id="tl_wpm_target" class="targetField"><?php echo $lessonWPMtarget; ?></span>
									<span class="note"> wpm</span>
								</div>
								<div>
									<span id="tl_accuracy_target" class="targetField"><?php echo $lessonAccTarget; ?></span>
									<span>%</span>
								</div>
							</div>
							<div id="targetEditDiv">
								<div class="posRel">
									<div id="editTargetsPopup">
										<span class="LI_title">Targets</span>
										<div class="editField">
											<label for="targetWPMspinner">Speed (wpm)</label>
											<input id="targetWPMspinner" class="editValue" name="targetWPM" value="<?php echo $lessonWPMtarget; ?>">
										</div>
										<div class="editField">
											<label for="targetAccuracyspinner">Accuracy (%)</label>
											<input id="targetAccuracyspinner" class="editValue" name="targetAccuracy" value="<?php echo $lessonAccTarget; ?>">
										</div>
									</div>
								</div>
							</div>
						</span>
					</div>
<?php
	include 'includes/profileBlurb.php';
?>
					<div id="prevLessonDiv" class="moveLessonCarets">
						<span id="prevLessonLink">‹</span>
					</div>
					<div id="nextLessonDiv" class="moveLessonCarets">
						<span id="nextLessonLink">›</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="centerConsoleWrap">
		<div id="replayBtn" class="pBtn">Replay <i class="fa fa-refresh" aria-hidden="true"></i></div>
		<div id="nextBtn" class="pBtn">Next <i class="fa fa-arrow-right" aria-hidden="true"></i></div>
		<div id="closeBtn" class="pBtn"><a href="#">Close <i class="fa fa-times" aria-hidden="true"></i></a></div>
		<div id="centerConsole">
			<?php 
				echo getAd("lead", $currPage, $debugMode, $user->isPremium());
				if(!$user->isLoggedIn()){
			?>
				<div id="loginOrRegister">
					<div id="askToLogin" class="stoInfoBox" onclick="location.href='/login.php';">
						<span>
							<a href="/login.php">Login</a> or <a href="/register.php">Register</a>
						</span>
						<span> to track progress and set targets.</span>
					</div>
				</div>
			<?php	
				}
			?>
			<div id="centerContent">
				<div id="divBackWrap">
					<div id="divBackTop"></div>
					<div id="divBackStripe"><div></div></div>
					<div id="divBackBottom"></div>
				</div>
				
				<div id='wrapStats'>
					<div id='timerDiv'>   
						<div id="timerText" class="statsText">Time</div>
						<div id='sWatch'>00:00</div>
					</div>
					<div id='wpmDiv'>                
						<div id='wpmText' class="statsText">Percent Complete</div>
						<div id='pCompleteValue'>0 <span>%</span></div>
					</div>
					<div id='accuracyDiv'> 
						<div id='accuracyText' class="statsText">Accuracy</div>
						<div id='accuracyValue'>100 <span>%</span></div>
					</div>
				</div>
				
				<div id="containerWrapper">
					<div id="blockDivGlow"<?php echo $divGlowStyle;?>></div>
					<div id="divCover"<?php echo $divCoverStyle;?>></div>
					<div id="lineDivContainer" class="mainDivInputs" tabindex="0" onmouseover="this.focus()"<?php echo $inputMode['INLINE'];?>></div>
					<div id="blockDivContainer" class="mainDivInputs" tabindex="0" onmouseover="this.focus()"<?php echo $inputMode['BLOCK'];?>></div>
				</div>
				
				<div id="buttonWrapper">
					<div id="resetBtnWrap">
						<div id="resetBtnDiv">
							<input type="button" name="ResetBtn" value="Reset" id="resetBtn"
							onclick="Reset(0,'sWatch')">
						</div>
					</div>
				
					<div id="switchModesBtnWrap">
						<div id="switchModesBtnDiv">
							<input type="button" name="SwitchModesBtn" value="Switch-Up" id="switchModesBtn">
						</div>
					</div>
					
					<?php 
						
						$nameToPrint = "<Name>";
						$personalName = '<Name>';
						
						if($user->isLoggedIn()){
							if(!empty($userData->firstname) || !empty($userData->lastname)){
								$nameToPrint = $user->fullName() . '\'s ';
								$personalName = $user->fullName();
							}
							else{
								$nameToPrint = $userData->username . '\'s ';
							}
						}
						
					?>
					
					<div id="PRIW" style="display: none">
						<form>
							<input type="text" id="personName" maxlength="22" value="<?php echo $personalName?>">
						</form>
						<button type="button" id="PN_Button" class="mediumBtn submitButtonCarrot">Personalize Results</button>	
					</div>
				</div>
				
				<div id="resultDivContainer" class="mainDivInputs" style="display: none; ">
					<div id="resultDiv">
						<div id="resultsTitle">
							<?php   echo $nameToPrint;?>Lesson Results
						</div>
							<div id="resultsCol1Wrap" class="iResults">

								<div id="WPM_Rtext" 
									 title="<strong>Raw speed with mistake penalties</strong><br /> Speed = Raw speed - Error Rate <br /> (each mistake left in per minute is -1 WPM)" 
									 class="resultStatRight">Speed: </div>
								<div id="Accur_Rtext" 
									 title="<strong>The percentage of correct <br />letters out of the total letters typed:</strong><br />Accuracy = (Correct Letters / Total Letters) x 100%" 
									 class="resultStatRight">Accuracy: </div>
							</div>
						<div id="resultsCol2Wrap" class="iResults">
							<div id="WPM_Result">0</div>
							<div id="Accur_Result">0</div>
						</div>
						<div id="resultsCol3Wrap" class="uiResults">
							<div id="LTC_Rtext" 
								 title="<strong>Total correct letters typed </strong><br />(even those deleted and re-entered)" 
								 class="resultStatLeft">Correct Letters: </div>
							<div id="nLTI_Rtext" 
								 title="<strong>Total letters skipped or typed incorrectly </strong><br />(even if deleted and corrected later)" 
								 class="resultStatLeft">Incorrect Letters: </div>
							<div id="MF_Rtext"
								 title="<strong>Number of incorrect entries that were fixed </strong><br /> (number of mistakes deleted and entered correctly)"
								 class="resultStatLeft">Fixed Mistakes: </div>
							<div id="TLT_Rtext"
								 title="<strong>All letters typed</strong> <br />Total Letters = Correct Letters + Incorrect Letters + Fixed Mistakes"
								 class="resultStatLeft">Total Letters: </div>
							<div id="ER_Rtext"
								 title="<strong>Number of incorrect letters<br /> (unfixed mistakes) per minute </strong><br /> (Error Rate = Incorrect Letters / Time in Minutes)"
								 class="resultStatLeft">Error Rate: </div>
							<div id="rWPM_Rtext"
								 title="<strong>Typing Speed before mistake penalties </strong><br /> Raw Speed = ( Total Letters / Time in minutes ) / 5 <br /> (where 5 = average length of a word)"
								 class="resultStatLeft">Raw Speed: </div>
							<div id="KPM_Rtext"
								 title="<strong>Number of keystrokes per minute </strong><br /> Key Speed = Total Letters / Time in minutes"
								 class="resultStatLeft">Key Speed: </div>
							<div id="words_Rtext"
								 title="<strong>Number of complete words typed </strong><br /> ('the' and 'messenger' are both one word)"
								 class="resultStatLeft">Complete Words: </div>
							<div id="time_Rtext"
								 title="<strong>Total time of round in seconds</strong><br /> (Time in minutes = Total Time / 60)"
								 class="resultStatLeft">Total Time: </div>
							 <div id="note_Rtext">*mouse over labels for more info</div>
						</div>
						<div id="resultsCol4Wrap" class="uiResults">
							<div id="LTC_Result">0</div>
							<div id="nLTI_Result">0</div>
							<div id="CL_Result">0</div>
							<div id="TLT_Result">0</div>
							<div id="ER_Result">0</div>
							<div id="rWPM_Result">0</div>
							<div id="KPM_Result">0</div>
							<div id="words_Result">0</div>
							<div id="time_Result">0</div>
						</div>
						<div id="snShare">
							<div id="fbWrap">
								<div id="fbLink">
									<a id="fbrLink" href="https://www.facebook.com/share.php?u=<?php echo Config::get('constants/rootUrl');?>/fbr.php" onClick="return popup(this, 'notes')" class="share_link">
										<div id="fbIcon" class="share_icon"></div>
										<span>Share Results on Facebook</span>
									</a>
								</div>
							</div>
							<div id="tWrap">
								<div id="tLink">
									<a id="twtrLink" href="https://twitter.com/home" onClick="return popup(this, 'notes')" class="share_link">
									   <div id="tIcon" class="share_icon"></div>
									   <span>Share Results on Twitter</span>
									</a>
								</div>
							</div>
							<div id="eWrap">
								<div id="eLink">
									<a id="genLinkEmail" class="share_link" title="Don't forget to personalize your results first!" href="mailto:example@domain.com" target="_blank">
										<div id="eIcon" class="share_icon"></div>
										<span>Email Results</span>
									</a>
								</div>
							</div>
							<div id="lWrap">
								<div id="lLink">
									<a id="genLink" href="/getResults.php" target="_blank" class="share_link" title="Don't forget to personalize your results first!">
										<div id="lIcon" class="share_icon"></div>
										<span>Link to Results:</span>
									</a>
									<input id="genLinkDisplay" title="Don't forget to personalize your results first!" value="" onClick="this.select();">
								</div>
							</div>
						</div>
					</div>	<!-- #resultDiv -->
				</div>	<!-- resultDivContainer -->
			</div>	<!-- end centerContent -->

			<div id="KHWrap">
			<div id="kbWrap">
				<div id="kbDiv">
					<div id="keysFilled"></div>
					<div id="keysShading"></div>
					<div id="keyToPress" style="display: none; "></div>
					<div id="spaceToPress" style="display: none; "></div>
					<div id="L_ShiftToPress" style="display: none; "></div>
					<div id="R_ShiftToPress" style="display: none; "></div>
					<div id="kbLegend">Home/Starting Position Keys</div>
				</div>
			
			
			<div id="keyLabelWrap">
				<div id="klDiv">
					<div id="keysLabels"></div>
				</div>
			</div>
			
			</div>
			<span id="how_to"></span>
			<div id="HandWrap">
				<div id="handDiv">
					<div id="L_HandWrap">
						<div id="L_HandOutline"></div>
						<div id="L_FingerToPress"></div>
					</div>
					<div id="R_HandWrap">
						<div id="R_HandOutline"></div>
						<div id="R_FingerToPress"></div>
					</div>
					<div id="BigHands">
						<div id="BigHandsImg">
							<div id="BigHandClick"></div>
							<div id="clickImg"></div>
						</div>
						
					</div>
				</div>
			</div>
			<div id="bottomTutText"></div>
		</div>
		
		</div> 	<!-- end centerConsole -->
		
	</div>	<!-- centerConsoleWrap -->
	<div id="bottomInfo">
		<div class="stoFancyList">
			<div><h2>How to Learn to Type:</h2></div>
				<div class="grid grid-pad">
					<div class="col-1-2">
						<ul class="arrowList">
							<li>
								<h3>No looking at your Keyboard!</h3>
								<div>
									<p>This is important - <span class="boldFont">don't do it!</span></p>
									<p>Touch typing is a skill that uses muscle memory to know where the keys are without the sense of sight. <span class="specialFont">You can't learn to swim without getting wet; likewise, you can't learn to touch type by looking down at the keyboard.</span> It might be hard at first but hang in there, and in no time it will become so natural you'll forget the keyboard is even there!</p>
									<p><span class="boldFont">If you have trouble finding a key resist the urge to look down.</span> Instead, use the onscreen virtual keyboard with orange highlights telling you where the key is and what finger to use. Put a towel over your hands if you still can't resist.
								</div>
							</li>
						</ul>
					</div>
					<div class="col-1-2">
						<ul class="arrowList">
							<li>
								<h3>Technique and Accuracy First</h3>
								<div>
									<p>Concentrate on correct form (using the right fingering, etc.) and accuracy above all else.</p>
									<p>Speed will be guaranteed through good technique and will come in time by practicing often. Proceeding with an incorrect technique will limit your potential speed in the future.
								</div>
							</li>
						</ul>
					</div>
					<span id="feature_jump"></span>
				</div>
			<div><h2>Typing Tutor Features</h2></div>
			<div class="grid grid-pad">
				<div class="col-1-2">			
					<ul class="checkList">
						<li><h3>Multiple Lesson Formats</h3>
							<p>Two different typing lesson formats give you more options to choose how you like to learn:</p>
							<h4>- Classic Lessons</h4> 
							<p class="doubleIndent">There is a reason why these repetitive lessons are so common: they work really well for those just starting to learn touch-typing.</p>
							<p class="doubleIndent">Typing repeated letters in a random pattern is the fastest way to teach your mind how to automatically know where the keys are without you having to actually think about it - the basics of touch-typing.<p>
							<p class="doubleIndent">For those just starting to learn touch-typing try these - they work!</p>
							<h4>- Advanced Lessons</h4> 
							<p class="doubleIndent">These lessons are for those who might already have a basic understanding of the keyboard and need a quicker refresher or maybe learn some of the rarer keys better.</p>
							<p class="doubleIndent">Advanced lessons are designed to move quicker through learning the keys while also introducing words instead of random letters.</p>
							<p>Try both types of lessons to find the one you like best. Or, for best results, complete both sets!</p>
						</li>
						<li>
							<h3>Multiple Lesson Lengths</h3>
							<p>The lesson length dropdown (under the method tabs in the left sidebar) allows you to customize the length of each lesson.</p>
						</li>
						
					</ul>
				</div>
				<div class="col-1-2">
					<ul class="checkList">
						<li>
							<h3>Set target speed and accuracy</h3>
							<p>Now you have the ability to set typing goals for your typing lessons! Simply set the typing speed and accuracy you would like to achieve and the typing tutor will track your progress, letting you know which lessons you have completed and which ones you should repeat to achieve your goals.</p>
							<p>As you improve and increase your target speed and accuracy, the tutor will automatically suggest lessons you should work on next!</p>
						</li>
						<li>
							<h3>Custom Lessons</h3>
							<p>Sometimes there are just a few keys you can't seem to remember. Simply enter the characters you wish to practice more in the left toolbar and click "Go!" to create a custom lesson for these keys, generated into random "words."</p>
						</li>
						<li>
							<h3>Restart Typing Lesson Hotkey</h3>
							<p>Keep your hands on the keyboard - use keyboard shortcut "Shift-Return" to restart the typing lesson to help you concentrate and keep your hands in the home position where they belong.</p>
						</li>
					</ul>
				</div>
			</div>
			
			<div><h2>Practice Frequently!</h2></div>
			<p>
				This free online typing tutor was designed to help you learn to type as fast and easy as possible. Try a few lessons a day and you'll start to notice your fingers naturally move to the right keys. Even if it seems at times that you are making no improvement, keep on working at it and <span class="boldFont">you will learn to type without looking!</span> Remember to take breaks often though - its good for the body and for the learning!
			</p>
		</div>
	</div>	<!-- bottomInfo -->
	<div id="miniPopout" class="mini" onclick="void(0)">
		<div class="scrollDiv">
			<div id="miniTitle">Lessons<div id="expandIcon">&#x21f2;</div>
			</div>
			<div><h3 class="STO_botDivide">Classic Lessons</h3></div>
			<div id="classicLessons"><?php echo $classicLessonsHTML; ?></div>
			<div><h3 class="STO_botDivide">Advanced Lessons</h3></div>
			<div id="advancedLessons"><?php echo $advancedLessonsHTML; ?></div>
		</div>
	</div>
	<div id="sidebar" class="stoInfoBox">
		
		<div class="STO_botDivide">
			<h3>Custom Lesson</h3>
				<div id="customLessonDiv">
					<div id="CL_Entry">
						<p>Enter characters:</p>
						<form>
							<input type="text" id="customLessonTA" maxlength="26" value="fj">
						</form>
						<button type="button" id="CL_Button">Go!</button>	
					</div>			
				</div>
		</div>
		
		<h3>Options</h3>
		<div id="sbOptionsWrap">
			<div id="lessonTimeDropDown">
				<span>Lesson Length: </span>
				<select id="lessonLengthSelected" class="stoUI stoUIClickable">
	<?php			if($debugMode){	?>
						<option value="4"<?php echo $lessonLength[4]; ?>>Test Mode</option>
	<?php			}?>
					<option value="70"<?php echo $lessonLength[0]; ?>>Short</option>
					<option value="140"<?php echo $lessonLength[1]; ?>>Normal</option>
					<option value="280"<?php echo $lessonLength[2]; ?>>Long</option>
					<option value="560"<?php echo $lessonLength[3]; ?>>Extra Long</option>
				</select>
			</div>
		</div>
		<div id="psContainer">
            <div id="psDiv" class="resultStatLeft" title="Auto-corrects misalignment with the text at the<br />cost of only 1 incorrect letter per shift<br /><strong>(recommend leaving enabled)</strong>" >
				<input type="checkbox" id="psCheckbox" value="psCorrect" checked>
				<label for="psCheckbox">Phase Shift Correction</label>
            </div>
        </div>
	</div>
			
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.js"></script>
<script type="text/javascript">  
	var sa_id = <?php echo (Input::get('sa_id') == '') ? "null" : Input::get('sa_id'); ?>;
	var lessonId = <?php echo $lessonId;?>;
	var lessonIndex = <?php echo $lessonIndex;?>;
	var currentIndex = lessonIndex;
	var prevLessonId = <?php echo $prevLessonId;?>;
	var prevLessonIndex = <?php echo $prevLessonIndex;?>;
	var lessonData;
	var lessonsWithoutFullHistory;
	var loggedInResults;
	var tutorGlobals = {defaultWPM : "___", defaultAccuracy : "___", targetWPM : <?php echo $lessonWPMtarget;?>, targetAccuracy : <?php echo $lessonAccTarget;?>, nextLessonMod : 0, customLessonChars : "fj", customLessonWPM : "___", customLessonAccuracy : "___", userSettings : {} };
</script>
<script src="js/jquery.hotkeys.js?v=1" type="text/javascript"></script>
<script src="js/sWatch.js?v=18" type="text/javascript"></script>
<script src="js/learnText.js?v=16" type="text/javascript"></script>
<script src="js/stats2.js?v=6" type="text/javascript"></script>
<script src="/js/general.js"></script>
<script type="text/javascript">  
	// handles all popup window links (used for Social Media Sharing)
    function popup(mylink, windowname){
        if (! window.focus)return true;
        var href;
        if (typeof(mylink) == 'string')
            href=mylink;
        else
            href=mylink.href;
        window.open(href, windowname, 'width=626,height=436,scrollbars=yes');
        return false;
    };
	
	$(document).ready(function() {
		
		setupFocus();
	
		// ADD HOTKEY
		shortcut.add("Shift+return",function() {
			Reset(0, 'sWatch');
			},{
			"type":"keyup",
			"propagate":false,
			"disable_in_input":false,
			"target":document
			});
		
		resizeElements();

        $('#blockDivContainer').on("keydown", function(event){
            if($('#blockDivContainer').get()[0].innerHTML != ""){
                return CreateTimer_block(event, 'sWatch', 0/* Round Time */);
            }
        });

        $('#lineDivContainer').on("keydown", function(event){
             return CreateTimer_line(event, 'sWatch', 0/* Round Time */);
        });

        $("#switchModesBtn").click(function(){
            $(this).attr('disabled', 'true');
			var blockDivElem = $("#blockDivContainer");
			var lineDivElem = $("#lineDivContainer");
			var divGlowElem = $("#blockDivGlow")
			
            if (blockDivElem.is(":hidden")) {
                tutorGlobals["userSettings"]["input_mode"] = "BLOCK";
                lineDivElem.hide();
                divGlowElem.hide();
				$("#divCover").css({"height": "160px","margin-top": "-1px"});
				divGlowElem.css({"height": "162px", "top": "-2px"});
                blockDivElem.slideDown('slow', function(){
                    $("#switchModesBtn").removeAttr('disabled');
                    divGlowElem.fadeIn(1000);
                });
            } else {
                tutorGlobals["userSettings"]["input_mode"] = "INLINE";
                blockDivElem.hide();
                divGlowElem.hide();
				$("#divCover").css({"height": "60px","margin-top": "50px"});
				divGlowElem.css({"height": "60px", "top": "48px"});
                lineDivElem.slideDown('slow', function(){
                    $("#switchModesBtn").removeAttr('disabled');
                    divGlowElem.fadeIn(1000);
                });
            }
        });
    
        // call reset function every time text type drop down menu changes
        $("#textTypeSelected").change(function () {
            Reset(0,'sWatch');
            
            if($('#textTypeSelected').val().toUpperCase() == "CUSTOM"){
                $('#hiddenContainer').slideDown('slow', function(){});
                $("#customTextContainer").slideDown('slow', function(){});
            }
            else{
                $("#customTextContainer").hide();
                if($('#colorContainer').is(":hidden")){
                    $('#hiddenContainer').slideUp('slow', function() {});
                }
            }
        });
		
		$( "#targetWPMspinner").spinner({
			mouseWheel: true,
			numberFormat: "n0",
		  spin: function( event, ui ) {
			if ( ui.value > 250 ) {
			  $( this ).spinner( "value", 250 );
			  return false;
			} else if ( ui.value < 1 ) {
			  $( this ).spinner( "value", 1 );
			  return false;
			}
		  }
		});
		
		$("#targetWPMspinner").keypress(function (e) {
			//if the letter is not digit then don't type anything
			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false;}
	    });
		
		$("#targetAccuracyspinner").keypress(function (e) {
			//if the letter is not digit then don't type anything
			if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) { return false;}
	    });
		
		$("#targetWPMspinner").attr({"max" : 250, "min" : 1, "var" : "targetWPM", "display_id" : "tl_wpm_target" });
		$("#targetAccuracyspinner").attr({"max" : 100, "min" : 0.1, "var" : "targetAccuracy", "display_id" : "tl_accuracy_target" });
		
		$( "#targetAccuracyspinner").spinner({
			step: 0.1,
			numberFormat: "n1",
			mouseWheel: true,
		  spin: function( event, ui ) {
			if ( ui.value > 100 ) {
			  $( this ).spinner( "value", 100 );
			  return false;
			} else if ( ui.value < 0.1 ) {
			  $( this ).spinner( "value", 0.1 );
			  return false;
			}
		  }
		});
		
		$('#targetEditDiv').on("mouseleave", function(){
			var elem = $(this).parent();
			var targetField = elem.find('.targetField');
			var editValue = elem.find('.editValue');
			
			var targetVals = editValue.map(function(){
				//var temp = [["id" : this.id, "val" : this.value, "max" : this.max, "min" : this.min]]; 
				var temp =[[]];
				temp[0]['id'] = this.id;
				temp[0]['val'] = this.value;
				temp[0]['max'] = this.max;
				temp[0]['min'] = this.min;
				temp[0]['var'] = this.attributes.var.value;
				temp[0]['display_id'] = this.attributes.display_id.value;
				return temp;
			});
			
			var postFormatValue;
			var colArr = [];
			var valArr = [];
			
			for(var ii = 0; ii < targetVals.length; ii++){
				postFormatValue = (Math.floor(targetVals[ii]['val'] * 10)) / 10;
				
				if(postFormatValue > targetVals[ii]['max']){
					postFormatValue = targetVals[ii]['max'];
				} else if(postFormatValue < targetVals[ii]['min']){
					postFormatValue = targetVals[ii]['min'];
				}
				
				$('#' + targetVals[ii]['id']).val(postFormatValue);
				tutorGlobals[targetVals[ii]['var']] = postFormatValue;	// update javascript global variable
				
				elem = $('#' + targetVals[ii]['display_id']);
				elemHTML = elem.html();
				
				if(elemHTML != postFormatValue && !isNaN(postFormatValue)){
					elem.html(postFormatValue);
					colArr.push(targetVals[ii]['display_id']);
					valArr.push(postFormatValue);
				} else {
					//editValue.val(targetField[0].innerHTML);	// set back just in case invalid input from user
				}
			}		
			
			getLessonData(function(output){
				myJSONdata = $.parseJSON(output);
				$('#classicLessons').html(myJSONdata["loggedInResults"].classicLessonHTML);
				$('#advancedLessons').html(myJSONdata["loggedInResults"].advancedLessonHTML);
				$('#lessonMod_' + lessonId).addClass("current");
			});
			
			$.post('setData.php', { type: "us", col: colArr, val: valArr},
				function(output){
			});
			Reset(0,'sWatch');
		});
		
		// personalize test report with name
		$('#personName').focus(function(){
			if($('#personName').val() == "<Name>")
				$('#personName').val("");
		});
		
		$('#personName').blur(function(){
			if($('#personName').val() == "")
				$('#personName').val("<Name>");
		});
		
		$(document).on('keydown', function(e){
			if($('#centerConsoleWrap').hasClass("projectorScreen")){
				
				if(e.which == 32){
					if(tutStep == 1){
						$('#BigHandsImg').finish();
						$('#LessonTutorialText').finish();
						tutStep++;
						RunAnimationStep(tutStep);
					}
				}
				
				
				return false;
			}
		});
		
		$('#replayBtn').click(function(){
			RunAnimationStep(tutStep);
		});
		
		$('#nextBtn').click(function(){
			$('#BigHandsImg').finish();
			$('#LessonTutorialText').finish();
			tutStep++;
			RunAnimationStep(tutStep);
		});
		
		$('#closeBtn').click(function(){
			$('#centerConsoleWrap').removeClass("projectorScreen");
			ResetLessonTutorial();
		});
		
		$('#PN_Button').click(function(){
			var name = $('#personName').val();
			
			if(name != '' && name != "<Name>"){
				generateGenLink();
				$('#resultsTitle').html(name + "'s " + resultsTitle);
				
				document.getElementById('genLinkEmail').title = '';
				document.getElementById('genLink').title = '';
				document.getElementById('genLinkDisplay').title = '';				
			} else {
				$('#resultsTitle').html(resultsTitle);
			}
		});
		
		//custom lesson go button click
		$("#CL_Button").click(function() {
			textSelection = "CUSTOM_LESSON";
			Reset(0,'sWatch');
		});

        // change display every time text type drop down menu changes
        $("#lessonLengthSelected").on("change", function () {
			$.post('setData.php', { type: "us", col: "tl_length", val: this.value},
				function(output){
			});
            Reset(0,'sWatch');
        });
		
		
		var currDateTime = new Date(<?php echo '"' . gmdate('Y-m-d H:i:s') . '"';?>.replace(/-/g,'/'));
		
		$('.makeDateReadable').each(function(){
			earlyDate = new Date($(this).html().replace(/-/g,'/'));
			timeDiffString = readableTimeDifference(earlyDate, currDateTime);
			$(this).html(timeDiffString);
			if(timeDiffString.indexOf("mins") > 0 || timeDiffString.indexOf("secs") > 0)
				$(this).addClass("borderRad_4 dkBlueBkgd boldFont centerTxt");
		});
        
        Reset(0,'sWatch');
	});
	
	// impose maxlength attribute on text areas
	window.onload = function() { 
		var txts = document.getElementsByTagName('TEXTAREA') 

		  for(var i = 0, l = txts.length; i < l; i++) {
			if(/^[0-9]+$/.test(txts[i].getAttribute("maxlength"))) { 
			  var func = function() { 
				var len = parseInt(this.getAttribute("maxlength"), 10); 

				if(this.value.length > len) { 
				  this.value = this.value.substr(0, len); 
				  alert('Maximum length exceeded: ' + len); 
				  return false; 
				} 
			  }

			  txts[i].onkeyup = func;
			  txts[i].onblur = func;
			} 
		  } 
	}	

  $(window).resize(function() {
	resizeElements();
  });
  
	$('#prevLessonLink').click(function(){
		if(currentIndex > 0){
			$('#lessonMod_' + lessonData[currentIndex].id).removeClass("current");
			currentIndex--;
			lessonIndex = currentIndex;
			lessonId = lessonData[currentIndex].id;
		}
		if(lessonData[currentIndex].type == "keyboard"){
			window.location.href = "/keyboard-basics.php?mod=" + lessonData[currentIndex].id;
		}
		Reset(0,'sWatch');
	});
  
	$('#nextLessonLink').click(function(){
		if(currentIndex < lessonData.length){
			$('#lessonMod_' + lessonData[currentIndex].id).removeClass("current");
			currentIndex += 1;
			lessonIndex = currentIndex;
		}
		
		Reset(0,'sWatch');
	});
	
	$('#recommendedNextLesson').click(function(){
		lessonId = tutorGlobals["nextLessonMod"];
		textSelection = "";
		Reset(0, 'sWatch');
	});
	
	$('div[id^="microlessonMod_"] a').removeAttr("onclick");
	
	$('#centerContent').on("mouseleave", function(){
		var colArr = [];
		var valArr = [];
		var doReset = false;
		
		$.each(tutorGlobals["userSettings"], function(key, value){
			colArr.push(key);
			valArr.push(value);
			delete tutorGlobals["userSettings"][key];
		});
		
		if(colArr.length > 0){
			$.post('setData.php', { type: "us", col: colArr, val: valArr},
				function(output){
			});
		}
	});
  
  function resizeElements(){
	var pageWidth = $('#widthDiv').width();
  		if(pageWidth < 1281 && pageWidth >= 1121){
			// deal with small page
			$('#centerConsole').removeClass();
			 $('#centerConsole').addClass("scale75p");
			 $("#bottomInfo").css("top","-300px");
		} else if(pageWidth < 1121){
			$('#centerConsole').removeClass();
			$('#centerConsole').addClass("scale90p");
		} else if(pageWidth >= 1281 && pageWidth < 1601){
			// deal with large page
			$('#centerConsole').removeClass();
			$('#centerConsole').addClass("scale90p");
			$("#bottomInfo").css("top","-205px");
		}
		else if(pageWidth > 2400){
			// deal with large page
			$('#centerConsole').removeClass();
			$('#centerConsole').addClass("scale115p");
			$("#bottomInfo").css("top","-40px");
		}
		else{
			$('#centerConsole').removeClass();
			$("#bottomInfo").css("top","-150px");
		}
  }
  $(window).scroll(function(){ 
	var elem = $('#centerConsole');
	var a = elem.offset().top + 100;
	var pos = $(window).scrollTop();
	if(pos > a) {
		$("#miniPopout").hide();
		$("#sidebar").hide();
	} else {
		$("#miniPopout").show();
		$("#sidebar").show();
	}
  });
    
  var name = "<?php echo $nameToPrint;?>";
</script>

<?php 
include 'includes/overall/footer.php'; 
?>
