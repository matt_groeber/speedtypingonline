<?php 
$currPage = "typingProducts";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$htmlTitle = "Typing Products";
$htmlDescription = "Computer accessories and other typing-related products.";
$cssFiles = "be";
$noWidgets = true;
$addContainer = true;
require_once 'includes/overall/header.php';

$user = new User();
if(!$user->hasPermission("admin"))
{
	echo 'You are not authorized to view this page';
	exit();
}
?>

		<h1>Typing Products</h1>
	</div>
	<div>
		<div id="headInfo">
		<p>This is a list of computer accessories and other typing-related products I recommend and in some cases use myself.</p>
		<p>The emphasis is on highly adjustable and ergonomic equipment that fit you best, which will help you be more comfortable and reduce your risk of injury.</p>
		</div>
	</div>
	<div>
		<h2>Keyboards</h2>
	</div>
</div>

<?php 
include 'includes/overall/footer.php'; 
?>