<div id="createAssignBtn">
	<input class="stoUI stoUIClickable" type="button" value="Create Assignment" onclick="CreateOrEditAssignment('Create and Assign', 'Save', 'Create Assignment', null);">
</div>

<div id="dialog-create-assignment" title="Create Assignment" style="display: none;">
	<div id="mainCont">
		<!--<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>You can create an assignment here.</p>-->
		<div class="transfer"></div>
		<div id="rightCol">
			<div>
				<div class="field stoUI">
					<label for="assignmentTitle"><h4>Title:</h4></label>
					<input type="text" id="assignmentTitle" name="assignmentTitle" class="backEndInput" value="Assignment">
				</div>
				<div class="field stoUI">
					<label for="assignmentNotes"><h4>Notes:</h4></label>
					<textarea id="assignmentNotes" name="assignmentNotes" class="backEndInput" value=""></textarea>
				</div>
				<div id="ATPdiv" class="field stoUI">
					<label>Type:</label>
					<select id="ATPselect" name="ATPselect" class="stoUI stoUIClickable backEndSelect">
					<?php 
						foreach($assignmentTypesArr as &$elem){
							echo '<option value="' . $elem->id . '" title="' . $elem->at_descrip . '">' . $elem->at_name . '</option>';
						}
					?>
					</select>
				</div>
				<div id="ASPdiv" class="field stoUI">
					<label>Lesson:</label>
					<select id="ASPselect" name="ASPselect" class="stoUI stoUIClickable backEndSelect">
					<?php 
						foreach($lessonsArr as &$elem){
							if($elem->display_text == '')
								$lessonDescrip = $elem->purpose;
							else
								$lessonDescrip = $elem->display_text;
							
							if($elem->type == 'keyboard'){
								$lessonType = "Keyboard Basics";
								$lessonDescrip = $elem->purpose;
							} else 
								$lessonType = ucfirst($elem->type) . ' ' . $elem->lesson_type . ' ' . $elem->lesson_num;
							
							/*if($elem->id == 83)
								echo '<optgroup class="keyboard group" label=" -- Keyboard Basics --">';
							else if($elem->id == 1)
								echo '<optgroup class="classic group" label=" -- Home Row --">';
							else if($elem->id == 6)
								echo '<optgroup class="classic group" label=" -- Top Row --">';
							else if($elem->id == 11)
								echo '<optgroup class="classic group" label=" -- Bottom Row --">';*/
							
							echo '<option class="specLesson ' . $elem->type . ' ' . $elem->lesson_type . '" value="' . $elem->id . '">' . $lessonType . ' - ' . $lessonDescrip . '</option>';
							
							/*if($elem->id == 84 || $elem->id == 20 || $elem->id == 23 || $elem->id == 26)
								echo '</optgroup>';*/
						}
						
						
						foreach($textTypes as &$tt){
							echo '<option class="textTypes" value="' . $tt->tid . '">' . $tt->display_text . '</option>';
						}
					?>
					</select>
				</div>
				<div id="ASVdiv" class="field stoUI">
					<label class="beforeInput" for="ASVselect"><h4><i class="fa fa-clock-o" aria-hidden="true"></i> Time:</h4></label>
					<input type="text" id="ASVselect" class="ui_spinner editValue" name="ASVselect" value="2">
					<label class="afterInput" for="ASVselect"><h4>minutes</h4></label>
				</div>
				<div id="ADDdiv" class="field stoUI">
					<label for="ADDpicker"><h4>Due Date:</h4></label>
					<div id="ADDpicker" class="stoUI stoUIClickable">
						<i class="fa fa-calendar"></i>&nbsp;
						<span>Select Due Date</span> <i class="fa fa-caret-down"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>