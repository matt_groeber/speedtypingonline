<?php
$currPage = "assignments";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];
$db = DB::getInstance();
$username = Input::get('user');

// first see if no username has been passed in
if($username === ''){
	$user = new User();

	// assignment emails will direct to this page without user data so handle accordingly
	if($user->isLoggedIn()){
		Redirect::to('/user/' . $user->data()->username . '/assignments');
	} else {
		Redirect::to('login.php');
	}
}

$user = new User();
$userData = $user->data();
$debugMode = $user->hasPermission('debug') || Config::get('constants/debugMode');

if($userData->username != $username){
	
	// restrict user profile to be viewed by only the actual user when logged in (aside from exception where user has changed their username and submitted)
	if(!Session::exists('updated') && !Session::exists('failed')){
		if(inMaintenanceMode()){
			require_once 'includes/overall/header.php';
			echo '<h2>Site is under Maintenance</h2><h4>User profile pages will be available shortly.</h4><h4>Please continue to use the site as normal but be aware that test, lesson, and game high score data will not be saved.</h4><h4>Sorry for any inconvenience and thank you for your patience!</h4>';
			exitPHPwithFooter();
		} else {
			Redirect::to('/typing-test.php');
		}
	} else {
		Redirect::to('/user/' . $username . '/test-stats');
	}
}

$userToProfile = $user;
$onPersonalProfile = true;

$htmlTitle = "Assignments, " . $username;
$htmlDescription = "Assignments list for " . $username;
$cssFiles = "behkiv";
$fontPacks = "dij";
$noWidgets = true;
$jQueryUI = true;
$fontAwesomeCDN = true;
$addContainer = true;
require_once 'includes/overall/header.php';
?>
<div class="borderBottom">
<?php
include 'includes/profileMenu.php';
?>
</div>
</div> <!-- <div id="heading"> -->

<div id="pageMain">
	<div id="genAdWrap">
		<?php echo getAd("generalLeadAd", null, $debugMode, $user->isPremium());?>
	</div>
<?php
	if(Session::exists('failed')){
		echo Session::flash('failed');
	} else {
		echo Session::flash('updated');
	}
	
	if(!$user->isTeacher() && !$user->isStudent()){
?>
		 <h2>You are not a teacher or student.</h2> 
<?php
	} else {	
?>	
	<div id="formErrors"><?php echo (isset($errorsToPrint)) ? $errorsToPrint : '';?></div>

	<div>
		<div class="flex">
			<div class="flex-3">
			
				<h2>Assignments</h2>
				
		<?php
			$loggedInID = ($userToProfile->isLoggedIn()) ? $userToProfile->data()->id : null;
			$classroomObj = new Classroom($userToProfile);
			
			$lessonsObj = new Lessons();
			$classes = $classroomObj->getClasses(false);
			$assignments = $classroomObj->GetAssignments('all');
			$assignmentTypesArr = $classroomObj->GetAssignmentTypes();
			$lessonsArr = $lessonsObj->GetLessons();
			$textTypes = $db->get('`text_types`', array('tid', '>', '0'), 'tid, type, display_text')->results();
			$studentUserData = $classroomObj->getClasses(true);
			$studentHistoryArr = $classroomObj->getStudentHistory();
			$studentLessonHistArr = $studentHistoryArr[0];
			$studentTestHistArr = $studentHistoryArr[1];
			$studentHistoryArr = array_merge($studentLessonHistArr, $studentTestHistArr);
			
				?>
				<div id="allAssignments" class="<?php echo ($user->isTeacher()) ? 'isTeacher' : '';?>">
				</div>
				<div id="allAssignmentsHiddenWarning" class="assignmentsNotify" style="display: none;">Your filter settings are hiding all assignments.</div>
			</div> <!-- <div class="col-3-4"> -->
			<div class="flex-1 sidebar">
				<div class="hudContainer">
<?php
					if($user->isTeacher()){
						require_once 'includes/widgets/createAssignment.php';
					}
?>
					<div id="assignSortWrap">
						<h4>Sort by:</h4>
						
						<select id="assignSortSelect">
							<option value="0">Due Date (earliest first)</option>
							<option value="1">Due Date (latest first)</option>
							<option value="2">Assigned Date (earliest first)</option>
							<option value="3">Assigned Date (latest first)</option>
						</select>
					</div>
					<!-- <div id="asideWrap"> -->
					<div id="timeframeWrap">
						<h4>Due Date Filter:</h4>
						
						<div id="reportrange" class="stoUI stoUIClickable" style="cursor: pointer;">
							<i class="fa fa-calendar"></i>&nbsp;
							<span></span> <i class="fa fa-caret-down"></i>
						</div>
					</div>
					
					
					<div id="filterAssignments">
<?php
					if($user->isTeacher()){
						require_once 'includes/widgets/createAssignment.php';
?>
						<label id="uaLbl" class="individualTypeFilter" for="showUnassignedAssignmentsBtn"><div class="filterBtnIcon"><i class="fa fa-sticky-note-o" aria-hidden="true"></i></div><div class="filterBtnLblText">Unassigned</div></label>
						<input type="checkbox" id="showUnassignedAssignmentsBtn" checked>
						
						<label id="caLbl" class="individualTypeFilter" for="showCurrentAssignmentsBtn"><div class="filterBtnIcon"><i class="fa fa-calendar" aria-hidden="true"></i></div><div class="filterBtnLblText">Current</div></label>
						<input type="checkbox" id="showCurrentAssignmentsBtn" checked>
						
						<label id="paLbl" class="individualTypeFilter" for="showPastAssignmentsBtn"><div class="filterBtnIcon"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div><div class="filterBtnLblText">Past</div></label>
						<input type="checkbox" id="showPastAssignmentsBtn" checked>
						
						<label id="aaLbl" for="showAllAssignmentsBtn">Hide All</label>
						<input type="checkbox" id="showAllAssignmentsBtn" checked>

					</div>
					<div id="otherOptions">
						<label id="showStudentProgress" for="showStudentProgressBtn">Show Student Progress</label>
						<input type="checkbox" id="showStudentProgressBtn" checked>

<?php
					} else if($user->isStudent()){				
?>
						
						<label id="caLbl" class="individualTypeFilter" for="showCurrentAssignmentsBtn"><div class="filterBtnIcon"><i class="fa fa-calendar" aria-hidden="true"></i></div><div class="filterBtnLblText">Current</div></label>
						<input type="checkbox" id="showCurrentAssignmentsBtn" checked>
						
						<label id="paLbl" class="individualTypeFilter" for="showPastAssignmentsBtn"><div class="filterBtnIcon"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></div><div class="filterBtnLblText">Completed</div></label>
						<input type="checkbox" id="showPastAssignmentsBtn" checked>
						
						<label id="pdaLbl" class="individualTypeFilter" for="showPastDueAssignmentsBtn"><div class="filterBtnIcon"><i class="fa fa-calendar-times-o" aria-hidden="true"></i></div><div class="filterBtnLblText">Past Due</div></label>
						<input type="checkbox" id="showPastDueAssignmentsBtn" checked>
						
						<label id="aaLbl" for="showAllAssignmentsBtn">Hide All</label>
						<input type="checkbox" id="showAllAssignmentsBtn" checked>
<?php
					}				
?>
					</div>
				</div>
			</div>
		</div>
	</div>
		
</div>

<?php if($user->isTeacher()){ ?>
		<div id="dialog-confirm" title="Permanently delete this Assignment?" style="display: none;">
			<div id="extraInfo"></div>
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>All data including all student progress will be permanently deleted and cannot be recovered.</p>
			<p>Are you sure?</p>
		</div>
<?php 	} ?>
<?php
		
	}
?>
<link rel="stylesheet" href="/css/jquery.transfer.css?v=18" type="text/css">
<script src="/js/general.js?v=39"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.js?v=2"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js?v=2"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js?v=2"></script>
<script type="text/javascript" src="/js/jquery.transfer.js?v=5"></script>
<?php
	require_once 'includes/assignmentJS.php';
?>
<script>

	var testGlobals = {userSettings : {} };
	$(document).ready(function(){
		$('.ui-spinner-button').on('click', function(event) {
			showUpdateButton();
		});

	});
	
	
	$('#textTypeSelected').change(function(){
		updateTextSelection();
	});
	
	
	function showUpdateButton(){
		$('#topUpdateBtn').show();
		$('#bottomUpdateBtn').show();
		$('#formInfo').hide();
		$('#formErrors').hide();
	}
	
	$('input').change(function(){
		showUpdateButton();
	})
	
	$('select').change(function(){
		showUpdateButton();
	})
	
	$('.ui_spinner').on('mousewheel', function(event) {
		showUpdateButton();
	});
	

	$( "#targetWPMspinner").spinner({
		mouseWheel: true,
		numberFormat: "n0",
		spin: function( event, ui ) {
			if ( ui.value > 255 ) {
				$( this ).spinner( "value", 255 );
				return false;
			} else if ( ui.value < 0 ) {
				$( this ).spinner( "value", 0 );
				return false;
			}
		}
	});

	$("#targetWPMspinner").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false;}
	});

	$("#targetAccuracyspinner").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) { return false;}
	});

	$( "#targetAccuracyspinner").spinner({
		step: 0.1,
		numberFormat: "n1",
		mouseWheel: true,
	  spin: function( event, ui ) {
		if ( ui.value > 100 ) {
		  $( this ).spinner( "value", 100 );
		  return false;
		} else if ( ui.value < 0 ) {
		  $( this ).spinner( "value", 0 );
		  return false;
		}
	  }
	});
</script>
<?php
require_once 'includes/overall/footer.php';
?>