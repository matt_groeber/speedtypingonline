<?php
$currPage = "FAQ";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];


$fontAwesomeCDN = false;
$htmlTitle = "Frequently Asked Questions";
$htmlDescription = "Answers to Frequently Asked Questions (FAQ)";
$cssFiles = "beh";
$noWidgets = true;
$addContainer = true;
require_once 'includes/overall/header.php';
	
?>

<h1>Frequently Asked Questions (FAQ)</h1>
</div> <!-- <div id="heading"> -->

<div id="pageMain" class="readingLineWidth">

	<div class="stoSimpleList">
		<ol class="numberedList">
			<li id="howToRegister">
				<h3 class="listTitle">How do I register an account?</h3>
				<div class="insideList">
					<ol>
						<li>Visit the <a href="/register.php">Register page</a>, fill out the fields and click "Register".
							<div>(You can also quickly sign up using your Google account)</div>
						</li>
						<li>You will get an activation email to the email address you provided for your account. 
							<div>(Email "<a href="mailto:speedtypingonline@gmail.com" target="_blank">speedtypingonline@gmail.com</a>" for help if you do not receive this email within 10-15 minutes)</div>
						</li>
						<li>Click the activation link in the email and your account should be registered and active!</li>
					</ol>
				</div>
			</li>
			<li id="profilePicture">
				<h3 class="listTitle">How do I change my profile picture?</h3>
				<div>
					<p>Currently the only way to customize your profile picture is to sign in with Google, in which case your profile picture will be updated to the one you have set for your Google account.</p>
					<p>A future update to allow you to customize your profile picture without having to sign in to Google is planned.</p>
				</div>
			</li>
			<li id="limitedData">
				<h3 class="listTitle">Why can't I see all of my typing test history?</h3>
				<div>
					<p>All (free) accounts are currently limited to <?php echo Config::get('constants/max_tt_entries');?> typing tests.</p>
					<p>A future update to include a premium account with ad-free unlimited data storage is planned.</p>
				</div>
			</li>
			<li id="maintenanceMode">
				<h3 class="listTitle">Site is under maintenance?</h3>
				<div>
					<p>This means the site is being fixed and/or updated and will be back to fully-functional shortly.</p>
					<p>The site will work mostly the same except you will be unable to login and therefore be unable to save data or track your progress.</p>
				</div>
			</li>
			<li id="shareTestResults">
				<h3 class="listTitle">How to share my typing test results?</h3>
				<div>
					<p>The best way to share your test results is to send the link that is generated and displayed on your test results.</p>
					<p>By going to this link anyone will be able to see your name (if you personalized your test results), the date, your typing speed and accuracy, and the length of test you took.</p>
					<p>Here is the location of the link on your test results:</p>
					<img src="/images/TypingTest_ShareTestResultsLink.png">
				</div>
			</li>
			<!--<li id="phaseShift">
				<h3 class="listTitle">What is "Phase Shift Correction"?</h3>
				<div>
					<p>"Phase Shift Correction," is a feature that solves a very frustrating problem while typing.</p>
					<p>The problem occurs when a small typing mistake (such as a key that you thought you typed but actually didn't) causes you to be one character off where you actually are according to the typing test. Once you are one-off, every character after that would end up being wrong even though you were actually typing the words correctly, just one character ahead every time.</p>
					<p>Phase shift correction detects this problem and realigns you with the typing test cursor at a cost of only one mistake--the only mistake you actually made.</p>
					<p>The best way to see this is (with Phase Shift Correction enabled) to start typing the text one character ahead or one character behind. The test will automatically realign you and everything you type after that will still be correct. The typing test will show you the phase shift by putting a yellow background on the letter you "phase shifted" on. Do the same thing with the Phase Shift Correct disabled and once you start typing one character ahead or behind nearly everything you type after that will be wrong.</p>
				</div>
			</li>-->
		</ol>
	</div>
</div>

<style>
	img{width:90%;}

</style>
<?php
require_once 'includes/overall/footer.php';
?>