<?php
class Validate{
	private $_passed = false,
			$_errors = array(),
			$_db = null,
			$_flag = false;
			
	public function __construct(){
		$this->_db = DB::getInstance();
		$this->_flag = false;
	}
	
	public function check($source, $items = array()){
		foreach ($items as $item => $rules){
			
			// get defined name if it exists
			$item_name = (isset($rules['name'])) ? $rules['name'] : 'Field "' . $item . '"';
						
			foreach($rules as $rule => $rule_value){
				
				$value = trim($source[$item]);
				$item = escape($item);
				
				if($rule === 'required' && empty($value)){
					$this->addError("{$item_name} is required");
				} else if(!empty($value)){
					switch($rule){
						case 'min':
							if(strlen($value) < $rule_value){
								$this->addError("{$item_name} must have at least {$rule_value} characters");
							}
						break;
						case 'max':
							if(strlen($value) > $rule_value){
								$this->addError("{$item_name} cannot be more than {$rule_value} characters");
							}
						break;
						case 'matches':
							$second_item_name = (isset($items[$rules['matches']]['name'])) ? $items[$rules['matches']]['name'] : 'Field "' . $rules['matches'] . '"';
							
							if($value != $source[$rule_value]){
								$this->addError("{$second_item_name} must match '{$item_name}'");
							}
						break;
						case 'equalsTrue':
							// report error in param 2 if param 1 is false
							if(!$rule_value[0]){
								$this->addError($rule_value[1]);
							}
						break;
						case 'unique':
							if(is_array($rule_value)){
								$table = $rule_value[0];
								$errorString = $rule_value[1];
							} else {
								$table = $rule_value;
								$errorString = '';
							}							
							$check = $this->_db->get($table, array($item, '=', $value));
							if($check->count()){
								if($errorString == ''){
									$this->addError("That {$item_name} already exists");
								} else {
									$this->addError($errorString);
								}
							}
						break;
						case 'exists':
							// check if $item = $value exists in table $rule_value or $rule_value[0]
							// will return positive results in $rule_value[1] if provided
							$table = (is_array($rule_value)) ? $rule_value[0] : $rule_value;
							$check = $this->_db->get($table, array($item, '=', $value));
							if($check->count() == 0){
								$this->addError("{$item_name} does not exist");
							} else if(is_array($rule_value)){
								$rule_value[1] = $check;
							}
						break;
						case 'doubleMatch':
							// check table {param 0} for value 1 {param 2} in column 1 {param 1} then see if value 2 {$value}
							// exists in the same row, column 2 {$item}
							$check = $this->_db->get($rule_value[0], array($rule_value[1], '=', $rule_value[2]));
							if($check->results()[0]->$item !== $value){
								$this->addError("{$item_name}");
							}
						break;
						case 'notNumeric':
							if(ctype_digit($value)){
								$this->addError("{$item_name} must contain at least one letter (a-z)");
							}
						break;
						case 'noSpaces':
							if(preg_match('/[ ]/', $value)){
								$this->addError("{$item_name} cannot contain spaces");
							}
						break;
						case 'noReservedSymbols':
							if(preg_match('/[#$]/', $value)){
								$this->addError("{$item_name} cannot contain a $ or # symbol");
							}
						break;
						case 'notDirty':
							$isBadWord = $this->checkForBadWords($value);
							
							if($isBadWord === 2){
								$this->addError("{$item_name} contained an unacceptable word");
							} else if($isBadWord === 1){
								$this->_flag = true;
							}
						break;
						case 'email':
							if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
								$this->addError("{$item_name} was not valid");
							}
						break;
					}
				}
			}
		}
		
		if(empty($this->_errors)){
			$this->_passed = true;
		}
		
		return $this;
	}
	
	public function checkForBadWords($inputStr){
		$unacceptable_words = "bastard|beastiality|bitch|bukkake|chink|cunt|ejaculate|faggot|foreskin|forskin|fuck|gangbang|handjob|lesbian|masturbate|nigga|nigger|paedophile|pedophile|prostitute|rimjob|schlong|vagina|whore";
		
		$bad_words = "abortion|ass|anal|anus|arse|blowjob|boob|clit|cock|condom|crap|cream|cum|cunt|damn|dick|dildo|dong|douche|dyke|gay|genital|fag|fart|fck|fuk|hell|homo|hooker|hump|jizz|kaffir|kill|kike|kunt|labia|lesbo|muff|murder|nazi|nut|piss|poop|pussy|queer|queef|rape|rectal|rectum|screw|scrotum|semen|sex|shit|slut|sperm|spic|suck|taint|terrorist|tit|twat|vaj|vulva|wank|weiner|wetback|xxx";
		
		if (preg_match('/' . $unacceptable_words . '/i', $inputStr)) {
			return 2;
		}
		
		if (preg_match('/' . $bad_words . '/i', $inputStr)) {
			return 1;
		}
		
		return 0;
	}
	
	private function addError($error){
		$this->_errors[] = $error;
	}
	
	public function errors(){
		return $this->_errors;
	}
	
	public function passed(){
		return $this->_passed;
	}
	
	public function flagged(){
		return $this->_flag;
	}
}

?>