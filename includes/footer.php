<?php if(!isset($microFooter)){$microFooter = false;}?>
<footer<?php echo ($microFooter) ? ' class="microFooter"' : '';?>>
<div>
<?php 
	if(!isset($microFooter) || !$microFooter){
?>
	<div id="footGrid1" class="grid grid-pad">
	   <div class="col-1-3">
		<h3>Update History</h3>
		<ul id="updatesUL">
			<li><span class="bold">(Apr 2021)</span> - You can now <a href="/goPremium">Go Premium!</a> - No more ads!</li>
			<li><span class="bold">(Aug 2020)</span> - Teacher & Student account support with Classrooms and Assignments</li>
			<li><span class="bold">(Aug 2020)</span> - Added <a href="/keyboard-basics">Keyboard Basics</a> page for learning the basics of touch-typing</li>
			<li><span class="bold">(Feb 2020)</span> - Update to Privacy Policy</li>
			<li><span class="bold">(Aug 2018)</span> - Numpad (10-key) and AZERTY keyboard support</li>
			<li><span class="bold">(Jan 2017)</span> - Added support for caps lock key.</li>
			<li><span class="bold">(July 2016)</span> - Added account creation with progress tracking and high score boards</li>
			<!--<li><span class="bold">(July 2016)</span> - Site-wide improvements in functionality and appearance.</li>
			<li><span class="bold">(July 2016)</span> - Added high score tracking to typing games.</li>-->
		</ul>
	   </div>
	   <div id="centFoot" class="col-1-3">
		<h3>Top Pages to Visit</h3>
		<ol id="topPagesOL">
			<li><a href="/typing-test">Typing Test</a></li>
			<li><a href="/typing-tutor">Typing Tutor</a></li>
			<li><a href="/typing-games">Typing Games</a></li>
		</ol>
		<div>
			<h3>Need Help?</h3>
			<div>
				<span><a href="/register.php">Create an Account</a></span>
				<span>|</span>
				<span><a href="/FAQ">FAQ</a></span>
			</div>
		</div>
	   </div>
	   <div class="col-1-3">
		<h3>Contact Me</h3>
		<p>Have a burning question?</p>
		<p>Want a new feature added?</p>
		<p>Send an email to:</p> 
		<a href="mailto:matt@speedtypingonline.com">matt@speedtypingonline.com</a>
		<p>Thanks!</p>
	   </div>
	</div>
<?php 
	}
?>
	<div>
		<a href="/cookie-policy">Cookie</a> & <a href="/privacy-policy">Privacy Policy</a> | Copyright &copy; MLGray LLC - 2020 | <a href="http://www.mattgroeber.com">Matt Groeber</a>
	</div>
</div>
</footer>