<?php
class HighScore{
	private $_db,
	        $_alphaTimeFrame = '-12 hours',
	        $_alphaTimeFramePremium = '-120 hours',
	        $_fftTimeFrame = '-12 hours',
	        $_fftTimeFramePremium = '-120 hours',
			$_defaultMaxScores = 10,
			$_inhumanAlphaScore = 1020,  // in milliseconds ==> equation: wpm = 26/msec * 12000 = 26/sec * 12
									     // score of 1020 = 26/1020 * 12000 = 305.88 wpm
			$_loggedInUserID;
	
	public function __construct($userID = null){
		$this->_db = DB::getInstance();
		$this->_loggedInUserID = $userID;
	}
	
	public function getPersonalAlphaHScores($userID, $type = null, $limitArr = null){
		if($userID !== null){
			return $this->getHScores('alpha_HS', 'total_time', 'total_time, type, timestamp', $userID, $type, null, null, $limitArr);
		} else {
			return false;
		}
	}
	
	public function getGlobalAlphaHScores($type = null, $limitArr = null, $uniqueUsernames = null, $extraCondition = null){
		if($uniqueUsernames === true){
			$colString = 'username, user_id, picture, premium, MIN(total_time) AS total_time, type, flagged, timestamp';
			$groupBy = 'username';
		} else {
			$colString = 'username, user_id, picture, premium, total_time, type, flagged, timestamp';
			$groupBy = null;
		}
		
		if($extraCondition !== null){
			array_unshift($extraCondition, 'total_time');
		}
		
		
		return $this->getHScores('alpha_HS', 'total_time', $colString, null, $type, null, array('users', 'user_id', 'id'), $limitArr, $groupBy, $extraCondition);
	}
	
	public function getPersonalFftHScores($userID, $type = null, $limitArr = null){
		if($userID !== null){
			return $this->getHScores('fft_HS', 'score', 'score, num_words, type, timestamp', $userID, $type, 'DESC', null, $limitArr);
		} else {
			return false;
		}
	}
	
	public function getGlobalFftHScores($type = null, $limitArr = null, $uniqueUsernames = null){
		if($uniqueUsernames === true){
			$colString = 'username, picture, MAX(score) AS score, num_words, type, flagged, timestamp';
			$groupBy = 'username';
		} else {
			$colString = 'username, picture, score, num_words, type, flagged, timestamp';
			$groupBy = null;
		}
		
		return $this->getHScores('fft_HS', 'score', 'username, user_id, picture, score, num_words, type, flagged, timestamp', null, $type, 'DESC', array('users', 'user_id', 'id'), $limitArr, $groupBy);
	}
	
	public function getHScores($table, $scoreStr, $cols = null, $userID = null, $type = null, $order = null, $join = null, $limitArr = null, $groupBy = null, $whereExt = null){
		if($cols === null)    {	$cols = '*'; }
		if($order === null)   { $order = 'ASC'; }
		
		if($limitArr === null){	
			$limitArr = array($this->_defaultMaxScores); 
		} else if(!is_array($limitArr)){
			$limitArr = array($limitArr);
		}
		
		$timeFrame = $this->_alphaTimeFrame;
		$timeFramePremium = $this->_alphaTimeFramePremium;
		
		if($table == 'fft_HS'){	$timeFrame = $this->_fftTimeFrame;	}
		
		if($userID !== null){
			$where = array('user_id', '=', $userID);
		} else {
			/*$user_obj = new User();
			$loggedInID = ($user_obj->isLoggedIn()) ? $user_obj->data()->id : null;*/
			
			// if global high score filter out cheaters and manually flagged offensive names
			$where = array('timestamp', '>', date('Y-m-d H:i:s', strtotime($timeFrame)), '(', "flagged & b'1110'", '=', 0, 'OR', 'user_id', '=', $this->_loggedInUserID, ')');
			//$where = array('timestamp', '>', date('Y-m-d H:i:s', strtotime($timeFrame)), 'OR', 'premium', '!=', 0, 'AND', 'timestamp', '>', date('Y-m-d H:i:s', strtotime($timeFramePremium)),'(', "flagged & b'1110'", '=', 0, 'OR', 'user_id', '=', $this->_loggedInUserID, ')');
			//$where = array('timestamp', '>', date('Y-m-d H:i:s', strtotime($timeFrame)), 'OR', 'premium', '!=', 0, 'AND', 'timestamp', '>', date('Y-m-d H:i:s', strtotime($timeFramePremium)), 'AND', '(', "flagged & b'1110'", '=', 0, 'OR', 'user_id', '=', $this->_loggedInUserID, ')');
			//var_dump("   ", implode($where));
			//exit();
		}
		
		if($type !== null){
			$where = array_merge($where, array('type', '=', $type));
		}
		
		if($whereExt !== null){
			$where = array_merge($where,$whereExt);
		}
		
		return $this->_db->get($table, $where, $cols, array($scoreStr, $order), $join, $limitArr, $groupBy)->results();
	}
	
	public function SetAlphaHighScore($currentUser, $highscore, $alphaType){
		$doInsertion = false;
		//error_log(" +++++++++++  GOT HERE  ++++++++++++++++");
		// see if user cheated
		if($highscore < $this->_inhumanAlphaScore){
			$currentUser->Cheated();
		}
		
		if($currentUser->isPremium()){
			$doInsertion = true;
		} else {
			$dbResult = $this->_db->get('alpha_HS', array('user_id', '=',$currentUser->data()->id, 'type', '=', $alphaType), 'id, total_time, timestamp', array('total_time','ASC'));
			$theResults = $dbResult->results();
			$maxAlphaEntries = Config::get('constants/max_tta_entries');

			// see if at limit of entries
			if(sizeof($theResults) >= $maxAlphaEntries){
				$lastResult = $theResults[$maxAlphaEntries - 1];
				
				// we want to delete all entries more than the limit
				if(sizeof($theResults) > $maxAlphaEntries){
					$IDsToDelete = array();
					
					for($ii = $maxAlphaEntries; $ii < sizeof($theResults); $ii++){
						array_push($IDsToDelete, $theResults[$ii]->id);
					}
					
					$this->_db->delete('alpha_HS', array('id', 'IN', $IDsToDelete));
				}
				
				// replace last entry if older than 24 hours or if new score is better/faster
				if( ($lastResult->timestamp < date('Y-m-d H:i:s', strtotime('-24 hours'))) || ($lastResult->total_time > $highscore) ){
					$this->_db->delete('alpha_HS', array('id', '=', $lastResult->id));
					$doInsertion = true;
				}
			} else {
				$doInsertion = true;
			}
		}
			
		if($doInsertion){
			$result = $this->_db->insert('alpha_HS', array('user_id' => $currentUser->data()->id, 'total_time' => $highscore, 'type' => $alphaType, 'timestamp' => gmdate('Y-m-d H:i:s')));
		}
	}
	
	
}

?>