var Timer = null;
var TotalSeconds;
var RoundTime = 0;
var TimeWasReset = true;
var TimeIsStopped = false;
var blockDiv = null;
var lineDiv = null;
var textString = "";
var deletes = 0;
var totalDeletes = 0;
var lineDivText = null;
var testCounter = 0;
var wbkScore = 0;
var startTime = 0;
var multiplier = 1;
var nextWord = true;
var typeSpeedSummed = 0;
var numCorrectWords = 0;
var timeoutStart = 0;
var inputHint = "";
var atStart = true;

var alphabetizedUsedWordsBank = [];
var chronologicalUsedWordsBank = [];
var badUsedWordsBank = [];

var wordBank = [];
var wordBankUnusedWords = [];
var startingLetters = "";
var letterStartBank = ["abs","acc","act","adv","aer","aff","air","ali","all","amb","amp","ana","ang","ani","ann","ant","apo","app","arc","art","ass","ast","att","aut","bac","bal","ban","bar","bas","bat","bea","bec","bed","beg","bel","ben","bes","bil","bio","bir","bla","ble","blo","blu","bon","boo","bor","bou","bra","bre","bri","bro","bru","bul","bur","bus","but","cab","cal","cam","can","cap","car","cas","cat","cel","cen","cer","cha","che","chi","cho","chr","chu","cir","cla","cle","cli","clo","coa","coc","cod","coe","col","com","con","coo","cop","cor","cos","cou","cra","cre","cri","cro","cru","cry","cul","cur","cut","cyc","dea","deb","dec","def","del","dem","den","dep","der","des","det","dev","dia","dic","dip","dis","div","dog","dow","dra","dre","dri","dro","ear","eff","ele","emb","emp","enc","end","eng","ens","ent","epi","equ","esc","est","eth","exc","exe","exo","exp","ext","fac","fan","far","fat","fel","fer","fil","fin","fir","fis","fla","fle","fli","flo","flu","foo","for","fra","fre","fri","fro","fun","fur","gal","gam","gar","gas","gen","geo","gla","glo","gra","gre","gri","gro","gru","hal","han","har","hea","hel","hem","her","het","his","hol","hom","hon","hor","hou","hum","hyd","hyp","ill","imm","imp","ina","inc","ind","ine","inf","inh","ins","int","inv","irr","iso","kin","lac","lam","lan","lar","lat","lea","leg","lib","lig","lim","lin","lit","loc","log","mac","mag","mai","mal","man","mar","mas","mat","mea","med","meg","mel","men","mer","mes","met","mic","mid","mil","min","mis","mod","mon","moo","mor","mot","mou","mul","mus","mut","nat","neu","non","obs","off","ost","out","ove","pac","pal","pan","pap","par","pas","pat","pea","ped","pen","per","pet","pha","phe","pho","phy","pic","pil","pin","pla","ple","plu","pol","por","pos","pot","pra","pre","pri","pro","psy","pul","pun","pur","pyr","qua","que","qui","rac","rad","ram","ran","rat","rea","reb","rec","red","ree","ref","reg","reh","rei","rel","rem","ren","rep","res","ret","rev","rou","sac","sal","san","sap","sar","sat","sca","sch","sco","scr","scu","sea","sec","sel","sem","sen","ser","sha","she","shi","sho","shr","sig","sil","sim","sin","ski","sla","sle","sli","slo","slu","sna","sni","sno","soc","sol","sor","sou","spa","spe","spi","spl","spo","spr","squ","sta","ste","sti","sto","str","stu","sub","suc","sul","sun","sup","sur","swa","swe","swi","sym","syn","tab","tac","tal","tan","tar","tea","tel","ten","ter","tet","the","thi","thr","tim","tin","tit","top","tor","tou","tra","tre","tri","tro","tru","tur","twi","ult","una","unb","unc","und","une","unf","unh","uni","unl","unm","unp","unr","uns","unt","unw","val","var","ven","ver","vic","vis","vol","war","was","wat","wea","whe","whi","wil","win","wit","woo","wor"];

var normWordScoreBank = [1,2,4,6,9,12,16,20,25,30,36,42,49,56,64,72,81,90,100,110,121,132,144,157,170,184,198];
var easyWordScoreBank = [1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8]; 
var wordScoreBank = [];


$(document).ready(function(){
	
	if($(window).width() < 895){
		m1_width = 30;
		m2_width = 38;
		m3_width = 46;
		m4_width = 60;
	} else {
		m1_width = 40;
		m2_width = 50;
		m3_width = 60;
		m4_width = 78;
	}

	$('#highScoreContainer').on("click", ".SLtoClick", function (){
		var LS_elem = $('#slbLettersSelect');
		LS_elem.val($("#slbLettersSelect option:contains('" + this.innerHTML + "')").val());
		LS_elem.selectmenu('refresh');
		Reset(60,'timer',true);
	});
	
	var myInput = document.getElementById('gInput');
	myInput.onpaste = function(e) {
	   e.preventDefault();
	 }
	 	
	 $("#xStatsToggleBut").click(function () {
	  
		if($('#moreLessStats').html() == "More")
		{
			$('#extraStatsHidden').show();					// consider adding top word score, top score, average typing speed, and difficulty/hideshow word hints
			$('#moreArrow').css('display', 'none');
			$('#lessArrow').css('display', 'inline-block');
			$('#moreLessStats').html("Less");
			$('.wordBank').css('min-height', '410px');
		}
		else
		{
			$('#extraStatsHidden').hide();
			$('#lessArrow').css('display', 'none');
			$('#moreArrow').css('display', 'inline-block');
			$('#moreLessStats').html("More");
			$('.wordBank').css('min-height', '350px');
		}
			
    });

	$("#wordHintCB").change(function () {
		if( $('#wordHintCB').is(':checked') ){
			GetNewWordHint();
		} else {
			$('#inputHint').val(startingLetters);
		}
		$('#gInput').focus();    //assume user wants to type after clicking word hint checkbox
	});
	
	$('#gInput').focus(function() {
	  $('#inputHint').addClass("redGlow");
	});

	$('#gInput').blur(function() {
	  $('#inputHint').removeClass("redGlow");
	});
		
	$("#gInput").keydown(function(event) {
		return CreateTimer(event, 'timer', 60 /* Round Time */);
    });
	
	$("#gInput").keyup(function(event) {
		UpdateWordHint();
    });
});

function populateWordBank() {

	var fileName = "words/ThreeLetBD/" + startingLetters.toUpperCase() + "_WORDS.txt";
	
	jQuery.ajax({
        type: "GET",
		url: fileName,
		async: true,
		error: function(){
			return false;
		},
		success: function(data) { 
			data = (data.substr(-1) == ",") ? data.substr(0, data.length - 1) : data;
			data.replace(/\s/g, "");
			wordBank = data.split(",");
			wordBankUnusedWords = wordBank.slice(0);
		}
	});
}

function checkWord(wordToFind){

	var wordList;
	var wordFound = false;
	
	$('#gInput').val("");		// first clear input
	
	// check to make sure word starts with required first 2 letters
	if(wordToFind.substr(0,3) != startingLetters)
		return false;

	var lcv = 0;
	while( !wordFound && lcv <= wordBank.length)
	{
		if(wordToFind == wordBank[lcv])
			wordFound = true;
		lcv++;
	}
	
	return wordFound;
}

function checkForDuplicates(wordToCheck){
	var wordList;
	var isDuplicate = false;
	
	$('#gInput').val("");		// first clear input
	
	var lcv = 0;
	while( !isDuplicate && lcv <= chronologicalUsedWordsBank.length)
	{
		if(wordToCheck == chronologicalUsedWordsBank[lcv])
			isDuplicate = true;
		else if(wordToCheck == badUsedWordsBank[lcv])
			isDuplicate = true;
		lcv++;
	}
	return isDuplicate;
}

function getHighScores(){
	var myJSONdata;
	$.post('/getData.php', { type: "fft", SLB_letters: startingLetters},
		function(output){
			myJSONdata = $.parseJSON(output);
			var currDateTime = new Date(myJSONdata["currDateTime"].replace(/-/g,'/'));
			var loggedInUname = myJSONdata["loggedInUsername"];
			var inMaintenanceMode = myJSONdata["maint_mode"];
			var rhs = myJSONdata["allUsersCurrentResults"];
			var rhss = myJSONdata["allUsersResults"];
			var phs = myJSONdata["loggedInCurrentResult"];
			var phss = myJSONdata["loggedInResults"];
			
			var rhsHTML = '';
			var rhssHTML = '';
			var phsHTML = '';
			var phssHTML = '';
			
			if(inMaintenanceMode){
				rhsHTML = '<table><tr><td class="orangeCTA largeBtn" colspan="6"><div>Site is under maintenance</div><div>High scores are temporarily disabled and will not be saved!</div></td></tr></table>';
				rhssHTML = '<table><tr><td class="orangeCTA largeBtn" colspan="6"><div>Site is under maintenance</div><div>High scores are temporarily disabled and will not be saved!</div></td></tr></table>';
				phsHTML = '<table><tr><td class="orangeCTA largeBtn" colspan="3"><div>Site is under maintenance</div><div>High scores are temporarily disabled and will not be saved!</div></td></tr></table>';
				phssHTML = '<table><tr><td class="orangeCTA largeBtn" colspan="5"><div>Site is under maintenance</div><div>High scores are temporarily disabled and will not be saved!</div></td></tr></table>';
			} else {
				rhsHTML = '<table><tr><td class="blueCTA largeBtn" colspan="6">Nobody has a recent high score for \'<span class="SLB_letters">?</span>\' yet!</td></tr></table>';
				rhssHTML = '<table><tr><td class="blueCTA largeBtn" colspan="6">No one has any recent high scores yet!</td></tr></table>';
				phsHTML = '<table><tr><td class="blueCTA largeBtn" colspan="3">You do not have a score for \'<span class="SLB_letters">?</span>\' yet</td></tr></table>';
				phssHTML = '<table><tr><td class="blueCTA largeBtn" colspan="5">You do not have any scores yet!</td></tr></table>';
			}
			
			
			if(rhs != null && rhs.length > 0){
				rhsHTML = '<table><tr><th>#</th><th class="shortCol">Start Letters</th><th>Name</th><th>Score</th><th>Words</th><th>Time Ago</th></tr>';
				var decoration = '';
				var adjustedName = '';
				
				for(var ii = 0; ii < rhs.length; ii++){
					if(rhs[ii].uname == loggedInUname)
						decoration = ' class="uname borderRad_4 orangeBkgd boldFont centerTxt"';
					else
						decoration = ' class="uname"';
					
					if(rhs[ii].uname.length > 13)
						adjustedName = rhs[ii].uname.substring(0, 11) + "...";
					else
						adjustedName = rhs[ii].uname;
					
					rhsHTML += '<tr><td>' + (ii + 1) + '</td><td class="shortCol SLtoClick">' + rhs[ii].typeLetters + '</td><td' + decoration + ' title="' + rhs[ii].uname + '"><a href="/user/' + rhs[ii].uname + '/test-stats"><img class="circle-image tinyPic" src="' + rhs[ii].profile_pic + '"><span class="adjName">' + adjustedName + '</td><td>' + rhs[ii].val + '</td><td>' + rhs[ii].numWords + '</td><td class="convertToTimeAgo">' + rhs[ii].dt + '</td></tr>';
				}
				rhsHTML += '</table>';
			}
			
			$('#recentCurrentHighScores').html(rhsHTML);
			
			if(rhss != null && rhss.length > 0){
				rhssHTML = '<table><tr><th>#</th><th class="shortCol">Start Letters</th><th>Name</th><th>Score</th><th>Words</th><th>Time Ago</th></tr>';
				var decoration = '';
				var adjustedName = '';
				
				for(var ii = 0; ii < rhss.length; ii++){
					if(rhss[ii].uname == loggedInUname)
						decoration = ' class="uname borderRad_4 orangeBkgd boldFont centerTxt"';
					else
						decoration = ' class="uname"';
					
					if(rhss[ii].uname.length > 13)
						adjustedName = rhss[ii].uname.substring(0, 11) + "...";
					else
						adjustedName = rhss[ii].uname;
					
					rhssHTML += '<tr><td>' + (ii + 1) + '</td><td class="shortCol SLtoClick">' + rhss[ii].typeLetters + '</td><td' + decoration + ' title="' + rhss[ii].uname + '"><a href="/user/' + rhss[ii].uname + '/test-stats"><img class="circle-image tinyPic" src="' + rhss[ii].profile_pic + '"><span class="adjName">' + adjustedName + '</span></a></td><td>' + rhss[ii].val + '</td><td>' + rhss[ii].numWords + '</td><td class="convertToTimeAgo">' + rhss[ii].dt + '</td></tr>';
				}
				rhsHTML += '</table>';
			}
			
			$('#recentHighScores').html(rhssHTML);
			
			$('#wbkHighScore').html('-');
			if(!inMaintenanceMode){
				if(phs == 'not logged in'){
					phsHTML = '<table><tr><td class="orangeCTA mediumBtn" colspan="3"><a href="/register.php">Register</a> or <a href="/login.php">Login</a> to keep track of your scores.</td></tr></table>';
				} else if(phs != null && phs.length > 0){
					phsHTML = '<table class="table3Col"><tr><th>Score</th><th>Words</th><th>Time Ago</th></tr>';
					phsHTML += '<tr><td>' + phs[0].val + '</td><td>' + phs[0].numWords + '</td><td class="convertToTimeAgo">' + phs[0].dt + '</td></tr>';
					phsHTML += '</table>';
					$('#wbkHighScore').html(phs[0].val);
				}
			}
			
			$('#personalHighScore').html(phsHTML);
			
			if(!inMaintenanceMode){
				if(phss == 'not logged in'){
					phssHTML = '<table><tr><td class="orangeCTA mediumBtn" colspan="5"><a href="/register.php">Register</a> or <a href="/login.php">Login</a> to keep track of your scores.</td></tr></table>';
				} else if(phss != null && phss.length > 0){
					phssHTML = '<table class="table5Col"><tr><th>#</th><th>Start Letters</th><th>Score</th><th>Words</th><th>Time Ago</th></tr>';
					var decoration = '';
					
					for(var ii = 0; ii < phss.length; ii++){
						if(phss[ii].typeLetters == startingLetters)
							decoration = ' class="borderRad_4 orangeBkgd boldFont centerTxt"';
						else
							decoration = ' class="SLtoClick"';
						
						phssHTML += '<tr><td>' + (ii + 1) + '</td><td' + decoration + '>' + phss[ii].typeLetters + '</td><td>' + phss[ii].val + '</td><td>' + phss[ii].numWords + '</td><td class="convertToTimeAgo">' + phss[ii].dt + '</td></tr>';
					}
					phssHTML += '</table>';
					
					// show delete scores button
					$('#clearDataBtn').show();
				}
			}
			
			$('#personalHighScores').html(phssHTML);
			
			$('.convertToTimeAgo').each(function(){
				 var earlyDate = new Date($(this).html().replace(/-/g,'/'));
				 var timeDiffString = readableTimeDifference(earlyDate, currDateTime);
				 $(this).html(timeDiffString);
				 if(timeDiffString.indexOf("mins") > 0 || timeDiffString.indexOf("secs") > 0)
					$(this).addClass("borderRad_4 dkBlueBkgd boldFont centerTxt");
			 });
			 
			var elems = $('.SLB_letters');
		
			for(var ii = 0; ii < elems.length; ii++){
				elems[ii].innerHTML = startingLetters;
			}
			
			var slbSelElem = $('#slbLettersSelect');
			
			if(slbSelElem.val() == "RANDOM"){
				$('#slbLettersSelectDropDown .ui-selectmenu-text').html(startingLetters);
			}
	});
}

function getStartingLetters(index){
	
	if(index == null || index == undefined || index.toUpperCase() == "RANDOM")
		index = Math.floor(Math.random()*letterStartBank.length);

	startingLetters = letterStartBank[index];
	populateWordBank();
	
	getHighScores();	
}

function increaseMultiplier(doBreathing) {
	var multChanged = false;
	
	if(multiplier == 1)	{
		multiplier = 2;
		multChanged = true;
	} else if(multiplier == 2) {
		multiplier = 3;
		multChanged = true;
	} else if(multiplier == 3) {
		multiplier = 5;
		multChanged = true;
	}
	document.getElementById('multText').innerHTML = "x" + multiplier;
	
	if(typeof doBreathing === "undefined"){
		doBreathing = true;
	}
	
	animateMultiplier(multiplier, {"multChanged" : multChanged, "doBreathing" : doBreathing});
}

function resetMultiplier()
{
	multiplier = 1;
	document.getElementById('multText').innerHTML = "x" + multiplier;
	animateMultiplier(multiplier);
}

function animateMultiplier(mult, opts)
{
	if (typeof opts !== 'undefined') {
		if(typeof(opts['doBreathing']) == "undefined"){
			opts['doBreathing'] = true;
		}
	}
	
	var multBBElem = $('#multBillboard');
	var centerEntryModuleElem = $('#centerEntryModule');
	
	// add $(object).stop() to beginning off every mult routine
	if(mult == 1)
	{
		multBBElem.clearQueue();
		multBBElem.stop();
		
		if(!atStart){
			centerEntryModuleElem.removeClass("level_2 level_3 level_4 redGlow").addClass("level_1");
		}
		  
		multBBElem.animate({ 
			width: [m1_width + "px", "easeOutBounce"],
			height: [m1_width + "px", "easeOutBounce"],
			lineHeight: m1_width + "px",
			'padding-top': "0px",
			border: "4px solid #5B4668",
			borderTopColor: "#055485",// PURPLE "#5B4668",
			borderBottomColor: "#055485",
			borderLeftColor: "#055485",
			borderRightColor: "#055485",
			backgroundColor: "#a7dfff", // LIGHT PURPLE: "#E6DCF2"
			color: "#1468bc",
			'-webkit-border-radius': "0px",
			'-moz-border-radius': "0px",
			'border-radius': "0px"
		  }, 1200 );
	}
	else if(mult == 2)
	{
		multBBElem.clearQueue();
		multBBElem.stop();
		centerEntryModuleElem.removeClass("level_1 level_3 level_4").addClass("level_2");
		multBBElem.animate({ 
			width: m2_width + "px",
			height: m2_width + "px",
			lineHeight: m2_width + "px",
			border: "4px solid #5B4668",
			borderTopColor: "#a59800",// PURPLE "#5B4668",
			borderBottomColor: "#a59800",
			borderLeftColor: "#a59800",
			borderRightColor: "#a59800",
			backgroundColor: "#ffffa5", // LIGHT PURPLE: "#E6DCF2"
			color: "#938700"
		  }, 400, function() {
			  if(opts['doBreathing'] == true){
					BreathObject(multBBElem, 700, "0px", "0px", m2_width + "px", m2_width + "px", 140, "15px", "8px", "58px", "56px", 180, 2);
				}
		  });
		  
			SpinObject($('#multBillboard'), 400, 40, 1);
	}
	else if(mult == 3)
	{
		multBBElem.clearQueue();
		multBBElem.stop();
		centerEntryModuleElem.removeClass("level_1 level_2 level_4").addClass("level_3");
		multBBElem.animate({ 
			width: m3_width + "px",
			height: m3_width + "px",
			lineHeight: m3_width + "px",
			borderTopColor: "#F75900", // BLUE: "#1468BC",
			borderBottomColor: "#F75900", // LIGHT BLUE: "#1468BC",
			borderLeftColor: "#F75900",
			borderRightColor: "#F75900",
			backgroundColor: "#ffe9d0",// LIGHT BLUE: "#C9E4FC"
			color: "#F75900"
			
		  }, 400, function() {
				if(opts['doBreathing'] == true){
					BreathObject(multBBElem, 400, "0px", "0px", m3_width + "px", m3_width + "px", 120, "20px", "10px", "72px", "70px", 140, 3);
				}
		  });
		  
		  SpinObject($('#multBillboard'), 300, 40, 1);
		  
		multBBElem.css("text-align","center");
	}
	else if(mult == 5)
	{
		multBBElem.clearQueue();
		multBBElem.stop();
		centerEntryModuleElem.removeClass("level_1 level_2 level_3").addClass("level_4");
		if(opts['multChanged'] == true)
		{
			$('#multBillboard').animate({ 
				width: m4_width + "px",
				height: m4_width + "px",
				lineHeight: m4_width + "px",
				borderTopColor: "#950000",
				borderBottomColor: "#950000",
				borderLeftColor: "#950000",
				borderRightColor: "#950000",
				backgroundColor: "#c02121",
				color: "#ffeded"
			}, 400, function() {
				if(opts['doBreathing'] == true){
					BreathObject(multBBElem, 200, "0px", "0px", m4_width + "px", m4_width + "px", 90, "24px", "10px", "92px", "88px", 100, 5);
				  }
			  });
			
		}
		
		//$('#centerEntryModule').addClass("redGlow");
		//$('#centerEntryModule').css('border-color', '#ed4b4b' /*#E51C12*/);
		//$('#centerEntryModule').css('background-color', '#FFDFDF');/*#f7adad, FCD1D2*/
		$('#centerEntryModule').removeClass("level_1 level_2 level_3").addClass("level_4");
		
		SpinObject(multBBElem, 200, 40, 1);
	}
}

function BreathObject(object, sleep, normRadius, normPadTop, normX, normY, normDelay, exRadius, exPadTop, expandX, expandY, expandDelay, multStarter)
{
	// breath 1 time immediately after call
	if (multiplier == multStarter)
	{
	   object.stop().animate({ 
			'width': expandX,
			'height': expandY,
			'-webkit-border-radius': exRadius,
			'-moz-border-radius': exRadius,
			'border-radius': exRadius,
			'padding-top': exPadTop
			
			}, expandDelay, function() {
				object.stop().animate({ 
					'width': normX,
					'height': normY,
					'-webkit-border-radius': normRadius,
					'-moz-border-radius': normRadius,
					'border-radius': normRadius,
					'padding-top': normPadTop
					
				},normDelay)
		});
	}
			
	// now breath once every "sleep" milliseconds
	if (multiplier == multStarter)
	{
		doTimer(sleep, 1, function(steps)
		{
		},
		function()
		{
			if (multiplier == multStarter)
			{
			   object.stop().animate({ 
					'width': expandX,
					'height': expandY,
					'-webkit-border-radius': exRadius,
					'-moz-border-radius': exRadius,
					'border-radius': exRadius,
					'padding-top': exPadTop
					
					}, expandDelay, function() {
						object.stop().animate({ 
							'width': normX,
							'height': normY,
							'-webkit-border-radius': normRadius,
							'-moz-border-radius': normRadius,
							'border-radius': normRadius,
							'padding-top': normPadTop
						},normDelay)
				});
			}
			if(multiplier == multStarter)
				BreathObject(object, sleep, normRadius, normPadTop, normX, normY, normDelay, exRadius, exPadTop, expandX, expandY, expandDelay, multStarter);
		});
	}
}

function SpinObject(object, timeLength, fps, numTurns)
{
	numTurns = numTurns * 360;

	var toRotate = 0;
	doTimer(timeLength, fps, function(steps)
	{		
		toRotate = toRotate + ( numTurns / steps);
		
		var rotateDegs = "rotate(" + toRotate + "deg)";
		
		object.css("transform",rotateDegs),
		object.css("-webkit-transform",rotateDegs),
		object.css("-moz-transform",rotateDegs),
		object.css("-o-transform",rotateDegs),
		object.css("-ms-transform",rotateDegs)
	},
	function()
	{
		object.css("transform","rotate(0deg)"),
		object.css("-webkit-transform","rotate(0deg)"),
		object.css("-moz-transform","rotate(0deg)"),
		object.css("-o-transform","rotate(0deg)"),
		object.css("-ms-transform","rotate(0deg)")
	});
}

function doTimer(length, resolution, oninstance, oncomplete) {
    var steps = Math.ceil((length / 100) * (resolution / 10)),
        speed = length / steps,
        count = 0,
        start = new Date().getTime();
    function instance() {
        if(count++ == steps){
            oncomplete(steps, count);
        }
        else {
            oninstance(steps, count);
            var diff = (new Date().getTime() - start) - (count * speed);
            window.setTimeout(instance, (speed - diff));
        }
    }
    window.setTimeout(instance, speed);
}

function InitializeTimer(Time, TimerID){
    
    Timer = document.getElementById(TimerID);
    TotalSeconds = Time;
	
	$('#gInput').val("");
   
}

function CreateTimer(event, TimerID, Time) {

	var wordFound;
	var wordIsDuplicate;
	
	if(event.key == "Shift"){
		return false;
	} else if(event.key == "Enter" && event.shiftKey ){
		return false;
	}
	
	if(atStart){
		$('.topLeftLink').hide();
		resetMultiplier();
		$('#multBox').show();
		$('#centerEntryModule').removeClass("level_2 level_3 level_4").addClass("level_1");
		atStart = false;
	}
	
	if( nextWord == true ){
		startTime = getTimeElapsed();
		nextWord = false;
	}

    
    if (TotalSeconds==undefined || TimeWasReset) {
		$('#slbLettersSelect').selectmenu("disable");
		
        TimeWasReset = false;
        RoundTime = Time;
		startTime = 0;
		

        Timer = document.getElementById(TimerID);
        TotalSeconds = Time;

        UpdateTimer()
		timeoutStart = (new Date).getTime();
        window.setTimeout("Tick()", 100);
    }
    
    if (TimeWasReset){
        TotalSeconds = Time;
        TimeWasReset = false;   //reset TimeWasReset flag        
    }
	    
    // This makes the box disabled when time is up (end if all text has been typed or after 30 mins (1800 secs)
    if (TotalSeconds == 0){
        return false;
    }
	
	var key_entered = keycode.getValueByEvent(event);
	
	
	if(key_entered == "return" || key_entered == " ")
	{
		var wordToCheck = $('#gInput').val();
		wordToCheck = wordToCheck.toLowerCase();
		wordFound = checkWord(wordToCheck);
		wordIsDuplicate = checkForDuplicates(wordToCheck);

		if(wordFound && !wordIsDuplicate){
		
			var entryWPM = ( (wordToCheck.length / 5) / (getTimeElapsed() - startTime)  ) * 60
			var entryScore = 200;
			
			// See if word hints are enabled to decide which word score bank to use
			if($('#wordHintCB').is(':checked')/*$('#diffTypeSelected').val() == "EasyMode"*/)
				wordScoreBank = easyWordScoreBank;
			else
				wordScoreBank = normWordScoreBank;
		
		
			if( (2 < wordToCheck.length) && (wordToCheck.length < wordScoreBank.length + 3) )
				entryScore = wordScoreBank[wordToCheck.length - 3];
			else
				entryScore = wordScoreBank[wordScoreBank.length - 1];
				
			var totalScore = Math.round( (entryWPM * entryScore) / 10);
			
			chronologicalUsedWordsBank.push(wordToCheck);
			$('#numWords').html(chronologicalUsedWordsBank.length);
			
			var wcbox = document.getElementById('WordCaptureBox');
			var para = document.createElement("DIV");
			var t = document.createTextNode(wordToCheck);
			para.appendChild(t)
			wcbox.appendChild(para);
			
			var scoreToAdd = Math.ceil((totalScore * multiplier) / 10) * 10;
			wbkScore += scoreToAdd;
			$('#wbkScore').html(wbkScore);
			
			var extraClass = '';
			var fadeOutTime = 1500;
			if(scoreToAdd > 1000){
				extraClass = " hugeScoreBubble";
				fadeOutTime = 2000;
			} else if(scoreToAdd > 500){
				extraClass = " bigScoreBubble";
				fadeOutTime = 3000;
			}
			
			$('#centerEntryModule').append('<div class="scoreBubble' + extraClass + '">+' + scoreToAdd + '</div>');
			$('.scoreBubble').fadeOut(fadeOutTime);
			
			increaseMultiplier();
			
			document.getElementById('typingSpeed').innerHTML = Math.round( entryWPM );
			document.getElementById('wordValue').innerHTML = entryScore;
			document.getElementById('wordScore').innerHTML = scoreToAdd;
			
			typeSpeedSummed += Math.round(entryWPM);
			numCorrectWords += 1;
			document.getElementById('aveTypingSpeed').innerHTML = Math.round(typeSpeedSummed / numCorrectWords);
			
		}
		else if (!wordFound && !wordIsDuplicate)
		{
			badUsedWordsBank.push(wordToCheck);
			var bwbox = document.getElementById('badWordCaptureBox');
			var para = document.createElement("DIV");
			var t = document.createTextNode(wordToCheck);
			para.appendChild(t)
			bwbox.appendChild(para);
			resetMultiplier();
		}
		
		if( $('#wordHintCB').is(':checked') )
		{
			GetNewWordHint();
		}
		else
		{
			$('#inputHint').val(startingLetters);
		}
		
		nextWord = true;
		//startTime = getTimeElapsed();
		return false;
	}
	else if(key_entered == "backspace" || key_entered == "delete" || key_entered == "left")
	{
		resetMultiplier();
	}
    
}

function Tick() {
	var timeoutEnd = 0;
	
    if (TotalSeconds <= 0.1) {
        TotalSeconds = 0;
        UpdateTimer()
      
        Timer.innerHTML = '<span class="endTimer">00:00</span>';
        //UpdateStats()
        TimeEnd();
        return;
    }

    if(!TimeWasReset){
	timeoutEnd = (new Date).getTime();
    TotalSeconds -= (timeoutEnd - timeoutStart)/1000;//0.1;
    UpdateTimer()
	timeoutStart = (new Date).getTime();
    window.setTimeout("Tick()", 100);
    }
}

function UpdateTimer() {
    var Seconds = TotalSeconds;
    var Minutes = Math.floor(Seconds / 60);
    Seconds -= Minutes * 60;
	Seconds = Math.floor(Seconds);
    var TimeStr = (LeadingZero(Minutes) + ":" + LeadingZero(Seconds))
    Timer.innerHTML = TimeStr;
}

function LeadingZero(Time) {
    return (Time < 10) ? "0" + Time : + Time;
}

function Reset(ResetTime, TimerID, getNewStartLetters, callback) {
    $('.topLeftLink').show();
	atStart = true;
    Timer = document.getElementById(TimerID);
    TotalSeconds = ResetTime;
	wbkScore = 0;
	typeSpeedSummed = 0;
	numCorrectWords = 0;
	$('#wbkScore').html("");
	
    UpdateTimer();   
	Initialize();
    //UpdateStats();

    InitializeTimer(ResetTime, TimerID);
	$('#resetBtn').css("visibility", "hidden");
	$('#scoreDiv').removeClass("borderRad_4 orangeBkgd boldFont centerTxt");
    TimeWasReset = true;
	
	var slbLettersElem = $('#slbLettersSelect');
	
	if(getNewStartLetters){
		getStartingLetters(slbLettersElem.val());
	}
	
	if(slbLettersElem.selectmenu("option", "disabled")){
		slbLettersElem.selectmenu("enable");
	}
		
	if( $('#wordHintCB').is(':checked') ){
		GetNewWordHint();
	} else {
		$('#inputHint').val(startingLetters);
	}
    
    $('#gInput').focus();    //assume user wants to type after clicking reset
	
	if(callback){
		callback();
	}
}

function Initialize(){
	chronologicalUsedWordsBank = [];
	alphabetizedUsedWordsBank = [];
	$('.scoreBubble').remove();
	
	document.getElementById('WordCaptureBox').innerHTML = "";
	document.getElementById('badWordCaptureBox').innerHTML = "";
	
	document.getElementById('aveTypingSpeed').innerHTML = "0";
	document.getElementById('typingSpeed').innerHTML = "0";
	document.getElementById('wordValue').innerHTML = "0";
	document.getElementById('wordScore').innerHTML = "0";
	
	$('#centerEntryModule').removeClass("level_2 level_3 level_4 redGlow").addClass("level_1");
	
	resetMultiplier();
}

function TimeEnd(){
	$.post('/setData.php', { type: "fft", score: wbkScore, num_words: chronologicalUsedWordsBank.length, fft_Type: startingLetters},
		function(output){
	});

	$('#resetBtn').css("visibility", "visible");
	
	var currScoreElem = $('#wbkScore');
	var highScoreElem = $('#wbkHighScore');
	
	if(parseInt(currScoreElem.html()) > parseInt(highScoreElem.html()) || highScoreElem.html() == '-')
		$('#scoreDiv').addClass("borderRad_4 orangeBkgd boldFont centerTxt");
		//$('#topScore').html($('#wbkScore').html());
		
	resetMultiplier();
}

function GetNewWordHint() {
	var wordHintIndex = Math.floor(Math.random()*wordBankUnusedWords.length);
	inputHint = wordBankUnusedWords[wordHintIndex];
	$('#inputHint').val(inputHint);
	wordBankUnusedWords.splice(wordHintIndex, 1);
}

function UpdateWordHint() {
	var inputWordSoFar = $('#gInput').val();
	
	if( $('#wordHintCB').is(':checked') ){
		// hide the hint if the entered string is not matching the hint word
		if(inputWordSoFar == inputHint.substr(0,inputWordSoFar.length) )
			$('#inputHint').val(inputHint);
		else
			$('#inputHint').val("");
	} else {
		if(inputWordSoFar == startingLetters.substr(0, inputWordSoFar.length))
			$('#inputHint').val(startingLetters);
		else
			$('#inputHint').val("");
	}
}

function formatForInnerHTML(StringToFormat){
    var StringToReturn = "";
    StringToFormat = StringToFormat.replace(/> </g, ">&nbsp;<");
    StringToReturn = '<div id="line_input">' + StringToFormat + "</div>";

    return StringToReturn;
}

function formatForInnerHTML_block(StringToFormat){
    
    var StringToReturn = "";
    
    for(i=0; i < 4; i++){
        StringToFormat[i] = StringToFormat[i].replace(/> </g, ">&nbsp;<");
        StringToReturn += '<div id="blockLine' + i + '" class="blockLines">' + StringToFormat[i] + "</div>";
    }

    return StringToReturn;
}

function getTimeElapsed(){
    if(typeof Timer == "undefined"){
        return TotalSeconds;
    }
    return (RoundTime - TotalSeconds);
}

function setupFocus(){
    $('.mainDivInputs').bind('click', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('mouseleave', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('mouseout', function() {
        this.focus();
    });
    
    $('.mainDivInputs').focus(function() {
        ApplyColorsFocused();
    });
    
    $('.mainDivInputs').blur(function() {
        ApplyColorsBlurred();
    });
}

keycode = {
    getKeyCode : function(e) {
        var keycode = null;
        if(window.event) {
            keycode = window.event.keyCode;
        }else if(e) {
            keycode = e.which;
        }
        return keycode;
    },
    getKeyCodeValue : function(keyCode, shiftKey) {
        shiftKey = shiftKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.modifiedByShift[keyCode];
        }else {
            value = this.keyCodeMap[keyCode];
        }
        return value;
    },
    getValueByEvent : function(e) {
        return this.getKeyCodeValue(this.getKeyCode(e), e.shiftKey);
    },
    keyCodeMap : {
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"capslock", 27:"escape", 32:" ", 33:"pageup",
        34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"+", 44:"printscreen", 45:"-", 46:"delete", 47:"/",
        48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:";",
        61:"=", 65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l",
        77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z",
        96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9",
        106: "*", 107:"+", 109:"-", 110:".", 111: "/",
        112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12",
        144:"numlock", 145:"scrolllock", 186:";", 187:"=", 188:",", 189:"-", 190:".", 191:"/", 192:"`", 219:"[", 220:"\\", 221:"]", 222:"'"
    },
    modifiedByShift : {
        32: " ", 65:"A", 66:"B", 67:"C", 68:"D", 69:"E", 70:"F", 71:"G", 72:"H", 73:"I", 74:"J", 75:"K", 76:"L",
        77:"M", 78:"N", 79:"O", 80:"P", 81:"Q", 82:"R", 83:"S", 84:"T", 85:"U", 86:"V", 87:"W", 88:"X", 89:"Y", 90:"Z", 192:"~", 48:")", 49:"!", 50:"@", 51:"#", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 109:"_", 61:"+",
        219:"{", 221:"}", 220:"|", 59:":", 222:"\"", 186:":", 187:"+", 188:"<", 189:"_", 190:">", 191:"?"/*,
        96:"insert", 97:"end", 98:"down", 99:"pagedown", 100:"left", 102:"right", 103:"home", 104:"up", 105:"pageup"*/
    }
};