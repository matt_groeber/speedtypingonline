<?php
$currPage = "classes";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];
$db = DB::getInstance();
$username = Input::get('user');

// first see if no username has been passed in
if($username === ''){
	
	Redirect::to('typing-test.php');
}

$user = new User();
$userData = $user->data();
$debugMode = $user->hasPermission('debug') || Config::get('constants/debugMode');

if($userData->username != $username){
	
	// restrict user profile to be viewed by only the actual user when logged in (aside from exception where user has changed their username and submitted)
	if(!Session::exists('updated') && !Session::exists('failed')){
		if(inMaintenanceMode()){
			require_once 'includes/overall/header.php';
			echo '<h2>Site is under Maintenance</h2><h4>User profile pages will be available shortly.</h4><h4>Please continue to use the site as normal but be aware that test, lesson, and game high score data will not be saved.</h4><h4>Sorry for any inconvenience and thank you for your patience!</h4>';
			exitPHPwithFooter();
		} else {
			Redirect::to('/typing-test.php');
			//Redirect::to('/user/' . $username . '/profile');
		}
	} else {
		/*echo 'in here';
		var_dump('/user/' . $username . '/profile');
		exit();*/
		Redirect::to('/user/' . $username . '/test-stats');
	}
}

$userToProfile = $user;
$onPersonalProfile = true;

$htmlTitle = "Classes, " . $username;
$htmlDescription = "Student list for " . $username;
$cssFiles = "behkiw";
$noWidgets = true;
$jQueryUI = true;
$fontAwesomeCDN = true;
$addContainer = true;
require_once 'includes/overall/header.php';
?>
<div class="borderBottom">
<?php
include 'includes/profileMenu.php';
?>
</div>
</div> <!-- <div id="heading"> -->

<div class="hudContainer_horiz flex">
	<div class="flex">
		<div id="genAdWrap" class="flex-7">
			<?php echo getAd("generalLeadAd", null, $debugMode, $user->isPremium());?>
			<!-- <div id="asideWrap"> -->
		</div>
		<div id="tfFilterContainer" class="flex-3">
			<h4>Timeframe Filter:</h4>
			
			<div id="reportrange" class="stoUI stoUIClickable" style="cursor: pointer;">
				<i class="fa fa-calendar"></i>
				<span></span> <i class="fa fa-caret-down"></i>
			</div>
			
			<?php
				if($user->isTeacher()){
					//require_once 'includes/widgets/createAssignment.php';
				}
			?>
		</div>
	</div>
</div>
	
<div id="pageMain">
	
<?php
	if(Session::exists('failed')){
		echo Session::flash('failed');
	} else {
		echo Session::flash('updated');
	}
	
	if(!$user->isTeacher()){
?>
		 <h2>You are not a teacher</h2> 
<?php
	} else {
?>

	<div>
		<div class="flex">
			<div id="listOfClasses" class="flex-1">
				<h2>Classes</h2>
				
		<?php
			$loggedInID = ($userToProfile->isLoggedIn()) ? $userToProfile->data()->id : null;
			$classroomObj = new Classroom($loggedInID);
			$lessonsObj = new Lessons();
			$classes = $classroomObj->getClasses(false);
			$assignmentTypesArr = $classroomObj->GetAssignmentTypes();
			$lessonsArr = $lessonsObj->GetLessons();
			$textTypes = $db->get('`text_types`', array('tid', '>', '0'), 'tid, display_text')->results();
			$studentUserData = $classroomObj->getClasses(true);
			$assignments = $classroomObj->GetAssignments('all', array('is_assigned', '=', '1'));
			//var_dump($lessonsArr);
			//exit();
			//var_dump($studentUserData);
			
			
			//echo '<br />';
			//echo '====================================<br />';
			//echo 'Starting Here<br /><br />';
			$studentHistoryArr = $classroomObj->getStudentHistory();
			$studentLessonHistArr = $studentHistoryArr[0];
			$studentTestHistArr = $studentHistoryArr[1];
			$studentHistoryArr = array_merge($studentLessonHistArr, $studentTestHistArr);
			//var_dump($studentArr);
			//exit();
			
			foreach($classes as $key => $classValue){
				?>
				<div id="classID_<?php echo $classValue->id; ?>" class="usfContainer stoInfoBox">
					<h3 class="classroomName"><?php echo $classValue->class_name;?></h3>
					
					<div class="classCode">
						<div>
							<div class="smallLabel">Join Code</div><?php 
						
							if($classValue->ccode == ''){
								$classValue->ccode = $classroomObj->generateCCode($classValue->class_id);
							}
								
							echo '<span class="ccToDisplay">' . $classValue->ccode . '</span>';
							?>
							<button onclick="CopyElemHTMLtoClipboard('code_<?php echo $classValue->ccode; ?>')" class="stoUI stoUIClickable stoUIClickableDark">Copy</button>
							<button onclick="ResetClassCode('<?php echo $classValue->id; ?>', '<?php echo 'classID_' . $classValue->id; ?>')" class="ccResetBtn stoUI stoUIClickable stoUIClickableDark">Reset</button>
<?php
							echo '<input id="code_' . $classValue->ccode . '" class="ccToCopy" value="' . $classValue->ccode . '" style="visibility: hidden;"></input>';
						echo '</div>';
					
					$autoJoinChecked = ($classValue->req_confirm == 0) ? " checked" : "";
					echo '<div>';
						echo '<div class="smallLabel">Join Method</div>';
						echo '<div class="field checkBox" title="Students will automatically be added when enabled. If disabled, students will only be added once you confirm them."><label><input id="' . $classValue->ccode . '" name="' . $classValue->ccode . '" type="checkbox" name="allow_email" class="autojoinChkBx" onclick="setClassJoinMethod(this)"' . $autoJoinChecked . '> Auto-Join</label></div>';
					echo '</div>';

?>
					</div>
					<div class="classTableWrap">
						<table id="<?php echo 'class_' . $classValue->id;?>" class="studentTable">
							<tr>
								<td>This class has no students</td>
							</tr>
			
					<?php
					//$studentArr = $classroomObj->getStudents($classValue->class_id);
					//var_dump($studentArr);
					foreach($studentUserData as $key => $studentValue){
						if($studentValue->class_id == $classValue->id){
					?>
							<tr>
								<td>
									<a href="/user/<?php echo $studentValue->username; ?>/lessons"><?php echo $studentValue->username;?></a>
								</td>
								<td>
									<?php echo $studentValue->email;?>
								</td>
								<td>
									<?php echo $studentValue->num_lessons;?>
								</td>
								<td>
									<?php echo $studentValue->num_lessons;?>
								</td>
							</tr>
					
					<?php 
						}
					}
					?>
						</table>
					</div>
					<div class="classBottomDiv">
						<button onclick="RemoveStudentsDialog(this,<?php echo $classValue->id;?>, 'My Title', 'Delete Students')" class="stoUI stoUIClickable stoUIClickableRed removeStudentsBtn <?php echo 'RSclass_' . $classValue->id;?> " disabled>Remove Selected Student(s)</button>
					</div>
					
				</div>
				<?php
			}
		?>
			</div>
		</div>
	</div>
		
</div>

<div id="dialog-confirm" title="Permanently delete students from class?" style="display: none;">
	<div id="extraInfo"></div>
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>All related student data including all student assignment progress will be permanently deleted and cannot be recovered.</p>
	<p>Are you sure?</p>
</div>

<?php
	}
?>
<link rel="stylesheet" href="/css/jquery.transfer.css?v=16" type="text/css">

<script src="/js/general.js?v=27"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>



<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/css/theme.blue.min.css" type="text/css">-->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/jquery.tablesorter.widgets.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.3/js/extras/jquery.tablesorter.pager.min.js"></script>
<script type="text/javascript" src="/js/jquery.transfer.js?v=3"></script>

<script>
	var keyboardArr = <?php echo json_encode($keyboardArr);?>;
	var assignmentTypesArr = <?php echo json_encode($assignmentTypesArr);?>;
	var studentUserData = <?php echo json_encode($studentUserData);?>;
	var studentHistory = <?php echo json_encode($studentHistoryArr);?>;
	var assignments = <?php echo json_encode($assignments);?>;
	var studentDataComplete = new Map();
	var studentClassMap = new Map();
	var startDate;
	var endDate;
	var start = <?php echo GetJSMoment($userData->ol_date_filter)[0]; ?>;
	var end = <?php echo GetJSMoment($userData->ol_date_filter)[1]; ?>;
	
	var studentList;
	var studentsGroupedByClass = new Array();
	var assignmentsGroupedByStudent = new Array();
	var filteredAssignmentsGroupedByStudent = new Array();
	var studentList = new Array();
	var selectedStudents;
	var dueDate;

    var settings = {
        "inputId": "languageInput",
        "data": studentList,
        "groupData": studentsGroupedByClass,
        "itemName": "username",
        "groupItemName": "groupName",
        "groupListName" : "groupData",
        "container": "transfer",
        "valueName": "user_id",
        "callable" : function (data, names) {
            //console.log("Selected ID：" + data);
			selectedStudents = data;
			if(selectedStudents.length == 0 || dueDate == undefined)
				$(".ui-dialog-buttonset button:contains('Create')").button("disable")
			else
				$(".ui-dialog-buttonset button:contains('Create')").button("enable")
        }
    };

	var testGlobals = {userSettings : {} };
	
	function MergeAssignmentsGroupedByStudent(item, index){
		// only deal with assignments assigned to at least 1 student
		if(item.stud_id != null){
		
			stud_idAsStr = item.stud_id.toString();
			if(assignmentsGroupedByStudent[stud_idAsStr] == undefined){
				assignmentsGroupedByStudent[stud_idAsStr] = new Array();
			}
			
			assignmentsGroupedByStudent[stud_idAsStr].push(
				{"assign_id" : item.assign_id, "completed_date" : item.completed_date, "assign_date" : item.assign_date, "due_date" : item.due_date, "goal" : item.goal, "progress" : item.progress, "classroom_id" : item.classroom_id}
				);
		}
	}
	
	$(document).ready(function(){
		
		assignments.forEach(MergeAssignmentsGroupedByStudent);

		$('#reportrange').daterangepicker({
			startDate: start,
			endDate: end,
			opens: 'left',
			applyButtonClasses: "stoUI stoUIClickable solidColor whiteTxt",
			cancelButtonClasses: "stoUI stoUIClickable ",
			ranges: {
			   'Today': [moment(), moment()],
			   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			   'This Week': [moment().startOf('week'), moment().endOf('week')],
			   'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
			   'This Month': [moment().startOf('month'), moment().endOf('month')],
			   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
			   'All Time': [moment('2020-01-01'), moment()]
			}
		}, cb);

		cb(start, end);
		
		
		startDate = start.format('YYYY-MM-DD');
		endDate = end.format('YYYY-MM-DD');
		
		
		studentUserData.forEach(SetupStudentClassroomMaps);
		
		SetupClassrooms();
		$(".topRowHeader th").data("sorter", false);
		
		$('.ui-spinner-button').on('click', function(event) {
			showUpdateButton();
		});
		
		$('.studentTable tbody tr').on('click', function(event){
			jqThis = $(this);
			jqThis.toggleClass("selected");
			//jqThis.parent().find(".selected")
			//jqThis.closest(".usfContainer").find(".removeStudentsBtn")
			if(jqThis.hasClass("selected")){
				jqThis.find(".selectStudChkbx").prop("checked", true);
				jqThis.closest(".usfContainer").find(".removeStudentsBtn").prop("disabled",false);
			} else {
				jqThis.find(".selectStudChkbx").prop("checked", false);
				if(jqThis.parent().find(".selected").length == 0){
					jqThis.closest(".usfContainer").find(".removeStudentsBtn").prop("disabled",true);
				}
			}
		});
		
	});
	
	function cb(start, end) {
		var myLabel = $('#reportrange').data('daterangepicker').chosenLabel;
		if(myLabel != undefined){
			labelHTML = HTMLelemStr($('#reportrange').data('daterangepicker').chosenLabel, 'div', '', 'dateRangeLabel');
		} else {
			labelHTML = HTMLelemStr(GetPickerLabelFromDates(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD')), 'div', '', 'dateRangeLabel');
		}
		
		calendarIconHTML = HTMLelemStr('<i class="fa fa-filter" aria-hidden="true"></i>', 'div', '', 'iconContainer')
		textContainerHTML = HTMLelemStr(labelHTML + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY') + '<i class="fa fa-caret-down"></i>', 'div', '', 'textContainer')
					
		$('#reportrange').html(calendarIconHTML + textContainerHTML);
		startDate = start.format('YYYY-MM-DD');
		endDate = end.format('YYYY-MM-DD');
	}
	
	// find student by class
	function SetupClassrooms(){
		//studentList = [];
		//studentsGroupedByClass = [];
		
		studentClassMap.forEach(function(val, key){
			SetupClasses(key);
		});
		
		// only initialize dual listbox once
		if($('#transfer_double_languageInput').length == 0)
			Transfer.transfer(settings);
		
		$('.studentTable').tablesorter({

			// *** APPEARANCE ***
			// Add a theme - 'blackice', 'blue', 'dark', 'default', 'dropbox',
			// 'green', 'grey' or 'ice' stylesheets have all been loaded
			// to use 'bootstrap' or 'jui', you'll need to include "uitheme"
			// in the widgets option - To modify the class names, extend from
			// themes variable. Look for "$.extend($.tablesorter.themes.jui"
			// at the bottom of this window
			// this option only adds a table class name "tablesorter-{theme}"
			theme: 'blue',

			// fix the column widths
			widthFixed: false,

			// Show an indeterminate timer icon in the header when the table
			// is sorted or filtered
			showProcessing: false,

			// header layout template (HTML ok); {content} = innerHTML,
			// {icon} = <i/> (class from cssIcon)
			headerTemplate: '{content}{icon}',

			// return the modified template string
			onRenderTemplate: null, // function(index, tmpl){ return tmpl; },

			// called after each header cell is rendered, use index to target
			// the column customize header HTML
			onRenderHeader: function(index) {
			  // the span wrapper is added by default
			  $(this)
				.find('div.tablesorter-header-inner')
				.addClass('roundedCorners');
			},

			// *** FUNCTIONALITY ***
			// prevent text selection in header
			cancelSelection: true,

			// add tabindex to header for keyboard accessibility
			tabIndex: true,

			// other options: "ddmmyyyy" & "yyyymmdd"
			dateFormat: "mmddyyyy",

			// The key used to select more than one column for multi-column
			// sorting.
			sortMultiSortKey: "shiftKey",

			// key used to remove sorting on a column
			sortResetKey: 'ctrlKey',

			// false for German "1.234.567,89" or French "1 234 567,89"
			usNumberFormat: true,

			// If true, parsing of all table cell data will be delayed
			// until the user initializes a sort
			delayInit: false,

			// if true, server-side sorting should be performed because
			// client-side sorting will be disabled, but the ui and events
			// will still be used.
			serverSideSorting: false,

			// default setting to trigger a resort after an "update",
			// "addRows", "updateCell", etc has completed
			resort: true,

			// *** SORT OPTIONS ***
			// These are detected by default,
			// but you can change or disable them
			// these can also be set using data-attributes or class names
			headers: {
			  // set "sorter : false" (no quotes) to disable the column
			  0: {
				sorter: "text"
			  },
			  1: {
				sorter: "text"
			  }
			},

			// ignore case while sorting
			ignoreCase: true,

			// forces the user to have this/these column(s) sorted first
			sortForce: null,
			// initial sort order of the columns,
			// example sortList: [[0,0],[1,0]],
			// [[columnIndex, sortDirection], ... ]
			sortList: [
			  [1, 0],
			  [2, 0]
			],
			// default sort that is added to the end of the users sort
			// selection.
			sortAppend: null,

			// when sorting two rows with exactly the same content,
			// the original sort order is maintained
			sortStable: false,

			// starting sort direction "asc" or "desc"
			sortInitialOrder: "asc",

			// Replace equivalent character (accented characters) to allow
			// for alphanumeric sorting
			sortLocaleCompare: false,

			// third click on the header will reset column to default - unsorted
			sortReset: false,

			// restart sort to "sortInitialOrder" when clicking on previously
			// unsorted columns
			sortRestart: false,

			// sort empty cell to bottom, top, none, zero, emptyMax, emptyMin
			emptyTo: "bottom",

			// sort strings in numerical column as max, min, top, bottom, zero
			stringTo: "max",

			// colspan cells in the tbody will have duplicated content in the
			// cache for each spanned column
			duplicateSpan: true,

			// extract text from the table
			textExtraction: {
			  0: function(node, table) {
				// this is how it is done by default
				return $(node).attr(table.config.textAttribute) ||
				  node.textContent ||
				  node.innerText ||
				  $(node).text() ||
				  '';
			  },
			  1: function(node) {
				return $(node).text();
			  }
			},

			// data-attribute that contains alternate cell text
			// (used in default textExtraction function)
			textAttribute: 'data-text',

			// use custom text sorter
			// function(a,b){ return a.sort(b); } // basic sort
			textSorter: null,

			// choose overall numeric sorter
			// function(a, b, direction, maxColumnValue)
			numberSorter: null,

			// *** WIDGETS ***
			// apply widgets on tablesorter initialization
			initWidgets: true,

			// table class name template to match to include a widget
			widgetClass: 'widget-{name}',

			// include zebra and any other widgets, options:
			// 'columns', 'filter', 'stickyHeaders' & 'resizable'
			// 'uitheme' is another widget, but requires loading
			// a different skin and a jQuery UI theme.
			widgets: ['zebra', 'columns', 'resizable'],

			widgetOptions: {

			  // *** COLUMNS WIDGET ***
			  // change the default column class names primary is the 1st column
			  // sorted, secondary is the 2nd, etc
			  columns: [
				"primary",
				"secondary",
				"tertiary"
			  ],
			  
			 //resizable_widths : [ '10%', '10%', '50px', '10%', '10%', '10%' ],

			  // If true, the class names from the columns option will also be added
			  // to the table tfoot
			  columns_tfoot: true,

			  // If true, the class names from the columns option will also be added
			  // to the table thead
			  columns_thead: true,

			  // *** FILTER WIDGET ***
			  // css class name added to the filter cell (string or array)
			  filter_cellFilter: '',

			  // If there are child rows in the table (rows with class name from
			  // "cssChildRow" option) and this option is true and a match is found
			  // anywhere in the child row, then it will make that row visible;
			  // default is false
			  filter_childRows: false,

			  // ( filter_childRows must be true ) if true = search
			  // child rows by column; false = search all child row text grouped
			  filter_childByColumn: false,

			  // if true, include matching child row siblings
			  filter_childWithSibs: true,

			  // if true, allows using '#:{query}' in AnyMatch searches
			  // ( column:query )
			  filter_columnAnyMatch: true,

			  // If true, a filter will be added to the top of each table column.
			  filter_columnFilters: true,

			  // css class name added to the filter row & each input in the row
			  // (tablesorter-filter is ALWAYS added)
			  filter_cssFilter: '',

			  // data attribute in the header cell that contains the default (initial)
			  // filter value
			  filter_defaultAttrib: 'data-value',

			  // add a default column filter type "~{query}" to make fuzzy searches
			  // default; "{q1} AND {q2}" to make all searches use a logical AND.
			  filter_defaultFilter: {},

			  // filters to exclude, per column
			  filter_excludeFilter: {},

			  // jQuery selector string (or jQuery object)
			  // of external filters
			  filter_external: '',

			  // class added to filtered rows; needed by pager plugin
			  filter_filteredRow: 'filtered',

			  // add custom filter elements to the filter row
			  filter_formatter: null,

			  // Customize the filter widget by adding a select dropdown with content,
			  // custom options or custom filter functions;
			  // see http://goo.gl/HQQLW for more details
			  filter_functions: null,

			  // hide filter row when table is empty
			  filter_hideEmpty: true,

			  // Set this option to true to hide the filter row initially. The row is
			  // revealed by hovering over the filter row or giving any filter
			  // input/select focus.
			  filter_hideFilters: false,

			  // Set this option to false to keep the searches case sensitive
			  filter_ignoreCase: true,

			  // if true, search column content while the user types (with a delay)
			  // or, set a minimum number of characters that must be present before
			  // a search is initiated
			  filter_liveSearch: true,

			  // global query settings ('exact' or 'match'); overridden by
			  // "filter-match" or "filter-exact" class
			  filter_matchType: {
				'input': 'exact',
				'select': 'exact'
			  },

			  // a header with a select dropdown & this class name will only show
			  // available (visible) options within the drop down
			  filter_onlyAvail: 'filter-onlyAvail',

			  // default placeholder text (overridden by any header
			  // "data-placeholder" setting)
			  filter_placeholder: {
				search: '',
				select: ''
			  },

			  // jQuery selector string of an element used to reset the filters.
			  filter_reset: null,

			  // Reset filter input when the user presses escape
			  // normalized across browsers
			  filter_resetOnEsc: true,

			  // Use the $.tablesorter.storage utility to save the most recent filters
			  filter_saveFilters: false,

			  // Delay in milliseconds before the filter widget starts searching;
			  // This option prevents searching for every character while typing
			  // and should make searching large tables faster.
			  filter_searchDelay: 300,

			  // allow searching through already filtered rows in special
			  // circumstances; will speed up searching in large tables if true
			  filter_searchFiltered: true,

			  // include a function to return an array of values to be added to the
			  // column filter select
			  filter_selectSource: null,

			  // filter_selectSource array text left of the separator is added to
			  // the option value, right into the option text
			  filter_selectSourceSeparator: '|',

			  // Set this option to true if filtering is performed on the
			  // server-side.
			  filter_serversideFiltering: false,

			  // Set this option to true to use the filter to find text from the
			  // start of the column. So typing in "a" will find "albert" but not
			  // "frank", both have a's; default is false
			  filter_startsWith: false,

			  // If true, ALL filter searches will only use parsed data. To only
			  // use parsed data in specific columns, set this option to false
			  // and add class name "filter-parsed" to the header
			  filter_useParsedData: false,

			  // *** RESIZABLE WIDGET ***
			  // If this option is set to false, resized column widths will not
			  // be saved. Previous saved values will be restored on page reload
			  resizable: true,

			  // If this option is set to true, a resizing anchor
			  // will be included in the last column of the table
			  resizable_addLastColumn: false,

			  // Set this option to the starting & reset header widths
			  resizable_widths: ['10px', '60px', '60px', '100px', '120px', '60px', '60px', '60px', '70px', '70px'],

			  // Set this option to throttle the resizable events
			  // set to true (5ms) or any number 0-10 range
			  resizable_throttle: false,

			  // When true, the last column will be targeted for resizing,
			  // which is the same has holding the shift and resizing a column
			  resizable_targetLast: false,

			  // *** SAVESORT WIDGET ***
			  // If this option is set to false, new sorts will not be saved.
			  // Any previous saved sort will be restored on page reload.
			  saveSort: true,

			  // *** STICKYhEADERS WIDGET ***
			  // stickyHeaders widget: extra class name added to the sticky header
			  // row
			  stickyHeaders: '',

			  // jQuery selector or object to attach sticky header to
			  stickyHeaders_attachTo: null,

			  // jQuery selector or object to monitor horizontal scroll position
			  // (defaults: xScroll > attachTo > window)
			  stickyHeaders_xScroll: null,

			  // jQuery selector or object to monitor vertical scroll position
			  // (defaults: yScroll > attachTo > window)
			  stickyHeaders_yScroll: null,

			  // number or jquery selector targeting the position:fixed element
			  stickyHeaders_offset: 0,

			  // scroll table top into view after filtering
			  stickyHeaders_filteredToTop: true,

			  // added to table ID, if it exists
			  stickyHeaders_cloneId: '-sticky',

			  // trigger "resize" event on headers
			  stickyHeaders_addResizeEvent: true,

			  // if false and a caption exist, it won't be included in the
			  // sticky header
			  stickyHeaders_includeCaption: true,

			  // The zIndex of the stickyHeaders, allows the user to adjust this
			  // to their needs
			  stickyHeaders_zIndex: 2,

			  // *** STORAGE WIDGET ***
			  // allows switching between using local & session storage
			  storage_useSessionStorage: false,
			  // alternate table id (set if grouping multiple tables together)
			  storage_tableId: '',
			  // table attribute to get the table ID, if storage_tableId
			  // is undefined
			  storage_group: '', // defaults to "data-table-group"
			  // alternate url to use (set if grouping tables across
			  // multiple pages)
			  storage_fixedUrl: '',
			  // table attribute to get the fixedUrl, if storage_fixedUrl
			  // is undefined
			  storage_page: '',

			  // *** ZEBRA WIDGET ***
			  // class names to add to alternating rows
			  // [ "even", "odd" ]
			  zebra: [
				"even",
				"odd"
			  ]

			},

			// *** CALLBACKS ***
			// function called after tablesorter has completed initialization
			initialized: null, // function (table) {}

			// *** extra css class names
			tableClass: '',
			cssAsc: '',
			cssDesc: '',
			cssNone: '',
			cssHeader: '',
			cssHeaderRow: '',
			// processing icon applied to header during sort/filter
			cssProcessing: '',

			// class name indiciating that a row is to be attached to its
			// parent
			cssChildRow: 'tablesorter-childRow',
			// don't sort tbody with this class name
			// (only one class name allowed here!)
			cssInfoBlock: 'tablesorter-infoOnly',
			// class name added to element inside header; clicking on it
			// won't cause a sort
			cssNoSort: 'tablesorter-noSort',
			// header row to ignore; cells within this row will not be added
			// to table.config.$headers
			cssIgnoreRow: 'tablesorter-ignoreRow',

			// if this class does not exist, the {icon} will not be added from
			// the headerTemplate
			cssIcon: 'tablesorter-icon',
			// class name added to the icon when there is no column sort
			cssIconNone: '',
			// class name added to the icon when the column has an ascending sort
			cssIconAsc: '',
			// class name added to the icon when the column has a descending sort
			cssIconDesc: '',

			// *** header events ***
			pointerClick: 'click',
			pointerDown: 'mousedown',
			pointerUp: 'mouseup',

			// *** SELECTORS ***
			// jQuery selectors used to find the header cells.
			selectorHeaders: '> thead th, > thead td',

			// jQuery selector of content within selectorHeaders
			// that is clickable to trigger a sort.
			selectorSort: "th, td",

			// rows with this class name will be removed automatically
			// before updating the table cache - used by "update",
			// "addRows" and "appendCache"
			selectorRemove: ".remove-me",

			// *** DEBUGING ***
			// send messages to console
			debug: false

		});
	}
	
	function SetupClasses(classID){
		classTableHTML = /*'<colgroup><col span="2" class="studentColSpan"><col span="3" class="statsColSpan"><col span="3" class="assignmentsColSpan"></colgroup>' + '<tr class="topRowHeader"><th colspan="2">Student</th><th colspan="3">Stats</th><th colspan="3">Assignments</th></tr>' + */'<thead><tr class="bottomRowHeader"><th class="checkBoxCol resizable-false">' /*<i class="fa fa-check-square-o" aria-hidden="true"></i>*/ + '<input class="selectAllChkbx" type="checkbox" name="selectAll" /></th><th>Last Name</th><th>First Name</th><th>Username</th><th>Email</th><th>Lesson Time</th><th>Num Lessons Completed</th><th>Total Test Time</th><th>Number Assignments</th><th>Assignments Completed</th><th>Assignment Progress</th></tr></thead><tbody>';
		var studentsInClass = studentClassMap.get(classID);
		
		if(studentsInClass.length > 0){
			studentsGroupedByClass.push({"groupName":studentsInClass[0].class_name, "groupData": new Array()});
		}
		
		filteredAssignmentsGroupedByStudent = new Array();
		var effectiveEndDate = end.add(1, 'days').format('YYYY-MM-DD');
		assignmentsGroupedByStudent.forEach(function(student, index){
			index_asStr = index.toString();

			student.forEach(function(assignment){
				if(assignment.assign_date > startDate && assignment.assign_date <= effectiveEndDate){
						if(filteredAssignmentsGroupedByStudent[index_asStr] == undefined){
							filteredAssignmentsGroupedByStudent[index_asStr] = {"assign_count":0, "completed_count" : 0, "progressSum" : 0, "goalSum" : 0};
						} 
						
						intValAssignmentCompleted = 0;
						if(assignment.completed_date != "0000-00-00 00:00:00"){
							intValAssignmentCompleted = 1;
						}
						
						intAssignmentProgress = (assignment.progress == null) ? 0 : parseInt(assignment.progress);
						intAssignmentGoal = (assignment.goal == null) ? 0 : parseInt(assignment.goal);
						
						filteredAssignmentsGroupedByStudent[index_asStr].assign_count += 1;
						filteredAssignmentsGroupedByStudent[index_asStr].completed_count += intValAssignmentCompleted;
						filteredAssignmentsGroupedByStudent[index_asStr].progressSum += intAssignmentProgress;
						filteredAssignmentsGroupedByStudent[index_asStr].goalSum += intAssignmentGoal;
				}
			});
		});
		
		
		studentsInClass.forEach(AddStudentWithDataToClass);
		$('#class_' + classID).html(classTableHTML + '</tbody>');
		
	}
	
	
	// lookup both student data and student history to get html string to display to teacher
	function AddStudentWithDataToClass(item, index, arr){
		var itemExtended = {...item, 
			"firstLastname": item.firstname + " " + item.lastname, 
			"lastFirstname": item.lastname + ", " + item.firstname,
			"firstLastnameEmail": item.firstname + " " + item.lastname + " (" + item.email + ")",
			"lastFirstnameEmail": item.lastname + ", " + item.firstname + " (" + item.email + ")"};
		
		
		// always add students to most recently added class
		studentsGroupedByClass[studentsGroupedByClass.length-1].groupData.push(itemExtended);	
		
		studentList.push(itemExtended);
		
		var allStudentHistory = studentHistory.filter(function(student){
			var effectiveEndDate = end.add(1, 'days').format('YYYY-MM-DD');
			return student.stud_id == item.user_id && student.timestamp > startDate && student.timestamp <= effectiveEndDate;
		});
		
		var totalLessonTime = 0;
		var totalNumLessons = 0;
		var totalTestTime = 0;
		
		for(var ii = 0; ii < allStudentHistory.length; ii++){
			if(typeof allStudentHistory[ii].lesson_module !== 'undefined'){
				totalLessonTime += parseInt(allStudentHistory[ii].lesson_timespent);
				totalNumLessons++;
			} else if (typeof allStudentHistory[ii].total_time_secs !== 'undefined'){
				totalTestTime += parseInt(allStudentHistory[ii].total_time_secs);
			}
		}
		
		var lastName = (item.lastname == null) ? '-' : item.lastname;
		var firstName = (item.firstname == null) ? '-' : item.firstname;
		
		var lastNameHTML = HTMLelemStr(lastName, 'td', '', 'studLastName');
		var firstNameHTML = HTMLelemStr(firstName, 'td', '', 'studFirstName');
		var usernameHTML = HTMLelemStr(item.username, 'span', '', 'studUsername');
		var emailHTML = HTMLelemStr(item.email, 'td', '', 'studEmail');
		
		// see if student is verified to be in class
		if(item.is_verified == 1){
			var numAssignments = "0";
			var numCompletedAssignments = "0";
			var progressAmt  = 0;
			var goalAmt  = 0;
			var progressBarVal = 0;
			var progressClass = "under50";
			
			if(filteredAssignmentsGroupedByStudent[item.user_id] != undefined){
				numAssignments = filteredAssignmentsGroupedByStudent[item.user_id].assign_count;
				numCompletedAssignments = filteredAssignmentsGroupedByStudent[item.user_id].completed_count;
				progressAmt  = filteredAssignmentsGroupedByStudent[item.user_id].progressSum;
				goalAmt  = filteredAssignmentsGroupedByStudent[item.user_id].goalSum;
				
				//numCompletedAssignments =7;
				progressBarVal = Math.round((numCompletedAssignments / numAssignments) * 1000) / 10;
				
				if(progressBarVal >= 100){
					progressClass = "at100";
				} else if(progressBarVal >= 50){
					progressClass = "under100";
				}
			} 
			
			var numAssignmentsHTML = HTMLelemStr(numAssignments, 'td', '');
			var numCompletedAssignmentsHTML = HTMLelemStr(numCompletedAssignments, 'td');
			
			var progressBarValHTML = HTMLelemStr('', 'span', '', 'progressBarVal', '', 'width: ' + progressBarVal + '%;');
			var progressBarHTML = HTMLelemStr(progressBarValHTML + progressBarVal + "%", 'td', '', 'progressBar ' + progressClass);
			
			// ensure studID_... is first class
			classTableHTML += '<tr class="studID_' + item.user_id + '"><td><input class="selectStudChkbx" type="checkbox" name="selectStudents" /></td>' + lastNameHTML + firstNameHTML + '<td><a href="/user/' + item.username + '/lessons">' + usernameHTML + '</a></td>' + emailHTML + "<td>" + makeSecondsReadable(totalLessonTime) + "</td><td>" + totalNumLessons + "</td><td>" + makeSecondsReadable(totalTestTime) + "</td>" + numAssignmentsHTML + numCompletedAssignmentsHTML + progressBarHTML + "</tr>";
		} else if(item.is_verified == 0){
			
			var acceptBtnHTML = '<button onclick="AcceptOrDenyStudent( this,\'' + item.user_id + '\', \'' + item.class_id + '\',true); event.stopPropagation();" class="stoUI stoUIClickable stoUIClickableGreen">Accept</button>';
			var denyBtnHTML = '<button onclick="AcceptOrDenyStudent(this,\'' + item.user_id + '\', \'' + item.class_id + '\', false); event.stopPropagation();" class="stoUI stoUIClickable stoUIClickableRed">Deny</button>'
			var unverifiedStudentBtnsHTML = HTMLelemStr(acceptBtnHTML + denyBtnHTML, 'div', '', '','','display:inline-block; margin: 0px 10px;');
			
			// ensure studID_... is first class
			classTableHTML += '<tr class="studID_' +item.user_id  + ' studentAwaitingVerify"><td><input class="selectStudChkbx" type="checkbox" name="selectStudents" /></td>' + lastNameHTML + firstNameHTML + '<td><a href="/user/' + item.username + '/lessons">' +usernameHTML + '</a></td>' + emailHTML + "<td colspan=6 class=\"boldText unverifiedStudMsg\">" + unverifiedStudentBtnsHTML + "Student awaiting verification</td></tr>";
		}
	}
	
	// forEach --studentUserData-- create --studentClassMap--
	function SetupStudentClassroomMaps(item, index){
		var studentByUserID = studentHistory.filter(function(student){
			return student.stud_id == item.user_id;
		});
		studentDataComplete.set(item.user_id, studentByUserID);
		
		var studentByClassroom = studentUserData.filter(function(student){
			return student.class_id == item.class_id;
		});
		
		studentClassMap.set(item.class_id, studentByClassroom);
	}
	
	
	$('#textTypeSelected').change(function(){
		updateTextSelection();
	});
	
	$('#ATPselect').change(function(){
		var selection = parseInt($(this).val());
		switch(selection){
			case 0:	// Lesson Practice - Time
				$('#ASVdiv').show();
				$('#ASVdiv label.beforeInput').html("<h4>Time:</h4>");
				$('#ASVdiv label.afterInput').html("<h4>minutes</h4>");
				
				$('option.specLesson').show();
				$('option.textTypes').hide();
				if($('#ASPselect option.specLesson:selected').length == 0){
					$('#ASPselect option:selected').prop('selected', false);
					$('#ASPselect option.specLesson[value="1"]').prop('selected', true);
				}
				//$('#ASPselect').find('option.specLesson[value="1"]').attr('selected', 'selected');
			break;
			case 1:	// Lesson Practice - Count
				$('#ASVdiv').show();
				$('#ASVdiv label.beforeInput').html("<h4>Count:</h4>");
				$('#ASVdiv label.afterInput').html("<h4>times</h4>");
				
				$('option.specLesson').show();
				$('option.textTypes').hide();
				if($('#ASPselect option.specLesson:selected').length == 0){
					$('#ASPselect option:selected').prop('selected', false);
					$('#ASPselect option.specLesson[value="1"]').prop('selected', true);
				}
			break;
			case 2:	// Lesson Achievement
				$('#ASVdiv').hide();
				
				$('option.specLesson').show();
				$('option.textTypes').hide();
				if($('#ASPselect option.specLesson:selected').length == 0){
					$('#ASPselect option:selected').prop('selected', false);
					$('#ASPselect option.specLesson[value="1"]').prop('selected', true);
				}
			break;
			case 3: // Typing Test - time
				$('#ASVdiv').show();
				$('#ASVdiv label.beforeInput').html("<h4>Time:</h4>");
				$('#ASVdiv label.afterInput').html("<h4>minutes</h4>");
				
				$('option.specLesson').hide();
				$('option.textTypes').show();
				if($('#ASPselect option.textTypes:selected').length == 0){
					$('#ASPselect option:selected').prop('selected', false);
					$('#ASPselect option.textTypes[value="1"]').prop('selected', true);
				}
			break;
			case 4:
				
			break;
		}
	});
	
	function showUpdateButton(){
		$('#topUpdateBtn').show();
		$('#bottomUpdateBtn').show();
		$('#formInfo').hide();
		$('#formErrors').hide();
	}
	
	$('input').change(function(){
		showUpdateButton();
	})
	
	$('select').change(function(){
		showUpdateButton();
	})
	
	$('.ui_spinner').on('mousewheel', function(event) {
		showUpdateButton();
	});
	
	$( "#ASVselect").spinner({
		mouseWheel: true,
		numberFormat: "n0",
	  spin: function( event, ui ) {
		if ( ui.value > 65000 ) {
		  $( this ).spinner( "value", 65000 );
		  return false;
		} else if ( ui.value < 1 ) {
		  $( this ).spinner( "value", 1 );
		  return false;
		}
	  },
		change: function(event, ui){
			if($(this).spinner("value") == null){
				$( this ).spinner( "value", 1 );
				return false;
			}
		}
	});

	$("#ASVselect").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false;}
	});
	

	$( "#targetWPMspinner").spinner({
		mouseWheel: true,
		numberFormat: "n0",
		spin: function( event, ui ) {
			if ( ui.value > 255 ) {
				$( this ).spinner( "value", 255 );
				return false;
			} else if ( ui.value < 0 ) {
				$( this ).spinner( "value", 0 );
				return false;
			}
		}
	});

	$("#targetWPMspinner").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false;}
	});

	$("#targetAccuracyspinner").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) { return false;}
	});

	$( "#targetAccuracyspinner").spinner({
		step: 0.1,
		numberFormat: "n1",
		mouseWheel: true,
	  spin: function( event, ui ) {
		if ( ui.value > 100 ) {
		  $( this ).spinner( "value", 100 );
		  return false;
		} else if ( ui.value < 0 ) {
		  $( this ).spinner( "value", 0 );
		  return false;
		}
	  }
	});
	
	
	
	$('#reportrange').on('apply.daterangepicker', function(ev, picker){
		startDate = picker.startDate.format('YYYY-MM-DD');
		endDate = picker.endDate.format('YYYY-MM-DD');
		$.post('/setData.php', { type: "us", col: 'ol_date_filter', val: GetTLdateFilterEnum(picker.chosenLabel)},
			function(output){
		});
		SetupClassrooms();
	});
	
	function AcceptOrDenyStudent(elem, studID, classroomID, accept=false){
		
		$.post('/setData.php', { type: "ol_aodstc", sid: studID, cid: classroomID, addstud:accept},
			function(output){
				if(output == "1"){
					if(accept){
						$(elem).closest('.unverifiedStudMsg').html('Student added to class - <a href=".">refresh page</a> to see stats')
					} else {
						$(elem).closest('.unverifiedStudMsg').html("Student rejected - refresh page")
					}
				}
		});
	}
	
	function RemoveStudentsDialog(elem, class_id, assignTitle, delButtonText){
		jqElemCont = $(elem).closest(".usfContainer");
		classID = jqElemCont[0].id.slice(8);
		classroomName = jqElemCont.find(".classroomName").html();
		studElemList = jqElemCont.find(".selected");
		studList = new Array();
		studListToDisplayHTML = '<table><th>Last</th><th>First</th><th>Username</th><th>Email</th>';
		
		for(var ii = 0; ii < studElemList.length; ii++){
			studID = studElemList[ii].className.match(/^stud[^ ]*/g)[0].slice(7);	// cut off the "studID_"
			studList.push(studID);
			studElem = $(studElemList[ii]);
			firstNameHTML = HTMLelemStr(studElem.find(".studFirstName").html(), 'td');
			lastNameHTML = HTMLelemStr(studElem.find(".studLastName").html(), 'td');
			usernameHTML = HTMLelemStr(studElem.find(".studUsername").html(), 'td');
			emailHTML = HTMLelemStr(studElem.find(".studEmail").html(), 'td');
			
			studListToDisplayHTML += HTMLelemStr(lastNameHTML + firstNameHTML + usernameHTML + emailHTML, 'tr');
		}
		
		studListToDisplayHTML += '</table>';
		
		$('#extraInfo').html("You are removing the following students: <div id=\"assignTitle\">" + studListToDisplayHTML + "</div><div class=\"classroomNameMsg\"> from class \"" + classroomName + "\"</div>");
		
		$( "#dialog-confirm" ).dialog({
		  resizable: false,
		  height:450,
		  width:690,
		  modal: true,
		  draggable: true,
		  buttons: [
			{
				text: delButtonText,
				click: function() {
					$( this ).dialog( "close" );
					$.post('/setData.php', { type: 'ol_rsfc', aos: studList, cid: classID},
						function(output){
							window.location.reload();
					});
				}			
			}, {
				text: "Cancel",
				click: function() {
					  $( this ).dialog( "close" );
					}
			}
		  ],
		  open: function() {
			  $(this).parent().find(":contains('Cancel')").focus();
			  $(this).parent().find(":contains('Delete')").blur();
		  }
		});
	}
</script>
<style>
h2{width:40%; color: #135d79; font-size:2.2em; font-weight:800; border-bottom: 4px solid #7CB9CE; margin-top:20px;}
.hudContainer_horiz{position: sticky; top: 54px; background: #e0e0e0; border-bottom: 3px solid grey; padding: 30px 0 10px 0; margin-top: -7px; width: 100%; border-top: 1px solid grey; z-index:10; margin-top: -16px;}

#tfFilterContainer{width:320px;}
.usfContainer{z-index:5; position:relative;padding-top:90px; max-width:1300px; overflow-y:auto;}
.usfContainer h3{position: absolute; left: 0; border: none; background: #7cb9ce; padding: 5px 20px 8px 20px; top: 0; border-top-left-radius: 12px; border-bottom-right-radius: 12px; min-width: 300px; max-width:570px; font-size: 2em; color: #0b3d4e; font-weight: 800;}
.usfContainer .classCode{position: absolute; right: 0; border: none; background: #7cb9ce; padding: 5px 20px 8px 20px; top: 0; border-top-right-radius: 12px; border-bottom-left-radius: 12px; color: #0b3d4e; font-weight: 600;}
.usfContainer .classCode .smallLabel{display:block; font-size:14px; font-weight:700; text-decoration:underline; text-align:left;}
.usfContainer input{width:0px;}
.usfContainer input[type=checkbox] {width: 16px; height: 16px;}
.usfContainer button:disabled{background: #dadada; border: 1px solid #a7a7a7; color: #6d6d6d;}
#listOfClasses .classCode .field{display:inline-block; vertical-align:text-bottom;}
.classCode > div{display:inline-block; height:60px; vertical-align:bottom; margin:0 10px;}
.classCode .field input{display:inline-block; width:auto;}
.classCode .field label{display:inline-block; width:auto;}
.classCode .ccToDisplay{font-size:24px; margin-right:8px;}
.classCode button{font-size:14px;}

table{margin-top:80px; table-layout:fixed;}


*, *:before, *:after {
    -moz-box-sizing: content-box;
    -webkit-box-sizing: content-box;
    box-sizing: content-box;
}
table td, table th{overflow:hidden; text-overflow: ellipsis;}

/*.amsify-suggestags-area{width:80%;}*/
.ui-widget{font-size:1em;}
#rightCol{float: right; position: absolute; width: 45%; height: 300px; right: 0; display: flex; justify-content: center; align-items: center; height: 100%; flex-direction: column;}
#rightCol > div{position:relative; height:100%; width:100%;}

#dialog-create-assignment{width:100% !important;}
#mainCont{position: relative; margin: 0 auto; width: 100%; height: 100%; min-width:850px;}

#rightCol input, #rightCol select, #rightCol textarea, #rightCol button{font-family:Open Sans;}
#rightCol div.field.stoUI{vertical-align:top; margin-top:15px;}
.field > label {width:72px; vertical-align:top;}
div.field input{width:78%;}
div.field textarea{width:78%; height:53px; padding:5px;}

#ATPselect{width:78%; height:45px; font-size:1.2em; font-weight:800px;}
#rightCol #ASPdiv{margin-top:38px;}
#ASPdiv label{text-align:left; width:423px;}
#ASPdiv select{max-width:none; width:435px;}

#rightCol #ASVdiv{#rightCol #ASVdiv}
#ASVdiv .ui-spinner{height:36px; width:72px;}
#ASVdiv .ui-spinner input{padding:3px 7px; width:69px; font-weight:700; color:#003b46; margin:0;}
#ASVdiv label{line-height: 35px; text-align:left;}
#ASVdiv select{max-width:none; width:435px;}
#ASVdiv label.beforeInput{width:80px; margin:0 20px 0 4px; text-align:right;}
#ASVdiv label.afterInput{margin-left:7px;}

#rightCol #ADDdiv{margin-top:35px;}
#ADDdiv label{width:435px; text-align:left;}
#ADDpicker{max-width: none; width:435px; height:48px; line-height:39px; font-family:Open Sans; cursor:pointer;}

.daterangepicker{background:#ddebef; box-shadow:rgb(114, 141, 156) 0px 0px 10px inset, rgb(53, 53, 53) -1px 7px 28px -2px;}
.daterangepicker .calendar-table{ background: #ddebef; border: none;}
.daterangepicker .drp-calendar.left, .daterangepicker .drp-calendar.right{padding: 20px 15px;}
.daterangepicker.openscenter .drp-calendar.left{padding: 8px 0 8px 12px;}
.daterangepicker .drp-calendar.left .calendar-table{padding:0 0 5px 0;}
.daterangepicker.drop-up{width:390px;}
.daterangepicker .drp-calendar{width:340px; max-width:none; margin: 3px 15px;}
.daterangepicker .calendar-time{font-size:25px; font-weight: 800; margin-top:15px;}
.daterangepicker select.ampmselect, .daterangepicker select.hourselect, .daterangepicker select.minuteselect, .daterangepicker select.secondselect{font-size: 20px; width: 61px; height: 38px; border: 1px solid #002e3c; border-radius: 6px; font-weight: 700; color: white; background: #2a6271;}
.daterangepicker select:hover{background: #003c4e;}
.daterangepicker .calendar-table th {font-size:18px; font-weight:800;}
.daterangepicker .calendar-table td {font-size: 18px; font-weight:600;}
.daterangepicker button:hover, .daterangepicker select:hover{cursor:pointer;}
.daterangepicker .prev.available, .daterangepicker .next.available{height:44px;}
.daterangepicker tbody tr{height:34px;}
.daterangepicker td.weekend { /*color: #00b7ef;*/}

.daterangepicker button, .ui-dialog-buttonset button:first-child {border-radius:5px; color: #fff; background: #2a6271; border: 1px solid #09637b; width:90px;}
.ui-dialog-buttonset button:first-child{width:auto;}
.ui-dialog-buttonset .ui-button-disabled.ui-state-hover:first-child{background: #2a6271;}
.daterangepicker button:hover, .ui-dialog-buttonset button:first-child:hover { background: #003c4e;}
.ui-dialog-buttonset button.ui-button-disabled:first-child:hover{background: #2a6271;}
.daterangepicker button.cancelBtn { color: #2d495f; background:#93c3cd;}
.daterangepicker button.cancelBtn:hover { background:#fae8b6;}

.daterangepicker td.off, .daterangepicker td.off.end-date, .daterangepicker td.off.in-range, .daterangepicker td.off.start-date{background-color:transparent; color: #6692a0;}
.daterangepicker td.available:hover, .daterangepicker th.available:hover{background:#74aebd; color:#053d4c;}
.daterangepicker td.active, .daterangepicker td.active:hover { background-color: #2a6271; border-color: transparent; color: #fff;}
.daterangepicker td.active:hover { background: #003c4e;}
.daterangepicker.drop-up:before{bottom:-8px; border-top: 6px solid #2a6271;}
.daterangepicker.drop-up:after{border-top: 6px solid #acc0c8;}

#reportrange {position:relative; margin-bottom:8px;}
#reportrange > div{display:inline-block;}
#reportrange .iconContainer{/*width:12%;*/}
#reportrange .iconContainer .fa{position: absolute; top: 50%; transform: translateY(-50%); left: 5%; font-size:25px;}
#reportrange .textContainer{width:88%; font-weight:500; padding:0px 5px; font-size:14px;}
#reportrange .textContainer .dateRangeLabel{font-size:19px; font-weight:700; /*margin-bottom:4px;*/}
#reportrange .textContainer .fa{font-size:18px; margin-left:10px;}

select option{font-size:1.1em;}
select optgroup[label]{font-weight:800;}

select option.keyboard{background:#444; color: white;}
select option.keyboard.Review{background:#444; color: white;}
select option.classic, select optgroup.classic{background:#a6c9e2; color: black;}
select option.advanced{background:#005f80; color: white;}
select option.classic.Review{background:#ffcd71; color: black;}
select option.advanced.Review{background:#c78508; color: white;}


th.tablesorter-header.resizable-false {
  background-color: #e6bf99;
}
/* ensure box-sizing is set to content-box, if using jQuery versions older than 1.8;
 this page is using jQuery 1.4 */
*, *:before, *:after {
  -moz-box-sizing: content-box;
  -webkit-box-sizing: content-box;
  box-sizing: content-box;
}
/* overflow table */
.wrapper {
  overflow-x: auto;
  overflow-y: hidden;
  width: 450px;
}
.wrapper table {
  width: auto;
  table-layout: fixed;
}
.wrapper .tablesorter td {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  min-width: 10px;
}
.wrapper .tablesorter th {
  overflow: hidden;
  text-overflow: ellipsis;
  min-width: 10px;
}

#pageMain .usfContainer .studentTable .progressBar{border: 1px solid #b30000; color: #4c0000; background: #ff9f9f; position: relative; z-index:0; text-align:center;}
#pageMain .usfContainer .studentTable .progressBarVal{position: absolute; top: 0; left: 0; height: 100%; background: #e43c3c; color:#4a0000; z-index:-1; font-weight: 600; padding-top: 4px; text-align: center;}
#pageMain .usfContainer .studentTable .progressBar.under100 .progressBarVal{border: 1px solid #0981a9; background: #0981a9;}
#pageMain .usfContainer .studentTable .progressBar.under100 {border: 1px solid #0b82a9; background: #67bedc; color:white; }
#pageMain .usfContainer .studentTable .progressBar.at100 .progressBarVal{background: green;}
#pageMain .usfContainer .studentTable .progressBar.at100 {border: 1px solid #023e02; color: white;}

.classBottomDiv{margin-top:40px;}

.tablesorter-blue tbody>tr.selected.odd>td{background:#ffc890;}
.tablesorter-blue tbody>tr.selected.odd{border:2px solid #a76c00;}
.tablesorter-blue tbody>tr.selected.odd:hover>td{background:#ffb56a;}
.tablesorter-blue tbody>tr.selected.even>td{background:#fdd2a6;}
.tablesorter-blue tbody>tr.selected.even{border:2px solid #a76c00;}
.tablesorter-blue tbody>tr.selected.even:hover>td{background:#ffb56a;}

.tablesorter-blue tbody tr.studentAwaitingVerify.odd td{background:#ddd; color: #656565; border:1px solid #b7b7b7;}
.tablesorter-blue tbody tr.studentAwaitingVerify.even td{background:#dedede; color: #656565; border:1px solid #b7b7b7;}
.tablesorter-blue tbody tr.studentAwaitingVerify td.boldText{font-weight:800; color:#444;}
.tablesorter-blue tbody tr.studentAwaitingVerify button{margin:0px 5px;}

#dialog-confirm table{margin:20px auto;}
#dialog-confirm table th{text-decoration:underline;}
#dialog-confirm table td{padding:4px 12px;}
#dialog-confirm .classroomNameMsg{margin-bottom:25px;}
</style>
<?php
require_once 'includes/overall/footer.php';
?>