<?php
class Lessons{
	private static $_instance = null;
	private $_db,
			$_lastLesson = 1,
			$_nextLesson = 2,
			$_practiceAgain = 3;
	
	public function __construct(){
		$this->_db = DB::getInstance();
	}	
	
	public static function getInstance(){

		if(!isset(self::$_instance)) {		
			self::$_instance = new Lessons();
		}
		return self::$_instance;

	}
	
	public function GetLessons(){
		return $this->_db->get('typing_lessons', array('type','!=','backup'), 'id, type, lesson_type, lesson_order, lesson_num, purpose, display_text, letters', array('lesson_order', 'ASC'), null,  array(999,0))->results();
	}
	
	public function GetLessonListByType($types){
		$typeArr = explode(',', $types);
		$where = array();
		foreach($typeArr as $type) {
			if(empty($where))
				$where = array_merge($where, array('type', '=', $type));
			else
				$where = array_merge($where, array('OR', 'type', '=', $type));
		}
		return $this->_db->get('typing_lessons', $where, 'type, id, lesson_order, lesson_num, purpose, display_text, letters', array('lesson_order', 'ASC'), null,  array(999,0))->results();
	}
	/*
	public function getLastLessonHTML(){return 'called Last Lesson method';}
	public function getNextLessonHTML(){return 'called Next Lesson method';}
	public function getPracticeAgainHTML(){return 'called Practice again method';}
	
		public function function getLessonHTMLfromArray($lessonArr, $wpmTarget = 'default', $accTarget = 'default', &$mostRecentLessonIndex = -1, &$worstLessonIndex = -1, $idPrefix = ''){
		$containerStart = '';
		$containerEnd = '</div>';
		$stoTT_HTML = '';
		$wpmTarget = ($wpmTarget == 'default') ? Config::get('constants/wpmLessonTarget') : $wpmTarget;
		$accTarget = ($accTarget == 'default') ? Config::get('constants/accLessonTarget') : $accTarget;
		$passedCount = 0;
		
		$letterSymbolMatch = array("Q W E R T Y U I O P", "A S D F G H J K L ;", "Z X C V B N M , .", "R T Y U F G H J V B N M", "E I D K C ,", "W O S L X .", "Q P A ; ' \" Z ? ! ^", "^", "*");
		$letterSymbolSwap = array("Top Row", "Home Row", "Bottom Row", "Index Finger", "Middle Finger", "Ring Finger", "Pinky Finger", "Shift", "All");
		
		
		for($ii = 0; $ii < sizeof($lessonArr); $ii++){
			
			$lessonFullTitle = $lessonArr[$ii]->lesson_type . ' ' . $lessonArr[$ii]->lesson_num;
			$lessonMiniTitle = $lessonArr[$ii]->lesson_type[0] . ' ' . $lessonArr[$ii]->lesson_num;
			$theLetters = implode(' ', str_split(strtoupper($lessonArr[$ii]->letters)));
			$theLetters = str_replace($letterSymbolMatch, $letterSymbolSwap, $theLetters);
			
			if(strlen($theLetters) <= 6){ 
				$letterSize = 'class="lessonText largeLessonText"';
			} else if(strlen($theLetters) > 7){
				$letterSize = 'class="lessonText doubleLineLessonText"';
			} else { 
				$letterSize = 'class="lessonText normalLessonText"';
			}
							
			if($lessonArr[$ii]->lesson_type == "Review"){
				if($containerEnd == ''){
					$containerStart = '';
					$containerEnd = '</div>';
				} else {
					$containerStart = '<div class="loneReview">';
					$containerEnd = '</div>';
				}
				$passedSymbol = '<div class="star">&#x2605;</div>';
			} else {
				if($containerEnd == ''){
					$containerStart = '';
					$containerEnd = '';
				} else {
					$containerStart = '<div class="lessonContainer">';
					$containerEnd = '';
				}
				$passedSymbol = '<div class="checkmark">&#x2714;</div>';
			}
			
			if(sizeof($lessonArr) == 1){
				$containerStart = '';
				$containerEnd = '';
			}
			
			$lessonStatus = '';
			
			if($lessonArr[$ii]->wpm != null){
				// see if most recent lesson
				if($mostRecentDate == '' || $lessonArr[$ii]->timestamp > $mostRecentDate){
					$mostRecentDate = $lessonArr[$ii]->timestamp;
					$mostRecentLessonIndex = $ii;
				}
				
				$lessonScore = $lessonArr[$ii]->wpm / $wpmTarget;
				//$lessonScore = $lessonArr[$ii]->accuracy / $accTarget;
				
				if($lessonScore >= 0){
					if($lessonScore >= 1){
						$lessonStatus = 'p100 passed ';
						$passedCount += 1;
					} else {
						if ($lessonScore >= 0.5){
							if($lessonScore >= 0.95){
								$lessonStatus = 'p95 progress ';
							} else if($lessonScore >= 0.85){
								$lessonStatus = 'p85 progress ';
							} else if($lessonScore >= 0.75){
								$lessonStatus = 'p75 progress ';
							} else {
								$lessonStatus = 'p50 progress ';
							} 
						} else {
							$lessonStatus = 'p0 progress fail ';
						} 
						
						// see if worse lesson score
						if($worstLessonIndex < 0 || $lessonScore < $worstLessonScore){
							$worstLessonScore = $lessonScore;
							$worstLessonIndex = $ii;
						} 
					}
					
				}
			
				$stoTT_HTML = '<span class="stoTT oCTA"><div class="divider">' . $lessonFullTitle . '</div><div class="mStat">' . $lessonArr[$ii]->wpm . '<span class="noteSmall"> WPM</span></div><div class="align"><span>Target Speed: </span>' . $wpmTarget . ' <span class="noteSmall">WPM</span></div><div class="align"><span>Target Accuracy: </span>' . $accTarget . ' <span class="noteSmall">%</span></div><div class="align"><span>Score: </span>' . round($lessonScore * 100) . '%</div>' . '</span>';
			}
			
			$lessonMod = $lessonArr[$ii]->lesson_order;
			
			$lessonHTML .= $containerStart . '<div id="' . $idPrefix . 'lessonMod_' . $lessonMod .  '" class="' . $lessonStatus . 'lessonButton ' . strtolower($lessonArr[$ii]->lesson_type) . '"><a href="/typing-tutor.php?mod=' . $lessonMod . '"><div class="lessonTitle">' . $lessonFullTitle . '</div><div class="lessonTitle miniTitle">' . $lessonMiniTitle . '</div><div class="meter"><span></span>' . $passedSymbol . '</div><div ' . $letterSize . '><span class="letters">' . $theLetters . '</span></div></a>' . $stoTT_HTML . '</div>' . $containerEnd;

		}
		
		if($idPrefix == "micro"){
			//echo '<div id="mattTest">' . $theLetters . '</div>';
		}
		
		return array($lessonHTML, $passedCount);
	}*/
}

?>