<aside>
	<?php 
	
	$user = new User();
	$aside = true;
	
	if ($user->isLoggedIn() && $current_file !== "goPremium.php"){
		include 'includes/widgets/loggedin.php';
	} else if($current_file !== "register.php" && $current_file !== "login.php" && $current_file !== "goPremium.php") {
		$nExt = '_w';
		include 'includes/widgets/login.php';
	}
	
	if($current_file !== "goPremium.php")
		include 'includes/widgets/user_count.php';
	?>
</aside>