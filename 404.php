<?php
$currPage = "404";
include $_SERVER['DOCUMENT_ROOT'] . '/core/init.php';

$htmlTitle = "Page Not Found";
$htmlDescription = "404 - Page Not Found";
$cssFiles = "be";
$noWidgets = true;
$addContainer = true;
include $_SERVER['DOCUMENT_ROOT'] . '/includes/overall/header.php';
?>

<h2>404 Not Found!</h2>
</div> <!-- <div id="heading"> -->


<div id="pageMain">
	<h3>Sorry about that.</h3>
	<div>
		<p>Please use the navigation bar at the top<p>
		<p>or the links at the bottom of this page to find what you're looking for.</p>
	</div>
	<h4>Thanks!</h4>
	
</div>
<style>
#pageMain > div{text-align:center;}
h3,h4{margin-top:25px;}
</style>
<?php

include $_SERVER['DOCUMENT_ROOT'] . '/includes/overall/footer.php';
?>