<div id="main-header" class="clearfix">
	<span id="top-logo"><a href="<?php echo Config::get('constants/rootUrl');?>"><img src="/images/STO_logo_wText.png?2" alt="Speed Typing Online" id="logo"></a></span>
	<p id="slogan"></p>
	<nav id="top-navigation">
		<ul id="menu-main">
			<li><a href="/home"<?php echo ($currPage == "home") ? ' class="current"' : ''; ?>>
			<span class="main_text" style="display: inline-block; opacity: 1; top: 0px;">Home</span>
			</a></li>
			<li><a href="/typing-test"<?php echo ($currPage == "typingTest") ? ' class="current"' : ''; ?>>
			<span class="main_text menu_2line" style="display: inline-block; opacity: 1;">Typing<br />Test</span>
			</a></li>
			<li class="has-sub-menu">
				<ul class="sub-menu">
					<li class="sub-menu-item<?php echo ($currPage == "typingTutor") ? ' current"' : '"'; ?>>
						<a href="/typing-tutor.php">Typing Tutor</a>
					</li>
					<li class="sub-menu-item<?php echo ($currPage == "keyboardBasics") ? ' current"' : '"'; ?>>
						<a href="/keyboard-basics">Keyboard Basics</a>
					</li>
					<li class="sub-menu-item<?php echo ($currPage == "typingLessons") ? ' current"' : '"'; ?>>
						<a href="/typing-lessons">Typing Lessons</a>
					</li>
				</ul>
				<a href="/typing-tutor"<?php echo ($currPage == "typingTutor" || $currPage == "typingLessons" || $currPage == "touchTypingTutorial") ? ' class="current"' : ''; ?>>
					<span class="main_text menu_2line" style="display: inline-block; opacity: 1;">Learn<br />To Type</span>
					<span class="sf-sub-indicator">&#9660;</span>
				</a>
			</li>
			<li class="has-sub-menu">
				<ul class="sub-menu">
					<li class="sub-menu-item<?php echo ($currPage == "typeTheAlphabet") ? ' current"' : '"'; ?>>
						<a href="/games/type-the-alphabet.php">Type the Alphabet</a>
					</li>
					<li class="sub-menu-item<?php echo ($currPage == "fastFireTyper") ? ' current"' : '"'; ?>>
						<a href="/games/fast-fire-typer.php">Fast Fire Typer</a>
					</li>
				</ul>
				<a href="/typing-games"<?php echo ($currPage == "typingGames" || $currPage == "typeTheAlphabet" || $currPage == "fastFireTyper") ? ' class="current"' : ''; ?>>
					<span class="main_text menu_2line" style="display: inline-block; opacity: 1;">Typing<br />Games</span>
					<span class="sf-sub-indicator">&#9660;</span>
				</a>
			</li>
			<li class="has-sub-menu">
				<ul class="sub-menu">
					<li class="sub-menu-item<?php echo ($currPage == "typingEquations") ? ' current"' : '"'; ?>>
					<a href="/typing-equations">Equations Used</a>
					</li>	
					<li class="sub-menu-item<?php echo ($currPage == "FAQ") ? ' current"' : '"'; ?>>
					<a href="/FAQ">FAQ</a>
					</li>
					<li class="sub-menu-item<?php echo ($currPage == "aboutUs") ? ' current"' : '"'; ?>>
					<a href="/about">About</a>
					</li>
				</ul>
				<a href="#">
					<span class="main_text" style="display: inline-block; opacity: 1;">More</span>
					<span class="sf-sub-indicator" style="top: 6px; left: -9px;">▼</span>
				</a>
			</li>
			<li id="et_mobile_nav_menu" class="has-sub-menu">
				<ul id="mobile_pages_menu" class="sub-menu">
					<li class="sub-menu-item">
						<a href="/home"<?php echo ($currPage == "home") ? ' class="current"' : ''; ?>>
							<span class="main_text" style="display: inline-block; opacity: 1; top: 0px;">Home</span>
						</a>
					</li>
					<li class="sub-menu-item">
						<a href="/typing-test"<?php echo ($currPage == "typingTest") ? ' class="current"' : ''; ?>>
							<span class="main_text" style="display: inline-block; opacity: 1;">Typing Test</span>
						</a>
					</li>
					<li class="sub-menu-item">
						<a href="/typing-tutor"<?php echo ($currPage == "typingTutor") ? ' class="current"' : ''; ?>>
							<span class="main_text" style="display: inline-block; opacity: 1;">Learn To Type</span>
						</a>
					</li>
					<li class="sub-menu-item">
						<a href="/typing-games"<?php echo ($currPage == "typingGames") ? ' class="current"' : ''; ?>>
							<span class="main_text" style="display: inline-block; opacity: 1;">Typing Games</span>
						</a>
					<li class="sub-menu-item">
						<a href="/typing-equations"<?php echo ($currPage == "typingEquations") ? ' class="current"' : ''; ?>>
							<span class="main_text" style="display: inline-block; opacity: 1; top: 0px;">Equations</span>
						</a>
					</li>	
					<li class="sub-menu-item">
						<a href="/aboutUs" id="lastMenuItem"<?php echo ($currPage == "aboutUs") ? ' class="current"' : ''; ?>>
							<span class="main_text" style="display: inline-block; opacity: 1; top: 0px;">About</span>
						</a>
					</li>	
				</ul>
				<a href="#">
					<span class="main_text">Menu</span>
				</a>
			</li>	
<?php 
			$user = new User();
			$aside = false;			// used to distinguish between menu or sidebar/aside
			$menuText = 'Login/<br />Register';
			$profileImage = '';
			$menuClass = 'main_text menu_2line';
			$tokenName = 'm_token_name';
			
			$path = $_SERVER['DOCUMENT_ROOT'];
			
			if($user->isPremium()){
				$iconHTML = '<img class="circle-image-premium-icon" alt="Bolt Icon" src="/images/crown.svg?v=13">';
				$powerClass = ' premium';
			} else {
				$iconHTML = '';
				$powerClass = '';
			}
			
			if ($user->isLoggedIn()){
				$path .= '/includes/widgets/loggedin.php';
				$menuText = $user->data()->username;
				$menuClass = 'main_text';
				$profileImage = ($user->data()->picture != '') ? '<img class="circle-image' . $powerClass . '" alt="User Profile Image" src="' . $user->data()->picture . '"/>' . $iconHTML : '';
			} else {
				$nExt = '_m';
				$path .= '/includes/widgets/login.php';
			}
?>			
			<li id="userMenu" class="has-sub-menu">
				<div class="sub-menu">
					<?php include $path; ?>
				</div>
				<a href="#" <?php echo ($currPage == "login" || $currPage == "register" || $currPage == "profile") ? 'class="current"' : ''; ?>>
					<span class="<?php echo $menuClass; ?>" style="display: inline-block; opacity: 1; font-weight:bold;">
						<?php echo $profileImage . $menuText; ?>
					</span>
					<span class="sf-sub-indicator">▼</span>
				</a>
			</li>								
		</ul>
	</nav>
</div>