
var lastRandNum;
var theEntry;
var tutStep = 0;
var tutIsPlaying = false;
var gRandKeyMode = false;
var gKeyAssignArr;
var gAnswerKey;
var gTotNumSteps;


function SetupKeyAssignArr(includeSpace=false){
	gKeyAssignArr = new Array(
		{"keycode":65,"theKey":"A","theFinger":"Left Pinky Finger","BHIC":"BH_leftPinky"},
		{"keycode":83,"theKey":"S","theFinger":"Left Ring Finger","BHIC":"BH_leftRing"},
		{"keycode":68,"theKey":"D","theFinger":"Left Middle Finger","BHIC":"BH_leftMiddle"},
		{"keycode":70,"theKey":"F","theFinger":"Left Pointer Finger","BHIC":"BH_leftIndex"},
		{"keycode":74,"theKey":"J","theFinger":"Right Pointer Finger","BHIC":"BH_rightIndex"},
		{"keycode":75,"theKey":"K","theFinger":"Right Middle Finger","BHIC":"BH_rightMiddle"},
		{"keycode":76,"theKey":"L","theFinger":"Right Ring Finger","BHIC":"BH_rightRing"},
		{"keycode":186,"theKey":";","theFinger":"Right Pinky Finger","BHIC":"BH_rightPinky"});
		
	if(includeSpace){
		gKeyAssignArr.push({"keycode":32,"theKey":"Space","theFinger":"thumbs","BHIC":"BH_thumbs"});
	}
}

function StartLessonTutorial(){
	$('#progressbarValue').css("width","0%");
	$('#pbTextVal').html("");
	$('#progressbarValue').stop(true, false);
	tutStep = 1;
	RunAnimationStep(tutStep);
}

function CleanSlate(){
	$('#BigHandClick').stop(true, false);
	$('#clickImg').stop(true, false);
	$('#bottomTutText').stop(true, false);
	$('#BigHandsImg').stop(true, false);
	$('#LessonTutorialText').stop(true, false);
	$('#replayBtn').stop(true, false);
	$('#findFJnub').stop(true, false);
	$('#BHC2').stop(true, false);
	//$('#progressbarValue').stop(true, false);
	$('#BigHandsImg').removeClass();
	
	$('#replayBtn').css("opacity", 0);
	$('#LessonTutorialText').css("opacity",0);
	$('#bottomTutText').css("opacity",0);
	$('#clickImg').css("opacity",0);
	$('#findFJnub').css("opacity",0);
	$('#BHC2').css("opacity",0);
	$('#keyArrow1').css("opacity",0);
	$('#keyArrow2').css("opacity",0);
	$('#keysLabels').css("opacity",100);
	//$('#progressbarValue').css("width","0%");
	//$('#pbTextVal').html("");
	
	gAnswerKeyCode = '';
}

function RunAnimationStep(theStep){
	CleanSlate();
	//if(theStep < 15){
		tutIsPlaying = true;
		if($('#KB_review').prop("checked")){
			
			gTotNumSteps = 22;
			RunReviewAnimationPart(1);
		} else {
			gTotNumSteps = 12;
			RunLessonAnimationPart(1);
		}
	//}
}

function DoProgressBar(theStep, duration){
	var percentComp = Math.round((theStep / gTotNumSteps) * 100);
	$('#pbTextVal').html(percentComp + "%");
	DoAnimation("progressbarValue", false, 0, {"width": percentComp + "%"}, duration, "easeInOutQuad", 0);
}

function GetRandomKeyIndex(){
	return Math.floor(Math.random() * gKeyAssignArr.length);
}

function RunReviewAnimationPart(thePart, theSpeed=false){
	if(tutStep == 1){
		switch(thePart){
			case 1:
				$('#LessonTutorialText').css("opacity",100);
				$('#BigHandsImg').css({"top": "-120px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thickText ltOrangeText">Reviewing</span>');
				$('#bottomTutText').css("opacity",0);
				DoAnimation("LessonTutorialText", false, 1000,false,false,false,thePart);
			break;
			case 2:
				$('#LessonTutorialText').html('<span class="thickText ltOrangeText">Reviewing</span><br /><span class="thickText">Keyboard &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>');
				DoAnimation("LessonTutorialText", false, 1000,false,false,false,thePart);
			break;
			case 3:
				$('#LessonTutorialText').html('<span class="thickText ltOrangeText">Reviewing</span><br /><span class="thickText">Keyboard Basics</span>');
				DoAnimation("LessonTutorialText", false, 2000,false,false,false,thePart);
			break;
			case 4:
				DoAnimation("LessonTutorialText", {"opacity": "100"}, 0, {"opacity": "0"}, 2000, "easeInOutQuad", thePart);
			break;
			case 5:
				$('#LessonTutorialText').html('<span class="thinText">Put your hands in</span><br /><span class="thickText ltBlueText">Home Position</span>');
				$('#LessonTutorialText').css("opacity",100);
				$('#bottomTutText').css("opacity",0);
				DoAnimation("BigHandsImg", {"top": "-120px"}, 1500, {top: "-204px"}, 3000, false, thePart);
			break;
			case 6:
				tutIsPlaying = false;
				gAnswerKey = " "; 	/* Spacebar */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">space</span> with thumbs</span>');
				$('#bottomTutText').css("opacity",100);
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 7:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_thumbs");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 8:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 2){
		switch(thePart){
			case 1:
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thinText">Complete this review</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:
				$('#LessonTutorialText').html('<span class="thinText">Complete this review</span><br /><span class="thickText">Without looking at the keyboard</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 3:	
				DoAnimation("keysLabels", {"opacity":"100"}, 500, {opacity: "0"}, 1600, "easeInOutExpo", thePart);
			break;
			case 4:
				tutIsPlaying = false;
				gAnswerKey = " "; 	/* Spacebar */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">space</span> with thumbs</span>');
				$('#bottomTutText').css("opacity",100);
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 5:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_thumbs");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 6:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 3){
		switch(thePart){
			case 1:
				$('#keysLabels').css("opacity", 0);
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thinText">Use the F-J bumps</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thinText">Use the F-J bumps</span><br /><span class="thinText">to verify home position</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 3:	
				$('#BigHandsImg').addClass("BH_rightIndex");
				$('#BHC2').addClass("BH_leftIndex");
				$('#BigHandClick').css("opacity", 100);
				$('#clickImg').css("opacity", 0);
				$('#BHC2').css("opacity", 100);
				DoAnimation("findFJnub", {"opacity":"0"}, 500, {opacity: "100"}, 3000,false,thePart);
			break;
			case 4:	
				DoAnimation("BigHandsImg", {"top": "-204px"}, 100, {top: "-198px"}, 500, false, thePart);
			break;
			case 5:	
				DoAnimation("BigHandsImg", {"top": "-198px"}, 100, {top: "-204px"}, 500, false, thePart);
			break;
			case 6:	
				DoAnimation("BigHandsImg", {"top": "-204px"}, 100, {top: "-198px"}, 500, false, thePart);
			break;
			case 7:	
				DoAnimation("BigHandsImg", {"top": "-198px"}, 100, {top: "-204px"}, 500, false, thePart);
			break;
			case 8:	
				DoAnimation("BigHandsImg", {"top": "-204px"}, 100, {top: "-198px"}, 500, false, thePart);
			break;
			case 9:	
				DoAnimation("BigHandsImg", {"top": "-198px"}, 100, {top: "-204px"}, 500, false, thePart);
			break;
			case 10:
				tutIsPlaying = false;
				gAnswerKey = "j"; 	/* J key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">right pointer finger</span></span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 11:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 12:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep >= 4 && tutStep <= 11){
		switch(thePart){
			case 1:
				gRandKeyMode = true;
				$('#keysLabels').css("opacity", 0);
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thinText">Press your</span>');
				DoAnimation("LessonTutorialText", false, 500,false,false,false,thePart);
			break;
			case 2:
				tutIsPlaying = false;
				if(gKeyAssignArr == undefined || gKeyAssignArr.length == 0)
					SetupKeyAssignArr();
				RKI = GetRandomKeyIndex();
				$('#LessonTutorialText').html('<span class="thinText">Press your</span><br /><span class="thickText">' + gKeyAssignArr[RKI].theFinger + '</span>');
				gAnswerKey = gKeyAssignArr[RKI].theKey.toLowerCase(); 	/* ? key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">' + gKeyAssignArr[RKI].theFinger + '</span></span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 3:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass(gKeyAssignArr[RKI].BHIC);
				gKeyAssignArr.splice(RKI, 1);
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 4:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 12){
		switch(thePart){
			case 1:
				$('#keysLabels').css("opacity", 0);
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thickText">Great Job!!!</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thickText">Great Job!!!</span><br /><span class="thinText">Let\'s try once more</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 3:
				tutIsPlaying = false;
				gAnswerKey = " "; 	/* Spacebar */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">space</span> with thumbs</span>');
				$('#bottomTutText').css("opacity",100);
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 4:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_thumbs");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 5:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 13){
		switch(thePart){
			case 1:
				$('#keysLabels').css("opacity", 0);
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thickText">Make sure</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thickText">Make sure</span><br /><span class="thinText">You aren\'t peeking!!!</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 3:
				tutIsPlaying = false;
				gAnswerKey = " "; 	/* Spacebar */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">space</span> with thumbs</span>');
				$('#bottomTutText').css("opacity",100);
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 4:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_thumbs");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 5:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep >= 14 && tutStep <= 21){
		switch(thePart){
			case 1:
				gRandKeyMode = true;
				$('#keysLabels').css("opacity", 0);
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thinText">Press your</span>');
				DoAnimation("LessonTutorialText", false, 500,false,false,false,thePart);
			break;
			case 2:
				tutIsPlaying = false;
				if(gKeyAssignArr == undefined || gKeyAssignArr.length == 0)
					SetupKeyAssignArr();
				RKI = GetRandomKeyIndex();
				$('#LessonTutorialText').html('<span class="thinText">Press your</span><br /><span class="thickText">' + gKeyAssignArr[RKI].theFinger + '</span>');
				gAnswerKey = gKeyAssignArr[RKI].theKey.toLowerCase(); 	/* ? key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">' + gKeyAssignArr[RKI].theFinger + '</span></span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 3:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass(gKeyAssignArr[RKI].BHIC);
				gKeyAssignArr.splice(RKI, 1);
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 4:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	}  else if(tutStep == 22){
		switch(thePart){
			case 1:
				$('#keysLabels').css("opacity", 0);
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thickText">Fantastic!!!</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thickText">Fantastic!!!</span><br /><span class="thinText">You have mastered Keyboard Basics!</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 3:
				tutIsPlaying = false;
				gAnswerKey = " "; 	/* Spacebar */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">space</span> with thumbs</span>');
				$('#bottomTutText').css("opacity",100);
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 4:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_thumbs");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 5:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 23){
		LessonCompleted();
		$('#keysLabels').css("opacity",0);
		$('#BigHandsImg').css({"top": "-204px","left":"50%"});
		$('#BigHandsImg').removeClass();
		$('#LessonTutorialText').html('<div><a class="thickText ltOrangeText" href="/typing-tutor.php?mod=1">Click Here</a></div><span class="thinText">To start learning to touch-type</span>');
		DoAnimation("LessonTutorialText", false, 1500,false,false,false,0);
	} else {
		tutIsPlaying = false;
	}
}

function RunLessonAnimationPart(thePart, theSpeed=false){
	if(theSpeed === false)
		theSpeed = 1;
	
	if(tutStep == 1){
		switch(thePart){
			case 1:
				$('#LessonTutorialText').css("opacity",100);
				$('#BigHandsImg').css({"top": "-120px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Learning</span>');
				$('#bottomTutText').css("opacity",0);
				DoAnimation("LessonTutorialText", false, 1000,false,false,false,thePart);
			break;
			case 2:
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Learning</span><br /><span class="thickText">Keyboard &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>');
				DoAnimation("LessonTutorialText", false, 1000,false,false,false,thePart);
			break;
			case 3:
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Learning</span><br /><span class="thickText">Keyboard Basics</span>');
				DoAnimation("LessonTutorialText", false, 2000,false,false,false,thePart);
			break;
			case 4:
				DoAnimation("LessonTutorialText", {"opacity": "100"}, 0, {"opacity": "0"}, 2000, "easeInOutQuad", thePart);
			break;
			case 5:
				$('#LessonTutorialText').css("opacity",100);
				$('#LessonTutorialText').html('<span class="thickText">Place fingers on keyboard</span>');
				$('#bottomTutText').css("opacity",0);
				DoAnimation("BigHandsImg", {"top": "-120px"}, 1500, {top: "-204px"}, 3000, false, thePart);
			break;
			case 6:
				$('#LessonTutorialText').html('<span class="thickText">Place fingers on keyboard</span><br /><span class="thinText">like this</span>');
				DoAnimation("LessonTutorialText", false, 2500,false,false,false,thePart);
			break;
			case 7:
				tutIsPlaying = false;
				gAnswerKey = " "; 	/* Spacebar */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">space</span> with thumbs</span>');
				$('#bottomTutText').css("opacity",100);
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 8:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_thumbs");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 9:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 2){
		switch(thePart){
			case 1:
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thinText">This is called the</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:
				$('#LessonTutorialText').html('<span class="thinText">This is called the</span><br /><span class="thickText ltBlueText">Home Position</span>');
				DoAnimation("LessonTutorialText", false, 2500,false,false,false,thePart);
			break;
			case 3:
				tutIsPlaying = false;
				gAnswerKey = " "; 	/* Spacebar */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">space</span> with thumbs</span>');
				$('#bottomTutText').css("opacity",100);
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 4:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_thumbs");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 5:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 3){
		var delayAmt = 800;
		switch(thePart){
			case 1:
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Home Position</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Home Position</span><br /><span class="thinText">Is</span>');
				$('#BigHandsImg').removeClass();
				$('#BigHandClick').css("opacity", 100)
				$('#BigHandsImg').addClass("BH_leftPinky");
				DoAnimation("LessonTutorialText", false, delayAmt,false,false,false,thePart);
			break;
			case 3:
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Home Position</span><br /><span class="thinText">Is every</span>');
				$('#BigHandsImg').removeClass();
				$('#BigHandClick').css("opacity", 100)
				$('#BigHandsImg').addClass("BH_leftRing");
				DoAnimation("LessonTutorialText", false, delayAmt,false,false,false,thePart);
			break;
			case 4:
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Home Position</span><br /><span class="thinText">Is every finger</span>');
				$('#BigHandsImg').removeClass();
				$('#BigHandClick').css("opacity", 100)
				$('#BigHandsImg').addClass("BH_leftMiddle");
				DoAnimation("LessonTutorialText", false, delayAmt,false,false,false,thePart);
			break;
			case 5:
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Home Position</span><br /><span class="thinText">Is every finger on</span>');
				$('#BigHandsImg').removeClass();
				$('#BigHandClick').css("opacity", 100)
				$('#BigHandsImg').addClass("BH_leftIndex");
				DoAnimation("LessonTutorialText", false, delayAmt,false,false,false,thePart);
			break;
			case 6:
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Home Position</span><br /><span class="thinText">Is every finger on only</span>');
				$('#BigHandsImg').removeClass();
				$('#BigHandClick').css("opacity", 100)
				$('#BigHandsImg').addClass("BH_rightPinky");
				DoAnimation("LessonTutorialText", false, delayAmt,false,false,false,thePart);
			break;
			case 7:
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Home Position</span><br /><span class="thinText">Is every finger on only one</span>');
				$('#BigHandsImg').removeClass();
				$('#BigHandClick').css("opacity", 100)
				$('#BigHandsImg').addClass("BH_rightRing");
				DoAnimation("LessonTutorialText", false, delayAmt,false,false,false,thePart);
			break;
			case 8:
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Home Position</span><br /><span class="thinText">Is every finger on only one key</span>');
				$('#BigHandsImg').removeClass();
				$('#BigHandClick').css("opacity", 100)
				$('#BigHandsImg').addClass("BH_rightMiddle");
				DoAnimation("LessonTutorialText", false, delayAmt,false,false,false,thePart);
			break;
			case 9:
				$('#LessonTutorialText').html('<span class="thickText ltBlueText">Home Position</span><br /><span class="thinText">Is every finger on only one key</span>');
				$('#BigHandsImg').removeClass();
				$('#BigHandClick').css("opacity", 100)
				$('#BigHandsImg').addClass("BH_rightIndex");
				DoAnimation("LessonTutorialText", false, delayAmt,false,false,false,thePart);
			break;
			case 10:
				tutIsPlaying = false;
				gAnswerKey = "j"; 	/* J key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">J</span> with right pointer finger</span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 11:
				$('#BigHandClick').css("opacity", 0);
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 12:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 4){
		switch(thePart){
			case 1:
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thickText">Always </span><span class="thinText">start and </span><span class="thinText">return to </span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thinText">Always </span><span class="thickText">start and return </span><span class="thinText">to </span><br /><span class="thinText">this position</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 3:
				tutIsPlaying = false;
				gAnswerKey = "f"; 	/* F key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">F</span> with left pointer finger</span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 4:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_leftIndex");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 5:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 5){
		switch(thePart){
			case 1:
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thickText">Now practice</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thickText">Now practice</span><br /><span class="thinText">Going to home position</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 3:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-204px","left":"50%"}, 0, {top: "-120px","left":"48%"}, 3000, false, thePart);
			break;
			case 4:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-120px","left":"48%"}, 500, {top: "-204px","left":"50%"}, 1500, false, thePart);
			break;
			case 5:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-204px","left":"50%"}, 400, {top: "-120px","left":"52%"}, 2000, false, thePart);
			break;
			case 6:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-120px","left":"52%"}, 400, {top: "-204px","left":"50%"}, 1000, false, thePart);
			break;
			case 7:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-204px","left":"50%"}, 300, {top: "-120px","left":"47%"}, 1000, false, thePart);
			break;
			case 8:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-120px","left":"47%"}, 300, {top: "-204px","left":"50%"}, 800, false, thePart);
			break;
			case 9:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-204px","left":"50%"}, 500, {top: "-120px","left":"52%"}, 800, false, thePart);
			break;
			case 10:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-120px","left":"52%"}, 500, {top: "-204px","left":"50%"}, 800, false, thePart);
			break;
			case 11:	
				$('#BigHandsImg').removeClass();
				$('#clickImg').css("opacity", 0);
				DoAnimation("BigHandsImg", {"top": "-204px","left":"50%"}, 1000, {top: "-120px","left":"49%"}, 300, false, thePart);
			break;
			case 12:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-120px","left":"49%"}, 500, {top: "-204px","left":"50%"}, 300, false, false);
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 13:
				tutIsPlaying = false;
				gAnswerKey = "a"; 	/* A key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">A</span> with left pinky finger</span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 14:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_leftPinky");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 15:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 6){
		switch(thePart){
			case 1:
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thinText">Now find home position...</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thinText">Now find home position...</span><br /><span class="thickText">Without Looking!?!</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 3:
				tutIsPlaying = false;
				gAnswerKey = "s"; 	/* S key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">S</span> with left ring finger</span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 4:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_leftRing");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 5:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 7){
		switch(thePart){
			case 1:
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thickText">How?</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thickText">How?</span><br /><span class="thinText">By using the F-J bumps!</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 3:
				tutIsPlaying = false;
				gAnswerKey = "j"; 	/* J key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">J</span> with right pointer finger</span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 4:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_rightIndex");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 5:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 8){
		switch(thePart){
			case 1:
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thinText">The </span><span class="thickText">F </span><span class="thinText">and </span><span class="thickText">J </span><span class="thinText">keys</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thinText">The </span><span class="thickText">F </span><span class="thinText">and </span><span class="thickText">J </span><span class="thinText">keys</span><br /><span class="thinText">have </span><span class="thickText">bumps</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 3:
				DoAnimation("BigHandsImg", {"top": "-204px"}, 100, {top: "-174px"}, 500, false, thePart);
			break;
			case 4:
				DoAnimation("keyArrow1", {"opacity": 0}, 100, {"opacity": 100}, 1500, "easeInOutExpo");
				DoAnimation("keyArrow2", {"opacity": 0}, 100, {"opacity": 100}, 1500, "easeInOutExpo", thePart);
			break;
			case 5:
				DoAnimation("keyArrow1", {"opacity": 0}, 100, {"opacity": 100}, 1500, "easeInOutExpo");
				DoAnimation("keyArrow2", {"opacity": 0}, 100, {"opacity": 100}, 1500, "easeInOutExpo", thePart);
			break;
			case 6:
				DoAnimation("keyArrow1", {"opacity": 0}, 100, {"opacity": 100}, 1500, "easeInOutExpo");
				DoAnimation("keyArrow2", {"opacity": 0}, 100, {"opacity": 100}, 1500, "easeInOutExpo", thePart);
			break;
			case 7:
				DoAnimation("keyArrow1", {"opacity": 0}, 100, {"opacity": 100}, 1500, "easeInOutExpo");
				DoAnimation("keyArrow2", {"opacity": 0}, 100, {"opacity": 100}, 1500, "easeInOutExpo", thePart);
			break;
			case 8:
				DoAnimation("keyArrow1", {"opacity": 100}, 100, {"opacity": 0}, 1500, "easeInOutExpo");
				DoAnimation("keyArrow2", {"opacity": 100}, 100, {"opacity": 0}, 1500, "easeInOutExpo");
				DoAnimation("BigHandsImg", {"top": "-174px"}, 100, {top: "-204px"}, 500, false, thePart);
			break;
			case 9:
				tutIsPlaying = false;
				gAnswerKey = "l"; 	/* L key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">L</span> with right ring finger</span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 10:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_rightRing");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 11:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 9){
		switch(thePart){
			case 1:
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thinText">Find the F-J bumps</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thinText">Find the F-J bumps</span><br /><span class="thinText">with your </span><span class="thickText">Pointer Fingers</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 3:	
				$('#BigHandsImg').addClass("BH_leftIndex");
				$('#BHC2').addClass("BH_rightIndex");
				$('#BigHandClick').css("opacity", 100);
				$('#clickImg').css("opacity", 0);
				$('#BHC2').css("opacity", 100);
				DoAnimation("findFJnub", {"opacity":"0"}, 500, {opacity: "100"}, 3000,false,thePart);
			break;
			case 4:	
				DoAnimation("BigHandsImg", {"top": "-204px"}, 100, {top: "-194px"}, 500, false, thePart);
			break;
			case 5:	
				DoAnimation("BigHandsImg", {"top": "-194px"}, 100, {top: "-204px"}, 500, false, thePart);
			break;
			case 6:	
				DoAnimation("BigHandsImg", {"top": "-204px"}, 100, {top: "-194px"}, 500, false, thePart);
			break;
			case 7:	
				DoAnimation("BigHandsImg", {"top": "-194px"}, 100, {top: "-204px"}, 500, false, thePart);
			break;
			case 8:	
				DoAnimation("BigHandsImg", {"top": "-204px"}, 100, {top: "-194px"}, 500, false, thePart);
			break;
			case 9:	
				DoAnimation("BigHandsImg", {"top": "-194px"}, 100, {top: "-204px"}, 500, false, thePart);
			break;
			case 10:	
				DoAnimation("BigHandsImg", {"top": "-204px"}, 100, {top: "-194px"}, 500, false, thePart);
			break;
			case 11:	
				DoAnimation("BigHandsImg", {"top": "-194px"}, 100, {top: "-204px"}, 500, false, thePart);
			break;
			case 12:	
				DoAnimation("BigHandsImg", {"top": "-204px"}, 100, {top: "-194px"}, 500, false, thePart);
			break;
			case 13:	
				DoAnimation("BigHandsImg", {"top": "-194px"}, 100, {top: "-204px"}, 1000, false, thePart);
			break;
			case 14:
				tutIsPlaying = false;
				gAnswerKey = "f"; 	/* F key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">F</span> with left pointer finger</span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 15:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 16:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 10){
		switch(thePart){
			case 1:
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thinText">Use these F-J bumps</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thinText">Use these F-J bumps</span><br /><span class="thinText">To find home position </span><span class="thickText">without looking</span>');
				DoAnimation("LessonTutorialText", false, 500,false,false,false,thePart);
			break;
			case 3:
				tutIsPlaying = false;
				gAnswerKey = "k"; 	/* K key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">K</span> with right middle finger</span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 4:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_rightMiddle");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 5:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 11){
		switch(thePart){
			case 1:	
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thinText">Practice finding home position</span>');
				DoAnimation("LessonTutorialText", false, 700,false,false,false,thePart);
			break;
			case 2:
				DoAnimation("BigHandsImg", {"top": "-204px"}, 100, {top: "-120px"}, 1400, false, thePart);
			break;
			case 3:	
				$('#LessonTutorialText').html('<span class="thinText">Practice finding home position</span><br /><span class="thickText">Without looking </span><span class="thinText">at the keyboard!</span>');
				DoAnimation("LessonTutorialText", false, 500,false,false,false,thePart);
			break;
			case 4:	
				DoAnimation("keysLabels", {"opacity":"100"}, 500, {opacity: "0"}, 1600, "easeInOutExpo", thePart);
			break;
			case 5:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-120px","left":"50%"}, 500, {top: "-204px","left":"50%"}, 1500, false, thePart);
			break;
			case 6:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-204px","left":"50%"}, 400, {top: "-120px","left":"52%"}, 2000, false, thePart);
			break;
			case 7:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-120px","left":"52%"}, 400, {top: "-204px","left":"50%"}, 1000, false, thePart);
			break;
			case 8:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-204px","left":"50%"}, 300, {top: "-120px","left":"47%"}, 1000, false, thePart);
			break;
			case 9:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-120px","left":"47%"}, 300, {top: "-204px","left":"50%"}, 800, false, thePart);
			break;
			case 10:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-204px","left":"50%"}, 500, {top: "-120px","left":"52%"}, 800, false, thePart);
			break;
			case 11:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-120px","left":"52%"}, 500, {top: "-204px","left":"50%"}, 800, false, thePart);
			break;
			case 12:	
				$('#BigHandsImg').removeClass();
				$('#clickImg').css("opacity", 0);
				DoAnimation("BigHandsImg", {"top": "-204px","left":"50%"}, 1000, {top: "-120px","left":"49%"}, 300, false, thePart);
			break;
			case 13:	
				$('#BigHandsImg').removeClass();
				DoAnimation("BigHandsImg", {"top": "-120px","left":"49%"}, 500, {top: "-204px","left":"50%"}, 300, false, false);
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 14:
				tutIsPlaying = false;
				gAnswerKey = ";"; 	/* ; key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">;</span> with right pinky finger</span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 15:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_rightPinky");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 16:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 12){
		switch(thePart){
			case 1:	
				$('#keysLabels').css("opacity",0);
				$('#BigHandsImg').css({"top": "-204px","left":"50%"});
				$('#BigHandsImg').removeClass();
				$('#LessonTutorialText').html('<span class="thickText">Congratulations!</span>');
				DoAnimation("LessonTutorialText", false, 1500,false,false,false,thePart);
			break;
			case 2:	
				$('#LessonTutorialText').html('<span class="thickText">Congratulations!</span><br /><span class="thinText">You learned keyboarding basics!</span>');
				DoAnimation("LessonTutorialText", false, 500,false,false,false,thePart);
			break;
			case 3:
				tutIsPlaying = false;
				gAnswerKey = "d"; 	/* D key */
				$('#bottomTutText').html('<span class="thickText ltBlueText">To Continue: </span><span class="thinText">Press <span class="ltOrangeText">D</span> with right ring finger</span>');
				DoAnimation("bottomTutText", false, 500,false,false,false,thePart);
			break;
			case 4:
				$('#BigHandClick').css("opacity", 0);
				$('#clickImg').css("opacity", 0);
				$('#BigHandsImg').addClass("BH_leftMiddle");
				DoAnimation("BigHandClick", false, 0, {opacity: "100"}, 1000,"easeInExpo",thePart);
			break;
			case 5:
				DoAnimation("clickImg", {"opacity":"0"}, 500, {opacity: "100"}, 1600, "easeInOutExpo", false, {"opacity":"0"}, 0);
				DoAnimation("replayBtn", {"opacity": 0}, 800, {"opacity": 100}, 0);
			break;
		}
	} else if(tutStep == 13){
		LessonCompleted();
		$('#keysLabels').css("opacity",0);
		$('#BigHandsImg').css({"top": "-204px","left":"50%"});
		$('#BigHandsImg').removeClass();
		$('#LessonTutorialText').html('<div><a class="thickText ltOrangeText" href="/keyboard-basics.php?mod=84">Click Here</a></div><span class="thinText">To review what you learned</span>');
		DoAnimation("LessonTutorialText", false, 1500,false,false,false,0);
	} else {
		tutIsPlaying = false;
	}
}

function DoAnimation(tag, startingCssArr=false, startDelay, cssArr=false, theDuration=false, theEasing=false, thePart=false, loopStartPosArr=false, loopDelay=false, loopCounter=false){
	var $elem = $("#" + tag);
	easingString = "swing";
	if(startingCssArr != false)
		$elem.css(startingCssArr);
	else
		startingCssArr = {"opacity":"100"};
	if(cssArr === false)
		cssArr = {"opacity":"100"};
	if(theEasing != false)
		easingString = theEasing;
	if(theDuration === false)
		theDuration = 500;
	
	
	$elem.animate(startingCssArr,{duration:startDelay, complete:function(){
			$elem.animate(cssArr, {duration: theDuration,easing: easingString, complete:function(){ 
					if(loopStartPosArr != false && loopCounter !== 0){
						if(loopCounter !== false)
							loopCounter--;
						$elem.t = setTimeout((function(){
							DoAnimation(tag,loopStartPosArr, 0,cssArr,theDuration,easingString,false,loopStartPosArr,loopDelay);
						}), loopDelay);
					} else { /* we aren't looping */
						if(thePart != false){
							thePart++;
							if($('#KB_review').prop("checked")){
								RunReviewAnimationPart(thePart);
							} else {
								RunLessonAnimationPart(thePart);
							}
						}
					}
				}
			});
		}
	});
}

function ResetLessonTutorial(){
	$("#BigHandsImg").css("top", "-80px");
}

function setupCurrentLesson(id, val, defaultVal, target){
	var elem = $(id);
	elem.html(val);
	if(val == defaultVal){
		elem.parent().removeClass("success");
		elem.parent().removeClass("warn");
		return;
	} 
	
	val = parseFloat(val);
	target = parseFloat(target);
	
	if(val < target){
		elem.parent().removeClass("success");
		elem.parent().addClass("warn");
	} else {
		elem.parent().addClass("success");
		elem.parent().removeClass("warn");
	}
}

function LessonCompleted(){
	$.post('setData.php', { type: "lh", lesson_mod: lessonModule, the_sa_id: sa_id, wpm: 300, accuracy: 100, total_entries: 0, incorrect_entries: 0, fixed_mistakes: 0, total_time_secs: 0},
		function(output){
			console.log(output);
	});
}

function setupLessonInfoPanel(theLessonTitle, theResultsTitle, thePurpose, theHint, lettersToLearn){
	var theLessonTitle = theLessonTitle===undefined ? "Default Purpose" : theLessonTitle;
	var theResultsTitle = theResultsTitle===undefined ? "Default Purpose" : theResultsTitle;
	var thePurpose = thePurpose===undefined ? "Default Purpose" : thePurpose;
	var theHint = theHint || "Focus on proper technique before speed.";
	var lettersToLearn = lettersToLearn===undefined ? "-" : lettersToLearn;
	var pn = $('#personName').val();
	var personalizedName = (pn == '' || pn == '<Name>') ? "" : pn + "'s ";
	
	$("#textPurposeDiv").html(theLessonTitle);
	$("#resultsTitle").html(personalizedName + theResultsTitle);
	$("#lessonPurposeDisplay").html(thePurpose);
	$("#lessonHintDisplay").html("<ul class=\"dashed\"><li>" + theHint.split("; ").join("</li><li>") + "</li></ul>");
	$("#lessonLettersToLearn").html(lettersToLearn);
}

function setupLessonProfileBlurb(JSONdata){
	var lessonData = JSONdata["lessonData"];
	var loggedInResults = JSONdata["loggedInResults"];
	var lessonsWithoutHistory = JSONdata["lessonsWithoutFullHistory"];
	
	if(!JSONdata["loggedInUsername"]){
		$('#loginOrRegister').html('<div id="askToLogin" class="stoInfoBox" onclick="location.href=\'/login.php\';"><span><a href="/login.php">Login</a> or <a href="/register.php">Register</a></span><span> to track progress and set targets.</span></div>');
	} else{
		var totalLessonsPassed = loggedInResults.classicPassedCount + loggedInResults.advancedPassedCount;
		var totalLessons = loggedInResults.classicLessonCount + loggedInResults.advancedLessonCount;
		
		$('#numLessonsPassed').html(totalLessonsPassed + ' <span class="noteSmall">(' + Math.round((totalLessonsPassed/totalLessons)*1000)/10 + '%)</span>');
		$('#classicLessons').html(loggedInResults.classicLessonsHTML);
		
		var lessonWPM;
		var lessonAcc;
		
		if(textSelection == "CUSTOM_LESSON"){
			lessonWPM = tutorGlobals['customLessonWPM'];
			lessonAcc = tutorGlobals['customLessonAccuracy'];
		} else {
			lessonWPM = (lessonsWithoutHistory[lessonModule].wpm == null) ? tutorGlobals["defaultWPM"] : lessonsWithoutHistory[lessonModule].wpm;
			lessonAcc = (lessonsWithoutHistory[lessonModule].accuracy == null) ? tutorGlobals["defaultAccuracy"] : lessonsWithoutHistory[lessonModule].accuracy;
			if(lessonAcc.slice(-2) == '.0'){ lessonAcc = lessonAcc.slice(0,-2);}
		}
				
		tutorGlobals["targetWPM"] = loggedInResults.wpmTarget;
		tutorGlobals["targetAccuracy"] = loggedInResults.accTarget;
		
		setupCurrentLesson('#currentLessonWPM', lessonWPM, tutorGlobals["defaultWPM"], loggedInResults.wpmTarget);
		setupCurrentLesson('#currentLessonAcc', lessonAcc, tutorGlobals["defaultAccuracy"], loggedInResults.accTarget);
		
		if(lessonWPM == tutorGlobals["defaultWPM"] || lessonAcc == tutorGlobals["defaultAccuracy"]){
			$('#lessonsPassedDiv').show();
			$('#lessonScoreDiv').hide();
		} else {
			var lessonScore = 0;
			var scoreHTML = '';
			var wpmScore = lessonWPM / loggedInResults.wpmTarget;
			var accScore = lessonAcc / loggedInResults.accTarget;
			
			var lowestScore = Math.min(wpmScore, accScore);
			if(lowestScore < 1){
				lessonScore = lowestScore;
			} else {
				lessonScore = wpmScore + accScore;
				
				if(lessonsWithoutHistory[lessonModule].lesson_type.toLowerCase().indexOf("review") >= 0){
					scoreHTML = ' <i class="fa fa-star" aria-hidden="true"></i>';
				} else {
					scoreHTML = ' <i class="fa fa-check" aria-hidden="true"></i>';
				}
				
			}
			
			$('#lessonsPassedDiv').hide();
			$('#lessonScoreDiv').show();
		}
		
		
		scoreHTML = Math.floor(lessonScore * 100) + scoreHTML;
		$('#currentLessonScore').html(scoreHTML);
		
		
	}
}

function setupLessonText(textString){
	var tempStr = "";
	var tempBlockDivStr = new Array("","","","");
	var localCutoff = 0;
	minCursorReplaceStart = 0;
	
	for(i=0; i < 4; i++){
		tempStr = textString.substring(cutoff, (42*(i+1)) - ((42*i) - cutoff));   // get next 42 characters from text string
		localCutoff = tempStr.search(/(\r\n|\n|\r)/);
		if (localCutoff < 1){      // if next 42 characters do not contain a carriage return (\n) (or solely a newline)
			
		
			localCutoff = tempStr.lastIndexOf(" ") + 1;                 // find last space in 42 chars

			if(localCutoff == 0){
				localCutoff = 42;   // if no space found within next 42 chars, cut word at full block line length
			}

			for(j=0; j<localCutoff; j++){

				blockDivText[i] += '<span class="plainText">' + tempStr.substring(j, j+1) + '</span>';
			}
			//blockDivText[i] = tempStr2.substring(0,localCutoff);     USE WITHOUT SECOND INSIDE FOR LOOP
			cutoff += localCutoff;
		}
		else{       // next 42 characters contained a carriage return
			var tempStr2 = tempStr.substring(0, localCutoff+1);
			tempStr2 = tempStr2.replace(/(\r\n|\n|\r|\s\r\n|\s\n|\s\r)/," ");

			for(j=0; j<tempStr2.length; j++){

				blockDivText[i] += '<span class="plainText">' + tempStr2.substring(j, j+1) + '</span>';
			}
			cutoff += localCutoff + 1;
			
		}
		if(i <= 1){ minCursorReplaceStart += localCutoff + 2;}      // minCursorReplaceStart is the length of the very first two lines...
		
	}
	// place cursor on first letter
	blockDivText[0] = blockDivText[0].replace(/plainText/i,"nxtLetter");
	tempBlockDivStr[0] = blockDivText[0];
	tempBlockDivStr[1] = blockDivText[1];
	tempBlockDivStr[2] = blockDivText[2];
	tempBlockDivStr[3] = blockDivText[3];
	
	cleanTextString = textString.replace(/\s\n|\n\s|\n/gm," ");    // remove all newline characters leaving spaces if needed
	tempStr = "";   // initialize for line div
	var spaceFound = textString.indexOf(" ", 41);
	cleanTextString = cleanTextString + "                                 ";				// add some blank spaces to the end so cursor stays at center at end of string
	
	tempStr += '<span class="nxtLetter">' + cleanTextString.substring(0, 1) + '</span>';
	keyIndicator(cleanTextString.substring(0, 1));
	
	for(i=1; i<=spaceFound; i++){
		
		tempStr += '<span class="plainText">' + cleanTextString.substring(i, i+1) + '</span>';
		
	}
					
	lineDivText = '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
						+ tempStr.substring(0,(32*22));
	
	
	blockDiv = $('#blockDivContainer').get();
	blockDiv = blockDiv[0];
	
	lineDiv = $('#lineDivContainer').get();
	lineDiv = lineDiv[0];
	
	blockDiv.innerHTML = formatForInnerHTML_block(tempBlockDivStr);
	lineDiv.innerHTML = formatForInnerHTML(lineDivText);
		
	//Target IE6 and below
	/*if ($.browser.msie && $.browser.version <= 6 ) {
		$("div.blockLines").css("font-size", "25px" );
		$("#line_input").css("font-size", "25px" );
	}*/
	
	$('#lessonDivContainer').show();
	disableKeyboard = false;
}

function getLessonData(callback){
	$.post('getData.php', { type: "tl" },callback);
}

 
 function keyIndicator(nL){

var numRowHeight = -249;
var topRowHeight = -205;		// was 77px
var homeRowHeight = -161;		// was 121px
var botRowHeight = -116;		// was 166px

	switch(nL){
		case "a": doKeyPressShading(true, 109, homeRowHeight, false, false, false, 1); break;
		case "A": doKeyPressShading(true, 109, homeRowHeight, false, false, true, 18); break;
		case "b": doKeyPressShading(true, 306, botRowHeight, false, false, false, 4); break;
		case "B": doKeyPressShading(true, 306, botRowHeight, false, false, true, 48); break;
		case "c": doKeyPressShading(true, 218, botRowHeight, false, false, false, 3); break;
		case "C": doKeyPressShading(true, 218, botRowHeight, false, false, true, 38); break;
		case "d": doKeyPressShading(true, 197, homeRowHeight, false, false, false, 3); break;
		case "D": doKeyPressShading(true, 197, homeRowHeight, false, false, true, 38); break;
		case "e": doKeyPressShading(true, 185, topRowHeight, false, false, false, 3); break;
		case "E": doKeyPressShading(true, 185, topRowHeight, false, false, true, 38); break;
		case "f": doKeyPressShading(true, 241, homeRowHeight, false, false, false, 4); break;
		case "F": doKeyPressShading(true, 241, homeRowHeight, false, false, true, 48); break;
		case "g": doKeyPressShading(true, 285, homeRowHeight, false, false, false, 4); break;
		case "G": doKeyPressShading(true, 285, homeRowHeight, false, false, true, 48); break;
		case "h": doKeyPressShading(true, 329, homeRowHeight, false, false, false, 5); break;
		case "H": doKeyPressShading(true, 329, homeRowHeight, false, true, false, 15); break;
		case "i": doKeyPressShading(true, 406, topRowHeight, false, false, false, 6); break;
		case "I": doKeyPressShading(true, 406, topRowHeight, false, true, false, 16); break;
		case "j": doKeyPressShading(true, 373, homeRowHeight, false, false, false, 5); break;
		case "J": doKeyPressShading(true, 373, homeRowHeight, false, true, false, 15); break;
		case "k": doKeyPressShading(true, 418, homeRowHeight, false, false, false, 6); break;
		case "K": doKeyPressShading(true, 418, homeRowHeight, false, true, false, 16); break;
		case "l": doKeyPressShading(true, 463, homeRowHeight, false, false, false, 7); break;
		case "L": doKeyPressShading(true, 463, homeRowHeight, false, true, false, 17); break;
		case "m": doKeyPressShading(true, 394, botRowHeight, false, false, false, 5); break;
		case "M": doKeyPressShading(true, 394, botRowHeight, false, true, false, 15); break;
		case "n": doKeyPressShading(true, 350, botRowHeight, false, false, false, 5); break;
		case "N": doKeyPressShading(true, 350, botRowHeight, false, true, false, 15); break;
		case "o": doKeyPressShading(true, 451, topRowHeight, false, false, false, 7); break;
		case "O": doKeyPressShading(true, 451, topRowHeight, false, true, false, 17); break;
		case "p": doKeyPressShading(true, 496, topRowHeight, false, false, false, 8); break;
		case "P": doKeyPressShading(true, 496, topRowHeight, false, true, false, 18); break;
		case "q": doKeyPressShading(true, 97, topRowHeight, false, false, false, 1); break;
		case "Q": doKeyPressShading(true, 97, topRowHeight, false, false, true, 18); break;
		case "r": doKeyPressShading(true, 229, topRowHeight, false, false, false, 4); break;
		case "R": doKeyPressShading(true, 229, topRowHeight, false, false, true, 48); break;
		case "s": doKeyPressShading(true, 153, homeRowHeight, false, false, false, 2); break;
		case "S": doKeyPressShading(true, 153, homeRowHeight, false, false, true, 28); break;
		case "t": doKeyPressShading(true, 273, topRowHeight, false, false, false, 4); break;
		case "T": doKeyPressShading(true, 273, topRowHeight, false, false, true, 48); break;
		case "u": doKeyPressShading(true, 361, topRowHeight, false, false, false, 5); break;
		case "U": doKeyPressShading(true, 361, topRowHeight, false, true, false, 15); break;
		case "v": doKeyPressShading(true, 262, botRowHeight, false, false, false, 4); break;
		case "V": doKeyPressShading(true, 262, botRowHeight, false, false, true, 48); break;
		case "w": doKeyPressShading(true, 141, topRowHeight, false, false, false, 2); break;
		case "W": doKeyPressShading(true, 141, topRowHeight, false, false, true, 28); break;
		case "x": doKeyPressShading(true, 174, botRowHeight, false, false, false, 2); break;
		case "X": doKeyPressShading(true, 174, botRowHeight, false, false, true, 28); break;
		case "y": doKeyPressShading(true, 317, topRowHeight, false, false, false, 5); break;
		case "Y": doKeyPressShading(true, 317, topRowHeight, false, true, false, 15); break;
		case "z": doKeyPressShading(true, 130, botRowHeight, false, false, false, 1); break;
		case "Z": doKeyPressShading(true, 130, botRowHeight, false, false, true, 18); break;
		case ";": doKeyPressShading(true, 508, homeRowHeight, false, false, false, 8); break;
		case ",": doKeyPressShading(true, 439, botRowHeight, false, false, false, 6); break;
		case ".": doKeyPressShading(true, 484, botRowHeight, false, false, false, 7); break;
		case "\"": doKeyPressShading(true, 553, homeRowHeight, false, true, false, 18); break;
		case "'": doKeyPressShading(true, 553, homeRowHeight, false, false, false, 8); break;
		case " ": doKeyPressShading(false, 0, 0, true, false, false, 9); break;
		case "!": doKeyPressShading(true, 75, numRowHeight, false, false, true, 18); break;
		case "?": doKeyPressShading(true, 529, botRowHeight, false, true, false, 18); break;
		default: doKeyPressShading(false, 0, 0, false, false, false, 0);
	}
 }
 
 // This function is called to position and show/hide certain key shading divs
 function doKeyPressShading(kPBool, kpLeft, kpTop, spaceBool, L_ShiftBool, R_ShiftBool, fingersToUse){
	if(kPBool){		
		$("#keyToPress").show(); 
	}
	else{
		$("#keyToPress").hide();
	}
	
	if(spaceBool){		
		$("#spaceToPress").show(); 
	}
	else{
		$("#spaceToPress").hide();
	}
	$("#keyToPress").css("margin-left", kpLeft);
	$("#keyToPress").css("margin-top", kpTop);
	
	if(L_ShiftBool){		
		$("#L_ShiftToPress").show(); 
		$("#R_ShiftToPress").hide();
	}
	else if(R_ShiftBool){
		$("#R_ShiftToPress").show();
		$("#L_ShiftToPress").hide();
	}
	else{
		$("#L_ShiftToPress").hide();
		$("#R_ShiftToPress").hide();
	}
	
	var L_Pink = "rect(42px 40px 100px 0px)";
	var L_Ring = "rect(0px 62px 77px 36px)";
	var L_Mid = "rect(0px 81px 100px 62px)";
	var L_Index = "rect(0px 120px 100px 80px)";
	var R_Index = "rect(0px 73px 80px 35px)";
	var R_Mid = "rect(0px 95px 76px 69px)";
	var R_Ring = "rect(0px 118px 76px 92px)";
	var R_Pink = "rect(40px 150px 100px 115px)";
	var L_Thumb = "rect(80px 162px 134px 95px)";
	var R_Thumb = "rect(80px 55px 150px 0px)";
	var noFinger = "rect(0px 0px 0px 0px)";
	
	switch(fingersToUse){
		case 1:	$("#L_FingerToPress").css("clip", L_Pink); $("#R_FingerToPress").css("clip", noFinger);	break;
		case 2:	$("#L_FingerToPress").css("clip", L_Ring); $("#R_FingerToPress").css("clip", noFinger); break;
		case 3:	$("#L_FingerToPress").css("clip", L_Mid); $("#R_FingerToPress").css("clip", noFinger); break;
		case 4:	$("#L_FingerToPress").css("clip", L_Index); $("#R_FingerToPress").css("clip", noFinger); break;
		case 5:	$("#L_FingerToPress").css("clip", noFinger); $("#R_FingerToPress").css("clip", R_Index); break;
		case 6:	$("#L_FingerToPress").css("clip", noFinger); $("#R_FingerToPress").css("clip", R_Mid); break;
		case 7:	$("#L_FingerToPress").css("clip", noFinger); $("#R_FingerToPress").css("clip", R_Ring); break;
		case 8:	$("#L_FingerToPress").css("clip", noFinger); $("#R_FingerToPress").css("clip", R_Pink);	break;
		case 9:	$("#L_FingerToPress").css("clip", L_Thumb);	$("#R_FingerToPress").css("clip", R_Thumb);	break;
		case 15: $("#L_FingerToPress").css("clip", L_Pink); $("#R_FingerToPress").css("clip", R_Index); break;
		case 16: $("#L_FingerToPress").css("clip", L_Pink); $("#R_FingerToPress").css("clip", R_Mid); break;
		case 17: $("#L_FingerToPress").css("clip", L_Pink); $("#R_FingerToPress").css("clip", R_Ring); break;
		case 18: // 18 = 1 + 8 = left pinkie + right pinkie
			$("#L_FingerToPress").css("clip", L_Pink); $("#R_FingerToPress").css("clip", R_Pink); break;
		case 28: $("#L_FingerToPress").css("clip", L_Ring); $("#R_FingerToPress").css("clip", R_Pink); break;
		case 38: $("#L_FingerToPress").css("clip", L_Mid); $("#R_FingerToPress").css("clip", R_Pink); break;
		case 48: $("#L_FingerToPress").css("clip", L_Index); $("#R_FingerToPress").css("clip", R_Pink); break;
		default: $("#L_FingerToPress").css("clip", noFinger); $("#R_FingerToPress").css("clip", noFinger);
		}
 }

function ApplyColorsBlurred(){
    var e = new Array;
    var i = 0;
    var fs = false;
    while ((i < document.styleSheets.length) && !fs) {
        if (document.styleSheets[i].cssRules && (findCSSRule("span.goodEntry", document.styleSheets[i].cssRules) >= 0)) {
            e = document.styleSheets[i].cssRules;
            fs = true;
        } else if (document.styleSheets[i].rules && (findCSSRule("span.goodEntry", document.styleSheets[i].rules) >= 0)) {
            e = document.styleSheets[i].rules;
            fs = true;
        }
        i++
    }
    if (fs) {
        e[findCSSRule('span.nxtLetter',e)].style.color = $("#PTFC").val(); 
		e[findCSSRule('span.nxtLetter',e)].style.backgroundColor = $("#PTBack").val();
    }
}

function ApplyColorsFocused(){
    var e = new Array;
    var i = 0;
    var fs = false;
    while ((i < document.styleSheets.length) && !fs) {
        if (document.styleSheets[i].cssRules && (findCSSRule("span.goodEntry", document.styleSheets[i].cssRules) >= 0)) {
            e = document.styleSheets[i].cssRules;
            fs = true;
        } else if (document.styleSheets[i].rules && (findCSSRule("span.goodEntry", document.styleSheets[i].rules) >= 0)) {
            e = document.styleSheets[i].rules;
            fs = true;
        }
        i++
    }
    if (fs) {
        e[findCSSRule('span.nxtLetter',e)].style.color = $("#CFC").val(); 
		e[findCSSRule('span.nxtLetter',e)].style.backgroundColor = $("#CBack").val();
    }
}
