<?php 
$currPage = "typingTest";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$htmlTitle = "Free Online Typing Test";
$htmlDescription = "Free online typing test to see how fast you type! Features lots of text options and many test lengths. Easy and fun way to test and improve your typing speed.";
$cssFiles = "ah";
$noWidgets = true;
$jQuery = true;
$fontAwesomeCDN = true;
require_once 'includes/overall/header.php';

$user = new User();
$userData = $user->data();
$debugMode = $user->hasPermission('debug') || Config::get('constants/debugMode');

if ($userData->input_mode == "INLINE"){
	$inputMode['BLOCK'] = ' style="display:none;"';
	$inputMode['INLINE'] = '';
	$divCoverStyle = ' style="z-index: 0; height:100%; margin-top:50px;"';
	$divGlowStyle = ' style="height:100%; top:48px; left:0px;"';
	
} else {
	$inputMode['BLOCK'] = '';
	$inputMode['INLINE'] = ' style="display:none;"';
	$divCoverStyle = ' style="z-index: 0; height:100%; margin-top:-1px;"';
	$divGlowStyle = ' style="height:100%; top:-2px; left:0px;"';
}
//var_dump($user);
//exit();
$textType["ENTRY"] = '';
$textType["SUMMARY"] = '';
$textType["BOOK"] = '';
$textType["SHORT_STORY"] = '';
$textType["FABLES"] = '';
$textType["LYRICS"] = '';
$textType["DATA_ENTRY"] = '';
$textType["PANGRAMS"] = '';
$textType["FACTS"] = '';
$textType["RANDOM_WORDS"] = '';
$textType["PROVERBS"] = '';
$textType["NUMPAD"] = '';
$textType["CUSTOM"] = '';


if(($ttdl = Input::get('ttdl')) != ''){
	$textType[$ttdl] = ' selected="selected"';
} else if($userData->tt_text_type == ''){
	$textType['ENTRY'] = ' selected="selected"';
} else {
	$textType[$userData->tt_text_type] = ' selected="selected"';
}

// change text type dropdown based on selected keyboard layout
$ttdl_normal = "<option value=\"ENTRY\"" . $textType["ENTRY"] . ">All Texts</option>" . 
				"<option value=\"SUMMARY\"" . $textType["SUMMARY"] . ">Book Summaries</option>" . 
				"<option value=\"BOOK\"" . $textType["BOOK"] . ">Books</option>" . 
				"<option value=\"SHORT_STORY\"" . $textType["SHORT_STORY"] . ">Short Stories</option>" . 
				"<option value=\"FABLES\"" . $textType["FABLES"] . ">Fables (Easy-To-Type)</option>" . 
				"<option value=\"LYRICS\"" . $textType["LYRICS"] . ">Song Lyrics</option>" . 
				"<option value=\"DATA_ENTRY\"" . $textType["DATA_ENTRY"] . ">Data Entry</option>" . 
				"<option value=\"PANGRAMS\"" . $textType["PANGRAMS"] . ">Random Pangrams</option>" . 
				"<option value=\"FACTS\"" . $textType["FACTS"] . ">Random Facts</option>" . 
				"<option value=\"RANDOM_WORDS\"" . $textType["RANDOM_WORDS"] . ">Random Words</option>" . 
				"<option value=\"PROVERBS\"" . $textType["PROVERBS"] . ">Proverbs</option>" . 
				"<option value=\"CUSTOM\"" . $textType["CUSTOM"] . ">Custom</option>";
							
$ttdl_numpad = 	"<option value=\"NUMPAD\"" . $textType["NUMPAD"] . ">Numpad (10-key)</option>" . 
				"<option value=\"CUSTOM\"" . $textType["CUSTOM"] . ">Custom</option>";

$keyboardArr = Config::get('constants/keyboards');
$keyboardType = array();
foreach($keyboardArr as &$keyboard){
	$keyboardType[$keyboard] = '';
}
if($userData->tt_keyboard_layout == ''){
	$keyboardType['QWERTY'] = ' selected="selected"';
} else {
	$keyboardType[$keyboardArr[$userData->tt_keyboard_layout]] = ' selected="selected"';
}

if($userData->tt_double_spacing == 1){
	$doubleSpacingSetting = ' checked';
} else {
	$doubleSpacingSetting = '';
}

if($userData->tt_color_text == '0'){
	$textColoringSetting = '';
} else {
	$textColoringSetting = ' checked';
}

if($userData->tt_phase_shift == '0'){
	$phaseShiftSetting = '';
} else {
	$phaseShiftSetting = ' checked';
}

$testLength = getTestLength($userData, $debugMode);
?>

<div id="wrap">
<div id="main">
    
    <div id="wrapHeading">
	
        <div id="heading">
            <h1>Free Online Typing Test</h1>
			<span>
				<h3><a href="#how_to">How to »</a></h3>
				<h3><a href="#feature_jump">Features »</a></h3>
			</span>
        </div>
    </div>
</div>
<?php echo getAd("rightSky", $currPage, $debugMode, $user->isPremium());?>


    <div id="centerConsole">
		<div id="cc_offset">
<?php
	$db_obj = DB::getInstance()->get('typing_tests', array('user_id', '=', $userData->id), 'wpm, accuracy, timestamp', null, null, array(9999999,0));
	
	$results = $db_obj->results();
	$numResults = sizeof($results);
	$wpmArr = array();
	$accuracyArr = array();
	
	if($results != null){
		foreach($results as $entry){
			array_push($wpmArr, $entry->wpm);
			array_push($accuracyArr, $entry->accuracy);
		}
	} else {
		array_push($wpmArr, 0);
	}
	
	if($numResults == 0){
		$averageWPM = 0;
		$averageAcc = 0;
	} else {
		$averageWPM = round(array_sum($wpmArr) / $numResults, 1);
		$averageAcc = round(array_sum($accuracyArr) / $numResults, 1);
	}
	
	include 'includes/profileBlurb.php';
	echo getAd("lead", $currPage, $debugMode, $user->isPremium());
	if(!$user->isLoggedIn() && !inMaintenanceMode()){
?>
			<div id="loginOrRegister">
				<div id="askToLogin" class="stoInfoBox" onclick="location.href='/login.php';">
					<span>

						<a href="/login.php">Login</a> or <a href="/register.php">Register</a>
					</span>
					<span> to track your progress.</span>
					
				</div>
			</div>
<?php	
	}
?>
			<div id="centerContent">
				<div id="divBackWrap">
					<div id="divBackTop"></div>
					<div id="divBackStripe"><div></div></div>
					<div id="divBackBottom"></div>
				</div>

				<div id='wrapStats'>
					<div id='timerDiv'>   
						<div id="timerText" class="statsText">Time</div>
						<div id='timer'>01:00</div>
					</div>
					<div id='wpmDiv'>                
						<div id='wpmText' class="statsText">Speed (WPM)</div>
						<div id='wpmValue'>0</div>
					</div>
					<div id='accuracyDiv'> 
						<div id='accuracyText' class="statsText">Accuracy</div>
						<div id='accuracyValue'>100 <span>%</span></div>
					</div>
				</div>
				<div id="containerWrapper">
					<div id="blockDivGlow"<?php echo $divGlowStyle;?>></div>
					<div id="divCover"<?php echo $divCoverStyle;?>></div>
					<div id="lineDivContainer" class="mainDivInputs" tabindex="0" onmouseover="this.focus()" onmouseout="this.blur()"<?php echo $inputMode['INLINE'];?>></div>
					<div id="blockDivContainer" class="mainDivInputs" tabindex="0" onmouseover="this.focus()" onmouseout="this.blur()"<?php echo $inputMode['BLOCK'];?>></div>
					<?php 
						if($isMobile === true){ 
							echo '<textarea id="mobileTextArea"></textarea>';}
					?>
				</div>
				
				<div id="buttonWrapper">
					<div id="resetBtnWrap">
						<div id="resetBtnDiv">
							<input type="button" name="ResetBtn" value="Reset" id="resetBtn"
							onclick="Reset($('#timeTypeSelected').val(),'timer')">
						</div>
					</div>
				
					<div id="switchModesBtnWrap">
						<div id="switchModesBtnDiv">
							<input type="button" name="SwitchModesBtn" value="Switch-Up" id="switchModesBtn">
						</div>
					</div>
					
					<?php 
						
						$nameToPrint = "<Name>";
						$personalName = '';
						
						if($user->isLoggedIn()){
							if(!empty($userData->firstname) || !empty($userData->lastname)){
								$nameToPrint = $user->fullName() . '\'s ';
								$personalName = $user->fullName();
							}
							else{
								$nameToPrint = $userData->username . '\'s ';
							}
						}
						
					?>
					
					<div id="PRIW" style="display: none">
						<form>
							<input type="text" id="personName" maxlength="25" value="<?php echo $personalName?>">
						</form>
						<button type="button" id="PN_Button" class="mediumBtn submitButtonCarrot">Personalize Results</button>	
					</div>
				</div>
				
				<div id="resultDivContainer" class="mainDivInputs" style="display: none; ">
					<div id="resultDiv">
						<div id="resultsTopWrap">
							<div id="resultsTitle">
								<?php   echo $nameToPrint;?>Test Results
							</div>
							<div id="resultsCol1Wrap" class="iResults">

								<div id="WPM_Rtext" 
									 title="<strong>Raw speed with mistake penalties</strong><br /> Speed = Raw speed - Error Rate <br /> (each mistake left in per minute is -1 WPM)" 
									 class="resultStatRight">Speed: </div>
								<div id="Accur_Rtext" 
									 title="<strong>The percentage of correct <br />entries out of the total entries typed:</strong><br />Accuracy = (Correct Letters / Total Letters) x 100%" 
									 class="resultStatRight">Accuracy: </div>
							</div>
							<div id="resultsCol2Wrap" class="iResults">
								<div id="WPM_Result">0</div>
								<div id="Accur_Result">0</div>
							</div>
							<div id="resultsCol3Wrap" class="uiResults">
								<div id="LTC_Rtext" 
									 title="<strong>All keys typed correctly</strong><br />(even correct entries deleted then re-entered <br />correctly again)" 
									 class="resultStatLeft">Correct Entries: </div>
								<div id="nLTI_Rtext" 
									 title="<strong>All keys typed incorrectly </strong><br />(even if deleted and corrected later)" 
									 class="resultStatLeft">Incorrect Entries: </div>
								<div id="MF_Rtext"
									 title="<strong>All incorrect entries that were fixed later</strong><br /> (number of mistakes deleted and entered correctly)"
									 class="resultStatLeft">Fixed Mistakes: </div>
								<div id="TLT_Rtext"
									 title="<strong>All keys typed (excluding function keys)</strong> <br />Total Letters = Correct Entries + Incorrect Entries"
									 class="resultStatLeft">Total Entries: </div>
								<div id="ER_Rtext"
									 title="<strong>Number of incorrect entries<br /> (unfixed mistakes) per minute </strong><br /> [Error Rate = (Incorrect Entries - Fixed Mistakes) / Time in Minutes]"
									 class="resultStatLeft">Error Rate: </div>
								<div id="rWPM_Rtext"
									 title="<strong>Typing Speed before mistake penalties </strong><br /> Raw Speed = ( Total Entries / Time in minutes ) / 5 <br /> (where 5 = average length of a word)"
									 class="resultStatLeft">Raw Speed: </div>
								<div id="KPM_Rtext"
									 title="<strong>Number of keystrokes per minute </strong><br /> Key Speed = Total Entries / Time in minutes"
									 class="resultStatLeft">Key Speed: </div>
								<div id="words_Rtext"
									 title="<strong>Number of complete words typed </strong><br /> ('the' and 'messenger' are both one word)"
									 class="resultStatLeft">Complete Words: </div>
								<div id="time_Rtext"
									 title="<strong>Total time of round in minutes</strong><br /> (Time in minutes = Time in seconds / 60)"
									 class="resultStatLeft">Total Time: </div>
								 <div id="note_Rtext">*mouse over labels for more info</div>
							</div>
							<div id="resultsCol4Wrap" class="uiResults">
								<div id="LTC_Result">0</div>
								<div id="nLTI_Result">0</div>
								<div id="CL_Result">0</div>
								<div id="TLT_Result">0</div>
								<div id="ER_Result">0</div>
								<div id="rWPM_Result">0</div>
								<div id="KPM_Result">0</div>
								<div id="words_Result">0</div>
								<div id="time_Result">0</div>
							</div>
							<div id="snShare">
								<div id="fbLink">
									<a id="fbrLink" href="https://www.facebook.com/share.php?u=<?php echo Config::get('constants/rootUrl');?>/fbr.php" onClick="return popup(this, 'notes')" class="share_link">
										<div id="fbIcon" class="share_icon"></div>
										<span>Share Results on Facebook</span>
									</a>
								</div>
								<div id="tLink">
									<a id="twtrLink" href="https://twitter.com/home" onClick="return popup(this, 'notes')" class="share_link">
									   <div id="tIcon" class="share_icon"></div>
									   <span>Share Results on Twitter</span>
									</a>
								</div>
								<div id="eLink">
									<a id="genLinkEmail" class="share_link" title="Don't forget to personalize your results first!" href="mailto:example@domain.com" target="_blank">
										<div id="eIcon" class="share_icon"></div>
										<span>Email Results</span>
									</a>
								</div>
								<div id="lLink">
									<a id="genLink" href="/getResults.php" target="_blank" class="share_link" title="Don't forget to personalize your results first!">
										<div id="lIcon" class="share_icon"></div>
										<span>Link to Results:</span>
									</a>
									<input id="genLinkDisplay" title="Don't forget to personalize your results first!" value="" onClick="this.select();">
								</div>
<?php 
		if($user->isLoggedIn() && !inMaintenanceMode()){
?>
								<div>
									<a id="gotoTestStats" class="stoHoverLink" href="/user/<?php echo $userData->username;?>/test-stats#testDataGraph">
										<div class="graphIcon" title="Go to Typing Test Data Line Graph">
											<i class="fa fa-area-chart" aria-hidden="true"></i>
										</div>
										<span>Go to my test data</span>
									</a>
								</div>
<?php   }
?>
							</div>
						</div>
						<div id="resultsBotWrap">
							<div id="resultsBot">
								<div>
									<div class="rbColWrap">
										<h4>Fastest Chars</h4>
										<div id="fastLetters_Result" class="rb">
										</div>
									</div>
									<div class="rbColWrap">
										<h4>Slowest Chars</h4>
										<div id="slowLetters_Result" class="rb">
										</div>
									</div>
								</div>
								<div>
									<div class="rbColWrap">
										<h4>Most Missed</h4>
										<div id="LetMissed_Result" class="rb">
										</div>
									</div>
									<div class="rbColWrap">
										<h4>Most Inaccurate</h4>
										<div id="LetPercent_Result" class="rb">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> 
			
			<div id="rectAdWrap">
				<?php echo getAd("thirdBottom", $currPage, $debugMode, $user->isPremium());?>
			</div>
			
			<div id="infoOpContainer">
				<div id="textInfo">
					<div id="textTitleDiv"><span id="textTitleDisplay" class="textInfoDisplay"></span></div>
					<div id="textAuthorDiv">Author: <span id="textAuthorDisplay" class="textInfoDisplay"></span></div>
					<div id="textTypeDiv">Type: <span id="textTypeDisplay" class="textInfoDisplay"></span></div>
				</div>
				
				<div id="textContainer">
					<h3>Text Settings</h3>
					<div id="titleSelectDropDown"<?php if($userData->tt_text_type == "CUSTOM"){echo ' style="display: none; "';}?>>
						<div>Selection</div>
						<select id="titleTypeSelected" class="stoUI stoUIClickable" data-default="<?php echo $userData->tt_text_title;?>">
							<option value="0" selected="selected">Random</option>
						</select>
					</div>
						
					<div id="textSelectDropDown">
						<div>Type</div>
						<select id="textTypeSelected" class="stoUI stoUIClickable">
							<?php 
								if($keyboardArr[$userData->tt_keyboard_layout] == "NUMPAD")
									echo $ttdl_numpad;
								else
									echo $ttdl_normal; 
							?>
						</select>
					</div>
					
					<div id="hiddenCTC"<?php if($userData->tt_text_type != "CUSTOM"){echo ' style="display: none; "';}?>>
						<div id="customTextContainer">
							<span id="customTextLabel">Custom Text:</span>
							<textarea id="customTextInput"></textarea>
							<button id="CT_subBut" type="button" onclick="SubmitCustomText();">Submit</button>
							<button id="CT_clrBut" type="button" onclick="ClearCustomText();">Clear Text</button>
							<div id="savedTextContainer">
								<a id="CT_saveA" href="/getResults.php" target="_blank">Link to saved text:</a>
								<input id="CT_saveInput" title="Copy and paste into any browser" value="" onClick="this.select();">
							</div>
						</div>
					</div>
				</div>
				<div id="timeContainer">
					<div id="timeSelectDropDown">
					<div>Test Time</div>
					<select id="timeTypeSelected" class="stoUI stoUIClickable">
	<?php			if($debugMode){	?>
						<option value="3"<?php echo $testLength[7]; ?>>3 seconds</option>
	<?php			}?>
						
						<option value="30"<?php echo $testLength[0]; ?>>30 seconds</option>
						<option value="60"<?php echo $testLength[1]; ?>>1 minute</option>
						<option value="120"<?php echo $testLength[2]; ?>>2 minutes</option>
						<option value="180"<?php echo $testLength[3]; ?>>3 minutes</option>
						<option value="300"<?php echo $testLength[4]; ?>>5 minutes</option>
						<option value="600"<?php echo $testLength[5]; ?>>10 minutes</option>
						<option value="900"<?php echo $testLength[6]; ?>>15 minutes</option>
						<option value="1200"<?php echo $testLength[7]; ?>>20 minutes</option>
					</select>
				</div>
				</div>
				
				<div id="tcDiv" class="resultStatLeft stoUI stoUICheckbox" title="Colors typed text based on entry (green = correct, red = incorrect...)" >
					<label><input type="checkbox" id="tcCheckbox" value="tcCorrect"<?php echo $textColoringSetting; ?>> Color Highlighting</label>
				</div>
				<div id="psDiv" class="resultStatLeft stoUI stoUICheckbox" title="Auto-corrects misalignment with text (recommend leaving enabled)" >
					<label><input type="checkbox" id="psCheckbox" value="psCorrect"<?php echo $phaseShiftSetting; ?>> Phase Shift Correction</label>
				</div>
				<div id="dsDiv" class="resultStatLeft stoUI stoUICheckbox" title="Will add 2 spaces after every sentence (recommend leaving disabled)" >
					<label><input type="checkbox" id="dsCheckbox" value="dsCorrect"<?php echo $doubleSpacingSetting; ?>> Double spacing between sentences</label>
				</div>
				<span id="how_to"></span>
				<div id="keyLayoutDropDown">
					<span>Keyboard Layout (Qwerty, Dvorak, Colemak...)</span>
					<select id="keyLayoutSelected" class="resultStatLeft stoUI stoUIClickable" title="Changes keyboard layout (Most common layout is QWERTY)">
						<option value="QWERTY"<?php echo $keyboardType["QWERTY"]; ?>>Standard (QWERTY)</option>
						<option value="QWERTY_UK"<?php echo $keyboardType["QWERTY_UK"]; ?>>QWERTY - UK</option>
						<option value="DVORAK"<?php echo $keyboardType["DVORAK"]; ?>>Dvorak</option>
						<option value="COLEMAK"<?php echo $keyboardType["COLEMAK"]; ?>>Colemak</option>
						<option value="COLEMAK_UK"<?php echo $keyboardType["COLEMAK_UK"]; ?>>Colemak - UK</option>
						<option value="AZERTY"<?php echo $keyboardType["AZERTY"]; ?>>AZERTY</option>
						<option value="QWERTZ"<?php echo $keyboardType["QWERTZ"]; ?>>QWERTZ</option>
						<option value="QWERTZ_SF"<?php echo $keyboardType["QWERTZ_SF"]; ?>>QWERTZ - SwissFrench</option>
						<option value="NUMPAD"<?php echo $keyboardType["NUMPAD"]; ?>>Numpad (10-key)</option>
					</select>
				</div>
			</div>
		</div> <!-- #cc_offset (end) -->
        <div id="ttFeatures">

			<h2>How To</h2>
			<div class="grid grid-pad">
				<div class="col-1-2">
					<ul class="arrowList">
						<li>
							<h3>Start Test</h3>
							<span>
								<p>Click on the text box with the orange border; the timer starts as soon as you begin typing!</p>
								<p>Click 'Reset' or press <span class="boldFont">'Shift-Return'</span> to restart typing test.</p>
							</span>
						</li>
					</ul>
					<ul class="arrowList">
						<li>
							<h3>Customize Text</h3>
							<span>
								<p>Click the <span class="boldFont">'Text to Type'</span> dropdown menu located directly below the typing test input box.</p>
							</span>
						</li>
					</ul>
				</div>
				<div class="col-1-2">
					<ul class="arrowList">
						<li>
							<h3>Share Results</h3>
							<span>
								<p>Click on the <span class="boldFont">'Link to Results'</span> or <span class="boldFont">'Email Results'</span> links on the test results panel to share your results.</p>
							</span>
						</li>
					</ul>
					<ul class="arrowList">
						<li>
							<h3>Change Entry Mode</h3>
							<span>
								<p>Click the <span class="boldFont">'Switch-Up'</span> button to toggle between multi-line and scrolling single-line entry.</p>
							</span>
							<span id="feature_jump"></span>
						</li>
					</ul>
				</div>
			</div>
			
			<h2 id="featuresDest">Features</h2>
			<div class="grid grid-pad">
				<div class="col-1-2">
					<ul class="checkList">
						<li><h3>Restart Test Hotkey</h3>
						<p>Keep your hands on the keyboard - use keyboard shortcut "Shift-Return" to restart typing test.</p></li>
						<li><h3>Live Statistics</h3>
						<p>Check your typing speed and accuracy as it is updated and displayed every second while you type.</p></li>
						<li><h3>Personalize and Save Report</h3>
						<p>Report can be personalized with your name and your results can be saved and shared easily using the link generated at the end of every test.</p></li>
						<li><h3>Custom and interesting text to type</h3>
						<p>If you're going to type something, you might as well type something interesting and fun to type.</p>
						<p>Current typing text options include random common English words, random easy words, books, book summaries short stories, fables, sight words for kids, random interesting and funny facts, random wise proverbs, and even the ability to type whatever you want with the custom text option.</p>
						<p>Fables and random sight words are good choices for easy typing texts that work great for younger kids and beginners.</p>
						<p>Custom text can be saved and shared easily using the custom URL generated when new custom text is loaded.</p></li>
						<li><h3>Dvorak, Colemak, AZERTY, QWERTZ, and Numpad support</h3>
						<p>No more messing with the settings on your computer! Support for many popular keyboard layouts makes switching to your favorite as easy as selecting it from the keyboard layout dropdown menu.</p>
						<p>The following <strong>keyboard layouts</strong> are supported:</p>
						<ul class="insideList centerList">
							<li>QWERTY (US and UK)</li>
							<li>Colemak (US and UK)</li>
							<li>Dvorak</li>
							<li>AZERTY</li>
							<li>QWERTZ</li>
							<li>Numpad (10-key)</li>
						</ul>
						</li>
					</ul>
				</div>
				<div class="col-1-2">
					<ul class="checkList">
						<li><h3>Multiple entry modes</h3>
						<p>Two different entry modes keep the experience fresh and help develop different speed typing skills.</p></li>
						<li><h3>Text lookahead 100% of the time</h3>
						<p>At any point in time both entry modes provide, at a minimum, the next 2-3 words (21 characters to be precise) so you can always be looking ahead at the next words to type - a useful skill in learning to type faster.</p></li>
						<li><h3>Broad range of test round times</h3>
						<p>Why should you always have to take a 1 minute typing test?</p>
						<p>Speed Typing Online provides six different test round times: 30 seconds, 1 minute, 2 minute, 3 minute, 5 minute, and 10 minute typing tests.</p></li>
						<li><h3>Type what you're typing!</h3>
						<p>Tired of having to frantically check back and forth between the text to type and what you're typing?</p>
						<p>Both entry modes combine the two into one, so you are always looking at exactly what you are typing on screen as you do normally, making it easy to refine your natural typing skill.</p></li>
						<li><h3>Correct Words per minute (WPM) Calculation</h3>
						<p>Typing speed is calculated according to the most widely accepted method where a 'word' is assumed an average of 5 characters (spaces included) with every error (per minute) being a 1 WPM deduction. Nearly all other significant statistics are also reported, allowing for a more custom calculation as well.</p>
						<p>All performance calculations are also clearly explained (simply mouse-hover over the labels) making it easy to understand your results.</p></li>
					</ul>
				</div>
			</div>
        </div>
		<div id="ttPractice">
			<h2>Practice Every Day!</h2>
			<p>
				For the best results try practicing proper typing techniques a little every day.
			</p>
			<p>
				Remember to take breaks often though - its good for the body and for the learning.
			</p>
		</div>
    </div>

<script src="js/jquery.hotkeys.js" type="text/javascript"></script>
<script src="js/combined2.js?v=76" type="text/javascript"></script>
<script type="text/javascript">  
	var testGlobals = {userSettings : {} };
  
    // handles all popup window links (used for Social Media Sharing)
    function popup(mylink, windowname){
        if (! window.focus)return true;
        var href;
        if (typeof(mylink) == 'string')
            href=mylink;
        else
            href=mylink.href;
        window.open(href, windowname, 'width=626,height=436,scrollbars=yes');
        return false;
    };
</script>
    
<script type="text/javascript">
	//$.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
	<?php 
	// set NUMPAD text type as selected (if CUSTOM is not set) so it is default when keyboard is switched to Numpad
	if($textType["CUSTOM"] == '')
		$ttdl_numpad = 	"<option value=\"NUMPAD\" selected=\"selected\">Numpad (10-key)</option>" . 
				"<option value=\"CUSTOM\">Custom</option>";
	?>
	var isMobile = <?php echo ($isMobile) ? 'true;' : 'false;';?> //(/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
	var keyboardArr = <?php echo json_encode($keyboardArr);?>;
	var ttdl_normal = <?php echo json_encode($ttdl_normal); ?>;
	var ttdl_numpad = <?php echo json_encode($ttdl_numpad); ?>;
	var sa_id = <?php echo (Input::get('sa_id') == '') ? "null" : Input::get('sa_id'); ?>;
	
	var blockDivHeight = 160;
	var inlineDivHeight = 60;
	
    $(document).ready(function(){
		
		ToggleTextColoring();
		if(isMobile){
			
			var mobileTextAreaElem = $('#mobileTextArea');
			
			$('.mainDivInputs').focusin(function(){
				window.addEventListener("load", function() { window. scrollTo(0, 0); });
				/*document.addEventListener("touchmove", function(e){e.preventDefault()});*/
				var body = document.documentElement;
				if (body.requestFullscreen) {
				  body.requestFullscreen();
				} else if (body.webkitrequestFullscreen) {
				  body.webkitrequestFullscreen();
				} else if (body.mozrequestFullscreen) {
				  body.mozrequestFullscreen();
				} else if (body.msrequestFullscreen) {
				  body.msrequestFullscreen();
				}
				
				$('#mobileTextArea').focus();
			});
			
			mobileTextAreaElem.on("keydown", function(event){
				var myEvent = event;
				/*var currentText = mobileTextAreaElem.val();
				var currentCount = currentText.length;
				
				if(event.keyCode == 8){
					CreateTimer_block(event, 'timer', $('#timeTypeSelected').val());
					if(mobileTextCount > 0)
						mobileTextCount--;
				} 	*/	
			});
			
			mobileTextAreaElem.on("input", function(event){
				var currentText = mobileTextAreaElem.val();
				var currentCount = currentText.length;
				var textLengthDifference = currentText.length - mobileText.length;
				
				if(textLengthDifference > 0){
					for(var ii = 0; ii < textLengthDifference; ii++){
						if($('#blockDivContainer').is(":visible")){
							CreateTimer_block(currentText.substr(currentText.length - textLengthDifference + ii, 1), 'timer', $('#timeTypeSelected').val());
						} else {
							CreateTimer_line(currentText.substr(currentText.length - textLengthDifference + ii, 1), 'timer', $('#timeTypeSelected').val());
						}
					}
					mobileText = currentText;
				} else if(textLengthDifference < 0){
					textLengthDifference *= -1;
					for(var ii = 0; ii < textLengthDifference; ii++){
						if($('#blockDivContainer').is(":visible")){
							CreateTimer_block("backspace", 'timer', $('#timeTypeSelected').val());
						} else {
							CreateTimer_line("backspace", 'timer', $('#timeTypeSelected').val());
						}
					}
					mobileText = currentText;
				}
				
				
				/*while(mobileTextCount < currentCount){
					CreateTimer_block(currentText.substring(mobileTextCount,mobileTextCount+1), 'timer', $('#timeTypeSelected').val());
					mobileTextCount++;
				}*/
				
			});
	}
<?php
	
	if(isset($_GET["cText"])){
		$customText = $_GET["cText"];
		echo "$('#textTypeSelected').val(\"CUSTOM\");";
		echo "$('#customTextInput').val(\"" . addslashes(str_replace('</script>', '<\script>', $customText)) . "\");";
		echo "$('#hiddenCTC').show();";		
		echo "Reset($('#timeTypeSelected').val(),'timer');";
	}
?>

        $('#blockDivContainer').live("keydown", function(event){
            if($('#blockDivContainer').get()[0].innerHTML != ""){
                return CreateTimer_block(event, 'timer', $('#timeTypeSelected').val()/* Round Time */);
            }
        });

        $('#lineDivContainer').live("keydown", function(event){
             return CreateTimer_line(event, 'timer', $('#timeTypeSelected').val()/* Round Time */);
        });

        $("#switchModesBtn").click(function(){
            $(this).attr('disabled', 'true');
			var blockDivElem = $("#blockDivContainer");
			var lineDivElem = $("#lineDivContainer");
			var divGlowElem = $("#blockDivGlow")
			if(isMobile)
				var mobileTextAreaElem = $("#mobileTextArea")
			
            if (blockDivElem.is(":hidden")) {
                testGlobals["userSettings"]["input_mode"] = "BLOCK";
                lineDivElem.hide();
                divGlowElem.hide();
				$("#divCover").css({"height": blockDivHeight + "px","margin-top": "-1px"});
				divGlowElem.css({"height": blockDivHeight + 2 +"px", "top": "-2px"});
				if(isMobile){
					mobileTextAreaElem.css({"height":blockDivHeight + "px", "top": "0px"});
				}
                blockDivElem.show('fast', function(){
                    $("#switchModesBtn").removeAttr('disabled');
                    divGlowElem.fadeIn(1000);
                });
            } else {
                testGlobals["userSettings"]["input_mode"] = "INLINE";
                blockDivElem.hide();
                divGlowElem.hide();
				$("#divCover").css({"height": inlineDivHeight + "px","margin-top": "50px"});
				divGlowElem.css({"height": inlineDivHeight + "px", "top": "48px"});
				if(isMobile){
					mobileTextAreaElem.css({"height":inlineDivHeight + "px", "top": (160 / 2) - (inlineDivHeight / 2) + "px"});
				}
                lineDivElem.show('fast', function(){
                    $("#switchModesBtn").removeAttr('disabled');
                    divGlowElem.fadeIn(1000);
                });
            }
			Reset($('#timeTypeSelected').val(),'timer');
        });
		
		// call reset function every time text type drop down menu changes
        $("#titleTypeSelected").change(function () {
			var newVal = $('#titleTypeSelected').val();
			testGlobals["userSettings"]["tt_text_title"] = newVal;
            Reset($('#timeTypeSelected').val(),'timer');
        });
    
        // call reset function every time text type drop down menu changes
        $("#textTypeSelected").change(function () {
			var newVal = $('#textTypeSelected').val();
			var ctcElem = $('#hiddenCTC');
			var titleSelectDD = $('#titleSelectDropDown');
			
			testGlobals["userSettings"]["tt_text_type"] = newVal;
			
			// update selection when type is changed
			testGlobals["userSettings"]["tt_text_title"] = $('#titleTypeSelected').val();
            Reset($('#timeTypeSelected').val(),'timer');
            
            if(newVal.toUpperCase() == "CUSTOM"){
                ctcElem.show('fast', function() {ctcElem.css('overflow','visible')});
				titleSelectDD.hide();
            }
            else{
				titleSelectDD.show();
				ctcElem.hide();
            }
        });

        // change display every time text type drop down menu changes
        $("#timeTypeSelected").change(function () {
			var newVal = $('#timeTypeSelected').val();
			
			testGlobals["userSettings"]["tt_length"] = newVal;
            Reset(newVal,'timer');
        });
		
		$("#keyLayoutSelected").change(function(){
			var kls_elem = $('#keyLayoutSelected');
			var kls_elemVal = kls_elem.val();
			var tts_elem = $('#textTypeSelected');
			var tts_elemVal = kls_elem.val();
			testGlobals["userSettings"]["tt_keyboard_layout"] = keyboardArr.indexOf(kls_elemVal);
			
			if(kls_elemVal == "NUMPAD"){
				tts_elem.empty();
				tts_elem.append(ttdl_numpad);
				//$textType['NUMPAD'] = ' selected="selected"';
			} else {
				var f = tts_elem.val();
				tts_elem.empty();
				tts_elem.append(ttdl_normal);
				tts_elem.val(f);
			}
			Reset($('#timeTypeSelected').val(),'timer');
		});
		
		$("#dsCheckbox").change(function(){
			var newVal = ($('#dsCheckbox').attr("checked") == "checked") ? 1 : 0;
			testGlobals["userSettings"]["tt_double_spacing"] = newVal;
		});
		
		$("#tcCheckbox").change(function(){
			var newVal = ($('#tcCheckbox').attr("checked") == "checked") ? 1 : 0;
			testGlobals["userSettings"]["tt_color_text"] = newVal;
			ToggleTextColoring();
		});
		
		$("#psCheckbox").change(function(){
			var newVal = ($('#psCheckbox').attr("checked") == "checked") ? 1 : 0;
			testGlobals["userSettings"]["tt_phase_shift"] = newVal;
		});
		
		$("#customTextInput").keyup(function(){
			$("#savedTextContainer").hide();
		});
		
		// personalize test report with name
		$('#personName').focus(function(){
			if($('#personName').val() == "<Name>")
				$('#personName').val("");
		});
		
		$('#personName').blur(function(){
			if($('#personName').val() == "")
				$('#personName').val("<Name>");
		});
		
		$('#PN_Button').click(function(){
			var name = $('#personName').val();
			
			if(name != '' && name != "<Name>"){
				generateGenLink();
				$('#resultsTitle').html(name + "'s Test Results");
				
				document.getElementById('genLinkEmail').title = '';
				document.getElementById('genLink').title = '';
				document.getElementById('genLinkDisplay').title = '';				
			} else {
				$('#resultsTitle').html("Test Results");
			}
		});
		
		$('#infoOpContainer, #centerContent').on("mouseleave", function(){
			var colArr = [];
			var valArr = [];
			var doReset = false;
			
			$.each(testGlobals["userSettings"], function(key, value){
				colArr.push(key);
				valArr.push(value);
				delete testGlobals["userSettings"][key];
			});
			
			if(colArr.length > 0){
				$.post('setData.php', { type: "us", col: colArr, val: valArr},
					function(output){
				});
			}
		});
		
		//var resizeTimer;
		var screenWidth = $(window).width();
		$(window).resize(function(){
			var newScreenWidth = $(window).width();
			
			if( (screenWidth < shortWidthCutoff && newScreenWidth > shortWidthCutoff) ||
				(screenWidth > shortWidthCutoff && newScreenWidth < shortWidthCutoff) ){
				Reset($('#timeTypeSelected').val(),'timer');	
				screenWidth = newScreenWidth;
			}
			
			if( newScreenWidth < 661 ){
				blockDivHeight = 115;
			} else {
				blockDivHeight = 160;
			}
		});
        
        Reset($('#timeTypeSelected').val(),'timer');
		 
		 
		 		// ADD HOTKEY
		shortcut.add("Shift+return",function() {
			Reset($('#timeTypeSelected').val(),'timer');
			},{
			"type":"keyup",
			"propagate":false,
			"disable_in_input":false,
			"target":document
			});
    });
	
	
	
	function SubmitCustomText(){
		$("#savedTextContainer").show();
		Reset($('#timeTypeSelected').val(),'timer');
	}
	
	function ClearCustomText(){
		$("#savedTextContainer").hide();
		$('#customTextInput').val('');
	}
</script>
<?php 
include 'includes/overall/footer.php'; 
?>
