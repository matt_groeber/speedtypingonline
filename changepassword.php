<?php
$currPage = "changePassword";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

session_regenerate_id(true);  

$user = new User();
$accountInfoHTML = '';

if(!inMaintenanceMode()){
	// first examine to see if user is trying to recover their password
	if(($pwResetCode = Input::get('emc')) && ($pwResetEmail = Input::get('email'))){
		
		$accountInfoHTML = "<div><h4>For account with email = \"" . $pwResetEmail . "\"</h4></div>";
		$db = DB::getInstance();
		$pwResetUserID = $db->get('users', array('email', '=', $pwResetEmail))->first()->id;
		$pwResetRow = $db->get('pw_rec', array('user_id', '=', $pwResetUserID))->first();
		
		// see if link is allowable based on matching code and code newer than 1 hour
		if($pwResetRow->code === $pwResetCode){
			if(strtotime($pwResetRow->code_expiry) > strtotime("-60 minutes")){
				$recoverPasswordValid = true;
				$user = new User($pwResetUserID);
			} else {
				$recoverPasswordValid = false;
				$db->delete('pw_rec', array('user_id', '=', $pwResetUserID));
				Session::flash('errored', 'Your password reset link has expired', 'ERROR');
				Redirect::to('recover.php?mode=password');
			}
		} else {
			$recoverPasswordValid = false;
			Session::flash('errored', 'Your password reset link is invalid', 'ERROR');
			Redirect::to('recover.php?mode=password');
		}
		
	} else if(!$user->isLoggedIn()){
		Redirect::to('register.php');
	}
}

if(Input::exists()&& !inMaintenanceMode()){
	if(Token::check(Input::get('token'))){
		
		$validate = new Validate();
		
		$currentPasswordArray = array(
			'password_current' => array(
				'name' => 'Current password',
				'required' => true,
				'equalsTrue' => array(password_verify(Input::get('password_current'), $user->data()->password), 'You entered the wrong current password')
			)
		);
		
		$newPasswordArray = array(
				'password_new' => array(
				'name' => 'New password',
				'required' => true,
				'min' => 6
			),
			'password_new_again' => array(
				'name' => 'New password again',
				'required' => true,
				'matches' => 'password_new'
			)
		);
		
		if($recoverPasswordValid){
			$validationArray = $newPasswordArray;
		} else {
			$validationArray = array_merge($currentPasswordArray, $newPasswordArray);
		}
		
		$validation = $validate->check($_POST, $validationArray);
		
		if($validation->passed()){
			//$db->delete('pw_rec', array('user_id', '=', $pwResetUserID));

			$user->update(array(
				'password' => password_hash(Input::get('password_new'), PASSWORD_DEFAULT)
			), $user->data()->id);

			Session::flash('updated', 'Your password has been updated', 'INFO');
			if($recoverPasswordValid){
				Redirect::to('login.php');
			} else {
				Redirect::to('user/' . $user->data()->username . '/profile');
			}
		} else {
			$errorsToPrint = '<ol><li>' . implode('</li><li>', $validation->errors()) . '</li></ol>';
		}
	}
}
$htmlTitle = "Change Password";
$htmlDescription = "Change user password";
$cssFiles = "be";
$noWidgets = true;
$addContainer = true;
require_once 'includes/overall/header.php';

if(inMaintenanceMode()){
	echo '<h2>Site is currently under Maintenance</h2>';
	echo '<h4>User data is currently disabled as a major site-wide upgrade is currently underway.</h4>';
	echo '<h4>Please feel free to continue using the site as normal but unfortunately none of your progress will be saved.</h4>';
	echo '<h4>Sorry for any inconvenience and thank you for your patience!</h4>';
	exitPHPwithFooter();
}
?>

<h2>Change Your Password</h2>
<?php //echo $accountInfoHTML;?>
</div> <!-- <div id="heading"> -->


<div id="pageMain">
	<form class="longFormLabels" action="" method="post">
		<?php if(!$recoverPasswordValid)
			{ ?>
			<div class="field alignInput">
				<label for="password_current">Current password:</label>
				<input type="password" name="password_current" id="password_current">
			</div>
				<?php 
			} 
		?>
		<div class="field alignInput">
			<label for="password_new">New password:</label>
			<input type="password" name="password_new" id="password_new">
		</div>
		<div class="field alignInput">
			<label for="password_new_again">New password again:</label>
			<input type="password" name="password_new_again" id="password_new_again">
		</div>
		
		<div id="formErrors"><?php echo (isset($errorsToPrint)) ? $errorsToPrint : '';?></div>
			
		<input class="mediumBtn flatBlueBtn" type="submit" value="Change Password">
		<input type="hidden" name="token" value="<?php echo Token::generateToken(Config::get('session/token_name')); ?>">
	</form>
</div>
<?php
require_once 'includes/overall/footer.php';
?>