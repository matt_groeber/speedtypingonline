<?php 
$currPage = "aboutUs";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$htmlTitle = "About";
$htmlDescription = "About SpeedTypingOnline - a fun, useful, and easy to use typing site.";
$cssFiles = "ben";
$noWidgets = true;
$addContainer = true;
require_once 'includes/overall/header.php';
?>

		<h1>About SpeedTypingOnline</h1>
	</div>
	<div>
		<div id="headInfo">
		<p>SpeedTypingOnline emerged from the frustration of being unable to find a free online typing test
		containing all of the necessary features believed to be basic requirements for any typing test. 
		These features can broken down into three main categories: </p>
			<ul>
				<li>a natural and polished system for text entry</li>
				<li>correct and extensive performance statistics</li>
				<li>a fair amount of customization (ie. text to type, length of round)</li> 
			</ul>
		<p>The intent is that SpeedTypingOnline fulfills all three of these categories to the greatest degree
		possible. Additional features and extras will continue to be added in the near future so be sure to
		check back often! <br/> <br/>
		Feel free to <a href="mailto:speedtypingonline@gmail.com">contact us</a> if you would like a certain feature to be added or if you have a question or problem
		with anything. We hope you enjoy using this typing site!</p>
		</div>
	</div>
</div>

<?php 
include 'includes/overall/footer.php'; 
?>