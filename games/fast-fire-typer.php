<?php 
$currPage = "fastFireTyper";
require_once '../core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$htmlTitle = "Fast Fire Typer";
$htmlDescription = "Learn and improve your typing, spelling, and vocabulary skills while playing a fun and challenging typing game.";
$cssFiles = "behri";
$fontPacks = "abc";
$noWidgets = true;
$fontAwesomeCDN = true;
require_once '../includes/overall/header.php';


$db = DB::getInstance();
$hs = new HighScore();

$user = new User();
$loggedInID = ($user->isLoggedIn()) ? $user->data()->id : null;
$debugMode = $user->hasPermission('debug') || Config::get('constants/debugMode');

?>

<div id="fbLikeDiv">
	<div class="fb-like" data-href="<?php echo Config::get('constants/rootUrl');?>/games/fast-fire-typer.php" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true" data-font="arial"></div>
</div>

<?php
	echo getAd("lead_ATF", $currPage, $debugMode, $user->isPremium());
?>


<div id="mainPanel" class="grid grid-pad">
   <div class="col-1-6">
     <div id="validWordBank" class="module wordBank">
		
		<p>Valid Words</p>
		<div class="floatCenterWrap">
			<span id="WordCaptureBox" class="floatCenterObj"></span>
		</div>
		<br style="clear:both;"/>
     </div>
   </div>
   <div class="col-4-6">
     <div id="centerEntryModule" class="red module level_4">
		<div class="topLeftLink"><a href="#howTo"><i class="fa fa-question-circle" aria-hidden="true"></i></a></div>
		<h1>Fast Fire Typer</h1>
     		<br />
			<div id="wrapStats">
				<div id="multText">x</div>
				<div id="multBox">
					<div id="multBillboard" class="pulse_image"></div>
				</div>
				<div id="timerDiv">   
					<div id="timerText">Time</div>
					<div id="timer" class="statsText" style="background-color: transparent; ">01:00</div>
				</div>
				
				<div id="scoreDiv"> 
					<div id="scoreText">Score</div>
					<div id="wbkScore" class="statsText">0</div>
				</div>
				
				<div id="highScoreDiv"> 
					<div id="highScoreText">High Score</div>
					<div id="wbkHighScore" class="statsText">-</div>
					<span id="clearDataBtn" style="display:none;"><input class="stoUI stoUIClickable" type="button" value="Clear Data" onclick="cleanData('fft', 'Delete all fast fire typer scores');"></span>
				</div>
			</div>
			
			<span id="inputBox">
				<form>
				<div id="inputBoxes">
					<input type="text" id="inputHint" class="redGlow" autocomplete="off">
					<input type="text" id="gInput" class="color-block" autocomplete="off">
					<input type="button" name="ResetBtn" value="Reset" id="resetBtn" class="orangeButton" onclick="Reset(60,'timer',true)">
				</div>		
				</form>
				<span id="wordHint"></span>
			</span>
			
			<span id="slbLettersSelectDropDown">
				<span class="boldFont">
					<span>Starting Letters:</span>
				</span>
				<select id="slbLettersSelect">
					<option value="RANDOM">Random</option>
					<option value="0">abs</option>
<option value="1">acc</option> <option value="2">act</option> <option value="3">adv</option> <option value="4">aer</option> <option value="5">aff</option>
<option value="6">air</option> <option value="7">ali</option> <option value="8">all</option> <option value="9">amb</option> <option value="10">amp</option>
<option value="11">ana</option> <option value="12">ang</option> <option value="13">ani</option> <option value="14">ann</option> <option value="15">ant</option>
<option value="16">apo</option> <option value="17">app</option> <option value="18">arc</option> <option value="19">art</option> <option value="20">ass</option>
<option value="21">ast</option> <option value="22">att</option> <option value="23">aut</option> <option value="24">bac</option> <option value="25">bal</option>
<option value="26">ban</option> <option value="27">bar</option> <option value="28">bas</option> <option value="29">bat</option> <option value="30">bea</option>
<option value="31">bec</option> <option value="32">bed</option> <option value="33">beg</option> <option value="34">bel</option> <option value="35">ben</option>
<option value="36">bes</option> <option value="37">bil</option> <option value="38">bio</option> <option value="39">bir</option> <option value="40">bla</option>
<option value="41">ble</option> <option value="42">blo</option> <option value="43">blu</option> <option value="44">bon</option> <option value="45">boo</option>
<option value="46">bor</option> <option value="47">bou</option> <option value="48">bra</option> <option value="49">bre</option> <option value="50">bri</option>
<option value="51">bro</option> <option value="52">bru</option> <option value="53">bul</option> <option value="54">bur</option> <option value="55">bus</option>
<option value="56">but</option> <option value="57">cab</option> <option value="58">cal</option> <option value="59">cam</option> <option value="60">can</option>
<option value="61">cap</option> <option value="62">car</option> <option value="63">cas</option> <option value="64">cat</option> <option value="65">cel</option>
<option value="66">cen</option> <option value="67">cer</option> <option value="68">cha</option> <option value="69">che</option> <option value="70">chi</option>
<option value="71">cho</option> <option value="72">chr</option> <option value="73">chu</option> <option value="74">cir</option> <option value="75">cla</option>
<option value="76">cle</option> <option value="77">cli</option> <option value="78">clo</option> <option value="79">coa</option> <option value="80">coc</option>
<option value="81">cod</option> <option value="82">coe</option> <option value="83">col</option> <option value="84">com</option> <option value="85">con</option>
<option value="86">coo</option> <option value="87">cop</option> <option value="88">cor</option> <option value="89">cos</option> <option value="90">cou</option>
<option value="91">cra</option> <option value="92">cre</option> <option value="93">cri</option> <option value="94">cro</option> <option value="95">cru</option>
<option value="96">cry</option> <option value="97">cul</option> <option value="98">cur</option> <option value="99">cut</option> <option value="100">cyc</option>
<option value="101">dea</option> <option value="102">deb</option> <option value="103">dec</option> <option value="104">def</option> <option value="105">del</option>
<option value="106">dem</option> <option value="107">den</option> <option value="108">dep</option> <option value="109">der</option> <option value="110">des</option>
<option value="111">det</option> <option value="112">dev</option> <option value="113">dia</option> <option value="114">dic</option> <option value="115">dip</option>
<option value="116">dis</option> <option value="117">div</option> <option value="118">dog</option> <option value="119">dow</option> <option value="120">dra</option>
<option value="121">dre</option> <option value="122">dri</option> <option value="123">dro</option> <option value="124">ear</option> <option value="125">eff</option>
<option value="126">ele</option> <option value="127">emb</option> <option value="128">emp</option> <option value="129">enc</option> <option value="130">end</option>
<option value="131">eng</option> <option value="132">ens</option> <option value="133">ent</option> <option value="134">epi</option> <option value="135">equ</option>
<option value="136">esc</option> <option value="137">est</option> <option value="138">eth</option> <option value="139">exc</option> <option value="140">exe</option>
<option value="141">exo</option> <option value="142">exp</option> <option value="143">ext</option> <option value="144">fac</option> <option value="145">fan</option>
<option value="146">far</option> <option value="147">fat</option> <option value="148">fel</option> <option value="149">fer</option> <option value="150">fil</option>
<option value="151">fin</option> <option value="152">fir</option> <option value="153">fis</option> <option value="154">fla</option> <option value="155">fle</option>
<option value="156">fli</option> <option value="157">flo</option> <option value="158">flu</option> <option value="159">foo</option> <option value="160">for</option>
<option value="161">fra</option> <option value="162">fre</option> <option value="163">fri</option> <option value="164">fro</option> <option value="165">fun</option>
<option value="166">fur</option> <option value="167">gal</option> <option value="168">gam</option> <option value="169">gar</option> <option value="170">gas</option>
<option value="171">gen</option> <option value="172">geo</option> <option value="173">gla</option> <option value="174">glo</option> <option value="175">gra</option>
<option value="176">gre</option> <option value="177">gri</option> <option value="178">gro</option> <option value="179">gru</option> <option value="180">hal</option>
<option value="181">han</option> <option value="182">har</option> <option value="183">hea</option> <option value="184">hel</option> <option value="185">hem</option>
<option value="186">her</option> <option value="187">het</option> <option value="188">his</option> <option value="189">hol</option> <option value="190">hom</option>
<option value="191">hon</option> <option value="192">hor</option> <option value="193">hou</option> <option value="194">hum</option> <option value="195">hyd</option>
<option value="196">hyp</option> <option value="197">ill</option> <option value="198">imm</option> <option value="199">imp</option> <option value="200">ina</option>
<option value="201">inc</option> <option value="202">ind</option> <option value="203">ine</option> <option value="204">inf</option> <option value="205">inh</option>
<option value="206">ins</option> <option value="207">int</option> <option value="208">inv</option> <option value="209">irr</option> <option value="210">iso</option>
<option value="211">kin</option> <option value="212">lac</option> <option value="213">lam</option> <option value="214">lan</option> <option value="215">lar</option>
<option value="216">lat</option> <option value="217">lea</option> <option value="218">leg</option> <option value="219">lib</option> <option value="220">lig</option>
<option value="221">lim</option> <option value="222">lin</option> <option value="223">lit</option> <option value="224">loc</option> <option value="225">log</option>
<option value="226">mac</option> <option value="227">mag</option> <option value="228">mai</option> <option value="229">mal</option> <option value="230">man</option>
<option value="231">mar</option> <option value="232">mas</option> <option value="233">mat</option> <option value="234">mea</option> <option value="235">med</option>
<option value="236">meg</option> <option value="237">mel</option> <option value="238">men</option> <option value="239">mer</option> <option value="240">mes</option>
<option value="241">met</option> <option value="242">mic</option> <option value="243">mid</option> <option value="244">mil</option> <option value="245">min</option>
<option value="246">mis</option> <option value="247">mod</option> <option value="248">mon</option> <option value="249">moo</option> <option value="250">mor</option>
<option value="251">mot</option> <option value="252">mou</option> <option value="253">mul</option> <option value="254">mus</option> <option value="255">mut</option>
<option value="256">nat</option> <option value="257">neu</option> <option value="258">non</option> <option value="259">obs</option> <option value="260">off</option>
<option value="261">ost</option> <option value="262">out</option> <option value="263">ove</option> <option value="264">pac</option> <option value="265">pal</option>
<option value="266">pan</option> <option value="267">pap</option> <option value="268">par</option> <option value="269">pas</option> <option value="270">pat</option>
<option value="271">pea</option> <option value="272">ped</option> <option value="273">pen</option> <option value="274">per</option> <option value="275">pet</option>
<option value="276">pha</option> <option value="277">phe</option> <option value="278">pho</option> <option value="279">phy</option> <option value="280">pic</option>
<option value="281">pil</option> <option value="282">pin</option> <option value="283">pla</option> <option value="284">ple</option> <option value="285">plu</option>
<option value="286">pol</option> <option value="287">por</option> <option value="288">pos</option> <option value="289">pot</option> <option value="290">pra</option>
<option value="291">pre</option> <option value="292">pri</option> <option value="293">pro</option> <option value="294">psy</option> <option value="295">pul</option>
<option value="296">pun</option> <option value="297">pur</option> <option value="298">pyr</option> <option value="299">qua</option> <option value="300">que</option>
<option value="301">qui</option> <option value="302">rac</option> <option value="303">rad</option> <option value="304">ram</option> <option value="305">ran</option>
<option value="306">rat</option> <option value="307">rea</option> <option value="308">reb</option> <option value="309">rec</option> <option value="310">red</option>
<option value="311">ree</option> <option value="312">ref</option> <option value="313">reg</option> <option value="314">reh</option> <option value="315">rei</option>
<option value="316">rel</option> <option value="317">rem</option> <option value="318">ren</option> <option value="319">rep</option> <option value="320">res</option>
<option value="321">ret</option> <option value="322">rev</option> <option value="323">rou</option> <option value="324">sac</option> <option value="325">sal</option>
<option value="326">san</option> <option value="327">sap</option> <option value="328">sar</option> <option value="329">sat</option> <option value="330">sca</option>
<option value="331">sch</option> <option value="332">sco</option> <option value="333">scr</option> <option value="334">scu</option> <option value="335">sea</option>
<option value="336">sec</option> <option value="337">sel</option> <option value="338">sem</option> <option value="339">sen</option> <option value="340">ser</option>
<option value="341">sha</option> <option value="342">she</option> <option value="343">shi</option> <option value="344">sho</option> <option value="345">shr</option>
<option value="346">sig</option> <option value="347">sil</option> <option value="348">sim</option> <option value="349">sin</option> <option value="350">ski</option>
<option value="351">sla</option> <option value="352">sle</option> <option value="353">sli</option> <option value="354">slo</option> <option value="355">slu</option>
<option value="356">sna</option> <option value="357">sni</option> <option value="358">sno</option> <option value="359">soc</option> <option value="360">sol</option>
<option value="361">sor</option> <option value="362">sou</option> <option value="363">spa</option> <option value="364">spe</option> <option value="365">spi</option>
<option value="366">spl</option> <option value="367">spo</option> <option value="368">spr</option> <option value="369">squ</option> <option value="370">sta</option>
<option value="371">ste</option> <option value="372">sti</option> <option value="373">sto</option> <option value="374">str</option> <option value="375">stu</option>
<option value="376">sub</option> <option value="377">suc</option> <option value="378">sul</option> <option value="379">sun</option> <option value="380">sup</option>
<option value="381">sur</option> <option value="382">swa</option> <option value="383">swe</option> <option value="384">swi</option> <option value="385">sym</option>
<option value="386">syn</option> <option value="387">tab</option> <option value="388">tac</option> <option value="389">tal</option> <option value="390">tan</option>
<option value="391">tar</option> <option value="392">tea</option> <option value="393">tel</option> <option value="394">ten</option> <option value="395">ter</option>
<option value="396">tet</option> <option value="397">the</option> <option value="398">thi</option> <option value="399">thr</option> <option value="400">tim</option>
<option value="401">tin</option> <option value="402">tit</option> <option value="403">top</option> <option value="404">tor</option> <option value="405">tou</option>
<option value="406">tra</option> <option value="407">tre</option> <option value="408">tri</option> <option value="409">tro</option> <option value="410">tru</option>
<option value="411">tur</option> <option value="412">twi</option> <option value="413">ult</option> <option value="414">una</option> <option value="415">unb</option>
<option value="416">unc</option> <option value="417">und</option> <option value="418">une</option> <option value="419">unf</option> <option value="420">unh</option>
<option value="421">uni</option> <option value="422">unl</option> <option value="423">unm</option> <option value="424">unp</option> <option value="425">unr</option>
<option value="426">uns</option> <option value="427">unt</option> <option value="428">unw</option> <option value="429">val</option> <option value="430">var</option>
<option value="431">ven</option> <option value="432">ver</option> <option value="433">vic</option> <option value="434">vis</option> <option value="435">vol</option>
<option value="436">war</option> <option value="437">was</option> <option value="438">wat</option> <option value="439">wea</option> <option value="440">whe</option>
<option value="441">whi</option> <option value="442">wil</option> <option value="443">win</option> <option value="444">wit</option> <option value="445">woo</option>
<option value="446">wor</option>
				</select>
			</span>
			
			<div id="diffSelectContainer">
					<div id="diffSelectDropDown" title='Enabling word hints comes with word score penalty.'>
						<input type="checkbox" id="wordHintCB" value="wordHintCB"><label for="wordHintCB">Show Word Hint(s)</label>
					</div>
			</div>
			<div id="xStatsToggleBut">
				<span id="moreArrow"></span>
				<span id="lessArrow"></span>
				<span id="moreLessStats">More</span>
			</div>
				<div id="extraStatsHidden">
					<div id="wrapExtraStats">
						<div id="wordValueDiv">                
							<div id="wordValueLabel" >Word Value</div>
							<div id="wordValue" class="statsText">0</div>
						</div>						
						<div id="wordScoreDiv"> 
							<div id="wordScoreLabel">Word Score</div>
							<div id="wordScore" class="statsText">0</div>
						</div>
						<div id="numWordsDiv"> 
							<div id="numWordsLabel">Num Words</div>
							<div id="numWords" class="statsText">0</div>
						</div>
						<div id="typingSpeedDiv">   
							<div id="typingSpeedLabel">Typing Speed<br /> (WPM)</div>
							<div id="typingSpeed" class="statsText">0</div>
						</div>
						<div id="aveTypingSpeedDiv">   
							<div id="aveTypingSpeedLabel">Average Typing<br />Speed (WPM)</div>
							<div id="aveTypingSpeed" class="statsText">0</div>
						</div>
					</div>
				</div>
     </div>
   </div>
   <div class="col-1-6">
     <div id="invalidWordBank" class="module wordBank">
		<p>Invalid Entries</p>
		<div class="floatCenterWrap">
			<span id="badWordCaptureBox" class="floatCenterObj"></span>
		</div>
		<br style="clear:both;"/>
     </div>
   </div>
</div>

<?php
	echo getAd("lead_BTF", $currPage, $debugMode, $user->isPremium());
?>

<a id="howToPlay"></a>

<div id="highScoreContainer" class="grid grid-pad">
	<div class="col-2-5 highScores" style="height: auto;">
		<div class="module instructWrap">
			<div id="phsContainer" class="hsBorderBottom" style="height: auto;">
				<h3>Your Best Score for '<span class="SLB_letters">?</span>'</h3>
				<div id="personalHighScore"></div>
			</div>
			<div style="height: auto;">
				<h3>Your Top Scores</h3>
				<div id="personalHighScores"></div>
			</div>
		</div>
		<?php
			echo getAd("largeRect", $currPage, $debugMode, $user->isPremium());
		?>
	</div>
	<div class="col-3-5 highScores" style="height: auto;">
		<div class="module instructWrap">
			<div class="hsBorderBottom" style="height: auto;">
				<h3>Recent High Scores for '<span class="SLB_letters">?</span>'</h3>
				<div id="recentCurrentHighScores"></div>
			</div>
			<div style="height: auto;">
				<h3>Recent Overall High Scores</h3>
				<div id="recentHighScores"></div>
			</div>
		</div>

	</div>
</div>
<div id="howTo"></div>
<div class="stoFancyList">
	<h2>How To</h2>
	<div class="grid grid-pad">
		<div class="col-1-2">
			<ul class="arrowList">
				<li>
					<h3>Start Game</h3>
					<span>
						<p>Click on the text box with the orange border; the timer starts as soon as you begin typing!</p>
						<p>Click 'Reset' or press <span class="boldFont">'Shift-Return'</span> to restart the game.</p>
					</span>
				</li>
				<li>
					<h3>Play</h3>
					<span class="insideList">
						<ol>
							<li>All words must start with the three "starting letters".</li>
							<li>Hit "Enter" or the space key to submit each word.</li>
							<li>Type as many correct words as you can. Spelling and typing speed counts!</li>
						</ol>
					</span>
				</li>
				<li>
					<h3>Change Starting Letters</h3>
					<span>
						<p>Click the <span class="boldFont">'Starting Letters'</span> dropdown menu located on the game panel.</p>
					</span>
				</li>
			</ul>
		</div>
		<div class="col-1-2">
			<ul class="arrowList">
				<li>
					<h3>Play Well</h3>
					<span>
						<p>Start with shorter words to increase the multiplier quickly, then switch to longer words for more points once the multiplier is high.</p>
						<p>Add endings to base words: -s, -er, -ers, -ed, -ing, -ness, -nesses, -ize, -ization, etc.</p>
						<p>Enable word hints to help think of words (for a small point penalty).</p>
					</span>
				</li>
				<li>
					<h3>Score Higher</h3>
					<span>
						<p><span class="boldFont">Type Fast</span> - Typing speed affects scoring.</p>
						<p><span class="boldFont">Longer Words</span> - More points are awarded for longer words.</p>
						<p><span class="boldFont">Be Accurate</span> - Increase the multiplier for every correct word (Max of x5). One mistake resets the multiplier back to 0.</p>
					</span>
				</li>
			</ul>
		</div>
	</div>
	
	<div><h2 id="featuresDest">Game Features</h2></div>
	<div class="grid grid-pad">
		<div class="col-1-2">
			<ul class="checkList">
				<li><h3>Restart Game Hotkey</h3>
				<p>Keep your hands on the keyboard - use keyboard shortcut <span class="boldFont">"Shift-Return"</span> to restart the game.</p></li>
				<li><h3>Live Statistics</h3>
				<p>Shows your typing speed as you play.</p></li>
				<li><h3>Vocabulary Skill Building</h3>
				<p>Gameplay depends on the player generating as many words as they can, encouraging a growing knowledge of words and spelling skills.</p></li>
			</ul>
		</div>
		<div class="col-1-2">
			<ul class="checkList">
				<li><h3>Typing Practice</h3>
				<p>Points are awarded based on performance in both typing speed and accuracy, encouraging practice in fundamental typing skills.</p></li>
				<li><h3>High Scores</h3>
				<p>Compete against yourself and other players with a high score board!</p>
				<p>Scores only last for 12 hours to keep the competition fresh and to discourage cheating.</p></li>
			</ul>
		</div>
	</div>




<div id="dialog-confirm" title="Permanently delete your fast fire typer scores?" style="display: none;">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Your high scores for the fast fire typer game will be permanently deleted and cannot be recovered.</p>
	<p>Are you sure?</p>
</div>
<div id="fb-root"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="../js/jquery.hotkeys.js" type="text/javascript"></script>
<script src="../js/wbk.js?v=9" type="text/javascript"></script>
<script src="../js/general.js" type="text/javascript"></script>
<script type="text/javascript">
$(function() {
	 
	 $('#slbLettersSelect').selectmenu({
		height:150,
		change: function(event, data){
			getStartingLetters(data.item.value);
			UpdateWordHint();
		}
	});
	
	$('#slbLettersSelect-button').change(function(){
		if($(this).html().toUpperCase() == "RANDOM"){
			$(this).html(startingLetters);
			UpdateWordHint();
		}
	});
	
	$('#slbLettersSelect-menu').on("click", function(){
		var elem = $('#slbLettersSelect-button .ui-selectmenu-text');
		if(elem.html().toUpperCase() == "RANDOM"){
			getStartingLetters("RANDOM");
			elem.html(startingLetters);
			UpdateWordHint();
		}
	});
	
	$( "#wordHintCB" ).button();
	
	// ADD HOTKEY
	shortcut.add("Shift+return",function() {
		Reset(60, 'timer', true);
		},{
		"type":"keyup",
		"propagate":false,
		"disable_in_input":false,
		"target":document
		});
	
	Reset(60,'timer', true, function(){
		multiplier = 3;
		increaseMultiplier(false);
		//animateMultiplier(5,{"multChanged" : true})
		});
	});

</script>
<?php 
include '../includes/overall/footer.php'; 
?>