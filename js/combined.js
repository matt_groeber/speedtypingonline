var Timer = null;
var TotalSeconds;
var RoundTime = 0;
var TimeWasReset = true;
var localCursor = 0;
var cursor = 0;
var lettersTypedCorrectly = 0;
var lettersTypedIncorrectly = 0;
var badEntriesDeleted = 0;
var totalEntriesTyped = 0;
var wordsTyped = 0;
var blockDiv = null;
var lineDiv = null;
var textString = "";
var deletes = 0;
var lineDivText = null;
var lineNum = 0;
var blockDivText = ["", "", "", ""];
var cutoff = 0;
var phaseShift = 0;
var minCursorReplaceStart = 0;
var timeoutStart = 0;
var cleanTextString;
var letterMistakes = {};
var letterPercentMisses = {};
var letterCounts = {};

var TestInfo = {};

Date.now = Date.now || function() { return +new Date; }; 	// support for Date.now in IE8

function InitializeTimer_block(Time, TimerID) {
    cursor = 24;        //first letter has "<span class="plainText">" before it
    localCursor = 24;
    cutoff = 0;
    lineNum = 0;
    blockDivText[0] = "";
    blockDivText[1] = "";
    blockDivText[2] = "";
    blockDivText[3] = "";
    Timer = document.getElementById(TimerID);
    TotalSeconds = Time;
    
    GetText();
    
    var tempStr = "";
    var tempBlockDivStr = ["", "", "", ""];
    var localCutoff = 0;
    minCursorReplaceStart = 0;
    
    for (var i=0; i < 4; i++){
        tempStr = textString.substring(cutoff, (42*(i+1)) - ((42*i) - cutoff));   // get next 42 characters from text string
        localCutoff = tempStr.search(/(\r\n|\n|\r)/);
        if (localCutoff < 1){      // if next 42 characters do not contain a carriage return (\n) (or solely a newline)
            
        
            localCutoff = tempStr.lastIndexOf(" ") + 1;                 // find last space in 42 chars

            if(localCutoff == 0){
                localCutoff = 42;   // if no space found within next 42 chars, cut word at full block line length
            }

            for(j=0; j<localCutoff; j++){

                blockDivText[i] += '<span class="plainText">' + tempStr.substring(j, j+1) + '</span>';
            }
            //blockDivText[i] = tempStr2.substring(0,localCutoff);     USE WITHOUT SECOND INSIDE FOR LOOP
            cutoff += localCutoff;
        }
        else{       // next 42 characters contained a carriage return
           // textString.replace(/(\r\n|\n|\r)/,"");    // remove the newline character we found
            //tempStr = tempStr.replace(/(\r\n|\n|\r|\s\r\n|\s\n|\s\r)/," ");    // remove the newline character we found                // ********TODO - use knowledge gained to fix spaces next to \n
            var tempStr2 = tempStr.substring(0, localCutoff+1);
            tempStr2 = tempStr2.replace(/(\r\n|\n|\r|\s\r\n|\s\n|\s\r)/," ");
            /*if(tempStr2.substring(localCutoff-1,localCutoff) != " "){
                var addedSpaceFlag = true
                tempStr2 += ' '; 
                localCutoff += 1;
            }*/
            
            
            for(var j=0; j<tempStr2.length; j++){

                blockDivText[i] += '<span class="plainText">' + tempStr2.substring(j, j+1) + '</span>';
            }
            /*if(!addedSpaceFlag){
                localCutoff += 1;
            }*/
            cutoff += localCutoff + 1;
            
        }
        if(i <= 0){ minCursorReplaceStart += localCutoff + 2;}      // minCursorReplaceStart is the length of the very first two lines...
        
    }
    // place cursor on first letter
    blockDivText[0] = blockDivText[0].replace(/plainText/i,"nxtLetter");
    tempBlockDivStr[0] = blockDivText[0];
    tempBlockDivStr[1] = blockDivText[1];
    tempBlockDivStr[2] = blockDivText[2];
    tempBlockDivStr[3] = blockDivText[3];
    
    cleanTextString = textString.replace(/\s\n|\n\s|\n/gm," ");    // remove all newline characters leaving spaces if needed
    tempStr = "";   // initialize for line div
    var spaceFound = textString.indexOf(" ", 141);
    
    tempStr += '<span class="nxtLetter">' + cleanTextString.substring(0, 1) + '</span>';
    
    for(i=1; i<=spaceFound; i++){
        
        tempStr += '<span class="plainText">' + cleanTextString.substring(i, i+1) + '</span>';
        
    }
                    
    lineDivText = '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
                        + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
                        + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
                        + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
                        + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
                        + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
                        + '<span class="hiddenTxt"> </span>' + '<span class="hiddenTxt"> </span>'
                        + tempStr.substring(0,(32*22));
    
    
    blockDiv = $('#blockDivContainer').get();
    blockDiv = blockDiv[0];
    
    lineDiv = $('#lineDivContainer').get();
    lineDiv = lineDiv[0];
    
    blockDiv.innerHTML = formatForInnerHTML_block(tempBlockDivStr);
    lineDiv.innerHTML = formatForInnerHTML(lineDivText);
            

        setupFocus();
        
        
    //Target IE6 and below
    if ($.browser.msie && $.browser.version <= 6 ) {
        $("div.blockLines").css("font-size", "25px" );
        $("#line_input").css("font-size", "25px" );
    }
    
}

function CreateTimer_block(event, TimerID, Time) {

    var functionKeyPressed = true;     // used to flag when function key (delete, shift...) is pressed
    var tempBlockDivStr = new Array("","","","");
    var NUM_LINES = 4;
    var MAX_CHARS_IN_LINE = 42;

    
    if (TotalSeconds==undefined || TimeWasReset) {
        cursor = 24;        //first letter has "<span class="plainText">" before it
        localCursor = 24;
        TimeWasReset = false;
        RoundTime = Time;

        Timer = document.getElementById(TimerID);
        TotalSeconds = Time;

        UpdateTimer()
		timeoutStart = (new Date).getTime();
        window.setTimeout("Tick()", 1000);
    }
    
    if (TimeWasReset){
        TotalSeconds = Time;
        TimeWasReset = false;   //reset TimeWasReset flag        
    }
    
    // This makes the box disabled when time is up
    if (TotalSeconds == 0){
        return false;
    }
    
    // Get text in RTE box
    var newRTEText = "";
    var RTELetter = blockDivText[lineNum].substring(localCursor,(localCursor+1));
	
	// add letter to letter bank
	if(RTELetter in letterCounts){
		letterCounts[RTELetter] = letterCounts[RTELetter] + 1;
		if(RTELetter in letterMistakes){
			letterPercentMisses[RTELetter] = Math.round((letterMistakes[RTELetter] / letterCounts[RTELetter]) * 100);	// gives number as a percent with 1 decimal place
		}
	} else {
		letterCounts[RTELetter] = 1;
	}
	
	
	
    /*
    var keyCode_entered = event.keyCode
    
    if($('#keyLayoutSelected').val().toUpperCase() == "DVORAK"){
        var key_entered = d_keycode.d_getValueByEvent(event);
    }
	else if($('#keyLayoutSelected').val().toUpperCase() == "QWERTY_UK"){
        var key_entered = UK_QWERTY_keycode.UK_QWERTY_getValueByEvent(event);
    }
	else if($('#keyLayoutSelected').val().toUpperCase() == "COLEMAK"){
        var key_entered = cole_keycode.cole_getValueByEvent(event);
    }
    else{       // default to QWERTY
        var key_entered = keycode.getValueByEvent(event);
    }*/
	
	
	var key_entered = event;
    
    

            // event.keyCode: 97-122 = a-z, 65-90 = A-Z, 8 = backspace, 32 = space
    if( key_entered == RTELetter){  /* characters match */
        functionKeyPressed = false;
        
        if(RTELetter == " "){
            newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "goodSpace" 
                            + blockDivText[lineNum].substring(localCursor-2);   
            wordsTyped++;
        }
        else{    
            newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "goodEntry" 
                        + blockDivText[lineNum].substring(localCursor-2);
        }
        cursor += 32;
        localCursor += 32;
        lettersTypedCorrectly += 1;
        totalEntriesTyped += 1;
        
        if(phaseShift != 0){
            phaseShift = 0;     // correct entry resets any possible phase shift
        }
        
    }      
    else if((keyCode_entered==32)||(keyCode_entered==42)||(keyCode_entered==43)||(keyCode_entered==45)||
            ((keyCode_entered >= 47  ) && ( keyCode_entered <= 59))||
            ((keyCode_entered >= 61  ) && ( keyCode_entered <= 90))||
            ((keyCode_entered >= 96  ) && ( keyCode_entered <= 111))||
            ((keyCode_entered >= 186  ) && ( keyCode_entered <= 223))){
        functionKeyPressed = false;
        
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION START >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // // -------------------------------------------------------------------------------------
        // **FIRST** see if we are looking for a phase shift
        if(phaseShift != 0){
            if(phaseShift == 1){
                if(key_entered == blockDivText[lineNum].substring(localCursor+32, localCursor+33)){
                    //we have a phase shift to the right
                    var shiftRight = true;
                }
            }
            else{   //phase shift = -1
                if(key_entered == blockDivText[lineNum].substring(localCursor-32, localCursor-31)){
                    var shiftLeft = true;
                }
            }
            phaseShift = 0;     // phase shift has been corrected and realigned
            
        }
        // **THEN** see if we should look for a phase shift to the right (positive time)
        else if(key_entered == blockDivText[lineNum].substring(localCursor+32, localCursor+33)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = 1;     //only set the flag if the user wants to do phase shift correction
            }
        }
        // see if we should look for a phase shift to the left (negative time)
        else if(key_entered == blockDivText[lineNum].substring(localCursor-32, localCursor-31)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = -1;     //only set the flag if the user wants to do phase shift correction
            }
        }
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION END >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // -------------------------------------------------------------------------------------
            
        if(shiftRight){
            newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "phaseRght" 
                         + blockDivText[lineNum].substring(localCursor-2,localCursor+21) + "goodEntry"
                         + blockDivText[lineNum].substring(localCursor+30);
            cursor +=32;
            localCursor +=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   Since we skipped a letter do not increment totalLettersTyped
            shiftRight = false;  // phase shift should have been successfully applied by now
        }
        else if(shiftLeft){
            newRTEText = blockDivText[lineNum].substring(0,localCursor-43) + "phaseLeft" 
                         + blockDivText[lineNum].substring(localCursor-34);
            cursor -=32;
            localCursor -=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   Removed because it was counting twice
            shiftLeft = false;  // phase shift should have been successfully applied by now
        }
        // we do not have a case of phase shift, so continue with bad input
        else if(blockDivText[lineNum].substring(localCursor, localCursor+1) == " "){   /* this was RTELetter == " " */
            newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "bad_Space" 
                         + blockDivText[lineNum].substring(localCursor-2);
            wordsTyped++;
        }
        else{                     
            newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "bad_Entry" 
                         + blockDivText[lineNum].substring(localCursor-2);
			// count bad letter
			if(RTELetter in letterMistakes){
				letterMistakes[RTELetter] = letterMistakes[RTELetter] + 1;
			} else {
				letterMistakes[RTELetter] = 1;
			}
			letterPercentMisses[RTELetter] = Math.round((letterMistakes[RTELetter] / letterCounts[RTELetter]) * 100);	// gives number as a percent with 1 decimal place
        }
        
        cursor += 32;
        localCursor += 32;
        lettersTypedIncorrectly += 1;
        totalEntriesTyped += 1;
    }
    else if( key_entered == "backspace" ){      /*no match and it is a backspace, and its not beginning of text*/ 
        // backstep first
        cursor -= 32;
        functionKeyPressed = false;

        if(localCursor == 24){
            var newLineNum = (lineNum + (NUM_LINES - 1)) % NUM_LINES;
            var newLocalCursor = blockDivText[newLineNum].length - 8;      // wrap localcursor around end of text
            
            if(blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) != "plainText" && cursor > 24){
                
                if(blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) == "bad_Space"){
                    badEntriesDeleted += 1;       // user has deleted bad space
                    totalEntriesTyped -= 1;
                    wordsTyped--;
                }
                else if(blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) == "goodSpace"){
                    wordsTyped--;
                }
                else if(blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) == "bad_Entry"
                        || blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) == "phaseLeft"
                        || blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) == "phaseRght"){
                        badEntriesDeleted += 1;       // user has deleted error
                        totalEntriesTyped -= 1;
                    }
                    
                blockDivText[lineNum] = blockDivText[lineNum].substring(0,localCursor-11) 
                                        + "plainText" + blockDivText[lineNum].substring(localCursor-2);
            
                lineNum = newLineNum;
                localCursor = newLocalCursor;
                }
             else{
                 
                 cursor += 32;
                 deletes--;
             }
             newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "nxtLetter" 
                          + blockDivText[lineNum].substring(localCursor-2);
                    
        } 
        else{
            localCursor -= 32;
            if(blockDivText[lineNum].substring(localCursor-11, localCursor-2) == "bad_Space" 
                || blockDivText[lineNum].substring(localCursor-11, localCursor-2) == "bad_Entry"
                || blockDivText[lineNum].substring(localCursor-11, localCursor-2) == "phaseLeft"){
                    badEntriesDeleted += 1;
                    totalEntriesTyped -= 1;
            }
            if(blockDivText[lineNum].substring(localCursor-11,localCursor-2) == "bad_Space"){
                wordsTyped--;
            }
            if(blockDivText[lineNum].substring(localCursor-11,localCursor-2) == "goodSpace"){
                wordsTyped--;
            }
            newRTEText = blockDivText[lineNum].substring(0, (localCursor+21)%blockDivText[lineNum].length) 
                         + "plainText" + blockDivText[lineNum].substring((localCursor+30)%blockDivText[lineNum].length);
                     
            newRTEText = newRTEText.substring(0,localCursor-11) + "nxtLetter" 
                        + newRTEText.substring(localCursor-2);
        }
        
        

        deletes++;
    }
    

    
    
    // move cursor to next letter
    if( localCursor > blockDivText[lineNum].length){
        
        blockDivText[(lineNum+1)%NUM_LINES] = blockDivText[(lineNum+1)%NUM_LINES].substring(0, 13)
                     + "nxtLetter" + blockDivText[(lineNum+1)%NUM_LINES].substring(22);

    }
    else{
        newRTEText = newRTEText.substring(0, (localCursor-11))
                     + "nxtLetter" + newRTEText.substring(localCursor-2);
    }
    
    // wraparound and copy to div or just copy to div depending
    if(localCursor > blockDivText[lineNum].length){
        if (cursor > (minCursorReplaceStart*32/*MAX_CHARS_IN_LINE*2*32*/)){ //make sure its not the first two lines being typed.
            if(deletes == 0){    // && deletes == 0 so no update if in the past
                tempStr = textString.substring(cutoff, (42*(i+1)) - ((42*i) - cutoff));   // get next 42 characters from text string
                /*var localCutoff = tempStr.lastIndexOf(" ") + 1;                 // find last space in 42 chars
                
                if(localCutoff == 0){
                    localCutoff = 42;
                }
        
                blockDivText[(lineNum+(NUM_LINES-2))%NUM_LINES] = "";    // initialize string
                for(j=0; j<localCutoff; j++){
            
                    blockDivText[(lineNum+(NUM_LINES-2))%NUM_LINES] += '<span class="plainText">' + tempStr.substring(j, j+1) + '</span>';
                }
                cutoff += localCutoff;
                */
                ////////////////
                var localCutoff = tempStr.search(/(\r\n|\n|\r)/);       // find first newline in 42 chars
                if (localCutoff < 1){      // if next 42 characters do not contain a carriage return (\n)


                    localCutoff = tempStr.lastIndexOf(" ") + 1;                 // find last space in 42 chars

                    if(localCutoff == 0){
                        localCutoff = 42;   // if no space found within next 42 chars, cut word at full block line length
                    }

                    blockDivText[(lineNum+(NUM_LINES-1))%NUM_LINES] = "";    // initialize string
                    for(j=0; j<localCutoff; j++){

                        blockDivText[(lineNum+(NUM_LINES-1))%NUM_LINES] += '<span class="plainText">' + tempStr.substring(j, j+1) + '</span>';
                    }
                    cutoff += localCutoff;
                }
                else{       // next 42 characters contained a carriage return
                    var tempStr2 = tempStr.substring(0, localCutoff+1);
                    tempStr2 = tempStr2.replace(/(\r\n|\n|\r|\s\r\n|\s\n|\s\r)/," ");


                    blockDivText[(lineNum+(NUM_LINES-1))%NUM_LINES] = "";    // initialize string
                    for(j=0; j<tempStr2.length; j++){

                        blockDivText[(lineNum+(NUM_LINES-1))%NUM_LINES] += '<span class="plainText">' + tempStr2.substring(j, j+1) + '</span>';
                    }
                    cutoff += localCutoff + 1;

                }
            }            
        }
       
        
        blockDivText[lineNum] = newRTEText;
        tempBlockDivStr[0] = blockDivText[0];
        tempBlockDivStr[1] = blockDivText[1];
        tempBlockDivStr[2] = blockDivText[2];
        tempBlockDivStr[3] = blockDivText[3];
        blockDiv.innerHTML = formatForInnerHTML_block(tempBlockDivStr);
        
        localCursor = 24;
        lineNum++;
        lineNum = lineNum % 4;
    }
    else if(!functionKeyPressed){
        blockDivText[lineNum] = newRTEText;
        tempBlockDivStr[0] = blockDivText[0];
        tempBlockDivStr[1] = blockDivText[1];
        tempBlockDivStr[2] = blockDivText[2];
        tempBlockDivStr[3] = blockDivText[3];
        blockDiv.innerHTML = formatForInnerHTML_block(tempBlockDivStr);
    }
    
    
    if(deletes > 0 && event.keyCode != 8){
        deletes--;
    }

    return false;
}

function Tick() {
	var timeoutEnd = 0;
	
    if (TotalSeconds <= 1) {
        TotalSeconds = 0;
        UpdateTimer()
      
        Timer.innerHTML = "&nbsp;00:00&nbsp;";
        UpdateStats()
        TimeEnd();
        return;
    }

    if(!TimeWasReset){
	timeoutEnd = (new Date).getTime();
    TotalSeconds -= (timeoutEnd - timeoutStart)/1000;
    UpdateTimer()
	timeoutStart = (new Date).getTime();
    window.setTimeout("Tick()", 1000);
    }
    UpdateStats()
}

function UpdateTimer() {
    var Seconds = TotalSeconds;
    var Minutes = Math.floor(Seconds / 60);
    Seconds -= Minutes * (60);
    var TimeStr = (LeadingZero(Minutes) + ":" + LeadingZero(Math.round(Seconds)))
    Timer.innerHTML = TimeStr;
}

function LeadingZero(Time) {
    return (Time < 10) ? "0" + Time : + Time;
}


function Reset(ResetTime, TimerID) {
    Timer = document.getElementById(TimerID);
    TotalSeconds = ResetTime;
    lettersTypedCorrectly = 0;
    lettersTypedIncorrectly = 0;
    badEntriesDeleted = 0;
    totalEntriesTyped = 0;
    wordsTyped = 0;
	letterMistakes = {};
	letterPercentMisses = {};
	letterCounts = {};
    
    UpdateTimer();   
    UpdateStats();

    InitializeTimer_block(ResetTime, TimerID);
    TimeWasReset = true;
    
    $('.mainDivInputs').focus();    //assume user wants to type after clicking reset
    TimeBegin();
    
}

function TimeBegin(){
    $("#timer").css("background-color","transparent");
	$("#divCover").css("z-index", "0");
	$('#PRIW').hide();
    
    $("#resultDivContainer").hide('medium', function(){
		$('.mainDivInputs').focus();    // assume user wants to type after using hotkey
		$("#centerContent").css("height","342px");
		$("#divBackStripe").css("height","234px");
		$("#divBackBottom").css("height","342px");
	});
}

function TimeEnd(){
	TestInfo.WPM = pad(getNetWPM(), 3);
	TestInfo.acc = pad((getAccuracy() * 10), 3);
    TestInfo.endTS = Math.round(Date.now()/1000);
	TestInfo.testLength = $('#timeTypeSelected').val();
	TestInfo.usrName = '';
	TestInfo.usrNameChkSum = 0;
	TestInfo.pn = 524286;
	
	if(TestInfo.acc == 0){
		TestInfo.acc = "000";
	} else if(TestInfo.acc.length < 3){
		TestInfo.acc = "0" + TestInfo.acc;
	} else if(TestInfo.acc.length > 3){
		TestInfo.acc = TestInfo.acc.substring(0,3);
	}
	
	if(TestInfo.testLength.length == 2)
		TestInfo.testLength = "0" + TestInfo.testLength;
	
    $("#timer").css("background-color","red");
	$("#divCover").css("z-index", "400");
	
	$('#PRIW').show();
    
    UpdateResults();
    
    var twtrTwt = "http://twitter.com/home?status=Just took a free typing test online - I type " + TestInfo.WPM + " words per minute! How fast are you? - http://www.speedtypingonline.com";
	var shareURL = "http://www.speedtypingonline.com/myResults.php?r=" + encodeURIComponent(parseInt("1" + TestInfo.WPM + TestInfo.acc + TestInfo.testLength) * TestInfo.pn);
	
	generateGenLink();
	
    var newURL = "http://www.facebook.com/share.php?u=http://www.speedtypingonline.com/fbr.php?r=" + encodeURIComponent(getNetWPM()*492368);
    $("#fbrLink").attr("href", newURL);
    $("#twtrLink").attr("href", twtrTwt);
    
    if ($("#resultDivContainer").is(":hidden")) {
            $("#resultDivContainer").slideDown('slow', function(){});
    }
    $("#centerContent").css("height","902px");
    $("#divBackStripe").css("height","798px");
    $("#divBackBottom").css("height","895px");
    
	testCounter++;	
	_gaq.push(['_trackEvent', 'Typing Test', 'Num Rounds', 'AtTimeEnd', testCounter.toString(), true]);
}

function generateGenLink(){
	var newUserName = $('#personName').val();
	if(newUserName != '' && newUserName != "<Name>"){
		userNameToAscii(newUserName);
	} else {
		TestInfo.usrName = '';
	}
	
	var shareURL = "http://www.speedtypingonline.com/myResults.php?r=" + encodeURIComponent(parseInt("1" + TestInfo.WPM + TestInfo.acc + TestInfo.testLength) * TestInfo.pn);
	shareURL += "&d=" + encodeURIComponent(TestInfo.endTS * TestInfo.pn);
	
	if(TestInfo.usrName != ''){
		shareURL += "&n=" + encodeURIComponent(TestInfo.usrName);
		shareURL += "&c=" + encodeURIComponent(TestInfo.usrNameChkSum * TestInfo.pn);
	}
	
	$('#genLink').attr("href", shareURL);
	$('#genLinkDisplay').attr("value", shareURL.substring(7));
	$('#genLinkEmail').attr("href", "mailto:example@domain.com?subject=" + encodeURIComponent("My Typing Test Results") + "&body=" + encodeURIComponent("Here is the link to my typing results:") + "%0D" + encodeURIComponent(shareURL));
}

function userNameToAscii(s)
{
  var ascii="";
  var sum=0;
  if(s.length>0)
    for(i=0; i<s.length; i++)
    {
      var c = s.charCodeAt(i).toString(16);
      while(c.length < 2)
       c = "0"+c;
      ascii += c;
	  sum += parseInt(c,16);
    }
  TestInfo.usrName = ascii;
  TestInfo.usrNameChkSum = sum;
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

/*function isCharToCount(ch){
	chAscii = ch.toLowerCase().charCodeAt(0);
	
	if( (chAscii) || () || )
}*/

function CreateTimer_line(event, TimerID, Time) {

    var functionKeyPressed = true;     // used to flag when function key (delete, shift...) is pressed
    var lineMiddle = (21*32)-8;

    
    if (TotalSeconds==undefined || TimeWasReset) {
    cursor = 24;        //first letter has "<span class="plainText">" before it
    localCursor = 0;
    TimeWasReset = false;
    RoundTime = Time;
    
    Timer = document.getElementById(TimerID);
    TotalSeconds = Time;
    
    UpdateTimer()
	timeoutStart = (new Date).getTime();
    window.setTimeout("Tick()", 1000);
    }
    
    if (TimeWasReset){
    // OLD DISABLING $('#main_input').attr('disabled', false);
    TotalSeconds = Time;
    TimeWasReset = false;   //reset TimeWasReset flag        
    }
    
    // This makes the box disabled when time is up
    if (TotalSeconds == 0){
        return false;
    }
    
    // Get text in RTE box
    var newRTEText = "";
    var RTELetter = lineDivText.substring(lineMiddle,lineMiddle+1);
    
    var keyCode_entered = event.keyCode;
	
	if($('#keyLayoutSelected').val().toUpperCase() == "DVORAK"){
        var key_entered = d_keycode.d_getValueByEvent(event);
    }
	else if($('#keyLayoutSelected').val().toUpperCase() == "QWERTY_UK"){
        var key_entered = UK_QWERTY_keycode.UK_QWERTY_getValueByEvent(event);
    }
	else if($('#keyLayoutSelected').val().toUpperCase() == "COLEMAK"){
        var key_entered = cole_keycode.cole_getValueByEvent(event);
    }
    else{       // default to QWERTY
        var key_entered = keycode.getValueByEvent(event);
    }
    
    //var new1 = keycode.getValueByEvent(event);

            // event.keyCode: 97-122 = a-z, 65-90 = A-Z, 8 = backspace, 32 = space
    if( key_entered == RTELetter){  /* characters match */
        functionKeyPressed = false;
        
        if(RTELetter == " "){
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "goodSpace" 
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30) 
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>";   
            wordsTyped++;
        }
        else{
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "goodEntry"
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30) 
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>";  
        }
        
        cursor += 32;
        lettersTypedCorrectly += 1;
        totalEntriesTyped += 1;
        
        if(phaseShift != 0){
            phaseShift = 0;     // correct entry resets any possible phase shift
        }
        
        if(localCursor < 20) {localCursor += 1;}
    }      
    else if((keyCode_entered==32)||(keyCode_entered==42)||(keyCode_entered==43)||(keyCode_entered==45)||
            ((keyCode_entered >= 47  ) && ( keyCode_entered <= 59))||
            ((keyCode_entered >= 61  ) && ( keyCode_entered <= 90))||
            ((keyCode_entered >= 96  ) && ( keyCode_entered <= 111))||
            ((keyCode_entered >= 186  ) && ( keyCode_entered <= 223))){
        functionKeyPressed = false;
        
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION START >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // // -------------------------------------------------------------------------------------
        // **FIRST** see if we are looking for a phase shift
        if(phaseShift != 0){
            if(phaseShift == 1){
                if(key_entered == lineDivText.substring(lineMiddle+32, lineMiddle+33)){
                    //we have a phase shift to the right
                    var shiftRight = true;
                }
            }
            else{   //phase shift = -1
                if(key_entered == lineDivText.substring(lineMiddle-32, lineMiddle-31)){
                    var shiftLeft = true;
                }
            }
            phaseShift = 0;     // phase shift has been corrected and realigned
            
        }
        // **THEN** see if we should look for a phase shift to the right (positive time)
        else if(key_entered == lineDivText.substring(lineMiddle+32, lineMiddle+33)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = 1;     //only set the flag if the user wants to do phase shift correction
            }
        }
        // see if we should look for a phase shift to the left (negative time)
        else if(key_entered == lineDivText.substring(lineMiddle-32, lineMiddle-31)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = -1;    //only set the flag if the user wants to do phase shift correction
            }   
        }
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION END >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // -------------------------------------------------------------------------------------
        
        if(shiftRight){
            newRTEText = lineDivText.substring(64,lineMiddle-11) + "phaseRght" 
                         + lineDivText.substring(lineMiddle-2,lineMiddle+21) + "goodEntry"
                         + lineDivText.substring(lineMiddle+30,lineMiddle+53) + "nxtLetter"
                         + lineDivText.substring(lineMiddle+62) + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>"
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+40)/32)+1, ((cursor+lineMiddle+40)/32)+2) + "</span>";
            cursor +=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   Do not count since we are jumping ahead by 1 letter
            shiftRight = false;  // phase shift should have been successfully applied by now
            
            if(localCursor < 20) {localCursor += 1;}
        }
        else if(shiftLeft){
            newRTEText = lineDivText.substring(0,lineMiddle-43) + "phaseLeft" 
                         + lineDivText.substring(lineMiddle-34);
            cursor -=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   For some reason double counts with this line
            shiftLeft = false;  // phase shift should have been successfully applied by now
            
            if(localCursor < 20) {localCursor += 1;}
        }    
        else if(RTELetter == " "){   /* put an _ if space typed in wrong */
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "bad_Space"
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30)
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>";  
            wordsTyped++;

        }
        else{
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "bad_Entry"
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30) 
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>";  
        }
        
        cursor += 32;
        lettersTypedIncorrectly += 1;
        totalEntriesTyped += 1;
        
        if(localCursor < 20) {localCursor += 1;}
    }
    else if( key_entered == "backspace" && cursor > 24 && localCursor > 0){      /*no match and it is a backspace*/ 
        functionKeyPressed = false;
        // backstep first
            cursor -= 32;
            localCursor -= 1;
            
        if(lineDivText.substring(lineMiddle-43, lineMiddle-34) == "bad_Space"){
            wordsTyped--;
            //totalEntriesTyped -= 1;
        }
        
        if(lineDivText.substring(lineMiddle-43, lineMiddle-34) == "goodSpace"){
            wordsTyped--;
        }
        
        if(lineDivText.substring(lineMiddle-43, lineMiddle-34) == "bad_Space"
                || lineDivText.substring(lineMiddle-43, lineMiddle-34) == "bad_Entry"
                || lineDivText.substring(lineMiddle-43, lineMiddle-34) == "phaseLeft"){
            badEntriesDeleted++;
            //totalEntriesTyped -= 1;
        }
            
        if(cursor < lineMiddle){
            //localCursor = lineDivText.length - 8;
            newRTEText = '<span class="hiddenTxt"> </span>'
                         + lineDivText.substring(0, lineMiddle-43) + "nxtLetter" 
                         + lineDivText.substring(lineMiddle-34,lineMiddle-11) + "plainText"
                         + lineDivText.substring(lineMiddle-2,lineDivText.length-32);    
        } 
        else{
           //newRTEText = blockLineText.substring(0,localCursor-11) + "plainText" + blockLineText.substring(localCursor-2);
            
           newRTEText = //textString.substring((cursor-(lineMiddle+8))/32, ((cursor-(lineMiddle+8))/32)+1)
                        '<span class="hiddenTxt"> </span>'
                        + lineDivText.substring(0, lineMiddle-43) + "nxtLetter" 
                         + lineDivText.substring(lineMiddle-34,lineMiddle-11) + "plainText"
                         + lineDivText.substring(lineMiddle-2,lineDivText.length-32);        
        }   

        deletes++;
    }
    
    if(!functionKeyPressed){
        lineDivText = newRTEText;
        lineDiv.innerHTML = formatForInnerHTML(newRTEText);
    }
    return false;
}

function formatForInnerHTML(StringToFormat){
    
    var StringToReturn = "";
    StringToFormat = StringToFormat.replace(/> </g, ">&nbsp;<");
    //StringToFormat = StringToFormat.replace(/">/g, '">&#8203;');
    StringToReturn = '<div id="line_input">' + StringToFormat + "</div>";
    return StringToReturn;
}

function formatForInnerHTML_block(StringToFormat){
    
    var StringToReturn = "";
    
    for(i=0; i < 4; i++){
        StringToFormat[i] = StringToFormat[i].replace(/> </g, ">&nbsp;<");
        //StringToFormat[i] = StringToFormat[i].replace(/">/g, '">&#8203;');
        
        StringToReturn += '<div id="blockLine' + i + '" class="blockLines" unselectable="on" style="resize:none;">' 
                            + StringToFormat[i]
                            + "</div>";
    }

   
   //StringToFormat = StringToFormat.replace(/">/g, '"><span class="junkSpace"> </span>');

    return StringToReturn;
}

function setupFocus(){
        /*$('.mainDivInputs').bind('blur', function() {
        this.focus();
    });*/
    
    
    $('.mainDivInputs').bind('click', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('mouseleave', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('mouseout', function() {
        this.focus();
    });
    
    $('.mainDivInputs').focus(function() {
        ApplyColorsFocused();
    });
    
    $('.mainDivInputs').blur(function() {
        ApplyColorsBlurred();
    });
    

}


keycode = {
    getKeyCode : function(e) {
        var keycode = null;
        if(window.event) {
            keycode = window.event.keyCode;
        }else if(e) {
            keycode = e.which;
        }
        return keycode;
    },
    getKeyCodeValue : function(keyCode, shiftKey) {
        shiftKey = shiftKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.modifiedByShift[keyCode];
        }else {
            value = this.keyCodeMap[keyCode];
        }
        return value;
    },
    getValueByEvent : function(e) {
        return this.getKeyCodeValue(this.getKeyCode(e), e.shiftKey);
    },
    keyCodeMap : {
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"capslock", 27:"escape", 32:" ", 33:"pageup",
        34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"+", 44:"printscreen", 45:"-", 46:"delete", 47:"/",
        48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:";",
        61:"=", 65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l",
        77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z",
        96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9",
        106: "*", 107:"+", 109:"-", 110:".", 111: "/",
        112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12",
        144:"numlock", 145:"scrolllock", 186:";", 187:"=", 188:",", 189:"-", 190:".", 191:"/", 192:"`", 219:"[", 220:"\\", 221:"]", 222:"'"
    },
    modifiedByShift : {
        32: " ", 65:"A", 66:"B", 67:"C", 68:"D", 69:"E", 70:"F", 71:"G", 72:"H", 73:"I", 74:"J", 75:"K", 76:"L",
        77:"M", 78:"N", 79:"O", 80:"P", 81:"Q", 82:"R", 83:"S", 84:"T", 85:"U", 86:"V", 87:"W", 88:"X", 89:"Y", 90:"Z", 192:"~", 48:")", 49:"!", 50:"@", 51:"#", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 109:"_", 61:"+",
        219:"{", 221:"}", 220:"|", 59:":", 222:"\"", 186:":", 187:"+", 188:"<", 189:"_", 190:">", 191:"?"/*,
        96:"insert", 97:"end", 98:"down", 99:"pagedown", 100:"left", 102:"right", 103:"home", 104:"up", 105:"pageup"*/
    }
};


d_keycode = {
    d_getKeyCode : function(e) {
        var d_keycode = null;
        if(window.event) {
            d_keycode = window.event.keyCode;
        }else if(e) {
            d_keycode = e.which;
        }
        return d_keycode;
    },
    d_getKeyCodeValue : function(d_keyCode, shiftKey) {
        shiftKey = shiftKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.d_modifiedByShift[d_keyCode];
        }else {
            value = this.d_keyCodeMap[d_keyCode];
        }
        return value;
    },
    d_getValueByEvent : function(e) {
        return this.d_getKeyCodeValue(this.d_getKeyCode(e), e.shiftKey);
    },
    d_keyCodeMap : {
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"capslock", 27:"escape", 32:" ", 33:"pageup",
        34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"}", 44:"printscreen", 45:"[", 46:"delete", 47:"z",
        48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:"s",
        61:"]", 65:"a", 66:"x", 67:"j", 68:"e", 69:".", 70:"u", 71:"i", 72:"d", 73:"c", 74:"h", 75:"t", 76:"n",
        77:"m", 78:"b", 79:"r", 80:"l", 81:"'", 82:"p", 83:"o", 84:"y", 85:"g", 86:"k", 87:",", 88:"q", 89:"f", 90:";",
        96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9",
        106: "*", 107:"}", 109:"[", 110:"v", 111: "z",
        112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12",
        144:"numlock", 145:"scrolllock", 186:"s", 187:"]", 188:"w", 189:"[", 190:"v", 191:"z", 192:"`", 219:"/", 220:"\\", 221:"=", 222:"-"
    },
    d_modifiedByShift : {
        32: " ", 65:"A", 66:"X", 67:"J", 68:"E", 69:">", 70:"U", 71:"I", 72:"D", 73:"C", 74:"H", 75:"T", 76:"N",
        77:"M", 78:"B", 79:"R", 80:"L", 81:"\"", 82:"P", 83:"O", 84:"Y", 85:"G", 86:"K", 87:"<", 88:"Q", 89:"F", 90:":", 192:"~", 48:")", 49:"!", 50:"@", 51:"#", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 109:"{", 61:"}",
        219:"?", 221:"+", 220:"|", 59:"S", 222:"_", 186:"S", 187:"}", 188:"W", 189:"{", 190:"V", 191:"Z"/*,
        96:"insert", 97:"end", 98:"down", 99:"pagedown", 100:"left", 102:"right", 103:"home", 104:"up", 105:"pageup"*/
    }
};

cole_keycode = {
    cole_getKeyCode : function(e) {
        var cole_keycode = null;
        if(window.event) {
            cole_keycode = window.event.keyCode;
        }else if(e) {
            cole_keycode = e.which;
        }
        return cole_keycode;
    },
    cole_getKeyCodeValue : function(cole_keyCode, shiftKey) {
        shiftKey = shiftKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.cole_modifiedByShift[cole_keyCode];
        }else {
            value = this.cole_keyCodeMap[cole_keyCode];
        }
        return value;
    },
    cole_getValueByEvent : function(e) {
        return this.cole_getKeyCodeValue(this.cole_getKeyCode(e), e.shiftKey);
    },
    cole_keyCodeMap : {
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"backspace", 27:"escape", 32:" ", 33:"pageup",
        34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"+", 44:"printscreen", 45:"-", 46:"delete", 47:"/",
        48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:"o",
        61:"=", 65:"a", 66:"b", 67:"c", 68:"s", 69:"f", 70:"t", 71:"d", 72:"h", 73:"u", 74:"n", 75:"e", 76:"i",
        77:"m", 78:"k", 79:"y", 80:";", 81:"q", 82:"p", 83:"r", 84:"g", 85:"l", 86:"v", 87:"w", 88:"x", 89:"j", 90:"z",
        96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9",
        106: "*", 107:"+", 109:"-", 110:".", 111: "/",
        112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12",
        144:"numlock", 145:"scrolllock", 186:"o", 187:"=", 188:",", 189:"-", 190:".", 191:"/", 192:"`", 219:"[", 220:"\\", 221:"]", 222:"'"
    },
    cole_modifiedByShift : {
        32: " ", 59:"O", 65:"A", 66:"B", 67:"C", 68:"S", 69:"F", 70:"T", 71:"D", 72:"H", 73:"U", 74:"N", 75:"E", 76:"I",
        77:"M", 78:"K", 79:"Y", 80:";", 81:"Q", 82:"P", 83:"R", 84:"G", 85:"L", 86:"V", 87:"W", 88:"X", 89:"J", 90:"Z", 192:"~", 48:")", 49:"!", 50:"@", 51:"#", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 109:"_", 61:"+",
        219:"{", 221:"}", 220:"|", 222:"\"", 186:"O", 187:"+", 188:"<", 189:"_", 190:">", 191:"?"/*,
        96:"insert", 97:"end", 98:"down", 99:"pagedown", 100:"left", 102:"right", 103:"home", 104:"up", 105:"pageup"*/
    }
};

UK_QWERTY_keycode = {
    UK_QWERTY_getKeyCode : function(e) {
        var UK_QWERTY_keycode = null;
        if(window.event) {
            UK_QWERTY_keycode = window.event.keyCode;
        }else if(e) {
            UK_QWERTY_keycode = e.which;
        }
        return UK_QWERTY_keycode;
    },
    UK_QWERTY_getKeyCodeValue : function(UK_QWERTY_keyCode, shiftKey, altKey) {
        shiftKey = shiftKey || false;
		altKey = altKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.UK_QWERTY_modifiedByShift[UK_QWERTY_keyCode];
        }else if (altKey === true) {
            value = this.UK_QWERTY_modifiedByAltGr[UK_QWERTY_keyCode];
        }else {
            value = this.UK_QWERTY_keyCodeMap[UK_QWERTY_keyCode];
        }
        return value;
    },
	UK_QWERTY_MOZILLA_getKeyCodeValue : function(UK_QWERTY_MOZILLA_keyCode, shiftKey, altKey) {
        shiftKey = shiftKey || false;
		altKey = altKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.UK_QWERTY_MOZILLA_modifiedByShift[UK_QWERTY_MOZILLA_keyCode];
        }else if (altKey === true) {
            value = this.UK_QWERTY_modifiedByAltGr[UK_QWERTY_MOZILLA_keyCode];
        }else {
            value = this.UK_QWERTY_MOZILLA_keyCodeMap[UK_QWERTY_MOZILLA_keyCode];
        }
        return value;
    },
    UK_QWERTY_getValueByEvent : function(e) {
		if(!$.browser.mozilla)
			return this.UK_QWERTY_getKeyCodeValue(this.UK_QWERTY_getKeyCode(e), e.shiftKey, e.altKey);
			
		return this.UK_QWERTY_MOZILLA_getKeyCodeValue(this.UK_QWERTY_getKeyCode(e), e.shiftKey, e.altKey);
    },
	UK_QWERTY_keyCodeMap : {
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"capslock", 27:"escape", 32:" ", 33:"pageup", 34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"+", 44:"printscreen", 45:"-", 46:"delete", 47:"/", 48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:";", 61:"=", 65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l", 77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z", 96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9", 106: "*", 107:"+", 109:"-", 110:".", 111: "/", 112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12", 144:"numlock", 145:"scrolllock", 163:"#", 173:"-", 186:";", 187:"=", 188:",", 189:"-", 190:".", 191:"/", 192:"'", 219:"[", 220:"\\", 221:"]", 222:"#", 223:"`"
    },
    UK_QWERTY_modifiedByShift : {
        32: " ", 50:"\"", 51:"£", 65:"A", 66:"B", 67:"C", 68:"D", 69:"E", 70:"F", 71:"G", 72:"H", 73:"I", 74:"J", 75:"K", 76:"L", 77:"M", 78:"N", 79:"O", 80:"P", 81:"Q", 82:"R", 83:"S", 84:"T", 85:"U", 86:"V", 87:"W", 88:"X", 89:"Y", 90:"Z", 192:"~", 48:")", 49:"!", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 59:":", 61:"+", 109:"_", 163:"~", 173:"_", 186:":", 187:"+", 188:"<", 189:"_", 190:">", 191:"?", 192:"@", 219:"{", 221:"}", 220:"|",  222:"~", 223:"¬"
    },
	UK_QWERTY_MOZILLA_keyCodeMap : {
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"capslock", 27:"escape", 32:" ", 33:"pageup", 34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"+", 44:"printscreen", 45:"-", 46:"delete", 47:"/", 48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:";", 61:"=", 65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l", 77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z", 96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9", 106: "*", 107:"+", 109:"-", 110:".", 111: "/", 112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12", 144:"numlock", 145:"scrolllock", 163:"#", 173:"-", 186:";", 187:"=", 188:",", 189:"-", 190:".", 191:"/", 192:"`", 219:"[", 220:"\\", 221:"]", 222:"'", 223:"`"
    },
    UK_QWERTY_MOZILLA_modifiedByShift : {
        32: " ", 50:"\"", 51:"£", 65:"A", 66:"B", 67:"C", 68:"D", 69:"E", 70:"F", 71:"G", 72:"H", 73:"I", 74:"J", 75:"K", 76:"L", 77:"M", 78:"N", 79:"O", 80:"P", 81:"Q", 82:"R", 83:"S", 84:"T", 85:"U", 86:"V", 87:"W", 88:"X", 89:"Y", 90:"Z", 192:"~", 48:")", 49:"!", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 59:":", 61:"+", 109:"_", 163:"~", 173:"_", 186:":", 187:"+", 188:"<", 189:"_", 190:">", 191:"?", 192:"¬", 219:"{", 221:"}", 220:"|",  222:"@", 223:"¬"
    },
	UK_QWERTY_modifiedByAltGr : {
		52:"€", 65:"á", 69:"é", 73:"í", 79:"ó", 85:"ú", 192:"¦", 223:"¦"
	}
};
function getPComplete() {
    var a;
    a = (lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight) / (textString.length - 1) * 100;
    a = Math.floor(a);
    return a
}
function getNumFixedMistakes() {
    var a = 0;
    if (lettersTypedCorrectly >= badEntriesDeleted) {
        a = badEntriesDeleted
    } else {
        a = badEntriesDeleted - (badEntriesDeleted - lettersTypedCorrectly)
    }
    return a
}
function getTimeInMins() {
    timeInSecs = getTimeElapsed();
    timeInMins = timeInSecs / 60;
    timeInMins *= 10;
    timeInMins = Math.round(timeInMins);
    timeInMins /= 10;
    return timeInMins
}
function getAccuracy() {
    var a = 100;
    var b = lettersTypedCorrectly + lettersTypedIncorrectly - badEntriesDeleted + getNumFixedMistakes();
    if (getTimeElapsed() != 0) {
        a = lettersTypedCorrectly / b * 100;
        a *= 10;
        a = Math.round(a);
        a /= 10;
        if (b == 0) {
            a = 100
        }
        if (a < 0) {
            a = 0
        }
    }
    return a
}
function getGrossWPM() {
    var a = 0;
    var b = lettersTypedCorrectly + lettersTypedIncorrectly;
    if (getTimeElapsed() != 0) {
        a = b / getTimeElapsed();
        a *= 12;
        if (a == "Infinity" || a < 0) {
            a = 0
        }
        a = Math.round(a)
    }
    return a
}
function getNetWPM() {
    var a = 0;
    var b = lettersTypedCorrectly + lettersTypedIncorrectly;
    if (getTimeElapsed() != 0) {
        a = b / getTimeElapsed();
        a *= 12;
        a = a - getEPM();
        if (a == "Infinity" || a < 0) {
            a = 0
        }
        a = Math.round(a)
    }
    return a
}
function getEPM() {
    return Math.round((lettersTypedIncorrectly - getNumFixedMistakes()) / getTimeElapsed() * 60)
}
function getTimeElapsed() {
    if (typeof Timer == "undefined") {
        return TotalSeconds
    }
    return RoundTime - TotalSeconds
}
function getSortedArray(arr, num){
	sortable = [];
	for(var p in arr){
		if (arr.hasOwnProperty(p)){
			sortable.push([p, arr[p]]);
		}
	}
	sortable.sort(function(a, b){return b[1] - a[1];});
	return sortable.slice(0, num);
}
function getKPM() {
    return Math.round((lettersTypedCorrectly + lettersTypedIncorrectly) / getTimeElapsed() * 60)
}
function UpdateResults() {
    testCounter++;
    var a = $("#WPM_Result").get();
	var b = getNetWPM();
	var c = getAccuracy();
	var d = lettersTypedCorrectly + lettersTypedIncorrectly;
	var e = getNumFixedMistakes();
	var f = getEPM();
	var g = getKPM();
	var h = getTimeInMins();
	var k = getSortedArray(letterMistakes, 3);
	var m = getSortedArray(letterPercentMisses, 3);
	
	var p = JSON.stringify(letterMistakes);
	var q = JSON.stringify(letterCounts);
	
	$.post('setData.php', { type: "tt", wpm: b, accuracy: c, total_entries: d, incorrect_entries: lettersTypedIncorrectly, fixed_mistakes: e, total_time_mins: h, letter_mistakes: p, letter_counts: q},
		function(output){
			alert(output);
			//$('#theTestCount').html(output);
			//alert("sent data: " + output);
	});
			
    a[0].innerHTML = b + "<span> WPM</span>";
    a = $("#rWPM_Result").get();
    a[0].innerHTML = getGrossWPM() + "<span> WPM</span>";
    a = $("#Accur_Result").get();
    a[0].innerHTML = c + "<span> %</span>";
    a = $("#TLT_Result").get();
    a[0].innerHTML = d;
    a = $("#LTC_Result").get();
    a[0].innerHTML = lettersTypedCorrectly;
    a = $("#nLTI_Result").get();
    a[0].innerHTML = lettersTypedIncorrectly;
    a = $("#CL_Result").get();
    a[0].innerHTML = e;
    a = $("#ER_Result").get();
    a[0].innerHTML = f;
    a = $("#KPM_Result").get();
    a[0].innerHTML = g + "<span> KPM</span>";
    a = $("#words_Result").get();
    a[0].innerHTML = wordsTyped;
	a = $("#LetMissed_Result").get();
	a[0].innerHTML = (k != '') ? k.map(function(elem){return "<div><span class=\"typeLetter bold\">" + elem[0] + "</span> (" + elem[1] + " misses)</div>";}).join('') : "None";
	a = $("#LetPercent_Result").get();
	a[0].innerHTML = (m != '') ? m.map(function(elem){return "<div><span class=\"typeLetter bold\">" + elem[0] + "</span> (" + elem[1] + "% missed)</div>";}).join('') : "&nbsp;&nbsp;No mistakes!";
    a = $("#time_Result").get();
    a[0].innerHTML = h + "<span> min</span>"
}
function UpdateStats() {
    var a = $("#wpmValue").get();
    if (a.length != 0) {
        a[0].innerHTML = getNetWPM()
    }
    var b = $("#accuracyValue").get();
    if (b.length != 0) {
        b[0].innerHTML = getAccuracy() + "<span> %</span>"
    }
    var c = $("#pCompleteValue").get();
    if (c.length != 0 && (lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight == textString.length - 1 || TotalSeconds % 2 == 0)) {
        c[0].innerHTML = getPComplete() + "<span> %</span>"
    }
}
function GetText(){var e=$("#textTypeSelected").val().toUpperCase();var t="";var n="N/A";var r="N/A";var i=0;var s="resources/textToType.php";while(i<2){if(window.XMLHttpRequest){xmlhttp=new XMLHttpRequest}else{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP")}xmlhttp.open("GET",s,false);xmlhttp.send();xmlDoc=xmlhttp.responseXML;if(xmlDoc!=null){i=2}else{s="resources/textToType.xml";i++}}if(xmlDoc!=null){var o=Math.floor(Math.random()*xmlDoc.getElementsByTagName(e.toString()).length);var i=0;while(o==lastRandNum&&i<3){o=Math.floor(Math.random()*xmlDoc.getElementsByTagName(e.toString()).length);i++}lastRandNum=o;theEntry=$(xmlDoc).find(e)[o];textString=theEntry.getElementsByTagName("TEXT")[0].childNodes[0].nodeValue;n=theEntry.getElementsByTagName("TITLE")[0].childNodes[0].nodeValue;r=theEntry.getElementsByTagName("AUTHOR")[0].childNodes[0].nodeValue;t=theEntry.getElementsByTagName("TYPE")[0].childNodes[0].nodeValue}else{textString="Sphinx of black quartz, judge my vow. The five boxing wizards jump quickly. Pack my box with five dozen liquor jugs. The quick brown fox jumps over a lazy dog. Every good cow, fox, squirrel, and zebra likes to jump over happy dogs. Just keep examining every low bid quoted for zinc etchings. A quick movement of the enemy will jeopardize six gunboats. Few black taxis drive up major roads on quiet hazy nights. Big July earthquakes confound zany experimental vow. Grumpy wizards make a toxic brew for the jovial queen. My girl wove six dozen plaid jackets before she quit. Painful zombies quickly watch a jinxed graveyard. The lazy major was fixing Cupid's broken quiver. The quick onyx goblin jumps over the lazy dwarf. Twelve ziggurats quickly jumped a finch box. My faxed joke won a pager in the cable TV quiz show. Woven silk pyjamas exchanged for blue quartz. We promptly judged antique ivory buckles for the next prize. How razorback jumping frogs can level six piqued gymnasts. Sixty zippers were quickly picked from the woven jute bag. The exodus of jazzy pigeons is craved by squeamish walkers. All questions asked by five watch experts amazed the judge. A quick movement of the enemy will jeopardize six gunboats. West quickly gave Bert handsome prizes for six juicy plums. Jeb quickly drove a few extra miles on the glazed pavement";n="Default";r="Default";t="Pangrams";e="DEFAULT"}$("#textTypeDisplay").html(t);$("#textTitleDisplay").html(n);$("#textAuthorDisplay").html(r);textString=textString.replace(/&/g,"&");textString=textString.replace(/</g,"<");textString=textString.replace(/>/g,"<");textString=textString.replace(/  /g," ");var u="";if(e=="DEFAULT"){u=textString.split(". ");textString="";while(textString.length<5e3){textString+=u[Math.floor(Math.random()*u.length)]+". "}}else if(e=="FACTS"||theEntry.getElementsByTagName("FACTS").length==1){u=textString.split(". ");textString="";while(textString.length<5e3){textString+=u[Math.floor(Math.random()*u.length)]+". "}}else if(e=="COMMON_WORDS"||theEntry.getElementsByTagName("COMMON_WORDS").length==1){u=textString.split(". ");textString="";while(textString.length<5e3){textString+=u[Math.floor(Math.random()*u.length)]+" "}}else if(e=="SIGHT_WORDS"||theEntry.getElementsByTagName("SIGHT_WORDS").length==1){u=textString.split(". ");textString="";while(textString.length<5e3){textString+=u[Math.floor(Math.random()*u.length)]+" "}}else if(e=="EASY_WORDS"||theEntry.getElementsByTagName("EASY_WORDS").length==1){u=textString.split(". ");textString="";while(textString.length<5e3){textString+=u[Math.floor(Math.random()*u.length)]+" "}}else if(e=="PROVERBS"||theEntry.getElementsByTagName("PROVERBS").length==1){u=textString.split(". ");textString="";while(textString.length<5e3){textString+=u[Math.floor(Math.random()*u.length)]+". "}}else if(e=="PANGRAMS"||theEntry.getElementsByTagName("PANGRAMS").length==1){u=textString.split(". ");textString="";while(textString.length<5e3){textString+=u[Math.floor(Math.random()*u.length)]+". "}}else if(e=="CUSTOM"||theEntry.getElementsByTagName("CUSTOM").length==1){u=$("#customTextInput").val();if(u!=""){textString=u+" "}else{$("#customTextInput").val(textString)}while(textString.length<5e3){textString+=textString}}else{while(textString.length<5e3){textString+=" "+textString}}if($("#dsCheckbox").is(":checked")){textString=textString.replace(/[.] /g,".  ");textString=textString.replace(/[?] /g,"?  ");textString=textString.replace(/[!] /g,"!  ")}}function findCSSRule(e,t){ruleIndex=-1;for(i=1;i<t.length;i++){if(t[i].selectorText.toLowerCase()==e.toLowerCase()){ruleIndex=i;break}}return ruleIndex}function ApplyColorsBlurred(){var e=new Array;var t="span.plainText";var i=0;var fs = false;while((i < document.styleSheets.length) && !fs){if(document.styleSheets[i].cssRules && (findCSSRule("span.goodEntry",document.styleSheets[i].cssRules) >= 0)){e=document.styleSheets[i].cssRules;fs = true;}else if(document.styleSheets[i].rules && (findCSSRule("span.goodEntry",document.styleSheets[i].rules) >= 0)){e=document.styleSheets[i].rules;fs = true;}i++;}if(fs){e[findCSSRule("span.nxtLetter",e)].style.color=$("#PTFC").val();e[findCSSRule("span.nxtLetter",e)].style.backgroundColor=$("#PTBack").val();}}function ApplyColorsFocused(){var e=new Array;var t="span.plainText";var i=0;var fs=false;while((i < document.styleSheets.length) && !fs){if(document.styleSheets[i].cssRules && (findCSSRule("span.goodEntry",document.styleSheets[i].cssRules) >= 0)){e=document.styleSheets[i].cssRules;fs = true;}else if(document.styleSheets[i].rules && (findCSSRule("span.goodEntry",document.styleSheets[i].rules) >= 0)){e=document.styleSheets[i].rules;fs = true;}i++;}if(fs){e[findCSSRule("span.nxtLetter",e)].style.color=$("#CFC").val();e[findCSSRule("span.nxtLetter",e)].style.backgroundColor=$("#CBack").val();}}function toggleColorContainer(){if($("#colorContainer").is(":hidden")){$("#colorHideIcon").attr("src","images/minusIcon.gif");$("#colorHideText").html("Hide Custom Colors")}else{$("#colorHideIcon").attr("src","images/plusIcon.gif");$("#colorHideText").html("Show Custom Colors")}$("#hiddenContainer").slideDown("slow",function(){});$("#colorContainer").toggle("slow",function(){if($("#customTextContainer").is(":hidden")&&$("#customTextContainer").is(":hidden")&&$("#colorContainer").is(":hidden")){$("#hiddenContainer").slideUp("slow",function(){})}});_gaq.push(['_setCustomVar',2,'Edit Colors Toggled','Yes',3]);_gaq.push(['_trackEvent','Typing Test', 'Using color editing',]);}function defaultColors(){$("#PTFC").attr("value","#999999");$("#PTBack").attr("value","#FFFFFF");$("#CTFC").attr("value","#009900");$("#CTBack").attr("value","#FFFFFF");$("#PSLFC").attr("value","#009900");$("#PSLBack").attr("value","#FFFFFF");$("#PSRFC").attr("value","#009900");$("#PSRBack").attr("value","#FFFFFF");$("#ITFC").attr("value","#FF0000");$("#ITBack").attr("value","#FFFFFF");$("#ISBack").attr("value","#FF0000");$("#CFC").attr("value","#FFFFFF");$("#CBack").attr("value","#000000");}var lastRandNum;var theEntry;var testCounter=0
