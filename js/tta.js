var sWatch = null;
var TotalSeconds;
var RoundTime = 0;
var TimeWasReset = true;
var TimeIsStopped = false;
var localCursor = 0;
var cursor = 0;
var lettersTypedCorrectly = 0;
var lettersTypedIncorrectly = 0;
var badEntriesDeleted = 0;
var totalEntriesTyped = 0;
var wordsTyped = 0;
var blockDiv = null;
var textString = "";
var deletes = 0;
var totalDeletes = 0;
var lineNum = 0;
var blockDivText = "";
var cutoff = 0;
var phaseShift = 0;
var pShiftWeight = 0;
var minCursorReplaceStart = 0;
var testCounter = 0;
var timeDivider = 0;

var textType = null;
var maxCharsInLine = 26;
var slowestTime = 99999999;
var highScores = [slowestTime, slowestTime, slowestTime, slowestTime, slowestTime];
var HScoresWereFiltered = false;

var t=[0, 0, 0, 0, 0, 0, 0, 1];

function GetText(){
	textType = $("#textTypeSelected").val().toUpperCase();
	textString = "abcdefghijklmnopqrstuvwxyz";
	
	if(textType == "SPACES" || textType == "BACKWARDS_SPACES"){
		maxCharsInLine = 52;
		$('#centerContent').removeClass('shortContent');
		$('#centerContent').addClass('longContent');
	} else {
		maxCharsInLine = 26;
		$('#centerContent').removeClass('longContent');
		$('#centerContent').addClass('shortContent');

	}
		
	if(textType == "SPACES"){
		textString = "a b c d e f g h i j k l m n o p q r s t u v w x y z";
	} else if(textType == "BACKWARDS"){
		textString = "zyxwvutsrqponmlkjihgfedcba";
	} else if(textType == "BACKWARDS_SPACES"){
		textString = "z y x w v u t s r q p o n m l k j i h g f e d c b a";
	} else if (textType == "RANDOM"){
		var newTextString = "";
		var ind = 0;
		
		while(textString.length > 0){
			ind = Math.floor(Math.random() * textString.length);
			newTextString += textString[ind];
			textString = textString.slice(0, ind) + textString.slice(ind+1);
		}
		textString = newTextString;
	}

}

function InitializeTimer_block(Time, TimerID){
    cursor = 24;        //first letter has "<span class="plainText">" before it
    localCursor = 24;
    cutoff = 0;
    lineNum = 0;
    blockDivText = "";
    sWatch = document.getElementById(TimerID);
    TotalSeconds = Time;
	totalDeletes = 0;
	pShiftWeight = 0;
	t[4]=t[3]=t[2]=t[1]=t[0]=0;
	t[7]=1;
	
    load();
    GetText();
    
    var tempStr = "";
    minCursorReplaceStart = 0;
	
	for(var ii = 0; ii <= maxCharsInLine; ii++){
		blockDivText += '<span class="plainText">' + textString.substring(ii, ii+1) + '</span>';
	}
  
    // place cursor on first letter
    blockDivText = blockDivText.replace(/plainText/i,"nxtLetter");

    blockDiv = $('#blockDivContainer').get();
    blockDiv = blockDiv[0];
    
    blockDiv.innerHTML = formatForInnerHTML(blockDivText);
}

function CreateTimer_block(event, TimerID, Time) {

    var functionKeyPressed = true;     // used to flag when function key (delete, shift...) is pressed
    if (TimeWasReset){  
		$('.topLeftLink').hide();		
    }
	
    if (TotalSeconds==undefined || TimeWasReset) {
        cursor = 24;        //first letter has "<span class="plainText">" before it
        localCursor = 24;
        TimeWasReset = false;
        RoundTime = Time;

        sWatch = document.getElementById(TimerID);
        TotalSeconds = Time;

        UpdateTimer()
		ss();
    }
    
    
	
    // This makes the box disabled when time is up (end if all text has been typed or after 30 mins (1800 secs)
    if (((lettersTypedCorrectly - totalDeletes + pShiftWeight) >= textString.length) || (TotalSeconds > 119900) ) {
        return false;
    }
    
    // Get text in RTE box
    var newRTEText = "";
    var RTELetter = blockDivText.substring(localCursor,(localCursor+1));
    
    var keyCode_entered = event.keyCode
    
	/* USED FOR QWERTY/DVORAK FUNCTIONALITY
    if($('#keyLayoutSelected').val().toUpperCase() == "DVORAK"){
        var key_entered = d_keycode.d_getValueByEvent(event);
    }
    else{       // default to QWERTY
	*/
    var key_entered = keycode.getValueByEvent(event);
    /*}
	*/

    // event.keyCode: 97-122 = a-z, 65-90 = A-Z, 8 = backspace, 32 = space
    if( key_entered == RTELetter){  /* characters match */
        functionKeyPressed = false;
        
        if(RTELetter == " "){
            newRTEText = blockDivText.substring(0,localCursor-11) + "goodSpace" 
                            + blockDivText.substring(localCursor-2);   
            wordsTyped++;
        }
        else{    
            newRTEText = blockDivText.substring(0,localCursor-11) + "goodEntry" 
                        + blockDivText.substring(localCursor-2);
        }
        cursor += 32;
        localCursor += 32;
        lettersTypedCorrectly += 1;
        totalEntriesTyped += 1;
        
        if(phaseShift != 0){
            phaseShift = 0;     // correct entry resets any possible phase shift
        }
        
    }      
    else if(((keyCode_entered==32)||(keyCode_entered==42)||(keyCode_entered==43)||(keyCode_entered==45)||
            ((keyCode_entered >= 47  ) && ( keyCode_entered <= 59))||
            ((keyCode_entered >= 61  ) && ( keyCode_entered <= 90))||
            ((keyCode_entered >= 96  ) && ( keyCode_entered <= 111))||
            ((keyCode_entered >= 186  ) && ( keyCode_entered <= 222))) &&
			((lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight) < textString.length)){
        functionKeyPressed = false;
        
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION START >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // // -------------------------------------------------------------------------------------
        // **FIRST** see if we are looking for a phase shift
        if(phaseShift != 0){
            if(phaseShift == 1){
                if(key_entered == blockDivText.substring(localCursor+32, localCursor+33)){
                    //we have a phase shift to the right
                    var shiftRight = true;
					pShiftWeight++;
                }
            }
            else{   //phase shift = -1
                if(key_entered == blockDivText.substring(localCursor-32, localCursor-31)){
                    var shiftLeft = true;
					pShiftWeight--;
                }
            }
            phaseShift = 0;     // phase shift has been corrected and realigned
            
        }
        // **THEN** see if we should look for a phase shift to the right (positive time)
        else if(key_entered == blockDivText.substring(localCursor+32, localCursor+33)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = 1;     //only set the flag if the user wants to do phase shift correction
            }
        }
        // see if we should look for a phase shift to the left (negative time)
        else if(key_entered == blockDivText.substring(localCursor-32, localCursor-31)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = -1;     //only set the flag if the user wants to do phase shift correction
            }
        }
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION END >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // -------------------------------------------------------------------------------------
            
        if(shiftRight){
            newRTEText = blockDivText.substring(0,localCursor-11) + "phaseRght" 
                         + blockDivText.substring(localCursor-2,localCursor+21) + "goodEntry"
                         + blockDivText.substring(localCursor+30);
            cursor +=32;
            localCursor +=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   Since we skipped a letter do not increment totalLettersTyped
            shiftRight = false;  // phase shift should have been successfully applied by now
        }
        else if(shiftLeft){
            newRTEText = blockDivText.substring(0,localCursor-43) + "phaseLeft" 
                         + blockDivText.substring(localCursor-34);
            cursor -=32;
            localCursor -=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   Removed because it was counting twice
            shiftLeft = false;  // phase shift should have been successfully applied by now
        }
        // we do not have a case of phase shift, so continue with bad input
        else if(blockDivText.substring(localCursor, localCursor+1) == " "){   /* this was RTELetter == " " */
            newRTEText = blockDivText.substring(0,localCursor-11) + "bad_Space" 
                         + blockDivText.substring(localCursor-2);
            wordsTyped++;
        }
        else{                     
            newRTEText = blockDivText.substring(0,localCursor-11) + "bad_Entry" 
                         + blockDivText.substring(localCursor-2);
        }
        
        cursor += 32;
        localCursor += 32;
        lettersTypedIncorrectly += 1;
        totalEntriesTyped += 1;
    }
    else if( event.keyCode == 8 ){      /*no match and it is a backspace, and its not beginning of text*/ 
        // backstep first
        cursor -= 32;
        functionKeyPressed = false;
		totalDeletes++;

        if(localCursor == 24){
            var newLocalCursor = blockDivText.length - 8;      // wrap localcursor around end of text
            
            if(blockDivText.substring(newLocalCursor-11,newLocalCursor-2) != "plainText" && cursor > 24){
                
                if(blockDivText.substring(newLocalCursor-11,newLocalCursor-2) == "bad_Space"){
                    badEntriesDeleted += 1;       // user has deleted bad space
                    totalEntriesTyped -= 1;
                    wordsTyped--;
                }
                else if(blockDivText.substring(newLocalCursor-11,newLocalCursor-2) == "goodSpace"){
                    wordsTyped--;
                }
                else if(blockDivText.substring(newLocalCursor-11,newLocalCursor-2) == "bad_Entry"
                        || blockDivText.substring(newLocalCursor-11,newLocalCursor-2) == "phaseLeft"
                        || blockDivText.substring(newLocalCursor-11,newLocalCursor-2) == "phaseRght"){
                        badEntriesDeleted += 1;       // user has deleted error
                        totalEntriesTyped -= 1;
                    }
                    
                blockDivText = blockDivText.substring(0,localCursor-11) + "plainText" + blockDivText.substring(localCursor-2);
                localCursor = newLocalCursor;
                }
             else{
                 
                 cursor += 32;
                 deletes--;
				 totalDeletes--;
             }
             newRTEText = blockDivText.substring(0,localCursor-11) + "nxtLetter" 
                          + blockDivText.substring(localCursor-2);
                    
        } 
        else{
            localCursor -= 32;
            if(blockDivText.substring(localCursor-11, localCursor-2) == "bad_Space" 
                || blockDivText.substring(localCursor-11, localCursor-2) == "bad_Entry"
                || blockDivText.substring(localCursor-11, localCursor-2) == "phaseLeft"){
                    badEntriesDeleted += 1;
                    totalEntriesTyped -= 1;
            }
            if(blockDivText.substring(localCursor-11,localCursor-2) == "bad_Space"){
                wordsTyped--;
            }
            if(blockDivText.substring(localCursor-11,localCursor-2) == "goodSpace"){
                wordsTyped--;
            }
            newRTEText = blockDivText.substring(0, (localCursor+21)%blockDivText.length) 
                         + "plainText" + blockDivText.substring((localCursor+30)%blockDivText.length);
                     
            newRTEText = newRTEText.substring(0,localCursor-11) + "nxtLetter" 
                        + newRTEText.substring(localCursor-2);
        }
        
        deletes++;
    }
    
    // move cursor to next letter
    if( localCursor > blockDivText.length){
        blockDivText = blockDivText.substring(0, 13)
                     + "nxtLetter" + blockDivText.substring(22);
    }
    else{
        newRTEText = newRTEText.substring(0, (localCursor-11))
                     + "nxtLetter" + newRTEText.substring(localCursor-2);
    }
    
    if(!functionKeyPressed){
		blockDivText = newRTEText;
        blockDiv.innerHTML = formatForInnerHTML(blockDivText);
    }
    
    if(deletes > 0 && event.keyCode != 8){
        deletes--;
    }

    return false;
}

function ss() {
	t[t[2]]=(new Date()).valueOf();
	t[2]=1-t[2];

	if (0==t[2]) {
		clearInterval(t[4]);
		t[3]+=t[1]-t[0];
		t[4]=t[1]=t[0]=0;
		Tick();
	} else {
		t[4]=setInterval(Tick, 30/*43*/);
	}
}

function load() {
	t[5]=new Date(1970, 1, 1, 0, 0, 0, 0).valueOf();
	t[6]=document.getElementById('sWatch');

	Tick();
}

function Tick() {

	if ( ((lettersTypedCorrectly + badEntriesDeleted - totalDeletes + pShiftWeight) >= textString.length) || 
	     (TotalSeconds > 119900) ){
        StopTime();
        return;
    }
	
	if (t[2]) t[1]=(new Date()).valueOf();
	t[6].value=format(t[3]+t[1]-t[0]);
	UpdateTimer();
	UpdateStats();
}
function format(ms) {
	// used to do a substr, but whoops, different browsers, different formats
	// so now, this ugly regex finds the time-of-day bit alone
	var d=new Date(ms+t[5]).toString()
		.replace(/.*([0-9][0-9]:[0-9][0-9]:[0-9][0-9]).*/, '$1');
	var x=String(ms%1000);
	while (x.length<3) x='0'+x;
	d+='.'+x;
	return d;
}

function r() {
	if (t[2]) ss();
	t[4]=t[3]=t[2]=t[1]=t[0]=0;
	Tick();
	t[7]=1;
}

function StopTime() {
	UpdateTimer();
	UpdateStats();
	TimeEnd();
}

function UpdateTimer() {
    TotalSeconds = t[3]+t[1]-t[0];
	var TimeStr = msecsToTimeStr(TotalSeconds);
	sWatch.innerHTML = TimeStr;
	timeDivider = (timeDivider + 1) % 20;
}

function updateGuiHS(){
	
	var wbkScoreElem = $('#wbkScore');
	var newHS = highScores[alphaTypeEnum[$("#textTypeSelected").val()] - 1];
	
	if(newHS == slowestTime){
		newHS = '-';
	} else {
		newHS = (newHS / 1000).toFixed(3);
	}
	
	$('#wbkScore').html(newHS);
}

function getHighScores(){
	var myJSONdata;
	$.post('/getData.php', { type: "tta", filter: $('#filterYourHS').val()},
		function(output){
			myJSONdata = $.parseJSON(output);
			var currDateTime = new Date(myJSONdata["currDateTime"].replace(/-/g,'/'));
			var loggedInUname = myJSONdata["loggedInUsername"];
			var inMaintenanceMode = myJSONdata["maint_mode"];
			var rhs = myJSONdata["allUsersResults"];
			var phss = myJSONdata["loggedInResults"];
			
			var rhsHTML = '';
			var phssHTML = '';
			
			if(inMaintenanceMode){
				rhsHTML = '<table><tr><td class="orangeCTA largeBtn" colspan="5"><div>Site is under maintenance</div><div>High scores are temporarily disabled and will not be saved!</div></td></tr></table>';
				phssHTML = '<table><tr><td class="orangeCTA largeBtn" colspan="4"><div>Site is under maintenance</div><div>High scores are temporarily disabled and will not be saved!</div></td></tr></table>';
			} else {
				rhsHTML = '<table><tr><td class="blueCTA largeBtn" colspan="5">Nobody has a recent high score yet!</td></tr></table>';
				phssHTML = '<table><tr><td class="blueCTA largeBtn" colspan="4">You do not have any scores yet!</td></tr></table>';
			}
			
			if(!inMaintenanceMode){
				if(rhs != null && rhs.length > 0){
					rhsHTML = '<table><tr><th>#</th><th>Name</th><th>Seconds</th><th>Type</th><th>Time Ago</th></tr>';
					var decoration = '';
					var adjustedName = '';
					
					for(var ii = 0; ii < rhs.length; ii++){
						if(rhs[ii].uname == loggedInUname)
							decoration = ' class="uname borderRad_4 orangeBkgd boldFont centerTxt"';
						else
							decoration = ' class="uname"';
						
						if(rhs[ii].uname.length > 13)
							adjustedName = rhs[ii].uname.substring(0, 11) + "...";
						else
							adjustedName = rhs[ii].uname;
						
						if(rhs[ii].ip > 0){
							premiumIconHTML = '<img class="circle-image-premium-icon" src="/images/crown.svg?v=13">';
							premiumClass = ' premium';
						} else {
							premiumIconHTML = '';
							premiumClass = '';
						}
						
						rhsHTML += '<tr><td>' + (ii + 1) + '</td><td' + decoration + ' title="' + rhs[ii].uname + '"><a href="/user/' + rhs[ii].uname + '/test-stats"><img class="circle-image tinyPic' + premiumClass + '" src="' + rhs[ii].profile_pic + '">' + premiumIconHTML + '<span class="adjName">' + adjustedName + '</span></a></td><td>' + (rhs[ii].val / 1000).toFixed(3).toString() + '</td><td>' + alphaTypeDecode[rhs[ii].type] + '</td><td class="convertToTimeAgo">' + rhs[ii].dt + '</td></tr>';
					}
					rhsHTML += '</table>';
				}
			}
			
			$('#alphaHS').html(rhsHTML);
			
			if(!inMaintenanceMode){
				if(phss == 'not logged in'){
					phssHTML = '<table><tr><td class="orangeCTA mediumBtn" colspan="4"><a href="https://www.learnspeedtyping.com/register.php">Register</a> or <a href="https://www.learnspeedtyping.com/login.php">Login</a> to keep track of your scores.</td></tr></table>';
				} else if(phss != null && phss.length > 0){
					phssHTML = '<table><tr><th>#</th><th>Seconds</th><th>Type</th><th>Time Ago</th></tr>';
					
					for(var ii = 0; ii < phss.length; ii++){
						phssHTML += '<tr><td>' + (ii + 1) + '</td><td>' + (phss[ii].val / 1000).toFixed(3).toString() + '</td><td>' + alphaTypeDecode[phss[ii].type] + '</td><td class="convertToTimeAgo">' + phss[ii].dt + '</td></tr>';
						if(highScores[(phss[ii].type - 1)] > phss[ii].val)
							highScores[(phss[ii].type - 1)] = phss[ii].val;
					}
					phssHTML += '</table>';
					
					// show delete scores button
					$('#clearDataBtn').show();
				}
			}
			
			$('#personalAlphaHS').html(phssHTML);
	
			$('.convertToTimeAgo').each(function(){
				 var earlyDate = new Date($(this).html().replace(/-/g,'/'));
				 var timeDiffString = readableTimeDifference(earlyDate, currDateTime);
				 $(this).html(timeDiffString);
				 if(timeDiffString.indexOf("mins") > 0 || timeDiffString.indexOf("secs") > 0)
					$(this).addClass("borderRad_4 dkBlueBkgd boldFont centerTxt");
			 });
			 
			 updateGuiHS();
	});
}


function Reset(ResetTime, TimerID) {
	$('#centerEntryModule').removeClass('passed failed');
	$('.topLeftLink').show();
	
    sWatch = document.getElementById(TimerID);
    TotalSeconds = ResetTime;
    lettersTypedCorrectly = 0;
    lettersTypedIncorrectly = 0;
    badEntriesDeleted = 0;
    totalEntriesTyped = 0;
    wordsTyped = 0;
	totalDeletes = 0;
	pShiftWeight = 0;
    
    UpdateTimer();   
    UpdateStats();
	clearInterval(t[4]);
	TimeIsStopped = true;
	getHighScores();
	

    InitializeTimer_block(ResetTime, TimerID);
    TimeWasReset = true;
	TimeIsStopped = false;
    
    $('.mainDivInputs').focus();    //assume user wants to type after clicking reset
    TimeBegin();
    t[6].value=format(0);
}

function TimeBegin(){
    $("#divCover").css("z-index", "0");
}

function TimeEnd(){
	var alphaType = $('#textTypeSelected').val();
	if(TotalSeconds > 0){
		$.post('/setData.php', { type: "tta", msecs: TotalSeconds, alpha_type: alphaType},
			function(output){
		});	
	}
	
	$("#divCover").css("z-index", "400");
    clearInterval(t[4]);
	
	timeDivider = 1		// set to guarantee last wpm calculation
    UpdateResults();
	if((highScores[alphaTypeEnum[alphaType] - 1] > TotalSeconds) && (TotalSeconds > 0)) {
		var sWatchElem = $('#sWatch');
		var wbkScoreElem = $('#wbkScore');
		highScores[alphaTypeEnum[alphaType] - 1] = TotalSeconds;
		wbkScoreElem.html((TotalSeconds/1000).toFixed(3));

		//cemElem.css("background","rgb(217, 233, 214)");
		//cemElem.css("border-color","rgb(118, 156, 111)");
		$('#centerEntryModule').removeClass('failed').addClass('passed');
		//wbkScoreElem.css("background","rgb(144, 179, 62)");
		//sWatchElem.css("background","rgb(144, 179, 62)");
	} else if(TotalSeconds > 0 && !TimeIsStopped) {
		//$("#sWatch").css("background-color","red");
		//$('#centerEntryModule').css("background","rgb(241, 232, 232)");
		//$('#centerEntryModule').css("border-color","red");
		$('#centerEntryModule').removeClass('passed').addClass('failed');
	}
	
    TimeIsStopped = true;
}

function formatForInnerHTML(StringToFormat){
    var StringToReturn = "";
    
    StringToFormat = StringToFormat.replace(/> </g, ">&nbsp;<");
    StringToReturn = '<div id="blockLine0" class="blockLines">' + StringToFormat + "</div>";

    return StringToReturn;
}

function setupFocus(){
    $('.mainDivInputs').bind('blur', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('click', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('mouseleave', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('mouseout', function() {
        this.focus();
    });
    
    $('.mainDivInputs').focus(function() {
        ApplyColorsFocused();
    });
    
    $('.mainDivInputs').blur(function() {
        ApplyColorsBlurred();
    });
}

keycode = {
    getKeyCode : function(e) {
        var keycode = null;
        if(window.event) {
            keycode = window.event.keyCode;
        }else if(e) {
            keycode = e.which;
        }
        return keycode;
    },
    getKeyCodeValue : function(keyCode, shiftKey) {
        shiftKey = shiftKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.modifiedByShift[keyCode];
        }else {
            value = this.keyCodeMap[keyCode];
        }
        return value;
    },
    getValueByEvent : function(e) {
        return this.getKeyCodeValue(this.getKeyCode(e), e.shiftKey);
    },
    keyCodeMap : {
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"capslock", 27:"escape", 32:" ", 33:"pageup",
        34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"+", 44:"printscreen", 45:"-", 46:"delete", 47:"/",
        48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:";",
        61:"=", 65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l",
        77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z",
        96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9",
        106: "*", 107:"+", 109:"-", 110:".", 111: "/",
        112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12",
        144:"numlock", 145:"scrolllock", 186:";", 187:"=", 188:",", 189:"-", 190:".", 191:"/", 192:"`", 219:"[", 220:"\\", 221:"]", 222:"'"
    },
    modifiedByShift : {
        32: " ", 65:"A", 66:"B", 67:"C", 68:"D", 69:"E", 70:"F", 71:"G", 72:"H", 73:"I", 74:"J", 75:"K", 76:"L",
        77:"M", 78:"N", 79:"O", 80:"P", 81:"Q", 82:"R", 83:"S", 84:"T", 85:"U", 86:"V", 87:"W", 88:"X", 89:"Y", 90:"Z", 192:"~", 48:")", 49:"!", 50:"@", 51:"#", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 109:"_", 61:"+",
        219:"{", 221:"}", 220:"|", 59:":", 222:"\"", 186:":", 187:"+", 188:"<", 189:"_", 190:">", 191:"?"/*,
        96:"insert", 97:"end", 98:"down", 99:"pagedown", 100:"left", 102:"right", 103:"home", 104:"up", 105:"pageup"*/
    }
};

d_keycode = {
    d_getKeyCode : function(e) {
        var d_keycode = null;
        if(window.event) {
            d_keycode = window.event.keyCode;
        }else if(e) {
            d_keycode = e.which;
        }
        return d_keycode;
    },
    d_getKeyCodeValue : function(d_keyCode, shiftKey) {
        shiftKey = shiftKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.d_modifiedByShift[d_keyCode];
        }else {
            value = this.d_keyCodeMap[d_keyCode];
        }
        return value;
    },
    d_getValueByEvent : function(e) {
        return this.d_getKeyCodeValue(this.d_getKeyCode(e), e.shiftKey);
    },
    d_keyCodeMap : {
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"capslock", 27:"escape", 32:" ", 33:"pageup",
        34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"}", 44:"printscreen", 45:"[", 46:"delete", 47:"z",
        48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:"s",
        61:"]", 65:"a", 66:"x", 67:"j", 68:"e", 69:".", 70:"u", 71:"i", 72:"d", 73:"c", 74:"h", 75:"t", 76:"n",
        77:"m", 78:"b", 79:"r", 80:"l", 81:"'", 82:"p", 83:"o", 84:"y", 85:"g", 86:"k", 87:",", 88:"q", 89:"f", 90:";",
        96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9",
        106: "*", 107:"}", 109:"[", 110:"v", 111: "z",
        112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12",
        144:"numlock", 145:"scrolllock", 186:"s", 187:"]", 188:"w", 189:"[", 190:"v", 191:"z", 192:"`", 219:"/", 220:"\\", 221:"=", 222:"-"
    },
    d_modifiedByShift : {
        32: " ", 65:"A", 66:"X", 67:"J", 68:"E", 69:">", 70:"U", 71:"I", 72:"D", 73:"C", 74:"H", 75:"T", 76:"N",
        77:"M", 78:"B", 79:"R", 80:"L", 81:"\"", 82:"P", 83:"O", 84:"Y", 85:"G", 86:"K", 87:"<", 88:"Q", 89:"F", 90:":", 192:"~", 48:")", 49:"!", 50:"@", 51:"#", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 109:"{", 61:"}",
        219:"?", 221:"+", 220:"|", 59:"S", 222:"_", 186:"S", 187:"}", 188:"W", 189:"{", 190:"V", 191:"Z"/*,
        96:"insert", 97:"end", 98:"down", 99:"pagedown", 100:"left", 102:"right", 103:"home", 104:"up", 105:"pageup"*/
    }
};

function UpdateStats(){    
    
	var wpmValue = $('#wpmValue').get();
	if((wpmValue.length != 0) && (timeDivider == 1)){
		wpmValue[0].innerHTML = getNetWPM();
	}
    
    var accuracyValue = $('#accuracyValue').get();
	if(accuracyValue.length != 0){
		accuracyValue[0].innerHTML = getAccuracy() + "<span> %</span>";
	}
	
	var pCompleteValue = $('#pCompleteValue').get();
	// only update percent complete if we are on typing tutor page and we are either 100% complete or it is an even second
	if((pCompleteValue.length != 0) && ( ((lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight) == textString.length) || ((TotalSeconds % 2) == 0) )){
		pCompleteValue[0].innerHTML = getPComplete() + "<span> %</span>";
	}
	
}

function UpdateResults(){    
	testCounter++;
}

function getKPM(){
    return Math.round(((lettersTypedCorrectly + lettersTypedIncorrectly)/(getTimeElapsed())) * 60);
}

function getTimeElapsed(){
    if(typeof Timer == "undefined"){
        return TotalSeconds;
    }
    return (RoundTime - TotalSeconds);
}

function getEPM(){
    return Math.round(((lettersTypedIncorrectly - getNumFixedMistakes()) / getTimeElapsed() ) * 60);
}


function getNetWPM(){    
    var netWPM = 0;
    var TotalLettersTyped = lettersTypedCorrectly + lettersTypedIncorrectly;
    
    if(getTimeElapsed() != 0) {
        netWPM = TotalLettersTyped / (getTimeElapsed()/1000);
        netWPM *= 12;
        netWPM = netWPM - getEPM();

        if(netWPM == "Infinity" || netWPM < 0){
            netWPM = 0;
        }
        netWPM = Math.round(netWPM);
    }
    
    return netWPM;
}

function getGrossWPM(){    
    var grossWPM = 0;
    var TotalLettersTyped = lettersTypedCorrectly + lettersTypedIncorrectly;
    
    if(getTimeElapsed() != 0) {
        grossWPM = TotalLettersTyped / getTimeElapsed();
        grossWPM *= 12;

        if(grossWPM == "Infinity" || grossWPM < 0){
            grossWPM = 0;
        }
        grossWPM = Math.round(grossWPM);
    }
    
    return grossWPM;
}

function getAccuracy(){    
    var Accuracy = 100;
    var TotalWordsTyped = (lettersTypedCorrectly + lettersTypedIncorrectly - badEntriesDeleted + getNumFixedMistakes());
    
    if(getTimeElapsed() != 0) {
        
        Accuracy = (lettersTypedCorrectly / TotalWordsTyped) * 100;
        Accuracy *= 10;
        Accuracy = Math.round(Accuracy);
        Accuracy /= 10;
        
        if(TotalWordsTyped == 0){
            Accuracy = 100;
        }
        if(Accuracy < 0){
            Accuracy = 0;
        }
    }
    
    return Accuracy;
}

function getTimeInMins(){
    timeInSecs = getTimeElapsed();
    timeInMins = timeInSecs / 60;
    timeInMins *= 10;
    timeInMins = Math.round(timeInMins);
    timeInMins /= 10;
    
    return timeInMins;
}

function getNumFixedMistakes(){
    
    var numFixMiss = 0;
    
    if(lettersTypedCorrectly >= badEntriesDeleted){
        numFixMiss = badEntriesDeleted;
    }
    else{
        numFixMiss = badEntriesDeleted - (badEntriesDeleted - lettersTypedCorrectly);
    }
    
    return numFixMiss;
}

function getPComplete(){

	var percentComplete;
	
	percentComplete = (lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight) / (textString.length - 1) * 100;
    percentComplete = Math.floor(percentComplete);		// floor instead of round so only when complete will it say 100%
	
	return percentComplete;
}
