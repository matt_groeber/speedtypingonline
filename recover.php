<?php 
$currPage = "recover";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$user = new User();

if($user->isLoggedIn() && !inMaintenanceMode()){
	Redirect::to('user/' . $user->data()->username . '/profile');
}

if(Input::exists('get') && !inMaintenanceMode()){
	
	$mode = Input::get('mode');

	if(Input::exists('post')){
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'email' => array(
				'name' => 'Your email address',
				'required' => true,
				'email' => true,
				'exists' => array('users', &$userFromEmail)
			)
		));
		
		if($validation->passed()){
			$ufe = $userFromEmail->first();
			
			if($mode == 'password'){
				$code = substr(md5(time()), 0, 32);
				
				$db = DB::getInstance();
				$dbResult = $db->get('pw_rec', array('user_id', '=', $ufe->id));
				
				if($dbResult->count() > 0){
					$dbResult = $db->update('pw_rec', $dbResult->first()->id, array('code' => $code, 'code_expiry' => date('Y-m-d H:i:s')));
				} else {
					$dbResult = $db->insert('pw_rec', array('user_id' => $ufe->id, 'code' => $code, 'code_expiry' => date('Y-m-d H:i:s')));
				}
				$pwResetLink = Config::get('constants/rootUrl') . "/changepassword.php?email=" . $ufe->email . "&emc=" . $code;
				$emailResult = $user->email($ufe->email, 'Password Reset - SpeedTypingOnline.com', 'Please click the following link:' . '<div style="border: 1px solid #FFD1AC; background: #FFF8EA; width: 120px; margin-top: 5px; border-radius: 8px;"><a href="' . $pwResetLink . '" style="width: 100%; height: 100%; display: inline-block; padding: 10px;">Reset Password</a></div><div style="margin-top:30px;"><p>Or copy and paste the following address into your internet browser:</p>' . $pwResetLink . '</div><div style="margin-top:30px;"><p>If you did not request this password reset you can safely ignore this email.</p></div><p>-SpeedTypingOnline</p>');
				
				
				if(!$emailResult[0] || !$dbResult){
					Session::flash('errored', 'Problems were encountered when processing your request', 'ERROR');
					Redirect::to('recover.php?mode=password');
				} 
			} else {
				$user->email($ufe->email, 'Your Recovered Username - SpeedTypingOnline.com', "Your username: \"" . $ufe->username . "\"<br /><br />-SpeedTypingOnline");
			}
					
			Session::flash('success', 'Thanks, we have emailed you', 'INFO');
			Redirect::to('recover.php?mode=' . $mode);
		} else {
			$errorsToPrint = '<ol><li>' . implode('</li><li>', $validation->errors()) . '</li></ol>';
		}
	}
}

$htmlTitle = ($mode === 'password') ? 'Recover Password' : 'Recover Username';
$htmlDescription = ($mode === 'password') ? 'Recover your lost password for your account.' : 'Recover your lost username for your account.';
$cssFiles = "beh";
if(inMaintenanceMode()){
	$noWidgets = true;
}
$addContainer = true;
include 'includes/overall/header.php';

if(inMaintenanceMode()){
	echo '<h2>Site is currently under Maintenance</h2>';
	echo '<h4>Access to user data is currently disabled as a major site-wide upgrade is currently underway.</h4>';
	echo '<h4>Please feel free to continue using the site as normal and the ability to recover your password/username will be available again shortly.</h4>';
	echo '<h4>Sorry for any inconvenience and thank you for your patience!</h4>';
	exitPHPwithFooter();
}

if(Session::exists('errored')){
	echo Session::flash('errored');
} else {
	echo Session::flash('success');
}
?>

<h2>Recover your <?php echo ($mode === 'password') ? 'Password' : 'Username';?></h2>
</div> <!-- <div id="heading"> -->


<div id="pageMain">
	<form action="" method="post">
		<div class="field">
			<label for="email">Your email address:</label>
			<input type="text" name="email">
		</div>
		
		<div id="formErrors"><?php echo (isset($errorsToPrint)) ? $errorsToPrint : '';?></div>
		
		<input class="mediumBtn flatBlueBtn" type="submit" value="Recover">
		
	</form>	
	<div class="note">
		Or recover your <?php echo ($mode === 'password') ? '<a href="/recover.php?mode=username">username</a>' : '<a href="/recover.php?mode=password">password</a>';?>
	</div>
</div>
<style>
	.note{margin-top:20px;}
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tools/1.2.7/jquery.tools.min.js" type="text/javascript"></script>
<?php 
include 'includes/overall/footer.php';
?>