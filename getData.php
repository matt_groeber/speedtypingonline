<?php
require_once 'core/init.php';

$db = DB::getInstance();
$user_obj = new User();
$loggedInID = ($user_obj->isLoggedIn()) ? $user_obj->data()->id : null;
$hs = new HighScore($loggedInID);
//$hs = new HighScore();

echo '{"currDateTime":' . json_encode(gmdate('Y-m-d H:i:s'));
if($loggedInID != null){
	echo ', "loggedInUsername":' . json_encode($user_obj->data()->username);
}
echo ', "maint_mode":' . json_encode(inMaintenanceMode());

//$_POST['type'] = "ol_ad";
//$_POST['sd'] = "2020-06-08";
//$_POST['type'] = 'ttt';
//$_POST['type'] = 'tta';
//$_POST['type'] = 'crd';
//$_POST['textType'] = "PANGRAMS";

// FAST FIRE TYPER
if($_POST['type'] == 'fft') {
	
	$allUsersResults = $hs->getGlobalFftHScores(null, 100);
	$allUsersCurrentResults = $hs->getGlobalFftHScores($_POST['SLB_letters'], 5);
	
	// rename keys to protect mysql column names: 'user_id, score, num_words, type, timestamp'
	$allUsersResults = array_map(function($tag){
		return array(
			'uname' => $tag->username,
			'profile_pic' => $tag->picture,
			'val' => $tag->score,
			'numWords' => $tag->num_words,
			'typeLetters' => $tag->type,
			'dt' => $tag->timestamp
		);
	}, $allUsersResults);
	
	$allUsersCurrentResults = array_map(function($tag){
		return array(
			'uname' => $tag->username,
			'profile_pic' => $tag->picture,
			'val' => $tag->score,
			'numWords' => $tag->num_words,
			'typeLetters' => $tag->type,
			'dt' => $tag->timestamp
		);
	}, $allUsersCurrentResults);
	
	$loggedInCurrentResult = 'not logged in';
	$loggedInResults = 'not logged in';
	
	if($loggedInID != null){
		
		$loggedInResults = $hs->getPersonalFftHScores($loggedInID);
		$loggedInCurrentResult = $hs->getPersonalFftHScores($loggedInID, $_POST['SLB_letters'], 1);
	
		// rename keys to protect mysql column names: 'user_id, score, num_words, type, timestamp'
		$loggedInResults = array_map(function($tag){
			return array(
				'val' => $tag->score,
				'numWords' => $tag->num_words,
				'typeLetters' => $tag->type,
				'dt' => $tag->timestamp
			);
		}, $loggedInResults);
		
		$loggedInCurrentResult = array_map(function($tag){
			return array(
				'val' => $tag->score,
				'numWords' => $tag->num_words,
				'typeLetters' => $tag->type,
				'dt' => $tag->timestamp
			);
		}, $loggedInCurrentResult);
	}

	
	echo ', "allUsersResults":' . json_encode($allUsersResults);
	echo ', "allUsersCurrentResults":' . json_encode($allUsersCurrentResults);
	echo ', "loggedInResults":' . json_encode($loggedInResults);
	echo ', "loggedInCurrentResult":' . json_encode($loggedInCurrentResult) . "}";
	exit();
} else if($_POST['type'] == 'tta'){		// TYPE THE ALPHABET
	
	$filterType = ($_POST['filter'] == "ALL") ? null : Config::get('constants/alphaTypeEnum')[$_POST['filter']];
	
	$allUsersResults = $hs->getGlobalAlphaHScores($filterType, 100, true, array('>', 755));
	
	// rename keys to protect mysql column names: 'user_id, score, num_words, type, timestamp'
	$allUsersResults = array_map(function($tag){
		return array(
			'uname' => $tag->username,
			'profile_pic' => $tag->picture,
			'val' => $tag->total_time,
			'type' => $tag->type,
			'dt' => $tag->timestamp,
			'ip' => $tag->premium
		);
	}, $allUsersResults);
	
	$loggedInResults = 'not logged in';
	
	if($loggedInID != null){
		
		$loggedInResults = $hs->getPersonalAlphaHScores($loggedInID, $filterType, 10);
	
		// rename keys to protect mysql column names: 'user_id, score, num_words, type, timestamp'
		$loggedInResults = array_map(function($tag){
			return array(
				'val' => $tag->total_time,
				'type' => $tag->type,
				'dt' => $tag->timestamp
			);
		}, $loggedInResults);
	}

	
	echo ', "allUsersResults":' . json_encode($allUsersResults);
	echo ', "loggedInResults":' . json_encode($loggedInResults) . "}";
	exit();
} else if($_POST['type'] == 'tt'){	// TYPING TEST
	if($loggedInID != null){
		$db_obj = $db->get('users_achieve', array('user_id', '=', $user_obj->data()->id), 'num_tests', null, null, array(1,0));
		
		$numTests = $db_obj->results()[0]->num_tests;
		
		$db_obj = $db->get('typing_tests', array('user_id', '=', $user_obj->data()->id), 'wpm, accuracy, timestamp', null, null, array($numTests,0));
		
		$results = $db_obj->results();
		$numResults = sizeof($results);
		$wpmArr = array();
		$accuracyArr = array();
		
		if($results != null){
			foreach($results as $entry){
				array_push($wpmArr, $entry->wpm);
				array_push($accuracyArr, $entry->accuracy);
			}
		} else {
			array_push($wpmArr, 0);
		}
		
		if($numResults == 0){
			$averageWPM = 0;
			$averageAcc = 0;
		} else {
			$averageWPM = round(array_sum($wpmArr) / $numResults, 1);
			$averageAcc = round(array_sum($accuracyArr) / $numResults, 1);
		}
		
	} else {
		$averageWPM = 'not logged in';
		$averageAcc = 'not logged in';
		$numTests = 'not logged in';
		$wpmArr = array('not logged in');
	}

	echo ', "averageWPM":' . json_encode($averageWPM);
	echo ', "averageAcc":' . json_encode($averageAcc);
	echo ', "numTTs":' . json_encode($numTests);
	echo ', "maxWPM":' . json_encode(max($wpmArr)) . "}";
	exit();
} else if($_POST['type'] == 'ttt'){	// TEXT TO TYPE
	$operator = '=';
	
	// get all texts except for custom
	if($_POST['textType'] == 'ENTRY' || $_POST['textType'] == ''){
		$operator = '!=';
		$_POST['textType'] = 'CUSTOM';
	}
	
	$whereArr = array('type', $operator, $_POST['textType']);
	
	$colToSort = 'title';
	if($_POST['textType'] == "NUMPAD"){
		$colToSort = 'id';
	} else {
		array_push($whereArr, 'text_type_id', '!=', '90');	// remove NUMPAD results if not Numpad keyboard
	}
	
	// get test text to type
	$db_obj = $db->get('test_texts', $whereArr, 'id, display_text, title, author, randomize_char, type', array($colToSort, 'ASC'), array('text_types', 'text_type_id', 'tid'), array(9999999,0));
	$textToType = $db_obj->results();
	
	$xmldata = simplexml_load_file(__DIR__ . '/resources/textToType.xml') or die("Failed to load");
	
	echo ', "textToType":[';
	
	$comma = ',';
	for ($ii = 0; $ii < sizeof($textToType); $ii++) {
		if($ii == (sizeof($textToType) - 1)){
			$comma = '';
		}
		$key = 'ID_' . $textToType[$ii]->id;
		$textToType[$ii]->text = $xmldata->$key->TEXT[0]->__toString();
		echo json_encode($textToType[$ii]);
		echo $comma;
	}
	echo ']}';
	exit();
} else if($_POST['type'] == 'tl'){	// TYPING LESSONS

	// see if a teacher is viewing their students page
	$classroomObj = new Classroom($loggedInID);
	
	if(isset($_POST['its'])){
		$studentUserId = $_POST['its'];
		$studentArr = $classroomObj->isUserTeacherOfStudent($studentUserId);
		
		if(!empty($studentArr)){
			// teacher is viewing their student's lessons
			$user_obj = new User($studentUserId);
		}
	}
	
	$retArr = getTutorData($user_obj->data(), $_POST['sd'], $_POST['ed']);
	$lessons = $retArr['lessons'];
	unset($retArr['lessons']);
	$lessonsWithoutFullHistory = $retArr['lessonsWithoutFullHistory'];
	unset($retArr['lessonsWithoutFullHistory']);
	$loggedInResults = $retArr;
	
	echo ', "lessonData":' . json_encode($lessons);
	echo ', "lessonsWithoutFullHistory":' . json_encode($lessonsWithoutFullHistory);
	echo ', "loggedInResults":' . json_encode($loggedInResults) . "}";
	exit();
} else if($_POST['type'] == 'crd'){	// CLASSROOM DATA
	echo ', "userIsStudent":' . json_encode($user_obj->isStudent());
	echo ', "userIsTeacher":' . json_encode($user_obj->isTeacher());

	$classroomObj = new Classroom($loggedInID);
	$classInfo = $classroomObj->getClasses();
	echo ', "classInfo":' . json_encode($classInfo) . "}";

	exit();
} else if($_POST['type'] == 'ol_ad'){	// ASSIGNMENT DATA
	$classroomObj = new Classroom($loggedInID);
			
	$assignments = $classroomObj->GetAssignments('all');
	
	echo ', "assignments":' . json_encode($assignments) . "}";

	exit();
} else {
	echo '}';
	exit();
}

?>