<?php 
$currPage = "typingGames";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$htmlTitle = "Typing Games";
$htmlDescription = "Typing games to make learning to type faster, easier, and more fun! How fast you can type the alphabet? Test vocab and typing skills with Fast Fire Typer.";
$cssFiles = "beq";
$noWidgets = true;
require_once 'includes/overall/header.php';

$user = new User();
$userData = $user->data();
$debugMode = $user->hasPermission('debug') || Config::get('constants/debugMode');
?>
<div id="container">
	<div id="heading">
		<h1>List of Typing Games</h1>
		<div id="headInfo">
			<p>Typing games are a great way to make learning to type easier and more fun. <br> Games discourage you from looking at the keyboard while encouraging you to spend more time practicing and improving your typing skills.</p>
		</div>
		<!--<h2>Typing games are a great way to make learning to type easier and more fun. <br /> Games discourage you from looking at the keyboard while encouraging you to spend more time practicing and improving your typing skills.</h2>-->
	</div>
<?php
	echo getAd("lead", $currPage, $debugMode, $user->isPremium());
?>
	
	<div class="grid grid-pad">
		<div class="col-1-6">
<?php
	echo getAd("sideSkyAd", null, $debugMode, $user->isPremium());
?>
		</div>
		<div class="col-5-6">
			<ul>
				<li id="tta">
					<a href="games/type-the-alphabet.php"><img src="images/TTA_icon_med.png" alt="Type the Alphabet Game Icon" title="Type the Alphabet Game Icon" width="300px"></a>
					<h2><a href="games/type-the-alphabet.php">Type the Alphabet</a></h2>
					<p><strong>Difficulty Level:</strong> Easy</p>
					<p><strong>Instructions:</strong> Challenge your best score as you race to type all letters A-Z as fast as you can! Try it once and you'll be hooked on this very fun and extremely addicting typing game.</p>
					<p>Despite it's simplicity, this game is a also a fantastic way to practice your typing skills, especially for all of the rarely-used keys. Use it as a fun way to take a break from the monotony of tedius typing lessons. It's also a great way to boost your typing speed.</p>
				</li>
				<li id="fft">
					<a href="games/fast-fire-typer.php"><img src="images/FFT_icon_med.png" alt="Fast Fire Typer Game Icon" title="Fast Fire Typer Game Icon" width="300px"></a>
					<h2><a href="games/fast-fire-typer.php">Fast Fire Typer</a></h2>
					<p><strong>Difficulty Level:</strong> Moderate to Difficult</p>
					<p><strong>Instructions:</strong> Type as many different words as you can that start with the random three starting letters, adding endings (-s, -ed, -ing, -er, -ers, -ness, -nesses) for more words. The faster you type and the longer the word, the more points you get. Each perfectly typed correct word increases the point multiplier, but one mistype resets it.</p>
					<p>This typing game is bit more challenging than most, but that's what makes it fun! Your score is not only exponentially affected by typing speed but also by typing accuracy. As a bonus, not only will you improve your typing skills but your vocabulary skills as well.</p>
				</li>
				<!--<li id="uts">
					<a href="http://www.ereflect.com/products/product-info.php?id=416&url=274"><img src="images/UTS_icon_med.png" alt="Ultimate Typing Software Game Icon" title="Ultimate Typing Software Game Icon" width="300px"></a>
					<h2><a href="http://www.ereflect.com/products/product-info.php?id=416&url=274">UltimateTyping Software</a></h2>
					<p>There are some seriously great typing games that come with this software. If you really want to improve your typing skills but can't seem to stick with boring typing lessons, then this is a great choice.</p>
				</li>-->
			</ul>
		</div>
</div>
	
<?php $currPage = "typingGames"; include 'includes/overall/footer.php'; ?>