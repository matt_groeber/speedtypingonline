<?php
$currPage = "register";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$currentUser = new User();

if($currentUser->isLoggedIn() && !inMaintenanceMode()){
	if(Cookie::exists(Config::get('remember/cookie_loginRedirect'))){
		$redirectURL = Cookie::get(Config::get('remember/cookie_loginRedirect'));
		Cookie::delete(Config::get('remember/cookie_loginRedirect'));
		Redirect::to($redirectURL);
	} else {
		Redirect::to('/user/' . $currentUser->data()->username . '/profile');
	}
}

$htmlTitle = "Create an Account";
$htmlDescription = "Create an account to track your progress, compete in the high scores, and get the most out of SpeedTypingOnline.com";
$cssFiles = "beh";
if(inMaintenanceMode()){
	$noWidgets = true;
}
$addContainer = true;
require_once 'includes/overall/header.php';

if(Input::exists('post') && !inMaintenanceMode()){
		
	if(Token::check(Input::get('token'))){
		$validate = new Validate();
		
		$validation = $validate->check($_POST, array(
			'username' => array(
				'name' => 'Username',
				'required' => true,
				'min' => 2,
				'max' => 20,
				'unique' => 'users',
				'notNumeric' => true,
				'noSpaces' => true,
				'noReservedSymbols' => true,
				'notDirty' => true
			),
			'email' => array(
				'name' => 'Your email address',
				'required' => true,
				'email' => true,
				'noSpaces' => true,
				'unique' => array('users', 'That email address has already been used')
			),
			'password' => array(
				'name' => 'Password',
				'required' => true,
				'min' => 6
			),
			'password_again' => array(
				'name' => 'Password again',
				'required' => true,
				'matches' => 'password'
			),
			'agree_to_terms' => array(
				'required' => true,
				'name' => 'Agreeing to the terms and privacy policy'
			)
		));
		
		
			
		if($validation->passed()){
			
			$user = new User();
			$newUsername = Input::get('username');
			$newEmailAddress = Input::get('email');
			$newEmailCode = md5(Input::get('username') + microtime());
			try {
				$user->create(array(
					'username' => $newUsername,
					'password' => password_hash(Input::get('password'), PASSWORD_DEFAULT),
					'email' => $newEmailAddress,
					'email_code'=> $newEmailCode,
					'joined' => gmdate('Y-m-d H:i:s'),
					'group' => 1,
					'premium' => 0,
					'picture' => get_gravatar($newEmailAddress),
					'flagged' => $validation->flagged() ? 1 : 0
				));
				
				$activateLink = Config::get('constants/rootUrl') . '/activate.php?email=' . $newEmailAddress . "&email_code=" . $newEmailCode;
				$emailRet = $user->email($newEmailAddress, "Welcome to SpeedTypingOnline!", '<h3 style="color: #00668C">Thank you for registering with SpeedTypingOnline.com!</h3>' . 
				'<p><div><b>Your Account Info:</b></div><div>Username: ' . $newUsername . '</div></p><p><div>Activate your account by clicking this link:</div>' . 
				'<div style="border: 1px solid #FFD1AC; background: #FFF8EA; max-width: 340px; margin-top: 5px; border-radius: 8px; text-align:center; font-size:1.2em; font-weight:bold;"><a href="' . $activateLink . '" style="width: 100%; height: 100%; display: inline-block; padding: 10px;">Activate my account!</a></div></p><p><div>Or copy and paste the following link into your browser address bar to activate your account:</div><div>' . $activateLink . '</div></p><p>Best Regards,</p><p style="color: #00668C">- SpeedTypingOnline</p>');
				if($emailRet[0] === true){
					echo '<h2>You have been registered!</h2><h3>Check your email for the activation link.</h3>';
					$accountRegistered = true;
				} else {
					$errorsToPrint = '<ol><li>' . $emailRet[1] . '</li></ol>';
				}
				
				
			} catch(Exception $e){
				die($e->getMessage());
			}
			
			
		} else {
			$errorsToPrint = '<ul><li>' . implode('</li><li>', $validation->errors()) . '</li></ul>';
		}
	}
}



if(inMaintenanceMode()){
	echo '<h2>Site is currently under Maintenance</h2>';
	echo '<h4>User registration is currently disabled as a major site-wide upgrade is currently underway.</h4>';
	echo '<h4>Please feel free to continue using the site and come back here soon to register your account.</h4>';
	echo '<h4>Sorry for any inconvenience and thank you for your patience!</h4>';
	exitPHPwithFooter();
}

if(!$accountRegistered){
?>

<h2>Create your Account</h2>
</div> <!-- <div id="heading"> -->


<div id="pageMain">
	<div class="responsiveCols">
		<div class="leftCol socialLogin">
			<?php include $_SERVER['DOCUMENT_ROOT'] . '/includes/login_google.php';?>
		</div>
		<div id="lineWrapper">
			<div class="line" style="position: absolute; left: 49%; top: 0; bottom: 0; width: 1px; background: #ddd; z-index: 1;"></div>
				<div class="wordwrapper" style="text-align: center; height: 12px; position: absolute; left: 0; right: 0; top: 50%; margin-top: -12px; z-index: 2;">
					<div class="word" style="color: #ccc; text-transform: uppercase; letter-spacing: 1px; padding: 3px; font: bold 12px arial,sans-serif; background: white;">or</div>                                        
			</div>
		</div>
		<div class="rightCol">
			<form action="" method="post">
				<div class="field alignInput">
					<label for="username">Username:</label>
					<input type="text" name="username" id="username" value="<?php echo escape(Input::get('username')) ?>" autocomplete="off">
				</div>
				
				<div class="field alignInput">
					<label for="email">Email:</label>
					<input type="text" name="email" id="email" value="<?php echo escape(Input::get('email')) ?>">
				</div>
				
				<div class="field alignInput">
					<label for="password">Password:</label>
					<input type="password" name="password" id="password">
				</div>
				
				<div class="field alignInput">
					<label for="password_again">Password again:</label>
					<input type="password" name="password_again" id="password_again">
				</div>
				
				<div class="field stoUICheckbox stoUI" title="You must agree to the terms before registering an account.">
					<label><input type="checkbox" name="agree_to_terms"> I agree to the <a href="/terms-of-service">Terms of Service</a> and <a href="/privacy-policy">Privacy Policy</a></label>
				</div>
				
				<div id="formErrors"><?php echo (isset($errorsToPrint)) ? $errorsToPrint : '';?></div>
				
				<input type="hidden" name="token" value="<?php echo Token::generateToken(Config::get('session/token_name')); ?>">
				<input type="hidden" id="tzOffset" name="tzOffset" value="">
				<input class="largeBtn flatBlueBtn" type="submit" value="Register">
				<div class="note">
					Already have an account? <a href="/login.php">Login Here</a>
				</div>
			</form>
		</div>
	</div>
	
</div>

<style>
	#lineWrapper{position: relative; width: 40px; height: 300px; margin: 0px 10px 10px;}
	.stoUICheckbox{margin-top:25px;}
	form div.alignInput label{width:135px;}
	
	@media only screen and (max-width: 1248px){
		aside{display:none;}
		#heading, #pageMain{margin-right:0px;}
	}
	
	@media only screen and (max-width: 870px){
		.socialLogin{display:block !important; margin-bottom:20px;}
		#lineWrapper{height:60px; display: block; margin: 0 auto 20px auto;}
	}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<?php
}
require_once 'includes/overall/footer.php';
?>