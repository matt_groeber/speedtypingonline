function UpdateStats(){    
    
	var wpmValue = $('#wpmValue').get();
	if(wpmValue.length != 0){
		wpmValue[0].innerHTML = getNetWPM();
	}
    
    var accuracyValue = $('#accuracyValue').get();
	if(accuracyValue.length != 0){
		accuracyValue[0].innerHTML = getAccuracy() + "<span> %</span>";
	}
	
	var pCompleteValue = $('#pCompleteValue').get();
	// only update percent complete if we are on typing tutor page and we are either 100% complete or it is an even second
	if((pCompleteValue.length != 0) && ( ((lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight) == (textString.length - 1)) || ((TotalSeconds % 2) == 0) )){
		pCompleteValue[0].innerHTML = getPComplete() + "<span> %</span>";
	}
	
}

function UpdateResults(){    

	testCounter++;
    var a = $("#WPM_Result").get();
	var b = getNetWPM();
	var c = getAccuracy();
	var d = lettersTypedCorrectly + lettersTypedIncorrectly;
	var e = getNumFixedMistakes();
	var f = getEPM();
	var g = getKPM();
	var h = getTimeInMins();
	var t = getTimeElapsed();
	
	a[0].innerHTML = b + "<span> WPM</span>";
    a = $("#rWPM_Result").get();
    a[0].innerHTML = getGrossWPM() + "<span> WPM</span>";
    a = $("#Accur_Result").get();
    a[0].innerHTML = c + "<span> %</span>";
    a = $("#TLT_Result").get();
    a[0].innerHTML = d;
    a = $("#LTC_Result").get();
    a[0].innerHTML = lettersTypedCorrectly;
    a = $("#nLTI_Result").get();
    a[0].innerHTML = lettersTypedIncorrectly;
    a = $("#CL_Result").get();
    a[0].innerHTML = e;
    a = $("#ER_Result").get();
    a[0].innerHTML = f;
    a = $("#KPM_Result").get();
    a[0].innerHTML = g + "<span> KPM</span>";
    a = $("#words_Result").get();
    a[0].innerHTML = wordsTyped;
    a = $("#time_Result").get();
    a[0].innerHTML = h + "<span> min</span>"
	
	$('#currentLessonWPM').html(b);
	$('#currentLessonAcc').html(c);
	
	setupCurrentLesson('#currentLessonWPM', b, tutorGlobals["defaultWPM"], tutorGlobals["targetWPM"]);
	setupCurrentLesson('#currentLessonAcc', c, tutorGlobals["defaultAccuracy"], tutorGlobals["targetAccuracy"]);
	 
	var completedLessonMod = lessonData[lessonIndex].id;
	//var completedLessonMod = lessonsWithoutFullHistory[lessonIndex].id;
	
	if(textSelection == "CUSTOM_LESSON"){
		completedLessonMod = textSelection;
		tutorGlobals["customLessonChars"] = $('#customLessonTA').val();
		tutorGlobals["customLessonWPM"] = b;
		tutorGlobals["customLessonAccuracy"] = c;
	}
	
	$.post('setData.php', { type: "lh", lesson_mod: completedLessonMod, wpm: b, accuracy: c, total_entries: d, incorrect_entries: lettersTypedIncorrectly, fixed_mistakes: e, total_time_secs: t, the_sa_id: sa_id},
		function(output){
			getLessonData(function(lessonData){
				myJSONdata = $.parseJSON(lessonData);
				
				setupLessonProfileBlurb(myJSONdata);
				$('#classicLessons').html(myJSONdata["loggedInResults"].classicLessonHTML);
				$('#advancedLessons').html(myJSONdata["loggedInResults"].advancedLessonHTML);
				$('#lessonMod_' + lessonId).addClass("current");
			});
	});
			
}

function getKPM(){
    return Math.round(((lettersTypedCorrectly + lettersTypedIncorrectly)/(getTimeElapsed())) * 60);
}

function getTimeElapsed(){
    if(typeof Timer == "undefined"){
        return TotalSeconds;
    }
    return (RoundTime - TotalSeconds);
}

function getEPM(){
    return Math.round(((lettersTypedIncorrectly - getNumFixedMistakes()) / getTimeElapsed() ) * 60);
}


function getNetWPM(){    
    var netWPM = 0;
    var TotalLettersTyped = lettersTypedCorrectly + lettersTypedIncorrectly;
    
    if(getTimeElapsed() != 0) {
        netWPM = TotalLettersTyped / getTimeElapsed();
        netWPM *= 12;
        netWPM = netWPM - getEPM();

        if(netWPM == "Infinity" || netWPM < 0){
            netWPM = 0;
        }
        netWPM = Math.round(netWPM);
    }
    
    return netWPM;
}

function getGrossWPM(){    
    var grossWPM = 0;
    var TotalLettersTyped = lettersTypedCorrectly + lettersTypedIncorrectly;
    
    if(getTimeElapsed() != 0) {
        grossWPM = TotalLettersTyped / getTimeElapsed();
        grossWPM *= 12;

        if(grossWPM == "Infinity" || grossWPM < 0){
            grossWPM = 0;
        }
        grossWPM = Math.round(grossWPM);
    }
    
    return grossWPM;
}

function getAccuracy(){    
    var Accuracy = 100;
    var TotalWordsTyped = (lettersTypedCorrectly + lettersTypedIncorrectly - badEntriesDeleted + getNumFixedMistakes());
    
    if(getTimeElapsed() != 0) {
        
        Accuracy = (lettersTypedCorrectly / TotalWordsTyped) * 100;
        Accuracy *= 10;
        Accuracy = Math.round(Accuracy);
        Accuracy /= 10;
        
        if(TotalWordsTyped == 0){
            Accuracy = 100;
        }
        if(Accuracy < 0){
            Accuracy = 0;
        }
    }
    
    return Accuracy;
}

function getTimeInMins(){
    timeInSecs = getTimeElapsed();
    
    timeInMins = timeInSecs / 60;
    timeInMins *= 10;
    timeInMins = Math.round(timeInMins);
    timeInMins /= 10;
    
    return timeInMins;
}

function getNumFixedMistakes(){
    
    var numFixMiss = 0;
    
    if(lettersTypedCorrectly >= badEntriesDeleted){
        numFixMiss = badEntriesDeleted;
    }
    else{
        numFixMiss = badEntriesDeleted - (badEntriesDeleted - lettersTypedCorrectly);
    }
    
    return numFixMiss;
}

function getPComplete(){

	var percentComplete;
	
	percentComplete = (lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight) / (textString.length - 1) * 100;
    percentComplete = Math.floor(percentComplete);		// floor instead of round so only when complete will it say 100%
	
	
	return percentComplete;
}