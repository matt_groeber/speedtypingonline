<?php 
$currPage = "typingRacer";
require_once '../core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$htmlTitle = "Typing Racer";
$htmlDescription = "Typing Racer";
$cssFiles = "behiu";
$fontPacks = "hib";
$noWidgets = true;
$fontAwesomeCDN = true;
$jQueryUI = true;
require_once '../includes/overall/header.php';

$db = DB::getInstance();
$hs = new HighScore();

$user_obj = new User();
$loggedInID = ($user_obj->isLoggedIn()) ? $user_obj->data()->id : null;
$userData = $user_obj->data();
$debugMode = $user_obj->hasPermission('debug') || Config::get('constants/debugMode');

$slowestTime = 99999999;
?>
<script>
<?php
echo "var alphaTypeEnum = " . json_encode(Config::get('constants/alphaTypeEnum')) . ";";
echo "var alphaTypeDecode = " . json_encode(Config::get('constants/alphaTypeDecode')) . ";";
echo 'var slowestTime = ' . $slowestTime;
?>
</script>

<div id="fbLikeDiv">
	<div class="fb-like" data-href="http://www.speedtypingonline.com/games/type-the-alphabet.php" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true" data-font="arial"></div>
</div>

<div id="topModule" class="grid grid-pad responsiveCols">
   <div id="trgInterfaceContainer" class="leftCol">
<?php
		echo getAd("lead", $currPage, $debugMode, $user->isPremium());
?>
		<div id="centerEntryModule" class="module">
			<div class="topLeftLink"><a href="#howTo"><i class="fa fa-question-circle" aria-hidden="true"></i></a></div>
			<!--<div id="trgLogo">Typing Racer</div>-->
			<h1>Typing Racer</h1>
			<div id="centerContent" class="longContent">
					<div id="divCover" style="z-index: 0;"></div>
					<div id="lineDivContainer" class="mainDivInputs longContent" tabindex="0" onmouseover="this.focus()" onmouseout="/*this.blur()*/" ></div>
			</div>	<!-- end centerContent -->
		</div>	
		<?php echo getAd("thirdBottom", $currPage, $debugMode, $user->isPremium()); ?>
    </div>
	<div class="rightCol">

<?php
		//echo getAd("rightSky", null, $debugMode, $user->isPremium());
?>
	</div>
</div>
<div>
	<span id="fuelLabel">Fuel:</span>
	<div id="fuelBar" class="stoProgBar dkBlue large"><span id="fuelProgBar" style="width:0%;">0%</span></div>
</div>
<div id="gameGraphics">
	<div id="roadLines"></div>
	<div id="theRoad"></div>
	<img src="/images/carFromTop_red.png" alt="race car" id="redCar">
</div>

<div id="buttonWrapper">
	<div id="resetBtnWrap">
		<div id="resetBtnDiv">
			<input type="button" name="ResetBtn" value="Reset" id="resetBtn" class="orangeButton" onclick="Reset(0,'sWatch')">
		</div>
	</div>
</div>

<div id="wrapStats">
	<div id="timerDiv">   
		<div id="timerText">Time</div>
		<div id="sWatch" class="statsText">00:00</div>
	</div>
	<div id='wpmDiv'>                
		<div id='wpmText'>Speed (WPM)</div>
		<div id='wpmValue' class="statsText">0</div>
	</div>
	<div id="scoreDiv"> 
		<div id="scoreText">Last Score</div>
		<div id="trgScore" class="statsText">-</div>
	</div>
</div>

<div id="highScoreContainer" class="grid grid-pad" style="display: none;">
   <div class="col-2-5 instructions" style="height: auto;">
     <div class="module instructWrap">
		<div class="instructDiv" style="height: auto;">
			<h3>Your Scores</h3>
			<div id="personalAlphaHS">
				<table>
					<tr>
						<th>#</th>
						<th>Seconds</th>
						<th>Type</th>
						<th>Time Ago</th>
					</tr>
				</table>
			</div>
		</div>
	 </div>
	</div>
	<div class="col-3-5 instructions" style="height: auto;">
		<div class="module instructWrap">
			<div class="instructDiv" style="height: auto;">
				<h3>Recent High Score Board</h3>
				<div id="alphaHS">
					<table>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Seconds</th>
							<th>Type</th>
							<th>Time Ago</th>
						</tr>
					</table>
				</div>
			</div>
		</div>
	 </div>
	 
	<div id="dialog-fuel" title="Enter the amount of fuel" style="display: none;">
		<p>Enter the amount of fuel you have (1 fuel = 1 second).</p>
		<span>Fuel: </span><input type="text" name="fuelAmt" id="fuelAmt">
	</div>
	<div id="dialog-score" title="Your Score" style="display: none;">
		<div id="dialogScoreDiv"><div id="dialogScoreLeftCol"></div><div id="dialogScoreRightCol"></div></div>
		<div id="dialogFinalScore"></div>
	</div>
</div>
<div id="howTo"></div>
<div class="stoFancyList">
			<h2>How To</h2>
			<div class="grid grid-pad">
				<div class="col-1-2">
					<ul class="arrowList">
						<li>
							<h3>Start Game</h3>
							<span>
								<p>Click on the text box with the orange border; the timer starts as soon as you begin typing!</p>
								<p>Click 'Reset' or press <span class="boldFont">'Shift-Return'</span> to restart the game.</p>
							</span>
						</li>
						<li>
							<h3>Change Fuel</h3>
							<span>
								<p>Click the <span class="boldFont">Fuel</span> bar to change the amount of fuel you start with.</p>
							</span>
						</li>
					</ul>
				</div>
				<div class="col-1-2">
					<ul class="arrowList">
						<li>
							<h3>Play</h3>
							<span>
								<p>Type the words to make the car drive. The faster you type the faster your car drives!</p>
								<p>Get points for words typed but type carefully as points are deducted for mistakes.</p>
							</span>
						</li>
					</ul>
				</div>
			</div>
			
			<div><h2 id="featuresDest">Game Features</h2></div>
			<div class="grid grid-pad">
				<div class="col-1-2">
					<ul class="checkList">
						<li><h3>Restart Game Hotkey</h3>
						<p>Keep your hands on the keyboard - use keyboard shortcut <span class="boldFont">"Shift-Return"</span> to restart game.</p></li>
						<li><h3>Racing Game Effects</h3>
						<p>Get your adrenaline pumping as you race to type faster to drive your car quicker.</p></li>
					</ul>
				</div>
				<div class="col-1-2">
					<ul class="checkList">
						<li><h3>Live Statistics</h3>
						<p>Shows your typing speed as you play.</p></li>
						<li><h3>High Scores</h3>
						<p>What's your highest score?</p></li>
					</ul>
				</div>
			</div>
			
<script src="../js/general.js" type="text/javascript"></script>
<script src="../js/trg.js?v=2" type="text/javascript"></script>
<script type="text/javascript">
 
	var currDateTime = new Date(<?php echo '"' . date('Y-m-d H:i:s') . '"';?>.replace(/-/g,'/'));
	var resizeTimer;
	
	$(function() {
		isResetting = true;
		UpdateFuelGauge();
		$('#fuelAmt').val(fuelTank);

		$(window).on('resize', function(e) {

		  clearTimeout(resizeTimer);
		  resizeTimer = setTimeout(function() {
			setMiddleCursor();
		  }, 250);

		});

		// ADD HOTKEY
		shortcut.add("Shift+return",function() {
			isResetting = true;
			StopAnimations();
			Reset(0, 'sWatch');
			},{
			"type":"keyup",
			"propagate":false,
			"disable_in_input":false,
			"target":document
			});

        $('#lineDivContainer').on("keydown", function(event){
            if($('#lineDivContainer').get()[0].innerHTML != ""){
                return CreateTimer_line(event, 'sWatch', 0/* Round Time */);
            }
        });
		
		$('#fuelBar').on("click", function(event){
			showDialog("dialog-fuel", "OK",280,590);
		});
    
        // call reset function every time text type drop down menu changes
        $("#textTypeSelected").change(function () {
            Reset(0,'sWatch');			
        });
	});
	
	function setMiddleCursor(){
		var windowWidth = $(window).width();
		
		if(windowWidth > 1850){
			middleCursor = 24;
		} else if(windowWidth > 1790) {
			middleCursor = 23;
		} else if(windowWidth > 1710) {
			middleCursor = 22;
		} else if(windowWidth > 1640) {
			middleCursor = 21;
		} else if(windowWidth > 1550) {
			middleCursor = 20;
		} else if(windowWidth > 1500) {
			middleCursor = 19;
		} else if(windowWidth > 1450) {
			middleCursor = 18;
		} else if(windowWidth > 1350) {
			middleCursor = 17;
		} else if(windowWidth > 1285) {
			middleCursor = 16;
		} else if(windowWidth > 1230) {
			middleCursor = 18;
		} else if(windowWidth > 1159) {
			middleCursor = 17;
		} else if(windowWidth > 1124) {
			middleCursor = 20;
		} else if(windowWidth > 1075) {
			middleCursor = 19;
		} else if(windowWidth > 1018) {
			middleCursor = 18;
		} else if(windowWidth > 967) {
			middleCursor = 17;
		} else if(windowWidth > 913) {
			middleCursor = 16;
		} else if(windowWidth > 880) {
			middleCursor = 19;
		} else if(windowWidth > 830) {
			middleCursor = 18;
		} else {
			middleCursor = 16;
		}
		Reset(0,'sWatch');
	}
	
	setMiddleCursor();
</script>
<?php 
include '../includes/overall/footer.php'; 
?>