<?php 
$currPage = "keyboardBasics";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$htmlTitle = "Keyboarding Basics";
$htmlDescription = "Learn the basics of touch-typing.";
$cssFiles = "aghfe";
$noWidgets = true;
$jQueryUI = true;
$fontAwesomeCDN = true;
$fontPacks = "d";
require_once 'includes/overall/header.php';

$user = new User();
$userData = $user->data();
$debugMode = $user->hasPermission('debug') || Config::get('constants/debugMode');

$retArr = getTutorData($userData);
$classicLessonsHTML = $retArr['classicLessonHTML'];
$advancedLessonsHTML = $retArr['advancedLessonHTML'];
$lessons = $retArr['lessons'];
$lessonsWithoutHistory = $retArr['lessonsWithoutFullHistory'];

$lessonWPMtarget = ($userData->tl_wpm_target == '') ? Config::get('constants/wpmLessonTarget') : $userData->tl_wpm_target;
$lessonAccTarget = ($userData->tl_accuracy_target == '') ? Config::get('constants/accLessonTarget') : trimTrailingZeroes($userData->tl_accuracy_target);

if(!$lessonModule = Input::get('mod')){
	$lessonModule = 83;
} 

if ($userData->input_mode == "INLINE"){
	$inputMode['BLOCK'] = ' style="display:none;"';
	$inputMode['INLINE'] = '';
	$divCoverStyle = ' style="z-index: 0; height:60px; margin-top:50px;"';
	$divGlowStyle = ' style="height:60px; width:668px; top:48px; left:0px;"';
	
} else {
	$inputMode['BLOCK'] = '';
	$inputMode['INLINE'] = ' style="display:none;"';
	$divCoverStyle = ' style="z-index: 0; height:160px; margin-top:-1px;"';
	$divGlowStyle = ' style="height:162px; width:668px; top:-2px; left:0px;"';
}

$lessonLength = array('', '', '', '', '');

if($userData->tl_length == 70){
	$lessonLength[0] = ' selected="selected"';
} else if($userData->tl_length == 280){
	$lessonLength[2] = ' selected="selected"';
} else if($userData->tl_length == 560){
	$lessonLength[3] = ' selected="selected"';
} else if($userData->tl_length == 1 && $debugMode){
	$lessonLength[4] = ' selected="selected"';
} else {
	// default normal length = 140
	$lessonLength[1] = ' selected="selected"';
}

?>
<div id="widthDiv"></div>
<div id="container">
	<div id="rightBar">
		<?php echo getAd("rightSky", $currPage, $debugMode, $user->isPremium());?>
	</div>
	
	<h1>Keyboarding Basics</h1>
	<div class="stoUI stoUIradio">
		<form id="LTradio">
			<label for="KB_lesson" class="radioLeft">
				<input type="radio" id="KB_lesson" name="keyHMtype" <?php echo ($lessonModule <= 83) ? 'checked' : ''; ?> value="Lesson">
				<span><h3 title="Start Here! Learn the basics of touch-typing!">Learn</h3></span>
			</label>
			<label for="KB_review" class="radioRight">
				<input type="radio" id="KB_review" name="keyHMtype" <?php echo ($lessonModule >= 84) ? 'checked' : ''; ?> value="Review">
				<span><h3 title="Test your touch-typing skills">Review</h3></span>
			</label>
		</form>
	</div>

	<div id="centerConsoleWrap" class="projectorScreen">
		<div id="prevBtn" class="pBtn"><i class="fa fa-arrow-left" aria-hidden="true"></i> Prev</div>
		<!--<div id="nextBtn" class="pBtn">Next <i class="fa fa-arrow-right" aria-hidden="true"></i></div>-->
		<div id="replayBtn" class="pBtn">Replay <i class="fa fa-refresh" aria-hidden="true"></i></div>
		<!--<div id="closeBtn" class="pBtn"><a href="#">Close <i class="fa fa-times" aria-hidden="true"></i></a></div>-->
		<div id="centerConsole">
			<div id="centerContent" style="display:none;">
				
				<div id='wrapStats'>
					<div id='timerDiv'>   
						<div id="timerText" class="statsText">Time</div>
						<div id='sWatch'>00:00</div>
					</div>
					<div id='wpmDiv'>                
						<div id='wpmText' class="statsText">Percent Complete</div>
						<div id='pCompleteValue'>0 <span>%</span></div>
					</div>
					<div id='accuracyDiv'> 
						<div id='accuracyText' class="statsText">Accuracy</div>
						<div id='accuracyValue'>100 <span>%</span></div>
					</div>
				</div>
				
				<div id="resultDivContainer" class="mainDivInputs" style="display: none; ">
					<div id="resultDiv">
						<div id="resultsTitle">
							<?php   echo $nameToPrint;?>Lesson Results
						</div>
							<div id="resultsCol1Wrap" class="iResults">

								<div id="WPM_Rtext" 
									 title="<strong>Raw speed with mistake penalties</strong><br /> Speed = Raw speed - Error Rate <br /> (each mistake left in per minute is -1 WPM)" 
									 class="resultStatRight">Speed: </div>
								<div id="Accur_Rtext" 
									 title="<strong>The percentage of correct <br />letters out of the total letters typed:</strong><br />Accuracy = (Correct Letters / Total Letters) x 100%" 
									 class="resultStatRight">Accuracy: </div>
							</div>
						<div id="resultsCol2Wrap" class="iResults">
							<div id="WPM_Result">0</div>
							<div id="Accur_Result">0</div>
						</div>
						<div id="resultsCol3Wrap" class="uiResults">
							<div id="LTC_Rtext" 
								 title="<strong>Total correct letters typed </strong><br />(even those deleted and re-entered)" 
								 class="resultStatLeft">Correct Letters: </div>
							<div id="nLTI_Rtext" 
								 title="<strong>Total letters skipped or typed incorrectly </strong><br />(even if deleted and corrected later)" 
								 class="resultStatLeft">Incorrect Letters: </div>
							<div id="MF_Rtext"
								 title="<strong>Number of incorrect entries that were fixed </strong><br /> (number of mistakes deleted and entered correctly)"
								 class="resultStatLeft">Fixed Mistakes: </div>
							<div id="TLT_Rtext"
								 title="<strong>All letters typed</strong> <br />Total Letters = Correct Letters + Incorrect Letters + Fixed Mistakes"
								 class="resultStatLeft">Total Letters: </div>
							<div id="ER_Rtext"
								 title="<strong>Number of incorrect letters<br /> (unfixed mistakes) per minute </strong><br /> (Error Rate = Incorrect Letters / Time in Minutes)"
								 class="resultStatLeft">Error Rate: </div>
							<div id="rWPM_Rtext"
								 title="<strong>Typing Speed before mistake penalties </strong><br /> Raw Speed = ( Total Letters / Time in minutes ) / 5 <br /> (where 5 = average length of a word)"
								 class="resultStatLeft">Raw Speed: </div>
							<div id="KPM_Rtext"
								 title="<strong>Number of keystrokes per minute </strong><br /> Key Speed = Total Letters / Time in minutes"
								 class="resultStatLeft">Key Speed: </div>
							<div id="words_Rtext"
								 title="<strong>Number of complete words typed </strong><br /> ('the' and 'messenger' are both one word)"
								 class="resultStatLeft">Complete Words: </div>
							<div id="time_Rtext"
								 title="<strong>Total time of round in seconds</strong><br /> (Time in minutes = Total Time / 60)"
								 class="resultStatLeft">Total Time: </div>
							 <div id="note_Rtext">*mouse over labels for more info</div>
						</div>
						<div id="resultsCol4Wrap" class="uiResults">
							<div id="LTC_Result">0</div>
							<div id="nLTI_Result">0</div>
							<div id="CL_Result">0</div>
							<div id="TLT_Result">0</div>
							<div id="ER_Result">0</div>
							<div id="rWPM_Result">0</div>
							<div id="KPM_Result">0</div>
							<div id="words_Result">0</div>
							<div id="time_Result">0</div>
						</div>
					</div>	<!-- #resultDiv -->
				</div>	<!-- resultDivContainer -->
			</div>	<!-- end centerContent -->

			<div id="KHWrap">
				<div id="LessonTutorialText"></div>
<?php 
	echo getAd("lead", $currPage, $debugMode, $user->isPremium());
	if(!$user->isLoggedIn()){
?>
				<div id="loginOrRegister">
					<div id="askToLogin" class="stoInfoBox" onclick="location.href='/login.php';">
						<span>
							<a href="/login.php">Login</a> or <a href="/register.php">Register</a>
						</span>
						<span> to track progress and set targets.</span>
					</div>
				</div>
<?php	
	}
?>
				<div id="kbWrap">
					
					<div id="kbDiv">
						<div id="keysFilled"></div>
						<div id="keysShading"></div>
						<div id="keyToPress" style="display: none; "></div>
						<div id="spaceToPress" style="display: none; "></div>
						<div id="L_ShiftToPress" style="display: none; "></div>
						<div id="R_ShiftToPress" style="display: none; "></div>
						<div id="kbLegend">Home/Starting Position Keys</div>
						<div id="keyArrow1"></div>
						<div id="keyArrow2"></div>
					</div>
				
				
				<div id="keyLabelWrap">
					<div id="klDiv">
						<div id="keysLabels"></div>
					</div>
				</div>
				
				</div>
				<span id="how_to"></span>
				<div id="HandWrap">
					<div id="handDiv">
						<div id="L_HandWrap">
							<div id="L_HandOutline"></div>
							<div id="L_FingerToPress"></div>
						</div>
						<div id="R_HandWrap">
							<div id="R_HandOutline"></div>
							<div id="R_FingerToPress"></div>
						</div>
						<div id="BigHands">
							<div id="BigHandsImg">
								<div id="BigHandClick"></div>
								<div id="BHC2"></div>
								<div id="clickImg"></div>
							</div>
						</div>
						<div id="findFJnub">
						</div>
					</div>
				</div>
				<div id="bottomTutText">Text</div>
			</div>
		
		</div> 	<!-- end centerConsole -->
		
	</div>	<!-- centerConsoleWrap -->
			
</div>
<div id="progressbar">
	<div id="progressbarValue">
		<span id="pbTextVal"></span>
	</div>
</div>
<script type="text/javascript">  
	var sa_id = <?php echo (Input::get('sa_id') == '') ? "null" : Input::get('sa_id'); ?>;
	var lessonModule = <?php echo $lessonModule;?>;
	var lessonData;
	var lessonsWithoutFullHistory;
</script>
<script src="js/jquery.hotkeys.js?v=1" type="text/javascript"></script>
<script src="js/learnKeyboard.js?v=24" type="text/javascript"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="/js/general.js"></script>
<script type="text/javascript">  
	// handles all popup window links (used for Social Media Sharing)
    function popup(mylink, windowname){
        if (! window.focus)return true;
        var href;
        if (typeof(mylink) == 'string')
            href=mylink;
        else
            href=mylink.href;
        window.open(href, windowname, 'width=626,height=436,scrollbars=yes');
        return false;
    };
	
	$(document).ready(function() {
		StartLessonTutorial();
	
		// ADD HOTKEY
		shortcut.add("Shift+return",function() {
			Reset(0, 'sWatch');
			},{
			"type":"keyup",
			"propagate":false,
			"disable_in_input":false,
			"target":document
			});
		
		resizeElements();

		
		$(document).on('keydown', function(e){
			if($('#centerConsoleWrap').hasClass("projectorScreen") && !tutIsPlaying){
				
				var correctKey = false;
				var mode;
				if(gRandKeyMode){
					mode = "RandKey";
					
				} else if($('#KB_review').prop("checked")){
					mode = "Review";
				} else {
					mode = "Lesson";
				}
				
				if(e.key.toLowerCase() == gAnswerKey.toLowerCase()){
					correctKey = true;
				}
				
				if(correctKey){
					
					$('#BigHandsImg').finish();
					$('#LessonTutorialText').finish();
					
					DoProgressBar(tutStep,500);
					
					tutStep++;
					RunAnimationStep(tutStep);
					
					
				}
				
				return false;
			}
		});
		
		$('#replayBtn').click(function(){
			RunAnimationStep(tutStep);
		});
		
		$('#nextBtn').click(function(){
			tutIsPlaying = false;
			DoProgressBar(tutStep,50);
			
			tutStep++;
			RunAnimationStep(tutStep);
		});
		
		$('#prevBtn').click(function(){
			tutIsPlaying = false;
			if(tutStep > 1)
				tutStep--;
				
			DoProgressBar(tutStep-1,50);
			
			if (tutStep == 0){
				$('#pbTextVal').html("");
			}
			RunAnimationStep(tutStep);
		});
		/*
		$('#closeBtn').click(function(){
			$('#centerConsoleWrap').removeClass("projectorScreen");
			ResetLessonTutorial();
		});
		*/
		/*$('#LTradio').on('change', function(){
			if($('input[name="keyHMtype"]:checked', '#LTradio').val() == "Learn"){
				$('#progressbarValue').css("width","0%");
				$('#pbTextVal').html("");
			} else {
				$('#progressbarValue').css("width","0%");
				$('#pbTextVal').html("");
			}
			
			StartLessonTutorial();
		});*/
		
		$('#KB_lesson').click(function(){
			StartLessonTutorial();
		});
		
		$('#KB_review').click(function(){
			StartLessonTutorial();
		});
		
        
	});
	
	
	// impose maxlength attribute on text areas
	window.onload = function() { 
		var txts = document.getElementsByTagName('TEXTAREA') 

		  for(var i = 0, l = txts.length; i < l; i++) {
			if(/^[0-9]+$/.test(txts[i].getAttribute("maxlength"))) { 
			  var func = function() { 
				var len = parseInt(this.getAttribute("maxlength"), 10); 

				if(this.value.length > len) { 
				  this.value = this.value.substr(0, len); 
				  alert('Maximum length exceeded: ' + len); 
				  return false; 
				} 
			  }

			  txts[i].onkeyup = func;
			  txts[i].onblur = func;
			} 
		  } 
	}	

  $(window).resize(function() {
	resizeElements();
  });
  
	$('#prevLessonLink').click(function(){
		if(lessonModule > 0){
			$('#lessonMod_' + (lessonModule + 1)).removeClass("current");
			lessonModule -= 1;
		}
		Reset(0,'sWatch');
	});
  
	$('#nextLessonLink').click(function(){
		if(lessonModule < lessonData.length){
			$('#lessonMod_' + (lessonModule + 1)).removeClass("current");
			lessonModule += 1;
		}
		
		Reset(0,'sWatch');
	});
	
	$('div[id^="microlessonMod_"] a').removeAttr("onclick");
	
	$('#centerContent').on("mouseleave", function(){
		var colArr = [];
		var valArr = [];
		var doReset = false;
		
		if(colArr.length > 0){
			$.post('setData.php', { type: "us", col: colArr, val: valArr},
				function(output){
			});
		}
	});
  
  function resizeElements(){
	var pageWidth = $('#widthDiv').width();
  		if(pageWidth < 1367 && pageWidth >= 1121){
			// deal with small page
			$('#centerConsole').removeClass();
			 $('#centerConsole').addClass("scale75p");
			 //$("#bottomInfo").css("top","-300px");
		} else if(pageWidth < 1121){
			$('#centerConsole').removeClass();
			$('#centerConsole').addClass("scale90p");
		} else if(pageWidth >= 1367 && pageWidth < 1601){
			// deal with large page
			$('#centerConsole').removeClass();
			$('#centerConsole').addClass("scale90p");
			$("#bottomInfo").css("top","-205px");
		}
		else if(pageWidth > 2400){
			// deal with large page
			$('#centerConsole').removeClass();
			$('#centerConsole').addClass("scale115p");
			$("#bottomInfo").css("top","-40px");
		}
		else{
			$('#centerConsole').removeClass();
			$("#bottomInfo").css("top","-150px");
		}
  }
  
    
</script>

<style>
html{height:100%;}
h1{font-size:2.2em; border-radius: 8px; color: #ffffff; background: #04516b; border: 2px solid #00202b; left: 0; display: inline-block; position: absolute; top: 55px;
padding: 10px 40px 10px 120px; border-left: none; border-bottom-left-radius: 0;}
body{background: #2f2f2f; height:100%;margin:0;}
#container {height:100%;}
#centerConsoleWrap {display:table; position: absolute; top: 0; left: 0; height:100%; width:100%; margin-top:20px;}
#centerConsole{display:table-cell; vertical-align:middle; width:auto; left:0; margin:0;}
#HandWrap{height:170px;}
#LessonTutorialText,#bottomTutText{font-family:'Roboto', 'Open Sans';}

.stoUIradio .radioLeft input:checked + span{background: #6acff1; color: #05516a; border: 1px solid #abe8fb;}
.stoUIradio .radioLeft input:checked:hover + span{color:#0d6e8e;}
.stoUIradio .radioRight input:checked + span{background: #ec682c; color:white; border: 1px solid #ffa278;}
.stoUIradio .radioRight input:hover + span{color:#e0853b;}
.stoUIradio .radioRight input:checked:hover + span{color:#ffd8c7;}
.stoUIradio{left: 40px; display: inline-block; position: absolute; top: 115px; z-index:106; font-size:2em; font-family: 'Open Sans', Arial, sans-serif;}
.stoUIradio label{width:210px;}
#LTradio label.radioLeft{filter: drop-shadow(-4px 4px 3px black); z-index: 99;}
#LTradio label.radioRight{filter: drop-shadow(-6px 4px 2px black);}
.stoUIradio .radioLeft span, .stoUIradio .radioRight span{background: #899ca2; color: #ecfbff; border: 1px solid #074352; margin-right: 10px;}
/*.stoUIradio .radioRight span{background: #b38a60; color: #ffd1a3; border: 1px solid #8c653d;}*/

#progressbar{position: absolute;
    left: 0;
    width: 100%;
	/*background: #032c3b;*/
    height: 3vh;
    bottom: 0;
    display: inline-block;
    margin-bottom: 40px;
    border-bottom: none;
border-top: none;}
#progressbarValue{width:0%; height: 30px;
text-align: right;
    background: #6acef1;
    border-right: 4px solid #279dc5;
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;}
#pbTextVal{
    margin-right:22px;
    bottom: 0;
    font-weight: 800;
    color: #032c3b;
    font-size: 1.5em;
    line-height: 30px;
}


.thickText {font-weight:800;}
.thinText {font-weight:100;}
.ltOrangeText {color: #fbbe7d; font-weight:800; filter: drop-shadow(2px 3px 0px #000);}
.ltBlueText {color: #7cdcfd; font-weight:600; filter: drop-shadow(3px 4px 0px #000);}
a.ltOrangeText{text-decoration: underline;}
a.ltOrangeText:hover{color:#ff963c;}

@media only screen and (max-width: 1367px) {
  h3{font-size:1.1em;}
  .stoUIradio label{width:180px;}
  #KHWrap{margin-top: 120px;}
}
</style>

<?php 
$microFooter = true;
include 'includes/overall/footer.php'; 
?>
