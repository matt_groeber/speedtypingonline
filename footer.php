<footer>
<div>
	<div id="footGrid1" class="grid grid-pad">
	   <div class="col-1-3">
		<h3>Typing Software</h3>
		<p>Looking to improve your typing skills even more?</p>
		<p>I recommend trying <a href="http://products.ereflect.com/aff_c?offer_id=26&aff_id=1040">Typesy Typing Software</a> by eReflect.</p>
		<p>Use the link above now for a special 20% discount!</p>
		<p>(Valid until December 25, 2015)</p>
	   </div>
	   <div id="centFoot" class="col-1-3">
		<h3>Top Pages to Visit</h3>
		<ol id="topPagesOL">
			<li><a href="http://www.speedtypingonline.com/typing-test">Typing Test</a></li>
			<li><a href="http://www.speedtypingonline.com/typing-tutor">Typing Tutor</a></li>
			<li><a href="http://www.speedtypingonline.com/typing-games">Typing Games</a></li>
		</ol>
	   </div>
	   <div class="col-1-3">
		<h3>Contact Me</h3>
		<p>Have a burning question?</p>
		<p>Want a new feature added?</p>
		<p>Send an email to:</p> 
		<a href="mailto:mattgroeber@speedtypingonline.com">mattgroeber@speedtypingonline.com</a>
		<p>Thanks!</p>
	   </div>
	</div>
	<div id="footer">Copyright &copy; <a href="http://www.mattgroeber.com">Matt Groeber</a> - 2014</div>
</div>
</footer>