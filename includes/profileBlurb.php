<?php

if($user->isLoggedIn()){
	// setup generally used variables
	$userFullNameWithParenthesis = $user->fullName();
	if($userFullNameWithParenthesis != ''){$userFullNameWithParenthesis = ' (' . $userFullNameWithParenthesis . ')';}
	
	$userProfileImage = '';
	if($userData->picture != ''){
		$userProfileImage = '<img class="circle-image" alt="Small User Profile Picture" src="' . $userData->picture . '">';
	}
	
	echo '<div id="loginInfo">';
		
	// deal with each page
	if($currPage == "typingTutor"){
		echo '<div id="loggedInInfo">';
		echo '<div id="infoBlurbName" class="stoInfoTitle"><a href="/user/' . $userData->username . '/lessons">View Typing Lessons (' . $userProfileImage . $userData->username . ')</a></div>';
		
		$wpmStatus = ' class="warn"';
		$accStatus = ' class="warn"';
		$wpmTarget = isset($wpmTarget) ? $wpmTarget : $userData->tl_wpm_target;
		$accTarget = isset($accTarget) ? $accTarget : $userData->tl_accuracy_target;
		if($lessonsWithoutHistory[$lessonIndex]->wpm >= $wpmTarget){	$wpmStatus = ' class="success"';}
		if($lessonsWithoutHistory[$lessonIndex]->accuracy >= $accTarget){ $accStatus = ' class="success"'; }

		echo '<div id="userTypingInfoBlurb">';
		echo '<div id="lessonsPassedDiv" style="display:none;"><span>Lessons Passed</span><div><span id="numLessonsPassed" class="bigTxt boldFont">' . $retArr['advancedPassedCount'] . '</span></div></div>';
		echo '<div id="lessonScoreDiv"><span>Lesson Score</span><div><span id="currentLessonScore" class="bigTxt boldFont">' . round(($lessonsWithoutHistory[$lessonIndex]->wpm / $lessonWPMtarget) * 100) . '</span></div></div>';
		//var_dump(round(($lessons[$lessonIndex]->wpm / $lessonWPMtarget) * 100));
		//exit();
		echo '<div id="infoBlurbAve"><span>Current</span><div><div class="first"><span id="currentLessonWPM" class="bigTxt boldFont">' . $lessonsWithoutHistory[$lessonIndex]->wpm . '</span><span class="note"> wpm</span></div><div class="last"><span id="currentLessonAcc" class="bigTxt boldFont">' . $lessonsWithoutHistory[$lessonIndex]->accuracy . '</span><span> %</span></div></div></div>';
		echo '<div><span class="boldFont">Next Lesson</span><div class="micro"><span id="recommendedNextLesson"></span></div></div>';
//var_dump($lessonsWithoutHistory);
//echo 'got here';
//var_dump($lessonsWithoutHistory[$lessonIndex]);
//exit();
		
	} else if($currPage == 'typingTest'){		
		echo '<div id="loggedInInfo" class="stoInfoBox">';
		echo '<div id="infoBlurbName" class="stoInfoTitle"><a href="/user/' . $userData->username . '/test-stats">View Test Stats (' . $userProfileImage . $userData->username . ')</a></div>';
		
		echo '<div id="userTypingInfoBlurb">';
		echo '<div><span>Fastest</span><div><span id="maxWPM_data" class="bigTxt boldFont">' . max($wpmArr) . '</span><span class="note"> wpm</span></div></div>';
		echo '<div id="infoBlurbAve"><span>Average</span><div><div><span id="aveWPM_data" class="bigTxt boldFont">' . $averageWPM . '</span><span class="note"> wpm</span></div><div><span id="aveAcc_data" class="bigTxt boldFont">' . $averageAcc . '%</span></div></div></div>';
		echo '<div><span>Tests Taken</span><div><span id="numTests_data" class="bigTxt boldFont">' . sizeof($results) . '</span></div></div>';
		
	}
	echo '</div></div></div>';
}
	?>