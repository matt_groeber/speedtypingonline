<?php
// use $nExt as '_w' or '_m' to separate when this is used as a widget
// and when this is used by the dropdown menu

echo Session::flash('failed' . $nExt);

if(Input::exists() && !inMaintenanceMode()){

	if(Token::check(Input::get('token'), 'token_name' . $nExt)){
		$captchaObj = new Captcha();
		$captchaPass = $captchaObj->passedCaptcha(Input::get('humanAns' . $nExt), 'captcha_ans' . $nExt);
		
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'username' . $nExt => array(
				'name' => 'Username',
				'required' => true
			),
			'password' . $nExt => array(
				'name' => 'Password',
				'required' => true,
				'equalsTrue' => array($captchaPass, 'You answered incorrectly. Please try again.')
			)
		));
		
		if($validation->passed()){
			
			$user = new User();
			
			$remember = (Input::get('remember' . $nExt) === 'on') ? true : false;
			$login = $user->login(Input::get('username' . $nExt), Input::get('password' . $nExt), $remember);
			
			
			if($login === true){
				Redirect::to($_SERVER['REQUEST_URI']);		// redirect to current page
			} else if($login == 'Inactive User'){
				Session::flash('failed' . $nExt, 'Your account is currently inactive! <br /><br />Check your email for your activation link if you have just registered.', 'ERROR');
				Redirect::to($_SERVER['REQUEST_URI']);		// redirect to current page
			} else {
				$captchaObj->addFailedLoginAttempt();
				Session::flash('failed' . $nExt, '<h3>Login failed</h3><p>Make sure you are using the correct username and password.</p>', 'ERROR');
				Redirect::to($_SERVER['REQUEST_URI']);		// redirect to current page
			}
		} else {
			$errorsToPrint = '<ol><li>' . implode('</li><li>', $validation->errors()) . '</li></ol>';
		}
	}
}
?>
<div class="widget">
<?php if(inMaintenanceMode()){?>
	<h3>Site is under maintenance</h3>
	<div class="inner message">
		<p>User login and data tracking is currently disabled as a major site-wide upgrade is currently underway.</p>
		<p>Please continue to use the site as normal but be aware that test, lesson, and game high score data will not be saved.</p>
		<p>Sorry for any inconvience and thank you for your patience!</p>
	</div>
</div>
<?php
		return;
	}
?>
	<h3>Log In/Register</h3>
	<div class="inner">		
		<form method="post">
			<?php
			if (isset($errorsToPrint))
			{ ?>
				<div id="formErrors">
				<?php echo $errorsToPrint;?>
				</div><?php
			}?>
			<div class="field alignInput">
				<label for="<?php echo 'username' . $nExt;?>">Username:</label>
				<input type="text" name="<?php echo 'username' . $nExt;?>" id="<?php echo 'username' . $nExt;?>" autocomplete="off">
			</div>
			<div class="field alignInput">
				<label for="<?php echo 'password' . $nExt;?>">Password:</label>
				<input type="password" name="<?php echo 'password' . $nExt;?>" id="<?php echo 'password' . $nExt;?>" autocomplete="off">
			</div>
			<?php
			$captchaObj = new Captcha();
			if($captchaObj->doCaptcha()){?>
				<div class="field">
					<?php
						$captcha = $captchaObj->getCaptcha();
						
						// display question to user as part of form
						echo '<p><span class="boldFont">Question</span>: ' . htmlentities($captcha['q']) . '</p>';

						// store answers in session
						Session::put('captcha_ans' . $nExt,$captcha['a']);
					?>
					<label for="<?php echo 'humanAns' . $nExt;?>" class="boldFont">Answer:</label>
					<input type="text" name="<?php echo 'humanAns' . $nExt;?>" id="<?php echo 'humanAns' . $nExt;?>" autocomplete="off">
				</div>
			<?php
			} else {
				Session::delete('captcha_ans' . $nExt);
			}
			?>
			<div class="cf">
				<div class="field checkbox left">
					<label for="<?php echo 'remember' . $nExt;?>">
						<input type="checkbox" name="<?php echo 'remember' . $nExt;?>" id="<?php echo 'remember' . $nExt;?>"> Remember me
					</label>
				</div>
				<input class="smallBtn flatBlueBtn right" type="submit" value="Log in">
			</div>
			<input type="hidden" name="token" value="<?php echo Token::generateToken(Config::get('session/token_name' . $nExt)); ?>">
			<div class="note hasNormalLinks">Recover your <a href="/recover.php?mode=username">username</a> or <a href="/recover.php?mode=password">password</a></div>
		</form>
		<div class="socialLogin">
			<?php include $_SERVER['DOCUMENT_ROOT'] . '/includes/login_google.php'; ?>
		</div>
		<div><a href="/register.php" class="largeBtn submitButtonCarrot fullWidth">Create an Account</a></div>
	</div>
</div>