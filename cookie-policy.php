<?php 
$currPage = "cookiePolicy";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$htmlTitle = "Cookie Policy";
$htmlDescription = "Cookie Policy for SpeedTypingOnline";
$noWidgets = true;
$cssFiles = "beh";
$addContainer = true;
require_once 'includes/overall/header.php';
?>
	<div class="readingLineWidth">
		<h1>Cookie Policy</h1>
	</div>
	</div> <!-- <div id="heading"> -->

	<div id="pageMain" class="readingLineWidth">
		<p>
			This Cookie Policy explains how SpeedTypingOnline.com ("Company", "we", "us", and "our") uses cookies and similar technologies to recognize you when you visit our websites at (http://speedtypingonline.com). It explains what these technologies are and why we use them, as well as your rights to control our use of them.
		</p>
		<p>
			In some cases we may use cookies to collect personal information, or that becomes personal information if we combine it with other information.
		</p>
		<ul>
			<li>
				<h2>What are cookies?</h2>
				<p>
					Cookies are small data files that are placed on your computer or mobile device when you visit a website. Cookies are widely used by website owners in order to make their websites work, or to work more efficiently, as well as to provide reporting information.
				</p>
				<p>
					Cookies set by the website owner (speedtypingonline.com) are called "first party cookies". Cookies set by parties other than the website owner are called "third party cookies". Third party cookies enable third party features or functionality to be provided on or through the website (e.g. like advertising, interactive content and analytics). The parties that set these third party cookies can recognize your computer both when it visits the website in question and also when it visits certain other websites.
				</p>
			</li>
			<li>
				<h2>Why do we use cookies?</h2>
				<p>
					We use first and third party cookies for several reasons. Some cookies are required for technical reasons in order for our Websites to operate, and we refer to these as "essential" or "strictly necessary" cookies. Other cookies also enable us to track and target the interests of our users to enhance the experience on our Online Properties. Third parties serve cookies through our Websites for advertising, analytics and other purposes. This is described in more detail below.
				</p>
				<p>
					The specific types of first and third party cookies served through our Websites and the purposes they perform are described below (please note that the specific cookies served may vary depending on the specific Online Properties you visit):
				</p>
			</li>
			<li>
				<h2>How can I control cookies?</h2>
				<p>
					You have the right to decide whether to accept or reject cookies. You can exercise your cookie rights by setting your preferences in the Cookie Consent Manager. The Cookie Consent Manager allows you to select which categories of cookies you accept or reject. Essential cookies cannot be rejected as they are strictly necessary to provide you with services.
				</p>
				<p>
					The Cookie Consent Manager can be found in the notification banner and on our website. If you choose to reject cookies, you may still use our website though your access to some functionality and areas of our website may be restricted. You may also set or amend your web browser controls to accept or refuse cookies. As the means by which you can refuse cookies through your web browser controls vary from browser-to-browser, you should visit your browser's help menu for more information.
				</p>
				<p>
					In addition, most advertising networks offer you a way to opt out of targeted advertising. If you would like to find out more information, please visit <a href="http://www.aboutads.info/choices/" target="_blank">http://www.aboutads.info/choices/</a> or <a data-fr-linked="true" href="http://www.youronlinechoices.com" target="_blank">http://www.youronlinechoices.com</a>.
				</p>
				<p>
					The specific types of first and third party cookies served through our Websites and the purposes they perform are described in the table below (please note that the specific cookies served may vary depending on the specific Online Properties you visit):
				</p>
				
				<h3>Speed Typing Online Cookies</h3>
				<table class="displayLines">
					<tr>
						<th>Name</th>
						<th>Expires</th>
						<th>Source</th>
						<th>Description</th>
					</tr>
					<tr>
						<td>hash</td>
						<td>1 Week</td>
						<td width="29%">Speed Typing Online</td>
						<td>"Remember me" functionality to remember login details between browser sessions</td>
					</tr>
					<tr>
						<td>PHPSESSID</td>
						<td>Session</td>
						<td>Speed Typing Online</td>
						<td>PHP session tracker required for account login and typing data tracking</td>
					</tr>
				</table>
				<h3>CloudFlare CDN Cookies</h3>
				<table class="displayLines">
					<tr>
						<th>Name</th>
						<th>Expires</th>
						<th>Source</th>
						<th>Description</th>
					</tr>
					<tr>
						<td>__cfduid</td>
						<td>1 Year</td>
						<td width="29%">Speed Typing Online</td>
						<td>Identify trusted web traffic</td>
					</tr>
				</table>
				<h3>Google Analytics Cookies</h3>
				<table class="displayLines">
					<tr>
						<th>Name</th>
						<th>Expires</th>
						<th>Source</th>
						<th>Description</th>
					</tr>
					<tr>
						<td>_ga</td>
						<td>2 Yrs</td>
						<td>Google Analytics</td>
						<td>Used to distinguish users</td>
					</tr>
					<tr>
						<td>_gat</td>
						<td>1 Min</td>
						<td>Google Analytics</td>
						<td>Used to throttle request rate</td>
					</tr>
					<tr>
						<td>_gid</td>
						<td>24 Hrs</td>
						<td>Google Analytics</td>
						<td>Used to distinguish users</td>
					</tr>
					<tr>
						<td>__utma</td>
						<td>2 Yrs</td>
						<td>Google Analytics</td>
						<td>Used to distinguish users and sessions.</td>
					</tr>
				</table>
				<h3>Google Adsense Cookies</h3>
				<table class="displayLines">
					<tr>
						<th>Name</th>
						<th>Expires</th>
						<th>Source</th>
						<th>Description</th>
					</tr>
					<tr>
						<td>__gads</td>
						<td>Various</td>
						<td>Google Advertising</td>
						<td>DoubleClick for Publishers service from Google. It serves purposes such as measuring interactions with the ads and preventing the same ads from being shown to you too many times.</td>
					</tr>
				</table>
				<p>
					Google/Doubleclick : We partner with Google/Doubleclick to display the advertising that pays for this website. For that to work effectively they will use cookies to store data and access data about your use of this website.  You can learn more about how Google use this data and how to opt out if you choose here:  <a href="http://www.google.com/intl/en/policies/privacy/partners/">http://www.google.com/intl/en/policies/privacy/partners/</a>
				</p>
			</li>
			<li>
				<h2>What about other tracking technologies, like web beacons?</h2>
				<p>
					Cookies are not the only way to recognize or track visitors to a website. We may use other, similar technologies from time to time, like web beacons (sometimes called "tracking pixels" or "clear gifs"). These are tiny graphics files that contain a unique identifier that enable us to recognize when someone has visited our Websites or opened an e-mail including them. This allows us, for example, to monitor the traffic patterns of users from one page within a website to another, to deliver or communicate with cookies, to understand whether you have come to the website from an online advertisement displayed on a third-party website, to improve site performance, and to measure the success of e-mail marketing campaigns. In many instances, these technologies are reliant on cookies to function properly, and so declining cookies will impair their functioning.
				</p>
			</li>
			<li>
				<h2>Do you serve targeted advertising?</h2>
				<p>
					Third parties may serve cookies on your computer or mobile device to serve advertising through our Websites. These companies may use information about your visits to this and other websites in order to provide relevant advertisements about goods and services that you may be interested in. They may also employ technology that is used to measure the effectiveness of advertisements. This can be accomplished by them using cookies or web beacons to collect information about your visits to this and other sites in order to provide relevant advertisements about goods and services of potential interest to you. The information collected through this process does not enable us or them to identify your name, contact details or other details that directly identify you unless you choose to provide these.
				</p>
			</li>
			<li>
				<h2>How often will you update this Cookie Policy?</h2>
				<p>
					We may update this Cookie Policy from time to time in order to reflect, for example, changes to the cookies we use or for other operational, legal or regulatory reasons. Please therefore re-visit this Cookie Policy regularly to stay informed about our use of cookies and related technologies.
				</p>
				<p>
					The date at the bottom of this Cookie Policy indicates when it was last updated.
				</p>
			</li>
			<li>
				<h2>Where can I get further information?</h2>
				<p>
					If you have any questions about our use of cookies or other technologies, please email us at <a href="mailto:admin@speedtypingonline.com">admin@speedtypingonline.com</a>.
				</p>
			</li>
		</ul>
	
		<div>
			<h3>Last updated February 2020</h3>
		</div>
	</div>

<?php
require_once 'includes/overall/footer.php';
?>
</body>
    
</html>