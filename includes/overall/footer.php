</div>
	
	<?php include $_SERVER['DOCUMENT_ROOT'] . '/includes/footer.php';?>
	
<script>
	// GDPR Cookie Popup
	function GetCookie(e){for(var o=e+"=",t=o.length,n=document.cookie.length,i=0;i<n;){var c=i+t;if(document.cookie.substring(i,c)==o)return"here";if(0==(i=document.cookie.indexOf(" ",i)+1))break}return null}function testFirstCookie(){var e=(new Date).getTimezoneOffset();-180<=e&&e<=0&&null==GetCookie("cookieCompliancyAccepted")&&$("#cookieConsentPopup").fadeIn(400)}$(document).ready(function(){$("#cookieButton").click(function(){console.log("Understood");var e=new Date;e=new Date(e.getTime()+7776e6),document.cookie="cookieCompliancyAccepted=here; expires="+e+";path=/",$("#cookieConsentPopup").hide(400)}),testFirstCookie()});

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-21029737-2', 'auto');
  ga('send', 'pageview'<?php echo ', \'' . escape($_SERVER['PHP_SELF']) . '\'';?>);

</script>

<?php $currPage="";?>
</body>
</html>