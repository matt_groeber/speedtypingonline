<?php

class GoogleAuth{
	
	protected $client, $ticket;
	
	public function __construct(Google_Client $googleClient = null){
	
		$this->client = $googleClient;
		$this->ticket = array();
		
		if($this->client){
			$protocol = $_SERVER['HTTPS'] ? 'https' : 'http';
			$this->client->setClientId('109468782129-teo1kktc78efh8emt9litnig8hj8epvf.apps.googleusercontent.com');
			$this->client->setClientSecret('VZqdLNF1fKeN0PQr4XJjHi6G');
			$this->client->setRedirectUri($protocol . '://' . $_SERVER['SERVER_NAME'] . '/includes/login_google.php');
			$this->client->setApplicationName('Speed Typing Online');
			$this->client->addScope('email');
			$this->client->addScope('profile');
			
		}
	}
	
	public function isLoggedIn(){
		return (isset($_SESSION['access_token']) && $_SESSION['access_token']);
	}
	
	public function getAuthUrl(){
		return $this->client->createAuthUrl();
	}
	
	public function checkRedirectCode(){
		if(isset($_GET['code'])){
			$this->client->authenticate($_GET['code']);
			$token = $this->client->getAccessToken();
			$this->setToken($token);
			
			$this->ticket = $this->client->verifyIdToken($token['id_token']);
			return $this;
		} 
		
		return false;
	}
	
	public function setToken($token){
		$_SESSION['access_token'] = $token;
		$this->client->setAccessToken($token);
	}
	
	public function getUserPayload(){
		return $this->ticket;
	}
	
	public function logout(){
		unset($_SESSION['access_token']);
	}
}


?>