<?php 
$currPage = "home";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];


$htmlTitle = "Home";
$htmlDescription = "SpeedTypingOnline Home Page";
$cssFiles = "opbe";
$noWidgets = true;
$addContainer = false;
$jQuery = true;
require_once 'includes/overall/header.php';
?>
	<div id="body-area">
		<div class="container">
			
<div id="featured" class="et_slider_auto et_slider_speed_7000">
	<div id="slides" class="clearfix">
			<div class="slide" style="z-index: 2; display: none; opacity: 0;">
				<div class="description">
					<h2><a href="/typing-test">Free, Online Typing Test</a></h2>
									<p>How fast do you type?</p>
								</div> <!-- .description -->

				<a href="/typing-test"><img src="/images/typingTest-banner-8.png" alt="Typing Test" width="960" height="295"></a>
			</div> <!-- .slide -->
			<div class="slide et-active-slide" style="z-index: 1; display: block; opacity: 1;">
				<div class="description">
					<h2><a href="/typing-tutor">Free, Online Typing Tutor</a></h2>
									<p>Make learning to type easy and fun!</p>
								</div> <!-- .description -->

				<a href="/typing-tutor"><img src="/images/typing-tutor-banner-1.png" alt="Typing Tutor" width="960" height="295"></a>
			</div> <!-- .slide -->
			<div class="slide" style="z-index: 1; display: none; opacity: 0;">
				<div class="description">
					<h2><a href="/games/fast-fire-typer.php">Fast Fire Typer</a></h2>
									<p>Not your average, boring typing game...</p>
								</div> <!-- .description -->

				<a href="/games/fast-fire-typer.php"><img src="/images/FFT-banner-8.png" alt="Fast Fire Typer" width="960" height="295"></a>
			</div> <!-- .slide -->
	</div> <!-- #slides -->
<div class="et-slider-arrows"><a class="et-arrow-prev" href="/home/#">Previous</a><a class="et-arrow-next" href="/home/#">Next</a></div></div> <!-- #featured -->

<!-- <div id="leadAd">
	<h3>Recommended Typing Software</h3>
	<a href="http://www.ereflect.com/products/product-info.php?id=416&url=272" target="_blank"><img border="0" src="http://www.ereflect.com/products/banners/970x90_05_2013.png" width="970" height="90"></a>
</div>-->

<div id="section-area">

	<div id="services" class="clearfix">
<div class="service"><h3>Testimonials</h3>
		<div class="et-testimonial-box">
			<div class="et-testimonial-author-info clearfix">
				
				<span class="t-author">M. Jones</span>
				<span class="t-position">User</span>
			</div>

			<div class="et-testimonial clearfix">
				I've been using your online typing practice program on-and-off for what must be almost a year now, and I must say it is probably the best out there. Particularly the option to type custom texts, instead of the same stale extracts that most sites are limited to, and the option to type for longer than most allow. I don't think I'd have made anywhere near the progress I have without your program - convenience and detailed feedback are vital in practicing a new skill. So, THANK YOU is what I'm trying to say I suppose!
			</div>

		    <div class="t-bottom-arrow"></div>
			
		</div>
		<img id="test_profile1" src="images/testProfileYngMale_269x220.jpg" alt="test" height="165" width="205" style="float:right; border: 5px solid rgb(70, 163, 236); -webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px;">
</div> <!-- end .service -->
<div class="service"><h3>Updates</h3>
		<ul>
			<li>
				<p><b><a href="/goPremium">Premium Membership</a> is here!</b></p>
				<p>Pay by the month or year to get an ad-free experience.</p>
				<p>Unlimited test history - plus more perks to come!</p>
			</li>
			<li>
				<p><b>Classroom update for schools!</b></p>
				<p>Accounts can now be designated as teacher or student</p>
				<p>Teachers can create classrooms to better track their students and give assignments.</p>
			</li>
			<li>
				<p><b><a href="/keyboard-basics">Keyboard Basics</a></b> - A new way to start learning touch-typing!</p>
				<p>Perfect start for those just beginning their typing journey.</p>
			</li>
			<li>
				<p>Numpad (10-key) keyboard support added</p>
			</li>
			<li>
				<p>AZERTY keyboard support added</p>
			</li>
		</ul>
</div> <!-- end .service -->
</div> <!-- #services -->

	<a id="callout" href="/typing-test">
		<strong>How fast do you <span>type</span>?</strong>
		<span>Take the Test!</span>
	</a>

</div> <!-- #section-area -->

		</div> <!-- .container -->
		
			<!-- GOOGLE CUSTOM SEARCH -->

			<!-- <form action="http://www.google.com" id="cse-search-box">
			  <div>
				<input type="hidden" name="cx" value="partner-pub-2169602241028960:8907766604" />
				<input type="hidden" name="ie" value="UTF-8" />
				<input type="text" name="q" size="55" />
				<input type="submit" name="sa" value="Search" />
			  </div>
			</form>-->

			<!--<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=cse-search-box&amp;lang=en"></script>-->
			<!-- GOOGLE CUSTOM SEARCH -->

	</div> <!-- #body-area -->

	<?php $currPage = "home";include 'includes/footer.php'; ?>

	<script type="text/javascript" src="js/admin-bar.min.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.fitvids.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var et_custom = {"mobile_nav_text":"Navigation Menu"};
/* ]]> */
</script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/devicepx-jetpack.js"></script>
<script type="text/javascript" src="js/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="js/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var et_ptemplates_strings = {"captcha":"Captcha","fill":"Fill","field":"field","invalid":"Invalid email"};
/* ]]> */
</script>
<script type="text/javascript" src="js/et-ptemplates-frontend.js"></script>
	<script type="text/javascript">
		(function() {
			var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

			request = true;

			b[c] = b[c].replace( rcs, ' ' );
			b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
		}());
	</script>
		
<div id="fancybox-tmp"></div><div id="fancybox-loading"><div></div></div><div id="fancybox-overlay"></div><div id="fancybox-wrap"><div id="fancybox-outer"><div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div><div id="fancybox-content"></div><a id="fancybox-close"></a><div id="fancybox-title"></div><a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a><a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a></div></div>

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21029737-2']);
  /*_gaq.push(['_setCustomVar', 1, 'TC', testCounter.toString(), 3]);*/
  _gaq.push(['_trackPageview', '/home']);
  _gaq.push(['_trackPageLoadTime']);
  
  /*_gaq.push(['_trackEvent',
      'Testing', // category of activity
      'Custom Var', // Action
   ]);*/

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</body></html>