<?php
$currPage = "updateProfile";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];
$db = DB::getInstance();
$username = Input::get('user');

// first see if no username has been passed in
if($username === ''){
	Redirect::to('typing-test.php');
}

$user = new User();
$userData = $user->data();
$subStatus = array('NOT_A_SUB', '');
$debugMode = $user->hasPermission('debug') || Config::get('constants/debugMode');

if($userData->username != $username){
	
	// restrict user profile to be viewed by only the actual user when logged in (aside from exception where user has changed their username and submitted)
	if(!Session::exists('updated') && !Session::exists('failed')){
		if(inMaintenanceMode()){
			require_once 'includes/overall/header.php';
			echo '<h2>Site is under Maintenance</h2><h4>User profile pages will be available shortly.</h4><h4>Please continue to use the site as normal but be aware that test, lesson, and game high score data will not be saved.</h4><h4>Sorry for any inconvenience and thank you for your patience!</h4>';
			exitPHPwithFooter();
		} else {
			Redirect::to('/typing-test.php');
		}
	} else {
		Redirect::to('/user/' . $username . '/test-stats');
	}
}

$userToProfile = $user;
$onPersonalProfile = true;

if($userData->allow_email == 1){
	$allowEmailSetting = ' checked';
} else {
	$allowEmailSetting = '';
}

$keyboardType=array("QWERTY"=>'',"QWERTY_UK"=>'',"DVORAK"=>'',"COLEMAK"=>'',"COLEMAK_UK"=>'',"AZERTY"=>'',"QWERTZ"=>'',"QWERTZ_SF"=>'',"NUMPAD"=>'');
$textType=array();


$db_obj = $db->get('`text_types`', array('tid', '>', '0'), 'type, display_text');
$textTypes = $db_obj->results();

$textType = array();
foreach($textTypes as &$tt){
	$textType[$tt->type] = '';
}

if($userData->tt_text_type == ''){
	$textType['ENTRY'] = ' selected="selected"';
} else {
	$textType[$userData->tt_text_type] = ' selected="selected"';
}

// change text type dropdown based on selected keyboard layout
$ttdl_normal = "<option value=\"ENTRY\"" . $textType["ENTRY"] . ">All Texts</option>" . 
				"<option value=\"SUMMARY\"" . $textType["SUMMARY"] . ">Book Summaries</option>" . 
				"<option value=\"BOOK\"" . $textType["BOOK"] . ">Books</option>" . 
				"<option value=\"SHORT_STORY\"" . $textType["SHORT_STORY"] . ">Short Stories</option>" . 
				"<option value=\"FABLES\"" . $textType["FABLES"] . ">Fables (Easy-To-Type)</option>" . 
				"<option value=\"LYRICS\"" . $textType["LYRICS"] . ">Song Lyrics</option>" . 
				"<option value=\"DATA_ENTRY\"" . $textType["DATA_ENTRY"] . ">Data Entry</option>" . 
				"<option value=\"PANGRAMS\"" . $textType["PANGRAMS"] . ">Random Pangrams</option>" . 
				"<option value=\"FACTS\"" . $textType["FACTS"] . ">Random Facts</option>" . 
				"<option value=\"RANDOM_WORDS\"" . $textType["RANDOM_WORDS"] . ">Random Words</option>" . 
				"<option value=\"PROVERBS\"" . $textType["PROVERBS"] . ">Proverbs</option>" . 
				"<option value=\"CUSTOM\"" . $textType["CUSTOM"] . ">Custom</option>";
							
$ttdl_numpad = 	"<option value=\"NUMPAD\"" . $textType["NUMPAD"] . ">Numpad (10-key)</option>" . 
				"<option value=\"CUSTOM\"" . $textType["CUSTOM"] . ">Custom</option>";

if($userData->tt_double_spacing == 1){
	$doubleSpacingSetting = ' checked';
} else {
	$doubleSpacingSetting = '';
}

if($userData->tt_color_text == 1){
	$textColoringSetting = ' checked';
} else {
	$textColoringSetting = '';
}

if($userData->tt_phase_shift == 1){
	$phaseShiftSetting = ' checked';
} else {
	$phaseShiftSetting = '';
}

$keyboardArr = Config::get('constants/keyboards');

if($userData->tt_keyboard_layout == ''){
	$keyboardType['QWERTY'] = ' selected="selected"';
} else {
	$keyboardType[$keyboardArr[$userData->tt_keyboard_layout]] = ' selected="selected"';
}

$testLength = getTestLength($userData, $debugMode);

$lessonLength = array('', '', '', '', '');

if($userData->tl_length == 70){
	$lessonLength[0] = ' selected="selected"';
} else if($userData->tl_length == 280){
	$lessonLength[2] = ' selected="selected"';
} else if($userData->tl_length == 560){
	$lessonLength[3] = ' selected="selected"';
} else if($userData->tl_length == 1 && $debugMode){
	$lessonLength[4] = ' selected="selected"';
} else {
	// default normal length = 140
	$lessonLength[1] = ' selected="selected"';
}
	
if(Input::exists()){
	if(Token::check(Input::get('token'))){
		
		$validate = new Validate();
		
		// only validate username if it was changed
		$unameValidationPassed = false;
		if(Input::get('username') != $username){
			$unameValidation = $validate->check($_POST, array(
				'username' => array(
					'name' => 'Username',
					'required' => true,
					'min' => 2,
					'max' => 20,
					'unique' => 'users',
					'notNumeric' => true,
					'noSpaces' => true,
					'noReservedSymbols' => true,
					'notDirty' => true
				)
			));
			if($unameValidation->passed()){
				$unameValidationPassed = true;
				$flagUsername = ($unameValidation->flagged()) ? true : false;
			} else {
				$unameErrorsToPrint = '<ol><li>' . implode('</li><li>', $unameValidation->errors()) . '</li></ol>';
			}
		} else {
			$unameValidationPassed = true;
		}
		
		$validation = $validate->check($_POST, array(
			'firstname' => array(
				'name' => 'First name',
				'min' => 1,
				'max' => 20,
				'notNumeric' => true,
				'notDirty' => true
			),
			'lastname' => array(
				'name' => 'Last name',
				'min' => 1,
				'max' => 20,
				'notNumeric' => true,
				'notDirty' => true
			),
			'tl_wpm_target' => array(
				'name' => 'Lesson WPM Target'
			),
			'tl_accuracy_target' => array(
				'name' => 'Lesson Accuracy Target'
			),
			'tl_length' => array(
				'name' => 'Lesson Length'
			)
		));
		
		if($validation->passed() && $unameValidationPassed){
			try{
				$newGroup = 1;
				if(Input::get('studentChkBx') == 'on'){
					$newGroup = 4;
				} else if(Input::get('teacherChkBx') == 'on'){
					$newGroup = 3;
				}
				$user->update(array(
					'username' => Input::get('username'),
					'firstname' => Input::get('firstname'),
					'lastname' => Input::get('lastname'),
					'allow_email' => (Input::get('allow_email') == 'on') ? 1 : 0,
					'group' => $newGroup,
					'flagged' => ($validation->flagged() || $flagUsername) ? 1 : 0
				));
				
				$user->updateSettings(array(
					'tt_text_title' => Input::get('tt_selection'),
					'tt_text_type' => Input::get('tt_type'),
					'tt_length' => Input::get('tt_length'),
					'tt_phase_shift' => (Input::get('tt_phase_shift') == 'on') ? 1 : 0,
					'tt_color_text' => (Input::get('tt_color_text') == 'on') ? 1 : 0,
					'tt_double_spacing' => (Input::get('tt_double_spacing') == 'on') ? 1 : 0,
					'tt_keyboard_layout' => array_search(Input::get('tt_keyboard_layout'), $keyboardArr),
					'tl_wpm_target' => Input::get('tl_wpm_target'),
					'tl_accuracy_target' => Input::get('tl_accuracy_target'),
					'tl_length' => Input::get('tl_length')
				));
				
				Session::flash('updated', 'Your details have been updated', 'INFO');
				Redirect::to('/user/' . $user->data()->username . '/profile');
			} catch(Exception $e){
				Session::flash('failed', $e->getMessage(), 'ERROR');
				Redirect::to('/user/' . $user->data()->username . '/profile');
				//die($e->getMessage());
			}
		} else {
			$errorsToPrint = '<ul><li>' . implode('</li><li>', $validation->errors()) . '</li></ul>';
		}
	}
}

$htmlTitle = "Profile, " . $username;
$htmlDescription = "Profile page for " . $username;
$cssFiles = "ibehk";
$noWidgets = true;
$jQueryUI = true;
$addContainer = true;
$fontAwesomeCDN = true;
require_once 'includes/overall/header.php';
?>
<div class="borderBottom">
<?php
include 'includes/profileMenu.php';
?>
</div>
</div> <!-- <div id="heading"> -->

<div id="pageMain">
	<div id="genAdWrap">
		<?php echo getAd("generalLeadAd", null, $debugMode, $user->isPremium());?>
	</div>
<?php
	if(Session::exists('failed')){
		echo Session::flash('failed');
	} else {
		echo Session::flash('updated');
	}
?>
	
	<form id="userSettingsForm" action="" method="post">
		<div id="formErrors"><?php echo (isset($errorsToPrint)) ? $errorsToPrint : '';?></div>
			
			
		<div id="accountContainer" class="usfContainer stoInfoBox">
			<div>
				<h2>Account</h2>
			</div>
			
		<?php //if($user->data()->id < 4){ ?>
			<?php 
				$subObj = new Subscription();
				$subStatus = $subObj->GetSubscriptionStatus($user->data()->id);
				
				
				$youArePremiumHTML = '<img class="circle-image-premium-icon largePic" src="/images/crown.svg?v=14"/>
									  <div class="field longLabel stoUI"><h3><i class="fa fa-thumbs-o-up" style="color:#0c6b8c;" aria-hidden="true"></i></h3> You are premium!</div>';
				 
				if($user->isPendingPremium()) { 
					echo '<div class="field longLabel stoUI">Your premium account activation is in process...</div>';
				} else if($user->isPendingCancelPremium()){
					echo $youArePremiumHTML;
					echo '<div class="field longLabel stoUI">Your premium account will expire in <div class="readableDateDiff" style="display:inline-block;">' . $subStatus[1] . '</div>.</div>';
					
					if($subStatus[0] == 'SUSPENDED')
						echo '<button onclick="ReactivateSub();" class="stoUI stoUIClickable stoUIClickableGreen smallBtn" type="button"><i class="fa fa-refresh" aria-hidden="true"></i></i> Reactivate Premium Membership</button>';
				} else if($user->isPendingReactivatePremium()){
					echo $youArePremiumHTML;
					echo '<div class="field longLabel stoUI">Your premium account reactivation is in process...</div>';
				} else if ($user->isPremium()){
			?>
			
			
			<?php 	if($subStatus[0] == 'ACTIVE'){ ?>
			
						<?php 
							echo $youArePremiumHTML; 
						
							// let user know if how much time left in free trial
							if($subStatus[2] == 1){
								echo '<div class="field longLabel stoUI">Your free trial will expire in <div class="readableDateDiff" style="display:inline-block;">' . $subStatus[1] . '</div>.</div>';
							}
						?>
						
						<div id="premiumButtonDiv"> <button onclick="ConfirmCancelSub();" class="stoUI stoUIClickable stoUIClickableRed smallBtn" type="button"><i class="fa fa-ban" aria-hidden="true"></i> Cancel Premium</button></div>
				
			<?php 	} else if ($subStatus[0] == 'CANCELLED') {
					$expireDateToPrint = new DateTime($subStatus[1]);
					$expireDateToPrint = $expireDateToPrint->format('g:ia \o\n l F jS, Y');
			?>
						<div>Your premium access will expire in <?php echo '<div class="readableDateDiff">' . $subStatus[1] . '</div>.'; ?> </div>
			<?php 	}?>
			<?php } else { ?>
			
				<div class="field longLabel stoUI">Current Plan: <h3>Free</h3> (Ad-Supported)</div>
				<div class="usfContainer">
					<div class="field longLabel stoUI">Want no more ads?</div>
					<div class="mediumBtn insideUpdateBtn flatBlueBtn" style="margin-top:25px; max-width:250px; color:white; ">
						<a href="/goPremium.php" style="text-decoration:none;">
							<div style="position:relative;">
								<img src="/images/crown.svg" style="height:40px; display:inline-block; position:absolute; left:7px; top:0px;"></img>
								<span style="color:white; margin-left: 39px;">Go Premium!</span>
							</div>
						</a>
					</div>
				</div>
			<?php } ?>
		<?php //}?>
			
		</div>
		
		
		<div id="profileContainer" class="usfContainer stoInfoBox">
			<div>
				<h2>Profile</h2>
			</div>
			
			<div class="fieldContainer">
				<div class="field stoUI">
					<label for="firstname">Username:</label>
					<input type="text" name="username" class="backEndInput" value="<?php echo escape($user->data()->username);?>">
				</div>
				<div class="field stoUI">
					<label for="firstname">First Name:</label>
					<input type="text" name="firstname" class="backEndInput" value="<?php echo escape($user->data()->firstname);?>">
				</div>
					
				<div class="field stoUI">
					<label for="lastname">Last Name:</label>
					<input type="text" name="lastname" class="backEndInput" value="<?php echo escape($user->data()->lastname);?>">
				</div>
				
				<div class="field checkBox longLabel stoUI" title="Give SpeedTypingOnline.com permission to send email updates to your account email address.">
					<label><input type="checkbox" name="allow_email" class="backEndInput" <?php echo $allowEmailSetting; ?>> Send me email notifications</label>
				</div>
				
				<div class="field checkBox longLabel stoUI" title="Click to become a teacher and form a classroom" style="<?php echo ($user->isStudent()) ? 'display:none;' : ''?>">
					<label id="teacherCheckboxLbl"><input id="teacherChkBx" type="checkbox" name="teacherChkBx" class="backEndInput" <?php echo $user->isTeacher() ? ' checked' : ''; ?>> I'm a Teacher</label>
				</div>
				
				<div class="field checkBox longLabel stoUI" title="Click to become a student and join a classroom" style="<?php echo ($user->isTeacher()) ? 'display:none;' : ''?>">
					<label id="studentCheckboxLbl"><input id="studentChkBx" type="checkbox" name="studentChkBx" class="backEndInput" <?php echo $user->isStudent() ? ' checked' : ''; ?>> I'm a Student</label>
					
				</div>
				<!--<div class="field checkBox longLabel stoUI">
					<h3>Account Type</h3>
					  <fieldset>
						<legend>Select an account type: </legend>
						<input type="radio" name="radio-1" id="radio-1" class="accountTypeRadioInpt">
						<label for="radio-1">Normal Account</label>
						<input type="radio" name="radio-1" id="radio-2" class="accountTypeRadioInpt">
						<label for="radio-2" title="Click to become a teacher and form a classroom">I'm a Teacher</label>
						<input type="radio" name="radio-1" id="radio-3" class="accountTypeRadioInpt">
						<label for="radio-3" title="Click to become a student and join a classroom">I'm a Student</label>
					  </fieldset>
				</div>-->
			</div>
			
			<input id="profileUpdateBtn" class="largeBtn insideUpdateBtn submitButtonCarrot" style="display:none;" type="submit" value="Save Changes">
		</div>
		
		<div id="classesContainer" class="usfContainer stoInfoBox" style="<?php echo ($user->isStudent() || $user->isTeacher()) ? '' : 'display: none;';?>">
			<div>
				<h2>Classes</h2>
			</div>
			
			<div id="listOfClasses" class="field stoUI"></div>
		
			<div class="field stoUI short" style="<?php echo ($user->isTeacher()) ? '' : 'display: none;';?>">
				<div id="newClassErrMsg" class="formErrors notTop">
				</div>
				<div style="display:inline-block;">
					<label for="newClassName">Class Name:</label>
					<input type="text" id="newClassName" name="newClassName" class="" value="" style="">
				</div>
				<div style="display:inline-block;">
					<input id="addClassBtn" class="mediumBtn insideUpdateBtn flatBlueBtn" style="" type="button" value="Add Class" onclick="AddClass();">
				</div>
			</div>
			
			<div id="enterClassCodeContainer" class="fieldContainer" style="<?php echo $user->isStudent() ? '' : 'display: none;';?>">
				<div class="field stoUI">
					<label for="className">Enter Class Code:</label>
					<input type="text" id="classCodeInpt" name="className" value="<?php echo '';?>">
					<div id="classCodeStatusInfo"></div>
					<input type="button" id="joinClassBtn" class="largeBtn insideUpdateBtn borderRad_8 dkBlueBkgd" value="Join Class"></input>
					<!--<input class="stoUI stoUIClickable" type="button" value="Clear Test Data" onclick="cleanData(\'tt\', \'Delete all typing test data\');">-->
				</div>
				<div id="classesInfo"></div>
			</div>
		</div>
		
		<div id="testSettingsContainer" class="usfContainer stoInfoBox">
			<div>
				<h2>Test Settings</h2>
			</div>
			
			<div class="fieldContainer">
				<div class="field stoUI">
					<label for="text_selection">Text Selection:</label>
					<select id="titleTypeSelected" name="text_selection" class="stoUI stoUIClickable backEndSelect" data-default="<?php echo $userData->tt_text_title;?>">
						<option value="0" selected="selected">Random</option>
					</select>
				</div>
				
				<div class="field stoUI">
					<label for="text_type">Text Type:</label>
					<select id="textTypeSelected" name="text_type" class="stoUI stoUIClickable backEndSelect">
						<?php 
								if($keyboardArr[$userData->tt_keyboard_layout] == "NUMPAD")
									echo $ttdl_numpad;
								else
									echo $ttdl_normal; 
							?>
					</select>
				</div>
				
				<div class="field stoUI">
					<label for="tt_length">Time:</label>
					<select name="tt_length" class="stoUI stoUIClickable backEndSelect" form="userSettingsForm">
<?php					if($debugMode){	?>
							<option value="3"<?php echo $testLength[7]; ?>>3 seconds</option>
<?php					}?>
						<option value="30"<?php echo $testLength[0]; ?>>30 seconds</option>
						<option value="60"<?php echo $testLength[1]; ?>>1 minute</option>
						<option value="120"<?php echo $testLength[2]; ?>>2 minutes</option>
						<option value="180"<?php echo $testLength[3]; ?>>3 minutes</option>
						<option value="300"<?php echo $testLength[4]; ?>>5 minutes</option>
						<option value="600"<?php echo $testLength[5]; ?>>10 minutes</option>
						<option value="1200"<?php echo $testLength[6]; ?>>20 minutes</option>
					</select>
				</div>
				
				<div class="field checkBox stoUI" title="Colors typed text based on entry (green = correct, red = incorrect...)">
					<label><input type="checkbox" name="tt_color_text" class="backEndInput" <?php echo $textColoringSetting; ?>> Color Highlighting</label>
				</div>
				
				<div class="field checkBox stoUI" title="Auto-corrects misalignment with text at cost of 1 incorrect letter per shift (recommend leaving enabled)">
					<label><input type="checkbox" name="tt_phase_shift" class="backEndInput" <?php echo $phaseShiftSetting; ?>> Phase Shift Correction</label>
				</div>
				
				<div class="field checkBox stoUI" title="Will add 2 spaces after every sentence (recommend leaving disabled)">
					<label><input type="checkbox" name="tt_double_spacing" class="backEndInput" <?php echo $doubleSpacingSetting; ?>> Two spaces after period</label>
				</div>
				
				<div class="field stoUI">
					<label for="tt_keyboard_layout">Keyboard Layout:</label>
					<select name="tt_keyboard_layout" class="stoUI stoUIClickable backEndSelect" form="userSettingsForm">
						<option value="QWERTY"<?php echo $keyboardType["QWERTY"]; ?>>Standard (QWERTY)</option>
						<option value="QWERTY_UK"<?php echo $keyboardType["QWERTY_UK"]; ?>>QWERTY - UK</option>
						<option value="DVORAK"<?php echo $keyboardType["DVORAK"]; ?>>Dvorak</option>
						<option value="COLEMAK"<?php echo $keyboardType["COLEMAK"]; ?>>Colemak</option>
						<option value="COLEMAK_UK"<?php echo $keyboardType["COLEMAK_UK"]; ?>>Colemak - UK</option>
						<option value="AZERTY"<?php echo $keyboardType["AZERTY"]; ?>>AZERTY</option>
						<option value="QWERTZ"<?php echo $keyboardType["QWERTZ"]; ?>>QWERTZ</option>
						<option value="QWERTZ_SF"<?php echo $keyboardType["QWERTZ_SF"]; ?>>QWERTZ - SwissFrench</option>
						<option value="NUMPAD"<?php echo $keyboardType["NUMPAD"]; ?>>Numpad (10-key)</option>
					</select>
				</div>
			</div>
			<input id="testSettingsUpdateBtn" class="largeBtn insideUpdateBtn submitButtonCarrot" style="display:none;" type="submit" value="Save Changes">
		</div>
		
		<div id="lessonSettingsContainer"class="usfContainer stoInfoBox">
			<div>
				<h2>Lesson Settings</h2>
			</div>
			
			<div class="fieldContainer">
				<div class="field stoUI">
					<label id="targetWPMlabel" for="tl_wpm_target">Target Speed:</label>
					<input type="text" id="targetWPMspinner" class="ui_spinner backEndInput" name="tl_wpm_target" value="<?php echo escape($user->data()->tl_wpm_target);?>">
					<span>wpm</span>
				</div>
				
				<div class="field stoUI">
					<label id="targetAccLabel" for="tl_accuracy_target">Target Accuracy:</label>
					<input type="text" id="targetAccuracyspinner" class="ui_spinner backEndInput" name="tl_accuracy_target" value="<?php echo escape($user->data()->tl_accuracy_target);?>">
					<span>%</span>
				</div>
				
				<div class="field stoUI">
					<label for="tl_length">Lesson Length:</label>
					<select name="tl_length" class="stoUI stoUIClickable" form="userSettingsForm">
<?php					if($debugMode){	?>
						<option value="1"<?php echo $lessonLength[4]; ?>>Test Mode</option>
<?php			}?>
						<option value="70"<?php echo $lessonLength[0]; ?>>Short</option>
						<option value="140"<?php echo $lessonLength[1]; ?>>Normal</option>
						<option value="280"<?php echo $lessonLength[2]; ?>>Long</option>
						<option value="560"<?php echo $lessonLength[3]; ?>>Extra Long</option>
					</select>
				</div>
			</div>
			<input id="lessonSettingsUpdateBtn" class="largeBtn insideUpdateBtn submitButtonCarrot" style="display:none;" type="submit" value="Save Changes">
		</div>
		
		<input type="hidden" name="token" value="<?php echo Token::generateToken(Config::get('session/token_name'));?>">	
	</form>
</div>


<div id="dialog-confirm" title="Permanently delete all data for this classroom?" style="display: none;">
	<div style="margin:0 7px 20px 0; ">
		<div id="extraInfo"></div>
		<span style="display:inline-block; font-size:34px; color:#ef533a;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>
		<span id="dialogMainBodyMsg" style="display:inline-block; width:85%;">Your typing test data will be permanently deleted and cannot be recovered.</span>
	</div>
	<p>Are you sure?</p>
</div>

<?php if($subStatus[0] == 'ACTIVE'){ ?>
<div id="cancel-sub-dialog-confirm" title="Cancel your premium subscription?" style="display: none;">
	<div style="margin:0 7px 20px 0; ">
		<div id="extraInfo"></div>
		<span style="display:inline-block; font-size:34px; color:#ef533a;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>
		<div id="dialogMainBodyMsg" style="display:inline-block; width:85%;">Your premium account subscription will be cancelled and will not renew after</div>
		<div class="makeReadableDate largeTxt"><?php echo $subStatus[1]; ?></div>
	</div>
	<p>Are you sure?</p>
</div>
<?php } ?>

<div id="reactivate-sub-dialog-confirm" title="Reactivate your premium subscription?" style="display: none;">
	<div style="margin:0 7px 20px 0; ">
		<div id="extraInfo"></div>
		<span style="display:inline-block; font-size:34px; color:#009600;"><i class="fa fa-repeat" aria-hidden="true"></i></i></span>
		<span id="dialogMainBodyMsg" style="display:inline-block; width:85%;">Your premium account subscription will be reactivated.</span>
	</div>
	<p>Are you sure?</p>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.js"></script>
<script src="/js/general.js?v=27"></script>
<script>
	var keyboardArr = <?php echo json_encode($keyboardArr);?>;
	var testGlobals = {userSettings : {} };
	$(document).ready(function(){
		updateTextSelection(<?php echo $userData->tt_text_title;?>);
		
		//$('.accountTypeRadioInpt').checkboxradio();
		
		$("#tt_keyboard_layout").change(function(){
			var kls_elem = $('#tt_keyboard_layout');
			var kls_elemVal = kls_elem.val();
			var tts_elem = $('#textTypeSelected');
			var tts_elemVal = kls_elem.val();
			testGlobals["userSettings"]["tt_keyboard_layout"] = keyboardArr.indexOf(kls_elemVal);
			
			if(kls_elemVal == "NUMPAD"){
				tts_elem.empty();
				tts_elem.append(ttdl_numpad);
				$textType['NUMPAD'] = ' selected="selected"';
			} else {
				var f = tts_elem.val();
				tts_elem.empty();
				tts_elem.append(ttdl_normal);
				tts_elem.val(f);
			}
			Reset($('#timeTypeSelected').val(),'timer');
		});
		setupListOfClasses();
		
		if($('.readableDateDiff').length){
			var expireDate = new Date($('.readableDateDiff').html().replace(/-/g, '/'));
			var nowDate = new Date();
			$('.readableDateDiff').html('<div title="' + expireDate + '">' + readableTimeDifference(nowDate, expireDate) + '</div>');
		}
		
		if($('.makeReadableDate').length){
			dateToMakeReadable = new Date($('.makeReadableDate').html().replace(/-/g,'/'));
			$('.makeReadableDate').html(dateToMakeReadable.toDateString());
		}
	});
	
	function setupListOfClasses(){
		$.post('/getData.php', { type: "crd"},
			function(output){
				myJSONdata = $.parseJSON(output);
				classInfo = myJSONdata["classInfo"];
				loc_elem = $('#listOfClasses');
				loc_elem.empty();
				var userIsStudent = myJSONdata["userIsStudent"];
				var userIsTeacher = myJSONdata["userIsTeacher"];
				var loggedInUsername = myJSONdata["loggedInUsername"];
				
				var insertHTML;

				if(userIsStudent){
					if(classInfo == ''){
						
					} else {
						insertHTML = '<table><thead><tr><th>#</th><th>Class</th><th>Teacher</th><th>Status</th></tr></thead><tbody>';
					
						for(var ii = 0; ii < classInfo.length; ii++){
							var awaitingVerifyMessage = '';
							if(classInfo[ii].is_verified == 0){
								awaitingVerifyMessage = "<td title=\"Waiting for your teacher to verify you\">Pending...</td>";
							} else if(classInfo[ii].is_verified == -1){
								awaitingVerifyMessage = "<td title=\"Teacher did not allow you into their class\">Denied</td>";
							}
							
							insertHTML += '<tr><td>' + (ii+1) + ')</td><td>' + classInfo[ii].class_name + '</td><td>' + classInfo[ii].firstname + ' ' + classInfo[ii].lastname + '</td>' + awaitingVerifyMessage + '</tr>';
						}
						insertHTML += '</tbody></table>';
					}
				} else if(userIsTeacher){
					insertHTML = '<table><thead><tr><th>#</th><th class="classroomName">Class Name</th><th class="editCRNbtn"></th><th>Code</th><th>Join Method</th><th></th></tr></thead><tbody>';
					var autoJoinCheckboxHTML;
					var checkedText;
					
					for(var ii = 0; ii < classInfo.length; ii++){
						checkedText = classInfo[ii].req_confirm == 0 ? " checked" : "";
						autoJoinCheckboxHTML = '<div class="field checkBox" title="Students will automatically be added when enabled. If disabled, students will only be added once you confirm them."><label><input id="' + classInfo[ii].ccode + '" name="' + classInfo[ii].ccode + '" type="checkbox" name="allow_email" class="autojoinChkBx" onclick="setClassJoinMethod(this)"' + checkedText + '> Auto-Join</label></div>'
						
						resetClassCodeBtnHTML = HTMLelemStr('<button onclick="ResetClassCode(\'' + classInfo[ii].id + '\', \'classID_' + classInfo[ii].id + '\')" class="ccResetBtn stoUI stoUIClickable stoUIClickableDark tinyBtn" type="button">Reset</button>', 'span', '', 'sideMarginsSmall');
						
						editClassroomNameBtnHTML = HTMLelemStr('<button onclick="EditClassroomName(this)" class="stoUI stoUIClickable stoUIClickableDark tinyBtn" type="button"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>', 'span', '', 'sideMarginsSmall');
						
						acceptNewCRnameBtnHTML = HTMLelemStr('<button onclick="SubmitNewClassroomName(this)" class="stoUI stoUIClickable stoUIClickableGreen mediumBtn" type="button"><i class="fa fa-check" aria-hidden="true"></i></button>', 'span', '', 'acceptNewCRName sideMarginsSmall');
						
						cancelNewCRnameBtnHTML = HTMLelemStr('<button onclick="CancelNewClassroomName(this)" class="stoUI stoUIClickable stoUIClickableRed mediumBtn" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>', 'span', '', 'cancelNewCRName sideMarginsSmall');
						
						editNewCRnameContainerHTML = HTMLelemStr('<input type="text" name="newCRname" class="newCRname" value="' + classInfo[ii].class_name + '" maxlength="32">', 'div', '', 'editNewCRnameContainer', '', 'display:none;');
						
						editNewCRnameBtnsContainerHTML = HTMLelemStr(acceptNewCRnameBtnHTML + cancelNewCRnameBtnHTML, 'div', '', 'editNewCRnameBtnsContainer', '', 'display:none;');
					
						deleteBtnHTML = HTMLelemStr('<button onclick="ConfirmAction(\'ol_dc\', \'' + classInfo[ii].id + '\',\'Delete Class\',\'&quot;' + classInfo[ii].class_name + '&quot;\',\'Permanently Delete Classroom?\',\'All assignments and student data from this class will be deleted.\')" class="stoUI stoUIClickable stoUIClickableRed smallBtn" type="button"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>', 'td', '', '');
						
						classCodeHTML = HTMLelemStr(classInfo[ii].ccode + resetClassCodeBtnHTML, 'td', '', 'ccode ccToDisplay');
						
						classNameLinkHTML = HTMLelemStr('<a href="/user/' + loggedInUsername + '/classes#classID_' + classInfo[ii].id + '">' + classInfo[ii].class_name + '</a>','span','','crnSpan');
						
						insertHTML += '<tr id="classID_' + classInfo[ii].id + '" class="classroomRow"><td>' + (ii+1) + ')</td><td class="classroomName">' + classNameLinkHTML + editNewCRnameContainerHTML + '</td><td class="editCRNbtn">' + editClassroomNameBtnHTML + editNewCRnameBtnsContainerHTML + '</td>' + classCodeHTML + '<td>' + autoJoinCheckboxHTML + '</td>' + deleteBtnHTML + '</tr>';
					}
					insertHTML += '</tbody></table>';
				}
				
				loc_elem.html(insertHTML);
		});
	}
	
	function updateTextSelection(val = null){
		var e = $("#textTypeSelected").val().toUpperCase();
		var f_elem = $("#titleTypeSelected");
		
		$.post('/getData.php', { type: "ttt", textType: e},
			function(output){
				myJSONdata = $.parseJSON(output);
				textToType = myJSONdata["textToType"];
				f_elem.empty();
				var appendText = '<option value="0">Random</option>';
				for(var ii = 0; ii < textToType.length; ii++){
					appendText += '<option id="' + ii + '" value="' + textToType[ii].id + '">' + textToType[ii].title + '</option>';
				}
				
				f_elem.append(appendText);
				
				if(val != null){
					$('#titleTypeSelected option[value=' + val + ']').prop('selected', true);
				}
		});
	}
	
	function setClassJoinMethod(elem){
		var theSetting = elem.checked ? 0 : 1;
		$.post('/setData.php', { type: "ol_scaj", ccode: elem.id, setting: theSetting},
			function(output){
				//$('#classCodeStatusInfo').html(output);
				console.log(output);
		});
	}
	
	$('.autojoinChkBx').click(function(e){
		var theSetting = e.checked ? 0 : 1;
		$.post('/setData.php', { type: "ol_scaj", ccode: e.target.id, setting: theSetting},
			function(output){
				//$('#classCodeStatusInfo').html(output);
				console.log(output);
		});
	});
	
	$('#textTypeSelected').change(function(){
		updateTextSelection();
	});
	
	$('#teacherCheckboxLbl').click(function(){
		if ($('#teacherChkBx').is(':checked')) {
			$('#classesContainer').show();
			$('#studentCheckboxLbl input').prop('checked', false);
		} else {
			$('#classesContainer').hide();
		}
			
	});
	
	$('#studentCheckboxLbl').click(function(){
		if ($('#studentChkBx').is(':checked')) {
			$('#classesContainer').show();
			$('#teacherCheckboxLbl input').prop('checked', false);
			$('#enterClassCodeContainer').show();
		} else {
			$('#classesContainer').hide();
			$('#enterClassCodeContainer').hide();
		}
			
	});
	
	$('#joinClassBtn').click(function(){
		$('#classCodeStatusInfo').html("Working...");
		$.post('/setData.php', { type: "ol_astc", clscd: $('#classCodeInpt').val()},
				function(output){
					$('#classCodeStatusInfo').html(output);
			});
	});
	
	function showUpdateButton(){
		$('.insideUpdateBtn').each(function(){
			$(this).show();
		});
		//$('#topUpdateBtn').show();
		//$('#bottomUpdateBtn').show();
		$('#formInfo').hide();
		$('#formErrors').hide();
	}
	
	$('.backEndInput').change(function(){
		showUpdateButton();
	})
	
	$('.backEndSelect').change(function(){
		showUpdateButton();
	})
	
	$('.ui_spinner').on('mousewheel', function(event) {
		showUpdateButton();
	});
	
	$(document).ready(function() {
		$('.ui-spinner-button').on('click', function(event) {
			showUpdateButton();
		});
	});
	

	$( "#targetWPMspinner").spinner({
		mouseWheel: true,
		numberFormat: "n0",
	  spin: function( event, ui ) {
		if ( ui.value > 255 ) {
		  $( this ).spinner( "value", 255 );
		  return false;
		} else if ( ui.value < 0 ) {
		  $( this ).spinner( "value", 0 );
		  return false;
		}
	  }
	});

	$("#targetWPMspinner").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false;}
	});

	$("#targetAccuracyspinner").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) { return false;}
	});

	$( "#targetAccuracyspinner").spinner({
		step: 0.1,
		numberFormat: "n1",
		mouseWheel: true,
	  spin: function( event, ui ) {
		if ( ui.value > 100 ) {
		  $( this ).spinner( "value", 100 );
		  return false;
		} else if ( ui.value < 0 ) {
		  $( this ).spinner( "value", 0 );
		  return false;
		}
	  }
	});
	
	function AddClass(){
		var newClassName = $('#newClassName').val();
		
		$.post('/setData.php', { type: 'ol_ac', ncn: newClassName },
			function(output){
				$('#newClassErrMsg').html(output);
					if(output == "New class was added")
						window.location.reload();
		});
	}
	
	function SubmitNewClassroomName(elem){
		var jqElem_RowParent = $(elem).closest('.classroomRow');
		var classID = jqElem_RowParent[0].id.slice(8);
		
		$.post('/setData.php', { type: 'ol_ecrn', cid: classID, ncrn: jqElem_RowParent.find('.newCRname')[0].value },
			function(output){
				$('#newClassErrMsg').html(output);
				if(output == "Successfully updated class name")
					window.location.reload();
		});
	}
	
	function EditClassroomName(elem){
		var jqElem_RowParent = $(elem).closest('.classroomRow');
		$('.editNewCRnameContainer').hide();
		$('.editNewCRnameBtnsContainer').hide();
		$('.crnSpan').show();
		$('.editCRNbtn button').show();
		jqElem_RowParent.find('.editNewCRnameContainer').show();
		jqElem_RowParent.find('.editNewCRnameBtnsContainer').show();
		jqElem_RowParent.find('.crnSpan').hide();
		$(elem).hide();
	}
	
	function CancelNewClassroomName(elem){
		var jqElem_RowParent = $(elem).closest('.classroomRow');
		$('.editNewCRnameContainer').hide();
		$('.editNewCRnameBtnsContainer').hide();
		$('.crnSpan').show();
		$('.editCRNbtn button').show();
		jqElem_RowParent.find('.editNewCRnameContainer').hide();
		jqElem_RowParent.find('.editNewCRnameBtnsContainer').hide();
		jqElem_RowParent.find('.crnSpan').show();
	}
	
	function ReactivateSub(){
	
		$( "#reactivate-sub-dialog-confirm" ).dialog({
		  resizable: false,
		  height:280,
		  width:590,
		  modal: true,
		  draggable: false,
		  buttons: [
			{
				text: "Reactivate Subscription",
				click: function() {
					$( this ).dialog( "close" );
					$.post('/setData.php', { type: 'rasub' },
						function(output){
							console.log("reactivate subscription");
							window.location.reload();
					});
				}			
			}, {
				text: "Cancel",
				click: function() {
					  $( this ).dialog( "close" );
					}
			}
		  ],
		  open: function() {
			  $(this).parent().find(":contains('Cancel')").focus();
			  $(this).parent().find(":contains('Delete')").blur();
		  }	
		});
	}
	
	function ConfirmCancelSub(){
	
		$( "#cancel-sub-dialog-confirm" ).dialog({
		  resizable: false,
		  height:280,
		  width:590,
		  modal: true,
		  draggable: false,
		  buttons: [
			{
				text: "Cancel Subscription",
				click: function() {
					$( this ).dialog( "close" );
					$.post('/setData.php', { type: 'sussub'},
						function(output){
							window.location.reload();
					});
				}			
			}, {
				text: "Cancel",
				click: function() {
					  $( this ).dialog( "close" );
					}
			}
		  ],
		  open: function() {
			  $(this).parent().find(":contains('Cancel')").focus();
			  $(this).parent().find(":contains('Delete')").blur();
		  }	
		});
	}
	
	function ConfirmAction(strType,secParam,delButtonText,specName=null,dialogTit=null,dialogMsg=null){
		if(specName == null){
			specName = "This";
		}
		if(dialogTit == null){
			dialogTit = "Permanently delete all data?";
		}
		if(dialogMsg == null){
			dialogTit = "All data will be permanently deleted and cannot be recovered.";
		}
		$('#dialog-confirm').prop('title',dialogTit);
		$('#dialogMainBodyMsg').html(dialogMsg);
		$('#extraInfo').html("You are deleting classroom: <div id=\"assignTitle\">" + specName + "</div>");
		
		$( "#dialog-confirm" ).dialog({
		  resizable: false,
		  height:280,
		  width:590,
		  modal: true,
		  draggable: false,
		  buttons: [
			{
				text: delButtonText,
				click: function() {
					$( this ).dialog( "close" );
					$.post('/setData.php', { type: strType, cid: secParam},
						function(output){
							window.location.reload();
					});
				}			
			}, {
				text: "Cancel",
				click: function() {
					  $( this ).dialog( "close" );
					}
			}
		  ],
		  open: function() {
			  $(this).parent().find(":contains('Cancel')").focus();
			  $(this).parent().find(":contains('Delete')").blur();
		  }
    });
}
</script>
<?php
require_once 'includes/overall/footer.php';
?>