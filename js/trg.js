var sWatch = null;
var TotalMilSeconds;
var RoundTime = 0;
var TimeWasReset = true;
var TimeIsStopped = false;
var localCursor = 0;
var cursor = 0;
var lettersTypedCorrectly = 0;
var lettersTypedIncorrectly = 0;
var badEntriesDeleted = 0;
var totalEntriesTyped = 0;
var wordsTyped = 0;
var lineDiv = null;
var textString = "";
var deletes = 0;
var totalDeletes = 0;
var lineNum = 0;
var lineDivText = "";
var cutoff = 0;
var phaseShift = 0;
var pShiftWeight = 0;
var minCursorReplaceStart = 0;
var testCounter = 0;
var timeDivider = 0;

var middleCursor;

var textType = null;
var maxCharsInLine = 52;
var slowestTime = 99999999;
var highScores = [slowestTime, slowestTime, slowestTime, slowestTime];
var HScoresWereFiltered = false;

var t=[0, 0, 0, 0, 0, 0, 0, 1];

var cleanTextString;
var letterMistakes = {};
var letterPercentMisses = {};
var letterCounts = {};
var letterResponseTimes = {};

var keyInputTime = 0;
var responseTimeStart = 0;
var savedResponseTimeStart = 0;
var responseTime = 0;

var defaultMinSpeed = 7000;
var minSpeed = defaultMinSpeed;
var maxSpeed = 100;
var defaultAcceleration = 170;
var maxAcceleration = defaultAcceleration;
var maxTypingSpeed = 120;
var roadSpeed = minSpeed;
var roadLineWidth = 80;
var desiredRoadLineSpacing = 200;
var carResetTime = 50;
var fuelTank = 60;
var readyToEnlivenCar = true;
var stopRoad = true;
var isResetting = false;

var roadLineSpacing;
var numRoadLines;

function SetRoadSpeed(newSpeed){
	var speedRange = minSpeed - maxSpeed;
	var pedalPercentage = Math.pow(newSpeed, 1/3) / Math.pow(maxTypingSpeed, 1/3);
	
	if(pedalPercentage > 1){
		pedalPercentage = 1;
	}
	
	var newRoadSpeed = minSpeed - (pedalPercentage * minSpeed);
	
	if( (roadSpeed - newRoadSpeed) > maxAcceleration){
		roadSpeed -= maxAcceleration;
	} else if ( (roadSpeed - newRoadSpeed) < -maxAcceleration ){
		roadSpeed += maxAcceleration;
	} else {
		roadSpeed = newRoadSpeed;
	}
	
	if(roadSpeed > minSpeed){
		roadSpeed = minSpeed;
	} else if(roadSpeed < maxSpeed){
		roadSpeed = maxSpeed;
	}
	
}

function GetText(){
	$.post('/getData.php', { type: "ttt", textType: "RANDOM_WORDS"},
		function(output){
			
			myJSONdata = $.parseJSON(output);
			tttArr = myJSONdata["textToType"];
			
			// if in maintenance mode get text to type using AJAX
			if(myJSONdata["maint_mode"]){
				$.getJSON('/resources/STO_ttt.json', function(data){
						tttArr = data;
						processTTT(tttArr,"RANDOM_WORDS");
				});
			} else {
				processTTT(tttArr,"RANDOM_WORDS");
			}
			
			cleanTextString = textString.replace(/\s\n|\n\s|\n/gm," ");    // remove all newline characters leaving spaces if needed
			
			var tempStr = "";
			minCursorReplaceStart = 0;
			
			for(var ii = 0; ii <= maxCharsInLine; ii++){
				lineDivText += '<span class="plainText">' + textString.substring(ii, ii+1) + '</span>';
			}
		  
			// place cursor on first letter
			lineDivText = lineDivText.replace(/plainText/i,"nxtLetter");

			lineDiv = $('#lineDivContainer').get();
			lineDiv = lineDiv[0];
			
			var hiddenTextString = "";
			
			for(var ii = 1; ii < middleCursor; ii++){
				hiddenTextString += '<span class="hiddenTxt"> </span>';
			}
			
			lineDivText = hiddenTextString + lineDivText.substring(0,(32*(middleCursor + 1)));
			
			lineDiv.innerHTML = formatForInnerHTML(lineDivText);
		});
}

function processTTT(tttArr, textType){
	
	// see if got good data back from database
	if((typeof tttArr != "undefined") && (tttArr.length > 0)){
		textString = tttArr[0].text;
		
		u = tttArr[0].text.split(tttArr[0].randomize_char);
		textString = "";
		while (textString.length < 5e4) {
			textString += u[Math.floor(Math.random() * u.length)] + " ";
		}
		
		textString = textString.replace(/&/g, "&");
		textString = textString.replace(/</g, "<");
		textString = textString.replace(/>/g, "<");
		textString = textString.replace(/  /g, " ");

	} else {
		// assign defaults
		textString = "Sphinx of black quartz, judge my vow. The five boxing wizards jump quickly. Pack my box with five dozen liquor jugs. The quick brown fox jumps over a lazy dog. Every good cow, fox, squirrel, and zebra likes to jump over happy dogs. Just keep examining every low bid quoted for zinc etchings. A quick movement of the enemy will jeopardize six gunboats. Few black taxis drive up major roads on quiet hazy nights. Big July earthquakes confound zany experimental vow. Grumpy wizards make a toxic brew for the jovial queen. My girl wove six dozen plaid jackets before she quit. Painful zombies quickly watch a jinxed graveyard. The lazy major was fixing Cupid's broken quiver. The quick onyx goblin jumps over the lazy dwarf. Twelve ziggurats quickly jumped a finch box. My faxed joke won a pager in the cable TV quiz show. Woven silk pyjamas exchanged for blue quartz. We promptly judged antique ivory buckles for the next prize. How razorback jumping frogs can level six piqued gymnasts. Sixty zippers were quickly picked from the woven jute bag. The exodus of jazzy pigeons is craved by squeamish walkers. All questions asked by five watch experts amazed the judge. A quick movement of the enemy will jeopardize six gunboats. West quickly gave Bert handsome prizes for six juicy plums. Jeb quickly drove a few extra miles on the glazed pavement";
	}
	
}

	
function StopAnimations(){
	readyToEnlivenCar = false;
	stopRoad = true;
	$('.yellowBox').stop();
	$('#redCar').stop();
}

function FinishAnimations(){
	minSpeed = 30000;
	var finishInterval = setInterval(function(){ 
		SetRoadSpeed(0);
		moveRoad()
		
		if(roadSpeed >= (minSpeed * .6) || isResetting){
			clearInterval(finishInterval);
			minSpeed = defaultMinSpeed;
			if(!isResetting)
				StopAnimations();
		}
	}, 60);
	
}
	
function resetRoad(element){
	StopAnimations();
	resetCar($("#redCar"));
	
	elem = $(element);
	
	$('.yellowBox').remove();
	$('.yellowBox').css("width", roadLineWidth + "px");
	
	var elemWidth = elem.width();
	numRoadLines = Math.floor(elemWidth / (roadLineWidth + desiredRoadLineSpacing));
	roadLineSpacing = (elemWidth - (roadLineWidth * numRoadLines)) / (numRoadLines);
	
	for(var ii = 0; ii <= numRoadLines; ii++){
		elem.append("<div id='yb" + ii + "' class='yellowBox'></div>");
		$('#yb' + ii).css("left", ii * (roadLineWidth + roadLineSpacing) + (roadLineSpacing / 2));
		
		if(ii == (numRoadLines + 1)){
			$('#yb' + ii).addClass("lastLine");
		}
	}
	
	
	numRoadLines++;
	readyToEnlivenCar = true;
	roadSpeed = minSpeed;
	maxAcceleration = defaultAcceleration;
	minSpeed = defaultMinSpeed;
	UpdateFuelGauge();
}

function startRoad(elem){
	readyToEnlivenCar = false;
	$('#redCar').animate(
		{left: "30%"}, 
		{
			duration: roadSpeed/3,
			easing: "easeInOutCubic",
			complete: function(){
				$('#redCar').css("left","30%");
				readyToEnlivenCar = true;
			}
		}
	);
	
	setTimeout(function(){
		if(!isResetting){
			stopRoad = false;
			$('.yellowBox').each(function(i){
				var elem = $(this);
				startSpeed = ((elem.position().left + roadLineWidth) / elem.parent().width()) * roadSpeed;
				
				elem.animate(
					{left: -elem.width()},
					{
						duration: startSpeed,
						easing: "linear",
						complete: function(){
							$(elem).css("left", elem.parent().width());
						}
					}
				);
			});
			
			if(!stopRoad)
				moveRoad();
		}
	}, roadSpeed/6);
	
}
	
function moveRoad(){
	if(!stopRoad){
		var parentWidth;
		var elemWidth;
			
		$('.yellowBox').each(function(i){
			elem = $(this);
			elem.stop();
			elemWidth = elem.width();
			
			adjustedSpeed = ((elem.position().left + roadLineWidth) / elem.parent().width()) * roadSpeed;
			
			$(elem).animate(
				{left: -elemWidth}, 
				{
					duration: adjustedSpeed,
					easing: "linear",
					complete: function(){
						wrapElement = (elem.attr("id").substr(2,1) - 1 + numRoadLines) % numRoadLines;
						$(elem).css("left", $('#yb' + wrapElement).position().left + roadLineWidth + roadLineSpacing);
						/*$(elem).css("left", $('.lastLine').position().left + elemWidth + roadLineSpacing);*/
						$('.lastLine').removeClass("lastLine");
						$(elem).addClass("lastLine");
						if(!stopRoad)
							moveRoad();
					}
				}
			);
		});
		
		var enlivenChance = getRandomInt(-5, 3);
		if(enlivenChance > 0)
			enlivenCar($("#redCar"));
	}
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function enlivenCar(elem){
	
	if(readyToEnlivenCar){
		readyToEnlivenCar = false;
		var currentX = $(elem).position().left;
		var currentY = $(elem).position().top;
		
		var driftX = getRandomInt(-4, 4);
		var driftY = getRandomInt(-8, 8);
		
		$(elem).animate(
			{left: currentX + driftX, top: currentY + driftY}, 
			{
				duration: roadSpeed/5,
				easing: "swing",
				complete: function(){
					$(elem).animate(
						{left: currentX, top: currentY}, 
						{
							duration: roadSpeed/2,
							easing: "swing",
							complete: function(){
								readyToEnlivenCar = true;
							}
						}
					);
				}
			}
		);
	}
	
}

function resetCar(elem){
	isResetting = true;
	readyToEnlivenCar = false;
	$(elem).animate(
		{left: "250%"}, 
		{
			duration: carResetTime,
			easing: "easeInCubic",
			complete: function(){
				$(elem).css("left","-10%");
				$(elem).animate(
					{left: "3%"}, 
					{
						duration: 800,
						easing: "easeOutCubic",
						complete: function(){
							$(elem).css("left","3%");
							carResetTime = 1500;
							readyToEnlivenCar = true;
							isResetting = false;
						}
					}
				);
			}
		}
	);
}

function UpdateFuelGauge(){
	
	if(TotalMilSeconds > 0){
		fuelLeft = Math.round(((fuelTank - (TotalMilSeconds / 1000)) / fuelTank) * 100);
		
		if(fuelLeft <= 0){
			fuelLeft = 0;
		}
			
		fuelBarHTML = fuelLeft + "%&nbsp;";
	} else {
		fuelLeft = 100;
		fuelBarHTML = "100% (" + fuelTank + ")&nbsp;";
	}
	
	
	
	$('#fuelProgBar').css("width", fuelLeft + "%");
	$('#fuelProgBar').html(fuelBarHTML);
}

function CreateTimer_line(input, TimerID, Time) {

	keyInputTime = (new Date()).valueOf();
    var functionKeyPressed = true;     // used to flag when function key (delete, shift...) is pressed
	var key_entered = input;
	var keyboardSelected = "QWERTY";
	
	if(typeof key_entered === "object"){
		if(keyboardSelected == "DVORAK"){
			key_entered = d_keycode.d_getValueByEvent(input);
		} else if(keyboardSelected == "QWERTY_UK"){
			key_entered = UK_QWERTY_keycode.UK_QWERTY_getValueByEvent(input);
		} else if(keyboardSelected == "COLEMAK"){
			key_entered = cole_keycode.cole_getValueByEvent(input);
		} else{       // default to QWERTY
			key_entered = keycode.getValueByEvent(input);
		}
	}
    
    var functionKeyPressed = !(/^.$/.test(key_entered));     // used to flag when function key (delete, shift...) is pressed
	
	
    var lineMiddle = (middleCursor*32)-8;
	
	if(isResetting){
		return false;
	}
    
    if (TotalMilSeconds==undefined || TimeWasReset) {
		cursor = 24;        //first letter has "<span class="plainText">" before it
		localCursor = 0;
		TimeWasReset = false;
		RoundTime = Time;
		
		sWatch = document.getElementById(TimerID);
		TotalMilSeconds = Time;

		UpdateTimer()
		ss();
		
		// need this to make shift-return hotkey not start road moving
		if(!functionKeyPressed)
			startRoad($('.yellowBox'));
    }
    
    if (TimeWasReset){
		TotalMilSeconds = Time;
		TimeWasReset = false;   //reset TimeWasReset flag        
    }
    
    // This makes the box disabled when time is up
    if (TimeIsStopped){
        return false;
    }
    
    // Get text in RTE box
    var newRTEText = "";
    var RTELetter = lineDivText.substring(lineMiddle,lineMiddle+1);
	
	// add letter to letter bank if not first letter
	// "responseTimeStart" = datetime of end of last key processed (start of time for present key)
	if(!functionKeyPressed && responseTimeStart != 0){
		
		// see if function key has been pressed in the meantime, if so add that time as well
		if(savedResponseTimeStart != 0){
			responseTimeStart = savedResponseTimeStart;
			savedResponseTimeStart = 0;
		} 
		responseTime = keyInputTime - responseTimeStart;
		
		if(RTELetter in letterCounts){
			letterCounts[RTELetter] = letterCounts[RTELetter] + 1;
			letterResponseTimes[RTELetter] = letterResponseTimes[RTELetter] + responseTime;
			if(RTELetter in letterMistakes){
				letterPercentMisses[RTELetter] = Math.round((letterMistakes[RTELetter] / letterCounts[RTELetter]) * 100);	// gives number as a percent with 1 decimal place
			}
		} else {
			letterCounts[RTELetter] = 1;
			letterResponseTimes[RTELetter] = responseTime;
		}
	} else if(functionKeyPressed && savedResponseTimeStart == 0){
		savedResponseTimeStart = responseTimeStart;
	}

    // event.keyCode: 97-122 = a-z, 65-90 = A-Z, 8 = backspace, 32 = space
    if( key_entered == RTELetter){  /* characters match */
        functionKeyPressed = false;
        
        if(RTELetter == " "){
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "goodSpace" 
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30) 
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle-16)/32)+1, ((cursor+lineMiddle-16)/32)+2) + "</span>";   
            wordsTyped++;
        }
        else{
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "goodEntry"
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30) 
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle-16)/32)+1, ((cursor+lineMiddle-16)/32)+2) + "</span>";  
        }
        
        cursor += 32;
        lettersTypedCorrectly += 1;
        totalEntriesTyped += 1;
        
        if(phaseShift != 0){
            phaseShift = 0;     // correct entry resets any possible phase shift
        }
        
        if(localCursor < 20) {localCursor += 1;}
    } else if(!functionKeyPressed){
        functionKeyPressed = false;
        
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION START >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // // -------------------------------------------------------------------------------------
        // **FIRST** see if we are looking for a phase shift
        if(phaseShift != 0){
            if(phaseShift == 1){
                if(key_entered == lineDivText.substring(lineMiddle+32, lineMiddle+33)){
                    //we have a phase shift to the right
                    var shiftRight = true;
                }
            }
            else{   //phase shift = -1
                if(key_entered == lineDivText.substring(lineMiddle-32, lineMiddle-31)){
                    var shiftLeft = true;
                }
            }
            phaseShift = 0;     // phase shift has been corrected and realigned
            
        }
        // **THEN** see if we should look for a phase shift to the right (positive time)
        else if(key_entered == lineDivText.substring(lineMiddle+32, lineMiddle+33)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = 1;     //only set the flag if the user wants to do phase shift correction
            }
        }
        // see if we should look for a phase shift to the left (negative time)
        else if(key_entered == lineDivText.substring(lineMiddle-32, lineMiddle-31)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = -1;    //only set the flag if the user wants to do phase shift correction
            }   
        }
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION END >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // -------------------------------------------------------------------------------------
        
        if(shiftRight){
            newRTEText = lineDivText.substring(64,lineMiddle-11) + "phaseRght" 
                         + lineDivText.substring(lineMiddle-2,lineMiddle+21) + "goodEntry"
                         + lineDivText.substring(lineMiddle+30,lineMiddle+53) + "nxtLetter"
                         + lineDivText.substring(lineMiddle+62) + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>"
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+40)/32)+1, ((cursor+lineMiddle+40)/32)+2) + "</span>";
            cursor +=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   Do not count since we are jumping ahead by 1 letter
            shiftRight = false;  // phase shift should have been successfully applied by now
            
            if(localCursor < 20) {localCursor += 1;}
        }
        else if(shiftLeft){
            newRTEText = lineDivText.substring(0,lineMiddle-43) + "phaseLeft" 
                         + lineDivText.substring(lineMiddle-34);
            cursor -=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   For some reason double counts with this line
            shiftLeft = false;  // phase shift should have been successfully applied by now
            
            if(localCursor < 20) {localCursor += 1;}
        }    
        else if(RTELetter == " "){   /* put an _ if space typed in wrong */
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "bad_Space"
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30)
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>";  
            wordsTyped++;

        }
        else{
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "bad_Entry"
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30) 
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>";  
			// count bad letter
			if(responseTime != 0){
				if(RTELetter in letterMistakes){
					letterMistakes[RTELetter] = letterMistakes[RTELetter] + 1;
				} else {
					letterMistakes[RTELetter] = 1;
				}
				letterPercentMisses[RTELetter] = Math.round((letterMistakes[RTELetter] / letterCounts[RTELetter]) * 100);	// gives number as a percent with 1 decimal place
			}
        }
        
        cursor += 32;
        lettersTypedIncorrectly += 1;
        totalEntriesTyped += 1;
        
        if(localCursor < 20) {localCursor += 1;}
    }
    else if( key_entered == "backspace" && cursor > 24 && localCursor > 0){      /*no match and it is a backspace*/ 
        functionKeyPressed = false;
        // backstep first
            cursor -= 32;
            localCursor -= 1;
            
        if(lineDivText.substring(lineMiddle-43, lineMiddle-34) == "bad_Space"){
            wordsTyped--;
            //totalEntriesTyped -= 1;
        }
        
        if(lineDivText.substring(lineMiddle-43, lineMiddle-34) == "goodSpace"){
            wordsTyped--;
        }
        
        if(lineDivText.substring(lineMiddle-43, lineMiddle-34) == "bad_Space"
                || lineDivText.substring(lineMiddle-43, lineMiddle-34) == "bad_Entry"
                || lineDivText.substring(lineMiddle-43, lineMiddle-34) == "phaseLeft"){
            badEntriesDeleted++;
            //totalEntriesTyped -= 1;
        }
            
        if(cursor < middleCursor){
            //localCursor = lineDivText.length - 8;
            newRTEText = '<span class="hiddenTxt"> </span>'
                         + lineDivText.substring(0, lineMiddle-43) + "nxtLetter" 
                         + lineDivText.substring(lineMiddle-34,lineMiddle-11) + "plainText"
                         + lineDivText.substring(lineMiddle-2,lineDivText.length-32);    
        } 
        else{
           newRTEText = //textString.substring((cursor-(lineMiddle+8))/32, ((cursor-(lineMiddle+8))/32)+1)
                        '<span class="hiddenTxt"> </span>'
                        + lineDivText.substring(0, lineMiddle-43) + "nxtLetter" 
                         + lineDivText.substring(lineMiddle-34,lineMiddle-11) + "plainText"
                         + lineDivText.substring(lineMiddle-2,lineDivText.length-32);        
        }   

        deletes++;
    }
    
    if(!functionKeyPressed){
        lineDivText = newRTEText;
        lineDiv.innerHTML = formatForInnerHTML(newRTEText);
    }
	
	if( (((cursor+lineMiddle-16)/32)+1) >= cleanTextString.length ){
		cleanTextString += ' ' + cleanTextString;
	}
	
	responseTimeStart = (new Date()).valueOf();		// this should be last to minimize the time added from js code having to run
    return false;
}

function InitializeTimer_line(Time, TimerID){
    cursor = 24;        //first letter has "<span class="plainText">" before it
    localCursor = 24;
    cutoff = 0;
    lineNum = 0;
    lineDivText = "";
    sWatch = document.getElementById(TimerID);
    TotalMilSeconds = Time;
	totalDeletes = 0;
	pShiftWeight = 0;
	t[4]=t[3]=t[2]=t[1]=t[0]=0;
	t[7]=1;
	
    load();
    GetText();
}

function ss() {
	t[t[2]]=(new Date()).valueOf();
	t[2]=1-t[2];

	if (0==t[2]) {
		clearInterval(t[4]);
		t[3]+=t[1]-t[0];
		t[4]=t[1]=t[0]=0;
		Tick();
	} else {
		t[4]=setInterval(Tick, 50/*43*/);
	}
}

function load() {
	t[5]=new Date(1970, 1, 1, 0, 0, 0, 0).valueOf();
	t[6]=document.getElementById('sWatch');

	Tick();
}

function Tick() {

	if ( TotalMilSeconds > (fuelTank * 1000) ){
        StopTime();
		FinishAnimations();
        return;
    }
	
	if (t[2]) t[1]=(new Date()).valueOf();
	t[6].value=format(t[3]+t[1]-t[0]);
	UpdateTimer();
	UpdateStats();
	UpdateFuelGauge();
	moveRoad();
	if(TotalMilSeconds > 2000 && TotalMilSeconds < (fuelTank * 1000))
		SetRoadSpeed(getNetWPM());
}
function format(ms) {
	// used to do a substr, but whoops, different browsers, different formats
	// so now, this ugly regex finds the time-of-day bit alone
	var d=new Date(ms+t[5]).toString()
		.replace(/.*([0-9][0-9]:[0-9][0-9]:[0-9][0-9]).*/, '$1');
	var x=String(ms%1000);
	while (x.length<3) x='0'+x;
	d+='.'+x;
	return d;
}

function r() {
	if (t[2]) ss();
	t[4]=t[3]=t[2]=t[1]=t[0]=0;
	Tick();
	t[7]=1;
}

function StopTime() {
	UpdateTimer();
	UpdateStats();
	TimeEnd();
}

function UpdateTimer() {
    TotalMilSeconds = t[3]+t[1]-t[0];
	var TimeStr = msecsToTimeStr(TotalMilSeconds);
	sWatch.innerHTML = TimeStr.substr(0, TimeStr.indexOf(".") + 2);
	timeDivider = (timeDivider + 1) % 20;
}

function updateGuiHS(){
	
	var wordsTyped = getGrossWPM() / getTimeInMins();
	
	if(TotalMilSeconds > 0){
		//newHS = Math.round(getNetWPM() * (TotalMilSeconds / 600));
		newHS = Math.round(getGrossWPM() * getTimeInMins() * 10)/0.1 - (lettersTypedIncorrectly - getNumFixedMistakes()) * 100;
	} else {
		newHS = '-'
	}
	
	var TimeStr = msecsToTimeStr(TotalMilSeconds);
	TimeStr = TimeStr.substr(0, TimeStr.indexOf(".") + 2);
	
	if(TimeStr.length < 5){
		timeUnits = "sec";
	} else{
		timeUnits = "min";
	}
	
	var dialogHTML = '<div>Time Elapsed: </div>';
	dialogHTML += '<div>Words Typed: </div>';
	dialogHTML += '<div>Unfixed Errors: </div>';
	dialogHTML += '<div>Net Typing Speed: </div>';
	dialogHTML += '<div>Accuracy: </div>';
	$("#dialogScoreLeftCol").html(dialogHTML);
	
	dialogHTML = '<div>' + TimeStr + " " + timeUnits + '</div>';
	dialogHTML += '<div>' + Math.round(getGrossWPM() * getTimeInMins() * 10)/10 + ' (x100 pts)</div>';
	dialogHTML += '<div>' + (lettersTypedIncorrectly - getNumFixedMistakes()) + ' <span class="redText">(-x100 pts)</span></div>';
	dialogHTML += '<div>' + getNetWPM() + ' WPM</div>';
	dialogHTML += '<div>' + getAccuracy() + '%</div>';
	$("#dialogScoreRightCol").html(dialogHTML);
	
	
	dialogHTML = '<h3 id="dialogFinalScoreH3"><span>Score = </span><span id="dialogFinalScore">' + newHS + ' pts</span></h3>';
	$("#dialogFinalScore").html(dialogHTML);
	
	showDialog("dialog-score", "OK", 430,650);
	$('#trgScore').html(newHS + " pts");
}

function getHighScores(){
	var myJSONdata;
	/*$.post('/getData.php', { type: "tta", filter: $('#filterYourHS').val()},
		function(output){
			myJSONdata = $.parseJSON(output);
			var currDateTime = new Date(myJSONdata["currDateTime"].replace(/-/g,'/'));
			var loggedInUname = myJSONdata["loggedInUsername"];
			var inMaintenanceMode = myJSONdata["maint_mode"];
			var rhs = myJSONdata["allUsersResults"];
			var phss = myJSONdata["loggedInResults"];
			
			var rhsHTML = '';
			var phssHTML = '';
			
			if(inMaintenanceMode){
				rhsHTML = '<table><tr><td class="orangeCTA largeBtn" colspan="5"><div>Site is under maintenance</div><div>High scores are temporarily disabled and will not be saved!</div></td></tr></table>';
				phssHTML = '<table><tr><td class="orangeCTA largeBtn" colspan="4"><div>Site is under maintenance</div><div>High scores are temporarily disabled and will not be saved!</div></td></tr></table>';
			} else {
				rhsHTML = '<table><tr><td class="blueCTA largeBtn" colspan="5">Nobody has a recent high score yet!</td></tr></table>';
				phssHTML = '<table><tr><td class="blueCTA largeBtn" colspan="4">You do not have any scores yet!</td></tr></table>';
			}
			
			if(!inMaintenanceMode){
				if(rhs != null && rhs.length > 0){
					rhsHTML = '<table><tr><th>#</th><th>Name</th><th>Seconds</th><th>Type</th><th>Time Ago</th></tr>';
					var decoration = '';
					var adjustedName = '';
					
					for(var ii = 0; ii < rhs.length; ii++){
						if(rhs[ii].uname == loggedInUname)
							decoration = ' class="uname borderRad_4 orangeBkgd boldFont centerTxt"';
						else
							decoration = ' class="uname"';
						
						if(rhs[ii].uname.length > 13)
							adjustedName = rhs[ii].uname.substring(0, 11) + "...";
						else
							adjustedName = rhs[ii].uname;
						
						rhsHTML += '<tr><td>' + (ii + 1) + '</td><td' + decoration + ' title="' + rhs[ii].uname + '"><a href="/user/' + rhs[ii].uname + '/test-stats"><img class="circle-image tinyPic" src="' + rhs[ii].profile_pic + '"><span class="adjName">' + adjustedName + '</span></a></td><td>' + (rhs[ii].val / 1000).toFixed(3).toString() + '</td><td>' + alphaTypeDecode[rhs[ii].type] + '</td><td class="convertToTimeAgo">' + rhs[ii].dt + '</td></tr>';
					}
					rhsHTML += '</table>';
				}
			}
			
			$('#alphaHS').html(rhsHTML);
			
			if(!inMaintenanceMode){
				if(phss == 'not logged in'){
					phssHTML = '<table><tr><td class="orangeCTA mediumBtn" colspan="4"><a href="https://www.learnspeedtyping.com/register.php">Register</a> or <a href="https://www.learnspeedtyping.com/login.php">Login</a> to keep track of your scores.</td></tr></table>';
				} else if(phss != null && phss.length > 0){
					phssHTML = '<table><tr><th>#</th><th>Seconds</th><th>Type</th><th>Time Ago</th></tr>';
					
					for(var ii = 0; ii < phss.length; ii++){
						phssHTML += '<tr><td>' + (ii + 1) + '</td><td>' + (phss[ii].val / 1000).toFixed(3).toString() + '</td><td>' + alphaTypeDecode[phss[ii].type] + '</td><td class="convertToTimeAgo">' + phss[ii].dt + '</td></tr>';
						if(highScores[(phss[ii].type - 1)] > phss[ii].val)
							highScores[(phss[ii].type - 1)] = phss[ii].val;
					}
					phssHTML += '</table>';
					
					// show delete scores button
					$('#clearDataBtn').show();
				}
			}
			
			$('#personalAlphaHS').html(phssHTML);
	
			$('.convertToTimeAgo').each(function(){
				 var earlyDate = new Date($(this).html().replace(/-/g,'/'));
				 var timeDiffString = readableTimeDifference(earlyDate, currDateTime);
				 $(this).html(timeDiffString);
				 if(timeDiffString.indexOf("mins") > 0 || timeDiffString.indexOf("secs") > 0)
					$(this).addClass("borderRad_4 dkBlueBkgd boldFont centerTxt");
			 });
			 
			 updateGuiHS();
	});*/
}


function Reset(ResetTime, TimerID) {
	isResetting = true;
	$('#centerEntryModule').removeClass('passed failed');
	$('.topLeftLink').show();
	if($('#dialog-score').hasClass("ui-dialog-content")){
		$('#dialog-score').dialog("close");
	}
	
	resetRoad($('#roadLines'));
	
    sWatch = document.getElementById(TimerID);
    TotalMilSeconds = ResetTime;
    lettersTypedCorrectly = 0;
    lettersTypedIncorrectly = 0;
    badEntriesDeleted = 0;
    totalEntriesTyped = 0;
    wordsTyped = 0;
	totalDeletes = 0;
	pShiftWeight = 0;
    
    UpdateTimer();   
    UpdateStats();
	clearInterval(t[4]);
	TimeIsStopped = true;
	getHighScores();
	

    InitializeTimer_line(ResetTime, TimerID);
    TimeWasReset = true;
	TimeIsStopped = false;
    
    $('.mainDivInputs').focus();    //assume user wants to type after clicking reset
    TimeBegin();
    t[6].value=format(0);
}

function TimeBegin(){
    $("#divCover").css("z-index", "0");
}

function TimeEnd(){
	/*var alphaType = $('#textTypeSelected').val();
	if(TotalMilSeconds > 0){
		$.post('/setData.php', { type: "tta", msecs: TotalMilSeconds, alpha_type: alphaType},
			function(output){
		});	
	}*/
	
	$("#divCover").css("z-index", "400");
    clearInterval(t[4]);
	
	timeDivider = 1		// set to guarantee last wpm calculation
    UpdateResults();
	
    TimeIsStopped = true;
}

function formatForInnerHTML(StringToFormat){
    var StringToReturn = "";
    
    StringToFormat = StringToFormat.replace(/> </g, ">&nbsp;<");
    StringToReturn = '<div id="blockLine0" class="blockLines">' + StringToFormat + "</div>";

    return StringToReturn;
}

function setupFocus(){
    $('.mainDivInputs').bind('blur', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('click', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('mouseleave', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('mouseout', function() {
        this.focus();
    });
    
    $('.mainDivInputs').focus(function() {
        ApplyColorsFocused();
    });
    
    $('.mainDivInputs').blur(function() {
        ApplyColorsBlurred();
    });
}

keycode = {
    getKeyCode : function(e) {
        var keycode = null;
        if(window.event) {
            keycode = window.event.keyCode;
        }else if(e) {
            keycode = e.which;
        }
        return keycode;
    },
    getKeyCodeValue : function(keyCode, shiftKey) {
        shiftKey = shiftKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.modifiedByShift[keyCode];
        }else {
            value = this.keyCodeMap[keyCode];
        }
        return value;
    },
    getValueByEvent : function(e) {
        return this.getKeyCodeValue(this.getKeyCode(e), e.shiftKey);
    },
    keyCodeMap : {
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"capslock", 27:"escape", 32:" ", 33:"pageup",
        34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"+", 44:"printscreen", 45:"-", 46:"delete", 47:"/",
        48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:";",
        61:"=", 65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l",
        77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z",
        96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9",
        106: "*", 107:"+", 109:"-", 110:".", 111: "/",
        112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12",
        144:"numlock", 145:"scrolllock", 186:";", 187:"=", 188:",", 189:"-", 190:".", 191:"/", 192:"`", 219:"[", 220:"\\", 221:"]", 222:"'"
    },
    modifiedByShift : {
        32: " ", 65:"A", 66:"B", 67:"C", 68:"D", 69:"E", 70:"F", 71:"G", 72:"H", 73:"I", 74:"J", 75:"K", 76:"L",
        77:"M", 78:"N", 79:"O", 80:"P", 81:"Q", 82:"R", 83:"S", 84:"T", 85:"U", 86:"V", 87:"W", 88:"X", 89:"Y", 90:"Z", 192:"~", 48:")", 49:"!", 50:"@", 51:"#", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 109:"_", 61:"+",
        219:"{", 221:"}", 220:"|", 59:":", 222:"\"", 186:":", 187:"+", 188:"<", 189:"_", 190:">", 191:"?"/*,
        96:"insert", 97:"end", 98:"down", 99:"pagedown", 100:"left", 102:"right", 103:"home", 104:"up", 105:"pageup"*/
    }
};

d_keycode = {
    d_getKeyCode : function(e) {
        var d_keycode = null;
        if(window.event) {
            d_keycode = window.event.keyCode;
        }else if(e) {
            d_keycode = e.which;
        }
        return d_keycode;
    },
    d_getKeyCodeValue : function(d_keyCode, shiftKey) {
        shiftKey = shiftKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.d_modifiedByShift[d_keyCode];
        }else {
            value = this.d_keyCodeMap[d_keyCode];
        }
        return value;
    },
    d_getValueByEvent : function(e) {
        return this.d_getKeyCodeValue(this.d_getKeyCode(e), e.shiftKey);
    },
    d_keyCodeMap : {
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"capslock", 27:"escape", 32:" ", 33:"pageup",
        34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"}", 44:"printscreen", 45:"[", 46:"delete", 47:"z",
        48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:"s",
        61:"]", 65:"a", 66:"x", 67:"j", 68:"e", 69:".", 70:"u", 71:"i", 72:"d", 73:"c", 74:"h", 75:"t", 76:"n",
        77:"m", 78:"b", 79:"r", 80:"l", 81:"'", 82:"p", 83:"o", 84:"y", 85:"g", 86:"k", 87:",", 88:"q", 89:"f", 90:";",
        96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9",
        106: "*", 107:"}", 109:"[", 110:"v", 111: "z",
        112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12",
        144:"numlock", 145:"scrolllock", 186:"s", 187:"]", 188:"w", 189:"[", 190:"v", 191:"z", 192:"`", 219:"/", 220:"\\", 221:"=", 222:"-"
    },
    d_modifiedByShift : {
        32: " ", 65:"A", 66:"X", 67:"J", 68:"E", 69:">", 70:"U", 71:"I", 72:"D", 73:"C", 74:"H", 75:"T", 76:"N",
        77:"M", 78:"B", 79:"R", 80:"L", 81:"\"", 82:"P", 83:"O", 84:"Y", 85:"G", 86:"K", 87:"<", 88:"Q", 89:"F", 90:":", 192:"~", 48:")", 49:"!", 50:"@", 51:"#", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 109:"{", 61:"}",
        219:"?", 221:"+", 220:"|", 59:"S", 222:"_", 186:"S", 187:"}", 188:"W", 189:"{", 190:"V", 191:"Z"/*,
        96:"insert", 97:"end", 98:"down", 99:"pagedown", 100:"left", 102:"right", 103:"home", 104:"up", 105:"pageup"*/
    }
};

function UpdateStats(){    
    
	var wpmValue = $('#wpmValue').get();
	if((wpmValue.length != 0) && (timeDivider == 1)){
		wpmValue[0].innerHTML = getNetWPM();
	}
    
    var accuracyValue = $('#accuracyValue').get();
	if(accuracyValue.length != 0){
		accuracyValue[0].innerHTML = getAccuracy() + "<span> %</span>";
	}
	
	var pCompleteValue = $('#pCompleteValue').get();
	// only update percent complete if we are on typing tutor page and we are either 100% complete or it is an even second
	if((pCompleteValue.length != 0) && ( ((lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight) == textString.length) || ((TotalMilSeconds % 2) == 0) )){
		pCompleteValue[0].innerHTML = getPComplete() + "<span> %</span>";
	}
	
}

function UpdateResults(){  
	updateGuiHS();
	testCounter++;
}

function getKPM(){
    return Math.round(((lettersTypedCorrectly + lettersTypedIncorrectly)/(getTimeElapsed())) * 60);
}

function getTimeElapsed(){
    if(typeof Timer == "undefined"){
        return TotalMilSeconds / 1000;
    }
    return (RoundTime - TotalMilSeconds);
}

function getEPM(){
    return Math.round(((lettersTypedIncorrectly - getNumFixedMistakes()) / getTimeElapsed() ) * 60);
}


function getNetWPM(){    
    var netWPM = 0;
    var TotalLettersTyped = lettersTypedCorrectly + lettersTypedIncorrectly;
    
    if(getTimeElapsed() != 0) {
        netWPM = TotalLettersTyped / getTimeElapsed();
        netWPM *= 12;
        netWPM = netWPM - getEPM();

        if(netWPM == "Infinity" || netWPM < 0){
            netWPM = 0;
        }
        netWPM = Math.round(netWPM);
    }
    
    return netWPM;
}

function getGrossWPM(){    
    var grossWPM = 0;
    var TotalLettersTyped = lettersTypedCorrectly + lettersTypedIncorrectly;
    
    if(getTimeElapsed() != 0) {
        grossWPM = TotalLettersTyped / getTimeElapsed();
        grossWPM *= 12;

        if(grossWPM == "Infinity" || grossWPM < 0){
            grossWPM = 0;
        }
        grossWPM = Math.round(grossWPM);
    }
    
    return grossWPM;
}

function getAccuracy(){    
    var Accuracy = 100;
    var TotalWordsTyped = (lettersTypedCorrectly + lettersTypedIncorrectly - badEntriesDeleted + getNumFixedMistakes());
    
    if(getTimeElapsed() != 0) {
        
        Accuracy = (lettersTypedCorrectly / TotalWordsTyped) * 100;
        Accuracy *= 10;
        Accuracy = Math.round(Accuracy);
        Accuracy /= 10;
        
        if(TotalWordsTyped == 0){
            Accuracy = 100;
        }
        if(Accuracy < 0){
            Accuracy = 0;
        }
    }
    
    return Accuracy;
}

function getTimeInMins(){
    timeInMins = TotalMilSeconds / 600;
	timeInMins = Math.round(timeInMins) / 100;
    
    return timeInMins;
}

function getNumFixedMistakes(){
    
    var numFixMiss = 0;
    
    if(lettersTypedCorrectly >= badEntriesDeleted){
        numFixMiss = badEntriesDeleted;
    }
    else{
        numFixMiss = badEntriesDeleted - (badEntriesDeleted - lettersTypedCorrectly);
    }
    
    return numFixMiss;
}

function getPComplete(){

	var percentComplete;
	
	percentComplete = (lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight) / (textString.length - 1) * 100;
    percentComplete = Math.floor(percentComplete);		// floor instead of round so only when complete will it say 100%
	
	return percentComplete;
}

function showDialog(id, OKbuttonText, height, width){
	
    $( "#" + id ).dialog({
      resizable: false,
      height:height,
	  width:width,
      modal: true,
	  draggable: false,
      buttons: [
		{
			text: OKbuttonText,
			click: function() {
				$( this ).dialog( "close" );
				if(id == "dialog-fuel"){
					returnVal = $("#fuelAmt").val();
					
					if(returnVal == parseInt(returnVal,10) && returnVal > 0){
						fuelTank = returnVal;
					} else {
						fuelTank = 60;
					}
					
					UpdateFuelGauge();
				}
			}			
        }, {
			text: "Cancel",
			click: function() {
				  $( this ).dialog( "close" );
				}
		}
      ],
	  open: function() {
		  $(this).parent().find(":contains('Cancel')").blur();
		  $(this).parent().find(":contains('Delete')").blur();
		  $(this).parent().find(":contains('OK')").blur();
	  }
    });
}
