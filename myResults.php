<?php 
	$currPage = "myResults";
	require_once 'core/init.php';
	$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

	$db = DB::getInstance();
	$user = new User();
	$loggedInID = ($user->isLoggedIn()) ? $user->data()->id : null;
	$debugMode = $user->hasPermission('debug') || Config::get('constants/debugMode');
	
	// r = {n length wpm} 0 {3 digit accuracy} {13 digit timestamp} {3 digit test time}
	// d = date
	// n = name
	// c = checksum
	// rt = result text
	// rtc = result text checksum
	$pn = 524286;
	$accuracy = 0;
	$wpm = 0;
	$defaultReportText = 'Typing Test Results';
	$testTime = 0;
	$decodedName = '';
	$GMTdateString = '';
	$UTCstring = '';
	$textTitle = '';
	
	$testIndex = Input::get('ttid');

	if($loggedInID != null && ($testIndex != null)){
		
		$userData = $user->data();

		// first get users name to personalize results
		if(!empty($userData->firstname) || !empty($userData->lastname)){
			$decodedName = $user->fullName() . '\'s ';
		}
		else{
			$decodedName = $userData->username . '\'s ';
		}
		
		$decodedReportText = "Typing Test Results";
		
		// get typing test
		$db_obj = $db->get('typing_tests', array('typing_tests.id', '=', $testIndex, 'user_id', '=', $loggedInID), 'wpm, accuracy, timestamp, total_time_secs, title', null, array('test_texts', 'text_index', 'id'), array(9999999,0));	
		$tt = $db_obj->first();

		$accuracy = $tt->accuracy;
		$wpm = $tt->wpm;
		$testTime = ($tt->total_time_secs >= 60) ? round($tt->total_time_secs / 60, 0) . " minute test" : $tt->total_time_secs . " second test";
		$UTCstring = " UTC";
		$textTitle = 'with "' . $tt->title . '" Text';
		
		//$dateString = strtotime($tt->timestamp); 
		$dateString = $tt->timestamp; 
		$GMTdateString = "" . date("m/d/y - H:i:s", strtotime($dateString)) . " GMT"; 
		
		$mustBeLoggedInNote = "<b>*Note:</b> These results are only viewable when logged into this account.";
	}
	else {
		// ***************************
		// Get Date ['d']
		$intDate = $_GET["d"] / $pn;
		
		if(is_float($intDate) || $intDate == null){
			$dateString = '';
		} else {
			$strDate = (string)$intDate;

			$dateString = date("M d, Y", $strDate);
		}
		
		// ***************************
		// Get Name ['n'] and ['c']
		if(isset($_GET["n"]) && isset($_GET["c"]))
			$decodedName = decodeAscii($_GET["n"], ($_GET["c"] / $pn));
		
		if($decodedName != ''){
			$decodedName .= "'s ";
		}
		
		// ***************************
		// Get Result Text ['rt'] and ['rtc']
		$resultText = $_GET["rt"];
		$resultTextChksum = $_GET["rtc"] / $pn;
		$decodedReportText = decodeAscii($resultText, $resultTextChksum);
		
		if($decodedReportText == ''){
			$decodedReportText = $defaultReportText;
		}
		
		// ***************************
		// Get Results ['r']
		$intResult = $_GET["r"] / $pn;
		

		if(is_float($intResult)){
			$strResult = 'Your link produced invalid results';
		} else {
			$strResult = substr((string)$intResult, 1);
			$wpm = substr($strResult, 0, -6);
			
			$accuracy = substr($strResult, -6, 3);
			$testTime = substr($strResult, -3);
			
			if(substr($wpm, 0, 2) == "00"){
				$wpm = $wpm[2];
			} else if ($wpm[0] == '0'){
				$wpm = substr($wpm, 1);
			}
			
			if($accuracy != '100'){
				$accuracy = (string)((int)substr($accuracy, -3, 3)/10);
			}
			
			if($decodedReportText == $defaultReportText){
				$testTime = ((int)ltrim($testTime, '0') / 60) . ' minute test';
			} else{
				$testTime = ltrim($testTime, '0') . ' characters long lesson';
			}
		}
			
		
	}
	
	// function to decode Ascii string and check with checksum
	function decodeAscii($asciiStr, $chkSum) {
		$asciiDec = 0;
		$decodedStr = '';
		$decodedCheckSum = 0;
	
		for($ii = 0; $ii < strlen($asciiStr); $ii+=2){
			$asciiDec = hexdec(substr($asciiStr, $ii, 2));
			$decodedCheckSum = $decodedCheckSum + $asciiDec;
			$decodedStr .= chr($asciiDec);
		}

		if(is_float($chkSum) || $chkSum != $decodedCheckSum || $asciiStr == ''){
			$decodedStr = '';
		}
		
		return $decodedStr;
	}
	
$htmlTitle = $decodedName . ' ' . $decodedReportText;
$htmlDescription = "Results page for typing tests and lessons.";
$cssFiles = "bet";
$noWidgets = true;
$metaNoIndex = true;
require_once 'includes/overall/header.php';
?>
		<div id="container">
			<?php echo getAd("leadAd", null, $debugMode);?>
			
			<div id="resultDiv" class="to-print">
				<h1><?php echo $decodedName . ' ' . $decodedReportText;?></h1>
				<div class="resultRegText">Achieved on <span id="localDateTime"></span></div>
				<div><?php echo $GMTdateString; ?></div>
				<div id="WPM_Rtext" class="resultStat">Speed: <span id="WPM_Result"><?php echo $wpm;?> WPM</span></div>
				<div id="Accur_Rtext" class="resultStat">Accuracy: <span id="Accur_Result"><?php echo $accuracy;?> %</span></div>
				<div class="resultRegText">On a <?php echo $testTime;?></div>
				<div id="textTitle" class="resultRegText"><?php echo $textTitle;?></div>
				
				<!--<div class="responsiveCols">
					<div>
						<span class="leftCol">Test Length:</span>
						<span class="rightCol">3 seconds</span>
					</div>
					<div>
						<span class="leftCol">Text:</span>
						<span class="rightCol">"The Tortoise and the Hare"</span>
					</div>
				</div>-->
			</div>
			
			
			
			<div id="afterResultsDiv" class="group">
				<div id="printBtnContainer">
					<button id="printBtn" class="submitButtonCarrot mediumBtn" onclick="window.print();">Print Results</button>
					<div id="loginNote"><?php echo $mustBeLoggedInNote; ?></div>
				</div>
				
				<?php echo getAd("lgRectAd", null, $debugMode);?>
			</div>
			
			
		</div>
		
		<script type="text/javascript">
		var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

		window.onload = function(){
			var localDT = new Date('<?php echo $dateString . $UTCstring;?>');
			document.getElementById('localDateTime').innerHTML = monthNames[localDT.getMonth()] + " " + localDT.getDate() + ", " + localDT.getFullYear(); 
		}
		
		function pad(n, width, z) {
		  z = z || '0';
		  n = n + '';
		  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
		}
		</script>
        
<?php

	require_once 'includes/overall/footer.php';
?>
