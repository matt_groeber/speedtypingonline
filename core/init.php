<?php
// get pagename (eg. index.php) --> used for forcing password change upon reset
$current_file = explode('/', $_SERVER['SCRIPT_NAME']);
$current_file = end($current_file);

session_start();

$protocol = $_SERVER['HTTPS'] ? 'https' : 'http';
$siteURL = $protocol . '://' . $_SERVER['SERVER_NAME'];
$isMobile = (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT'] );

$GLOBALS['config'] = array(
	'mysql' => array(
		'host' 		  => '127.0.0.1',
		'username' 	  => 'STO_genUser',
		'password' 	  => '4Frjxp6J43JsvfXu',
		'db' 		  => 'STO',
		'maint_mode'  => false			// this (should) cutoff all database traffic when true
	),
	'remember' => array(
		'cookie_name'	=> 'hash',
		'cookie_expiry' => 604800,
		'cookie_loginRedirect' => 'loginRedirect'
	),
	'session' => array(
		'session_name' => 'user',
		'token_name' => 'token',
		'token_name_w' => 'token_w',
		'token_name_m' => 'token_m'
	),
	'constants' => array(
		'debugMode' => false,  // grey out ads, 3s test option, and very short lessons
		'rootUrl' => $siteURL,
		'max_entries' => 10,
		'max_tt_entries' => 10,
		'max_lh_entries' => 10,
		'max_tta_entries' => 5,
		'max_fft_entries' => 3,
		'wpmLessonTarget' => 10,
		'accLessonTarget' => 90,
		'touchTypingAchieved' => 30,
		'alphaTypeEnum' => array("NORMAL" => 1, "SPACES" => 2, "BACKWARDS" => 3, "RANDOM" => 4, "BACKWARDS_SPACES" => 5),
		'alphaTypeDecode' => array("N/A", "Normal", "w/ Spaces", "Backwards", "Random", "Backwards /w Spaces"),
		'keyboards' => array("QWERTY", "DVORAK", "COLEMAK", "QWERTY_UK", "COLEMAK_UK", "AZERTY", "QWERTZ", "NUMPAD", "QWERTZ_SF"),	// always add keyboards to the end of list
		'keyboardsLabels' => array("QWERTY", "Dvorak", "Colemak", "QWERTY UK", "Colemak UK", "AZERTY", "QWERTZ", "Numpad", "QWERTZ SwissFrench"),
		'userGroupEnum' => array("STANDARD_USER" => 1, "ADMIN" => 2, "TEACHER" => 3, "STUDENT" => 4),
		'tlDateFilterEnum' => array("TODAY" => 0, "YESTERDAY" => 1, "LAST7DAYS" => 2, "LAST30DAYS" => 3, "THIS_WEEK" => 4, "LAST_WEEK" => 5, "THIS_MONTH" => 6, "LAST_MONTH" => 7, "ALL_TIME" => 8, "CUSTOM_RANGE" => 9)
	)
);

spl_autoload_register(function($class) { 
	require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/' . $class . '.php';
});

require_once $_SERVER['DOCUMENT_ROOT'] . '/functions/general.php';

// 'remember me' functionality
if(Cookie::exists(Config::get('remember/cookie_name')) && !Session::exists(Config::get('session/session_name'))){
	$hash = Cookie::get(Config::get('remember/cookie_name'));
	$hashCheck = DB::getInstance()->get('users_session', array('hash', '=', $hash));
	
	if($hashCheck->count()){
		$user = new User($hashCheck->first()->user_id);
		$user->login();
	}
}
?>