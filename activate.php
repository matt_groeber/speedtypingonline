<?php
$currPage = "activate";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$printString = "No input provided";

$userToCheck = new User();
if($userToCheck->isLoggedIn() && !inMaintenanceMode()){
	Redirect::to('user/' . $userToCheck->data()->username . '/profile');
}

if(Input::exists('get')){
	$printString = "Input provided";	
	
	if(Input::get('success') == true){
		$printString = '<h2>Thanks, your account has been successfully activated.</h2>';
		$printString .= '<p>You may <a href="/login.php">login</a> now.</p>';
	} else if(($email = Input::get('email')) && (Input::get('email_code'))){		
		$validate = new Validate();
		
		$validation = $validate->check($_GET, array(
			'email' => array(
				'name' => 'Email',
				'required' => true,
				'exists' => array('users', &$userFromEmail)
			),
			'email_code' => array(
				'name' => 'We had problems activating your account',
				'required' => true,
				'doubleMatch' => array('users', 'email', $email)
			)
		));
		
		if($validation->passed()){
			$activateUserID = $userFromEmail->first()->id;
			$user = new User();
			
			// update user to 'active'
			$retVal = $user->update(array('active' => 1), $activateUserID);
			
			Session::flash('activate', 'Your account has been activated!');
			Redirect::to('activate.php?success=t');
			
		}else {
			$printString = implode(', <br />', $validation->errors()) . '<br />';
		}
		
	} else {
		Session::flash('errored', 'Issues encountered when trying to active your account', 'ERROR');
		Redirect::to('login.php');
	}
} else {
	Redirect::to('login.php');
}


$htmlTitle = "Activate Your Account";
$htmlDescription = "Activate your recently registered SpeedTypingOnline account.";
$cssFiles = "be";
$noWidgets = true;
$addContainer = true;
require_once 'includes/overall/header.php';
echo $printString;
echo '</div>';
require_once 'includes/overall/footer.php';
?>