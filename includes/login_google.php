<?php 

require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/googleAuthInit.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/functions/general.php';

$googleClient = new Google_Client;
$auth = new GoogleAuth($googleClient);

if($auth->checkRedirectCode()){
	// get user
	$userData = $auth->getUserPayload();
	require_once $_SERVER['DOCUMENT_ROOT'] . '/core/init.php';
	
	$user = new User($userData['email']);
	
	if($userData['email_verified']){
		
		$userPicture = isset($userData['picture']) ? $userData['picture'] : '';
		if($userPicture == ''){
			$userPicture = get_gravatar($userData['email']);
		}
		
		if(!$user->exists()){
			$newUsername = $userData['given_name'] . $userData['family_name'][0];
			while($user->find($newUsername)){
				$newUsername .= rand(0,20);
			}
			
			try {
				$user->create(array(
					'username' => $newUsername,
					'password' => '',
					'firstname' => $userData['given_name'],
					'lastname' => $userData['family_name'],
					'email' => $userData['email'],
					'email_code'=> '',
					'joined' => gmdate('Y-m-d H:i:s'),
					'last_login' => gmdate('Y-m-d H:i:s'),
					'group' => 1,
					'premium' => 0,
					'picture' => $userPicture,
					'active' => 1,
					'social_login' => 1
				));
			} catch(Exception $e){
				die($e->getMessage());
			}
			
			// get newly added user now
			$user = new User($userData['email']);
		} else {
			$user->update(array('picture' => $userPicture, 'social_login' => 1), $user->data()->id);
		}
	}
	
	$user->google_login();
	
	if(Cookie::exists(Config::get('remember/cookie_loginRedirect'))){
		$redirectURL = Cookie::get(Config::get('remember/cookie_loginRedirect'));
		Cookie::delete(Config::get('remember/cookie_loginRedirect'));
		
		if($redirectURL == '/login.php?loginfirst=true' && !$user->isPremium())
			$redirectURL = '/goPremium.php';
		
		header('Location: ' . $redirectURL);
	} else {
		header('Location: login.php');
	}
	
} else {
	$redirectURL = $_SERVER['REQUEST_URI'];
	if($redirectURL != '/register.php' && $redirectURL != '/login.php'){
		Cookie::put(Config::get('remember/cookie_loginRedirect'), $redirectURL, Config::get('remember/cookie_expiry'));
	}
}


	if(!$auth->isLoggedIn()){
		echo '<a class="login" href="' . $auth->getAuthUrl() . '"><img src="/images/google/btn_google+_signin_dark_normal_web.png"></a>';
	} else {
		echo '<span>You are signed in.</span> <a href="logout_google.php">Sign out</a>';
	}
 ?>
	