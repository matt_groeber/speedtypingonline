<?php ob_start(); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
<title>Type the Alphabet - Typing Game by Speed Typing Online</title>
<meta name="description" content="How fast can you type alphabet? Learn every letter on the keyboard while trying to beat your fastest time at typing A to Z."></meta>
<meta name="keywords" content="type the alphabet, alphabet typing game, Typing games, speed typing online"></meta>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"></meta>
<link rel="stylesheet" href="css/_b_menuGeneral.css" type="text/css"></link>
<link rel="stylesheet" href="../css/tta.css" type="text/css"></link>
<link rel="stylesheet" href="../css/_e_footer.css" type="text/css"></link>
<link rel="stylesheet" href="../css/_i_STOjUI.css" type="text/css"></link>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/general.js"></script>
<script type="text/javascript" src="js/tta.js"></script>
<link rel="shortcut icon" href="http://www.speedtypingonline.com/STO_favicon.ico"></link>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
</head>
 <body>

<?php 
require_once 'core/init.php';
	
$currPage = "typeTheAlphabet";
include 'includes/topMenu.php'; 
ob_end_flush();
?>
<script>
<?php
echo "var alphaTypeEnum = " . json_encode(Config::get('constants/alphaTypeEnum')) . ";";

$user_obj = new User();
$loggedInID = ($user_obj->isLoggedIn()) ? $user_obj->data()->id : null;

$usersFastestTime = '99999999';


if($loggedInID !== null){
	$db_obj = DB::getInstance()->get('alpha_HS', array('user_id', '=', $loggedInID), 'total_time, type, timestamp', array('total_time', 'ASC'));
	$results = $db_obj->results();
	if(!empty($results)){
		$usersFastestTime = $results[0]->total_time;
	
	
		echo 'var myData = [[';
		$prefix = '';
		$ii = 1;
		foreach($results as $entry){
			echo $prefix . $ii . ", " . $entry->total_time . ", " . $entry->type . ",new Date('" . $entry->timestamp . "'.replace(/-/g,'/'))";
			$prefix = '], [';
			$ii++;
		}
		echo ']];';
	} else {
		echo 'var myData = null;';
	}
} else {
	echo 'var myData = null;';
}
?>
</script>
<div id="fbLikeDiv">
	<div class="fb-like" data-href="http://www.speedtypingonline.com/games/type-the-alphabet.php" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true" data-font="arial"></div>
</div>

<div id="header">
	<h1>Type the Alphabet</h1>
	<h2>How fast can you type the alphabet? <br /> Learn all of the letters on the keyboard while trying to beat your fastest score.</h2>
</div>

<div class="grid grid-pad">
   <div class="col-1-6">
		<div id="sideSkyAd" style="background:#666;>
			<!-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- STO_TTA_WideSky -->
			<!-- <ins class="adsbygoogle"
				 style="display:inline-block;width:160px;height:600px"
				 data-ad-client="ca-pub-2169602241028960"
				 data-ad-slot="9438152208"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>-->
		</div>
   </div>
   <div class="col-4-6">
	   <div id="topLeadAd" style="background:#666;">
			<!--<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>-->
				<!-- STO_TTA_Lead -->
				<!--<ins class="adsbygoogle"
					 style="display:inline-block;width:728px;height:90px"
					 data-ad-client="ca-pub-2169602241028960"
					 data-ad-slot="7961419002"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
			</script>-->
		 </div>
		<div id="centerEntryModule" class="module">
			<div id="wrapStats">
				<div id="timerDiv">   
					<div id="timerText">Time</div>
					<div id="sWatch" class="statsText" style="background-color: transparent; ">00:00</div>
				</div>
				<div id='wpmDiv'>                
                    <div id='wpmText'>Speed (WPM)</div>
                    <div id='wpmValue' class="statsText">0</div>
                </div>
				<div id="scoreDiv"> 
					<div id="scoreText">Fastest Time</div>
					<div id="wbkScore" class="statsText">0</div>
					<span id="clearDataBtn"><input class="stoUI stoUIClickable" type="button" value="Clear All Data" onclick="cleanData();"></input></span>
					<span id="storedTime" style="display: none;"><?php echo $usersFastestTime; ?></span>
					<span id="textSelectDropDown">
						<span class="boldFont">Mode: </span>
						<select id="textTypeSelected" class="stoUI stoUIClickable">
							<option value="NORMAL">A-Z (Normal)</option>
							<option value="SPACES">A-Z (with Spaces)</option>
							<option value="BACKWARDS">Backwards</option>
							<option value="RANDOM">Random</option>
						</select>
					</span>
				</div>
			</div>
			
			<div id="centerContent">
				<div id="containerWrapper">
					<div id="divCover" style="z-index: 0;"></div>
					<div id="blockDivContainer" class="mainDivInputs" tabindex="0" onmouseover="this.focus()" onmouseout="/*this.blur()*/" ></div>
				</div>
				
				<div id="buttonWrapper">
					<div id="resetBtnWrap">
						<div id="resetBtnDiv">
							<input type="button" name="ResetBtn" value="Reset" id="resetBtn" class="orangeButton" onclick="Reset(0,'sWatch')"></input>
						</div>
					</div>
				</div>
				
			</div>	<!-- end centerContent -->
			
			<div id="textSelectContainer">
				
			</div>	
		</div>				
    </div>
    <div class="col-1-6">
		<span>&nbsp;</span>
    </div>
</div>
<div class="grid grid-pad">
   <div class="col-1-2 instructions" style="height: auto;">
     <div class="module instructWrap">
		<div id="instructDiv" style="height: auto;">
			<h3>Your High Scores</h3>
			<div id="alphaHS">
				<table>
					<tr>
						<th>#</th>
						<th>Score</th>
						<th>Type</th>
						<th>Time Ago</th>
					</tr>
<?php
	/* User logged in all high scores */
	if($loggedInID !== null){
		if(!empty($results)){
		
			$ii = 1;
			$decoration = '';
			
			foreach($results as $entry){
				if(strtotime($entry->timestamp) > strtotime("-30 minutes"))
					$decoration = ' borderRad_4 orangeBkgd boldFont centerTxt"';
				else
					$decoration = '';
				
				echo '<tr><td>' . $ii . '</td><td class="convertToTimeStr">' . $entry->total_time . '</td><td>' . Config::get('constants/alphaTypeDecode')[$entry->type] . '</td><td class="convertToTimeAgo' . $decoration . '">' . $entry->timestamp . '</td></tr>';
				$ii++;
			}
		} else {
			echo '<tr><td class="orangeCTA mediumBtn" colspan="4">You don\'t have any scores yet.</td></tr>';
		}			
	} else {
		echo '<tr><td class="orangeCTA mediumBtn" colspan="4"><a href="http://www.stotest.tk/register.php">Register</a> or <a href="http://www.stotest.tk/login.php">Login</a> to keep track of your scores.</td></tr>';
	}
	
?>
				</table>
			</div>
		</div>
	 </div>
	</div>
	<div class="col-1-2 instructions" style="height: auto;">
		<div class="module instructWrap">
			<div id="instructDiv" style="height: auto;">
				<h3>High Scores</h3>
				<div id="alphaHS">
					<table>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Score</th>
							<th>Time Ago</th>
						</tr>
<?php
	/* Most Recent High Scores from all users */
	$db_obj = DB::getInstance()->get('alpha_HS', array('timestamp', '>', date('Y-m-d H:i:s', strtotime('-24 hours'))), 'user_id, total_time, timestamp', array('total_time', 'ASC'));
	$results = $db_obj->results();
	
	
	$maxNum = (sizeof($results) > 10) ? 10 : sizeof($results);
	
	if($maxNum > 0){
		$highScoreIDs = array();
		foreach($results as $result){
			array_push($highScoreIDs, $result->user_id);
		}

		$usernameResults = $user_obj->get_users('id', $highScoreIDs, 'id, username', array('id', $highScoreIDs));
		$theUsername = '';
		$decoration = '';
		
		for ($ii = 0; $ii < $maxNum; $ii++){
			$decoration = '';
			// see if we need to handle duplicates
			if(sizeof($usernameResults) != $maxNum){
				foreach($usernameResults as $userReturned){
					if($userReturned->id == $results[$ii]->user_id){
						$theUsername = $userReturned->username;
						break;
					}
				}
			} else {
				$theUsername = $usernameResults[$ii]->username;
			}
			
			if($results[$ii]->user_id == $loggedInID)
				$decoration = ' class="borderRad_4 orangeBkgd boldFont centerTxt"';
			
			echo '<tr><td>' . ($ii + 1) . '</td><td' . $decoration . '>' . $theUsername . '</td><td class="convertToTimeStr">' . $results[$ii]->total_time .'</td><td class="convertToTimeAgo">' . $results[$ii]->timestamp . '</td></tr>';
		}
	} else {
		echo '<tr><td class="orangeCTA mediumBtn" colspan="4">Nobody has a high score yet!</td></tr>';
	}
	
?>
					</table>
				</div>
			</div>
		</div>
	 </div>
	 
	<div id="dialog-confirm" title="Permanently delete all your alphabet game scores?" style="display: none;">
		<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Your high scores for the alphabet typing game will be permanently deleted and cannot be recovered.</p>
		<p>Are you sure?</p>
	</div>
</div>

<?php $currPage = "typeTheAlphabet"; include 'footer.php'; ?>

<script type="text/javascript">
 var currDateTime = new Date(<?php echo '"' . date('Y-m-d H:i:s') . '"';?>.replace(/-/g,'/'));
 
	/*$.holdReady(true);
	$.getScript("../js/general.js", function() {
		$.getScript("../js/tta.js", function(){
			$.holdReady(false);
		});
	});*/

 $(window).ready(function() {
	 var storedScore = $('#storedTime').html();
	 storedScore = (storedScore == '99999999') ? '-' : msecsToTimeStr(storedScore);
	 
	 $('#wbkScore').html(storedScore);
	 
	 $('.convertToTimeStr').each(function(){
		 var timeStr = msecsToTimeStr($(this).html());
		 $(this).html(timeStr);
	 });
	 
	 $('.convertToTimeAgo').each(function(){
		 var earlyDate = new Date($(this).html().replace(/-/g,'/'));
		 $(this).html(readableTimeDifference(earlyDate, currDateTime) + " ago");
	 });
	 
	// $('#textTypeSelected').selectmenu();
	 
    //InitializeTimer_block(30, 'sWatch')
		//resizeElements();
		
		// ADD HOTKEY
		shortcut.add("Shift+return",function() {
			Reset(0, 'sWatch');
			},{
			"type":"keyup",
			"propagate":false,
			"disable_in_input":false,
			"target":document
			});

        $('#blockDivContainer').on("keydown", function(event){
            if($('#blockDivContainer').get()[0].innerHTML != ""){
                return CreateTimer_block(event, 'sWatch', 0/* Round Time */);
            }
        });
    
        // call reset function every time text type drop down menu changes
        $("#textTypeSelected").change(function () {
            Reset(0,'sWatch');
			var newHS = getHSfromType(alphaTypeEnum[$("#textTypeSelected").val()]);
			$('#storedTime').html(newHS);
			storedScore = (newHS == '99999999') ? '-' : msecsToTimeStr(newHS);
	 
			$('#wbkScore').html(storedScore);
	 
			
        });
		
        Reset(0,'sWatch');
    
	
		$("#xStatsToggleBut").click(function () {
		  
			if($('#moreLessStats').html() == "More")
			{
				$('#extraStatsHidden').show();					// consider adding top word score, top score, average typing speed, and difficulty/hideshow word hints
				$('#moreArrow').css('display', 'none');
				$('#lessArrow').css('display', 'inline-block');
				$('#moreLessStats').html("Less");
				$('.wordBank').css('min-height', '350px');
			}
			else
			{
				$('#extraStatsHidden').hide();
				$('#lessArrow').css('display', 'none');
				$('#moreArrow').css('display', 'inline-block');
				$('#moreLessStats').html("More");
				$('.wordBank').css('min-height', '310px');
			}
				
		});
	});
	
	function getHSfromType(type){
		var retVal = '99999999';
		$.each( myData, function( key, value ) {
			if(value[2] == type){
				retVal = value[1];
				return false;
			}
				
		});
		
		return retVal;
	}
</script>
	
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-21029737-2']);
  /*_gaq.push(['_setCustomVar', 1, 'TC', testCounter.toString(), 3]);*/
  _gaq.push(['_trackPageview', '/games/type-the-alphabet']);
  _gaq.push(['_trackPageLoadTime']);
  
  /*_gaq.push(['_trackEvent',
      'Testing', // category of activity
      'Custom Var', // Action
   ]);*/

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</body>
</html>