<?php $user = new User();?>
<div class="widget">
	<h3>Hi, <?php echo escape($user->data()->username); ?>!<?php echo (($groupID = $user->data()->group) > 1) ? ' (' . $user->groupDisplayName($groupID) . ')' : '';?>
	</h3>
	
	<div class="inner">
		<div class="profile">
			<?php 
			/* PROFILE STUFF  START*/
			/*
			if (isset($_FILES['profile']) === true){
				if(empty($_FILES['profile']['name']) === true){
					echo 'Please choose a file';
				} else {
					$allowed = array('jpg', 'jpeg', 'gif', 'png');
					$file_name = $_FILES['profile']['name'];
					$file_extn = strtolower(end(explode('.', $file_name)));
					$file_temp = $_FILES['profile']['tmp_name'];
					
					if(in_array($file_extn, $allowed) === true){
						change_profile_image($session_user_id, $file_temp, $file_extn);
						//header('Location: ' . $current_file);
						echo 'got here';
						exit();
					} else {
						echo 'Incorrect file type. Allowed: ';
						echo implode(', ', $allowed);
					}
					
				}
			}
			
			if(empty($user_data['profile']) === false) {
				echo '<img src="' . $user_data['profile'] . '" alt="' . $user_data['firstname'] . '\'s Profile Image"';
			}
			
			/* PROFILE STUFF END*/
			
			
			?>
			<!--<form action="" method="post" enctype="multipart/form-data">
				<input type="file" name="profile"> <input type="submit">
			</form>-->
		</div>
		
		<ul>
			<?php
				/*if($aside){
					echo '<li><a href="<?php echo \'/user/\' . $user->data()->username . \'/profile\';?>">View Profile</a></li>';	
				} else {
					
				} */
			?>
			<li <?php echo ($currPage == "updateProfile") ? 'class="current"' : ''; ?>><a href="<?php echo '/user/' . $user->data()->username . '/profile';?>">My Profile</a></li>
			
			<!-- SEE IF STUDENT OR TEACHER -->
			<?php if($user->isTeacher()){ ?>
				<li <?php echo ($currPage == "classes") ? 'class="current"' : ''; ?>><a href="<?php echo '/user/' . $user->data()->username . '/classes';?>">Classes</a></li>
				<li <?php echo ($currPage == "assignments") ? 'class="current"' : ''; ?>><a href="<?php echo '/user/' . $user->data()->username . '/assignments';?>">Assignments</a></li>
			<?php } else if($user->isStudent()){ ?>
				<li <?php echo ($currPage == "assignments") ? 'class="current"' : ''; ?>><a href="<?php echo '/user/' . $user->data()->username . '/assignments';?>">Assignments</a></li>
			<?php } ?>
			
			<li <?php echo ($currPage == "testStats") ? 'class="current"' : ''; ?>><a href="<?php echo '/user/' . $user->data()->username . '/test-stats';?>">Typing Test Stats</a></li>
			<li <?php echo ($currPage == "typingLessons") ? 'class="current"' : ''; ?>><a href="/user/<?php echo $user->data()->username; ?>/lessons">Typing Lessons</a></li>
			<li <?php echo ($currPage == "changePassword") ? 'class="current"' : ''; ?>><a href="/changepassword.php">Change Password</a></li>
			
			<?php if(!$user->isPremium()){ ?>
				<li <?php echo ($currPage == "goPremium") ? 'class="current premium-li"' : 'class="premium-li"'; ?>><a href="/goPremium.php"><img src="/images/crown.svg"></img><span>Go Premium!</span></a></li>
			<?php } ?>
			
			<li><a href="/logout.php?redir=<?php echo $_SERVER['REQUEST_URI'];?>">Log Out</a></li>
		</ul>
	</div>
</div>