<?php
$currPage = "testStats";
require_once 'core/init.php';
$pageURL = '/test-stats.php';


if(!$username = Input::get('user')){
	Redirect::to('typing-test.php');
} else {
	$userToProfile = new User($username);
	$utpTemp = $userToProfile;
	$user = new User();
	$debugMode = $user->hasPermission('debug') || Config::get('constants/debugMode');
	$onPersonalProfile = $user->data()->username == $username;
	
	$htmlTitle = "Typing Test Stats, " . $username;
	$htmlDescription = "Typing Test Data and Statistics for " . $username;
	$cssFiles = "behij";
	$noWidgets = true;
	$addContainer = true;
	$fontAwesomeCDN = true;
	$jQuery = true;
	require_once 'includes/overall/header.php';
	
	$userFullName = '';
	if(!$userToProfile->exists()){
		if(inMaintenanceMode()){
			echo '<h2>Site is under Maintenance</h2><h4>User profile pages will be available shortly.</h4><h4>Please continue to use the site as normal but be aware that test, lesson, and game high score data will not be saved.</h4><h4>Sorry for any inconvenience and thank you for your patience!</h4>';
		} else {
			echo '<h2>That user does not exist!</h2>';
		}
		exitPHPwithFooter();
	} else {
		$data = $userToProfile->data();
		$userFullName = $userToProfile->fullName();
		if($userFullName != ''){$userFullName = $userFullName . '\'s ';}
	}

	$db = DB::getInstance();
	
	// get all typing tests done by user
	$db_obj = $db->get('typing_tests', array('user_id', '=', $data->id),'typing_tests.id, wpm, accuracy, timestamp, keyboard_used, total_time_secs, title',array('timestamp', 'DESC'), array('test_texts', 'text_index', 'id'),  array(9999999,0));
	$ttResults = $db_obj->results();
	
	if(Session::exists('failed')){
		echo Session::flash('failed');
	} else {
		echo Session::flash('updated');
	}
?>
<div class="borderBottom">
<?php
$classroomObj = new Classroom($user);
$isTeachersStudent = $classroomObj->isUserTeacherOfStudent($userToProfile->data()->id);
require_once 'includes/profileMenu.php';
?>
</div>
</div> <!-- <div id="heading"> -->

<div id="pageMain">
	<div id="genAdWrap">
		<?php echo getAd("generalLeadAd", null, $debugMode, $user->isPremium());?>
	</div>
	<div>
		<h2 class="second_h1"><?php echo ($onPersonalProfile) ? $userFullName : $username . "'s "; ?>Typing Test Stats</h2>
	</div>
	
<?php
	
	if(empty($ttResults)){
		if(!$onPersonalProfile){
			echo '<div class="orangeCTA long"><h3>This user has not taken a typing test yet.</h3></div>';
		} else {
			echo '<div class="orangeCTA long"><h3>You have not taken a typing test yet!</h3>';
			echo '<h4><a href="/typing-test">CLICK HERE to take your first typing test</a></h4></div>';
		}
	} else { 
		// get union of typing tests and character counts
		$db_obj = $db->get('typing_tests', array('user_id', '=', $data->id), 'the_char, total_count, incorrect_count, response_time, id', null, array('tt_char_counts', 'id','test_id'), array(9999999,0));
		$ccResults = $db_obj->results();
		//var_dump($ccResults);
		//exit();
		
		$db_obj = $db->get('users_achieve', array('user_id', '=', $data->id), 'num_tests', null, null, array(1,0));
		$numTotalTests = $db_obj->results()[0]->num_tests;
		
		// form string of all typing tests data for graphing in google visualization
		$prefix = ""; 
		$jj = 1;
		$ttDataToGraph = "";
		$runningSum = 0;
		$accuracySum = 0;
		$average = $ttResults[0]->wpm;
		$averageAcc = $ttResults[0]->accuracy;
		$topSpeed = $ttResults[0]->wpm;
		$lowestSpeed = $ttResults[0]->wpm;
		
		for($ii = sizeof($ttResults) - 1; $ii >= 0; $ii--){
			$runningSum += $ttResults[$ii]->wpm;
			$accuracySum += $ttResults[$ii]->accuracy;
			if($ttResults[$jj]->wpm > $topSpeed){$topSpeed = $ttResults[$jj]->wpm;}
			if($ttResults[$jj]->wpm < $lowestSpeed){$lowestSpeed = $ttResults[$jj]->wpm;}
			if($jj > 1){$average = round($runningSum / $jj, 1); $averageAcc = round($accuracySum / $jj, 1);}
			$ttDataToGraph .= $prefix . "[" . $jj . "," . $ttResults[$ii]->wpm . ","
											//. " customTooltip(''," . $jj . ",new Date('" . $ttResults[$ii]->timestamp . "'.replace(/-/g,'/')), " . $ttResults[$ii]->wpm . "),"
											. $average . ",new Date('" . $ttResults[$ii]->timestamp . " UTC'.replace(/-/g,'/'))," . $ttResults[$ii]->accuracy . "," . $ttResults[$ii]->total_time_secs . "," . $ttResults[$ii]->id . ", \"" . $ttResults[$ii]->title . "\", " . $ttResults[$ii]->keyboard_used . "]";
			$prefix = ", ";
			$jj++;
		}
		
		$topSpeed = round($topSpeed, 1);
		$lowestSpeed = round($lowestSpeed, 1);
		$lastTest = $ttResults[0]->wpm;
		$numTests = sizeof($ttResults);
	
	?>
				<div id="genAdWrap_2">
					<div id="quickNav">
						<div id="qnTitle">Quick Nav</div>
						<div class="qnButtonWrap first" title="GoTo Keyboard Heatmap">
							<a href="#keyboardHeatmap"><i class="fa fa-keyboard-o" aria-hidden="true"></i></a>
						</div>
						<div class="qnButtonWrap" title="GoTo Typing Test Data Line Graph">
							<a href="#testDataGraph"><i class="fa fa-area-chart" aria-hidden="true"></i></a>
						</div>
					</div>
					
					<?php echo getAd("generalLeftAd", null, $debugMode, $user->isPremium());?>
				</div>
<?php
			
	$averageMSPL = 60000 / ($average * 5);
	
	$ccArr = array();
	$cdArr = array();
	$ksArr = array();
	
	for($ii = 0; $ii < sizeof($ccResults); $ii++){
		$theChar = $ccResults[$ii]->the_char;
		if(array_key_exists($theChar, $ccArr)){
			$newTwrong = $ccArr[$theChar]['tWrong'] + $ccResults[$ii]->incorrect_count;
			$newTcount = $ccArr[$theChar]['tCount'] + $ccResults[$ii]->total_count;
			$newResponseTime = $ccArr[$theChar]['tResponseTime'] + $ccResults[$ii]->response_time;
			$ccArr[$theChar] = array('tWrong' => $newTwrong, 'tCount' => $newTcount, 'tResponseTime' => $newResponseTime);
		} else {
			$ccArr[$theChar] = $ccResults[$ii]->total_count;
			$ccArr[$theChar] = array('tWrong' => (int)$ccResults[$ii]->incorrect_count, 'tCount' => (int)$ccResults[$ii]->total_count, 'tResponseTime' => (int)$ccResults[$ii]->response_time);
		}
	}
	$kk = 1;
	
	foreach($ccArr as $key => $value){
		$cdArr[$key] = round(($value['tCount'] - $value['tWrong']) / $value['tCount'], 3); 
		$responseTimeNorm = $value['tResponseTime'] / $value['tCount'];
		$ksArr[$key] = round((1 / (($responseTimeNorm * 5) / 60000)), 1); 
	}
	
	//asort($cdArr);
	//asort($ccArr);
	?>
	<div id="testSummaryContainer" class="stoStatusBox">
		<div class="stoStatBoxTitle">
			<h3>Test Summary</h3>
		</div>
		<div id="overall_div" class="stoStatBoxBody">
			<table>
				<tr>
					<th class="txtRight">Last Test</th>
					<td id="lastWPM" class="borderRad_8 dkBlueBkgd"><span class="bigTxt"><?php echo $lastTest;?></span><span class="note"> wpm</span></td>
					<td id="lastWPMdate" class="txtLeft">-</td>
				</tr>
				<tr>
					<th class="txtRight">Fastest Speed</th>
					<td id="fastestWPM"><span class="bigTxt"><?php echo $topSpeed;?></span><span class="note"> wpm</span></td>
					<td id="fastestWPMdate" class="txtLeft">-</td>
				</tr>
				<tr>
					<th class="txtRight">Slowest Speed</th>
					<td id="slowestWPM"><span class="bigTxt"><?php echo $lowestSpeed;?></span><span class="note"> wpm</span></td>
					<td id="slowestWPMdate" class="txtLeft">-</td>
				</tr>
				<tr>
					<th class="txtRight">Average Speed</th>
					<td id="aveWPM" class="borderRad_8 orangeBkgd"><span class="bigTxt"><?php echo $average;?></span><span class="note"> wpm</span></td>
					<td id="aveWPMdateRange" class="txtLeft">-</td>
				</tr>
				<tr>
					<th class="txtRight">Average Accuracy</th>
					<td id="aveAcc"><span class="bigTxt"><?php echo $averageAcc;?>%</span></td>
				</tr>
				<tr>
					<th class="txtRight">Total Tests</th>
					<td id="totalNumTests"><span class="bigTxt"><?php echo $numTotalTests;?></span></td>
					<?php if($onPersonalProfile){ echo '<td class="txtLeft"><div id="clearDataBtn" style=""><input class="stoUI stoUIClickable" type="button" value="Clear Test Data" onclick="cleanData(\'tt\', \'Delete all typing test data\');"></div></td>';}?>
				</tr>
			</table>
			
		</div>
		<div id="keyboardHeatmap"></div>
	</div>


<?php
//if($onPersonalProfile){
?>
	<div id="keyHmapContainer">
		<div><h2>Keyboard Heatmap</h2></div>
		<div class="stoUI stoUIradio">
			<form id="KHMradio">
				<label for="HM_KeySpeed" class="radioLeft">
					<input type="radio" id="HM_KeySpeed" name="keyHMtype" checked="checked" value="Speed">
					<span><h3>Speed</h3></span>
				</label>
				<label for="HM_KeyAccuracy" class="radioRight">
					<input type="radio" id="HM_KeyAccuracy" name="keyHMtype" value="Accuracy">
					<span><h3>Accuracy</h3></span>
				</label>
			</form>
		</div>

		<div id="kbWrap">
			<div id="kbDiv" class="hideAcc">
				<div id="keysFilled">
					<div id="keyRow1" class="keyRows">
						<div id="key1_1">~<br />`</div>
						<div id="key1_2">!<br />1</div>
						<div id="key1_3">@<br />2</div>
						<div id="key1_4">#<br />3</div>
						<div id="key1_5">$<br />4</div>
						<div id="key1_6">%<br />5</div>
						<div id="key1_7">^<br />6</div>
						<div id="key1_8">&<br />7</div>
						<div id="key1_9">*<br />8</div>
						<div id="key1_10">(<br />9</div>
						<div id="key1_11">)<br />0</div>
						<div id="key1_12" class="smLH">--<br />-</div>
						<div id="key1_13">+<br />=</div>
						<div id="key1_14" class="functionKey">Backspace</div>
					</div>
					<div id="keyRow2" class="keyRows">
						<div id="key2_1" class="functionKey">Tab</div>
						<div id="key2_2">Q</div>
						<div id="key2_3">W</div>
						<div id="key2_4">E</div>
						<div id="key2_5">R</div>
						<div id="key2_6">T</div>
						<div id="key2_7">Y</div>
						<div id="key2_8">U</div>
						<div id="key2_9">I</div>
						<div id="key2_10">O</div>
						<div id="key2_11">P</div>
						<div id="key2_12">{<br />[</div>
						<div id="key2_13">}<br />]</div>
						<div id="key2_14">|<br />\</div>
					</div>
					<div id="keyRow3" class="keyRows">
						<div id="key3_1" class="functionKey smLH">Caps Lock</div>
						<div id="key3_2">A</div>
						<div id="key3_3">S</div>
						<div id="key3_4">D</div>
						<div id="key3_5">F</div>
						<div id="key3_6">G</div>
						<div id="key3_7">H</div>
						<div id="key3_8">J</div>
						<div id="key3_9">K</div>
						<div id="key3_10">L</div>
						<div id="key3_11">:<br />;</div>
						<div id="key3_12">"<br />'</div>
						<div id="key3_13" class="functionKey">Enter</div>
					</div>
					<div id="keyRow4" class="keyRows">
						<div id="key4_1" class="functionKey">Shift</div>
						<div id="key4_2">Z</div>
						<div id="key4_3">X</div>
						<div id="key4_4">C</div>
						<div id="key4_5">V</div>
						<div id="key4_6">B</div>
						<div id="key4_7">N</div>
						<div id="key4_8">M</div>
						<div id="key4_9" class="smLH">&lt;<br />,</div>
						<div id="key4_10" class="smLH">&gt;<br />.</div>
						<div id="key4_11">?<br />/</div>
						<div id="key4_12" class="functionKey">Shift</div>
					</div>
					<div id="keyRow5" class="keyRows">
						<div id="key5_1"></div>
					</div>
				</div>
			
				<div id="kbLegend">
					<div class="bigTxt">Legend</div>
					<div class="keyRows">
						<span id="kbLow" class="kbl_label">
							<span class="accToHide">0%</span>
							<span class="spdToHide"></span>
						</span>
						<div class="p_0 key">
							<span class="stoTT oCTA">
								<span class="accToHide"><div>Key Accuracy</div>0% - 50%</span>
								<span class="spdToHide"><div>Key Speed</div><span class="keySpdRange">&lt; 35 wpm</span></span>
							</span>
						</div>
						<div class="p_1 key">
							<span class="stoTT oCTA">
								<span class="accToHide"><div>Key Accuracy</div>50% - 75%</span>
								<span class="spdToHide"><div>Key Speed</div><span class="keySpdRange">35 - 45 wpm</span></span>
							</span>
						</div>
						<div class="p_2 key">
							<span class="stoTT oCTA">
								<span class="accToHide"><div>Key Accuracy</div>75% - 90%</span>
								<span class="spdToHide"><div>Key Speed</div><span class="keySpdRange">45 - 55 wpm</span></span>
							</span>
						</div>
						<div class="p_3 key">
							<span class="stoTT oCTA">
								<span class="accToHide"><div>Key Accuracy</div>90% - 95%</span>
								<span class="spdToHide"><div>Key Speed</div><span class="keySpdRange">55 - 65 wpm</span></span>
							</span>
						</div>
						<div class="p_4 key">
							<span class="stoTT oCTA">
								<span class="accToHide"><div>Key Accuracy</div>95% - 100%</span>
								<span class="spdToHide"><div>Key Speed</div><span class="keySpdRange">65 - 75 wpm</span></span>
							</span>
						</div>
						<div class="p_5 key">
							<span class="stoTT oCTA">
								<span class="accToHide"><div>Key Accuracy</div>= 100%</span>
								<span class="spdToHide"><div>Key Speed</div><span class="keySpdRange">&gt; 75 wpm</span></span>
							</span>
						</div>
						<span id="kbHigh" class="kbl_label">
							<span class="accToHide">100%</span>
							<span class="spdToHide"></span>
						</span>
					</div>
				</div>
				<div id="testDataGraph"></div>
			</div>
		</div>
	</div>
	<div id="genAdWrap_bottom">
		<?php echo getAd("generalBottomLeadAd", null, $debugMode, $user->isPremium());?>
	</div>
	<script>
		var qwertyKeyMap = {a:"3_2",b:"4_6",c:"4_4",d:"3_4",e:"2_4",f:"3_5",g:"3_6",h:"3_7",i:"2_9",j:"3_8",k:"3_9",l:"3_10",m:"4_8",n:"4_7",o:"2_10",p:"2_11",q:"2_2",r:"2_5",s:"3_3",t:"2_6",u:"2_8",v:"4_5",w:"2_3",x:"4_3",y:"2_7",z:"4_2",',':"4_9",'.':"4_10",'\'':"3_12"};
	<?php
		echo "var ccArr = " . json_encode($ccArr) . ";";
		echo "var cdArr = " . json_encode($cdArr) . ";";
		echo "var ksArr = " . json_encode($ksArr) . ";";
		echo "var averageSpeed = " . $average . ";";
		echo "var averageAcc = " . $averageAcc . ";";
	?>
		var elem;
		var accuracyClassName;
		var keySpeedCutoffs = [0, 0.7, 0.8, 0.95, 1.05, 1.25];
		
		for(var key in ccArr){
			elem = document.getElementById('key' + qwertyKeyMap[key]);
			if(elem){
				elem.innerHTML = '<span class="stoTT oCTA"><div class="divider">' + key + '</div>' +
									'<div class="mStat accToHide">' + (Math.round((cdArr[key] * 10000)) / 100) + '%</div>' + 
									'<div class="mStat spdToHide">' + ksArr[key] + '<span class="noteSmall"> WPM</span></div>' + 
									'<div class="align"><span>Incorrect:</span>' + ccArr[key].tWrong + '</div>' +
									'<div class="align"><span>Total Count:</span>' + ccArr[key].tCount + '</div>' +
									'<div class="align accToHide"><span>Speed:</span>' + ksArr[key] + ' wpm</div>' +
									'<div class="align spdToHide"><span>Accuracy:</span>' + (Math.round((cdArr[key] * 10000)) / 100) + '%</div>' +
									'</span>' + elem.innerHTML;
									
				accuracyClassName = GetKeyClass(ksArr[key]);
				
				if(elem.className != ""){
					elem.className = accuracyClassName + " " + elem.className;
				} else {
					elem.className = accuracyClassName;
				}
			}
			
		}
		
		function ChangeHeatmap(val){
			for(var key in ccArr){
				elem = document.getElementById('key' + qwertyKeyMap[key]);
				
				if(elem){					
					if(val != "accuracy"){
						pClassName = GetKeyClass(ksArr[key]);
					} else {
						pClassName = GetKeyClass(cdArr[key], "ACCURACY");
					}
					
					if(elem.className == "" || ( (elem.className.substring(0,2) == "p_") && (elem.className.length == 3) )){
						elem.className = pClassName;
					} else {
						if(elem.className.substring(0,2) == "p_"){
							elem.className = pClassName + " " + elem.className.substring(4);
						} else {
							elem.className = pClassName + " " + elem.className;
						}
					}
				}
			}
			
			if(val != "accuracy"){
				$('#kbDiv').removeClass("hideSpd");
				$('#kbDiv').addClass("hideAcc");
			} else {
				$('#kbDiv').removeClass("hideAcc");
				$('#kbDiv').addClass("hideSpd");
			}
		}
		
		function GetKeyClass(val, type){
			type = typeof type !== 'undefined' ? type : "";
			var keyClass = "";
			var classArr = ["p_0", "p_1", "p_2", "p_3", "p_4", "p_5"];
			
			
			if(type.toUpperCase() != "ACCURACY"){
				if(val > (averageSpeed * keySpeedCutoffs[5])){
					keyClass = classArr[5];		// need the order this way, don't replace with +=
				} else if(val > (averageSpeed * keySpeedCutoffs[4])){
					keyClass = classArr[4];		// need the order this way, don't replace with +=
				} else if(val > (averageSpeed * keySpeedCutoffs[3])){
					keyClass = classArr[3];
				} else if(val > (averageSpeed * keySpeedCutoffs[2])){
					keyClass = classArr[2];
				} else if(val > (averageSpeed * keySpeedCutoffs[1])){
					keyClass = classArr[1];
				} else {			
					keyClass = classArr[0];
				}
			} else {
				if(val == 1){
					keyClass = classArr[5];		// need the order this way, don't replace with +=
				} else if(val >= 0.95){
					keyClass = classArr[4];		// need the order this way, don't replace with +=
				} else if(val >= 0.9){
					keyClass = classArr[3];
				} else if(val >= 0.75){
					keyClass = classArr[2];
				} else if(val > 0.5){
					keyClass = classArr[1];
				} else {			
					keyClass = classArr[0];
				}
			}
			
			return keyClass;
		}
	
</script>
<script type="text/javascript" src="/js/general.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tools/1.2.7/jquery.tools.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>


<script type="text/javascript">
<?php
	
	// ** START ** default to start graphing at 6th last typing test score
	//$numResults = sizeof($ttResults);
	
	echo "var ttData = [" . $ttDataToGraph . "];";
	echo "var keyboardLabels = " . json_encode(Config::get('constants/keyboardsLabels')) . ";";
?>

$('.p_0 .keySpdRange')[0].innerHTML = "< " + (Math.round((keySpeedCutoffs[1] * averageSpeed) * 10) / 10) + " <span class=\"noteSmall\">wpm</span><div>(ave = " + averageSpeed + ")</div>";
$('.p_1 .keySpdRange')[0].innerHTML = (Math.round((keySpeedCutoffs[1] * averageSpeed) * 10) / 10) + " - " + (Math.round((keySpeedCutoffs[2] * averageSpeed) * 10) / 10) + " <span class=\"noteSmall\">wpm</span><div>(ave = " + averageSpeed + ")</div>";
$('.p_2 .keySpdRange')[0].innerHTML = (Math.round((keySpeedCutoffs[2] * averageSpeed) * 10) / 10) + " - " + (Math.round((keySpeedCutoffs[3] * averageSpeed) * 10) / 10) + " <span class=\"noteSmall\">wpm</span><div>(ave = " + averageSpeed + ")</div>";
$('.p_3 .keySpdRange')[0].innerHTML = (Math.round((keySpeedCutoffs[3] * averageSpeed) * 10) / 10) + " - " + (Math.round((keySpeedCutoffs[4] * averageSpeed) * 10) / 10) + " <span class=\"noteSmall\">wpm</span><div>(ave = " + averageSpeed + ")</div>";
$('.p_4 .keySpdRange')[0].innerHTML = (Math.round((keySpeedCutoffs[4] * averageSpeed) * 10) / 10) + " - " + (Math.round((keySpeedCutoffs[5] * averageSpeed) * 10) / 10) + " <span class=\"noteSmall\">wpm</span><div>(ave = " + averageSpeed + ")</div>";
$('.p_5 .keySpdRange')[0].innerHTML = "> " + (Math.round((keySpeedCutoffs[5] * averageSpeed) * 10) / 10) + " <span class=\"noteSmall\">wpm</span><div>(ave = " + averageSpeed + ")</div>";

$('#kbLow .spdToHide')[0].innerHTML = (Math.round((keySpeedCutoffs[1] * averageSpeed) * 10) / 10) +  " <span class=\"noteSmall\">wpm</span>";
$('#kbHigh .spdToHide')[0].innerHTML = (Math.round((keySpeedCutoffs[5] * averageSpeed) * 10) / 10) + " <span class=\"noteSmall\">wpm</span>";

$('#KHMradio').on('change', function(){
	if($('input[name="keyHMtype"]:checked', '#KHMradio').val() == "Accuracy"){
		ChangeHeatmap('accuracy');
	} else {
		ChangeHeatmap('speed');
	}
});

// Google Charts ------------
	
google.load('visualization', '1', {packages: ['corechart', 'line', 'controls', 'charteditor']});
google.setOnLoadCallback(drawChart);

var data;
var view;
var dash;
var chartWrapper;
var options;
var control;
var controlOptions;
var mode = "byTest";
var chartContainer = $('#chart_div');

function changeView(){
	
	control.clear();
	control = new google.visualization.ControlWrapper({
		controlType: 'ChartRangeFilter',
		containerId: 'control_div',
		options: controlOptions
	});

	chartWrapper.clear();
	chartWrapper = new google.visualization.ChartWrapper({
		chartType: 'AreaChart',
		containerId: 'chart_div',
		options: options
	});
	dash.clear();
	dash = new google.visualization.Dashboard(
		document.getElementById('dashboard_div'));
	dash.bind([control], [chartWrapper]);

	if (mode == "byTest") {
        view.setColumns([3, 1, 2]);
		//chartWrapper.setOption({'hAxis.title':'Test Number'});
        dash.draw(view);
		addErrorHandling();
        mode = "byTime";
    } else {
        view.setColumns([0, 1, 2]);
		//chartWrapper.setOption({'hAxis.title':'Date and Time'});
        dash.draw(view);
		addErrorHandling();
        mode = "byTest";
    }
}
function addErrorHandling(){
	google.visualization.events.addListener(chartWrapper, 'error', errorHandler);
	google.visualization.events.addListener(control, 'error', errorHandler);
	google.visualization.events.addListener(dash, 'error', errorHandler);
}

function errorHandler(errorMessage) {
    //curisosity, check out the error in the console
    console.log(errorMessage);

    //simply remove the error, the user never see it
    google.visualization.errors.removeError(errorMessage.id);
}

function drawChart() {
	// vertical hover here: http://jsfiddle.net/asgallant/tVCv9/12/
	data = new google.visualization.DataTable();
	view = new google.visualization.DataView(data);
	data.addColumn('number', 'X');
	data.addColumn('number', 'Typing Speed (WPM)');
	//data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
	data.addColumn('number', 'Average Typing Speed (WPM)');
	data.addColumn('datetime', 'Date/Time');
	data.addColumn('number', 'Accuracy');
	data.addColumn('number', 'Test Time');
	data.addColumn('number', 'Test Id');
	data.addColumn('string', 'Text Title');
	data.addColumn('number', 'Keyboard Used');
	view.setColumns([0,1,2]);
	  

    data.addRows(ttData);
			
	var container = document.querySelector('#chart_div');
			
	// Overall Data
	// TOADD: Expected average speed if you keep up your improvement over the last 10 tests/days...
	var fastestWPM = data.getColumnRange(1).max;
	var slowestWPM = data.getColumnRange(1).min;
	var aveWPM = "-";
	var aveWPM_timeDiff = <?php echo ($onPersonalProfile) ? '\'<a href="/typing-test">Take another test!</a>\'' : '""';?>;
	var fastestIndexArr = data.getFilteredRows([{column:1, value:fastestWPM}])
	var slowestIndexArr = data.getFilteredRows([{column:1, value:slowestWPM}])
	var lastDate = data.getValue(data.getNumberOfRows() - 1, 3);
	var fastestDate = data.getValue(fastestIndexArr[fastestIndexArr.length - 1], 3);
	var slowestDate = data.getValue(slowestIndexArr[slowestIndexArr.length - 1], 3);
	var firstDate = data.getValue(0, 3);
	var currDateTime = new Date(<?php echo '"' . gmdate('Y-m-d H:i:s') . ' UTC"';?>.replace(/-/g,'/'));
	var totalNumTests = data.getNumberOfRows();
	
	if(totalNumTests > 1){
		aveWPM = data.getValue(data.getNumberOfRows() - 1, 2).toString();
		aveWPM_timeDiff = "Over a span of " + readableTimeDifference(firstDate, lastDate)
	}
	
	document.getElementById('totalNumTests').innerHTML = '<span class="bigTxt">' + <?php echo $numTotalTests; ?> + '</span>';
	
	document.getElementById('lastWPMdate').innerHTML = readableTimeDifference(lastDate, currDateTime) + " ago (" + formatAMPM(lastDate) + ", " + lastDate.toDateString() + ")";
	document.getElementById('fastestWPMdate').innerHTML = readableTimeDifference(fastestDate, currDateTime) + " ago (" + formatAMPM(fastestDate) + ", " + fastestDate.toDateString() + ")";
	document.getElementById('slowestWPMdate').innerHTML = readableTimeDifference(slowestDate, currDateTime) + " ago (" + formatAMPM(slowestDate) + ", " + slowestDate.toDateString() + ")";
	document.getElementById('aveWPMdateRange').innerHTML = aveWPM_timeDiff;
		
    dash = new google.visualization.Dashboard(document.getElementById('dashboard_div'));
        
	controlOptions = {
		filterColumnIndex: 0,
		ui: {
			chartOptions: {
				height: 50,
				width: 600,
				chartArea: {
					width: '80%'
				}
			},
			chartView: {
				columns: [0, 1]
			}
		}
	};
	
	var printTicksOption = "";
	var min_hAxisOption = "";
	var maxVaxis = 45;
	if(totalNumTests == 1){
		min_hAxisOption = 0;
	}
	
	if(totalNumTests < 10){
		for(var kk = 1; kk <= totalNumTests; kk++){
			printTicksOption += kk + ',';
		}
		printTicksOption = '[' + printTicksOption.substring(0, printTicksOption.length - 1) + ']';
	}

	
	if(fastestWPM > 40 && fastestWPM <= 80){
		maxVaxis = 90;
	} else if (fastestWPM > 80 && fastestWPM <= 125){
		maxVaxis = 135;
	} else if (fastestWPM > 125 && fastestWPM <= 170){
		maxVaxis = 180;
	} else if (fastestWPM > 170){
		maxVaxis = Math.round(fastestWPM + 20, 0);
	}
	
    options = {
		
		/*title: 'Typing Test Data',
		subtitle: 'subtitle',*/
		tooltip: {trigger: 'none'},
        hAxis: {
          title: 'Test Number'
        },
        vAxis: {
          title: 'Typing Speed (WPM)'
        },
		width: '95%',
		height: 500,
		lineWidth: 2,
		pointSize: 8,
		vAxis: { 
			gridlines: { 
				count: 10 
			},
			minValue:0,
			maxValue:maxVaxis
		},
		hAxis: {
			gridlines: {
				count: -1
			},
			textStyle: {
				fontSize: 18
			}
		},
        colors: ['#005E7C', '#64B4CF'],
		legend: {
			position: 'right'
			
		},
		explorer: {
			axis: 'horizontal',
			keepInBounds: true,
			maxZoomOut: 1.1,
			maxZoomIn: 0.05
		},
		trendlines: {
			0: {
				pointSize: 0,
				color: '#E67E22',
				lineWidth: 5,
				pointsVisible: false,
				opacity: 0.5,
				type: 'polynomial',
				labelInLegend: 'Speed Trend',
				visibleInLegend: true,
				tooltip: false
			} 
		}
      };

	  
	control = new google.visualization.ControlWrapper({
        controlType: 'ChartRangeFilter',
        containerId: 'control_div',
		options: controlOptions
    });
	
	chartWrapper = new google.visualization.ChartWrapper({
        chartType: 'AreaChart',
        containerId: 'chart_div',
		options: options
    });
	
	google.visualization.events.addListener(chartWrapper, 'click', hideYaxisDecimals);
	google.visualization.events.addListener(chartWrapper, 'ready', hideYaxisDecimals);
	// create 'ready' event listener to add mousemove event listener to the chart
    var runOnce = google.visualization.events.addListener(chartWrapper, 'ready', function () {
        google.visualization.events.removeListener(runOnce);
		
		$(container).bind('mousewheel DOMMouseScroll', function(event){
			// hide all y-axis or hAxis labels that are decimals leaving only integers
			//setTimeout(function(){$('#chart_div g g').find('g').filter(':contains(".")').hide()},2);
		});
		
        // create mousemove event listener in the chart's container
        // I use jQuery, but you can use whatever works best for you
        $(container).mousemove(function (e) {
			hideYaxisDecimals();
            var xPos = e.pageX - container.offsetLeft;
            var yPos = e.pageY - container.offsetTop;
            var cli = chartWrapper.getChart().getChartLayoutInterface();
			var cliBB = cli.getChartAreaBoundingBox();
            //var xBounds = cli.getBoundingBox('hAxis#0#gridline');
            //var yBounds = cli.getBoundingBox('vAxis#0#gridline');
            
            // is the mouse inside the chart area?
            if (
                (xPos >= cliBB.left && xPos <= cliBB.left + cliBB.width) &&
                (yPos >= cliBB.top + 30 && yPos <= cliBB.top + cliBB.height + 30) 
            ) {
                // if so, draw the vertical line here
                // get the x-axis value at these coordinates
                var xVal = cli.getHAxisValue(xPos);
				
				var dCol = 3;
				var dVal = xVal;
				// see if xaxis by test, otherwise assume by date
				if(typeof(xVal) == "number"){
					xVal = xVal.toFixed(0);
					dCol = 0;
					dVal = parseFloat(xVal);
				} else{
					var lastDiff = 0;
					var currDiff = -1;
					var ii = data.getNumberOfRows();
					
					while( (ii > 0) && (currDiff < lastDiff || lastDiff < 0)){
						ii--;
						lastDiff = currDiff;
						currDiff = Math.abs(xVal - data.getValue(ii,3));
					}
					var closestXval = (ii == 0 && currDiff < lastDiff) ? 0 : ii + 1;
					
					xVal = data.getValue(closestXval,3);
					dVal = xVal;
				}
				
				// now see if a datapoint is inside current view
				if(
					(dVal >= cli.getHAxisValue(cliBB.left) && dVal <= cli.getHAxisValue(cliBB.left + cliBB.width))
				){
						
					
					// set the x-axis value of the annotation
					////data.setValue(annotationRowIndex, 0, xVal.toFixed(0));
					// set the value to display on the line, this could be any value you want
				   //// data.setValue(annotationRowIndex, 5, "test");
					$("#cursorLine").show();
					$("#cursorLine").offset({left: cli.getXLocation(xVal) + container.offsetLeft - 2 /*+ 113*/, top: container.offsetTop + cliBB.top + 30});
					$("#cursorLine").height(cliBB.height);
					$("#hoverTT").show();
					$("#hoverTT").css({left: e.pageX - 190, top: container.offsetTop - 156 /*e.pageY - 300*/});
					// get the data value (if any) at the line
					// truncating xVal to one decimal place,
					// since it is unlikely to find an annotation like that aligns precisely with the data
					var rows = data.getFilteredRows([{column: dCol, value: dVal}]);
					if (rows.length) {
						var WPMvalue = data.getValue(rows[0], 1);
						var AccValue = data.getValue(rows[0], 4);
						var AveValue = data.getValue(rows[0], 2);
						var hoverTime = data.getValue(rows[0], 3);
						var testNum = data.getValue(rows[0], 0);
						var testLength = data.getValue(rows[0], 5);
						var testId = data.getValue(rows[0], 6);
						var textTitle = data.getValue(rows[0], 7);
						var keyboardUsed = keyboardLabels[data.getValue(rows[0], 8)];
						
						if(testLength >= 60){
							testLength = Math.round(testLength / 60);
							testLength += ' min';
						} else {
							testLength += ' sec';
						}
						
						// DEBUGGING HELP
						//$('#theValue').html("Value: " + WPMvalue + ", Ave: " + AveValue + ", Time: " + hoverTime);
						
						$('#TTwpm').html("<span class=\"bigTxt\">" + WPMvalue + "</span><span class=\"noteSmall\"> WPM</span>");
						$('#TTacc').html("Accuracy: " + AccValue + "%");
						$('#TTtestNum').html('Test #' + testNum);
						$('#TTtestLength').html(testLength + ' test (' + keyboardUsed + ')');
						$('#TTtextTitle').html("\"" + textTitle + "\"");
						if(AveValue != null){
							$('#TTaveWPM').html("(Average = " + AveValue + " <span class=\"note\"> wpm</span>)");
						}
						$('#TTdate').html(hoverTime.toDateString() + ", " + hoverTime.toLocaleTimeString());
<?php 	if($onPersonalProfile)
			{?>
						$('#TTresultPage').html("<a href=\"<?php echo Config::get('constants/rootUrl');?>/myResults.php?ttid=" + testId + "\" target=\"_blank\">View Report</a>");
<?php 		}?>
					}
                } else{
					$("#cursorLine").hide();
					$("#hoverTT").hide();
				}
                // draw the chart with the new annotation
                //dash.bind([control], [chartWrapper]);
				//dash.draw(view);
            } else {
				$("#cursorLine").hide();
				$("#hoverTT").hide();
			}
        });
    });
	addErrorHandling();
		
	dash.bind([control], [chartWrapper]);
	dash.draw(view);
	
	if(totalNumTests < 5){
		$('#control_div').hide();
	}
}

//create trigger to resize chart when page is resized   
$(window).resize(function() {
	dash.draw(view);
	hideYaxisDecimals();
});

setInterval( hideYaxisDecimals, 20);

function hideYaxisDecimals(){
	$('#chart_div g g').find('g').filter(':contains(".")').hide();		// hide all y-axis or hAxis labels that are decimals leaving only integers
}

function getValidJSdate(theDateStr){
	return theDateStr;//.substring(0,10) + 'T' + theDateStr.substring(11);
}
</script>

<!--<input type="button" value="Toggle Time/Test" onclick="changeView()" />-->
<div><span id="theValue"></span></div>
<div id="hoverTT" class="orangeCTA stoTT">
	<div id="TTtestNum" style="padding:6px 2px 2px 2px; font-size:1.2em;"></div>
	<div id="TTdate" class="divider"></div>
	<div id="TTwpm" class="boldFont bigTxt"></div>
	<div id="TTacc" class="boldFont"></div>
	<div id="TTaveWPM"></div>
	<div id="TTtextTitle"></div>
	<div id="TTtestLength"></div>
	<div id="TTresultPage"></div>
</div>


<div id="cursorLine" style="position:absolute; width:4px; background:rgba(205, 140, 48, 0.65); z-index:149; display:none;"></div>

<h2>Typing Test Data</h2>
<div id="dashboard_div">
	
	<div id="chart_div"></div>
	<div id="control_div"></div>
</div>

<div id="dialog-confirm" title="Permanently delete all your typing test data?" style="display: none;">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Your typing test data will be permanently deleted and cannot be recovered.</p>
	<p>Are you sure?</p>
</div>

<?php
} // connects to if user has no typing test history around line 100 (empty $ttResults)
}

require_once 'includes/overall/footer.php';
?>