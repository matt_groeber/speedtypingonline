<?php
$currPage = "login";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

$userToCheck = new User();
$titleSuffix = '';

if($userToCheck->isLoggedIn() && !inMaintenanceMode()){
	if(Cookie::exists(Config::get('remember/cookie_loginRedirect'))){
		$redirectURL = Cookie::get(Config::get('remember/cookie_loginRedirect'));
		Cookie::delete(Config::get('remember/cookie_loginRedirect'));
		Redirect::to($redirectURL);
	} else {
		Redirect::to('user/' . $userToCheck->data()->username . '/test-stats');
	}
}

if(Input::exists() && !inMaintenanceMode()){	
	if(Token::check(Input::get('token'))){
		$captchaObj = new Captcha();
		$captchaPass = $captchaObj->passedCaptcha(Input::get('humanAns'), 'captcha_ans');
		
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'username' => array(
				'name' => 'Username',
				'required' => true
			),
			'password' => array(
				'name' => 'Password',
				'required' => true,
				'equalsTrue' => array($captchaPass, 'You answered incorrectly. Please try again.')
			)
		));
		
		if($validation->passed()){
			$user = new User();
			
			$remember = (Input::get('remember') === 'on') ? true : false;
			$login = $user->login(Input::get('username'), Input::get('password'), $remember);
			
			if($login === true){
				if(Input::exists('get') && Input::get('loginfirst')){
					Redirect::to('goPremium.php');
				} else {
					Redirect::to('user/' . $user->data()->username . '/profile');
				}
			} else if($login == 'Inactive User'){
				Session::flash('failed', '<h2>Inactive Account</h2><p>Check your email for your activation link if you have just registered.</p>', 'ERROR');
				Redirect::to($_SERVER['REQUEST_URI']);		// redirect to current page
			} else {
				$captchaObj->addFailedLoginAttempt();
				Session::flash('failed', '<h2>Login failed</h2><p>Make sure you are using the correct username and password.</p>', 'ERROR');
				Redirect::to($_SERVER['REQUEST_URI']);		// redirect to current page
			}
		} else {
			$errorsToPrint = '<ol><li>' . implode('</li><li>', $validation->errors()) . '</li></ol>';
		}
	} 
} else if(Input::exists('get')){
	if(Input::get('loginfirst')){
		$titleSuffix = ' First';
	}
}
$htmlTitle = "Account Login";
$htmlDescription = "Log in to your SpeedTypingOnline.com account";
$cssFiles = "be";
if(inMaintenanceMode()){
	$noWidgets = true;
}
$addContainer = true;
require_once 'includes/overall/header.php';

if(inMaintenanceMode()){
	echo '<h2>Site is currently under Maintenance</h2>';
	echo '<h4>User login is currently disabled as a major site-wide upgrade is currently underway.</h4>';
	echo '<h4>Please feel free to continue using the site as normal but unfortunately none of your progress will be saved.</h4>';
	echo '<h4>Sorry for any inconvenience and thank you for your patience!</h4>';
	exitPHPwithFooter();
}

if(Session::exists('failed')){
	echo Session::flash('failed');
} else {
	echo Session::flash('updated');
}
?>

<h2>Login to your Account<?php echo $titleSuffix; ?></h2>
</div> <!-- <div id="heading"> -->


<div id="pageMain">
	<div class="responsiveCols">
		<div class="leftCol socialLogin">
			<?php include $_SERVER['DOCUMENT_ROOT'] . '/includes/login_google.php';?>
		</div>
		<div style="position: relative; width: 40px; height: 300px; margin: 0px 10px 10px;">
			<div class="line"></div>
				<div class="wordwrapper" style="text-align: center; height: 12px; position: absolute; left: 0; right: 0; top: 50%; margin-top: -12px; z-index: 2;">
					<div class="word" style="color: #ccc; text-transform: uppercase; letter-spacing: 1px; padding: 3px; font: bold 12px arial,sans-serif; background: white;">or</div>                                        
			</div>
		</div>
		<div class="rightCol">
			<form action="" method="post">
				<div class="field alignInput">
					<label for="username">Username:</label>
					<input type="text" name="username" id="username" autocomplete="off">
				</div>
				
				<div class="field alignInput">
					<label for="password">Password:</label>
					<input type="password" name="password" id="password" autocomplete="off">
				</div>
				
				<div class="field checkbox">
					<label for="remember">
						<input type="checkbox" name="remember" id="remember" checked="checked"> Keep me logged in
					</label>
				</div>
				
				<?php
				$captchaObj = new Captcha();
				if($captchaObj->doCaptcha()){?>
					<div class="field alignInput">
						<?php
							$captcha = $captchaObj->getCaptcha();
							
							// display question to user as part of form
							echo '<p><span class="boldFont">Question</span>: ' . htmlentities($captcha['q']) . '</p>';

							// store answers in session
							Session::put('captcha_ans',$captcha['a']);
						?>
						<label for="humanAns" class="boldFont">Answer:</label>
						<input type="text" name="humanAns" id="humanAns" autocomplete="off">
					</div>
				<?php
				} else {
					Session::delete('captcha_ans');
				}?>
				
				<div id="formErrors"><?php echo (isset($errorsToPrint)) ? $errorsToPrint : '';?></div>
				
				<input type="hidden" name="token" value="<?php echo Token::generateToken(Config::get('session/token_name')); ?>">
				<input class="mediumBtn flatBlueBtn" type="submit" value="Log in">
				
			</form>
			<div class="note">
				Recover your <a href="/recover.php?mode=username">username</a> or <a href="/recover.php?mode=password">password</a>
			</div>
			<div class="note">
				Don't have an account yet? <a href="/register.php">Register Here</a>
			</div>
		</div>
	</div>
	
	
</div>
<style>
	.line{margin-top:20px; position: absolute; left: 49%; top: 0; bottom: 0; width: 1px; background: #ddd; z-index: 1;}
	.rightCol .note{margin-top:20px;}
	@media only screen and (max-width: 1248px){
		aside{display:none;}
		#heading, #pageMain{margin-right:0px;}
	}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tools/1.2.7/jquery.tools.min.js" type="text/javascript"></script>
<?php
require_once 'includes/overall/footer.php';
?>