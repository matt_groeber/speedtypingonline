<?php
class Token {
		
	public static function generateToken($name) {
		// problems with token seem to be fixable here
		$backtrace = debug_backtrace();
		$uniqid = md5(uniqid());
		Session::put($name, $uniqid);
		return $uniqid;
	}
	
	public static function check($token, $tokenVarName = '') {
		if($tokenVarName === ''){
			$tokenName = Config::get('session/token_name');
		} else {
			$tokenName = Config::get('session/'.$tokenVarName);
		}
			
		if(Session::exists($tokenName) && $token === Session::get($tokenName)){
			Session::delete($tokenName);
			return true;
		} 
		return false;
	}
}