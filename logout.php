<?php
$cssFiles = "be";
require_once 'core/init.php';

$user = new User();
$user->logout();

$redirectPath = '/typing-test';

if(Input::exists('get')){	
	$redirectPath = Input::get('redir');
}

Redirect::to($redirectPath);
?>