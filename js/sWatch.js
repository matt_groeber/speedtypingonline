var sWatch = null;
var TotalSeconds;
var RoundTime = 0;
var TimeWasReset = true;
var TimeIsStopped = false;
var localCursor = 0;
var cursor = 0;
var lettersTypedCorrectly = 0;
var lettersTypedIncorrectly = 0;
var badEntriesDeleted = 0;
var totalEntriesTyped = 0;
var wordsTyped = 0;
var blockDiv = null;
var lineDiv = null;
var textString = "";
var deletes = 0;
var totalDeletes = 0;
var lineDivText = null;
var lineNum = 0;
var blockDivText = new Array("","","","");
var cutoff = 0;
var phaseShift = 0;
var pShiftWeight = 0;
var minCursorReplaceStart = 0;
var testCounter = 0;
var textSelection = "CL1";
var resultsTitle = "Lesson Results";
var TestInfo = {};
var disableKeyboard = true;


function InitializeTimer_block(Time, TimerID){
    
    cursor = 24;        //first letter has "<span class="plainText">" before it
    localCursor = 24;
    cutoff = 0;
    lineNum = 0;
    blockDivText[0] = "";
    blockDivText[1] = "";
    blockDivText[2] = "";
    blockDivText[3] = "";
    sWatch = document.getElementById(TimerID);
    TotalSeconds = Time;
	totalDeletes = 0;
	pShiftWeight = 0;
    
	disableKeyboard = true;
    GetText();
}

function CreateTimer_block(event, TimerID, Time) {

	if(disableKeyboard){
		return false;
	}
    var functionKeyPressed = true;     // used to flag when function key (delete, shift...) is pressed
    var tempBlockDivStr = new Array("","","","");
    var NUM_LINES = 4;
    var MAX_CHARS_IN_LINE = 42;

    
    if (TotalSeconds==undefined || TimeWasReset) {
        cursor = 24;        //first letter has "<span class="plainText">" before it
        localCursor = 24;
        TimeWasReset = false;
        RoundTime = Time;

        sWatch = document.getElementById(TimerID);
        TotalSeconds = Time;

        UpdateTimer()
        window.setTimeout("Tick()", 500);
    }
    
    if (TimeWasReset){
        TotalSeconds = Time;
        TimeWasReset = false;   //reset TimeWasReset flag        
    }
	
    
    // This makes the box disabled when time is up (end if all text has been typed or after 30 mins (1800 secs)
    if (((lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight) >= textString.length - 1) || (TotalSeconds > 1199) ) {
		if(!TimeIsStopped){
			StopTime();
		}
	
        return false;
    }
    
    // Get text in RTE box
    var newRTEText = "";
    var RTELetter = blockDivText[lineNum].substring(localCursor,(localCursor+1));
    
    var keyCode_entered = event.keyCode
    
	/* USED FOR QWERTY/DVORAK FUNCTIONALITY
    if($('#keyLayoutSelected').val().toUpperCase() == "DVORAK"){
        var key_entered = d_keycode.d_getValueByEvent(event);
    }
    else{       // default to QWERTY
	*/
    var key_entered = keycode.getValueByEvent(event);
    /*}
	*/
    
    

            // event.keyCode: 97-122 = a-z, 65-90 = A-Z, 8 = backspace, 32 = space
    if( key_entered == RTELetter){  /* characters match */
        functionKeyPressed = false;
        
        if(RTELetter == " "){
            newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "goodSpace" 
                            + blockDivText[lineNum].substring(localCursor-2);   
            wordsTyped++;
        }
        else{    
            newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "goodEntry" 
                        + blockDivText[lineNum].substring(localCursor-2);
        }
        cursor += 32;
        localCursor += 32;
        lettersTypedCorrectly += 1;
        totalEntriesTyped += 1;
        
        if(phaseShift != 0){
            phaseShift = 0;     // correct entry resets any possible phase shift
        }
        
    }      
    else if((keyCode_entered==32)||(keyCode_entered==42)||(keyCode_entered==43)||(keyCode_entered==45)||
            ((keyCode_entered >= 47  ) && ( keyCode_entered <= 59))||
            ((keyCode_entered >= 61  ) && ( keyCode_entered <= 90))||
            ((keyCode_entered >= 96  ) && ( keyCode_entered <= 111))||
            ((keyCode_entered >= 186  ) && ( keyCode_entered <= 222))){
        functionKeyPressed = false;
        
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION START >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // // -------------------------------------------------------------------------------------
        // **FIRST** see if we are looking for a phase shift
        if(phaseShift != 0){
            if(phaseShift == 1){
                if(key_entered == blockDivText[lineNum].substring(localCursor+32, localCursor+33)){
                    //we have a phase shift to the right
                    var shiftRight = true;
					pShiftWeight++;
                }
            }
            else{   //phase shift = -1
                if(key_entered == blockDivText[lineNum].substring(localCursor-32, localCursor-31)){
                    var shiftLeft = true;
					pShiftWeight--;
                }
            }
            phaseShift = 0;     // phase shift has been corrected and realigned
            
        }
        // **THEN** see if we should look for a phase shift to the right (positive time)
        else if(key_entered == blockDivText[lineNum].substring(localCursor+32, localCursor+33)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = 1;     //only set the flag if the user wants to do phase shift correction
            }
        }
        // see if we should look for a phase shift to the left (negative time)
        else if(key_entered == blockDivText[lineNum].substring(localCursor-32, localCursor-31)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = -1;     //only set the flag if the user wants to do phase shift correction
            }
        }
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION END >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // -------------------------------------------------------------------------------------
            
        if(shiftRight){
            newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "phaseRght" 
                         + blockDivText[lineNum].substring(localCursor-2,localCursor+21) + "goodEntry"
                         + blockDivText[lineNum].substring(localCursor+30);
            cursor +=32;
            localCursor +=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   Since we skipped a letter do not increment totalLettersTyped
            shiftRight = false;  // phase shift should have been successfully applied by now
        }
        else if(shiftLeft){
            newRTEText = blockDivText[lineNum].substring(0,localCursor-43) + "phaseLeft" 
                         + blockDivText[lineNum].substring(localCursor-34);
            cursor -=32;
            localCursor -=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   Removed because it was counting twice
            shiftLeft = false;  // phase shift should have been successfully applied by now
        }
        // we do not have a case of phase shift, so continue with bad input
        else if(blockDivText[lineNum].substring(localCursor, localCursor+1) == " "){   /* this was RTELetter == " " */
            newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "bad_Space" 
                         + blockDivText[lineNum].substring(localCursor-2);
            wordsTyped++;
        }
        else{                     
            newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "bad_Entry" 
                         + blockDivText[lineNum].substring(localCursor-2);
        }
        
        cursor += 32;
        localCursor += 32;
        lettersTypedIncorrectly += 1;
        totalEntriesTyped += 1;
    }
    else if( event.keyCode == 8 ){      /*no match and it is a backspace, and its not beginning of text*/ 
        // backstep first
        cursor -= 32;
        functionKeyPressed = false;
		totalDeletes++;

        if(localCursor == 24){
            var newLineNum = (lineNum + (NUM_LINES - 1)) % NUM_LINES;
            var newLocalCursor = blockDivText[newLineNum].length - 8;      // wrap localcursor around end of text
            
            if(blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) != "plainText" && cursor > 24){
                
                if(blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) == "bad_Space"){
                    badEntriesDeleted += 1;       // user has deleted bad space
                    totalEntriesTyped -= 1;
                    wordsTyped--;
                }
                else if(blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) == "goodSpace"){
                    wordsTyped--;
                }
                else if(blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) == "bad_Entry"
                        || blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) == "phaseLeft"
                        || blockDivText[newLineNum].substring(newLocalCursor-11,newLocalCursor-2) == "phaseRght"){
                        badEntriesDeleted += 1;       // user has deleted error
                        totalEntriesTyped -= 1;
                    }
                    
                blockDivText[lineNum] = blockDivText[lineNum].substring(0,localCursor-11) 
                                        + "plainText" + blockDivText[lineNum].substring(localCursor-2);
            
                lineNum = newLineNum;
                localCursor = newLocalCursor;
                }
             else{
                 
                 cursor += 32;
                 deletes--;
				 totalDeletes--;
             }
             newRTEText = blockDivText[lineNum].substring(0,localCursor-11) + "nxtLetter" 
                          + blockDivText[lineNum].substring(localCursor-2);
                    
        } 
        else{
            localCursor -= 32;
            if(blockDivText[lineNum].substring(localCursor-11, localCursor-2) == "bad_Space" 
                || blockDivText[lineNum].substring(localCursor-11, localCursor-2) == "bad_Entry"
                || blockDivText[lineNum].substring(localCursor-11, localCursor-2) == "phaseLeft"){
                    badEntriesDeleted += 1;
                    totalEntriesTyped -= 1;
            }
            if(blockDivText[lineNum].substring(localCursor-11,localCursor-2) == "bad_Space"){
                wordsTyped--;
            }
            if(blockDivText[lineNum].substring(localCursor-11,localCursor-2) == "goodSpace"){
                wordsTyped--;
            }
            newRTEText = blockDivText[lineNum].substring(0, (localCursor+21)%blockDivText[lineNum].length) 
                         + "plainText" + blockDivText[lineNum].substring((localCursor+30)%blockDivText[lineNum].length);
                     
            newRTEText = newRTEText.substring(0,localCursor-11) + "nxtLetter" 
                        + newRTEText.substring(localCursor-2);
        }
        
        

        deletes++;
    }
    
    
    // move cursor to next letter
    if( localCursor > blockDivText[lineNum].length){
        
        blockDivText[(lineNum+1)%NUM_LINES] = blockDivText[(lineNum+1)%NUM_LINES].substring(0, 13)
                     + "nxtLetter" + blockDivText[(lineNum+1)%NUM_LINES].substring(22);

    }
    else{
        newRTEText = newRTEText.substring(0, (localCursor-11))
                     + "nxtLetter" + newRTEText.substring(localCursor-2);
    }
    
    // wraparound and copy to div or just copy to div depending
    if(localCursor > blockDivText[lineNum].length){
        if (cursor > (minCursorReplaceStart*32/*MAX_CHARS_IN_LINE*2*32*/)){ //make sure its not the first two lines being typed.
            if(deletes == 0){    // && deletes == 0 so no update if in the past
                tempStr = textString.substring(cutoff, (42*(i+1)) - ((42*i) - cutoff));   // get next 42 characters from text string

                var localCutoff = tempStr.search(/(\r\n|\n|\r)/);       // find first newline in 42 chars
                if (localCutoff < 1){      // if next 42 characters do not contain a carriage return (\n)


                    localCutoff = tempStr.lastIndexOf(" ") + 1;                 // find last space in 42 chars

                    if(localCutoff == 0){
                        localCutoff = 42;   // if no space found within next 42 chars, cut word at full block line length
                    }

                    blockDivText[(lineNum+(NUM_LINES-2))%NUM_LINES] = "";    // initialize string
                    for(j=0; j<localCutoff; j++){

                        blockDivText[(lineNum+(NUM_LINES-2))%NUM_LINES] += '<span class="plainText">' + tempStr.substring(j, j+1) + '</span>';
                    }
                    cutoff += localCutoff;
                }
                else{       // next 42 characters contained a carriage return
                    var tempStr2 = tempStr.substring(0, localCutoff+1);
                    tempStr2 = tempStr2.replace(/(\r\n|\n|\r|\s\r\n|\s\n|\s\r)/," ");


                    blockDivText[(lineNum+(NUM_LINES-2))%NUM_LINES] = "";    // initialize string
                    for(j=0; j<tempStr2.length; j++){

                        blockDivText[(lineNum+(NUM_LINES-2))%NUM_LINES] += '<span class="plainText">' + tempStr2.substring(j, j+1) + '</span>';
                    }
                    cutoff += localCutoff + 1;

                }
            }            
        }
       
        
        blockDivText[lineNum] = newRTEText;
        tempBlockDivStr[0] = blockDivText[0];
        tempBlockDivStr[1] = blockDivText[1];
        tempBlockDivStr[2] = blockDivText[2];
        tempBlockDivStr[3] = blockDivText[3];
        blockDiv.innerHTML = formatForInnerHTML_block(tempBlockDivStr);
        
        localCursor = 24;
        lineNum++;
        lineNum = lineNum % 4;
    }
    else if(!functionKeyPressed){
        blockDivText[lineNum] = newRTEText;
        tempBlockDivStr[0] = blockDivText[0];
        tempBlockDivStr[1] = blockDivText[1];
        tempBlockDivStr[2] = blockDivText[2];
        tempBlockDivStr[3] = blockDivText[3];
        blockDiv.innerHTML = formatForInnerHTML_block(tempBlockDivStr);
    }
    
    
    if(deletes > 0 && event.keyCode != 8){
        deletes--;
    }
	
	
	keyIndicator(blockDivText[lineNum].substring(localCursor,(localCursor+1)));

    return false;
}

function Tick() {
    if ( ((lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight) >= textString.length - 1) || (TotalSeconds > 1199) ){
        if(!TimeIsStopped)
			StopTime();
        return;
    }

    if(!TimeWasReset){
    TotalSeconds += 0.5;
    UpdateTimer()
    window.setTimeout("Tick()", 500);
    }
    UpdateStats()
}

function StopTime() {
		TimeIsStopped = true;
        UpdateTimer();
        UpdateStats();
        TimeEnd();
		}


function UpdateTimer() {
    var Seconds = TotalSeconds;
    var Minutes = Math.floor(Seconds / 60);
    Seconds -= Minutes * 60;
	Seconds = Math.floor(Seconds);
    var TimeStr = (LeadingZero(Minutes) + ":" + LeadingZero(Seconds))
    sWatch.innerHTML = TimeStr;
}

function LeadingZero(Time) {
    return (Time < 10) ? "0" + Time : + Time;
}


function Reset(ResetTime, TimerID) {
    sWatch = document.getElementById(TimerID);
    TotalSeconds = ResetTime;
    lettersTypedCorrectly = 0;
    lettersTypedIncorrectly = 0;
    badEntriesDeleted = 0;
    totalEntriesTyped = 0;
    wordsTyped = 0;
	totalDeletes = 0;
	pShiftWeight = 0;
    
    UpdateTimer();   
    UpdateStats();

    InitializeTimer_block(ResetTime, TimerID);
    TimeWasReset = true;
	TimeIsStopped = false;
    
    $('.mainDivInputs').focus();    //assume user wants to type after clicking reset
    TimeBegin();
    
}

function TimeBegin(){
    $("#sWatch").css("background-color","transparent");
    $("#divCover").css("z-index", "0");
	$('#PRIW').hide();
    
    $("#resultDivContainer").hide('fast', function(){
		$('.mainDivInputs').focus();    // assume user wants to type after using hotkey
	});
}

function TimeEnd(){
    TestInfo.WPM = pad(getNetWPM(), 3);
	TestInfo.acc = pad((getAccuracy() * 10),3);
    TestInfo.endTS = Math.round(Date.now()/1000);
	TestInfo.testLength = pad($('#lessonLengthSelected').val(),2);
	TestInfo.usrName = '';
	TestInfo.usrNameChkSum = 0;
	TestInfo.resultTitle = '';
	TestInfo.resultTitleChkSum = 0;
	TestInfo.pn = 524286;
	
	if(TestInfo.acc == 0){
		TestInfo.acc = "000";
	} else if(TestInfo.acc.length < 3){
		TestInfo.acc = "0" + TestInfo.acc;
	} else if(TestInfo.acc.length > 3){
		TestInfo.acc = TestInfo.acc.substring(0,3);
	}
	
	if(TestInfo.testLength.length == 2)
		TestInfo.testLength = "0" + TestInfo.testLength;
	
    $("#sWatch").css("background-color","red");
	$("#divCover").css("z-index", "400");
    
    UpdateResults();
    
    var WPM = getNetWPM();
    var twtrTwt = "https://twitter.com/home?status=Just took a free typing test online - I type " + WPM + " words per minute! How fast are you? - https://www.speedtypingonline.com";
    var newURL = "https://www.facebook.com/share.php?u=https://www.speedtypingonline.com/fbr.php?r=" + encodeURIComponent(getNetWPM()*492368);
    $("#fbrLink").attr("href", newURL);
    $("#twtrLink").attr("href", twtrTwt);
	
	generateGenLink();
    
    if ($("#resultDivContainer").is(":hidden")) {
            $("#resultDivContainer").show('fast', function() {});
			$('#PRIW').show();
    }

}

function generateGenLink(){
	var newUserName = $('#personName').val();
	if(newUserName != '' && newUserName != "<Name>"){
		var myObj = TextToAscii(newUserName);
		TestInfo.usrName = myObj.ascii;
		TestInfo.usrNameChkSum = myObj.chkSum;
	} else {
		TestInfo.usrName = '';
	}
	
	var shareURL = "https://www.speedtypingonline.com/myResults.php?r=" + encodeURIComponent(parseInt("1" + TestInfo.WPM + TestInfo.acc + TestInfo.testLength) * TestInfo.pn);
	shareURL += "&d=" + encodeURIComponent(TestInfo.endTS * TestInfo.pn);
	
	if(TestInfo.usrName != ''){
		shareURL += "&n=" + encodeURIComponent(TestInfo.usrName);
		shareURL += "&c=" + encodeURIComponent(TestInfo.usrNameChkSum * TestInfo.pn);
	}
	
	if(resultsTitle != ''){
		var myObj = TextToAscii(resultsTitle);
		TestInfo.resultTitle = myObj.ascii;
		TestInfo.resultTitleChkSum = myObj.chkSum;
		
		shareURL += "&rt=" + encodeURIComponent(TestInfo.resultTitle);
		shareURL += "&rtc=" + encodeURIComponent(TestInfo.resultTitleChkSum * TestInfo.pn);
	}
	
	$('#genLink').attr("href", shareURL);
	$('#genLinkDisplay').attr("value", shareURL.substring(8));
	$('#genLinkEmail').attr("href", "mailto:example@domain.com?subject=" + encodeURIComponent("My Typing Test Results") + "&body=" + encodeURIComponent("Here is the link to my typing results:") + "%0D" + encodeURIComponent(shareURL));
}

function TextToAscii(s)
{
	var retObj = {ascii: 0, chkSum: 0};
	var ascii="";
	var sum=0;
	if(s.length>0)
		for(i=0; i<s.length; i++){
			var c = s.charCodeAt(i).toString(16);
			while(c.length < 2)
			c = "0"+c;
			ascii += c;
			sum += parseInt(c,16);
		}
	
	retObj.ascii = ascii;
	retObj.chkSum = sum;

	return retObj;
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function CreateTimer_line(event, TimerID, Time) {
	
	if(disableKeyboard){
		return false;
	}

    var functionKeyPressed = true;     // used to flag when function key (delete, shift...) is pressed
    var lineMiddle = (21*32)-8;

    
    if (TotalSeconds==undefined || TimeWasReset) {
		cursor = 24;        //first letter has "<span class="plainText">" before it
		localCursor = 0;
		TimeWasReset = false;
		TotalSeconds = Time;
		
		sWatch = document.getElementById(TimerID);
		TotalSeconds = Time;
		
		UpdateTimer()
		window.setTimeout("Tick()", 500);
    }
    
    if (TimeWasReset){
		// OLD DISABLING $('#main_input').attr('disabled', false);
		TotalSeconds = Time;
		TimeWasReset = false;   //reset TimeWasReset flag        
    }
    
    // This makes the box disabled when time is up
    if (((lettersTypedCorrectly + lettersTypedIncorrectly - totalDeletes + pShiftWeight) >= textString.length - 1) || (TotalSeconds > 1199) ){
        return false;
    }
    
    // Get text in RTE box
    var newRTEText = "";
    var RTELetter = lineDivText.substring(lineMiddle,lineMiddle+1);
    
    var keyCode_entered = event.keyCode
    var key_entered = keycode.getValueByEvent(event);
    
    //var new1 = keycode.getValueByEvent(event);

            // event.keyCode: 97-122 = a-z, 65-90 = A-Z, 8 = backspace, 32 = space
    if( key_entered == RTELetter){  /* characters match */
        functionKeyPressed = false;
        
        if(RTELetter == " "){
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "goodSpace" 
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30) 
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>";   
            wordsTyped++;
        }
        else{
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "goodEntry"
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30) 
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>";  
        }
        
        cursor += 32;
        lettersTypedCorrectly += 1;
        totalEntriesTyped += 1;
        
        if(phaseShift != 0){
            phaseShift = 0;     // correct entry resets any possible phase shift
        }
        
        if(localCursor < 20) {localCursor += 1;}
    }      
    else if((keyCode_entered==32)||(keyCode_entered==42)||(keyCode_entered==43)||(keyCode_entered==45)||
            ((keyCode_entered >= 47  ) && ( keyCode_entered <= 59))||
            ((keyCode_entered >= 61  ) && ( keyCode_entered <= 90))||
            ((keyCode_entered >= 96  ) && ( keyCode_entered <= 111))||
            ((keyCode_entered >= 186  ) && ( keyCode_entered <= 222))){
        functionKeyPressed = false;
        
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION START >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // // -------------------------------------------------------------------------------------
        // **FIRST** see if we are looking for a phase shift
        if(phaseShift != 0){
            if(phaseShift == 1){
                if(key_entered == lineDivText.substring(lineMiddle+32, lineMiddle+33)){
                    //we have a phase shift to the right
                    var shiftRight = true;
					pShiftWeight++;
                }
            }
            else{   //phase shift = -1
                if(key_entered == lineDivText.substring(lineMiddle-32, lineMiddle-31)){
                    var shiftLeft = true;
					pShiftWeight--;
                }
            }
            phaseShift = 0;     // phase shift has been corrected and realigned
            
        }
        // **THEN** see if we should look for a phase shift to the right (positive time)
        else if(key_entered == lineDivText.substring(lineMiddle+32, lineMiddle+33)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = 1;     //only set the flag if the user wants to do phase shift correction
            }
        }
        // see if we should look for a phase shift to the left (negative time)
        else if(key_entered == lineDivText.substring(lineMiddle-32, lineMiddle-31)){
            if ($('#psCheckbox').is(':checked')){
                phaseShift = -1;    //only set the flag if the user wants to do phase shift correction
            }   
        }
        // -------------------------------------------------------------------------------------
        // <<<<<<<<<<<<<<<<<<<<<<<<<<<<  PHASE SHIFT DETECTION END >>>>>>>>>>>>>>>>>>>>>>>>>>>>
        // -------------------------------------------------------------------------------------
        
        if(shiftRight){
            newRTEText = lineDivText.substring(64,lineMiddle-11) + "phaseRght" 
                         + lineDivText.substring(lineMiddle-2,lineMiddle+21) + "goodEntry"
                         + lineDivText.substring(lineMiddle+30,lineMiddle+53) + "nxtLetter"
                         + lineDivText.substring(lineMiddle+62) + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>"
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+40)/32)+1, ((cursor+lineMiddle+40)/32)+2) + "</span>";
            cursor +=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   Do not count since we are jumping ahead by 1 letter
            shiftRight = false;  // phase shift should have been successfully applied by now
            
            if(localCursor < 20) {localCursor += 1;}
        }
        else if(shiftLeft){
            newRTEText = lineDivText.substring(0,lineMiddle-43) + "phaseLeft" 
                         + lineDivText.substring(lineMiddle-34);
            cursor -=32;
            lettersTypedIncorrectly -= 1;
            lettersTypedCorrectly += 1;
            //totalEntriesTyped += 1;   For some reason double counts with this line
            shiftLeft = false;  // phase shift should have been successfully applied by now
            
            if(localCursor < 20) {localCursor += 1;}
        }    
        else if(RTELetter == " "){   /* put an _ if space typed in wrong */
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "bad_Space"
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30)
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>";  
            wordsTyped++;

        }
        else{
            newRTEText = lineDivText.substring(32, lineMiddle-11) + "bad_Entry"
                         + lineDivText.substring(lineMiddle-2, lineMiddle+21) 
                         + "nxtLetter" + lineDivText.substring(lineMiddle+30) 
                         + '<span class="plainText">'
                         + cleanTextString.substring(((cursor+lineMiddle+8)/32)+1, ((cursor+lineMiddle+8)/32)+2) + "</span>";  
        }
        
        cursor += 32;
        lettersTypedIncorrectly += 1;
        totalEntriesTyped += 1;
        
        if(localCursor < 20) {localCursor += 1;}
    }
    else if( event.keyCode == 8 && cursor > 24 && localCursor > 0){      /*no match and it is a backspace*/ 
        functionKeyPressed = false;
		totalDeletes++;
        // backstep first
            cursor -= 32;
            localCursor -= 1;
            
        if(lineDivText.substring(lineMiddle-43, lineMiddle-34) == "bad_Space"){
            wordsTyped--;
            //totalEntriesTyped -= 1;
        }
        
        if(lineDivText.substring(lineMiddle-43, lineMiddle-34) == "goodSpace"){
            wordsTyped--;
        }
        
        if(lineDivText.substring(lineMiddle-43, lineMiddle-34) == "bad_Space"
                || lineDivText.substring(lineMiddle-43, lineMiddle-34) == "bad_Entry"
                || lineDivText.substring(lineMiddle-43, lineMiddle-34) == "phaseLeft"){
            badEntriesDeleted++;
            //totalEntriesTyped -= 1;
        }
            
        if(cursor < lineMiddle){
            //localCursor = lineDivText.length - 8;
            newRTEText = '<span class="hiddenTxt"> </span>'
                         + lineDivText.substring(0, lineMiddle-43) + "nxtLetter" 
                         + lineDivText.substring(lineMiddle-34,lineMiddle-11) + "plainText"
                         + lineDivText.substring(lineMiddle-2,lineDivText.length-32);    
        } 
        else{
           //newRTEText = blockLineText.substring(0,localCursor-11) + "plainText" + blockLineText.substring(localCursor-2);
            
           newRTEText = //textString.substring((cursor-(lineMiddle+8))/32, ((cursor-(lineMiddle+8))/32)+1)
                        '<span class="hiddenTxt"> </span>'
                        + lineDivText.substring(0, lineMiddle-43) + "nxtLetter" 
                         + lineDivText.substring(lineMiddle-34,lineMiddle-11) + "plainText"
                         + lineDivText.substring(lineMiddle-2,lineDivText.length-32);        
        }   

        deletes++;
    }
    
    if(!functionKeyPressed){
        lineDivText = newRTEText;
        lineDiv.innerHTML = formatForInnerHTML(newRTEText);
    }

	
		keyIndicator(lineDivText.substring(lineMiddle,lineMiddle+1));
    

    return false;
}

function formatForInnerHTML(StringToFormat){
    
    var StringToReturn = "";
    
    StringToFormat = StringToFormat.replace(/> </g, ">&nbsp;<");
    //StringToFormat = StringToFormat.replace(/">/g, '">&#8203;');
   
    StringToReturn = '<div id="line_input">' + StringToFormat + "</div>";

    return StringToReturn;
}

function formatForInnerHTML_block(StringToFormat){
    
    var StringToReturn = "";
    
    for(i=0; i < 4; i++){
        StringToFormat[i] = StringToFormat[i].replace(/> </g, ">&nbsp;<");
        //StringToFormat[i] = StringToFormat[i].replace(/">/g, '">&#8203;');
        
        StringToReturn += '<div id="blockLine' + i + '" class="blockLines">' 
                            + StringToFormat[i]
                            + "</div>";
    }

   
   //StringToFormat = StringToFormat.replace(/">/g, '"><span class="junkSpace"> </span>');

    return StringToReturn;
}

function setupFocus(){
        /*$('.mainDivInputs').bind('blur', function() {
        this.focus();
    });*/
    
    /*
    $('.mainDivInputs').bind('click', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('mouseleave', function() {
        this.focus();
    });
    
    $('.mainDivInputs').bind('mouseout', function() {
        this.focus();
    });
    
    $('.mainDivInputs').focus(function() {
        //ApplyColorsFocused();
    });
    
    $('.mainDivInputs').blur(function() {
        //ApplyColorsBlurred();
    });
    */

}


keycode = {
    getKeyCode : function(e) {
        var keycode = null;
        if(window.event) {
            keycode = window.event.keyCode;
        }else if(e) {
            keycode = e.which;
        }
        return keycode;
    },
    getKeyCodeValue : function(keyCode, shiftKey) {
        shiftKey = shiftKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.modifiedByShift[keyCode];
        }else {
            value = this.keyCodeMap[keyCode];
        }
        return value;
    },
    getValueByEvent : function(e) {
        return this.getKeyCodeValue(this.getKeyCode(e), e.shiftKey);
    },
    keyCodeMap : { // keep both 173 codes to support different keyboards
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"capslock", 27:"escape", 32:" ", 33:"pageup",
        34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"+", 44:"printscreen", 45:"-", 46:"delete", 47:"/",
        48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:";",
        61:"=", 65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l",
        77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z",
        96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9",
        106: "*", 107:"+", 109:"-", 110:".", 111: "/", 112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12", 144:"numlock", 145:"scrolllock", 173:"'", 186:";", 187:"=", 188:",", 189:"-", 190:".", 191:"/", 192:"'", 219:"[", 220:"\\", 221:"]", 222:"'"
    },
    modifiedByShift : {
        32: " ", 65:"A", 66:"B", 67:"C", 68:"D", 69:"E", 70:"F", 71:"G", 72:"H", 73:"I", 74:"J", 75:"K", 76:"L",
        77:"M", 78:"N", 79:"O", 80:"P", 81:"Q", 82:"R", 83:"S", 84:"T", 85:"U", 86:"V", 87:"W", 88:"X", 89:"Y", 90:"Z", 192:"~", 48:")", 49:"!", 50:"@", 51:"#", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 109:"_", 61:"+",
        219:"{", 221:"}", 220:"|", 59:":", 222:"\"", 186:":", 187:"+", 188:"<", 189:"_", 190:">", 191:"?"/*,
        96:"insert", 97:"end", 98:"down", 99:"pagedown", 100:"left", 102:"right", 103:"home", 104:"up", 105:"pageup"*/
    }
};


d_keycode = {
    d_getKeyCode : function(e) {
        var d_keycode = null;
        if(window.event) {
            d_keycode = window.event.keyCode;
        }else if(e) {
            d_keycode = e.which;
        }
        return d_keycode;
    },
    d_getKeyCodeValue : function(d_keyCode, shiftKey) {
        shiftKey = shiftKey || false;
        var value = null;
        if(shiftKey === true) {
            value = this.d_modifiedByShift[d_keyCode];
        }else {
            value = this.d_keyCodeMap[d_keyCode];
        }
        return value;
    },
    d_getValueByEvent : function(e) {
        return this.d_getKeyCodeValue(this.d_getKeyCode(e), e.shiftKey);
    },
    d_keyCodeMap : {
        8:"backspace", 9:"tab", 13:"return", 16:"shift", 17:"ctrl", 18:"alt", 19:"pausebreak", 20:"capslock", 27:"escape", 32:" ", 33:"pageup",
        34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 42: "*", 43:"}", 44:"printscreen", 45:"[", 46:"delete", 47:"z",
        48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 59:"s",
        61:"]", 65:"a", 66:"x", 67:"j", 68:"e", 69:".", 70:"u", 71:"i", 72:"d", 73:"c", 74:"h", 75:"t", 76:"n",
        77:"m", 78:"b", 79:"r", 80:"l", 81:"'", 82:"p", 83:"o", 84:"y", 85:"g", 86:"k", 87:",", 88:"q", 89:"f", 90:";",
        96:"0", 97:"1", 98:"2", 99:"3", 100:"4", 101:"5", 102:"6", 103:"7", 104:"8", 105:"9",
        106: "*", 107:"}", 109:"[", 110:"v", 111: "z",
        112:"f1", 113:"f2", 114:"f3", 115:"f4", 116:"f5", 117:"f6", 118:"f7", 119:"f8", 120:"f9", 121:"f10", 122:"f11", 123:"f12",
        144:"numlock", 145:"scrolllock", 186:"s", 187:"]", 188:"w", 189:"[", 190:"v", 191:"z", 192:"`", 219:"/", 220:"\\", 221:"=", 222:"-"
    },
    d_modifiedByShift : {
        32: " ", 65:"A", 66:"X", 67:"J", 68:"E", 69:">", 70:"U", 71:"I", 72:"D", 73:"C", 74:"H", 75:"T", 76:"N",
        77:"M", 78:"B", 79:"R", 80:"L", 81:"\"", 82:"P", 83:"O", 84:"Y", 85:"G", 86:"K", 87:"<", 88:"Q", 89:"F", 90:":", 192:"~", 48:")", 49:"!", 50:"@", 51:"#", 52:"$", 53:"%", 54:"^", 55:"&", 56:"*", 57:"(", 109:"{", 61:"}",
        219:"?", 221:"+", 220:"|", 59:"S", 222:"_", 186:"S", 187:"}", 188:"W", 189:"{", 190:"V", 191:"Z"/*,
        96:"insert", 97:"end", 98:"down", 99:"pagedown", 100:"left", 102:"right", 103:"home", 104:"up", 105:"pageup"*/
    }
};

