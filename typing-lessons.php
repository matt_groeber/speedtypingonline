<?php
ob_start(); 
$currPage = "typingLessons";
require_once 'core/init.php';
$pageURL = fullSiteURL() . $_SERVER['REQUEST_URI'];

	$userFullName = '';
	$userUsername = 'Typing Lessons';
	$user = new User();
	$userData = $user->data();
	$debugMode = $user->hasPermission('debug') || Config::get('constants/debugMode');
	$username = Input::get('user');
	$isTeachersStudent = false;
	$isRestrictedStudent = false;
	$userToProfile = null;
	$onPersonalProfile = false;
	
	if($unameProvided = ($username != '')){
		$userToProfile = new User($username);
		
		if($userToProfile->exists()) {
			if($userData->username == $userToProfile->data()->username){
				$onPersonalProfile = true;
				$userToProfile = $user;
				
				if($userToProfile->isStudent()){
					$isRestrictedStudent = true;
				}
			} else if ($user->isLoggedIn()) {
				$currPage = 'anotherUsersTypingLessons';
				$onPersonalProfile = false;
				
				// see if a teacher is viewing their students page
				$classroomObj = new Classroom($userData->id);
				$isTeachersStudent = $classroomObj->isUserTeacherOfStudent($userToProfile->data()->id);
				
				//$teacherData = $userData;
				//$userData = $userToProfile->data();
			}
			$userUsername = $userToProfile->data()->username;
			$userFullName = $user->fullName();
			if($userFullName != ''){$userFullName = $userFullName . '\'s ';}
		}
	} else {
		if($user->isLoggedIn()){
			Redirect::to('/user/' . $user->data()->username . '/lessons');
		}
	}
	$fontAwesomeCDN = true;
	$htmlTitle = "Typing Lessons, " . $username;
	$htmlDescription = "Classic and advanced lessons for learning how to touch type. Keep track of your typing skill progress and set target speed (WPM) and accuracy.";
	$cssFiles = "kbehgi";
	$fontPacks = "d";
	$noWidgets = true;
	$jQuery = true;
	$addContainer = true;
	require_once 'includes/overall/header.php';
	
	if(($userToProfile != null) && !$userToProfile->exists()){
		if(inMaintenanceMode()){
			echo '<h2>Site is under Maintenance</h2><h4>User profile pages will be available shortly.</h4><h4>Please continue to use the site as normal but be aware that test, lesson, and game high score data will not be saved.</h4><h4>Sorry for any inconvenience and thank you for your patience!</h4>';
		} else {
			echo '<h2>That user does not exist!</h2>';
		}
		exitPHPwithFooter();
	} 
	
	if($userToProfile == null){
		$retArr = getTutorData($userData);
	} else {
		$retArr = getTutorData($userToProfile->data());
	}
	
	$classicLessonsHTML = $retArr['classicLessonHTML'];
	$advancedLessonsHTML = $retArr['advancedLessonHTML'];
	$keyboardBasicsHTML = $retArr['keyboardBasicsHTML'];
	$lessons = $retArr['lessons'];
	$mostRecentLessonIndex = $retArr['lastLesson'];
	$worstLessonIndex = $retArr['worstLesson'];
	$nextLessonHTML = $retArr['nextLessonHTML'];
	$wpmTarget = $retArr['wpmTarget'];
	$accTarget = $retArr['accTarget'];
	$classicLessonCount = $retArr['classicLessonCount'];
	$classicPassedCount = $retArr['classicPassedCount'];
	$advancedLessonCount = $retArr['advancedLessonCount'];
	$advancedPassedCount = $retArr['advancedPassedCount'];
?>

<div <?php echo ($userToProfile != null) ? 'class="borderBottom"' : '';?>>
<?php
ob_end_flush();	
$noUnameClassPrint = ' class="noUsername"';

if($unameProvided){
	require_once 'includes/profileMenu.php';
	$noUnameClassPrint = '';
}

?>
</div>
</div> <!-- <div id="heading"> -->

<div id="pageMain" <?php echo $noUnameClassPrint;?>>
	<div id="genAdWrap">
		<?php echo getAd("lead_ATF", "typingLessons", $debugMode, $user->isPremium());?>
	</div>
	
	<?php 
		if(Session::exists('failed')){
			echo Session::flash('failed');
		} else {
			echo Session::flash('updated');
		}
		
		if($onPersonalProfile) {
			$userNameToPrint = $userFullName;
		} else if($unameProvided) {
			$userNameToPrint = $username . "'s ";
		} else if($unameProvided) {
			$userNameToPrint = "";
		}
	
	if(empty($lessons)){
		echo '<div class="orangeCTA long"><h3>No lesson data found!</h3>';
		echo '<h4><a href="/typing-tutor">CLICK HERE to go to the typing tutor page</a></h4></div>';
		exit();
	} else if(false){
		echo '<div class="orangeCTA long"><h3>You have not taken a typing lessons yet!</h3>';
		echo '<h4><a href="/typing-tutor">CLICK HERE to take your first typing lesson</a></h4></div>';
		exit();
		
	}
	
	if(($onPersonalProfile && !$isRestrictedStudent) || $isTeachersStudent){
		$targetWPMelemHTML = '<input type="text" id="targetWPMspinner" class="ui_spinner editValue" name="tl_wpm_target" value="' . $wpmTarget . '">';
		$targetAccElemHTML = '<input type="text" id="targetAccuracyspinner" class="ui_spinner editValue" name="tl_accuracy_target" value="' . $accTarget . '">';
	} else {
		$targetWPMelemHTML = '<span class="targetValue">' . $wpmTarget . '</span>';
		$targetAccElemHTML = '<span class="targetValue">' . $accTarget . '</span>';
	}
	
	// lesson targets
	$tutorTargetsHTML = '';
	$tutorTargetsHTML .= '<div id="targetContainer" class="field">';
	$tutorTargetsHTML .= '<h4>Targets</h4>';
	$tutorTargetsHTML .= '<span id="speedTargetContainer" class="subField">';
	$tutorTargetsHTML .= $targetWPMelemHTML;
	$tutorTargetsHTML .= '<span class="units">wpm</span>';
	$tutorTargetsHTML .= '</span>';
	$tutorTargetsHTML .= '<span class="subField">';
	$tutorTargetsHTML .= $targetAccElemHTML;
	$tutorTargetsHTML .= '<span class="units">%</span>';
	$tutorTargetsHTML .= '</span>';
	$tutorTargetsHTML .= '</div>';
	
	// lesson progress
	$lessonProgressHTML = "";
	if($userToProfile != null){
		$lessonProgressHTML .= '<span id="lessonProgressContainer" class="stoStatusBox">';
		
		if($user->isTeacher()){
			$lessonProgressHeaderHTML = '<h3><div>' . $userNameToPrint . '</div> Lesson Progress</h3>';
		} else {
			$lessonProgressHeaderHTML = '<h3>Lesson Progress</h3>';
		}
		
		$lessonProgressHTML .= '<div class="stoStatBoxTitle">' . $lessonProgressHeaderHTML . '</div>';
		$lessonProgressHTML .= '<div class="stoStatBoxBody">';
		$totalPassedCount = $classicPassedCount + $advancedPassedCount;
		$totalLessonCount = $classicLessonCount + $advancedLessonCount;
		$overallPercent = round(($totalPassedCount / $totalLessonCount) * 100, 0);
		$classicPercent = round(($classicPassedCount / $classicLessonCount) * 100, 0);
		$advancedPercent = round(($advancedPassedCount / $advancedLessonCount) * 100, 0);
		
		$lessonProgressHTML .= '<div class="responsiveCols">';
		$lessonProgressHTML .= 	'<div class="rc-1-2">';
		$lessonProgressHTML .= 		'<div>Classic</div>';
		$lessonProgressHTML .= 		'<div class="stoProgBar"><span id="classicProgBar" style="width:' . $classicPercent . '%;">' . $classicPercent . '% <span class="notBold">(' . $classicPassedCount . '/' . $classicLessonCount . ')</span></div>';
		$lessonProgressHTML .= 	'</div>';
		$lessonProgressHTML .= 	'<div class="rc-1-2 lastCol">';
		$lessonProgressHTML .= 		'<div>Advanced</div>';
		$lessonProgressHTML .= 		'<div class="stoProgBar dkBlue"><span id="advancedProgBar" style="width:' . $advancedPercent . '%;">' . $advancedPercent . '% <span class="notBold">(' . $advancedPassedCount . '/' . $advancedLessonCount . ')</span></div>';
		$lessonProgressHTML .= 	'</div>';
		$lessonProgressHTML .= '</div>';
		$lessonProgressHTML .= '<h4>Overall</h4>';
		$lessonProgressHTML .= '<div class="stoProgBar orange large"><span id="overallProgBar" style="width:' . $overallPercent . '%;">' . $overallPercent . '% <span class="notBold">(' . $totalPassedCount . '/' . $totalLessonCount . ')</span></span></div>';
		$lessonProgressHTML .= '</div>';
		$lessonProgressHTML .= $tutorTargetsHTML;
		$lessonProgressHTML .= '</span>';
	}
	
	echo '<div class="flex">';
	echo '<div class="flex-5">';
	
		
	
?>
<?php 
		$kbFirst = '';
		$kbLast = '';
		/*
		ini_set('xdebug.var_display_max_data', '8024');
			var_dump($stoTT_HTML);
		var_dump($keyboardBasicsHTML);
		exit();
		*/
		
		if($wpmTarget >= Config::get('constants/touchTypingAchieved')){
			$kbFirst ='<div id="tkbcContainer" style="display: none;"><div id="topKeyboardBasicsContainer"></div></div>';
			$kbLast = '<div id="bkbcContainer"><h2 id="kB_Header"><span>Keyboard Basics</span></h2><div id="bottomKeyboardBasicsContainer">' . $keyboardBasicsHTML . '</div></div>';
		} else {
			$kbFirst = '<div id="tkbcContainer"><div id="topKeyboardBasicsContainer">' . $keyboardBasicsHTML . '</div></div>';
			$kbLast ='<div id="bkbcContainer" style="display: none;"><h2 id="kB_Header"><span>Keyboard Basics</span></h2><div id="bottomKeyboardBasicsContainer"></div></div>';
		}
?>
			
			<?php echo $kbFirst; ?>
			<div>
				<h2 id="cL_Header"><span>Classic Lessons</span></h2>
				<div id="classicLessonContainer">
					<?php echo $classicLessonsHTML;?>
				</div>
			</div>
			
			<div id="genAdWrap_bottom">
				<?php echo getAd("lead_BTF", "typingLessons", $debugMode, $user->isPremium());?>
			</div>
			
			<h2 id="aL_Header"><span>Advanced Lessons</span></h2>
			<div class="grid grid-pad">
				<div id="advancedLessonContainer">
					<?php echo $advancedLessonsHTML; ?>
				</div>
			</div>
			<?php echo $kbLast; ?>

		</div><!-- col-2-3 -->
		<div class="flex-2">
<?php 		
		if($user->isTeacher()){
			echo '<div><h2 class="second_h1">Lesson Filter:</h2></div>'; 
		} else {
			echo '<div><h2 class="second_h1">' . $userNameToPrint . 'Typing Lessons</h2></div>'; 
		}
		
			echo '<div class="hudContainer">';
		if($user->isTeacher()){
?>
		
			<!--<h3>Timeframe:</h3>-->
			
			<div id="reportrange" class="stoUI stoUIClickable" style="cursor: pointer; width: 300px">
				<i class="fa fa-calendar"></i>&nbsp;
				<span></span> <i class="fa fa-caret-down"></i>
			</div>
			<?php
		}
			
			echo	$lessonProgressHTML;
			
			echo '<div id="topRow" class="mini">';
		
				if($user->isLoggedIn()){
					
					// Practice Again
					if($worstLessonIndex >= 0 && ($onPersonalProfile || $isTeachersStudent)){
						echo '<span id="worstLessonContainer" class="fluidColumn">';
						echo '<div><h2>Practice Again</h2></div>';
						$retIndex = ($lessons[$worstLessonIndex]->type == "advanced") ? 1 : 0;
						echo '<div id="worstLessonHTML">';
						echo getLessonHTMLfromArray(array($lessons[$worstLessonIndex]), $wpmTarget, $accTarget)[$retIndex];
						echo '</div>';
						echo '</span>';
					}
					
					// Next Lesson
					if($nextLessonHTML != '' && ($onPersonalProfile || $isTeachersStudent)){
						echo '<span id="nextLessonContainer" class="fluidColumn stoAttention">';
						echo '<div class="stoAttentionTitle"><h2>Next Lesson</h2></div>';
						echo '<div id="nextLessonHTML">';
						echo $nextLessonHTML;
						echo '</div>';
						echo '</span>';
					}
				}
				
				// Last Lesson
				if($mostRecentLessonIndex >= 0){
					echo '<span id="lastLessonContainer" class="fluidColumn">';
					echo '<div><h2>Last Lesson</h2></div>';
					$retIndex = ($lessons[$mostRecentLessonIndex]->type == "advanced") ? 1 : 0;
					echo '<div id="lastLessonHTML">';
					echo getLessonHTMLfromArray(array($lessons[$mostRecentLessonIndex]), $wpmTarget, $accTarget)[$retIndex];
					echo '</div>';
					echo '</span>';
				}
				
			echo '</div>';
			
			echo 	getAd("generalAsideAd", "typingLessons", $debugMode, $user->isPremium());;
			?>
		</div><!-- hudContainer -->
	</div><!-- col-1-3 -->
	</div><!-- grid grid-pad -->
	

<script src="/js/general.js?v=5"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

<?php 
if(($onPersonalProfile && !$isRestrictedStudent) || $isTeachersStudent){
?>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
	var startDate;
	var endDate;
	var start = <?php echo GetJSMoment($userData->ol_date_filter)[0]; ?>;
	var end = <?php echo GetJSMoment($userData->ol_date_filter)[1]; ?>;
	$(document).ready(function() {
		$('.ui-spinner-button').on('click', function(event) {
			//showUpdateButton();
		});
		var currDateTime = new Date(<?php echo '"' . gmdate('Y-m-d H:i:s') . '"';?>.replace(/-/g,'/'));
		//doReadableTimeDifference(currDateTime, "makeDateReadable", true, true);
		
		function cb(start, end) {
			//$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			
			var myLabel = $('#reportrange').data('daterangepicker').chosenLabel;
			if(myLabel != undefined){
				labelHTML = HTMLelemStr($('#reportrange').data('daterangepicker').chosenLabel, 'div', '', 'dateRangeLabel');
			} else {
				labelHTML = HTMLelemStr(GetPickerLabelFromDates(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD')), 'div', '', 'dateRangeLabel');
			}
			
			calendarIconHTML = HTMLelemStr('<i class="fa fa-filter" aria-hidden="true"></i>', 'div', '', 'iconContainer')
			textContainerHTML = HTMLelemStr(labelHTML + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY') + '<i class="fa fa-caret-down"></i>', 'div', '', 'textContainer')
						
			$('#reportrange').html(calendarIconHTML + textContainerHTML);
			
			startDate = start.format('YYYY-MM-DD');
			endDate = end.format('YYYY-MM-DD');
			getLessonData();
		}

		$('#reportrange').daterangepicker({
			startDate: start,
			endDate: end,
			opens: 'left',
			applyButtonClasses: "stoUI stoUIClickable solidColor whiteTxt",
			cancelButtonClasses: "stoUI stoUIClickable ",
			ranges: {
			   'Today': [moment(), moment()],
			   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			   'This Week': [moment().startOf('week'), moment().endOf('week')],
			   'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
			   'This Month': [moment().startOf('month'), moment().endOf('month')],
			   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
			   'All Time': [moment('2016-08-01'), moment()]
			}
		}, cb);

		cb(start, end);
		
		
		startDate = start.format('YYYY-MM-DD');
		endDate = end.format('YYYY-MM-DD');
	});
	

	$( "#targetWPMspinner").spinner({
		mouseWheel: true,
		numberFormat: "n0",
	  spin: function( event, ui ) {
		if ( ui.value > 255 ) {
		  $( this ).spinner( "value", 255 );
		  return false;
		} else if ( ui.value < 0 ) {
		  $( this ).spinner( "value", 0 );
		  return false;
		}
	  }
	});

	$("#targetWPMspinner").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false;}
	});

	$("#targetAccuracyspinner").keypress(function (e) {
		//if the letter is not digit then don't type anything
		if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) { return false;}
	});

	$( "#targetAccuracyspinner").spinner({
		step: 0.1,
		numberFormat: "n1",
		mouseWheel: true,
	  spin: function( event, ui ) {
		if ( ui.value > 100 ) {
		  $( this ).spinner( "value", 100 );
		  return false;
		} else if ( ui.value < 0 ) {
		  $( this ).spinner( "value", 0 );
		  return false;
		}
	  }
	});
	
	$("#targetWPMspinner").attr({"max" : 250, "min" : 1, "var" : "targetWPM", "display_id" : "tl_wpm_target" });
	$("#targetAccuracyspinner").attr({"max" : 100, "min" : 0.1, "var" : "targetAccuracy", "display_id" : "tl_accuracy_target" });
	
	$('#lessonProgressContainer, .daterangepicker').on("mouseleave", function(){
			var elem = $(this).parent();
			var targetField = elem.find('.targetField');
			var editValue = elem.find('.editValue');
			
			var targetVals = editValue.map(function(){
				//var temp = [["id" : this.id, "val" : this.value, "max" : this.max, "min" : this.min]]; 
				var temp =[[]];
				temp[0]['id'] = this.id;
				temp[0]['val'] = this.value;
				temp[0]['max'] = this.max;
				temp[0]['min'] = this.min;
				temp[0]['var'] = this.attributes.var.value;
				temp[0]['display_id'] = this.attributes.display_id.value;
				return temp;
			});
			
			var postFormatValue;
			var colArr = [];
			var valArr = [];
			
			for(var ii = 0; ii < targetVals.length; ii++){
				postFormatValue = (Math.floor(targetVals[ii]['val'] * 10)) / 10;
				
				if(postFormatValue > targetVals[ii]['max']){
					postFormatValue = targetVals[ii]['max'];
				} else if(postFormatValue < targetVals[ii]['min']){
					postFormatValue = targetVals[ii]['min'];
				}
				
				$('#' + targetVals[ii]['id']).val(postFormatValue);
				
				elem = $('#' + targetVals[ii]['display_id']);
				elemHTML = elem.html();
				
				if(elemHTML != postFormatValue && !isNaN(postFormatValue)){
					elem.html(postFormatValue);
					colArr.push(targetVals[ii]['display_id']);
					valArr.push(postFormatValue);
				} else {
					//editValue.val(targetField[0].innerHTML);	// set back just in case invalid input from user
				}
			}		
			
			$.post('/setData.php', { type: "us", col: colArr, val: valArr, its:<?php echo $userToProfile->data()->id; ?>},
				function(output){
					getLessonData();
			});
	});
	
	function getLessonData(){
		$.post('/getData.php', { type: "tl", its:<?php echo $userToProfile->data()->id; ?>, sd: startDate, ed: moment(endDate).add(1, 'days').format('YYYY-MM-DD')},function(output){
			myJSONdata = $.parseJSON(output);
			var currDateTime = new Date(myJSONdata["currDateTime"].replace(/-/g,'/'));
			$('#classicLessonContainer').html(myJSONdata["loggedInResults"].classicLessonHTML);
			$('#advancedLessonContainer').html(myJSONdata["loggedInResults"].advancedLessonHTML);
			
			if($('#targetWPMspinner').val() >= <?php echo Config::get('constants/touchTypingAchieved'); ?>){
				$('#tkbcContainer').hide();
				$('#bkbcContainer').show();
				$('#topKeyboardBasicsContainer').html("");
				$('#bottomKeyboardBasicsContainer').html(myJSONdata["loggedInResults"].keyboardBasicsHTML);
			} else {
				$('#tkbcContainer').show();
				$('#bkbcContainer').hide();
				$('#topKeyboardBasicsContainer').html(myJSONdata["loggedInResults"].keyboardBasicsHTML);
				$('#bottomKeyboardBasicsContainer').html("");
			}
			
			$('#nextLessonHTML').html(myJSONdata["loggedInResults"].nextLessonHTML);
			
			if(myJSONdata["loggedInResults"].worstLesson >= 0){
				$('#worstLessonContainer').show();
				$('#worstLessonHTML').html(myJSONdata["loggedInResults"].worstLessonHTML);
			} else {
				$('#worstLessonContainer').hide();
				$('#worstLessonHTML').html("");
			}
			
			if(myJSONdata["loggedInResults"].lastLesson >= 0){
				$('#lastLessonContainer').show();
				$('#lastLessonHTML').html(myJSONdata["loggedInResults"].lastLessonHTML);
			} else {
				$('#lastLessonContainer').hide();
				$('#lastLessonHTML').html("");
			}
			
			classicPassedCount = myJSONdata["loggedInResults"].classicPassedCount;
			classicLessonCount = myJSONdata["loggedInResults"].classicLessonCount;
			advancedPassedCount = myJSONdata["loggedInResults"].advancedPassedCount;
			advancedLessonCount = myJSONdata["loggedInResults"].advancedLessonCount;
			totalPassedCount = classicPassedCount + advancedPassedCount;
			totalLessonCount = classicLessonCount + advancedLessonCount;
			
			classicPercent = Math.round((classicPassedCount / classicLessonCount) * 100);
			advancedPercent = Math.round((advancedPassedCount / advancedLessonCount) * 100);
			overallPercent = Math.round((totalPassedCount / totalLessonCount) * 100);

			var cpbElem = $('#classicProgBar');
			var apbElem = $('#advancedProgBar');
			var opbElem = $('#overallProgBar');
			
			cpbElem.css("width", classicPercent + "%");
			cpbElem.html(classicPercent + '% <span class="notBold">(' + classicPassedCount + '/' + classicLessonCount + ')');
			apbElem.css("width", advancedPercent + "%");
			apbElem.html(advancedPercent + '% <span class="notBold">(' + advancedPassedCount + '/' + advancedLessonCount + ')');
			opbElem.css("width", overallPercent + "%");
			opbElem.html(overallPercent + '% <span class="notBold">(' + totalPassedCount + '/' + totalLessonCount + ')');
			
			
			doReadableTimeDifference(currDateTime, "makeDateReadable", true, true);
		});
	}
	
	$('#reportrange').on('apply.daterangepicker', function(ev, picker){
		startDate = picker.startDate.format('YYYY-MM-DD');
		endDate = picker.endDate.format('YYYY-MM-DD');
		$.post('/setData.php', { type: "us", col: 'ol_date_filter', val: GetTLdateFilterEnum(picker.chosenLabel)},
			function(output){
		});
	});
<?php
} else {
?>
<script>
	var currDateTime = new Date(<?php echo json_encode(gmdate('Y-m-d H:i:s')); ?>);
	$(document).ready(function() {
		doReadableTimeDifference(currDateTime, "makeDateReadable", true, true);
	});
<?php
}
?>
</script>
<?php
require_once 'includes/overall/footer.php';
?>