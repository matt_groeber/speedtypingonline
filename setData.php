<?php
require_once 'core/init.php';

$currentUser = new User();

/*$_POST['type'] = 'us';
$_POST['its'] = 209491;
$_POST['lesson_mod'] = 18;*/

/*$_POST['type'] = 'tta';
$_POST['alpha_type'] = "NORMAL";
$_POST['msecs'] = 1111;*/

/*$_POST['type'] = 'fft';
$_POST['fft_Type'] = "att";
$_POST['score'] = 999;*/

/*$_POST['type'] = "tt";
$_POST['wpm'] = "23";
$_POST['accuracy'] = "95.5";
$_POST['total_entries'] = "23";
$_POST['incorrect_entries'] = "23";
$_POST['fixed_mistakes'] = "23";
$_POST['total_time_secs'] = "60";
$_POST['total_time_secs'] = "60";
$_POST['text_type'] = "FACTS";

$_POST['letter_mistakes'] = '{"T":1,"o":5,"K":1,"i":7}';
$_POST['letter_counts'] = '{"M":1,"c":6,"T":7,"o":9,"K":1,"i":16,"a":4}';
$_POST['type'] = 'us';
$_POST['set'] = 'auto_track_lesson_targets';
$_POST['val'] = '25';*/

/*$_POST['type'] = 'lh';
$_POST['lesson_mod'] = 3;
$_POST['accuracy'] = 90.5;
$_POST['wpm'] = 99;
$_POST['total_time_secs'] = 5;
$_POST['total_entries'] = 12;
$_POST['incorrect_entries'] = 2;
$_POST['fixed_mistakes'] = 4;
*/

/*$_POST['type'] = 'us';
$_POST['col'] = array('tl_wpm_target', 'tl_accuracy_target');
$_POST['val'] = array(25, 53);*/

/*$_POST['type'] = 'ol_astc';
$_POST['clscd'] = '26ff38';*/

/*$_POST['type'] = 'ol_scaj';
$_POST['ccode'] = 'f58959';*/

/*$_POST['type'] = 'ol_sa';
$_POST['ssa'] = array(209491, 209492);*/

/*$_POST['type'] = 'ol_da';
$_POST['aid'] = '143';*/

/*$_POST['type'] = 'ol_rsfc';
$_POST['aos'] = array('209493', '209492');
$_POST['cid'] = 11;*/

/*$_POST['type'] = "ol_aodstc";
$_POST['sid'] = 209492;
$_POST['cid'] = 3;
$_POST['addstud'] = false;*/

/*var_dump(date('Y-m-d H:i:s', strtotime("2020-07-09 07:06:00")));
exit();*/

/*$_POST['type'] = 'ol_ac';
$_POST['ncn'] = 'First Class';*/


/*$classroomObj = new Classroom($currentUser);
$db_obj = $classroomObj->getStudentEmailList(array(209492, 209494));
var_dump($db_obj);
exit();*/

//$_POST['type'] = 'csub';
//$_POST['type'] = 'asubp';
//$_POST['type'] = 'cns';
//$_POST['type'] = 'rasub';

if(!isset($_POST['type'])){
	$_POST['type'] = '';
}


if($currentUser->isLoggedIn()){
	
	$user_id = $currentUser->data()->id;
	$db = DB::getInstance();
	
	if($_POST['type'] == "tt"){
		
		// first delete superfluous typing tests if not premium user
		if(!$currentUser->isPremium()) {
			$dbResult = $db->get('typing_tests', array('user_id', '=',$user_id), 'id, timestamp', array('timestamp','DESC'));
			$theResults = $dbResult->results();
			$maxTtEntries = Config::get('constants/max_tt_entries');

			// see if at limit of entries
			if(sizeof($theResults) >= $maxTtEntries){
				
				$IDsToDelete = array();
				
				for($ii = ($maxTtEntries - 1); $ii <= sizeof($theResults); $ii++){
					array_push($IDsToDelete, $theResults[$ii]->id);
				}
				
				$db->delete('typing_tests', array('id', 'IN', $IDsToDelete));

			}
		}
		
		$dbResult = $db->insert('typing_tests', array('user_id' => $user_id, 'text_index' => $_POST['text_ind'], 'keyboard_used' => $_POST['keyboard'], 'wpm' => $_POST['wpm'], 'accuracy' => $_POST['accuracy'], 'timestamp' => gmdate('Y-m-d H:i:s'), 'total_entries' => $_POST['total_entries'], 'incorrect_entries' => $_POST['incorrect_entries'], 'fixed_mistakes' => $_POST['fixed_mistakes'], 'total_time_secs' => $_POST['total_time_secs']));
		
		$testID = $db->getLastInsertID();
		
		$fieldArr = array('test_id', 'the_char', 'total_count', 'incorrect_count', 'response_time');
		$postLetterCounts = json_decode($_POST['letter_counts']);
		$postIncorrectCounts = json_decode($_POST['letter_mistakes']);
		$postResponseTimes = json_decode($_POST['response_times']);
		
		$aAscii = ord("a");
		$zAscii = ord("z");
		$commaAscii = ord(",");
		$periodAscii = ord(".");
		$apostAscii = ord("'");
		$quoteAscii = ord("\"");
		$questAscii = ord("?");
		$exclaimAscii = ord("!");
		
		foreach($postLetterCounts as $key => $val){
			$keyAscii = ord(strtolower($key));
			
			if( ($keyAscii >= $aAscii && $keyAscii <= $zAscii) || $keyAscii == $commaAscii || $keyAscii == $periodAscii 
				|| $keyAscii == $apostAscii || $keyAscii == $quoteAscii	|| $keyAscii == $questAscii || $keyAscii == $exclaimAscii ){
				
				$testIdArr[] = $testID;
				$charArr[] = $key;
				$totalCountArr[] = $val;
				$responseTimesArr[] = $postResponseTimes->$key;
				
				if(isset($postIncorrectCounts->$key)){
					$incorrectCountArr[] = $postIncorrectCounts->$key;
				} else {
					$incorrectCountArr[] = 0;
				}
			}
		}
		
		$dbResult = $db->insert('tt_char_counts', array($fieldArr, $testIdArr, $charArr, $totalCountArr, $incorrectCountArr, $responseTimesArr));
		
		// see if we need to update assignments
		$classroomObj = new Classroom($user_id);
		$assignmentUpdateResult = $classroomObj->UpdateAssignmentProgress($_POST['text_type'], $_POST['wpm'], $_POST['accuracy'], $_POST['total_time_secs'], 'Test', $_POST['the_sa_id']);
		
		$db_obj = $db->get('users_achieve', array('user_id', '=', $user_id), 'num_tests', null, null, array(1,0));
		$numTests = $db_obj->results()[0]->num_tests + 1;
		$dbResult = $db->update('users_achieve', $user_id, array('num_tests' => $numTests), 'user_id');
		
	} else if($_POST['type'] == "lh"){
		
		$dbResult = $db->get('lesson_history', array('user_id', '=',$user_id,'lesson_module', '=',$_POST['lesson_mod']), 'id, timestamp, lesson_count, lesson_timespent', array('timestamp','DESC'));
		$theResults = $dbResult->results();
		$lessonCount = 1;
		$lessonTimeSpent = $_POST['total_time_secs'];
		
		// first delete superfluous lesson history if not premium user
		if(!$currentUser->isPremium()) {
			
			$maxTtEntries = Config::get('constants/max_lh_entries');

			// see if at limit of entries
			if(sizeof($theResults) >= $maxTtEntries){
				
				$IDsToDelete = array();
				
				for($ii = ($maxTtEntries - 1); $ii <= sizeof($theResults); $ii++){
					array_push($IDsToDelete, $theResults[$ii]->id);
				}
				
				$db->delete('lesson_history', array('id', 'IN', $IDsToDelete));
			}
		}
		
		if(sizeof($theResults) >= 1){
			$lessonCount = $theResults[0]->lesson_count+1;
			$lessonTimeSpent += $theResults[0]->lesson_timespent;
		} 
		/*
		var_dump($lessonCount);
		exit();*/
		
		if(isset($_POST['wpm']) && isset($_POST['total_time_secs'])){
			$dbResult = $db->insert('lesson_history', array('user_id' => $user_id, 'lesson_module' => $_POST['lesson_mod'], 'wpm' => $_POST['wpm'], 'accuracy' => $_POST['accuracy'], 'timestamp' => gmdate('Y-m-d H:i:s'), 'total_entries' => $_POST['total_entries'], 'incorrect_entries' => $_POST['incorrect_entries'], 'fixed_mistakes' => $_POST['fixed_mistakes'], 'lesson_length' => $_POST['total_time_secs'], 'lesson_count' => $lessonCount, 'lesson_timespent' => $lessonTimeSpent));
		} else {
			$dbResult = $db->insert('lesson_history', array('user_id' => $user_id, 'lesson_module' => $_POST['lesson_mod'], 'timestamp' => gmdate('Y-m-d H:i:s'), 'lesson_count' => $lessonCount/*, 'lesson_timespent' => $lessonTimeSpent*/));
		}
		
		$db_obj = $db->get('users_achieve', array('user_id', '=', $user_id), 'num_lessons', null, null, array(1,0));
		$numTests = $db_obj->results()[0]->num_lessons + 1;
		$dbResult = $db->update('users_achieve', $user_id, array('num_lessons' => $numTests), 'user_id');

		// see if we need to update assignments
		$classroomObj = new Classroom($user_id);
		$assignmentUpdateResult = $classroomObj->UpdateAssignmentProgress($_POST['lesson_mod'], $_POST['wpm'], $_POST['accuracy'], $_POST['total_time_secs'], 'Lesson', $_POST['the_sa_id']);
		//var_dump($assignmentUpdateResult);
		
		
	} else if($_POST['type'] == 'tta') {
		
		$alphaType = Config::get('constants/alphaTypeEnum')[$_POST['alpha_type']];
		$highScoreObj = new HighScore();
		
		$highScoreObj->SetAlphaHighScore($currentUser, $_POST['msecs'], $alphaType);
		/*$doInsertion = false;
		//error_log(" +++++++++++  GOT HERE  ++++++++++++++++");
		// see if user cheated
		if($_POST['msecs'] < 500000){
			$currentUser->Cheated();
		}
		
		if($currentUser->isPremium()){
			$doInsertion = true;
		} else {
			$dbResult = $db->get('alpha_HS', array('user_id', '=',$user_id, 'type', '=', $alphaType), 'id, total_time, timestamp', array('total_time','ASC'));
			$theResults = $dbResult->results();
			$maxAlphaEntries = Config::get('constants/max_tta_entries');

			// see if at limit of entries
			if(sizeof($theResults) >= $maxAlphaEntries){
				$lastResult = $theResults[$maxAlphaEntries - 1];
				
				// we want to delete all entries more than the limit
				if(sizeof($theResults) > $maxAlphaEntries){
					$IDsToDelete = array();
					
					for($ii = $maxAlphaEntries; $ii < sizeof($theResults); $ii++){
						array_push($IDsToDelete, $theResults[$ii]->id);
					}
					
					$db->delete('alpha_HS', array('id', 'IN', $IDsToDelete));
				}
				
				// replace last entry if older than 24 hours or if new score is better/faster
				if( ($lastResult->timestamp < date('Y-m-d H:i:s', strtotime('-24 hours'))) || ($lastResult->total_time > $_POST['msecs']) ){
					$db->delete('alpha_HS', array('id', '=', $lastResult->id));
					$doInsertion = true;
				}
			} else {
				$doInsertion = true;
			}
		}
			
		if($doInsertion){
			$result = $db->insert('alpha_HS', array('user_id' => $user_id, 'total_time' => $_POST['msecs'], 'type' => $alphaType, 'timestamp' => gmdate('Y-m-d H:i:s')));
		}*/
		
	} else if($_POST['type'] == 'fft') {
		$fftType = $_POST['fft_Type'];
		$doInsertion = false;
		
		if($currentUser->isPremium()){
			$doInsertion = true;
		} else {
			$dbResult = $db->get('fft_HS', array('user_id', '=',$user_id, 'type', '=', $fftType), 'id, score, timestamp', array('score','DESC'));
			$theResults = $dbResult->results();
			$maxFftEntries = Config::get('constants/max_fft_entries');
			
			// see if at limit of entries
			if(sizeof($theResults) >= $maxFftEntries){
				$lastResult = $theResults[$maxFftEntries - 1];

				// we want to delete all entries more than the limit with the worse score
				if(sizeof($theResults) > $maxFftEntries){
					$IDsToDelete = array();
					
					for($ii = $maxFftEntries; $ii < sizeof($theResults); $ii++){
						array_push($IDsToDelete, $theResults[$ii]->id);
					}

					$db->delete('fft_HS', array('id', 'IN', $IDsToDelete));
				}

				// replace last entry if older than 24 hours or if new score is better/faster
				if( ($lastResult->timestamp < date('Y-m-d H:i:s', strtotime('-24 hours'))) || ($lastResult->score <= $_POST['score']) ){
					$db->delete('fft_HS', array('id', '=', $lastResult->id));
					$doInsertion = true;
				}
			} else {
				$doInsertion = true;
			}
		}
		
		if($doInsertion){
			$result = $db->insert('fft_HS', array('user_id' => $user_id, 'score' => $_POST['score'], 'num_words' => $_POST['num_words'], 'type' => $fftType, 'timestamp' => gmdate('Y-m-d H:i:s')));
		}
		
	} else if($_POST['type'] == 'us'){
		$fieldArr = array();
		
		if(is_array($_POST['col'])){
			for($ii = 0; $ii < sizeof($_POST['col']); $ii++){
				$fieldArr = array_merge($fieldArr, array($_POST['col'][$ii] => $_POST['val'][$ii]));
			}
		} else {
			$fieldArr = array($_POST['col'] => $_POST['val']);
		}
		//var_dump($fieldArr);
		 
		// see if a teacher is viewing their students page
		$classroomObj = new Classroom($user_id);
		$studentArr;
		if(isset($_POST['its'])){
			$studentUserId = $_POST['its'];
			$studentArr = $classroomObj->isUserTeacherOfStudent($studentUserId);
		}
		
		if(!empty($studentArr)){
			// teacher is editing their student's targets
			$user_id = $studentUserId;
		}
		if($currentUser->isStudent()) {
			// for now restrict students from editing their targets
			unset($fieldArr['tl_wpm_target']);
			unset($fieldArr['tl_accuracy_target']);
		}

		$dbResult = $db->update('user_settings', $user_id, $fieldArr, 'user_id');
	} else if($_POST['type'] == 'ol_astc'){
		if($currentUser->isStudent()){
			
			$classroomObj = new Classroom($currentUser);
			$results = $classroomObj->addStudentToClass($user_id, $_POST['clscd']);
			echo $results;
		} else {
			echo 'User is not a student';
		}
		
	} else if($_POST['type'] == 'ol_scaj'){
		if($currentUser->isTeacher()){
			
			$classroomObj = new Classroom($currentUser);
			$results = $classroomObj->setClassroomJoinMethod($_POST['ccode'], $_POST['setting']);
			echo $results;
		}
	} else if($_POST['type'] == 'ol_sa'){	// set assignment
	
		if(isset($_POST['tit'])){
			$postTitle = htmlentities($_POST['tit'], ENT_QUOTES, 'UTF-8');
		}
		
		if($currentUser->isTeacher()){
			$fieldArr = array('assign_id', 'stud_id', 'due_date');
			$selectedStudentsArr = $_POST['ssa'];
			$currTime = gmdate('Y-m-d H:i:s');
			$dueDate = $_POST['add'];
			$classroomObj = new Classroom($currentUser);
			
			$postASV = $_POST['asv'];
			if($_POST['atp'] != 1 && $_POST['atp'] != 2){
				$postASV *= 60;
			}
			
			if(isset($_POST['mode']) && $_POST['mode'] == 'save'){
				$isAssigned = 0;
			} else {
				$isAssigned = 1;
			}
			
			//var_dump($isAssigned);
			
			$dbResult = $db->get('OL_assignments', array('teach_id', '=',$user_id, 'id', '=', $_POST['aid']), 'id, is_assigned, selection, goal, type, notes, due_date', null, array('OL_assignees','id','assign_id'));
			$assignmentResults = $dbResult->results();
			
			if($assignmentResults){
				
				// if assignment went assigned to/from unassigned
				if($assignmentResults[0]->is_assigned == '1' && $isAssigned == 0){
					$wasUnassigned = true;
					$wasAssigned = false;
				} else if($assignmentResults[0]->is_assigned == '0' && $isAssigned == 1){
					$wasUnassigned = false;
					$wasAssigned = true;
				} else {
					$wasUnassigned = false;
					$wasAssigned = false;
				}
				
				$ar = $assignmentResults[0];
				if($ar->type != $_POST['atp'] || $ar->selection != $_POST['asp'] 
					|| $ar->goal != $postASV || $ar->notes != $_POST['note'] || $ar->due_date != $dueDate ){
					$sigChange = true;
				} else {
					$sigChange = false;
				}
				
				//var_dump($_POST);
				
				$db->update('OL_assignments', $_POST['aid'], array('title' => $postTitle, 'notes' => $_POST['note'], 'type' => $_POST['atp'], 'selection' => $_POST['asp'], 'goal' => $postASV, 'is_assigned' => $isAssigned), 'id');
				
				$studentMatchArr = array('assign_id', '=', $_POST['aid']/*, 'stud_id', 'IN', $selectedStudentsArr*/);
				$db_obj = $db->get('OL_assignees', $studentMatchArr, 'stud_id');
				$assignees = $db_obj->results();
				
				$assigneesArr = array();
				for($ii = 0; $ii < count($assignees); $ii++){
					array_push($assigneesArr, $assignees[$ii]->stud_id);
				}
				
				$studentsToAdd = array();
				$studentsAlreadyThere = array();
				for($ii = 0; $ii < count($selectedStudentsArr); $ii++){
					if(in_array($selectedStudentsArr[$ii], $assigneesArr)){
						array_push($studentsAlreadyThere, $selectedStudentsArr[$ii]);
					} else {
						array_push($studentsToAdd, $selectedStudentsArr[$ii]);
					}
				}
				
				$studentsToDelete = array();
				for($ii = 0; $ii < count($assigneesArr); $ii++){
					if(in_array($assigneesArr[$ii], $selectedStudentsArr)){
						
					} else {
						array_push($studentsToDelete, $assigneesArr[$ii]);
					}
				}
				
				// need to update selected students with possible new due date				
				$dbResult = $db->update('OL_assignees', $_POST['aid'], array('due_date' => $dueDate), 'assign_id');
				
				// need to insert newly selected students
				if(!empty($studentsToAdd)){
					for($ii = 0; $ii < sizeof($studentsToAdd); $ii++){
						$assignmentIdArr[] = $_POST['aid'];
						$dueDateArr[] = $dueDate;
					}
					
					$dbResult = $db->insert('OL_assignees', array($fieldArr, $assignmentIdArr, $studentsToAdd, $dueDateArr));
					
					if($isAssigned == 1)
						$retVal = $classroomObj->EmailNewAssignment($studentsToAdd, $_POST['aid'], $postTitle, $_POST['atp'], $_POST['ddwf'], $_POST['asp'], $_POST['note'], 'new');
				}
				
				// delete deselected students
				if(!empty($studentsToDelete)){
					$db->delete('OL_assignees', array('assign_id', '=', $_POST['aid'], 'stud_id', 'IN', $studentsToDelete));
					echo 'in studentsToDelete';
					if($isAssigned == 1 || $wasUnassigned)
						$retVal = $classroomObj->EmailNewAssignment($studentsToDelete, $_POST['aid'], $postTitle, $_POST['atp'], $_POST['ddwf'], $_POST['asp'], $_POST['note'], 'delete');
				}
				
				// WE SHOULD FIRST SEE IF ASSIGNMENT REALLY CHANGED
				if(!empty($studentsAlreadyThere)){
					echo 'in studentsAlreadyThere';
					if ($wasAssigned){
						$retVal = $classroomObj->EmailNewAssignment($studentsAlreadyThere, $_POST['aid'], $postTitle, $_POST['atp'], $_POST['ddwf'], $_POST['asp'], $_POST['note'], 'new');
					} else if ($wasUnassigned){
						$retVal = $classroomObj->EmailNewAssignment($studentsAlreadyThere, $_POST['aid'], $postTitle, $_POST['atp'], $_POST['ddwf'], $_POST['asp'], $_POST['note'], 'delete');
					} else if($isAssigned == 1 && $sigChange) {
						$retVal = $classroomObj->EmailNewAssignment($studentsAlreadyThere, $_POST['aid'], $postTitle, $_POST['atp'], $_POST['ddwf'], $_POST['asp'], $_POST['note'], 'edit');
					} 
				}
				
			} else {
				$result = $db->insert('OL_assignments', array('type' => $_POST['atp'],'assign_date' => $currTime, 'teach_id' => $currentUser->data()->id, 'title' => $postTitle, 'notes' => $_POST['note'], 'min_speed' => 2,'selection' => $_POST['asp'], 'goal' => $postASV, 'is_assigned' => $isAssigned));
				if($result){
					$db_obj = $db->get('OL_assignments', array('teach_id', '=', $currentUser->data()->id,'assign_date', '=', $currTime), 'id');
					$assignmentId = $db_obj->results()[0]->id;
					
					for($ii = 0; $ii < sizeof($selectedStudentsArr); $ii++){
						$assignmentIdArr[] = $assignmentId;
						$dueDateArr[] = $dueDate;
					}
					
					$dbResult = $db->insert('OL_assignees', array($fieldArr, $assignmentIdArr, $selectedStudentsArr, $dueDateArr));
					
					if($isAssigned == 1){
						$retVal = $classroomObj->EmailNewAssignment($selectedStudentsArr, $assignmentId, $postTitle, $_POST['atp'], $_POST['ddwf'], $_POST['asp'], $_POST['note'], 'new');
					}
					
				}
			}
		}
	} else if($_POST['type'] == 'ol_da'){	// delete assignment
		$db_obj = $db->get('OL_assignments', array('id', '=', $_POST['aid']), 'teach_id');
		$assignTeachID = $db_obj->results()[0]->teach_id;
		
		// make sure teacher actually owns assignment before deleting
		if($currentUser->isTeacher() && $currentUser->data()->id == $assignTeachID){
			$result = $db->delete('OL_assignments', array('id', '=', $_POST['aid']));
			//var_dump($result);
		}
	} else if($_POST['type'] == 'ol_ac'){	// add classroom

		if($currentUser->isTeacher()){
			$classroomObj = new Classroom($currentUser);
			
			if(isset($_POST['ncn']) && $_POST['ncn'] != ''){
				$result = $classroomObj->CreateNewClassroom($_POST['ncn']);
				//echo 'new class name is ' . $result;
				if($result === true){
					echo 'New class was added';
				} else {
					echo $result;
				}
			} else {
				echo 'Provide a class name';
			}
		} else {
			echo 'You must be a teacher to do that';
		}
	} else if($_POST['type'] == 'ol_dc'){	// delete classroom
		$db_obj = $db->get('OL_classrooms', array('id', '=', $_POST['cid']), 'teach_id');
		$classroomTeachID = $db_obj->results()[0]->teach_id;
		
		// make sure teacher actually owns assignment before deleting
		if($currentUser->isTeacher() && $currentUser->data()->id == $classroomTeachID){
			$result = $db->delete('OL_classrooms', array('id', '=', $_POST['cid']));
		}
	} else if($_POST['type'] == 'ol_rcc'){	// reset class code
		if($currentUser->isTeacher()){
			$classroomObj = new Classroom($currentUser);
			echo $classroomObj->generateCCode($_POST['cid']);
		}
	} else if($_POST['type'] == 'cns'){	// create new subscription
		//echo GetAccessToken();
		header('Access-Control-Allow-Origin: *');
		
		$subObj = new Subscription();
		
		$approvalLink = $subObj->CreateSubscription("test@email.com", $currentUser->data()->email);
		//$approvalLink = CreateSubscription("purchaseEmail@email.com", $currentUser->data()->email);
		//var_dump($approvalLink);
		Redirect::to($approvalLink);
	} else if($_POST['type'] == 'spp'){	// set to pending premium
		//echo GetAccessToken();
		error_log("---------------------------------------------");
		error_log("----> called setData -> set to pending premium");
		error_log("---------------------------------------------");
		$newResult = $db->update('users', $currentUser->data()->id, array('premium' => 2), 'id');
	} else if($_POST['type'] == 'rasub'){	// reactivate subscription

		error_log("---------------------------------------------");
		error_log("----> called setData -> reactivate subscription");
		error_log("---------------------------------------------");
		$results = $db->get('users_subs', array('user_id', '=', $currentUser->data()->id), 'subscription_id, created, status', array('created', 'desc'))->results();
		//var_dump($results);
		
		if(!empty($results) && $results[0]->status == 'SUSPENDED'){
			
			$sub = new Subscription();
			$sub->ReactivateSubscription($results[0]->subscription_id);
			$newResult = $db->update('users', $currentUser->data()->id, array('premium' => 4), 'id');
		}
		//var_dump($results);
		/*$subObj = new Subscription();
		
		$subObj->SuspendOrCancelSubscriptionByUserId($currentUser->data()->id);*/
		
		/*exit();
		
		
		$results = $db->get('users_subs', array('user_id', '=', $currentUser->data()->id), 'subscription_id, status')->results();
			
		if(!empty($results) && $results[0]->status == 'ACTIVE'){
			var_dump(CancelSubscription($results[0]->subscription_id));
		}*/
		
	} else if($_POST['type'] == 'sussub'){	// suspend subscription

		$newResult = $db->update('users', $currentUser->data()->id, array('premium' => 3), 'id');
		$subObj = new Subscription();
		
		$subObj->SuspendOrCancelSubscriptionByUserId($currentUser->data()->id, false);
		
		/*exit();
		
		
		$results = $db->get('users_subs', array('user_id', '=', $currentUser->data()->id), 'subscription_id, status')->results();
			
		if(!empty($results) && $results[0]->status == 'ACTIVE'){
			var_dump(CancelSubscription($results[0]->subscription_id));
		}*/
		
	} else if($_POST['type'] == 'asubp'){	// subscription account processing
		echo 'got here';
		$results = $db->get('users_subs', array('user_id', '=', $currentUser->data()->id, 'status', '=', 'ACTIVE'), 'id')->results();
			
			if(!empty($results)){
				var_dump($results);
				//$newResult = $db->update('OL_students', $results[0]->id, array('is_verified' => $newVal, 'date_added' => gmdate('Y-m-d H:i:s')), 'id');
			}
		
		/*exit();
		
		
		$results = $db->get('users_subs', array('user_id', '=', $currentUser->data()->id), 'subscription_id, status')->results();
			
		if(!empty($results) && $results[0]->status == 'ACTIVE'){
			var_dump(CancelSubscription($results[0]->subscription_id));
		}*/
		
			
	}  else if($_POST['type'] == 'ol_aodstc'){	// accept or deny student into classroom
		if($currentUser->isTeacher()){
			
			if($_POST['addstud'] === 'true'){
				$newVal = 1;
			} else {
				$newVal = -1;
			}
			
			$results = $db->get('OL_classrooms', array('OL_classrooms.id', '=', $_POST['cid'], 'teach_id', '=', $currentUser->data()->id, 'stud_id', '=', $_POST['sid']), 'OL_students.id', null, array('OL_students','id','class_id'))->results();
			
			if(!empty($results)){
				//var_dump($results[0]->id);
				$newResult = $db->update('OL_students', $results[0]->id, array('is_verified' => $newVal, 'date_added' => gmdate('Y-m-d H:i:s')), 'id');
			}
			echo $newResult;
		}
	} else if($_POST['type'] == 'ol_rsfc'){	// remove students from class

		if($currentUser->isTeacher()){
			$results = $db->get('OL_classrooms', array('id', '=', $_POST['cid'], 'teach_id', '=', $currentUser->data()->id), 'id')->results();
			
			if(!empty($results)){
				$db->delete('OL_students', array('class_id', '=',$_POST['cid'], 'stud_id', 'IN', $_POST['aos']));
			}
		}
	} else if($_POST['type'] == 'ol_ecrn'){	// edit classroom name

		if($currentUser->isTeacher()){
			$results = $db->get('OL_classrooms', array('id', '=', $_POST['cid'], 'teach_id', '=', $currentUser->data()->id), 'id')->results();
			
			if(!empty($results) && isset($_POST['ncrn'])){
				$newResult = $db->update('OL_classrooms', $results[0]->id, array('class_name' => $_POST['ncrn']), 'id');
				if($newResult === true){
					echo 'Successfully updated class name';
				} else {
					echo 'Class name update failed';
				}
			} else {
				echo 'Unable to edit class name';
			}
		}
	} /*else if(isset($_POST['subid'])){
		$result = $db->insert('users_subs', array('payer_email' => $currentUser->data()->email, 'subscription_id' => $_POST['subid']));
	}*/
		
}
	
// user is not logged in... DO NOTHING!

?>